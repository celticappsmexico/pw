<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaccionBraintree extends Model
{
    //
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transacciones_braintree';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['tb_tipo',
			'tb_estatus',
			'tb_respuesta'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	*/
	protected $hidden = [];
	
	public function user(){
		return $this->belongsTo(User::class);
	}

	
}
