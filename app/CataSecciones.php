<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Articulos;

class CataSecciones extends Model
{
    protected $table = 'cata_secciones';
    protected $primarykey = 'id';
    protected $fillable = [
        'id', 'nombre', 'active'
    ];

    public function articulos(){
        return $this->hasMany(Articulos::class, 'id_seccion')->where('active', 1);
    }
}
