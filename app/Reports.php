<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $fillable = [
    						'month',
							'day',
							'year',
							'date',
							'month_total_amount',
							'rent_percent',
							'storages_rent'
	                      ];
}