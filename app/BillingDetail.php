<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingDetail extends Model
{
    //
	protected $table = 'billing_detail';
	
	protected $fillable = [
			'billing_id',
			'bd_nombre',
			'bd_codigo',
			'bd_valor_neto',
			'bd_porcentaje_vat',
			'bd_total_vat',
			'bd_total',
			'bd_tipo_partida',
			'bd_numero'

			
	];
	
	public function billing() {
		return $this->belongsTo(Billing::class);
	}
}

