<?php

	namespace App\Library;

	class FileSystem {
		
		
		/**
		 * regresa array
		 * @param $pathDir
		 */
		public static function getDirFiles($pathDir){
			$arrOutput = array();
			$gestor = opendir($pathDir);
			while ($entrada = readdir($gestor)) {
        		//echo "$entrada\n";
        		array_push($arrOutput,$entrada);
    		}
 			closedir($gestor);
 			return $arrOutput;
		}
		
		public static function crearCarpeta($path,$access)
		{
			//echo "<br>Path: $path";
			//echo "<br>Access: $access";
			if (!is_dir($path)) {
				mkdir($path, $access);
			}
			
		}
		
		public static function borrarArchivosDir($dir){
			
			$ficheroseliminados= 0;
			$handle = opendir($dir);
			while ($file = readdir($handle))
			{
				if (is_file($dir.$file))
				{
					if ( unlink($dir.$file) )
					{
						$ficheroseliminados++;
					}
				}
			}
		}
		
		
		public static function getMIMEType($ext){
			
			switch ($ext) { 
		      case "pdf": $ctype="application/pdf"; break; 
		      case "exe": $ctype="application/octet-stream"; break; 
		      case "zip": $ctype="application/zip"; break; 
		      case "doc": $ctype="application/msword"; break; 
		      case "xls": $ctype="application/vnd.ms-excel"; break; 
		      case "ppt": $ctype="application/vnd.ms-powerpoint"; break; 
		      case "gif": $ctype="image/gif"; break; 
		      case "png": $ctype="image/png"; break; 
		      case "jpeg": $ctype="image/jpg"; break;
		      case "jpg": $ctype="image/jpg"; break;
		      // SSL
		      case "cer": $ctype="application/pkix-cert"; break; 
		      //case "cer": $ctype="application/octet-stream"; break; 
		      
		      case "key" : $ctype="application/pkcs8"; break;
		      // WORD X
		      case "docx" : $ctype="application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
		      case "xlsx" : $ctype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; break;
		      case "pptx" : $ctype="application/vnd.openxmlformats-officedocument.presentationml.presentation"; break;
		      // DEFAULT
		      default: $ctype="application/force-download"; 
			} 
			return $ctype;
		}
		
		public static function agregarHtaccessDenyAll($dir){
			if(!file_exists($dir.".htaccess")){
				$strDeny = "Deny from all";
				$handle = fopen($dir.".htaccess","w+");
				fwrite($handle,$strDeny);
				fclose($handle);
			}
		}
		/**
		 * Crea Zip de directorio completo
		 * @param $zipPath  - Path absoluto con nombre, donde se crea el zip
		 * @param $srcDir   - Directorio a zippear
		 */
		public static function crearZIPDir($zipPath,$srcDir){
			
			$zip = new ZipArchive();
			$zip->open($zipPath,ZipArchive::CREATE);
			$files= scandir($srcDir);
			//var_dump($files);
			unset($files[0],$files[1]);
			foreach ($files as $file) {
			  $zip->addFile("{$file}");    
			}
			$zip->close();
			
		}
		/**
		 * Crea Zip de una lista de archivos
		 * @param $zipPath  - Path absoluto con nombre, donde se crea el zip
		 * @param $arrFiles - Arreglo de archivos con path a zippear
		 */
		public static function crearZIPFiles($zipPath,$arrFiles){

			$zip = new \ZipArchive();
		
			
			$zip->open($zipPath,\ZipArchive::CREATE);
		
			foreach ($arrFiles as $file) {
			  $fname = explode("/",$file);
			  $fname = $fname[sizeof($fname)-1];
			  //$zip->addFile("{$file}");  
			  $zip->addFile($file,$fname);
			}
			$zip->close();
			
		}
		
		public static function crearArchivo($pathFile,$content){
			$handle = fopen($pathFile,"w+");
			fwrite($handle,$content);
			fclose($handle);
		}
		
	}


?>