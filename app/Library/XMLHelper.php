<?php

namespace App\Library;


use Psy\Util\Json;
use Illuminate\Support\Facades\Log;

class XmlCfdiHelper {
    
    
    public static function json2Xml(&$json){
        
        
        
        // CFDI..
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<tns:JPK xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://jpk.mf.gov.pl/wzor/2016/03/09/03095/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/">';
        
        $xml .= '<tns:KodFormularza kodSystemowy="JPK_FA (1)" wersjaSchemy="1-0">JPK_FA</tns:KodFormularza>
                		<tns:WariantFormularza>1</tns:WariantFormularza>
                		<tns:CelZlozenia>1</tns:CelZlozenia>
                		<tns:DataWytworzeniaJPK>2018-09-04T10:53:05</tns:DataWytworzeniaJPK>
                		<tns:DataOd>2018-08-01</tns:DataOd>
                		<tns:DataDo>2018-08-31</tns:DataDo>
                		<tns:DomyslnyKodWaluty>PLN</tns:DomyslnyKodWaluty>
                		<tns:KodUrzedu>1473</tns:KodUrzedu>
                	</tns:Naglowek>
                ';
        
        /*$xml .= '<cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" Sello="" ';
        self::popularAtributos($json, $xml);
        $xml.= ">";
        
        // CFDIS RELACIONADOS ----------
        if(count($json->CfdiRelacionados->cfdis) > 0){
            $xml.= '<cfdi:CfdiRelacionados';
            self::popularAtributos($json->CfdiRelacionados, $xml);
            $xml.= '>';
            foreach ($json->CfdiRelacionados->cfdis as $rel){
                $xml.= '<cfdi:CfdiRelacionado';
                self::popularAtributos($rel, $xml);
                $xml.= '/>';
            }
            
            $xml.= '</cfdi:CfdiRelacionados>';
        }
        
        // EMISOR..
        $xml.= '<cfdi:Emisor';
        self::popularAtributos($json->Emisor, $xml);
        $xml.= '/>';
        
        // RECEPTOR..
        $xml.= '<cfdi:Receptor';
        self::popularAtributos($json->Receptor, $xml);
        $xml.="/>";
        
        // CONCEPTOS...
        $xml.= '<cfdi:Conceptos>';
        foreach($json->Conceptos as $concepto){
            $xml.= '<cfdi:Concepto';
            self::popularAtributos($concepto, $xml); // TODO COMPLMENETOS CONCEPTO, NO APLICAN PARA NOMINA
            
            if(isset($concepto->Impuestos) && (count($concepto->Impuestos->Traslados) > 0
                || count($concepto->Impuestos->Retenciones) > 0)){
                    $xml.= '>';
                    $xml.= '<cfdi:Impuestos>';
                    if(count($concepto->Impuestos->Traslados) > 0){
                        $xml.= '<cfdi:Traslados>';
                        foreach($concepto->Impuestos->Traslados as $traslado){
                            $xml.= '<cfdi:Traslado';
                            self::popularAtributos($traslado, $xml);
                            $xml.= '/>';
                        }
                        $xml.= '</cfdi:Traslados>';
                    }
                    if(count($concepto->Impuestos->Retenciones) > 0){
                        $xml.= '<cfdi:Retenciones>';
                        foreach($concepto->Impuestos->Retenciones as $retencion){
                            $xml.= '<cfdi:Retencion';
                            self::popularAtributos($retencion, $xml);
                            $xml.= '/>';
                        }
                        $xml.= '</cfdi:Retenciones>';
                    }
                    
                    $xml.= '</cfdi:Impuestos>';
                    
                    // REVISAMOS INFORMACION ADUANERA..
                    if(isset($concepto->InformacionAduanera)){
                        $xml.= '<cfdi:InformacionAduanera NumeroPedimento="'.$concepto->InformacionAduanera->NumeroPedimento.'"/>';
                    }
                    
                    $xml.= '</cfdi:Concepto>';
            }else{
                $xml.= '/>';
            }
        }*/
        $xml.= '</tns:JPK>';
        
        return $xml;
        
    }
    
    
}