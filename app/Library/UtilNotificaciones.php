<?php
namespace App\Library;
use App\Library\PushBots;
use App\User;

class UtilNotificaciones {

    public static function Enviar($mensaje, $user_id) {
        $pb = new PushBots();
        ////ENVIO DE  NOTIFICACIONES
        $pb->App(env('APPID'), env('APPSECRET'));
        
        $customfields = array("lIco" => env('LOGO_NOTIFICACION'));

        $pb->Payload($customfields);
        // Notification Settings
        $pb->Alert($mensaje);
        $pb->Platform(array("0","1"));
        $pb->Badge("+1");
        $pb->Sound("");

        $pb->Alias($user_id);
        //Push to Single Device
        $pb->PushOne();
        // Push it !
        $pb->Push();
    }

    public static function EnviarNotice($mensaje, $user_id) {
        $pb = new PushBots();
        //ENVIO DE  NOTIFICACIONES 
        $pb->App(env('APPID'), env('APPSECRET'));

        $customfields = array("nextActivity" => "notificacion", "lIco" => env('LOGO_NOTIFICACION'));
        $pb->Payload($customfields);
        // Notification Settings
        $pb->Alert($mensaje);

        $pb->Platform(array("0","1"));
        
        $pb->Badge("+1");
        $pb->Sound("");

        $pb->Alias($user_id);
        //Push to Single Device
        $pb->PushOne();
        // Push it !
        $pb->Push();        
    }

}
?>
