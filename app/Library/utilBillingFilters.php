<?php
namespace App\Library;
use App\Billing;
use App\Storages;
class utilBillingFilters {

    /**
     * Method to get the billing details of the year and month selected
     * @param  [type] $year  [Year Selected]
     * @param  [type] $month [Month Selected]
     * @return [type]        [Return the Client with the sum of his amounts and charges in the specific date]
     */
    public static function getClients($year, $month, $levels) {
      if ($levels != 'null') {
        $array = explode(',', $levels);
        $id_storages = Storages::whereIn('level_id', $array)->lists('id')->toArray();
        // dd($id_storages);
      }else {
        $id_storages = Storages::lists('id')->toArray();
      }
      $clients = Billing::with(['user'])
      ->where(function($qry) use($year, $month, $id_storages){
        $qry->whereIn('storage_id', $id_storages);
        $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $month);
        $qry->where(\DB::raw('YEAR(`pay_month`)'), '=', $year);
      })
      ->selectRaw("user_id, sum(abono) as sumAbonos, sum(cargo) as sumCargos, $month as month, $year as year, (sum(abono)-sum(cargo)) as balance")
      ->groupBy('user_id')
      ->get();
      // dd($clients);
      return $clients;
    }

    /**
     * Method to get the complete billing details of a client
     * @param  [type] $id [Id of a client]
     * @return [type]     [Return the client with the entire amounts and charges made]
     */
    public static function getBillingClientDetails($id, $year, $month) {
      $billings = Billing::with(['user', 'storages.level'])
      ->where(function($qry) use ($id, $year, $month){
          $qry->where(\DB::raw('user_id'), '=', $id);
          $qry->where(\DB::raw('YEAR(`pay_month`)'), '=', $year);
          $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $month);
      })
      ->selectRaw("user_id, storage_id, abono, cargo, pay_month, (abono-cargo) as balance")
      ->get();
      // dd($billings);
      return $billings;
    }

    /**
     * Method to filter the Billings per month and year
     * @param  [type] $año [Year to filter]
     * @return [type]      [Return the Billings of the year selected]
     */
    public static function getMesesMontoAnio($año, $levels) {
      // dd($level);
      if ($levels != 'null') {
        $array = explode(',', $levels);
        $id_storages = Storages::whereIn('level_id', $array)->lists('id')->toArray();
        // dd($id_storages);
      }else {
        $id_storages = Storages::lists('id')->toArray();
      }
      $ultimo = "";
      $meses = array();
      for ($i=12; $i != 0; $i--) {
        if ($i == 1) {
            $febrero = Billing::with(['storages'])
            ->selectRaw("$i as mes, YEAR(`pay_month`) as year, MONTHNAME('0000-".$i."-00') as monthName, COALESCE(sum(abono), 0) as sum_abonos, COALESCE(sum(cargo), 0) as sum_cargos, storage_id as storage, (COALESCE(sum(abono), 0)-COALESCE(sum(cargo), 0)) as balance")
            ->where(function($qry) use($i, $año, $id_storages) {
              $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $i);
              $qry->whereIn('storage_id', $id_storages);
              if ($año != '') {
                $qry->where(\DB::raw('YEAR(`pay_month`)'), '=', $año);
              }
            })
            ->unionAll($meses[10])
            ->unionAll($meses[9])
            ->unionAll($meses[8])
            ->unionAll($meses[7])
            ->unionAll($meses[6])
            ->unionAll($meses[5])
            ->unionAll($meses[4])
            ->unionAll($meses[3])
            ->unionAll($meses[2])
            ->unionAll($meses[1])
            ->unionAll($meses[0])
            ->get();
            $ultimo = $febrero;
        }else {
            $enero = Billing::with(['storages'])
            ->selectRaw("$i as mes,YEAR(`pay_month`) as year, MONTHNAME('0000-".$i."-00') as monthName, COALESCE(sum(abono), 0) as sum_abonos, COALESCE(sum(cargo), 0) as sum_cargos, storage_id as storage, (COALESCE(sum(abono), 0)-COALESCE(sum(cargo), 0)) as balance")
            ->where(function($qry) use($i, $año, $id_storages) {
                $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $i);
                $qry->whereIn('storage_id', $id_storages);
                if ($año != '') {
                  $qry->where(\DB::raw('YEAR(`pay_month`)'), '=', $año);
                }
            });
            $enero->mesNum=$i;
            array_push($meses, $enero);
        }
      }
      return $ultimo;
    }

    // /**
    //  * Method to show the sum of billings per month
    //  * @return [type] [Return the sum of billings per month]
    //  */
    // public static function getMesesMonto(){
    //     $ultimo = "";
    //     $meses = array();
    //     for ($i=12; $i != 0; $i--) {
    //         if ($i == 1) {
    //             $febrero = Billing::
    //             selectRaw("MONTH(`pay_month`) as mes, MONTHNAME(STR_TO_DATE(MONTH(`pay_month`),'%m')) as monthName, COALESCE(sum(abono), 0) as sum_abonos, COALESCE(sum(cargo), 0) as sum_cargos ")
    //             ->where(function($qry) use($i) {
    //                 $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $i);
    //                 $qry->where(\DB::raw('storage_id'), '=', 1);
    //             })
    //             ->unionAll($meses[10])
    //             ->unionAll($meses[9])
    //             ->unionAll($meses[8])
    //             ->unionAll($meses[7])
    //             ->unionAll($meses[6])
    //             ->unionAll($meses[5])
    //             ->unionAll($meses[4])
    //             ->unionAll($meses[3])
    //             ->unionAll($meses[2])
    //             ->unionAll($meses[1])
    //             ->unionAll($meses[0])
    //             ->get();
    //             $ultimo = $febrero;
    //         }else {
    //             $enero = Billing::
    //             selectRaw("$i as mes, MONTHNAME('0000-".$i."-00') as monthName, COALESCE(sum(abono), 0) as sum_abonos, COALESCE(sum(cargo), 0) as sum_cargos ")
    //             ->where(function($qry) use($i) {
    //                 $qry->where(\DB::raw('MONTH(`pay_month`)'), '=', $i);
    //             });
    //             $enero->mesNum=$i;
    //             array_push($meses, $enero);
    //         }
    //     }
    //     return $ultimo;
    // }

}
?>
