<?php 

     /* ************************************************************
     ** services.Formato.class.php *****************************
     ** Author rcarvalho@mysystemsolution  ***********************
     ** Clase utileria para formatos ***************************
     ************************************************************* */

    namespace App\Library;
    use Carbon\Carbon;
	
	class MonthToWords {
		
		public static function convertMonthToWords($date){
            
			//OBITIENE MES
            setlocale(LC_ALL, 'de_DE');
            $mes = "";
            
            switch(Carbon::parse($date)->format('m'))
            {
                case '01':
                    $mes = 'styczeń';
                break;
                case '02':
                    $mes = 'luty';
                break;
                case '03':
                    $mes = 'marzec';
                break;
                case '04':
                    $mes = 'kwiecień';
                break;
                case '05':
                    $mes = 'maj';
                break;
                case '06':
                    $mes = 'czerwiec';
                break;
                case '07':
                    $mes = 'lipiec';
                break;
                case '08':
                    $mes = 'sierpień';
                break;
                case '09':
                    $mes = 'wrzesień';
                break;
                case '10':
                    $mes = 'październik';
                break;
                case '11':
                    $mes = 'listopad';
                break;
                case '12':
                    $mes = 'grudzień';
                break;
                                                            
            }
                    
            $mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');

            return $mes;
            
		}
		
	}
	