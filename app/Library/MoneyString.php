<?php 

     /* ************************************************************
     ** services.Formato.class.php *****************************
     ** Author rcarvalho@mysystemsolution  ***********************
     ** Clase utileria para formatos ***************************
     ************************************************************* */

	namespace App\Library;
	use NumberToWords\NumberToWords;
	use Illuminate\Support\Facades\Log;
	
	class MoneyString {
		
		public static function transformQtyCurrency($quantity){
			
			//https://packagist.org/packages/kwn/number-to-words
			
			$quantityAux = number_format($quantity,2,","," ");
				
			$decimals = strstr($quantityAux, ',', false);
			$decimals = str_replace(",","",$decimals);
			
			// create the number to words "manager" class
			$numberToWords = new NumberToWords();
			
			// build a new number transformer using the RFC 3066 language identifier
			$numberTransformer = $numberToWords->getNumberTransformer('pl');
			//echo ' Quantity: '.$quantity;
			
			$quantityString = $numberTransformer->toWords($quantity, 'PLN'); // outputs "five thousand one hundred twenty"
			
			//echo ' String : '.$quantityString;
			
			//
			$numberTransformer->toWords($quantity, 'PLN'); // outputs "five thousand one hundred twenty"
			$lastDigit = substr($quantity, -2); // 8
			 
			if($lastDigit == 1){
				$currency = 'złotych';
			}elseif($lastDigit > 1 && $lastDigit <= 4){
				$currency = 'złote';				
			}else{
				$currency = 'złotych';
			}
			
			return $quantityString. " ". $currency . " ".$decimals."/100.";
			
			/*
			// create the number to words "manager" class
			$numberToWords = new NumberToWords();
				
			// build a new number transformer using the RFC 3066 language identifier
			$numberTransformer = $numberToWords->getNumberTransformer('pl');
			echo ' Quantity: '.$quantity;
				
			$quantityString = $numberTransformer->toWords($quantity, 'PLN'); // outputs "five thousand one hundred twenty"
				
			echo ' String : '.$quantityString;
				
			//
			$stringAux = $numberTransformer->toWords($quantity, 'PLN'); // outputs "five thousand one hundred twenty"
			
			echo ' String Aux: '.$stringAux;
			
			
			$lastDigit = substr($quantity, -2); // 8
			
			if($lastDigit == 1){
				$currency = 'złotych';
			}elseif($lastDigit > 1 && $lastDigit <= 4){
				$currency = 'złote';
			}else{
				$currency = 'złotych';
			}
				
			
			return $quantityString;
				
	*/		
		}
		
	}
	