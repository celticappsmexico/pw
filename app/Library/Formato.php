<?php 

     /* ************************************************************
     ** services.Formato.class.php *****************************
     ** Author rcarvalho@mysystemsolution  ***********************
     ** Clase utileria para formatos ***************************
     ************************************************************* */

	namespace App\Library;

	class Formato {
		
 
		
            public static function formatear($formato,$str) {
            	
            	
            	if($formato == "STRING"){
            		
            		// hacer nada
            		
            	}else if($formato == "MONEY"){
            		
            		if(!is_numeric($str)){
            			//echo "NO ES NUMERICO: *$str* ";
            			str_replace("\$","",$str);
            			str_replace(",","",$str);
            			
            			if(!is_numeric($str)){
            				$str = 0;
            			}
            			if(trim($str) ==""){
            				$str = 0;
            			}
            			//echo "NUEVO VALOR: *$str*    ";
            		}
            		
            		//  "en formato: $formato  $str ";
            		//echo "money: ".$str;
            		//$str = $str == "" ? 0 : $str;
            		//$str = "$".number_format($str,2,","," ");
            		$str = number_format($str,2,","," ");
            		
            	}else if($formato == "ZLOTY"){
            		
            		if(!is_numeric($str)){
            			//echo "NO ES NUMERICO: *$str* ";
            			str_replace("\$","",$str);
            			str_replace(",","",$str);
            			
            			if(!is_numeric($str)){
            				$str = 0;
            			}
            			if(trim($str) ==""){
            				$str = 0;
            			}
            			//echo "NUEVO VALOR: *$str*    ";
            		}
            		
            		//  "en formato: $formato  $str ";
            		//echo "money: ".$str;
            		//$str = $str == "" ? 0 : $str;
            		$str = number_format($str,2,","," ")." pln";
            		
            	}else if($formato == "DATE"){
            		
            		list($YY, $MM, $DD) = explode('-', $str);
					$str = $DD.'/'.$MM.'/'.$YY;
            		
            	}else if($formato == "PERCENT"){
            		
            		$str = $str."%";
            		
            	}else if($formato == "DECIMAL2"){
            		
            		$str = number_format($str,2);
            		
            	}else if($formato == "SEMAFORO"){
            		if($str=='ROJO'){
            			$str = "<img height='32px' src='imagenes/iconos/s_rojo.png' />";
            		}else if($str=='AMARILLO'){
            			$str = "<img height='32px' src='imagenes/iconos/s_amarillo.png' />";
            		}else if($str=='VERDE'){
            			$str = "<img height='32px' src='imagenes/iconos/s_verde.png' />";
            		}else if($str=='MAXIMO'){
            			$str = "<img height='32px' src='imagenes/iconos/stop_mano.png' />";	
            		}
            	}else if($formato == "INPUT_FILE"){
            		// SE USA PARA DESCARGA PROTEGIDA EN MODX!
            		$str = "<a href='index.php?id=179&fname=".base64_encode($str)."'>{{img_descargar}}</a>";
            	}else if($formato == "TIMESTAMP"){
            		if($str=="0000-00-00 00:00:00"){
            			$str = "---";
            		}
            	}else if ($formato == "ESTATUS")
                 {
	                $flag= $str;
	                if($flag == 0 )
		                {
		                $str = '<img border="0" src="imagenes/iconos/flag1.png" />';
	                    }
                    else{
                        $str = '<img border="0" src="imagenes/iconos/flag0.png" />';
                        }
                 }else if($formato == "BOOLEAN"){
                 	if($str=="1"){
                 		$str = '<img border="0" src="imagenes/iconos/flag1.png" />';
                 	}else{
                 		$str = ' --- ';
                 	}
                 }
                 else if ($formato == "flag")
                    {
		                     $id= $str;                     
		                     $pieces = explode("-", $id);
		                    $estatus= $pieces[2];
		                    if($estatus == 0 ){
		                    			$str = '<img border="0" id="flag_servicios_cambio" dir="'.$id.'" src="imagenes/iconos/flag_of.png"  title="verde'.$estatus.'"/>';
		                  			  }
		                  	elseif($estatus == 1){
		                    					$str = '<img border="0" id="flag_servicios_cambio" dir="'.$id.'" src="imagenes/iconos/flag_on.png"  title="rojo'.$estatus.'"/>';     	
		                  					  }
  
                    }
               else if($formato == "DETALLE_EMPLEADO"){
                  	if($str==""){
                   		$str = "---";
                   	}else{
                   		$str ="<a href='index.php?id=257&id_empleado=".base64_encode($str)."'> <img src='imagenes/iconos/zoom.png'/> </a>";
               	}
               }else if($formato == "DETALLE_CLIENTE"){
                  	if($str==""){
                   		$str = "---";
                   	}else{
                   		$str = '<img src="imagenes/iconos/lupa.png" style="cursor:pointer" class="detalle_cliente" id="'.$str.'"/>';
               	}
               	
               }else if($formato == "DETALLE_PRODUCTO"){
                  	if($str==""){
                   		$str = "---";
                   	}else{
                   		$str = '<img src="imagenes/iconos/zoom.png" style="cursor:pointer" class="detalle_productos" id="'.base64_encode($str).'"/>';
               	}
               }else if ($formato == "PDF"){
            		 if($str==""){
            		 	$str = "---";
            		 }else{
                     	$str = "<a href='index.php?id=179&fname=".base64_encode($str)."'>{{img_descarga_pdf}}</a>";    
            		 }
            	}
            	else if ($formato == "XML"){
            		 if($str==""){
            		 	$str = "---";
            		 }else{
                       $str = "<a href='index.php?id=179&fname=".base64_encode($str)."'>{{img_descarga_xml}}</a>";        
                
            		 }
            	}
            	else if ($formato == "KEY")
            	{
            		$str;
            		$str = "<a href='index.php?id=179&fname=".base64_encode($str)."'><img border='0' src='imagenes/iconos/descarga.png'></a>";
            		 
            	}
            	
            	else if ($formato == "CER")
            	{
            		$str;
            		$str = "<a href='index.php?id=179&fname=".base64_encode($str)."'><img border='0'src='imagenes/iconos/descarga.png'></a>";
            	}
            	
            	else if ($formato == "RESPALDO")
            	{
            		if($str==""){
            			$str = "---";
            		}else{
            		$str = "<a href='index.php?id=179&fname=".base64_encode($str)."'><img border='0'src='imagenes/iconos/descarga.png'></a>";
            		  }
            		}
            		else if ($formato == "MES")
            		{
            			if($str==""){
            				$str = "---";
            			}else{
            				$mes='';
            				$meses =array(1 =>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril',5=>'Mayo',6=>'Junio',7=>'Julio',8=>'Agosto',9=>'Septiembre',10=>'Octubre',11=>'Noviembre',12=>'Diciembre');
                            for($i=1; $i<=12;$i++){
                            	if($str == $i ){
                            		$mes = $meses[$i];
                            	}
                            }    
            				
            				
            				$str = "<div>".$mes."</div>";
            			}
            		}
            		
            		
            	else if ($formato == "DETALLEMISOR"){
            		$str;
            		$str = '<img border="0" src="imagenes/iconos/edita.png"  style="cursor:pointer"  title="Editar" dir="'.$str.'" class="edita_consultorio"   >';	
            	}
            	else if ($formato == "CFDI_REPROCESAR")
                    {
                     // 0 - con error
                     // 1 - exitoso 
                     $str = explode("_",$str);
                     $idCFDI = $str[0];
                     $status = $str[1];
                     $path= "index.php?id=177&c=".$idCFDI;
                     if($status==0){
                     	$str = '<a  href="'.$path.'">{{img_reprocesar_cfdi}}</a>';  
                     }else{
                     	$str = '{{img_reprocesar_cfdi_off}}'; 
                     }
                        
           		 }
            	else if ($formato == "CFDI_CANCELAR")
                    {
                    	
                    	// 0 - con error
                    	// 1 - exitoso
                    	//2 -  cancelado
                    	
                    	$str = explode("_",$str);
                    	$idCFDI = $str[0];
                    	$status = $str[1];
                    	$path= "index.php?id=177&c=".$idCFDI;
                    	if($status==1){
                    $str = '<div  id="cfdi_cancelar" dir="'.$idCFDI.'"  style="cursor:pointer"; >{{img_cancelar_cfdi}}</div>';
                    		 
                    	}
                    	elseif ($status == 2){
                    		$str = '---';
                    		 
                    	}
                    	
                    	
                    	else{
                    		$str = '{{img_cancelar_cfdi_off}}';
                    	}
                    	 
                    	 
                    	    
           		 }
            	else if ($formato == "CFDI_ESTATUS")
                 {
	                $flag= $str;
	                if($flag == 1 )
		                {
		                $str = '<img border="0" src="imagenes/iconos/flag1.png" /> OK';
	                    }
                    else if($flag ==0){
                        $str = '<img border="0" src="imagenes/iconos/flag0.png" /> ERROR';
                        }
                    else if($flag ==2){
                    	$str = '<img border="0" src="imagenes/iconos/cancel_small.png" /> CANCELADO';
                        
                    }
              
                   }else if ($formato == "VER_CFDI"){
                	$str = "<a href='index.php?id=236&id_lote_batch=".$str.'\'><img border="0" src="imagenes/iconos/zoom.png"/>CFDIs</a>';    
            	}
             	else if($formato == "ENVIO_EMAIL"){
             		$str = '<img border="0" src="imagenes/iconos/mail.png" class="icono_envio_mail" id="'.$str.'" style="cursor:pointer" />';
             	}
             	else if($formato == "ADDENDA"){
             		$str = explode("_",$str);
                    $rfc = $str[0];
                    $uuid = $str[1];
                    $str = '<img border="0" src="imagenes/iconos/addenda.png" class="show_addenda" id="'.$rfc.'" lang="'.$uuid.'" style="cursor:pointer" />';
                    //$str = '<a href="http://timbrado.expidetufactura.com.mx/addendas/ado.php?rfc='.$rfc.'&uuid='.$uuid.'"\'><img border="0" src="imagenes/iconos/addenda.png"/></a>';
             	}
             	else if($formato == "PDF_EXT"){
             		$str = "<a href='index.php?id=323&fname=".base64_encode($str)."'>{{img_descarga_pdf}}</a>";
             			   
             	}
             	else if($formato == "XML_EXT"){
             		$str = "<a href='index.php?id=323&fname=".base64_encode($str)."'>{{img_descarga_xml}}</a>";
             	}
            	else if($formato == "STATUS_VALIDACION"){
             		if($str == "1"){
             			$str = "{{img_ok_mini}} VALIDO";
             		}else if($str =="2"){
             			$str = "{{img_error_mini}} INVALIDO";
             		}else if($str == "3"){
             			$str = "{{img_error_mini}}INVALIDO - CANCELADO";
             		}else if($str=="4"){
             			$str = "{{img_pendiente_mini}}VALIDACION PENDIENTE";	
             		}else if($str == "5"){
             			$str = "{{img_error_mini}}ERROR AL CANCELAR";
             		}
             	}
             	else if($formato == "STATUS_ENVIO_MAIL"){
             		if($str == 1 ){
		                $str = '<img border="0" src="imagenes/iconos/flag1.png" />';
	                    }
                    else{
                        $str = '---';
                        } 
             	}else if($formato == "CHECKBOX"){
             		if($str == 1 ){
		                $str = '<img border="0" src="imagenes/iconos/flag1.png" />';
	                    }
                    else{
                        $str = '';
                        } 
             	}
             	
             	////////////////////////////////////////////////////////////////////////
             	
             else if ($formato == 'EDITASEC'){
             	$str = '<img border="0" src="imagenes/iconos/edita.png"  style="cursor:pointer"  title="Edita" id="'.$str.'" class="edita_seccion"   >';
             }
             else if ($formato == 'DETALLE'){
             	$str = '<img border="0" src="imagenes/iconos/lupa.png"  style="cursor:pointer"  title="Edita" id="'.$str.'" class="edita_seccion"   >';
             }

             else if ($formato == 'FOTO'){
             	$str = '<img border="0" src="'.$str.'"  style="cursor:pointer;  "  id="'.$str.'"     class="ver_foto" >';
             }
             else if($formato == 'ROUDN'){
             	$str = round($str, 2);
             }else if ($formato == "USER")
                 {
	                $flag= $str;
	                if($flag >=1 )
		                {
		                $str = '<img border="0" src="imagenes/iconos/user_sistem.png" />';
	                    }
                    else{
                        $str = '<img border="0" src="imagenes/iconos/no_user.png" />';
                        }
                   }
              else if($formato == 'BORRADOLOG'){

               
              	$str = '<img border="0"  id="'.$str.'"     class="borrar_usuario" style="cursor:pointer"  src="imagenes/iconos/elimina_archivo.png" />';
              }
              else if($formato == 'TESTIMP'){
              	$str = '<img border="0"  id="'.$str.'"     class="tets_printer" style="cursor:pointer"  src="imagenes/iconos/tets_printer.png" title="Prueba de Impresion" />';
              	
              } else if($formato == 'FLAGREINICIO'){
                    $flag= $str;

                    if($flag == 0){
                      $str = '<img border="0"  id="'.$str.'"     class="reinicio_on" style="cursor:pointer"   src="imagenes/iconos/reinicio_of.png" title="Reinicio Stock Diario" />';

                    }
                    else{
                      $str = '<img border="0"  id="'.$str.'"     class="reinicio_on" style="cursor:pointer"   src="imagenes/iconos/reinicio_on.png" title="Reinicio Stock Diario" />';

                    }
               
              }
              else if($formato == 'STOCK'){
                $str = '<img border="0"  id="'.$str.'"     class="alimenta_stock" style="cursor:pointer"  src="imagenes/iconos/stock.png" title="Alimentar Stock" />';
                
              }else if($formato == "BACKUP_FILE"){
              	$str= "<a href=\"index.php?id=179&fname=".base64_encode($str)."\" target=\"_blank\"><img height=\"30px\" src=\"imagenes/iconos/img-backup.png\" /></a>";
              	
              }else if($formato == "DIA_SEMANA"){
              		$numDia = date('N', strtotime($str)); 
              		
		              if($numDia == 0){
						$ret = "Domingo";
					}else if($numDia == 1){
						$ret = "Lunes";
					}else if($numDia == 2){
						$ret = "Martes";
					}else if($numDia == 3){
						$ret = "Miercoles";
					}else if($numDia == 4){
						$ret = "Jueves";
					}else if($numDia == 5){
						$ret = "Viernes";
					}else if($numDia == 6){
						$ret = "Sabado";
					}
					return $ret;
              }else if($formato == "ORDEN_SECCION"){
              	$arrVals = explode(",",$str);
              	$str = "<input type= \"text\" id=\"ord_".$arrVals[0]."\" class=\"edita_orden_input\" value=\"".$arrVals[1]."\"> 
              			<input type=\"button\" id=\"bord_".$arrVals[0]."\" value=\"Guardar\" class=\"edita_orden\">";
              }else if($formato == "STATUS_AJUSTE"){
              	if($str=="0"){
              		$str = "OK";
              	}else{
              		$str = "PERDIDA";
              	}
              }else if($formato=="ROUND4"){
              	$str = round($str,4);
              }else if($formato=="IMPR_TICKET_FACT"){
              	$str = '<img height="25px" style="cursor:pointer" src="imagenes/iconos/paper6.png" class="impr_ticket_fact" lang="'.$str.'" />';
              }else if($formato == "CSD_FILE"){
              	
              	$str = explode('_', $formato);
              	$id = $str[0];
              	$tipo = $str[1];
              	
              }
              
              
                 
            	return $str;
            	
            }    
  
	}
	