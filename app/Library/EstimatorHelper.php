<?php
namespace App\Library;

use App\Storages;
use Carbon\Carbon;
use App\CataPrepay;
use Illuminate\Support\Facades\Log;
use App\CataTerm;

class EstimatorHelper {
	
	public static function estimate($idStorage, $dateStart, $insurance, $extraPercentage, $prepay, $term , $ws = false, $creditCard = false) {
		
		//ws = false ; parametro para indicar que se esta estimando desde un ws de la app
		
		Log::info('Idstorage: '.$idStorage);
		Log::info('$dateStart: '.$dateStart);
		Log::info('$insurance: '.$insurance);
		Log::info('$extraPercentage: '.$extraPercentage);
		Log::info('$prepay: '.$prepay);
		
		$dateStart = Carbon::parse($dateStart)->toDateString();
		
		$lastDayMonth = Carbon::parse($dateStart)->lastOfMonth()->toDateString();
		
		$storage = Storages::find($idStorage);
		
		$daysXMonth = cal_days_in_month(CAL_GREGORIAN, Carbon::parse($dateStart)->format('m'), Carbon::parse($dateStart)->format('y'));
		
		$vat = env('VAT') * 100;
		$insurancePrice = $insurance;
		$daysMonthLeft = Carbon::parse($lastDayMonth)->diffInDays(Carbon::parse($dateStart));
		$daysMonthLeft = $daysMonthLeft + 1; //SUMA EL DIA EN CURSO
		
		Log::info('daysMonthLeft: ' . $daysMonthLeft);
		
		//$extraPercentage ;
		
		$boxPrice = $storage->price;
		$boxPriceXDay = $boxPrice / $daysXMonth;
		
		Log::info('boxPriceXDay: ' . $boxPriceXDay);
		
		$boxInsurance = $insurancePrice;
		$boxInsuranceXDay = $insurancePrice / $daysXMonth;
		
		Log::info('$boxInsuranceXDay: ' . $boxInsuranceXDay);
		
		
		//CURRENT MONTH
		$boxPriceSubtotal = round(($boxPriceXDay * $daysMonthLeft) * (1 + ($extraPercentage) / 100), 2);
		$boxPriceVAT 	= round(($boxPriceSubtotal * $vat / 100),2) ;
		$boxPriceTotal	= round($boxPriceSubtotal + $boxPriceVAT,2);
		
		Log::info('$boxPriceSubtotal: '. $boxPriceSubtotal);
		Log::info('$boxPriceSubtotal Formula: round(('.$boxPriceXDay.' * '.$daysMonthLeft.') * (1 + ('.$extraPercentage.') / 100), 2);' );
		
		Log::info('$boxPriceVAT: '. $boxPriceVAT);
		Log::info('$boxPriceTotal: '. $boxPriceTotal);
		
		$insurancePriceSubtotal = round(round($boxInsuranceXDay * $daysMonthLeft,2),2);
		$insurancePriceVAT = round($insurancePriceSubtotal * $vat / 100,2);
		$insurancePriceTotal = round($insurancePriceSubtotal + $insurancePriceVAT ,2);
		
		$currentMonthSubtotal = round($boxPriceSubtotal + $insurancePriceSubtotal,2);
		$currentMonthVAT = round($boxPriceVAT + $insurancePriceVAT,2);
		$currentMonthTotal = round($boxPriceTotal + $insurancePriceTotal,2);
		
		//NEXT MONTH
		$boxSubtotal = round(($boxPrice) * (1 + $extraPercentage / 100),2);
		$boxVAT = round(($boxSubtotal * $vat / 100),2) ;
		$boxTotal = round($boxSubtotal + $boxVAT,2);

		$insuranceSubtotal = round($boxInsurance,2);
		$insuranceVAT = round($insuranceSubtotal * $vat / 100,2);
		$insuranceTotal = round($insuranceSubtotal + $insuranceVAT,2);
		
		$nextMonthSubtotal = round($boxSubtotal + $insuranceSubtotal,2);
		$nextMonthVAT = round($boxVAT + $insuranceVAT,2);
		$nextMonthTotal = round($boxTotal + $insuranceTotal,2);
		
		
		$deposit = $nextMonthTotal;
		
		$descuento = 0;
		//
		$cataPrepay = CataPrepay::find($prepay);
		$descuento += $cataPrepay->off;
		
		
		$cataTerm = CataTerm::find($term); 
		$descuento += $cataTerm->off;		
	
		Log::info('Descuento: '.$descuento);
		
		$meses = $cataPrepay->months;
		
		if(/*+$prepay != 1*/ $descuento > 0){
			
			//CURRENT MONTH
			$boxPriceSubtotal = round($boxPriceSubtotal * ( 1 - ($descuento/100)),2);
			$boxPriceVAT = round($boxPriceSubtotal * ($vat / 100),2);
			$boxPriceTotal = round($boxPriceSubtotal + $boxPriceVAT,2);
			
			//INSURANCE QUEDA IGUAL...
			
			$currentMonthSubtotal = round($boxPriceSubtotal + $insurancePriceSubtotal,2);
			$currentMonthVAT = round($boxPriceVAT + $insurancePriceVAT,2);
			$currentMonthTotal = round($boxPriceTotal + $insurancePriceTotal,2);
			
			//NEXT MONTH
			//$boxSubtotal = round($boxSubtotal * (1 - $descuento / 100) * $meses ,2);
			$boxSubtotal = round($boxSubtotal * (1 - $descuento / 100) ,2);
			$boxVAT = round($boxSubtotal * $vat / 100 ,2);
			$boxTotal = $boxSubtotal + $boxVAT;
			
			//INSURANCE QUEDA IGUAL..
			
			$nextMonthSubtotal = round($boxSubtotal + $insuranceSubtotal,2);
			$nextMonthVAT = round($boxVAT + $insuranceVAT,2);
			$nextMonthTotal = round($boxTotal + $insuranceTotal,2);
		}
		
		if($prepay > 1){
			Log::info('Prepay....');
			//PREPAY
			
			$firstDayOfMonth = Carbon::parse($dateStart)->firstOfMonth()->format('Y-m-d');
			
			/*
			Log::info('***************');
			Log::info('$firstDayOfMonth: '. $firstDayOfMonth);
			Log::info('$dateStart: '. $dateStart);
			Log::info('***************');
			*/
			
			if($firstDayOfMonth == $dateStart){
				Log::info('Se detecta estimator desde el primer dia del mes');
				
				$boxPriceSubtotal = 0;
				$boxPriceVAT = 0;
				$boxPriceTotal = 0;
				
				$insurancePriceSubtotal = 0;
				$insurancePriceVAT = 0;
				$insurancePriceTotal = 0;
				
				$currentMonthSubtotal = 0;
				$currentMonthVAT = 0;
				$currentMonthTotal = 0;
				
			}
			
			
			$boxSubtotalPrepay = round($boxSubtotal * $meses ,2);
			$boxVATPrepay = round($boxSubtotalPrepay * $vat / 100 ,2);
			$boxTotalPrepay = $boxSubtotalPrepay + $boxVATPrepay;
			
			Log::info('$boxSubtotalPrepay:' . $boxSubtotalPrepay);
			Log::info('$boxVATPrepay:' . $boxVATPrepay);
			Log::info('$boxTotalPrepay:' . $boxTotalPrepay);
			
			//INSURANCE
			$insuranceSubtotalPrepay = round($boxInsurance * $meses,2);
			$insuranceVATPrepay = round($insuranceSubtotalPrepay * $vat / 100,2);
			$insuranceTotalPrepay = round($insuranceSubtotalPrepay + $insuranceVATPrepay,2);

			Log::info('$$insuranceSubtotalPrepay:' . $insuranceSubtotalPrepay);
			Log::info('$$insuranceVATPrepay:' . $insuranceVATPrepay);
			Log::info('$$insuranceTotalPrepay:' . $insuranceTotalPrepay);
			
			$subtotalPrepay = round($boxSubtotalPrepay + $insuranceSubtotalPrepay,2);
			$vatPrepay = round($boxVATPrepay + $insuranceVATPrepay,2);
			$totalPrepay = round($boxTotalPrepay + $insuranceTotalPrepay,2);
			
			Log::info('$$subtotalPrepay:' . $subtotalPrepay);
			Log::info('$$vatPrepay:' . $vatPrepay);
			Log::info('$$totalPrepay:' . $totalPrepay);
			
			Log::info('Finaliza Prepay....');
		}
		else{
			Log::info('Sin Prepay....');
			//PREPAY
			$boxSubtotalPrepay = 0;
			$boxVATPrepay = 0;
			$boxTotalPrepay = 0;
				
			//INSURANCE
			$insuranceSubtotalPrepay = 0;
			$insuranceVATPrepay = 0;
			$insuranceTotalPrepay = 0;
			
			$subtotalPrepay = 0;
			$vatPrepay = 0;
			$totalPrepay = 0;
		}

		Log::info('*******END*****');
		
		Log::info('Return....');
		Log::info('Idstorage: '.$idStorage);
		Log::info('price'					. $boxPrice);
		Log::info('sqm'					. $storage->sqm);
		
		Log::info('currentMonthBoxPrice'.$idStorage.": "	. $boxPriceSubtotal);
		Log::info('currentMonthBoxVAT'.$idStorage.": "	. $boxPriceVAT);
		Log::info('currentMonthBoxTotal'.$idStorage.": "	. $boxPriceTotal);
		
		Log::info('currentMonthInsurancePrice'.$idStorage.": ".  $insurancePriceSubtotal);
		Log::info('currentMonthInsuranceVAT'.$idStorage.": "	.  $insurancePriceVAT);
		Log::info('currentMonthInsuranceTotal'.$idStorage.": ".  $insurancePriceTotal);
		
		Log::info('currentMonthSubtotal'.$idStorage.": ". 	 $currentMonthSubtotal );
		Log::info('currentMonthVAT'	.$idStorage.": ".	 $currentMonthVAT);
		Log::info('currentMonthTotal'	.$idStorage.": ".	 $currentMonthTotal);
		
		Log::info('nextMonthSubtotal'	.$idStorage.": ".	 $nextMonthSubtotal);
		Log::info('nextMonthVAT' 	.$idStorage.": ".		 $nextMonthVAT);
		Log::info('nextMonthTotal'	.$idStorage.": ".	 $nextMonthTotal);
		
		//PREPAY DATA
		Log::info('boxSubtotalPrepay'	.$idStorage.": ".	$boxSubtotalPrepay);
		Log::info('boxVATPrepay'	.$idStorage.": ".		 $boxVATPrepay );
		Log::info('boxTotalPrepay'	.$idStorage.": ".	 $boxTotalPrepay);
		
		Log::info('insuranceSubtotalPrepay'.$idStorage.": ". $insuranceSubtotalPrepay);
		Log::info('insuranceVATPrepay'	.$idStorage.": ".	$insuranceVATPrepay);
		Log::info('insuranceTotalPrepay'	.$idStorage.": ". 	$insuranceTotalPrepay);
		
		Log::info('subtotalPrepay' 	.$idStorage.": "	. $subtotalPrepay);
		Log::info('vatPrepay'		.$idStorage.": "		. $vatPrepay);
		Log::info('totalPrepay'		.$idStorage.": "	. $totalPrepay);
		Log::info('Fin Return....');
		
		/*
		if($ws == 1){
			if($extraPercentage == 0)//CREDIT CARD YES
			{
				$depositTotal = 0;
			}else{
				$depositTotal = $nextMonthTotal;
			}
		}else{
			if($extraPercentage == 0)//CREDIT CARD YES
			{
				$depositTotal = 0;
			}else{
				$depositTotal = $nextMonthTotal;
			}	
		}
		*/
		
		if($creditCard == 2){
			//CREDIT CARD NO
			$depositTotal = $nextMonthTotal;
		}else{
			$depositTotal = 0;
		}
		
		$regPrice = ($boxPrice + $insurance) * 1.20 * 1.23;
		$regPrice = round($regPrice ,2);
		
		return [
				'price'					=> $boxPrice,
				'sqm'					=> $storage->sqm,	
				
				'currentMonthBoxPrice'	=> $boxPriceSubtotal,
				'currentMonthBoxVAT'	=> $boxPriceVAT,
				'currentMonthBoxTotal'	=> $boxPriceTotal,
				
				'currentMonthInsurancePrice'=>  $insurancePriceSubtotal,
				'currentMonthInsuranceVAT'	=>  $insurancePriceVAT,
				'currentMonthInsuranceTotal'=>  $insurancePriceTotal,
				
				'currentMonthSubtotal' 	=> $currentMonthSubtotal ,
				'currentMonthVAT'		=> $currentMonthVAT,
				'currentMonthTotal'		=> $currentMonthTotal,
				
				'nextMonthSubtotal'		=> $nextMonthSubtotal,
				'nextMonthVAT' 			=> $nextMonthVAT,
				'nextMonthTotal'		=> $nextMonthTotal,
				
				//PREPAY DATA
				'boxSubtotalPrepay'		=> $boxSubtotalPrepay,
				'boxVATPrepay'			=> $boxVATPrepay ,
				'boxTotalPrepay'		=> $boxTotalPrepay, 
				
				'insuranceSubtotalPrepay'=> $insuranceSubtotalPrepay,
				'insuranceVATPrepay'	=>	$insuranceVATPrepay, 
				'insuranceTotalPrepay'	=> 	$insuranceTotalPrepay,
				
				'subtotalPrepay' 		=> $subtotalPrepay,
				'vatPrepay'				=> $vatPrepay,
				'totalPrepay'			=> $totalPrepay ,
				
				'depositTotal'			=> $depositTotal,

				'regPrice'				=> $regPrice 
		];
	}

}