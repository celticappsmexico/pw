<?php

namespace App\Library;
use App\Library\PushBots;

class Notificacion {

    public static function Enviar($mensaje, $numero, $user_id) {
        $pb = new PushBots();
        ////ENVIO DE  NOTIFICACIONES PARA TRABAJADOR PRINCIPAL
        $pb->App(env('APPID'), env('APPSECRET'));
        //  $customfields = array("nextActivity" =>"mx.novacloud.novadmin.Menu","posFrag"=>"2","sIco" => "ic_stat_custom_small_icon","lIco" => "http://novaventa.novacloud.link/imgs/logos/logo_nova.png");
        //$customfields = array("nextActivity" => env('ACTIVIDAD'),"lIco" => env('LOGO_NOTIFICACION'));
        $customfields = array("lIco" => env('LOGO_NOTIFICACION'));

        $pb->Payload($customfields);
        // Notification Settings
        $pb->Alert($mensaje.$numero);
        $pb->Platform(array("0","1"));
        $pb->Badge("+1");
        $pb->Sound("");

        $pb->Alias($user_id);
        //Push to Single Device
        $pb->PushOne();
        // Push it !
        $pb->Push();
    }

}

?>