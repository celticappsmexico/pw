<?php
namespace App\Library;

use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Braintree_Gateway;
use Illuminate\Support\Facades\Log;
use App\TransaccionBraintree;
use App\PaymentData;
use App\Billing;

class BraintreeHelper {
	
	public static function registerUserOnBrainTree($nombre, $apellido, $correo, $phone, $user_id) {
		
		$result = Braintree_Customer::create(array(
				'firstName' => $nombre,
				'lastName' => $apellido,
				'email' => $correo,
				'phone' => $phone
		));
		
		Log::info('BraintreeHelper@registerUserOnBrainTree');
		Log::info($result);
		Log::info('Fin...registerUserOnBrainTree');
		
		$tb = new TransaccionBraintree();
		$tb->cliente_id = $user_id;
		$tb->tb_tipo 		= "registerUserOnBrainTree";
		$tb->tb_respuesta 	= $result;
		
		if ($result->success) {
			$tb->tb_estatus = 'SUCCESS';
			$tb->save();
				
			return $result->customer->id;
		} else {
			$tb->tb_estatus = 'ERROR';
			$tb->save();
				
			$errorFound = '';
			foreach ($result->errors->deepAll() as $error) {
				$errorFound .= $error->message . "<br />";
			}
			return $errorFound ;
		}
	}
	
	
	public static function getCardToken($customer_id,$payment_method_nonce,$user_id)
	{
		$card_result = Braintree_CreditCard::create(array(
				'customerId' => $customer_id,
				'paymentMethodNonce' => $payment_method_nonce
		));
		
		Log::info('BraintreeHelper@getCardToken');
		Log::info($card_result);
		Log::info('Fin...getCardToken');
		
		$tb = new TransaccionBraintree();
		$tb->cliente_id 	= $user_id;
		$tb->tb_tipo 		= "getCardToken";
		$tb->tb_respuesta 	= $card_result;
		
		if($card_result->success)
		{
			$tb->tb_estatus = 'SUCCESS';
			$tb->save();
			
			return ['returnCode'	=> 200, 'token' => $card_result->creditCard->token];
		}
		else {
			$tb->tb_estatus = 'ERROR';
			$tb->save();
			
			return ['returnCode'	=> 100, 'card_result'	=> $card_result];
		}
	}
	
	public static function createSuscription($token_card,$planId,$price,$paymentMethodNonce = false,$user_id = false){
		 
		if($paymentMethodNonce == false){
			
			Log::info('Se suscribe con TokenCard');
			
			$subscriptionData = array(
					'paymentMethodToken' => $token_card,
					'planId' => $planId,
					'price'  => $price
			);
		}else{
			Log::info('Se suscribe con PaymentNonce');
			
			$subscriptionData = array(
					'paymentMethodNonce' => $token_card,
					'planId' => $planId,
					'price'  => $price
			);
		}
		 
		$subscription_result = Braintree_Subscription::create($subscriptionData);
		
		Log::info('BraintreeHelper@createSuscription');
		Log::info($subscription_result);
		Log::info('Fin...createSuscription');
		
		$tb = new TransaccionBraintree();
		$tb->cliente_id 	= $user_id;
		$tb->tb_tipo 		= "createSuscription";
		$tb->tb_respuesta 	= $subscription_result;
		
		if($subscription_result->subscription->id){
			$tb->tb_estatus = 'SUCCESS';
			$tb->save();
			
			$nextBillingDate = $subscription_result->subscription->nextBillingDate;
			$nextBillAmount = $subscription_result->subscription->nextBillAmount;
			$status = $subscription_result->subscription->status;
			
			return ['returnCode' => 200  , 'msg' => trans('ws.subscription_success') ,
					'subscription_id'	=> $subscription_result->subscription->id, 
					'paymentMethodToken'	=> $subscription_result->subscription->paymentMethodToken,
					'nextBillingDate'		=> $nextBillingDate,
					'nextBillAmount'		=> $nextBillAmount,
					'status'				=> $status
			];
		}else{
			$tb->tb_estatus = 'ERROR';
			$tb->save();
			
			return ['returnCode' => 100  , 'msg' => trans('ws.subscription_error')
			];
		}
	}
	
	
	
	public static function createTransaction($tokenCard,$customerId,$amount,$order,$paymentMethodNonce = 0,$user_id = false)
	{
		Log::info('BraintreeHelper@createTransaction');
		
		Log::info('Params: tokenCard:'.$tokenCard.',customerId:'.$customerId.',amount:'.$amount.',order:'.$order.',paymentMethodNonce:'.$paymentMethodNonce.',user_id:'.$user_id);
		
		if($paymentMethodNonce == 0){
				
			Log::info('Se hace transaccion con TokenCard');
			
			$result = Braintree_Transaction::sale(
					[
							'paymentMethodToken' => $tokenCard,
							'customerId' => $customerId,
							'amount' => $amount,
							'orderId' => $order
					]
			);				
		}else{
			Log::info('Se hace transaccion con PaymentNonce');
				
			$result = Braintree_Transaction::sale(
					[
							'paymentMethodNonce' => $tokenCard,
							'customerId' => $customerId,
							'amount' => $amount,
							'orderId' => $order
					]
			);
		}
		
		
		Log::info($result);
		//Log::info('Fin...createTransaction');
		
		$tb = new TransaccionBraintree();
		$tb->cliente_id 	= $user_id;
		$tb->tb_tipo 		= "createTransaction";
		$tb->tb_respuesta 	= $result;
		
		//Log::info('Result Braintree Transaction');
		
		
		if ($result->success) {
			
			$tb->tb_estatus = 'SUCCESS';
			$tb->save();
				
			
			$result = Braintree_Transaction::submitForSettlement($result->transaction->id);
			
			$tb2 = new TransaccionBraintree();
			$tb2->cliente_id 	= $user_id;
			$tb2->tb_tipo 		= "submitForSettlement";
			$tb2->tb_respuesta 	= $result;
			
			if ($result->success) {
				$tb2->tb_estatus = 'SUCCESS';
				$tb2->save();
				
				$settledTransaction = $result->transaction;
				//Log::info('SUCCESS: settledTransaction');
				//Log::info($settledTransaction);
				
				
			} else {
				$tb2->tb_estatus = 'ERROR';
				$tb2->save();
				
				Log::info('ERROR: settledTransaction');
				Log::info($result->errors);
				
				
				
			}
			
			Log::info('STATUS: '. $result->transaction->status);
			//Log::info('cardType: '. $result->transaction->creditCardDetails->cardType);
			
			return ['returnCode'	=> 200, 'transaction_id'	=> $result->transaction->id,
					'paymentMethodToken'	=> $result->transaction->creditCardDetails->token,
					'settledTransaction'	=> $settledTransaction,
					'status'	=> $result->transaction->status,
					'cardType'	=> $result->transaction->creditCardDetails->cardType
					
			];
			//return $result->transaction->id;
		} else {
			$tb->tb_estatus = 'ERROR';
			$tb->save();
				
			
			$errorFound = '';
			foreach ($result->errors->deepAll() as $error1) {
				$errorFound .= $error1->message . "<br />";
			}
			Log::info('Error Found:'. $errorFound);
			Log::info('Error Message:'. $result->message);
				
			$errorFound = $result->message;
			
			return ['returnCode'	=> 100 , 'msg'	=> $errorFound , 'result'	=> $result ];
		}
	}
	
	public static function cancelSubscription($suscription_id)
	{
		Log::info('Suscription id: '. $suscription_id);
		 
		$result = Braintree_Subscription::cancel($suscription_id);
		
		$paymentData = PaymentData::where(['pd_suscription_id' => $suscription_id])->first();
		
		$tb = new TransaccionBraintree();
		$tb->cliente_id 	= $paymentData->user_id;
		$tb->tb_tipo 		= "cancelSubscription";
		
		if($result->success == true){
			
			$tb->tb_respuesta 	= $result;
			
			$tb->tb_estatus = 'SUCCESS';
			$tb->save();
	
	
			return ['returnCode' => 200, 'msg'	=> trans('cart.suscription_canceled')];
		}else{
			$tb->tb_respuesta 	= $result;
				
			$tb->tb_estatus = 'ERROR';
			$tb->save();
			
			return ['returnCode' => 100];
		}
	}

	public static function retryCharge($suscription_id, $amount)
	{
		$gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);

		$retryResult = $gateway->subscription()->retryCharge(
			$suscription_id,
			$amount,
			true
		);

		if ($retryResult->success) {
			$result = $gateway->transaction()->submitForSettlement(
				$retryResult->transaction->id
			);
			$result->success;


			//SE INSERTA ABONO.. y se marcan como pagados los billing pendientes...
			$pd = PaymentData::where([
				'pd_suscription_id'	=> $suscription_id
			])->first();

			if(is_null($pd)){
				return ['message' => 'Error: Contact your admin'];
			}

		    $billingUpdate = Billing::where([
					'bi_flag_last' 	=> 1 ,	 
					'user_id' 		=> $pd->user_id, 
					'storage_id' => $pd->storage_id
			])->first();
		    	
	    	if(!is_null($billingUpdate)){
	    		$saldo = $billingUpdate->saldo;
	    		$billingUpdate->bi_flag_last = 0;
	    		$billingUpdate->save();
	    	}
   			else{
   				$saldo = 0;
			   }
			   
			Log::info($retryResult);
			
			/*
			//GENERAMOS EL ABONO DE PREPAY
			$billing = new Billing();
			$billing->user_id = $pd->user_id;
			$billing->storage_id = $pd->storage_id;
			$billing->pay_month = date("Y-m-d");
			$billing->flag_payed = 1;
			$billing->abono = $amount;
			$billing->cargo = 0;
			$billing->saldo = $amount - $saldo;
			//$billing->pdf = $pdfName;
			$billing->reference = $retryResult->transaction->id;
			$billing->bi_transaction_braintree_id	= $retryResult->transaction->id;
			$billing->bi_flag_last	= 1;
			$billing->bi_recibo_fiscal 		= "";
			$billing->bi_year_invoice		= date("y");
			$billing->payment_data_id		= $pd->id;
			$billing->payment_type_id = 2;
	
			$billing->save();	 

			Log::info("Billing ID : ". $billing->id);
			*/
			
			//Actualiza todos los billing pendientes a PAGADOS
			
			Billing::where([
				'bi_braintree_suscription' => $suscription_id,
				'flag_payed'	=> 0,
				'user_id'		=> $pd->user_id, 
				'storage_id'	=> $pd->storage_id
			])->update([
				'flag_payed'	=> 1
			]);
		}
	
		Log::info("Result: ". $retryResult );

		return $retryResult;
	}

	
}