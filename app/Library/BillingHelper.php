<?php
namespace App\Library;

use Illuminate\Support\Facades\Log;
use App\TransaccionBraintree;
use App\PaymentData;
use App\Billing;
use App\Storages;
use Carbon\Carbon;
use App\User;
use App\BillingDetail;
use App\CataPrepay;
use App\InvoiceSequence;
use App\ParagonSequence;
use App\CardUser;
use App\DetOrders;

class BillingHelper {

	public static function insertaCargos($userID,$storageID,$paymentDataID,$flagPaid,
				$paymentTypeID,$tokenCard,$customer,$ws) {

		Log::info('paymentTypeID : '. $paymentTypeID);
		
		$amountCurrent = "";
		$amountPrepay = "";
		 
		$transactionIDCurrent = "";
		$transactionIDPrepay = "";
		 
		$returnCodeCurrent = "";
		$returnCodePrepay = "";
		 
		$errorCurrent = "";
		$errorPrepay = "";
		 
		$statusCurrent = "";
		$cardTypeCurrent = "";
		 
		$statusPrepay = "";
		$cardTypePrepay = "";
		
		//
		$user = User::find($userID);
		$storage = Storages::find($storageID);
		$paymentData = PaymentData::find($paymentDataID);
		//
		
		$prepay = $paymentData->prepay;
		$prepayMonths = CataPrepay::find($prepay);
		 
		//$desc2 = ($prepayMonths->off / 100);--
		$prepayMonths = $prepayMonths->months;
		 
		
		///
		//ACTUALIZA STORAGES
		$items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$paymentData->order_id)->get();
		
		foreach ($items as $key => $item) {
			
			if($item->product_type == 'box'){
		
				$idStorage = $item->product_id;
				$fecha = $item->rent_starts;
			}
		}
		
		$storage->flag_rent = 1;
		$storage->user_id = $userID;
		$storage->rent_start = $fecha;
		$storage->st_meses_prepago = $prepayMonths;
		$storage->save();
		
		//FIN DE ACTUALIZA STORAGES
		
		$paragon = 0;
		 
		if($user->userType_id == 1){
			//FISCAL RECIPT
			$paragon = 1;
		}
		
		$paragon = 0;

		//OBTIENE LA FECHA DE INICIO ... NUEVA LOGICA PARA CReAR TRANSACCIONES Y CARGOS SI ES 1er DIA DE MES
		$firstDayOfMonth = Carbon::parse($storage->rent_start)->firstOfMonth()->format('Y-m-d');
		 
		$flagPrimerDiaMes = 0;
		
		if($firstDayOfMonth == $storage->rent_start && $prepayMonths > 0){
			$flagPrimerDiaMes = 1;
		}
		 
		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userID, 'storage_id' => $storageID])->first();
		if(!is_null($billingUpdate)){
			$saldo = $billingUpdate->saldo;
			$billingUpdate->bi_flag_last = 0;
			$billingUpdate->save();
		}
		else{
			$saldo = 0;
		}
		 
		///
		$paymentData	= PaymentData::find($paymentDataID);
		$ins = $paymentData->insurance;
		$vat = $paymentData->pd_vat * 100;
		 
		Log::info("Primer dia de mes: ". $flagPrimerDiaMes);

		//GENERAMOS EL CARGO
		if($flagPrimerDiaMes == 0){
			 
			//FISCAL CHANGES
			if($paragon == 0)
			{
				$invoiceSecuence = new InvoiceSequence();
				$invoiceSecuence->is_contador =  0;
				$invoiceSecuence->save();
			}
			else{
				$invoiceSecuence = new ParagonSequence();
				$invoiceSecuence->save();
			}
			 
			$amountCurrent = $paymentData->pd_total;
			
			$billing = Billing::create([
					'user_id'			=> $userID,
					'storage_id'		=> $storageID,
					'pay_month'			=> Carbon::now()->format('Y-m-d'),
					'abono'				=> 0,
					'cargo'				=> $paymentData->pd_total,
					'saldo'				=> ($paymentData->pd_total * -1) + $saldo,
					'bi_flag_last'		=> 1,
					'bi_subtotal'		=> $paymentData->pd_subtotal,
					'bi_porcentaje_vat'	=> $vat,
					'bi_total_vat'		=> $paymentData->pd_total_vat,
					'bi_total'			=> $paymentData->pd_total,
					'bi_status_impresion'=>'PENDIENTE PDF',
					'payment_data_id'	=> $paymentDataID,
					'bi_number_invoice'	=> $invoiceSecuence->id,
					'bi_year_invoice'	=> Carbon::now()->format('Y'),
					'bi_batch'			=> 0,
					'bi_paragon'		=> $paragon,
					'bi_start_date'		=> NULL,
					'bi_end_date'		=> NULL,
					'bi_months_invoice'	=> NULL ,
					
			]);			 
			
			$billing->bi_status_impresion	= 'PENDIENTE PDF';
			$billing->save();
			 
			//GENERA BILLING DETAIL
			//BOX
			$billingDetail = BillingDetail::create([
					'billing_id'		=> $billing->id,
					'bd_nombre'			=> "Wynajem powierzchni magazynowej ".$storage->sqm ."m2",
					'bd_codigo'			=> "box ".$storage->alias,
					'bd_numero'			=> "1",
					'bd_valor_neto'		=> $paymentData->pd_box_price,
					'bd_porcentaje_vat' => $vat,
					'bd_total_vat'		=> $paymentData->pd_box_vat,
					'bd_total'			=> $paymentData->pd_box_total,
					'bd_tipo_partida'	=> "BOX"
			]);
			 
			//INSURANCE
			if($ins > 0){
				$billingDetail = BillingDetail::create([
						'billing_id'		=> $billing->id,
						'bd_nombre'			=> "ubezpieczenie pomieszczenia magazynowego",
						'bd_codigo'			=> "ubezpieczenie",
						'bd_numero'			=> "1",
						'bd_valor_neto'		=> $paymentData->pd_insurance_price,
						'bd_porcentaje_vat' => $vat,
						'bd_total_vat'		=> $paymentData->pd_insurance_vat,
						'bd_total'			=> $paymentData->pd_insurance_total,
						'bd_tipo_partida'	=> "INSURANCE"
				]);
			}
			
			//////////----------------
			//INSERTAR PAGO
			if($flagPaid == 1){
			
				$pagado = 1;
				$ref = "";
				$returnCodeCurrent = 200;
				
				//VALIDA METoDO DE PAGO
				if($paymentTypeID == 2){
					//Credit Card - Braintree
					if($prepayMonths > 0){
						//SI HAY PREPAY PROGRAMA SUSCRIPCION
						$fechaProgramada = Carbon::now()->addMonths($prepayMonths)->startOfMonth();
						
					}else{
						//PROGRAMA SUSCRIPCION PARA INICIO DEL PROXIMO MES
						$fechaSuscripcion = Carbon::parse($storage->rent_start)->startOfMonth()->addMonths(1)->toDateString();
						
						$storage->date_subscribe_braintree	= $fechaSuscripcion;
						$storage->save();
						
					}

					//GENERA TRANSACCION
					//Parametro $ws envia payment method nonce
					$trans = BraintreeHelper::createTransaction($tokenCard,$customer,$billing->cargo,$paymentData->order_id,$ws,$userID);
					
					if($trans['returnCode'] == 200){
						$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
						$billing->flag_payed = 1;
						$billing->save();
							
						$transactionIDCurrent = $trans['transaction_id'];
						$ref = $transactionIDCurrent ;
							
						$statusCurrent = $trans['status'];
						$cardTypeCurrent = $trans['cardType'];

						///
						$tokenCard = $trans['paymentMethodToken'];
							
						Log::info('tokenCard: '.$tokenCard);
							
						$cardUser = CardUser::where(['cu_token_card'	=> $tokenCard])->first();
							
						if(is_null($cardUser)){
							$cardUser = new CardUser();
							$cardUser->user_id 			= $userID;
							$cardUser->cu_token_card	= $tokenCard;
							$cardUser->cu_customer_id	= $customer;
							$cardUser->save();
						}
							
						$paymentData->card_user_id 		= $cardUser->id;
						$paymentData->save();
						
					}
					else{
						$returnCodeCurrent = 100;
						$billing->flag_payed = 0;
						$errorCurrent = $trans['msg'];
						
						$pagado = 0;
					}
					
					Log::info('Resultado de Generar Transaccion');
					Log::info($trans);
				}
				
				
				//SI ES PAGO EXITOSO INSERTA ABONO
				if($pagado == 1){
				
					$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userID, 'storage_id' => $storageID])->first();
						
					if(!is_null($billingUpdate)){
						$saldo = $billingUpdate->saldo;
						$billingUpdate->bi_flag_last = 0;
						$billingUpdate->save();
					}else{
						$saldo = 0;
					}
					
					//Actualiza el cargo
					$billing->flag_payed = 1;
					$billing->save();

					//Inserta el abono
					$billingA = new Billing();
					$billingA->user_id 				= $userID;
					$billingA->storage_id 			= $storageID;
					$billingA->pay_month 			= Carbon::now()->format('Y-m-d');
					$billingA->flag_payed 			= 1;
					$billingA->abono 				= $paymentData->pd_total;
					$billingA->cargo 				= 0;
					$billingA->saldo 				= $paymentData->pd_total + $saldo;
					$billingA->reference 			= $ref;
					$billingA->bi_flag_last			= 1;
					$billingA->payment_type_id 		= $paymentTypeID;
					$billingA->payment_data_id		= $paymentData->id;
					$billingA->bi_paragon			= $paragon;
					$billingA->bi_transaction_braintree_id	= $ref;
					$billingA->reference			= $ref;
					$billingA->save();
				}
				//
			}
		}
		
		//Actualiza precio de storage
		$storage->price_per_month = $paymentData->pd_total_next_month;
		$storage->save();

		
		// PREPAGOS
		
		//GENERAMOS EL CARGO DEL PREPAY
		
		if($prepayMonths > 0){
			
			Log::info('OJO: Hay prepay');
				
			if($paragon == 0){
				$invoiceSecuence = new InvoiceSequence();
				$invoiceSecuence->is_contador =  0;
				$invoiceSecuence->save();
			}else{
				$invoiceSecuence = new ParagonSequence();
				$invoiceSecuence->save();
			}
		
			$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userID, 'storage_id' => $storageID])->first();
			if(!is_null($billingUpdate)){
				$saldo = $billingUpdate->saldo;
				$billingUpdate->bi_flag_last = 0;
				$billingUpdate->save();
			}
			else{
				$saldo = 0;
			}
		
			//GENERAMOS EL CARGO DE PREPAY
			$amountPrepay = $paymentData->pd_total_prepay;
		
			///////
			$billing = Billing::create([
					'user_id'			=> $userID,
					'storage_id'		=> $storageID,
					'pay_month'			=> Carbon::now()->format('Y-m-d'),
					'abono'				=> 0,
					'cargo'				=> $paymentData->pd_total_prepay,
					'saldo'				=> ($paymentData->pd_total_prepay * -1) + $saldo,
					'bi_flag_last'		=> 1,
					'bi_subtotal'		=> $paymentData->pd_subtotal_prepay,
					'bi_porcentaje_vat'	=> $vat,
					'bi_total_vat'		=> $paymentData->pd_total_vat_prepay,
					'bi_total'			=> $paymentData->pd_total_prepay,
					'bi_status_impresion'=>'PENDIENTE PDF',
					'payment_data_id'	=> $paymentDataID,
					'bi_number_invoice'	=> $invoiceSecuence->id,
					'bi_year_invoice'	=> Carbon::now()->format('Y'),
					'bi_batch'			=> 0,
					'bi_paragon'		=> $paragon,
					'bi_flag_prepay'	=> 1
			]);
			
			$billing->bi_status_impresion	= 'PENDIENTE PDF';
			$billing->save();
			
			//GENERA BILLING DETAIL
			//BOX
			$billingDetail = BillingDetail::create([
					'billing_id'		=> $billing->id,
					'bd_nombre'			=> "Wynajem powierzchni magazynowej ".$storage->sqm ."m2",
					'bd_codigo'			=> "box ".$storage->alias,
					'bd_numero'			=> $prepayMonths,
					'bd_valor_neto'		=> $paymentData->pd_box_price_prepay,
					'bd_porcentaje_vat' => $vat,
					'bd_total_vat'		=> $paymentData->pd_box_vat_prepay,
					'bd_total'			=> $paymentData->pd_box_total_prepay,
					'bd_tipo_partida'	=> "BOX"
			]);
			
			//INSURANCE
			if($ins > 0){
				$billingDetail = BillingDetail::create([
						'billing_id'		=> $billing->id,
						'bd_nombre'			=> "ubezpieczenie pomieszczenia magazynowego",
						'bd_codigo'			=> "ubezpieczenie",
						'bd_numero'			=> $prepayMonths,
						'bd_valor_neto'		=> $paymentData->pd_insurance_price_prepay,
						'bd_porcentaje_vat' => $vat,
						'bd_total_vat'		=> $paymentData->pd_insurance_vat_prepay,
						'bd_total'			=> $paymentData->pd_insurance_total_prepay,
						'bd_tipo_partida'	=> "INSURANCE"
				]);
			}
			///////
		
			/***/
			//////////----------------
			//INSERTAR PAGO
			if($flagPaid == 1){
					
				$pagado = 1;
				$ref = "";
				$returnCodePrepay = 200;
				
				//VALIDA METoDO DE PAGO
				if($paymentTypeID == 2){
					//Credit Card - Braintree
			
					//GENERA TRANSACCION
					$trans = BraintreeHelper::createTransaction($tokenCard,$customer,$billing->cargo,$paymentData->order_id,$flagPrimerDiaMes,$userID);
						
					if($trans['returnCode'] == 200){
						$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
						$billing->flag_payed = 1;
						$billing->save();
							
						$transactionIDPrepay = $trans['transaction_id'];
						$ref = $transactionIDPrepay	;
						$statusPrepay = $trans['status'];
						$cardTypePrepay = $trans['cardType'];
						
					}
					else{
						$returnCodePrepay = 100;
						$errorPrepay = $trans['msg'];
			
						$pagado = 0;
					}
				}
			
				//SI ES PAGO EXITOSO INSERTA ABONO
				if($pagado == 1){
			
					$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userID, 'storage_id' => $storageID])->first();
			
					if(!is_null($billingUpdate)){
						$saldo = $billingUpdate->saldo;
						$billingUpdate->bi_flag_last = 0;
						$billingUpdate->save();
					}else{
						$saldo = 0;
					}
						
					//Actualiza el cargo
					$billing->flag_payed = 1;
					$billing->save();
			
					//Inserta el abono
					$billingA = new Billing();
					$billingA->user_id 				= $userID;
					$billingA->storage_id 			= $storageID;
					$billingA->pay_month 			= Carbon::now()->format('Y-m-d');
					$billingA->flag_payed 			= 1;
					$billingA->abono 				= $paymentData->pd_total_prepay;
					$billingA->cargo 				= 0;
					$billingA->saldo 				= $paymentData->pd_total_prepay + $saldo;
					$billingA->reference 			= $ref;
					$billingA->bi_flag_last			= 1;
					$billingA->payment_type_id 		= $paymentTypeID;
					$billingA->payment_data_id		= $paymentData->id;
					$billingA->bi_paragon			= $paragon;
					$billingA->bi_transaction_braintree_id	= $ref;
					$billingA->reference			= $ref;
					$billingA->save();
				}
				//
			}
			/***/
			
		}
		
		return [
				'returnCode'	=> 200,
				
				'amountCurrent'	=> $amountCurrent,
				'amountPrepay'	=> $amountPrepay,
					
				'transactionIDCurrent'=> $transactionIDCurrent,
				'transactionIDPrepay' => $transactionIDPrepay,
					
				'returnCodeCurrent'	=> $returnCodeCurrent,
				'returnCodePrepay'	=> $returnCodePrepay,
					
				'errorCurrent'		=> $errorCurrent,
				'errorPrepay' 		=> $errorPrepay,
					
				'statusCurrent' 	=> $statusCurrent,
				'cardTypeCurrent'	=> $cardTypeCurrent,
					
				'statusPrepay'		=> $statusPrepay,
				'cardTypePrepay'	=> $cardTypePrepay
		];
		
		
		
		 
	}
	
	public static function insertaAbonos($user,$storage,$abono,$reference,$paymentType) {
		
		
		$saldoActual = 0;
		
		$billingA = new Billing();
		
		$billingA->user_id 				= $userID;
		$billingA->storage_id 			= $idStorage;
		$billingA->pay_month 			= Carbon::now()->format('Y-m-d');
		$billingA->flag_payed 			= 1;
		$billingA->abono 				= $abono;
		$billingA->cargo 				= 0;
		$billingA->saldo 				= $saldoActual;
		$billingA->reference 			= $ref;
		$billingA->bi_flag_last			= 1;
		$billingA->payment_type_id 		= $paymentType;
		$billingA->payment_data_id		= $paymentData->id;
		$billingA->bi_paragon			= $paragon;
		
		if(isset($trans['transaction_id'])){
		
			if($customer != ""){
				$billingA->bi_transaction_braintree_id	= $trans['transaction_id'];
				$billingA->reference	= $trans['transaction_id'];
			}
		}
		
		$billingA->save();
		
		/*
    		
    		$newBilling->pdf				= "";
    		$newBilling->reference			= $transactionID;
    		$newBilling->bi_flag_last		= 1;
    		$newBilling->payment_data_id 	= $paymentData->id;
    		$newBilling->payment_type_id 	= "2";
    		$newBilling->bi_number_invoice	= $billing->bi_number_invoice;
    		$newBilling->bi_year_invoice	= $billing->bi_year_invoice;
    		$newBilling->bi_webhook			= 1;
    		$newBilling->save();
		 * 
		 * */
	}
	/*
	public static function obtieneSaldoActual($user,$storage) {
		
		$lastBilling = Billing::where(['bi_flag_last' => 1, 'user_id' => $user->id, 'storage_id' => $storage->id])->first();
		
		if(!is_null($lastBilling)){
			$saldo = $lastBilling->saldo;
			$lastBilling->bi_flag_last = 0;
			$lastBilling->save();
		}
		else{
			$saldo = 0;
		}
		
		return $saldo;
	}*/
	
	public static function pagarCargo($id,$payment_type_id,$reference){
		
		$billing = Billing::find($id);
		
		$billing->flag_payed 		= 1;
		$billing->payment_type_id 	= $payment_type_id;
		$billing->reference 		= $reference;
		$billing->save();
		
		return [
				'returnCode'	=> 200,
				'msg'			=> 'Billing pagado'
		];
	}
	
	public static function insertarCargo($user_id,$storage_id,$pay_month,$flag_payed,$abono,$cargo,$saldo,
			$pdf,$reference,$bi_flag_last,$payment_type_id,$bi_subtotal,
			$bi_porcentaje_vat,$bi_total_vat,$bi_total,$bi_recibo_fiscal,$bi_number_invoice,
			$bi_year_invoice,$bi_transaction_braintree_id,$payment_data_id,$bi_batch,
			$bi_flag_prepay,$id_billing_correccion,$bi_razon_correccion,
			$bi_number_invoice_corrected,$bi_contendido_app,
			$bi_paragon,$bi_webhook
	){
	
		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userID, 'storage_id' => $storageID])->first();
		if(!is_null($billingUpdate)){
			$saldo = $billingUpdate->saldo;
			$billingUpdate->bi_flag_last = 0;
			$billingUpdate->save();
		}
		else{
			$saldo = 0;
		}
	
		Billing::create([
				'user_id'			=> $userID,
				'storage_id'		=> $storageID,
				'pay_month'			=> Carbon::now()->format('Y-m-d'),
				'abono'				=> 0,
				'cargo'				=> $paymentData->pd_total,
				'saldo'				=> ($paymentData->pd_total * -1) + $saldo,
				'bi_flag_last'		=> 1,
				'bi_subtotal'		=> $paymentData->pd_subtotal,
				'bi_porcentaje_vat'	=> $vat,
				'bi_total_vat'		=> $paymentData->pd_total_vat,
				'bi_total'			=> $paymentData->pd_total,
				'bi_status_impresion'=>'PENDIENTE PDF',
				'payment_data_id'	=> $paymentDataID,
				'bi_number_invoice'	=> $invoiceSecuence->id,
				'bi_year_invoice'	=> Carbon::now()->format('Y'),
				'bi_batch'			=> 0,
				'bi_paragon'		=> $paragon
		]);
	
		//GENERA BILLING DETAIL
		//BOX
		$billingDetail = BillingDetail::create([
				'billing_id'		=> $billing->id,
				'bd_nombre'			=> "Wynajem powierzchni magazynowej ".$storage->sqm ."m2",
				'bd_codigo'			=> "box ".$storage->alias,
				'bd_numero'			=> "1",
				'bd_valor_neto'		=> $paymentData->pd_box_price,
				'bd_porcentaje_vat' => $vat,
				'bd_total_vat'		=> $paymentData->pd_box_vat,
				'bd_total'			=> $paymentData->pd_box_total,
				'bd_tipo_partida'	=> "BOX"
		]);
	
		//INSURANCE
		if($ins > 0){
			$billingDetail = BillingDetail::create([
					'billing_id'		=> $billing->id,
					'bd_nombre'			=> "ubezpieczenie pomieszczenia magazynowego",
					'bd_codigo'			=> "ubezpieczenie",
					'bd_numero'			=> "1",
					'bd_valor_neto'		=> $paymentData->pd_insurance_price,
					'bd_porcentaje_vat' => $vat,
					'bd_total_vat'		=> $paymentData->pd_insurance_vat,
					'bd_total'			=> $paymentData->pd_insurance_total,
					'bd_tipo_partida'	=> "INSURANCE"
			]);
		}
	
	
	}
	
}