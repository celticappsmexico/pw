<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'warehouse';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [	'name',
							'lat',
							'lng',
							'country_id',
							'address',
							'Active'
						];


	public function Country(){
    return $this->belongsTo(Countries::class,'country_id');
  }

  public function Levels(){
    return $this->hasMany(Levels::class, 'warehouse_id', 'id')->where('active', 1)/*->groupBy('warehouse_id')*/;
  }
}
