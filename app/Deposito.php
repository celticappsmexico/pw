<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposito extends Model
{
    //
    protected $table = 'depositos';
	
	protected $fillable = [
			'storage_id',
    		'user_id',
    		'de_monto_deposito',
			'de_regresado',
			'de_pagado',
			'payment_type_id',
			'de_notas',
			'de_transaction_id_braintree'
	];
	
	public function storage() {
		return $this->belongsTo(Storages::class);
	}
	
	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function PaymentType(){
		return $this->belongsTo(CataPaymentTypes::class,'payment_type_id','id');
	}
}
