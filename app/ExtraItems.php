<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraItems extends Model
{
    protected $table = 'cata_extra_items';
    protected $primarykey = 'id';
    protected $fillable = [
        'id', 'nombre', 'img_path', 'price'
    ];
}
