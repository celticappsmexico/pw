<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storages extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'storages';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

     protected $fillable = [
	        'alias',
	        'text_posx',
	        'text_posy',
	        'size',
	        'rect_posx',
	        'rect_posy',
	        'rect_width',
	        'rect_height',
	        'rect_background',
	        'class',
	        'active',
	        'level_id',
	        'user_id',
	        'sqm',
	        'price',
	        'final_price',
	        'rent_start',
	        'rent_end',
	        'comments',
	        'flag_rent',
         	'price_per_month',
     		'st_meses_prepago',
     		'date_subscribe_braintree',
			 'st_es_contenedor',
			 'st_mostrar_app'
     ];

     public function StorageBilling(){
         return $this->hasMany(Billing::class,'storage_id')->orderBy('id', 'desc');
     }

     public function StorageLastBilling(){
         return $this->hasMany(Billing::class,'storage_id')->where('flag_payed','!=',0)->orderBy('id', 'desc');
     }

     public function StoragePaymentData(){
         return $this->hasMany(PaymentData::class,'storage_id')->orderBy('id', 'desc');
     }

     public function StorageOwner(){
         return $this->belongsTo(User::class,'user_id');
     }

     public function level() {
         return $this->belongsTo(Levels::class, 'level_id', 'id')->where('active', 1);
     }
}
