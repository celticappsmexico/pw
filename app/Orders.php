<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Orders extends Model
{
  /**
 * The database table used by the model.
 *
 * @var string
 */
protected $table = 'orders';

/**
 * The attributes that are mass assignable.
 *
 * @var array
 */

protected $fillable = [	'user_id',
                        'active'
                      ];


  public function Client(){
    return $this->belongsTo(User::class,'user_id');
  }

  public function OrderDeatil(){
      return $this->hasMany(DetOrders::class,'order_id');
  }
}
