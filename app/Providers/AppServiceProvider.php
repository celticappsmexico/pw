<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Role;
use App\UserType;
use App\Countries;
use App\CataSecciones;
USE App\Warehouse;
USE App\Articulos;
USE App\User;
USE App\ExtraItems;
use App\CataPrepay;
use App\CataTerm;
use App\CataPaymentTypes;
use App\ConfigSystem;
use App\Reports;
use App\Storages;
use App\Billing;
use App\cata_months;
use App\Library\utilMesesGrafica;
use Carbon\Carbon;
use Log;
use App\BillingDetail;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.modals.new_client', function($view) {
          $roles = Role::all()->sortBy('id');
          $userTypes = UserType::all();
          $view->with('roles', $roles);
          $view->with('userTypes', $userTypes);
        });

        view()->composer('partials.modals.edit_client', function($view) {
          $roles = Role::all()->sortBy('id');
          $userTypes = UserType::all();
          $view->with('roles', $roles);
          $view->with('userTypes', $userTypes);
        });

        view()->composer('partials.modals.new_client', function($view) {
          $countries = Countries::all()->sortBy('name');
          $view->with('countries', $countries);
        });


        // modal add_storage
        view()->composer('partials.modals.add_storage_modal', function($view) {
          $buildings = Warehouse::with('Levels')->where('active', 1)->get();
          $view->with('buildings', $buildings);
        });

        view()->composer('partials.modals.edit_client', function($view) {
          $countries = Countries::all()->sortBy('name');
          $view->with('countries', $countries);
        });

        view()->composer('partials.modals.new_building', function($view) {
          $countries = Countries::all()->sortBy('name');
          $view->with('countries', $countries);
        });

        view()->composer('partials.modals.edit_building', function($view) {
          $countries = Countries::all()->sortBy('name');
          $view->with('countries', $countries);
        });

        view()->composer('partials.layout.sidebar_pw', function($view) {
          $build = Warehouse::with('Levels')->where('active', 1)->get();
          //dd($build);
          $view->with('buildings', $build);
        });

        view()->composer('size-estimator.size-estimator', function($view) {
          $secciones = CataSecciones::all();
          $totalSecciones = CataSecciones::count();
          $articulos = Articulos::all();
          $view->with('articulos', $articulos);
          $view->with('secciones', $secciones);
          $view->with('totalSecciones', $totalSecciones);
        });

        view()->composer('partials.modals.extra_items_modal', function($view) {
            $extraitems = ExtraItems::all();
            $view->with('extraItems', $extraitems);
        });

        view()->composer('partials.modals.extra_items_bk_modal', function($view) {
            $extraitems = ExtraItems::all();
            $view->with('extraItems', $extraitems);
        });

        view()->composer('size-estimator.size-estimator', function($view) {
            $cataterm = CataTerm::all();
            $view->with('cataterm', $cataterm);
            $cataprepay = CataPrepay::all();
            $view->with('cataprepay', $cataprepay);
        });

        view()->composer('partials.modals.add_storage_modal', function($view) {
            $cataterm = CataTerm::all();
            $view->with('cataterm', $cataterm);
            $cataprepay = CataPrepay::all();
            $view->with('cataprepay', $cataprepay);
        });

        view()->composer('partials.modals.payment_modal', function($view) {
            $payment_method = CataPaymentTypes::all();
            $view->with('catapaymenttype', $payment_method);
        });

        view()->composer('pw.cart', function($view) {
            $data = ConfigSystem::all();
            $ins = $data[0]->value;
            $vat = $data[1]->value;
            $view->with('insDb', $ins)->with('vatDb',$vat);
        });

        view()->composer('home', function($view) {
            $traMonth = trans('billing_reports.month');
            
            $January = trans('billing_reports.January');
            $February = trans('billing_reports.February');
            $March = trans('billing_reports.March');
            $April = trans('billing_reports.April');
            $May = trans('billing_reports.May');
            $June = trans('billing_reports.June');
            $July = trans('billing_reports.July');
            $August = trans('billing_reports.August');
            $September = trans('billing_reports.September');
            $October = trans('billing_reports.October');
            $November = trans('billing_reports.November');
            $December = trans('billing_reports.December');
            
            
            $date1 = Carbon::now()->addMonths(-11);
            $date2 = Carbon::now()->addMonths(-10);
            $date3 = Carbon::now()->addMonths(-9);
            $date4 = Carbon::now()->addMonths(-8);
            $date5 = Carbon::now()->addMonths(-7);
            $date6 = Carbon::now()->addMonths(-6);
            $date7 = Carbon::now()->addMonths(-5);
            $date8 = Carbon::now()->addMonths(-4);
            $date9 = Carbon::now()->addMonths(-3);
            $date10 = Carbon::now()->addMonths(-2);
            $date11 = Carbon::now()->addMonths(-1);
            $date12 = Carbon::now();
            
            $month1Name = Carbon::parse($date1)->format('F');
            $month2Name = Carbon::parse($date2)->format('F');
            $month3Name = Carbon::parse($date3)->format('F');
            $month4Name = Carbon::parse($date4)->format('F');
            $month5Name = Carbon::parse($date5)->format('F');
            $month6Name = Carbon::parse($date6)->format('F');
            $month7Name = Carbon::parse($date7)->format('F');
            $month8Name = Carbon::parse($date8)->format('F');
            $month9Name = Carbon::parse($date9)->format('F');
            $month10Name = Carbon::parse($date10)->format('F');
            $month11Name = Carbon::parse($date11)->format('F');
            $month12Name = Carbon::parse($date12)->format('F');
            
            $month1 = Carbon::parse($date1)->format('m');
            $month2 = Carbon::parse($date2)->format('m');
            $month3 = Carbon::parse($date3)->format('m');
            $month4 = Carbon::parse($date4)->format('m');
            $month5 = Carbon::parse($date5)->format('m');
            $month6 = Carbon::parse($date6)->format('m');
            $month7 = Carbon::parse($date7)->format('m');
            $month8 = Carbon::parse($date8)->format('m');
            $month9 = Carbon::parse($date9)->format('m');
            $month10 = Carbon::parse($date10)->format('m');
            $month11 = Carbon::parse($date11)->format('m');
            $month12 = Carbon::parse($date12)->format('m');
            
            $year1 = Carbon::parse($date1)->format('Y');
            $year2 = Carbon::parse($date2)->format('Y');
            $year3 = Carbon::parse($date3)->format('Y');
            $year4 = Carbon::parse($date4)->format('Y');
            $year5 = Carbon::parse($date5)->format('Y');
            $year6 = Carbon::parse($date6)->format('Y');
            $year7 = Carbon::parse($date7)->format('Y');
            $year8 = Carbon::parse($date8)->format('Y');
            $year9 = Carbon::parse($date9)->format('Y');
            $year10 = Carbon::parse($date10)->format('Y');
            $year11 = Carbon::parse($date11)->format('Y');
            $year12 = Carbon::parse($date12)->format('Y');
            
            

            /*
            \Illuminate\Support\Facades\Log::info('Date1: '. $year1 );
            \Illuminate\Support\Facades\Log::info('Date2: '. $year2 );
            \Illuminate\Support\Facades\Log::info('Date3: '. $year3 );
            \Illuminate\Support\Facades\Log::info('Date4: '. $year4 );
            \Illuminate\Support\Facades\Log::info('Date5: '. $year5 );
            \Illuminate\Support\Facades\Log::info('Date6: '. $year6 );
            \Illuminate\Support\Facades\Log::info('Date7: '. $year7 );
            \Illuminate\Support\Facades\Log::info('Date8: '. $year8 );
            \Illuminate\Support\Facades\Log::info('Date9: '. $year9 );
            \Illuminate\Support\Facades\Log::info('Date10: '. $year10 );
            \Illuminate\Support\Facades\Log::info('Date11: '. $year11 );
            \Illuminate\Support\Facades\Log::info('Date12: '. $year12 );
            */
            $now = Carbon::now();
            $data = Reports::orderBy('id','desc')->take(6)->get();

            //** Start Row 2 **//
            $pastMonth = Carbon::now()->firstOfMonth()->subMonth()->month;
            $currentMonth = $now->month;
            $currentYear = $now->year;
            $onRent = Storages::where('flag_rent','=',1)->get();
            $currentRent = count($onRent);
            $allStorages = Storages::all();
            $allBoxes = Storages::where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`rent_start`)'), '=', $currentMonth)
            ->count();


            //Log::info("Current Month: " . $currentMonth);
            //Log::info("Past Month: " . $pastMonth);


            $boxesRented = Storages::where('flag_rent','=',1)
            ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`rent_start`)'), '=', $currentMonth)
            ->count();

            //Log::info("Boxes Rented: ". $boxesRented);

            $boxesRentedPastMonth = Storages::where('flag_rent','=',1)
            ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`rent_start`)'), '=', $pastMonth)
            ->count();

            //Log::info("Boxes Rented Past Month: ". $boxesRentedPastMonth);

            if ($boxesRentedPastMonth > 0) {
                $porcentajeBoxesRented = (($boxesRented/$boxesRentedPastMonth)-1)*100;
            }else {
                $porcentajeBoxesRented = 0;
            }
/*
            $totalBilledMonth = Billing::select(\DB::raw('SUM(`cargo`) as billed'))
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`pay_month`)'), '=', $currentMonth)
            ->first();
            */
            $bills = Billing::whereMonth('pay_month','=',$currentMonth)
            					->whereYear('pay_month','=',$currentYear)
            					->with('billingDetail')
            					->whereHas('billingDetail', function($q) {
						    		$q->where('bd_tipo_partida', 'BOX');
								})->get();
								
			$totalBilledMonth = BillingDetail::where('bd_tipo_partida', 'BOX')
							->with('billing')
							->whereHas('billing', function($q) use($currentMonth,$currentYear) {
						    		$q->whereMonth('pay_month','=',$currentMonth);
                                    $q->whereYear('pay_month','=',$currentYear);
                                    $q->whereNull('id_billing_correccion');
								})->sum('bd_valor_neto');

			$totalBilledPastMonth = BillingDetail::where('bd_tipo_partida', 'BOX')
								->with('billing')
								->whereHas('billing', function($q) use($pastMonth,$currentYear) {
						    		$q->whereMonth('pay_month','=',$pastMonth);
                                    $q->whereYear('pay_month','=',$currentYear);
                                    $q->whereNull('id_billing_correccion');
								})->sum('bd_valor_neto');
								
			/*
            $totalBilledPastMonth = Billing::select(\DB::raw('SUM(`cargo`) as billed'))
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`pay_month`)'), '=', $pastMonth)
            ->first();
            */
            /*if ($totalBilledMonth->billed == null) {
                $totalBilledMonth->billed = 0;
            }elseif ($totalBilledPastMonth->billed == null) {
                $totalBilledPastMonth->billed = 0;
            }*/

            if ($totalBilledPastMonth > 0) {
                $porcentajeBilled = (($totalBilledMonth/$totalBilledPastMonth)-1)*100;
            }else {
                $porcentajeBilled = 0;
            }

            $totalPayedMonth =  Billing::select(\DB::raw('SUM(`abono`) as payed'))
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->where(\DB::raw('MONTH(`pay_month`)'), '=', $currentMonth)
            ->whereNull('bi_number_invoice_corrected')
            ->first();

            if ($totalPayedMonth->payed == null) {
                $totalPayedMonth->payed = 0;
            }

            if ($totalBilledMonth > 0) {
                $paidInvoices = ($totalPayedMonth->payed/$totalBilledMonth)*100;
            }else {
                $paidInvoices = 0;
            }

            $warehouses = Warehouse::with(['Levels.Storages'])->get();
            foreach ($warehouses as $warehouse) {
                foreach ($warehouse->Levels as $key => $Level) {
                    $ocupados = 0;
                    $totalStorage = 0;
                    $ocupados = $Level->Storages()->where('flag_rent', 1)->count();
                    $totalStorage = $Level->Storages()->count();
                    $Level->ocupados=$ocupados;
                    $Level->totalStorage=$totalStorage;
                    $Level->Promedio=($ocupados/$totalStorage)*100;
                }
            }
            //** End Row 2 **//

            //** Start Row 3 Graph Boxes **//
            $months = cata_months::get();
            $rented_boxes = [];
            /*foreach ($months as $key => $month) {
                $boxes_rented = Storages::where(\DB::raw('monthname(`rent_start`)'), '=', $month->month)
                ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
                ->where('flag_rent', '=', 1)
                ->count();
                $rented_boxes[$key] = $boxes_rented;
            }*/
            
            //Totales Tabla STorages
            $totalMonth1_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month1)
            ->whereYear('rent_start','=',$year1)->count(); 
            
            $totalMonth2_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month2)
            ->whereYear('rent_start','=',$year2)->count();
            
            $totalMonth3_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month3)
            ->whereYear('rent_start','=',$year3)->count();
            
            $totalMonth4_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month4)
            ->whereYear('rent_start','=',$year4)->count();
            
            $totalMonth5_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month5)
            ->whereYear('rent_start','=',$year5)->count();
            
            $totalMonth6_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month6)
            ->whereYear('rent_start','=',$year6)->count();
            
            $totalMonth7_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month7)
            ->whereYear('rent_start','=',$year7)->count();
            
            $totalMonth8_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month8)
            ->whereYear('rent_start','=',$year8)->count();
            
            $totalMonth9_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month9)
            ->whereYear('rent_start','=',$year9)->count();
            
            $totalMonth10_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month10)
            ->whereYear('rent_start','=',$year10)->count();
            
            $totalMonth11_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month11)
            ->whereYear('rent_start','=',$year11)->count();
            
            $totalMonth12_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month12)
            ->whereYear('rent_start','=',$year12)->count();
            
            $rented_boxes[0] = $totalMonth1_ST;
            $rented_boxes[1] = $totalMonth2_ST;
            $rented_boxes[2] = $totalMonth3_ST;
            $rented_boxes[3] = $totalMonth4_ST;
            $rented_boxes[4] = $totalMonth5_ST;
            $rented_boxes[5] = $totalMonth6_ST;
            $rented_boxes[6] = $totalMonth7_ST;
            $rented_boxes[7] = $totalMonth8_ST;
            $rented_boxes[8] = $totalMonth9_ST;
            $rented_boxes[9] = $totalMonth10_ST;
            $rented_boxes[10] = $totalMonth11_ST;
            $rented_boxes[11] = $totalMonth12_ST;
            
            $rented_boxes = implode(",",$rented_boxes);
            
            //dd($months_last_12);
            //dd($rented_boxes);
            
            $new_boxes = [];
            /*
            foreach ($months as $key => $month) {
                $new = Storages::where(\DB::raw('monthname(`created_at`)'), '=', $month->month)
                ->where(\DB::raw('YEAR(`created_at`)'), '=', $currentYear)
                ->where('active', '=', 1)
                ->count();
                $new_boxes[$key] = $new;
            }*/
            $new_boxes[0] = Storages::where('active',1)->whereMonth('created_at','=',$month1)->whereYear('created_at','=',$year1)->count();
            $new_boxes[1] = Storages::where('active',1)->whereMonth('created_at','=',$month2)->whereYear('created_at','=',$year2)->count();
            $new_boxes[2] = Storages::where('active',1)->whereMonth('created_at','=',$month3)->whereYear('created_at','=',$year3)->count();
            $new_boxes[3] = Storages::where('active',1)->whereMonth('created_at','=',$month4)->whereYear('created_at','=',$year4)->count();
            $new_boxes[4] = Storages::where('active',1)->whereMonth('created_at','=',$month5)->whereYear('created_at','=',$year5)->count();
            $new_boxes[5] = Storages::where('active',1)->whereMonth('created_at','=',$month6)->whereYear('created_at','=',$year6)->count();
            $new_boxes[6] = Storages::where('active',1)->whereMonth('created_at','=',$month7)->whereYear('created_at','=',$year7)->count();
            $new_boxes[7] = Storages::where('active',1)->whereMonth('created_at','=',$month8)->whereYear('created_at','=',$year8)->count();
            $new_boxes[8] = Storages::where('active',1)->whereMonth('created_at','=',$month9)->whereYear('created_at','=',$year9)->count();
            $new_boxes[9] = Storages::where('active',1)->whereMonth('created_at','=',$month10)->whereYear('created_at','=',$year10)->count();
            $new_boxes[10]= Storages::where('active',1)->whereMonth('created_at','=',$month11)->whereYear('created_at','=',$year11)->count();
            $new_boxes[11]= Storages::where('active',1)->whereMonth('created_at','=',$month12)->whereYear('created_at','=',$year12)->count();
            
            $new_boxes = implode(",",$new_boxes);
            //** End Row 3 Graph Boxes **//
            
            
            //----------------
            //** Start Row 3.1 Graph Boxes **//
            $rented_sqm = [];
           
            //Totales Tabla STorages
            $totalSqmMonth1_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month1)
            ->whereYear('rent_start','=',$year1)->sum('sqm');
            
            $totalSqmMonth2_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month2)
            ->whereYear('rent_start','=',$year2)->sum('sqm');
            
            $totalSqmMonth3_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month3)
            ->whereYear('rent_start','=',$year3)->sum('sqm');
            
            $totalSqmMonth4_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month4)
            ->whereYear('rent_start','=',$year4)->sum('sqm');
            
            $totalSqmMonth5_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month5)
            ->whereYear('rent_start','=',$year5)->sum('sqm');
            
            $totalSqmMonth6_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month6)
            ->whereYear('rent_start','=',$year6)->sum('sqm');
            
            $totalSqmMonth7_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month7)
            ->whereYear('rent_start','=',$year7)->sum('sqm');
            
            $totalSqmMonth8_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month8)
            ->whereYear('rent_start','=',$year8)->sum('sqm');
            
            $totalSqmMonth9_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month9)
            ->whereYear('rent_start','=',$year9)->sum('sqm');
            
            $totalSqmMonth10_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month10)
            ->whereYear('rent_start','=',$year10)->sum('sqm');
            
            $totalSqmMonth11_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month11)
            ->whereYear('rent_start','=',$year11)->sum('sqm');
            
            $totalSqmMonth12_ST = Storages::where([
            		'flag_rent'	=> '1'
            ])->whereMonth('rent_start','=',$month12)
            ->whereYear('rent_start','=',$year12)->sum('sqm');
            
            $rented_sqm[0] = $totalSqmMonth1_ST;
            $rented_sqm[1] = $totalSqmMonth2_ST;
            $rented_sqm[2] = $totalSqmMonth3_ST;
            $rented_sqm[3] = $totalSqmMonth4_ST;
            $rented_sqm[4] = $totalSqmMonth5_ST;
            $rented_sqm[5] = $totalSqmMonth6_ST;
            $rented_sqm[6] = $totalSqmMonth7_ST;
            $rented_sqm[7] = $totalSqmMonth8_ST;
            $rented_sqm[8] = $totalSqmMonth9_ST;
            $rented_sqm[9] = $totalSqmMonth10_ST;
            $rented_sqm[10] = $totalSqmMonth11_ST;
            $rented_sqm[11] = $totalSqmMonth12_ST;
            
            $rented_sqm = implode(",",$rented_sqm);
            
            //dd($months_last_12);
            //dd($rented_boxes);
            
            $new_boxes_sqm = [];
            
            $newBoxSqmMonth1 = Storages::where('active',1)->whereMonth('created_at','=',$month1)->whereYear('created_at','=',$year1)->sum('sqm');
            $newBoxSqmMonth2 = Storages::where('active',1)->whereMonth('created_at','=',$month2)->whereYear('created_at','=',$year2)->sum('sqm');
            $newBoxSqmMonth3 = Storages::where('active',1)->whereMonth('created_at','=',$month3)->whereYear('created_at','=',$year3)->sum('sqm');
            $newBoxSqmMonth4 = Storages::where('active',1)->whereMonth('created_at','=',$month4)->whereYear('created_at','=',$year4)->sum('sqm');
            $newBoxSqmMonth5 = Storages::where('active',1)->whereMonth('created_at','=',$month5)->whereYear('created_at','=',$year5)->sum('sqm');
            $newBoxSqmMonth6 = Storages::where('active',1)->whereMonth('created_at','=',$month6)->whereYear('created_at','=',$year6)->sum('sqm');
            $newBoxSqmMonth7 = Storages::where('active',1)->whereMonth('created_at','=',$month7)->whereYear('created_at','=',$year7)->sum('sqm');
            $newBoxSqmMonth8 = Storages::where('active',1)->whereMonth('created_at','=',$month8)->whereYear('created_at','=',$year8)->sum('sqm');
            $newBoxSqmMonth9 = Storages::where('active',1)->whereMonth('created_at','=',$month9)->whereYear('created_at','=',$year9)->sum('sqm');
            $newBoxSqmMonth10 = Storages::where('active',1)->whereMonth('created_at','=',$month10)->whereYear('created_at','=',$year10)->sum('sqm');
            $newBoxSqmMonth11 = Storages::where('active',1)->whereMonth('created_at','=',$month11)->whereYear('created_at','=',$year11)->sum('sqm');
            $newBoxSqmMonth12 = Storages::where('active',1)->whereMonth('created_at','=',$month12)->whereYear('created_at','=',$year12)->sum('sqm');
            
            $new_boxes_sqm[0] =  ($newBoxSqmMonth1) ? $newBoxSqmMonth1 : 0;
            $new_boxes_sqm[1] =  ($newBoxSqmMonth2) ? $newBoxSqmMonth2 : 0;
            $new_boxes_sqm[2] =  ($newBoxSqmMonth3) ? $newBoxSqmMonth3 : 0;
            $new_boxes_sqm[3] =  ($newBoxSqmMonth4) ? $newBoxSqmMonth4 : 0;
            $new_boxes_sqm[4] = ($newBoxSqmMonth5) ? $newBoxSqmMonth5 : 0;
            $new_boxes_sqm[5] =  ($newBoxSqmMonth6) ? $newBoxSqmMonth6 : 0;
            $new_boxes_sqm[6] =  ($newBoxSqmMonth7) ? $newBoxSqmMonth7 : 0;
            $new_boxes_sqm[7] =  ($newBoxSqmMonth8) ? $newBoxSqmMonth8 : 0;
            $new_boxes_sqm[8] =  ($newBoxSqmMonth9) ? $newBoxSqmMonth9 : 0;
            $new_boxes_sqm[9] =  ($newBoxSqmMonth10) ? $newBoxSqmMonth10 : 0;
            $new_boxes_sqm[10]=  ($newBoxSqmMonth11) ? $newBoxSqmMonth11 : 0;
            $new_boxes_sqm[11]=  ($newBoxSqmMonth12) ? $newBoxSqmMonth12 : 0;
            
            $new_boxes_sqm = implode(",",$new_boxes_sqm);
            
            Log::info($new_boxes_sqm);
            //** End Row 3.1 Graph Boxes **//
            //----------------

            //** Start Row 4 Graph Income Reports **//
            $billed_current_year = [];
            foreach ($months as $key => $month) {
                $billed = Billing::select(\DB::raw('ROUND(SUM(cargo), 2) as billed'))
                ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
                ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
                ->first();
                if ($billed->billed == null) {
                    $billed->billed = "0.00";
                }
                $billed_current_year[$key] = $billed->billed;
            }
            $billed_current_year = implode(",", $billed_current_year);

            $payed_current_year = [];
            foreach ($months as $key => $month) {
                $payed = Billing::select(\DB::raw('ROUND(SUM(abono), 2) as payed'))
                ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
                ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
                ->where('flag_payed', '=', 1)
                ->first();
                if ($payed->payed == null) {
                    $payed->payed = "0.00";
                }
                $payed_current_year[$key] = $payed->payed;
            }
            $payed_current_year = implode(",", $payed_current_year);

            $not_paid_current_year = [];
            foreach ($months as $key => $month) {
                $not_payed = Billing::select(\DB::raw('ROUND((sum(cargo)-sum(abono)), 2) as not_payed'))
                ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
                ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
                ->first();
                if ($not_payed->not_payed == null or $not_payed->not_payed < 0) {
                    $not_payed->not_payed = "0.00";
                }
                $not_paid_current_year[$key] = $not_payed->not_payed;
            }
            $not_paid_current_year = implode(",", $not_paid_current_year);
            
			            
            $netoMonth1 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month1){
                $q->whereMonth('pay_month', '=', $month1);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth2 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month2){
                $q->whereMonth('pay_month', '=', $month2);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth3 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month3){
                $q->whereMonth('pay_month', '=', $month3);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth4 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month4){
                $q->whereMonth('pay_month', '=', $month4);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth5 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month5){
                $q->whereMonth('pay_month', '=', $month5);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth6 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month6){
                $q->whereMonth('pay_month', '=', $month6);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth7 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month7){
                $q->whereMonth('pay_month', '=', $month7);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth8 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month8){
                $q->whereMonth('pay_month', '=', $month8);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth9 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month9){
                $q->whereMonth('pay_month', '=', $month9);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth10 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month10){
                $q->whereMonth('pay_month', '=', $month10);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth11 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month11){
                $q->whereMonth('pay_month', '=', $month11);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoMonth12 = BillingDetail::where(['bd_tipo_partida' => 'BOX'])
            ->whereHas('billing', function($q) use($month12){
                $q->whereMonth('pay_month', '=', $month12);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            ///INSURANCE
            $netoInsMonth1 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month1){
                $q->whereMonth('pay_month', '=', $month1);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth2 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month2){
                $q->whereMonth('pay_month', '=', $month2);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth3 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month3){
                $q->whereMonth('pay_month', '=', $month3);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth4 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month4){
                $q->whereMonth('pay_month', '=', $month4);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth5 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month5){
                $q->whereMonth('pay_month', '=', $month5);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth6 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month6){
                $q->whereMonth('pay_month', '=', $month6);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth7 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month7){
                $q->whereMonth('pay_month', '=', $month7);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth8 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month8){
                $q->whereMonth('pay_month', '=', $month8);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth9 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month9){
                $q->whereMonth('pay_month', '=', $month9);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth10 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month10){
                $q->whereMonth('pay_month', '=', $month10);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth11 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month11){
                $q->whereMonth('pay_month', '=', $month11);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            
            $netoInsMonth12 = BillingDetail::where(['bd_tipo_partida' => 'INSURANCE'])
            ->whereHas('billing', function($q) use($month12){
                $q->whereMonth('pay_month', '=', $month12);
                $q->whereNull('id_billing_correccion');
            })->sum('bd_valor_neto');
            ///
            
            $netoMonth1 =  ($netoMonth1) ? $netoMonth1 : 0;
            $netoMonth2 =  ($netoMonth2) ? $netoMonth2 : 0;
            
            $netoInsMonth1 =  ($netoMonth1) ? $netoMonth1 : 0;
            $netoInsMonth2 =  ($netoMonth2) ? $netoMonth2 : 0;
            
			$netos_box = [];
            $netos_box[$month1] = $netoMonth1;
            $netos_box[$month2] = $netoMonth2;
            $netos_box[$month3] = $netoMonth3;
            $netos_box[$month4] = $netoMonth4;
            $netos_box[$month5] = $netoMonth5;
            $netos_box[$month6] = $netoMonth6;
            $netos_box[$month7] = $netoMonth7;
            $netos_box[$month8] = $netoMonth8;
            $netos_box[$month9] = $netoMonth9;
            $netos_box[$month10] = $netoMonth10;
            $netos_box[$month11] = $netoMonth11;
            $netos_box[$month12] = $netoMonth12;
            $netos_box = implode(",", $netos_box);
            
            $netos_insurance = [];
            $netos_insurance[$month1] = $netoInsMonth1;
            $netos_insurance[$month2] = $netoInsMonth2;
            $netos_insurance[$month3] = $netoInsMonth3;
            $netos_insurance[$month4] = $netoInsMonth4;
            $netos_insurance[$month5] = $netoInsMonth5;
            $netos_insurance[$month6] = $netoInsMonth6;
            $netos_insurance[$month7] = $netoInsMonth7;
            $netos_insurance[$month8] = $netoInsMonth8;
            $netos_insurance[$month9] = $netoInsMonth9;
            $netos_insurance[$month10] = $netoInsMonth10;
            $netos_insurance[$month11] = $netoInsMonth11;
            $netos_insurance[$month12] = $netoInsMonth12;
            
            $netos_insurance = implode(",", $netos_insurance);
            
            //** End Row 4 Graph Income Reports **//

            //** Start Row 5 **//
            $sqm_rented_year = Storages::select(\DB::raw('SUM(`sqm`) as sqm_rented'))
            ->where('active', '=', 1)
            ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->where('flag_rent', '=', 1)
            ->first();
            if ($sqm_rented_year->sqm_rented == null) {
                $sqm_rented_year->sqm_rented = "0.00";
            }

            $sqm_space = Storages::select(\DB::raw('SUM(`sqm`) as space_sqm'))
            ->where('active', '=', 1)
            ->first();
            if ($sqm_space->space_sqm == null) {
                $sqm_space->space_sqm = "0.00";
            }

            $sqm_available = Storages::select(\DB::raw('SUM(`sqm`) as sqm_available'))
            ->where('active', '=', 1)
            ->where('flag_rent', '=', 0)
            ->first();
            if ($sqm_available->sqm_available == null) {
                $sqm_available->sqm_available = "0.00";
            }

            $sqm_rented = Storages::select(\DB::raw('SUM(`sqm`) as sqm_rented'))
            ->where('active', '=', 1)
            ->where('flag_rent', '=', 1)
            ->first();
            if ($sqm_rented->sqm_rented == null) {
                $sqm_rented->sqm_rented = "0.00";
            }

            $sqm_gained = Storages::select(\DB::raw('SUM(`sqm`) as sqm_gained'))
            ->where(\DB::raw('month(`rent_start`)'), '=', $currentMonth)
            ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->first();
            if ($sqm_gained->sqm_gained == null) {
                $sqm_gained->sqm_gained = "0.00";
            }

            $sqm_lost = Storages::select(\DB::raw('SUM(`sqm`) as sqm_lost'))
            ->where(\DB::raw('month(`rent_end`)'), '=', $currentMonth)
            ->where(\DB::raw('YEAR(`rent_end`)'), '=', $currentYear)
            ->first();
            if ($sqm_lost->sqm_lost == null) {
                $sqm_lost->sqm_lost = "0.00";
            }

            if ($sqm_space->space_sqm == "0.00" or $sqm_space->space_sqm == 0) {
                $porcentaje_sqm_available = "0.00";
            }
            $porcentaje_sqm_available = ($sqm_available->sqm_available/$sqm_space->space_sqm)*100;

            if ($sqm_space->space_sqm == "0.00" or $sqm_space->space_sqm == 0) {
                $porcentaje_sqm_available = "0.00";
            }
            $porcentaje_sqm_rented = ($sqm_rented->sqm_rented/$sqm_space->space_sqm)*100;

            $total_tenants = \DB::select('select count(*) as tenants from users where Role_id not in (1)');

            $tenants_grouped = \DB::select('SELECT t.name, count(u.id) as conteo
                FROM users u join cata_user_types t on u.userType_id = t.id and Role_id not in(1)
                group by t.id');
            //** End Row 5 **//

            //** Start Row 6 **//
            $sqms = Storages::select('sqm')->groupBy('sqm')->get();
            foreach ($sqms as $sqm) {
                $countSqm = Storages::where('sqm', '=', $sqm->sqm)->count();
                $countSqmRented = Storages::where('sqm', '=', $sqm->sqm)->where('flag_rent', '=', 1)->count();
                $countSqmAvailable = Storages::where('sqm', '=', $sqm->sqm)->where('flag_rent', '=', 0)->count();
                $totalBillingSqm = Storages::select(\DB::raw('SUM(price_per_month) as totalBillingSqm'))
                ->where('sqm', '=', $sqm->sqm)->first();
                $occupancyRateSqm = ($countSqmRented/$countSqm)*100;
                $sizeSqm = Storages::select(\DB::raw('SUM(sqm) as size'))->where('sqm', '=', $sqm->sqm)->first();
                $totalSize =  Storages::select(\DB::raw('SUM(sqm) as totalSize'))->first();
                $porcentajeSize = ($sizeSqm->size/$totalSize->totalSize)*100;
                $totalBilling = Storages::select(\DB::raw('SUM(price_per_month) as totalBilling'))->first();

                // $idStorages = Storages::select('id')->where('sqm', '=', $sqm->sqm)->get();
                // foreach ($idStorages as $idStorage) {
                //     $billing = Billing::select(\DB::raw('SUM(`cargo`) as billing'))->where('storage_id', '=', $idStorage->id)->first();
                //     $totalBillingSqm += $billing->billing;
                // }

                $porcentajeBilling = ($totalBillingSqm->totalBillingSqm/$totalBilling->totalBilling)*100;

                $sqm->countSqm = $countSqm;
                $sqm->countSqmRented = $countSqmRented;
                $sqm->countSqmAvailable = $countSqmAvailable;
                $sqm->totalBillingSqm = $totalBillingSqm->totalBillingSqm;
                $sqm->occupancyRateSqm = $occupancyRateSqm;
                $sqm->porcentajeSize = $porcentajeSize;
                $sqm->porcentajeBilling = $porcentajeBilling;
            }
            //** End Row 6 **//

            
            $boxesExpiresThisMonth = Storages::whereBetween(
                    'rent_end' , [Carbon::now()->firstOfMonth(),Carbon::now()->lastOfMonth()]
            )->count();

            $boxesExpiresNextMonth = Storages::whereBetween(
                    'rent_end' , [new Carbon ('first day of next month'),new Carbon('last day of next month')]
            )->count();
            
            $countSt = count($allStorages);
            $actual = round(($currentRent/$countSt)*100,2);
            $view->with('historyGraph', $data)
                  ->with('currentRent',$currentRent)
                  ->with('actual',$actual)
                  ->with('boxesRented',$boxesRented)
                  ->with('boxesRentedPastMonth',$boxesRentedPastMonth)
                  ->with('porcentajeBoxesRented',$porcentajeBoxesRented)
                  ->with('totalBilledMonth',$totalBilledMonth)
                  ->with('totalBilledPastMonth',$totalBilledPastMonth)
                  ->with('porcentajeBilled',$porcentajeBilled)
                  ->with('totalPayedMonth',$totalPayedMonth->payed)
                  ->with('paidInvoices',$paidInvoices)
                  ->with('rented_boxes',$rented_boxes)
                  ->with('rented_sqm',$rented_sqm)
                  ->with('new_boxes',$new_boxes)
                  ->with('new_boxes_sqm',$new_boxes_sqm)
                  ->with('billed_current_year',$billed_current_year)
                  ->with('payed_current_year',$payed_current_year)
                  ->with('not_paid_current_year',$not_paid_current_year)
                  ->with('sqm_rented_year',$sqm_rented_year->sqm_rented)
                  ->with('sqm_space',$sqm_space->space_sqm)
                  ->with('sqm_available',$sqm_available->sqm_available)
                  ->with('sqm_rented',$sqm_rented->sqm_rented)
                  ->with('porcentaje_sqm_available',$porcentaje_sqm_available)
                  ->with('porcentaje_sqm_rented',$porcentaje_sqm_rented)
                  ->with('sqm_gained',$sqm_gained->sqm_gained)
                  ->with('sqm_lost',$sqm_lost->sqm_lost)
                  ->with('total_tenants',$total_tenants[0]->tenants)
                  ->with('tenants_grouped',$tenants_grouped)
                  ->with('sqms',$sqms)
                  ->with('traMonth',$traMonth)
                  ->with('January',$January)
                  ->with('February',$February)
                  ->with('March',$March)
                  ->with('April',$April)
                  ->with('May',$May)
                  ->with('June',$June)
                  ->with('July',$July)
                  ->with('August',$August)
                  ->with('September',$September)
                  ->with('October',$October)
                  ->with('November',$November)
                  ->with('December',$December)
                  ->with('warehouses', $warehouses)

                  ->with('month1',$month1Name)
                  ->with('month2',$month2Name)
                  ->with('month3',$month3Name)
                  ->with('month4',$month4Name)
                  ->with('month5',$month5Name)
                  ->with('month6',$month6Name)
                  ->with('month7',$month7Name)
                  ->with('month8',$month8Name)
                  ->with('month9',$month9Name)
                  ->with('month10',$month10Name)
                  ->with('month11',$month11Name)
                  ->with('month12',$month12Name)
                  
                  ->with('netos_box',$netos_box)
                  ->with('netos_insurance',$netos_insurance)
                  
                  ->with('boxesExpiresThisMonth', $boxesExpiresThisMonth)
                  ->with('boxesExpiresNextMonth', $boxesExpiresNextMonth)
                  
            
            ;
        });

        view()->composer('pw.billing_reports_filter', function($view) {
            $warehouses = Warehouse::with(['Levels'])->get();
            $view->with('warehouses', $warehouses);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
