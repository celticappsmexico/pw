<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientAddress extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'client_address';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
                          'user_id',
                          'street',
                          'number',
                          'apartmentNumber',
                          'postCode',
                          'city',
                          'country_id'
                        ];

  public function country(){
    return $this->belongsTo(Countries::class,'country_id');
  }

}
