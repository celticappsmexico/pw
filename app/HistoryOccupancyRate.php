<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryOccupancyRate extends Model
{
    protected $table = 'history_occupancy_rate';
    protected $primarykey = 'id';
    protected $fillable = [
						    'warehouse',
                            'level',
                            'sqm',
							'sqm_quantity',
							'occupancy_rate',
						];
}
