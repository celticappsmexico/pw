<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataInsurance extends Model
{
    //
	//
	//Definicion de tabla
	protected $table = "invoice_sequence";
	
	//Definicion de campos
	protected $fillable = ["id"];
	
	//Definicion de exclusion campos en respuesta JSON
	protected $hidden = [];
	
	//Definicion de realciones
}
