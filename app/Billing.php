<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billing';
    
    protected $fillable = [
		    'user_id',
		    'storage_id',
		    'pay_month',
		    'flag_payed',
		    'abono',
		    'cargo',
		    'saldo',
			'pdf',
			'pdf_corregido',
		    'reference',
		    'bi_flag_last',
            'payment_type_id',
    		'bi_subtotal',
    		'bi_porcentaje_vat',
    		'bi_total_vat',
    		'bi_total',
    		'bi_recibo_fiscal',
    		'bi_number_invoice',
    		'bi_year_invoice',
    		'bi_transaction_braintree_id',
    		'payment_data_id',
    		'bi_batch',
    		'bi_flag_prepay',
    		'id_billing_correccion',
    		'bi_razon_correccion',
			'bi_number_invoice_corrected',
			'bi_year_invoice_corrected',
    		'bi_contendido_app',
    		'bi_paragon',
			'bi_webhook',
			'bi_start_date',
			'bi_end_date',
			'bi_months_invoice',
			'bi_braintree_suscription',
			'bi_status_impresion'
    ];

    public function user() {
    return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function storages() {
        return $this->belongsTo(Storages::class, 'storage_id', 'id');
    }
    
    public function billingDetail(){
    	return $this->hasMany(BillingDetail::class);
    }

    public function paymentType() {
        return $this->belongsTo(CataPaymentTypes::class, 'payment_type_id', 'id');
    }
    

}
