<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataPaymentTypes extends Model
{
  protected $table = 'cata_payment_types';
  protected $primarykey = 'id';
  protected $fillable = [
      'name', 'active','cpt_lbl_factura'
  ];
}
