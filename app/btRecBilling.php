<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class btRecBilling extends Model
{
     
	protected $table = 'bt_recurring_billing';

	protected $fillable = [	'data',
							'type'
						];


	/*public function Country(){
      return $this->belongsTo(Countries::class,'country_id');
    }*/

}
