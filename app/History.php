<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{

	protected $table = 'history';
    protected $primarykey = 'id';
    protected $fillable = [
						    'storage_id',
							'user_id',
							'rent_start',
							'rent_end',
							'price_per_month',
						];

	public function HistoryClient(){
      return $this->hasMany(User::class,'id','user_id');
    }

    public function HistoryStorage(){
      return $this->hasMany(Storages::class,'id','storage_id');
    }

}
