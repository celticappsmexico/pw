<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notificaciones extends Model
{
    protected $table = "notificaciones";

	//Definicion de campos
	protected $fillable = [
		'user_id',
		'storage_id',
		'notf_mensaje',
		'notf_eliminado'
	];

	protected $hidden = [];

	public function user() {
		return $this->belongsTo ( User::class, 'user_id', 'id' );
	}

	public function storage() {
		return $this->belongsTo ( Storages::class, 'storage_id', 'id' );
	}
}

