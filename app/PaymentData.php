<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentData extends Model
{
	protected $table = 'payment_data';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [	'user_id',
							'storage_id',
							'term',
							'prepay',
							'creditcard',
							'order_id',
							'insurance',
							'ext',
							'bt_id_plan',
							'idBt',
							'start_plan',
							'flag_plan_active',
							'token_pay',
							'active',
							'bt_id_suscription',
							'pd_vat',

							'pd_box_price',
							'pd_box_vat',
							'pd_box_total',
							
							'pd_insurance_price',
							'pd_insurance_vat',
							'pd_insurance_total',
							
							'pd_subtotal' ,
							'pd_total_vat' ,
							'pd_total',

							'pd_total_next_month',
			
			

							'pd_box_price_prepay',
							'pd_box_vat_prepay',
							'pd_box_total_prepay',
							'pd_insurance_price_prepay',
							'pd_insurance_vat_prepay',
							'pd_insurance_total_prepay',
							'pd_subtotal_prepay',
							'pd_total_vat_prepay',
							'pd_total_prepay',
								
							'user_transaction_id',
							'pd_method',

							'pd_pdf_contract',
							'pd_status_contract'
			
	                      ];


	public function paymentTerm(){
    	return $this->hasMany(CataTerm::class,'id','term');
  }

	public function paymentPrepay(){
    	return $this->hasMany(CataPrepay::class,'id','prepay');
  }

}
