<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correspondences extends Model
{
    protected $table = "correspondences";

	//Definicion de campos
	protected $fillable = [
		'users_ids',
		'asunto',
		'corresp_mensaje',
		'mail_from',
		'co_status'
	];

	protected $hidden = [];
/*
	public function user() {
		return $this->belongsTo ( User::class, 'users_ids', 'id' );
	}
*/
}

