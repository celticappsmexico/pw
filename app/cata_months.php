<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cata_months extends Model
{
    protected $table = 'cata_months';
    protected $fillable = [
        'id',
        'month',
    ];
}
