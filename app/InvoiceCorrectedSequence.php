<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceCorrectedSequence extends Model
{
    //
	//
	//Definicion de tabla
	protected $table = "invoice_corrected_sequence";
	
	//Definicion de campos
	protected $fillable = ["id"];
	
	//Definicion de exclusion campos en respuesta JSON
	protected $hidden = [];
	
	//Definicion de realciones
}
