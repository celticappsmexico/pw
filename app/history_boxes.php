<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_boxes extends Model
{
    protected $table = 'history_boxes';
    protected $primarykey = 'id';
    protected $fillable = [
						    'boxes_rented',
                            'new_boxes',
                            'lost_boxes',
							'month',
						];
}
