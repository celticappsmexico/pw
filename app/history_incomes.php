<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_incomes extends Model
{
    protected $table = 'history_incomes';
    protected $primarykey = 'id';
    protected $fillable = [
						    'billed',
                            'payed',
                            'not_payed',
							'month',
						];
}
