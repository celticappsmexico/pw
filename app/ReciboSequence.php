<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReciboSequence extends Model
{
    //
	//Definicion de tabla
	protected $table = "recibos_sequence";
	
	//Definicion de campos
	protected $fillable = ["id"];
	
	//Definicion de exclusion campos en respuesta JSON
	protected $hidden = [];
	
}
