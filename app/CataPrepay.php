<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataPrepay extends Model
{
  protected $table = 'cata_prepay';
  protected $primarykey = 'id';
  protected $fillable = [
      'months', 'off'
  ];
}
