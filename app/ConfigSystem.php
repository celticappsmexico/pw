<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigSystem extends Model
{
  protected $table = 'system_config';
  protected $primarykey = 'id';
  protected $fillable = [
      'name', 'value'
  ];
}
