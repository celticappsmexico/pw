<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Levels extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'levels';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [	'name',
							'warehouse_id',
							'active'
						];


	/*public function Country(){
      return $this->belongsTo(Countries::class,'country_id');
    }*/

    public function Storages(){
      return $this->hasMany(Storages::class,'level_id')->where('active', 1);
    }

    public function StoragesGrouped(){
      return $this->hasMany(Storages::class,'level_id')->where('active', 1)->orderBy('sqm', 'desc');
    }

    public function warehouse() {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'id');
    }
}
