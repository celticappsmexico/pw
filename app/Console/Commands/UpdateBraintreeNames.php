<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use Braintree_Exception;
use App\User;

class UpdateBraintreeNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update_braintree_names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);

        $users = User::where([
                    'active'        => '1',
                    //'customer_id'   => '1671468874'
                ])->whereNotNull('customer_id')->get();


        foreach($users as $user){
            $this->info('**********************');
            $this->info('User ID : '. $user->id);
            $this->info('Customer BT ID : '. $user->customer_id);
            
            $firstName = $user->name;
            $lastName = $user->lastName;
            $companyName = $user->companyName;

            //$customer = $gateway->customer()->find($user->customer_id);

            if($user->userType_id == 3){
                $updateResult = $gateway->customer()->update(
                    $user->customer_id,
                    [
                      'firstName' => $companyName,
                      'company' => $companyName
                    ]
                );
            
            }else{
                $updateResult = $gateway->customer()->update(
                    $user->customer_id,
                    [
                      'firstName' => $firstName,
                      'lastName' => $lastName,
                      'company' => $companyName
                    ]
                );
                    
            }
            
            try {
                if($updateResult->success){
                    $this->info("CustomerID: ". $user->customer_id . " updated!!" );
                }
              } catch (Braintree_Exception_NotFound $e) {
                echo $e->getMessage();
              }
            
            
            # true if update was successful

            $this->info('**********************');
        }
    }
}
