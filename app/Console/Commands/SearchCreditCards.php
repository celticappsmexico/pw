<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use Braintree_CustomerSearch;

class SearchCreditCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:search_credit_cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);

        $collection = $gateway->customer()->search([
            Braintree_CustomerSearch::id()->is("137342831")
        ]);
        
        foreach($collection as $customer) {
            $this->info("Firstname: ". $customer->firstName) ;
        }
    }
}
