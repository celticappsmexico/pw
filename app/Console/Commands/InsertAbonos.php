<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Billing;
use App\BraintreeLog;
use App\PaymentData;
use Carbon\Carbon;

class InsertAbonos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:insert_abonos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $rows = BraintreeLog::where(
            'created_at', '>=', '2019-12-06'
        )->where([
            'bl_status'    => 'SUCCESSFUL'
        ])->get();

        foreach($rows as $row){
            $id = $row->id;
            $user_id = $row->user_id;
            $storage_id = $row->storage_id;
            $suscription_id = $row->bl_suscription_id;
            $transaction_id = $row->bl_transaction_id;
            $amount = $row->bl_amount;

            $this->info("**************");
            $this->info("ID: ". $id);
            $this->info("UserID:". $user_id);
            $this->info("StorageID:". $storage_id);

            if($id != "1146"){

                
                
                $paymentData = PaymentData::where([
                        'pd_suscription_id'	=> $suscription_id
                ])->first();
            
                //OBTIENE SALDO
                $saldo = Billing::where(['user_id' => $user_id, 'bi_flag_last' => 1, 'storage_id' => $storage_id])->sum('saldo');
                $saldoNuevo = $saldo + $amount;

                //OBTIENE BILLING DEL MES
                $billing = Billing::where([
                    'payment_data_id' 	=> $paymentData->id,
                    'flag_payed'		=> '0'
                    ])->whereMonth('pay_month', '=' , Carbon::now()->month)
                    ->first();

                if(!is_null($billing)){
                    $billing->flag_payed = 1;
                    $billing->save();
    
                    $this->info("BillingID: ".$billing->id);
                    
                }
                
                /*
                Billing::where([
                        'user_id' => $user_id, 
                        'bi_flag_last' => 1, 
                        'storage_id' => $storage_id
                    ])->update(['bi_flag_last' => '0']);
                */
                /*

                $newBilling = new Billing();
                $newBilling->user_id 			= $user_id;
                $newBilling->storage_id			= $storage_id;
                $newBilling->pay_month			= Carbon::now()->format('Y-m-d');
                $newBilling->flag_payed 		= 1;
                $newBilling->abono				= $amount;
                $newBilling->cargo				= 0;
                $newBilling->saldo				= $saldoNuevo;
                $newBilling->pdf				= "";
                $newBilling->reference			= $transaction_id;
                $newBilling->bi_flag_last		= 1;
                $newBilling->payment_data_id 	= $paymentData->id;
                $newBilling->payment_type_id 	= "2";
                $newBilling->bi_number_invoice	= $billing->bi_number_invoice;
                $newBilling->bi_year_invoice	= $billing->bi_year_invoice;
                $newBilling->bi_webhook			= 1;
                $newBilling->save();
                */
                
                $this->info("**************");
            }

        }

        $this->info("Total Rows: ".$rows->count());
    }
}
