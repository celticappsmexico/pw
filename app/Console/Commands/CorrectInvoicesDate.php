<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Billing;
use App\User;
use App\ConfigSystem;
use App\Countries;
use App\InvoiceCorrectedSequence;
use Illuminate\Support\Facades\Mail;

class CorrectInvoicesDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:correct_invoices_date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("inicio: ".Carbon::now());

        $invoices = Billing::where([
                            #'bi_batch'     => '1',
                            'pay_month'    => '2019-10-08',
                            'id'           => '35119'
                        ])
                        #->where('id','>','34552')
                        #->where('id','<=','34918')
                        ->where('cargo','>','0')->get();

        $this->info("Total a enviar: " . $invoices->count());

        foreach($invoices as $invoice){
            
            //INFO TENANT
            $user = User::find($invoice->user_id);

            if($user->userType_id == 1){
                isset($user->name) ? $userName = $user->name : $userName = '';
                isset($user->lastName) ? $userLast = $user->lastName : $userLast = '';
                $clientName = $userName.' '.$userLast;
            }else{
                //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
                isset($user->companyName) ? $userName = $user->companyName : $userName = '';
                $clientName = $userName;
            }
            //-- INFO TENANT

            //ADDRESS TENANT
            if($user->UserAddress[0]->street != '' ){
    	
                $street = $user->UserAddress[0]->street;
                $numExt = $user->UserAddress[0]->number;
                $numInt = $user->UserAddress[0]->apartmentNumber;
                $city = $user->UserAddress[0]->city;
                $country = $user->UserAddress[0]->country_id;
                $country = Countries::find($country);
                $country = $country->name;
                $cp = $user->UserAddress[0]->postCode;
            
                if($numInt != "")
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
                else
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
            
            }else{
                $fullAddress = '';
            }
            //-- ADDRESS TENANT

            //SYSTEM CONFIG
            $data = ConfigSystem::all();
            $placeDb = $data[2]->value;
            $addressDb = $data[3]->value;
            $nipDb = $data[4]->value;
            $bankDb = $data[5]->value;
            $accountDb = $data[6]->value;
            $seller = $data[8]->value;
            //END DATA SYSTEM CONFIG

            $invoiceCorrected = InvoiceCorrectedSequence::create();
            
            $data =  [
                'invoiceCorrected'      => 63,
                'copy'                  => 0,
                'invoiceNumber'         => $invoice->bi_number_invoice,

                //System Config
                'seller'                => $seller,
                'address'               => $addressDb,
                'bank' 				    => $bankDb,
    			'account' 			    => $accountDb,
    				

                //Tenant
                'clientName'            => $clientName,
                'clientAddress'         => $fullAddress,
                'clientType'		    => $user->userType_id,
                'clientNip' 		    => $user->nipNumber,
                'clientPesel'		    => $user->peselNumber,
                'accountant_number'	    => $user->accountant_number,

                'date'				    => Carbon::now()->addDays(7)->format('Y-m-d'),

                'consecutiveCorrected'  => $invoiceCorrected->id ,
                'numberInvoice'         => $invoice->bi_number_invoice
            ];
    
            $pdfName = date("y")."-PW-".$invoice->bi_number_invoice.'_'.$user->accountant_number.'_CORRECTED.pdf';
    			                                                  
            $view =  \View::make('factura_corrected_date', compact('data', 'date'))->render();
                                                                            
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            $pdf->save(storage_path().'/app/invoices/'.$pdfName);

            //Nuevo Billing de correccion
            
            $newBilling = Billing::create([
                "user_id"       => $invoice->user_id ,
                "storage_id"    => $invoice->storage_id ,
                "pay_month"     => Carbon::now()->format('Y-m-d'),
                "flag_payed"    => $invoice->flag_payed,
                "abono"         => 0,
                "cargo"         => 0,
                "saldo"         => 0,
                "pdf"           => "",
                "reference"     => "CORRECTION",
                "bi_flag_last"  => "0",
                "payment_type_id"=> $invoice->payment_type_id,
                "bi_subtotal"   => $invoice->bi_subtotal,
                "bi_porcentaje_vat" => $invoice->bi_porcentaje_vat,
                "bi_total_vat"  => $invoice->bi_total_vat,
                "bi_total"      => $invoice->bi_total,
                "bi_status_impresion"   => "TERMINADO",
                "bi_recibo_fiscal"  => $invoice->bi_recibo_fiscal,
                "bi_number_invoice" => $invoice->bi_number_invoice,
                "bi_year_invoice"   => $invoice->bi_year_invoice,
                "bi_transaction_braintree_id"   => $invoice->bi_transaction_braintree_id,
                "payment_data_id"   => $invoice->payment_data_id,
                "bi_flag_prepay"    => $invoice->bi_flag_prepay,
                "bi_batch"          => $invoice->bi_batch,
                "pdf_corregido"     => $pdfName,
                "bi_razon_correccion" => "Błędna data i miejsce wystawienia faktury",
                "bi_number_invoice_corrected"  => $invoiceCorrected->id, 
                "bi_year_invoice_corrected"   => "2019",
                "bi_contenido_app"  => $invoice->bi_contenido_app,
                "bi_paragon"        => $invoice->bi_paragon,
                "bi_webhook"        => $invoice->bi_webhook,
                "bi_start_date"     => $invoice->bi_start_date,
                "bi_end_date"       => $invoice->bi_end_date,
                "bi_months_invoice" => $invoice->bi_months_invoice,
                "bi_braintree_suscription"  => $invoice->bi_braintree_suscription
                
            ]);

            $invoice->id_billing_correccion = $newBilling->id;
            $invoice->pdf_corregido         = $pdfName;
            $invoice->save();
            

            //Envio de correo
            if(env('ENVIRONMENT') == 'DEV'){
                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* DEV */
                        'email' 				=> 'oscar@novacloud.systems',
                        'email2' 				=> 'oscarbar17@gmail.com',
                        'tituloCorreo'			=> "Przechowamy Wszystko - korekta faktury 19-KPW-".$invoiceCorrected->id,
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'id_codificado'         => base64_encode($invoice->id)
                ];

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_factura_corrected_date', $emailData, function ($message) use ($emailData) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );

                    //DEV
                    #$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    #$message->bcc ( ['myss.latam@gmail.com'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject ( $emailData["tituloCorreo"]);
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            else{
                //PROD
                $email2 = $user->email2;
                
                if($email2 == "")
                    $email2 = $user->email;

                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* PROD */
                        
                        'email' 				=> $user->email,
                        'email2' 				=> $email2,
                        #'email' 				=> 'oscar@novacloud.systems',
                        #'email2' 				=> 'oscarbar17@gmail.com',
                     
                        'tituloCorreo'			=> "Przechowamy Wszystko - korekta faktury 19-KPW-".$invoiceCorrected->id,
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'id_codificado'         => base64_encode($invoice->id)
                ];
                

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_factura_corrected_date', $emailData, function ($message) use ($emailData) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );
                
                    //PROD
                    $message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    $message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl', 'karedys@celticproperty.pl', 'oscar.sanchez@novacloud.mx'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject ( $emailData["tituloCorreo"] );
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            
            
            $this->info('Invoice sent');

    
        }
        $this->info("fin: ".Carbon::now());                                                                        
        
    }
}
