<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\BraintreeLog;

class EnviarTransaccionesBraintree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:enviar_transacciones_braintree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $exitosas = BraintreeLog::where([
                            'bl_notified'   => false ,
                            'bl_status'     => 'SUCCESSFUL'
                        ])->get();

        $erroneas = BraintreeLog::where([
                            'bl_notified'   => false ,
                            'bl_status'     => 'UNSUCCESSFUL'
                        ])->get();

        if($exitosas->count() == 0 and $erroneas == 0){
            $this->info("Nada por enviar");
            return false;
        }

        $emailData = [
                'exitosas'  	=> $exitosas,
                'erroneas'      => $erroneas
        ];

        //ENVIO DE CORREO
        if(env('ENVIRONMENT') == 'DEV'){
            $this->info("DEV");
            $resp = Mail::send ( 'emails.2019_braintree_transactions', $emailData, function ($message) use ($emailData) {
                $message->to ( "oscar.sanchez@novacloud.mx" );
                $message->to ( "oscarbar17@gmail.com" );
                
                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( "Braintree Transactions" );
                
            } );
      
        }else{
            $this->info("PROD");
            $resp = Mail::send ( 'emails.2019_braintree_transactions', $emailData, function ($message) use ($emailData) {
                $message->to ( "oscar.sanchez@novacloud.mx" );
                
                $message->to ( "darbey@celticproperty.pl" );
                $message->to ("oscar.sanchez@novacloud.mx");
                $message->to ("baldyga@celticproperty.pl");
                $message->to ("abraham@novacloud.systems");
                $message->to ("jones@celticproperty.pl");
                
                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( "Braintree Transactions" );
                
            } );
    

        }
       
        foreach($exitosas as $row){
            $log = BraintreeLog::find($row->id);
            $log->bl_notified = 1;
            $log->save();
        }

        foreach($erroneas as $row){
            $log = BraintreeLog::find($row->id);
            $log->bl_notified = 1;
            $log->save();
        }

    }
}
