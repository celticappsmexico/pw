<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Orders;
use App\DetOrders;
use Carbon\Carbon;
use App\Storages;

class DoReleaseOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_release_orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    	$this->info("//-------------------");
    	$this->info('Iniciando Proceso (Release orders)...'. Carbon::now());
    	
        $fecha = Carbon::now()->addDay(-5)->format('Y-m-d');
        
        $this->info('Fecha limite (-5 Dias): '. $fecha);
        
        $orders = Orders::where(['active' => '1'])->where('created_at','<',$fecha)->lists('id');
        $detOrders = DetOrders::whereIn('order_id',$orders)->lists('product_id');

        $this->info('Total de Ordenes: '. $orders->count());
        
        foreach($detOrders as $st){
        	//$this->info('StorageID: ' . $item->product_id);
        	$this->info('Se libera el storage: ' . $st);
        	
        	//LIBERA STORAGE
        	$storage = Storages::find($st);
        	$storage->flag_rent			= 0 ;
        	$storage->user_id			= 0 ;
        	$storage->payment_data_id	= 0 ;
        	$storage->final_price		= 0 ;
        	$storage->rent_start		= '0000-00-00';
        	$storage->rent_end			= '0000-00-00';
        	$storage->date_subscribe_braintree = '0000-00-00';
        	$storage->price_per_month	= 0;
        	$storage->st_meses_prepago	= 0;
        	$storage->save();
        	
        }
        
        foreach($orders as $order){
        	$this->info('Se inhabilita orden: ' . $order);
        	
        	$o = Orders::find($order);
        	$o->active = 0;
        	$o->save();	
        }
        
        
        $this->info('Fin de Proceso...'. Carbon::now());
        $this->info("//-------------------");
         
    }
}
