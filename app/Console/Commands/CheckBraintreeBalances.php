<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use Carbon\Carbon;
use App\PaymentData;
use App\Billing;

class CheckBraintreeBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:check_braintree_balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('******************************');
        $this->info('Inicia - '. Carbon::now());

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);


        $collection = $gateway->subscription()->search([
            Braintree_SubscriptionSearch::planId()->is(env('PLAN_ID')),
            Braintree_SubscriptionSearch::status()->in(
              [Braintree_Subscription::ACTIVE]
            )
          ]);
        
        //

        
        foreach($collection as $subscription) {
           
            //echo $subscription->billingDayOfMonth;
            
            $this->info("Subscription ID: ". $subscription->id);
            $this->info("Balance: ". $subscription->balance);
            
            $pd = PaymentData::where([
                'pd_suscription_id' => $subscription->id
            ])->first();

            if(is_null($pd)){
                $flag = true;
            }

            if(!is_null($pd)){
                $storage_id = $pd->storage_id;
                $user_id    = $pd->user_id;

                $saldo = Billing::where(['user_id' => $user_id, 'bi_flag_last' => 1, 'storage_id' => $storage_id])->sum('saldo');
                if(is_null($saldo)){
                    $saldo = 0;
                }

                if($saldo != $subscription->balance){
                    $this->info("Ojo: saldos incorrectos!!!!!!!!");
                    
                    $this->info('ID: '. $subscription->id);
                    $this->info('Balance: '. $subscription->balance);

                    $this->info("PaymentDataID: ". $pd->id);
                    $this->info("Saldo PW: ". $saldo);
                    $this->info("Proximo Cargo: ". $subscription->nextBillAmount);
                    $this->info("-----");
                }
            }
            
            $this->info("**************");
            
            
        }
    }
}
