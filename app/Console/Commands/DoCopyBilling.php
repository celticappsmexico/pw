<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Billing;
use App\Storages;
use App\CataPaymentTypes;
use App\User;
use App\Countries;
use App\ConfigSystem;
use DebugBar\Storage\StorageInterface;
use Illuminate\Support\Facades\Storage;	
use App\Library\MoneyString;
use Carbon\Carbon;

class DoCopyBilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_copy_billing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $billing = Billing::where('pdf','<>','')
        			->where('cargo', '>', '0')
        			->whereNotNull('bi_recibo_fiscal')
        			->get();
        
        
        foreach($billing as $bill){
        	$this->info('Billing_id: '. $bill->id);
        	
        	$st = Storages::with('StoragePaymentData')->find($bill->storage_id);
        	 
        	$paymentData = $st->StoragePaymentData->first();
        	
        	$this->info('Storage ID '. $st->id);
        	
        	if ($paymentData->creditcard) {
        		$pt = CataPaymentTypes::find(2);
        		$pt = $pt->cpt_lbl_factura;
        	}else{
        		$pt = CataPaymentTypes::find(3);
        		$pt = $pt->cpt_lbl_factura;
        	}
        	
			$this->info('PT: '.$pt);
        	
        	$dataUser = User::with(['UserAddress'])->find($bill->user_id);
        	
        	//id user is a client, the name is the real name if not, name is a company name
        	if($dataUser->userType_id == 1){
        		isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
        		isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
        		$clientName = $userName.' '.$userLast;
        	}else{
        		//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
        		isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
        		$clientName = $userName;
        	}
        	
        	$this->info('Client name: '. $clientName);
        	
        	// NIP
        	$clientNip = $dataUser->nipNumber;
        	
        	$this->info('Nip number: '.$clientNip);
        	
        				// Address
        	if($dataUser->UserAddress[0]->street != '' ){
        		$street = $dataUser->UserAddress[0]->street;
        		$numExt = $dataUser->UserAddress[0]->number;
        		$numInt = $dataUser->UserAddress[0]->apartmentNumber;
        		$city = $dataUser->UserAddress[0]->city;
        		$country = $dataUser->UserAddress[0]->country_id;
        		$country = Countries::find($country);
        		$country = $country->name;
        		$cp = $dataUser->UserAddress[0]->postCode;
        	
        		if($numInt != "")
        			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
        		else
        			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
        		   
        	}else{
        		$fullAddress = '';
        	}
        	
        	$this->info('Full Address: '.$fullAddress);
        	
        	if ($paymentData != null) {
        		$activePay = $paymentData->id;
        	
        		$this->info('Active Pay: '.$activePay);
        	
        		$data = ConfigSystem::all();
        		    
        		$ins = $paymentData->insurance;
        		$vatTag = $paymentData->pd_vat + 1;
        		$vat = $paymentData->pd_vat * 100;
        	
        		$placeDb = $data[2]->value;
        		$addressDb = $data[3]->value;
        		$nipDb = $data[4]->value;
        		$bankDb = $data[5]->value;
        		$accountDb = $data[6]->value;
        		$invoiceNumberDb = $data[7]->value;
        		$seller = $data[8]->value;
        	
        		// increments invoice number:
        		$invoiceNumberDb = (int)$invoiceNumberDb;
        		
        		$invoiceNumberDb = $invoiceNumberDb + 1;
        		
        		$newInvoiceNumberDb = sprintf( '%04d', $invoiceNumberDb );
        		$data[7]->value = $newInvoiceNumberDb;
        		$res = $data[7]->save();
        		    
        		$pdfName = date("y")."-PW-".$bill->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
        		$fakturaName = date("y")."-PW-".$bill->bi_number_invoice.'_'.$dataUser->accountant_number;
        		    
        		$this->info('PDF NAME: '.$pdfName);
        	
        		//crear el pdf
        		$ss = Storage::disk('invoices')->makeDirectory('1');
        		//dd($ss);
        		    
        		    
        		$this->info('******************');
        		$this->info('*** Se convierte  dinero a letras');
        		$this->info('*** Precio por mes: ' . $st->price_per_month);
        		    
        		$totalString = MoneyString::transformQtyCurrency($st->price_per_month);
        		    
        		$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
        		    
        		$this->info('*** Precio por mes (letras): ' . $totalString);
        		$this->info('*********FIN');
        		    
        		//OBITIENE MES
        		setlocale(LC_ALL, 'de_DE');
        		    
        		$mes = "";
        		    
        		switch(Carbon::parse($bill->created_at)->format('m'))
        		{
        			case '01':
        				$mes = 'styczeń';
        			break;
        			case '02':
        				$mes = 'luty';
        			break;
        			case '03':
        				$mes = 'marzec';
        			break;
        			case '04':
        				$mes = 'kwiecień';
        			break;
        			case '05':
        				$mes = 'maj';
        			break;
        			case '06':
        				$mes = 'czerwiec';
        			break;
        			case '07':
        				$mes = 'lipiec';
        			break;
        			case '08':
        				$mes = 'sierpień';
        			break;
        			case '09':
        				$mes = 'wrzesień';
        			break;
        			case '10':
        				$mes = 'październik';
        			break;
        			case '11':
        				$mes = 'listopad';
        			break;
        			case '12':
        				$mes = 'grudzień';
        			break;
        	}
        	
        	$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
        		    
        	$year = Carbon::now()->year;
        		    
			//CALUCLO D SUBTOTAL VAT
			$subtotal = round( $st->price_per_month / $vatTag , 2 );
        	$vatTotal = round( $st->price_per_month - $subtotal , 2 );
        		    
        	$box = $subtotal - $ins;
        	$vatInsurance = round($ins * ($vatTag - 1), 2);
        	$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
        	$insuranceBruto = round($ins + $vatInsurance, 2);
        		    
        	$boxBruto = round( $box + $vatTotalInsurance,2);
        		    
        	$this->info('***** Calculando totales');
        	$this->info('Subtotal: '.$subtotal);
        	$this->info('vat total: '.$vatTotal);
        	$this->info('box: '. $box);
        	$this->info('vatInsurance: '. $vatInsurance);
        	$this->info('vatTotalInsurance: '. $vatTotalInsurance);
        	$this->info('vat '. $vat );
        	$this->info('***** ');
        	//FIN DE CALCULO
        		    
        	$reciboFiscal = str_pad($bill->bi_recibo_fiscal,6,"0",STR_PAD_LEFT);
        	
        	$reciboFiscal = "W".$reciboFiscal;
        		    
        	$this->info("Recibo Fiscal: ". $reciboFiscal);
        	
        	$data =  [
        			'insurance' 		=> $ins,
        			'place' 			=> $placeDb,
        			'address' 			=> $addressDb,
        			'nip' 				=> $nipDb,
        			'bank' 				=> $bankDb,
        			'account' 			=> $accountDb,
        			'invoiceNumber' 	=> $bill->bi_number_invoice,
        			'seller' 			=> $seller,
        			'2y' 				=> date("y"),
        			'currentDate' 		=> $bill->pay_month,
        			'paymentType' 		=> $pt,
        			'clientName' 		=> $clientName,
        			'clientAddress' 	=> $fullAddress,
        			'clientNip' 		=> $clientNip,
        			'clientPesel'		=> $dataUser->peselNumber,
        			'clientType'		=> $dataUser->userType_id,
        			'grandTotal' 		=> $st->price_per_month,
        			'grandTotalString' 	=> $totalString,
        			'date'				=> Carbon::parse($bill->pay_month)->addDays(7)->format('Y-m-d'),
        			'dataUser'			=> $dataUser,
        			'mes'				=> $mes,
        			'year'				=> $year,
        			'storage'			=> $st ,
        			'vat'				=> $vat ,
        			'box'				=> $box,
        			'vatTotalInsurance' => $vatTotalInsurance,
        			'subtotal'			=> $subtotal ,
        								
        			'vatTotal'			=> $vatTotal,
        			'vatInsurance'		=> $vatInsurance ,
        								
        			'boxBruto'			=> $boxBruto,
        			'insuranceBruto'	=> $insuranceBruto,
        								
        			'accountant_number'	=> $dataUser->accountant_number,
        			'reciboFiscal'		=> $reciboFiscal, 
        			'copy'				=> 1
        								
       		];
        		    
        	
        	$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
        		    
        	$pdf = \App::make('dompdf.wrapper');
        	$pdf->loadHTML($view);
        	$pdf->save(storage_path().'/app/invoices/'.$pdfName);
        
        	$this->info('Search for: user_id '.$st->user_id . ', storage_id : '.$st->id);
        	}

        	$this->info('***********************');
        	    		
        	    	
        	
        	//END FOREACH
        }
        
    }
}
