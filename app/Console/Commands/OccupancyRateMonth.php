<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\HistoryOccupancyRate;
use App\Warehouse;
use App\Storages;
use Carbon\Carbon;

class OccupancyRateMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OccupancyRateMonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$this->info('Start Occupancy Rate');
    	
        $end = Carbon::now()->subMonth()->endOfMonth()->toDateString();
        
        $this->info('END '. $end );
        
        // $warehouses = Warehouse::with(['Levels.Storages'])->get();
        //
        // foreach ($warehouses as $warehouse) {
        //     foreach ($warehouse->Levels as $key => $Level) {
        //         $historyOccupancyRate = new HistoryOccupancyRate();
        //         $ocupados = 0;
        //         $totalStorage = 0;
        //         $ocupados = $Level->Storages()->where('flag_rent', 1)->count();
        //         $totalStorage = $Level->Storages()->count();
        //         $Level->ocupados=$ocupados;
        //         $Level->totalStorage=$totalStorage;
        //         $Level->Promedio=($ocupados/$totalStorage)*100;
        //         $historyOccupancyRate->warehouse=$warehouse->name;
        //         $historyOccupancyRate->level=$Level->name;
        //         $historyOccupancyRate->occupancy_rate=$Level->Promedio;
        //         $historyOccupancyRate->created_at=$end;
        //         $historyOccupancyRate->updated_at=$end;
        //         $historyOccupancyRate->save();
        //     }
        // }

        $storages = Storages::with(['level.warehouse'])->select('level_id', 'sqm')->distinct('sqm')->orderBy('sqm', 'desc')->get();
        
        $this->info('Storages: '. $storages);

        foreach ($storages as $storage) {
            $historyOccupancyRate = new HistoryOccupancyRate();
            $ocupados = 0;
            $totalSqm = 0;
            $level_id = $storage->level->id;
            $sqm = $storage->sqm;
            $totalSqm = Storages::where('level_id', $level_id)->where('sqm', $sqm)->count();
            $ocupados = Storages::where('level_id', $level_id)->where('flag_rent', 1)->where('sqm', $sqm)->count();
            $storage->totalSqm=$totalSqm;
            $storage->ocupados=$ocupados;
            $storage->promedio=($ocupados/$totalSqm)*100;
            $historyOccupancyRate->warehouse=$storage->level->warehouse->name;
            $historyOccupancyRate->level=$storage->level->name;
            $historyOccupancyRate->sqm=$sqm;
            $historyOccupancyRate->sqm_quantity=$storage->totalSqm;
            $historyOccupancyRate->occupancy_rate=$storage->promedio;
            $historyOccupancyRate->created_at=$end;
            $historyOccupancyRate->updated_at=$end;
            $historyOccupancyRate->save();
        }

    }
}
