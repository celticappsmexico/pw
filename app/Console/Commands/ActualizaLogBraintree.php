<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use App\BraintreeLog;
use Carbon\Carbon;

class ActualizaLogBraintree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:actualiza_log_braintree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('******************************');
        $this->info('Inicia - '. Carbon::now());

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);


        $collection = $gateway->subscription()->search([
            Braintree_SubscriptionSearch::planId()->is(env('PLAN_ID')),
            Braintree_SubscriptionSearch::status()->in(
              [
                Braintree_Subscription::ACTIVE,
                Braintree_Subscription::PAST_DUE,
                Braintree_Subscription::PENDING
              ]
            )
          ]);

          foreach($collection as $subscription) {
            $this->info('----------------');
            $this->info('ID: '. $subscription->id);
            $this->info('----------------');
          }
        
          
          $this->info('Fin - '. Carbon::now());
          $this->info('******************************');
    }
}
