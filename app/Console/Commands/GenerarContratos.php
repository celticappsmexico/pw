<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\PaymentData;
use App\Storages;

class GenerarContratos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generar_contratos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("***********************************");
        $this->info("Inicia generacion de contratos - ". Carbon::now());

        $paymentData = PaymentData::where([
                        'pd_status_contract'    => 'PENDIENTE'
                    ])->first();

        if(is_null($paymentData)){
            $this->info("No hay contratos pendientes");
            $this->info("Fin generacion de contratos - ". Carbon::now());
            $this->info("***********************************");

            return false;
        }

        $storage = Storages::where([
                        'payment_data_id'   => $paymentData->id
                    ])->first();

        if(!is_null($storage)){
            $this->info("Storage ID: ". $storage->id);
            $this->info("Storage Alias: ". $storage->alias);

            //Genera PDF
            $view =  \View::make('contract', [$paymentData])->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
                
            $pdf->save(storage_path().'/app/contracts/'."CONTRACT_".$paymentData->id.".pdf");
    
            $paymentData->pd_pdf_contract = "CONTRACT_".$paymentData->id.".pdf";
            $paymentData->pd_status_contract = "FINALIZADO";
            $paymentData->save();
        }

        $this->info("Fin generacion de contratos - ". Carbon::now());
        $this->info("***********************************");
    }
}
