<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Storages;
use Carbon\Carbon;

class DoSubtractMonths extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_substract_months';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    	$this->info("//-------------------");
    	$this->info('Iniciando Proceso de restar meses de prepago...'. Carbon::now());
    	
    	$storages = Storages::where('st_meses_prepago','>','0')->get();
    	
    	$this->info('Total de storages: ' . $storages->count());
    	
    	foreach($storages as $st){
    		$this->info('-----------');
    		$this->info('StorageID: ' . $st->id);
    		$this->info('Alias: ' . $st->alias);
    		$this->info('Antes: '. $st->st_meses_prepago);
    		$this->info('Ahora: '. ($st->st_meses_prepago - 1));
    		$this->info('-----------');
    		
    		$st->st_meses_prepago = $st->st_meses_prepago - 1;
    		$st->save();
    	}
    	
    	$this->info('Fin de Proceso...'. Carbon::now());
    	$this->info("//-------------------");
    	
    }
}
