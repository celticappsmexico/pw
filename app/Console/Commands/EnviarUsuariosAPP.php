<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Mail;

class EnviarUsuariosAPP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_users_app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        
    	//UPDATE SEGUN SEA EL TIPO DE USUARIO
    	$filtros = [
    			'active'	=> '1'
    	];
    	
    	$users = User::where($filtros)
			->where('id','>','1')
			->where('id','=','396')
			->get();
			
		$this->info('Total : '.$users->count());
		
		
    	foreach($users as $user){
    		
    		$userApp = "";
    		 
    		if($user->userType_id == 1){
    			//PERSON
    			$userApp = $user->peselNumber;
    		}else{
    			//ONE PERSON COMPANY Y COMPANY
    			$userApp = $user->nipNumber;
    		}
    		
    		$password = uniqid();
    		$passwordBcrypt = bcrypt($password);
    		
    		$user = User::find($user->id);
    		$user->username = $userApp;
    		$user->password	 = $passwordBcrypt;
    		$user->save();
			 
			if($user->userType_id == 1){
				isset($user->name) ? $userName = $user->name : $userName = '';
				isset($user->lastName) ? $userLast = $user->lastName : $userLast = '';
				$clientName = $userName.' '.$userLast;
			}else{
				//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
				isset($user->companyName) ? $userName = $user->companyName : $userName = '';
				$clientName = $userName;
			}

    		$emailData = [
    					'name' 		=> $clientName,
    					'email' 	=> $user->email,
    					'user'		=> $userApp,
    					'pass'		=> $password
    			];
    		
    		
    		$this->info('Email al que se envia');
    		$this->info($user->email);

    		//ENVIO DE CORREO
    		if(env('ENVIRONMENT') == 'DEV'){

				$this->info('DEV');

				$resp = Mail::send ( 'emails.email_welcome_tenant', $emailData, function ($message) use ($emailData) {
				
					$message->to ( 'oscar.sanchez@novacloud.mx' );
				
					$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
					$message->subject ( 'Welcome to PW ' );
				} );
			}else{
				
				$this->info('PROD');

				$resp = Mail::send ( 'emails.email_welcome_tenant', $emailData, function ($message) use ($emailData) {
				
					$message->to ( $emailData['email'] );
					$message->bcc ( 'oscar.sanchez@novacloud.mx' );
				
					$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
					$message->subject ( 'Welcome to PW ' );
				} );
			}
    		
    		$this->info('Resp Email: '.$resp);
    		
    	}
    }
}
