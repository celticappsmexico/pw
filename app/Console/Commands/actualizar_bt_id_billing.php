<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use Carbon\Carbon;
use App\PaymentData;
use App\Storages;
use App\Billing;

class actualizar_bt_id_billing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:actualizar_bt_biling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $this->info('******************************');
        $this->info('Inicia - '. Carbon::now());

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);


        $collection = $gateway->subscription()->search([
            Braintree_SubscriptionSearch::planId()->is(env('PLAN_ID')),
            Braintree_SubscriptionSearch::status()->in(
                [
                    Braintree_Subscription::ACTIVE,
                    Braintree_Subscription::PAST_DUE,
                    Braintree_Subscription::PENDING
                ]
            )
          ]);
        
        //

        $contador = 0 ;
        foreach($collection as $subscription) {
            $contador++;
            $this->info('***************');
            $this->info('ID: '. $subscription->id);
            //$this->info('ALL: '. $subscription);
            $fecha = $subscription->createdAt;
        
            $date = date_format($fecha, 'Y-m-d');
            
            $this->info('Fecha de creacion: '. $date);

            $pd = PaymentData::where([
                'pd_suscription_id' => $subscription->id
            ])->first();

            if(!is_null($pd)){
                $user_id    = $pd->user_id;
                $storage_id = $pd->storage_id;

                $this->info("USER ID: ". $user_id);
                $this->info("STORAGE ID: ". $storage_id);

                Billing::where([
                    'user_id'       => $user_id,
                    'storage_id'    => $storage_id
                ])->where(
                    'pay_month' , '>=' , $date
                )->where(
                    'cargo'     , '>', '0'
                )->update([
                    'bi_braintree_suscription'  => $subscription->id
                ]);
                /*
                TEST
                if($subscription->id == "gpyh66"){
                    return false;
                }
                */

                /*
                    ItemTable::where('item_type_id', '=', 1)
                    ->update(['colour' => 'black']);
                            */
                /*
                            Billing::where([

                            ])
                */        
            }else{
                $this->info("PD NO EXISTE");

                //return false;

            }

              
            $this->info('***************');
        }

        $this->info('Total de Suscripciones:'.$contador);

        $this->info('Fin - '. Carbon::now());
        $this->info('******************************');
        
    }
}
