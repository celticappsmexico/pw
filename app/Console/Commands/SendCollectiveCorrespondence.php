<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Correspondences;
use App\User;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class SendCollectiveCorrespondence extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_correspondence';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Inicia Proceso: '.Carbon::now());
        
        $correspondence = Correspondences::where([
        			'co_status'	=> 'PENDING'
        		])->first();
        
        if(is_null($correspondence)){
        	$this->info('Sin correspondencias pendientes');	
        	return false;
        }
        
        $users = $correspondence->users_ids;
        
        $users = rtrim($users, ']');
        $users = ltrim($users, '[');
        
        $arrUsers = explode(',',$users);
        
        $i = 0;
        
        foreach($arrUsers as $user){
        	$this->info('***************');
        	
        	$user = rtrim($user, '"');
        	$user = ltrim($user, '"');
        	
        	$usuario = User::find($user);
        	
        	$email 	=	$usuario->email;
        	
        	if($usuario->userType_id == 1){
        	 isset($usuario->name) ? $userName = $usuario->name : $userName = '';
        	 isset($usuario->lastName) ? $userLast = $usuario->lastName : $userLast = '';
        	 	$nombre = $userName.' '.$userLast;
        	 }else{
        	 	isset($usuario->companyName) ? $userName = $usuario->companyName : $userName = '';
        	 	$nombre = $userName;
        	 }
        	
        	 
        	 $this->info('email: '. $email);
        	 $this->info('nombre: '. $nombre);
        	
            //enviar el correo por usuario
            
            if(env('ENVIRONMENT') == 'DEV'){
                $email = "oscar.sanchez@novacloud.mx";
            }
        	
        	 $emailData = [
		        	 'nombreDestinatario'    => $nombre,
		        	 'email'                 => $email,
		        	 'tituloCorreo'          => $correspondence->asunto,
		        	 'mensaje'               => $correspondence->corresp_mensaje
        	 ];
        	
        	 $resp = Mail::send ( 'emails.new_colective_correspondence_email', $emailData, function ($message) use ($emailData) {
        	  
        	 //$message->to ( 'biuro@przechowamy-wszystko.pl' );
        	 $message->to ( $emailData['email'] );
        	 //$message->cc ( 'biuro@przechowamy-wszystko.pl' );
        	  
        	 $message->from('no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
        	 $message->subject($emailData['tituloCorreo']);
        	
        	 });
        	 
        	 $i++;
        }
        
        $correspondence->co_status = 'SENT';
        $correspondence->save();
        
        
		$this->info('Correos enviados: '.$i);
        $this->info('Finaliza Proceso: '.Carbon::now());
        
    }
}
