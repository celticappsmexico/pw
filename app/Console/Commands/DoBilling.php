<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Http\Controllers\InvoiceController;
use App\Storages;
use App\CataPaymentTypes;
use App\User;
use App\Countries;
use App\ConfigSystem;
use DebugBar\Storage\StorageInterface;
use Illuminate\Support\Facades\Storage;
use App\Billing;
use App\BillingDetail;
use App\Library\MoneyString;
use App\InvoiceSequence;
use App\ReciboSequence;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use App\CardUser;
use App\notificaciones;
use App\Library\UtilNotificaciones;

class DoBilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_billing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		//
		$this->info('Error: Run new billing');
		return false;

    	$this->info(Carbon::now().' - Inicia Billing...');
    	     	
    	$start = Carbon::now()->startOfMonth();
    	
    	$this->info('DATE START :'. $start);
    	 
    	$storages = Storages::where(['flag_rent' => 1])->where('price_per_month' ,'<>', '0.00' )
    				//->whereIn('id',[1])
    				//->where(['user_id' => '35'])
    				//->where('id' ,'>', 270)
    				/*
    				->whereMonth('rent_start','<>',Carbon::now()->month)
    				->whereYear('rent_start','<>',Carbon::now()->month)*/
    				->where('rent_start' , '<', $start)
    				->get();
    	
    	$this->info('Count storages: '. count($storages));
    	
    	$dataSt = array();
    	
    	foreach ($storages as $st) {
    		 
    		$paymentData = $st->StoragePaymentData->first();
    		
    		if($st->rent_end > Carbon::now() || $st->rent_end == '0000-00-00' /*|| $st->price_per_month != "0.00"*/){
    			
    			//Valida meses de prepago
    			if($st->st_meses_prepago > 0){
    				//TIENE MESES DE PREPAGO.. NO GENERA BILLING
    				
    				//$st->st_meses_prepago = $st->st_meses_prepago - 1;
    				//$st->save();

    				$this->info('Storage ID '. $st->id . ' tiene meses de prepago, no genera billing.');
    				
    			} else{
    			
	    			$this->info('Storage ID '. $st->id);
	    	
	    			$this->info('payment data: '.$paymentData);
	    			    			
	    	
	    			if ($paymentData->creditcard) {
	    				$pt = CataPaymentTypes::find(2);
	    				$pt = $pt->cpt_lbl_factura;
	    			}else{
	    				$pt = CataPaymentTypes::find(3);
	    				$pt = $pt->cpt_lbl_factura;
	    			}
	    	
	    			$this->info('PT: '.$pt);
	    	
	    			$dataUser = User::with(['UserAddress'])->find($st->user_id);
	    	
	    			//return [$dataUser];
	    			// id user is a client, the name is the real name if not, name is a company name
	    			if($dataUser->userType_id == 1){
	    				isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
	    				isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
	    				$clientName = $userName.' '.$userLast;
	    			}else{
	    				//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
	    				isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
	    				$clientName = $userName;
	    			}
	    	
	    			$this->info('Client name: '. $clientName);
	    	
	    			// NIP
	    			$clientNip = $dataUser->nipNumber;
	    	
	    			$this->info('Nip number: '.$clientNip);
	    	
	    			// Address
	    			if($dataUser->UserAddress[0]->street != '' ){
	    				$this->info('if');
	    				
	    				$street = $dataUser->UserAddress[0]->street;
	    				$numExt = $dataUser->UserAddress[0]->number;
	    				$numInt = $dataUser->UserAddress[0]->apartmentNumber;
	    				$city = $dataUser->UserAddress[0]->city;
	    				$country = $dataUser->UserAddress[0]->country_id;    				
	    				$country = Countries::find($country);
	    				$country = $country->name;
	    				$cp = $dataUser->UserAddress[0]->postCode;

	    				if($numInt != "")
	    					$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
	    				else 
	    					$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
	    				
	    			}else{
	    				$this->info('else');
	    				$fullAddress = '';
	    			}
	    	
	    			$this->info('Full Address: '.$fullAddress);
	    	
	    			if ($paymentData != null) {
	    				 
	    				$activePay = $paymentData->id;
	    				 
	    				$this->info('Active Pay: '.$activePay);
	    				 
	    				$data = ConfigSystem::all();
	    				 
	    				//$ins = $data[0]->value;
	    				
	    				$ins = $paymentData->insurance;
	    				$vatTag = $paymentData->pd_vat + 1;
	    				$vat = $paymentData->pd_vat * 100;
	    					
	    				$placeDb = $data[2]->value;
	    				$addressDb = $data[3]->value;
	    				$nipDb = $data[4]->value;
	    				$bankDb = $data[5]->value;
	    				$accountDb = $data[6]->value;
	    				$invoiceNumberDb = $data[7]->value;
	    				$seller = $data[8]->value;
	    				 
	    				// increments invoice number:
	    				$invoiceNumberDb = (int)$invoiceNumberDb;
	    				//return [$invoiceNumberDb];
	    				$invoiceNumberDb = $invoiceNumberDb + 1;
	    				//return [$invoiceNumberDb];
	    				$newInvoiceNumberDb = sprintf( '%04d', $invoiceNumberDb );
	    				$data[7]->value = $newInvoiceNumberDb;
	    				$res = $data[7]->save();
	    				
	    				$reciboSequence = new ReciboSequence();
	    				$reciboSequence->save();
	    				
	    				$invoiceSecuence = new InvoiceSequence();
	    				$invoiceSecuence->is_contador =  $reciboSequence->id;
	    				$invoiceSecuence->save();
	    				 
	    				//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
	    				$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';    				
	    				$fakturaName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number;
	    				
	    				$this->info('PDF NAME: '.$pdfName);
	    				 
	    				//crear el pdf
	    				$ss = Storage::disk('invoices')->makeDirectory('1');
	    				//dd($ss);
	    				
	    				
	    				$this->info('******************');
	    				$this->info('*** Se convierte  dinero a letras');
	    				$this->info('*** Precio por mes: ' . $st->price_per_month);
	    				
	    				$totalString = MoneyString::transformQtyCurrency($st->price_per_month);
	    				
	    				$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
	    				
	    				$this->info('*** Precio por mes (letras): ' . $totalString);
	    				$this->info('*********FIN');
	    				
	    				//OBITIENE MES
	    				setlocale(LC_ALL, 'de_DE');
	    				
	    				$mes = "";
	    				
	    				switch(Carbon::now()->format('m'))
	    				{
	    					case '01':
	    						$mes = 'styczeń';
	    						break;
	    					case '02':
	    						$mes = 'luty';
	    						break;
	    					case '03':
	    						$mes = 'marzec';
	    						break;
	    					case '04':
	    						$mes = 'kwiecień';
	    						break;
	    					case '05':
	    						$mes = 'maj';
	    						break;
	    					case '06':
	    						$mes = 'czerwiec';
	    						break;
	    					case '07':
	    						$mes = 'lipiec';
	    						break;
	    					case '08':
	    						$mes = 'sierpień';
	    						break;
	    					case '09':
	    						$mes = 'wrzesień';
	    						break;
	    					case '10':
	    						$mes = 'październik';
	    						break;
	    					case '11':
	    						$mes = 'listopad';
	    						break;
	    					case '12':
	    						$mes = 'grudzień';
	    						break;
	    				
	    				}
	    				 
	    				$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
	    				
	    				$year = Carbon::now()->year;
	    				
	    				//CALCULO SUBTOTAL VAT 
	    				$subtotal = round( $st->price_per_month / $vatTag , 2 );
	    				$vatTotal = round( $st->price_per_month - $subtotal , 2 );
	    				
	    				$box = $subtotal - $ins;
	    				$vatInsurance = round($ins * ($vatTag - 1), 2);
	    				$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
	    				$insuranceBruto = round($ins + $vatInsurance, 2);
	    				
	    				$boxBruto = round( $box + $vatTotalInsurance,2);
	    				
	    				$this->info('***** Calculando totales');
	    				$this->info('Subtotal: '.$subtotal);
	    				$this->info('vat total: '.$vatTotal);    				
	    				$this->info('box: '. $box);
	    				$this->info('vatInsurance: '. $vatInsurance);
	    				$this->info('vatTotalInsurance: '. $vatTotalInsurance);
	    				$this->info('vat '. $vat );
	    				$this->info('***** ');
	    				//FIN DE CALCULO
	    				
	    				$reciboFiscal = str_pad($reciboSequence->id,6,"0",STR_PAD_LEFT);
	    				 
	    				$reciboFiscal = "W".$reciboFiscal;
	    				
	    				$this->info("Recibo Fiscal: ". $reciboFiscal);
	    				
	    				$data =  [
	    						'insurance' => $ins,
	    						'place' => $placeDb,
	    						'address' => $addressDb,
	    						'nip' 				=> $nipDb,
	    						'bank' 				=> $bankDb,
	    						'account' 			=> $accountDb,
	    						'invoiceNumber' 	=> $invoiceSecuence->id,
	    						'seller' 			=> $seller,
	    						'2y' 				=> date("y"),
	    						'currentDate' 		=> date("Y-m-d"),
	    						'paymentType' 		=> $pt,
	    						'clientName' 		=> $clientName,
	    						'clientAddress' 	=> $fullAddress,
	    						'clientNip' 		=> $clientNip,
	    						'clientPesel'		=> $dataUser->peselNumber,
	    						'clientType'		=> $dataUser->userType_id,
	    						'grandTotal' 		=> $st->price_per_month,
	    						'grandTotalString' => $totalString,
	    						'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
	    						'dataUser'			=> $dataUser,
	    						'mes'				=> $mes, 
	    						'year'				=> $year,
	    						'storage'			=> $st , 
	    						'vat'				=> $vat ,
	
	    						'box'				=> $box,
	    						'vatTotalInsurance' => $vatTotalInsurance,
	    						'subtotal'			=> $subtotal ,
	    						
	    						'vatTotal'			=> $vatTotal,
	    						'vatInsurance'		=> $vatInsurance , 
	    						
	    						'boxBruto'			=> $boxBruto,
	    						'insuranceBruto'	=> $insuranceBruto, 
	    						
	    						'accountant_number'	=> $dataUser->accountant_number,
	    						'reciboFiscal'		=> $reciboFiscal
	    						
	    				];
	    				
	    				 
	    				//$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
	    				
	    				//$pdf = \App::make('dompdf.wrapper');
	    				//$pdf->loadHTML($view);
	    				//$pdf->save(storage_path().'/app/invoices/'.$pdfName);
	    				 
	    				//
	    				$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $st->user_id, 'storage_id' => $st->id])->first();
	    				$this->info('Search for: user_id '.$st->user_id . ', storage_id : '.$st->id);
	    				 
	    				if(!is_null($billingUpdate))
	    				{
	    					$saldo = $billingUpdate->saldo;
	    					$billingUpdate->bi_flag_last = 0;
	    					$billingUpdate->save();
	    				}
	    				else{
	    					$saldo = 0;
	    				}
	    				//
	    				
	    				//if($dataUser->userType_id == 3){
	    					//$status_impresion = "NO APLICA";
	    				//}
	    				//else{
	    					$status_impresion = "PENDIENTE PDF";
	    				//}
	    				 
	    				$billing = new Billing();
	    				$billing->user_id 				= $st->user_id;
	    				$billing->storage_id 			= $st->id;
	    				$billing->pay_month 			= Carbon::now()->format("Y-m-d");
	    				$billing->flag_payed 			= 0;
	    				$billing->abono 				= 0;
	    				$billing->cargo 				= $st->price_per_month;
	    				$billing->saldo 				= ($st->price_per_month * -1) + $saldo;
	    				$billing->payment_data_id		= $paymentData->id;
	    				//$billing->pdf 					= $pdfName;
	    				$billing->reference 			= '';
	    				$billing->bi_flag_last 			= 1;
	    				$billing->bi_subtotal 			= $subtotal;
	    				$billing->bi_porcentaje_vat 	= $vat;
	    				$billing->bi_total_vat 			= $vatTotal;
	    				$billing->bi_total 				= $st->price_per_month;
	    				$billing->bi_status_impresion 	= $status_impresion;
	    				$billing->bi_recibo_fiscal 		= $reciboSequence->id;
	    				$billing->bi_number_invoice		= $invoiceSecuence->id;
	    				$billing->bi_year_invoice		= date("y");
	    				$billing->bi_batch 				= "1"; //FLAG PARA IDENTIFICAR QUE ES UN REGISTRO DESDE BATCH BILLING
	    				
	    				$billing->save();
	    				
	    				//GENERA BILLING DETAIL
	    				//BOX
	    				
	    				$billingDetail = new BillingDetail();
	    				$billingDetail->billing_id			= $billing->id;
	    				$billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$st->sqm ."m2";
	    				$billingDetail->bd_codigo			= "box ".$st->alias;
	    				$billingDetail->bd_numero			= "1";
	    				$billingDetail->bd_valor_neto		= $box;
	    				$billingDetail->bd_porcentaje_vat 	= $vat;
	    				$billingDetail->bd_total_vat		= $vatTotalInsurance;
	    				$billingDetail->bd_total			= $boxBruto;
	    				$billingDetail->bd_tipo_partida		= "BOX";    				
	    				$billingDetail->save();
	    				
	    				//INSURANCE
	    				if($ins > 0){
	    					$billingDetail = new BillingDetail();
	    					$billingDetail->billing_id			= $billing->id;
	    					$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
	    					$billingDetail->bd_codigo			= "ubezpieczenie";
	    					$billingDetail->bd_numero			= "1";
	    					$billingDetail->bd_valor_neto		= $ins;
	    					$billingDetail->bd_porcentaje_vat 	= $vat;
	    					$billingDetail->bd_total_vat		= $vatInsurance;
	    					$billingDetail->bd_total			= $insuranceBruto;
	    					$billingDetail->bd_tipo_partida		= "INSURANCE";
	    					$billingDetail->save();
	    				}
	    					
	    				$this->info('New Billing id: '.$billing->id);

	    				$mensaje = "Your billing of the box #".$st->alias." has been generated successfully.";
			            //UtilNotificaciones::EnviarNotice($mensaje, $st->user_id);    

			            $notificacion = new notificaciones();
			            $notificacion->user_id = $st->user_id;
			            $notificacion->storage_id = $st->id;
			            $notificacion->notf_mensaje = $mensaje;
			            $notificacion->save();
	    				
	    				$this->info('Correo del cliente al que se debe enviar: '. $dataUser->email);
	    				
	    				$email2 = $dataUser->email2;
	    				
	    				if($email2 == "")
	    					$email2 = $dataUser->email;
	    				
	    				$emailData = [
	    						'nombreDestinatario' 	=> $clientName,
	    						/*
	    						 PROD 
	    						
	    						'email' 				=> $dataUser->email,
	    						'email2' 				=> $email2,
	    						*/ 
	    						 
	    						/*
	    						 DEV
	    						* */ 
	    						'email' 				=> 'abraham.hernandez@gmail.com',
	    						'email2' 				=> 'abraham.hernandez@gmail.com',
	    						
	    						
	    						'tituloCorreo'			=> 'Faktura',
	    						'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
	    						'nombrePDF'				=> $pdfName,
	    						'period'				=> $mes,
	    						'fakturaName'			=> $fakturaName
	    				];
	    				/*
	    				
	    				$resp = Mail::send ( 'emails.new_billing_email', $emailData, function ($message) use ($emailData) {
	    					$message->to ( $emailData['email'] );
	    					$message->cc ( $emailData['email2'] );
	    					
	    					//$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
	    					//$message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl'] );
	    					
	    					$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
	    					$message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
	    					$message->attach ( $emailData ['pathPDF'], array (
	    							'as' => $emailData ['nombrePDF'],
	    							'mime' => 'application/pdf'
	    					) );
	    				} );
	    				
	    				$this->info('Se envia correo a la direccion: '. $emailData['email']);
	    				*/
	    				$this->info('Finaliza .. queda pendiente de pdf');
	    				
	    				
	    			}
	    		} //END ELSE
    			 
    		} //END IF
    		
    		$this->info('***********************');
    		
    	}
    	
    	///RESET STORAGES QUE TENGAN FECHA MENOR A HOY POR TERM NOTICE
    	/*
    	Storages::where('rent_end' , '<' ,Carbon::now())
    				->where('rent_end' , '<>', '0000-00-00')
    				->where(['active' => 1, 'flag_rent' => 1])
    				->update([
    						'flag_rent'			=> 0 ,
    						'user_id'			=> 0 ,
    						'payment_data_id'	=> 0 ,
    						'final_price'		=> 0 ,
    						'rent_start'		=> '0000-00-00',
    						'rent_end'			=> '0000-00-00',
    						'date_subscribe_braintree' => '0000-00-00',
    						'price_per_month'	=> 0,
    						'st_meses_prepago'	=> 0
    				]);
    
    	 */  	
    	$this->info('End of billing....');
    	 
    	 
    }
}
