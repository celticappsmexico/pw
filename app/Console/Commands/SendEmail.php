<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Billing;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        
    	$billing = Billing::where('id','>','185')
    						->where('pdf','<>','')
    						->get();
        
    	
    	foreach($billing as $bill){
    		
    		$dataUser = User::with(['UserAddress'])->find($bill->user_id);
    		
    		if($dataUser->userType_id == 1){
    			isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
    			isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
    			$clientName = $userName.' '.$userLast;
    		}else{
    			//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
    			isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
    			$clientName = $userName;
    		}
    		
    		$email2 = $dataUser->email2;
    		
    		if($email2 == "")
    			$email2 = $dataUser->email;
    		
    		$mes = "";
    		
    		switch(Carbon::now()->format('m'))
    		{
    			case '01':
    				$mes = 'styczeń';
    				break;
    			case '02':
    				$mes = 'luty';
    				break;
    			case '03':
    				$mes = 'marzec';
    				break;
    			case '04':
    				$mes = 'kwiecień';
    				break;
    			case '05':
    				$mes = 'maj';
    				break;
    			case '06':
    				$mes = 'czerwiec';
    				break;
    			case '07':
    				$mes = 'lipiec';
    				break;
    			case '08':
    				$mes = 'sierpień';
    				break;
    			case '09':
    				$mes = 'wrzesień';
    				break;
    			case '10':
    				$mes = 'październik';
    				break;
    			case '11':
    				$mes = 'listopad';
    				break;
    			case '12':
    				$mes = 'grudzień';
    				break;
    					
    		}
    			
    		$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    		
    		$this->info('mes: '.$mes);
    		$this->info('email2: '.$email2);
    		
    		$pdfName = $bill->pdf;
    		$fakturaName = date("y")."-PW-".$bill->bi_number_invoice.'_'.$dataUser->accountant_number;

    		$this->info('pdf: '.$pdfName);
    		$this->info('faktura: '.$fakturaName);
    		
    		
    		if(env('ENVIRONMENT') == 'DEV'){
    			
    			$this->info('DEV');
    			
    			$emailData = [
    					'nombreDestinatario' 	=> $clientName,
    					/* DEV */
    					'email' 				=> 'oscar.sanchez@novacloud.mx',
    					'email2' 				=> 'abraham.hernandez@gmail.com',
    					'tituloCorreo'			=> 'Faktura',
    					'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
    					'nombrePDF'				=> $pdfName,
    					'period'				=> $mes,
    					'fakturaName'			=> $fakturaName
    			];
    			 
    			//ENVIO DE CORREO
    			$resp = Mail::send ( 'emails.new_billing_email', $emailData, function ($message) use ($emailData) {
    				$message->to ( $emailData['email'] );
    				$message->cc ( $emailData['email2'] );
    				 
    				//DEV
    				$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
    				$message->bcc ( ['myss.latam@gmail.com'] );
    				//
    		
    				$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
    				$message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
    				$message->attach ( $emailData ['pathPDF'], array (
    						'as' => $emailData ['nombrePDF'],
    						'mime' => 'application/pdf'
    				) );
    			} );
    		}
    		else{
    			//PROD
    			$this->info('PROD');
    			
    			$emailData = [
    					'nombreDestinatario' 	=> $clientName,
    					/* PROD */
    					'email' 				=> $dataUser->email,
    					'email2' 				=> $email2,
    						
    					/* DEV
    					 'email' 				=> 'oscar.sanchez@novacloud.mx',
    			'email2' 				=> 'abraham.hernandez@gmail.com',
    			*/
    					'tituloCorreo'			=> 'Faktura',
    					'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
    					'nombrePDF'				=> $pdfName,
    					'period'				=> $mes,
    					'fakturaName'			=> $fakturaName
    			];
    				
    			 
    			//ENVIO DE CORREO
    			$resp = Mail::send ( 'emails.new_billing_email', $emailData, function ($message) use ($emailData) {
    				$message->to ( $emailData['email'] );
    				$message->cc ( $emailData['email2'] );
    				 
    				//PROD
    				$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
    				$message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl','jagiello@jsg-cs.com.pl'] );
    				//
    					
    				$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
    				$message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
    				$message->attach ( $emailData ['pathPDF'], array (
    						'as' => $emailData ['nombrePDF'],
    						'mime' => 'application/pdf'
    				) );
    			} );
    		}
    		
    	}
    	
    }
}
