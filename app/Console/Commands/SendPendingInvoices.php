<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Billing;
use App\History;
use App\BillingDetail;
use Illuminate\Support\Facades\Log;
use App\ReciboSequence;
use Illuminate\Support\Facades\DB;
use App\Library\EstimatorHelper;
use App\Library\BraintreeHelper;
use App\Library\Formato;
use App\Storages;
use App\ConfigSystem;
use App\ExtraItems;
use App\CataPaymentTypes;
use App\Library\MoneyString;
use App\User;
use App\Countries;
use App\InvoiceSequence;
use App\ParagonSequence;
use DebugBar\Storage\StorageInterface;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use App\DetOrders;
use App\Orders;
use App\notificaciones;
use App\CataTerm;
use App\clientAddress;
use App\LegalRepresentative;
use App\CataPrepay;
use App\Http\Fecha;
use App\CardUser;
use App\App;
use App\Library\UtilNotificaciones;
use App\CataSecciones;
use App\Library\BillingHelper;


class SendPendingInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_pending_invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('*******************************************');
        $this->info('Start: '. Carbon::now());


        $billing = Billing::with(['storages','user','billingDetail'])
    		->where(['bi_status_impresion' => 'PENDIENTE PDF'])
    		->where('cargo','>','0')
            ->first();
        
        if(is_null($billing)){
            
            $this->info('No hay billing pendiente');
            
        }else{

    	
            $this->info('BillingID: '.$billing->id);
            $recibo_fiscal = -1;

            $rentStartBilling = $billing->pay_month;
		
            $flagPaid = $billing->flag_payed; 
            
            //NUMERO QUE SE COLOCA EN EL PDF
            $reciboFiscal = str_pad($recibo_fiscal,6,"0",STR_PAD_LEFT);
            $reciboFiscal = "W".$reciboFiscal;
            
            
            if($recibo_fiscal == -1){
                $reciboFiscal = "-1";
                
                $this->info('Recibo fiscal -1');
                
            }
            
            $paragon = $billing->bi_paragon;
            
            $encabezado = "";
            if($paragon == 1)
                $encabezado = "PAR";
            else
                $encabezado = "PW";
                
            $this->info("Recibo Fiscal: ". $reciboFiscal);
            
            $dataUser = User::with(['UserAddress'])->find($billing->user_id);
            
            // id user is a client, the name is the real name if not, name is a company name
            if($dataUser->userType_id == 1){
                isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
                isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
                $clientName = $userName.' '.$userLast;
            }else{
                //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
                isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
                $clientName = $userName;
            }

            if($clientName == ""){
                //FIX POR SI ESTA MAL EL TIPO DE CLIENTE
                isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
                isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
                $clientName = $userName.' '.$userLast;
            }
            
            $this->info('ClientName: '.$clientName);
            
            //NIP
            $clientNip = $dataUser->nipNumber;
                
            // Address
            if($dataUser->UserAddress[0]->street != '' ){
                    
                $street = $dataUser->UserAddress[0]->street;
                $numExt = $dataUser->UserAddress[0]->number;
                $numInt = $dataUser->UserAddress[0]->apartmentNumber;
                $city = $dataUser->UserAddress[0]->city;
                $country = $dataUser->UserAddress[0]->country_id;
                $country = Countries::find($country);
                $country = $country->name;
                $cp = $dataUser->UserAddress[0]->postCode;
                    
                if($numInt != "")
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
                else
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
                    
            }else{
                $fullAddress = '';
            }
            
            //GENERA PDF
            //VALIDA SI ES POR BATCH O ESTIMATOR
            $st = Storages::find($billing->storage_id);
            
            $paymentData = $st->StoragePaymentData->first();
            

            //PAYMENT DATA INFO
            if ($paymentData->creditcard) {
                $pt = CataPaymentTypes::find(2);
                $pt = $pt->cpt_lbl_factura;
            }else{
                $pt = CataPaymentTypes::find(3);
                $pt = $pt->cpt_lbl_factura;
            }
            
            //DATA SYSTEM CONFIG
            $data = ConfigSystem::all();
            //$ins = $data[0]->value;
                
            $ins = $paymentData->insurance;
            $vatTag = $paymentData->pd_vat + 1;
            $vat = $paymentData->pd_vat * 100;
            
            //			
            $activePay = $paymentData->id;
            
            $vatDb = $data[1]->value;
                
            $data = ConfigSystem::all();
            //		
            $placeDb = $data[2]->value;
            $addressDb = $data[3]->value;
            $nipDb = $data[4]->value;
            $bankDb = $data[5]->value;
            $accountDb = $data[6]->value;
            $seller = $data[8]->value;
            //END DATA SYSTEM CONFIG
            

            //OBITIENE MES
            setlocale(LC_ALL, 'de_DE');
            $mes = "";
            
            switch(Carbon::now()->format('m'))
            {
                case '01':
                    $mes = 'styczeń';
                    break;
                case '02':
                    $mes = 'luty';
                    break;
                case '03':
                    $mes = 'marzec';
                    break;
                case '04':
                    $mes = 'kwiecień';
                    break;
                case '05':
                    $mes = 'maj';
                    break;
                case '06':
                    $mes = 'czerwiec';
                    break;
                case '07':
                    $mes = 'lipiec';
                    break;
                case '08':
                    $mes = 'sierpień';
                    break;
                case '09':
                    $mes = 'wrzesień';
                    break;
                case '10':
                    $mes = 'październik';
                    break;
                case '11':
                    $mes = 'listopad';
                    break;
                case '12':
                    $mes = 'grudzień';
                    break;
                        
            }

                
            $mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
            
            $year = Carbon::now()->year;
            
            if($billing->bi_batch == 1){
                //BATCH
        
                if ($paymentData != null) {
                
                    //$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
                    if($paragon == 1){
                        $pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                        $fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                    }else{
                        $pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                        $fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                    }
                            
                    $this->info('PDF NAME: '.$pdfName);
                    
                    //ACTUALIZA NOMBRE DEL PDF
                    $billing->pdf  = $pdfName;
                    $billing->save();
                
                    //crear el pdf
                    $ss = Storage::disk('invoices')->makeDirectory('1');
                            
                    $totalString = MoneyString::transformQtyCurrency($st->price_per_month);
                            
                    $totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');

                    $this->info('Total String: '.$totalString);
                            
                    //CALUCLO D SUBTOTAL VAT
                    $subtotal = round( $st->price_per_month / $vatTag , 2 );
                    $vatTotal = round( $st->price_per_month - $subtotal , 2 );
                            
                    $box = $subtotal - $ins;
                    $vatInsurance = round($ins * ($vatTag - 1), 2);
                    $vatTotalInsurance = round($vatTotal - $vatInsurance,2);
                    $insuranceBruto = round($ins + $vatInsurance, 2);
                        
                    $boxBruto = round( $box + $vatTotalInsurance,2);
                            
                    $this->info('***** Calculando totales');
                    $this->info('Subtotal: '.$subtotal);
                    $this->info('vat total: '.$vatTotal);
                    $this->info('box: '. $box);
                    $this->info('vatInsurance: '. $vatInsurance);
                    $this->info('vatTotalInsurance: '. $vatTotalInsurance);
                    $this->info('vat '. $vat );
                    $this->info('***** ');
                    //FIN DE CALCULO
                            
                    $this->info("RFISCAL: ".$reciboFiscal);
                        $data =  [
                                'insurance' => $ins,
                                'place' => $placeDb,
                                'address' => $addressDb,
                                'nip' 				=> $nipDb,
                                'bank' 				=> $bankDb,
                                'account' 			=> $accountDb,
                                'invoiceNumber' 	=> $billing->bi_number_invoice,
                                'seller' 			=> $seller,
                                '2y' 				=> $billing->bi_year_invoice,
                                'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                                'paymentType' 		=> $pt,
                                'clientName' 		=> $clientName,
                                'clientAddress' 	=> $fullAddress,
                                'clientNip' 		=> $clientNip,
                                'clientPesel'		=> $dataUser->peselNumber,
                                'clientType'		=> $dataUser->userType_id,
                                'grandTotal' 		=> $st->price_per_month,
                                'grandTotalString'  => $totalString,
                                'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                                'dataUser'			=> $dataUser,
                                'mes'				=> $mes,
                                'year'				=> $year,
                                'storage'			=> $st ,
                                'vat'				=> $vat ,
                                'box'				=> $box,
                                'vatTotalInsurance' => $vatTotalInsurance,
                                'subtotal'			=> $subtotal ,
                        
                                'vatTotal'			=> $vatTotal,
                                'vatInsurance'		=> $vatInsurance ,
                        
                                'boxBruto'			=> $boxBruto,
                                'insuranceBruto'	=> $insuranceBruto,
                    
                                'accountant_number'	=> $dataUser->accountant_number,
                                'reciboFiscal'		=> $reciboFiscal,
                                'flag_payed'		=> $flagPaid,
                                'paragon'			=> $paragon,
                                'encabezado'		=> $encabezado,
                                'container'			=> $st->st_es_contenedor
                        ];
                            
                
                        $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                            
                        $pdf = \App::make('dompdf.wrapper');
                        $pdf->loadHTML($view);
                        $pdf->save(storage_path().'/app/invoices/'.$pdfName);
                        
                        //GENERA COPIA
                        $this->info('Inicia generacion de copia ');
                        
                        if($paragon == 1){
                            $pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        }else{
                            $pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        }
                        //	$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        
                            
                        $data =  [
                                'insurance' => $ins,
                                'place' => $placeDb,
                                'address' => $addressDb,
                                'nip' 				=> $nipDb,
                                'bank' 				=> $bankDb,
                                'account' 			=> $accountDb,
                                'invoiceNumber' 	=> $billing->bi_number_invoice,
                                'seller' 			=> $seller,
                                '2y' 				=> $billing->bi_year_invoice,
                                'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                                'paymentType' 		=> $pt,
                                'clientName' 		=> $clientName,
                                'clientAddress' 	=> $fullAddress,
                                'clientNip' 		=> $clientNip,
                                'clientPesel'		=> $dataUser->peselNumber,
                                'clientType'		=> $dataUser->userType_id,
                                'grandTotal' 		=> $st->price_per_month,
                                'grandTotalString'  => $totalString,
                                'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                                'dataUser'			=> $dataUser,
                                'mes'				=> $mes,
                                'year'				=> $year,
                                'storage'			=> $st ,
                                'vat'				=> $vat ,
                                'box'				=> $box,
                                'vatTotalInsurance' => $vatTotalInsurance,
                                'subtotal'			=> $subtotal ,
                        
                                'vatTotal'			=> $vatTotal,
                                'vatInsurance'		=> $vatInsurance ,
                        
                                'boxBruto'			=> $boxBruto,
                                'insuranceBruto'	=> $insuranceBruto,
                    
                                'accountant_number'	=> $dataUser->accountant_number,
                                'reciboFiscal'		=> $reciboFiscal,
                                'flag_payed'		=> $flagPaid,
                                'copy'				=> 1,
                                'paragon'			=> $paragon,
                                'encabezado'		=> $encabezado,
                                'container'			=> $st->st_es_contenedor
                        ];
                            
                        $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                        $pdf = \App::make('dompdf.wrapper');
                        $pdf->loadHTML($view);
                            
                        $pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
                        
                        $billing->bi_status_impresion = 'TERMINADO';
                        $billing->save();
                            
                            
                }
                //FIN DE BATCH
            }
            else{
                //ESTIMATOR
                
                if($billing->bi_flag_prepay == 0){
                    
                        $paymentData = PaymentData::find($billing->payment_data_id);
                        
                        $orderId = $paymentData->order_id;
                        $paymentType = $paymentData->payment_type_id;
                        
                        $this->info('OrderID: ' . $orderId );
                        
                        $items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
                        $order = Orders::where('id','=',$orderId)->first();
                        $userId = $order->user_id;
                        //return [$dataUser];
                        // id user is a client, the name is the real name if not, name is a company name
                        
                        // NIP
                        $clientNip = $dataUser->nipNumber;
                        
                        $subtotal = 0;
                        
                        //return ['items' => $items];
                        $idStorage = 0;
                        
                        foreach ($items as $key => $item) {
                            if($item->product_type == 'box'){
                                $this->info('IF');
                                
                                $idStorage = $item->product_id;
                                $desc3 = 0;
                                $nextMonth = 0;
                        
                                $this->info('idStorage: '.$idStorage);
                        
                                $term = $item->paymentData[0]->term;
                                $term = CataTerm::where('id','=',$term)->first();
                                $desc1 = ($term->off / 100);
                                $term = $term->months;
                                //return [$term,$desc1];
                                
                                $prepayMonths = $item->paymentData[0]->prepay;
                                $prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
                                $desc2 = ($prepayMonths->off / 100);
                                $prepayMonths = $prepayMonths->months;
                        
                                $this->info('desc2: '.$desc2);
                                $this->info('prepayMonths: '.$prepayMonths);
                        
                                //return [$desc2,$prepayMonths];
                        
                                $cc = $item->paymentData[0]->creditcard;
                                if ($cc == 1) {
                                    $ext = 1;
                                }else{
                                    $ext = ($item->paymentData[0]->ext /100 ) + 1;
                                }
                        
                                //return [$ext];
                        
                                $ins = $item->paymentData[0]->insurance;
                        
                                $this->info('Ins. '.$ins);
                        
                                //return [$ins];
                        
                                $fecha = $item->rent_starts;
                                $price = $item->price;
                                $utilFecha = new Fecha();
                                $lastDay = $utilFecha->getUltimoDiaMes($fecha);
                                $lastDay = $utilFecha->getDia($lastDay);
                                //return [$lastDay];
                                $currentDay = $utilFecha->getDia($fecha);
                                //return [$lastDay,$currentDay];
                                $daysCurrent = $lastDay-$currentDay;
                                $pricePerDay = $price / $lastDay;
                                //return [$fecha,$lastDay,$daysCurrent,$pricePerDay];
                                $totalCurrentMonth = ($daysCurrent + 1) * $pricePerDay;
                                //return [$daysCurrent,$price,$pricePerDay,$totalCurrentMonth];
                        
                                //calculo del current insurance
                                $insurencePerDay =  $ins / $lastDay;
                                $currentInsurence = $insurencePerDay * ($daysCurrent + 1);
                        
                                $this->info('insurencePerDay: '.$insurencePerDay);
                                $this->info('currentInsurence: '.$currentInsurence);
                        
                                //caclculo primer descuento:
                                $descTermCurrent = $totalCurrentMonth - ($totalCurrentMonth * $desc1);
                                $descTermNext = $price - ($price * $desc1);
                                //return [$descTermCurrent,$descTermNext];
                        
                                $this->info('descTermCurrent: '.$descTermCurrent);
                                $this->info('descTermNext: '.$descTermNext);
                        
                                //calculo segundo desc
                                $descPrepayCurrent = $descTermCurrent - ($descTermCurrent * $desc2);
                                $descPrepayNext = $descTermNext - ($descTermNext * $desc2);
                                //return [$descPrepayCurrent,$descPrepayNext];
                        
                                //calculo tercer desc
                                $descCCCurrent = $descPrepayCurrent * $ext;
                                $descCCNext = $descPrepayNext * $ext;
                                //return [$descCCCurrent,$descCCNext];
                                $this->info('$descCCCurrent: '  . $descCCCurrent);
                                $this->info('$descCCNext: '. $descCCNext);
                                    
                                //Mes extra antes de impuestos
                                //$deposit = $descCCCurrent + $descCCNext;
                                $deposit = $descCCNext;
                                $this->info('Deposit1: '.$deposit);
                                //return [$deposit];
                        
                                //sumamos el seguro
                                //$deposit = $deposit + $ins + $currentInsurence;
                                //$this->info('Deposit2: '.$deposit);
                                //return [$deposit];
                        
                                // calculamos los mese de prepago
                                $prepayTotal = ($descCCNext * $prepayMonths) + ($ins * $prepayMonths);
                                $this->info('PrepayTotal'. $prepayTotal);
                                //return [$prepayTotal];
                        
                                // calculamos el subtotal
                                //QUITAMOS EL DEPOSIT
                                $total = $prepayTotal; //+ $deposit;
                                $this->info('PrepayTotal'. $total);
                                //return [$total];
                        
                                //sacamos el precio total del next
                                $subtotalNextMonth = $descCCNext + $ins;
                                $vatNextMonth = $subtotalNextMonth * $vat;
                                $totalNextMonth = $subtotalNextMonth + $vatNextMonth;
                                //return [$totalNextMonth];
                        
                                $subtotal += $total;
                    
                        
                            }else{
                                //Entonces es un item
                                $price = $item->price;
                                $qt = $item->quantity;
                                $art = $item->articleData[0]->nombre;
                                $total = $price * $qt;
                        
                                //return ['Art' => $art,'Price' => $price, 'Quantity' => $qt,'Total' => $total];
                                $subtotal += $total;
                            }
                        }
                        
                        // El subtotal y el vat se calcula junto con los demas articulos.
                        $vat = $subtotal * $vat;
                        $granTotal = $subtotal + $vat;
                        //return ['Subtotal' => $subtotal,'vat' => $vat,'Total' => $granTotal];
                        
                        $this->info('***** Info recibida...');
                        $this->info('Subtotal: '. $subtotal);
                        $this->info('Vat: '.$vat);
                        $this->info('granTotal: '. $granTotal);
                        
                        //Generamos el nombre del pdf
                        if($paragon == 1){
                            $pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                            $fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                        }else{
                            $pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                            $fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                        }
                        
                        //CALUCLO D SUBTOTAL VAT
                        $this->info('***** Calculando totales');
                        
                        $paymentData = $st->StoragePaymentData->first();
                        
                        $ins = $paymentData->insurance;
                        $vatTag = $paymentData->pd_vat + 1;
                        $vat = $paymentData->pd_vat * 100;
                        
                        $this->info('Paymentdata ID: '.$paymentData->id);
                        $this->info('ins: '.$ins);
                        $this->info('vatTag: '.$vatTag);
                        $this->info('vat: '.$vat);
                        
                        
                        //$subtotal = round( $st->price_per_month / $vatTag , 2 );
                        //$vatTotal = round( $st->price_per_month - $subtotal , 2 );
                        
                        $subtotal = 0;
                        $vatTotal = 0;
                        
                        
                        $box = $subtotal - $ins;
                        $vatInsurance = round($ins * ($vatTag - 1), 2);
                        //$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
                        $vatTotalInsurance = 0;
                        
                        $insuranceBruto = round($ins + $vatInsurance, 2);
                        
                        $boxBruto = round( $box + $vatTotalInsurance,2);
                        
                        $this->info('Subtotal: '.$subtotal);
                        $this->info('vat total: '.$vatTotal);
                        $this->info('box: '. $box);
                        $this->info('vatInsurance: '. $vatInsurance);
                        $this->info('vatTotalInsurance: '. $vatTotalInsurance);
                        $this->info('vat '. $vat );
                        $this->info('***** ');
                        //FIN DE CALCULO
                        
                        $billing->pdf = $pdfName;
                        $billing->bi_status_impresion = "TERMINADO";
                        $billing->save();
                        
                                    
                        // generar el pdf
                        //$data = $this->getData();
                        $ss = Storage::disk('invoices')->makeDirectory('1');
                        //dd($ss);
                        
                        $totalString = MoneyString::transformQtyCurrency($paymentData->pd_total);
                        
                        $totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
                                    
                        
                        //dd.mm-dd.mm.yyyy
                        //$startPeriod = $rentStartBilling;
                        
                        //RENT START
                        $startPeriod = $st->rent_start;

                        $startPeriod = Carbon::parse($startPeriod)->toDateString();
                        $this->info('Start period invoice: '. $startPeriod);
                        
                        $arrStartPeriod = explode('-', $startPeriod);
                        
                        $endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->toDateString();

                        $this->info('End period invoice: '. $endPeriod);

                        $arrEndPeriod = explode('-',$endPeriod);
                        
                        $period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
                        
                        $this->info('Period: '.$period);
                        $year = $arrEndPeriod[0];
                        
                        switch($arrEndPeriod[1])
                        {
                            case '01':
                                $mes = 'styczeń';
                                break;
                            case '02':
                                $mes = 'luty';
                                break;
                            case '03':
                                $mes = 'marzec';
                                break;
                            case '04':
                                $mes = 'kwiecień';
                                break;
                            case '05':
                                $mes = 'maj';
                                break;
                            case '06':
                                $mes = 'czerwiec';
                                break;
                            case '07':
                                $mes = 'lipiec';
                                break;
                            case '08':
                                $mes = 'sierpień';
                                break;
                            case '09':
                                $mes = 'wrzesień';
                                break;
                            case '10':
                                $mes = 'październik';
                                break;
                            case '11':
                                $mes = 'listopad';
                                break;
                            case '12':
                                $mes = 'grudzień';
                                break;
                                    
                        }
                            
                        $mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
                        //	
                        
                        $data =  [
                                'insurance' => $paymentData->pd_insurance_price,
                                'vat' => $vat,
                                'vatDb' => $vatDb,
                                'place' => $placeDb,
                                'address' => $addressDb,
                                'nip' => $nipDb,
                                'bank' => $bankDb,
                                'account' => $accountDb,
                                'invoiceNumber' => $billing->bi_number_invoice,
                                'seller' => $seller,
                                '2y' => $billing->bi_year_invoice,
                                'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                                'clientName' => $clientName,
                                'clientAddress' => $fullAddress,
                                'clientNip' 		=> $clientNip,
                                'clientPesel'		=> $dataUser->peselNumber,
                                'clientType'		=> $dataUser->userType_id,
                                'grandTotal' 		=> $paymentData->pd_total,
                                'grandTotalString'	=> $totalString,
                                'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                                'mes'				=> $mes,
                                'year'				=> $year,
                                'storage'			=> $st    ,
                                'paymentType' 		=> $pt,	
                                'dataUser'			=> $dataUser,
                                'vat'				=> $vat ,
                        
                                'box'				=> $paymentData->pd_box_price,
                                'vatTotalInsurance' => $paymentData->pd_box_vat,
                                'subtotal'			=> $paymentData->pd_subtotal,
                                
                                'vatTotal'			=> $paymentData->pd_total_vat,
                                'vatInsurance'		=> $paymentData->pd_insurance_vat,
                                
                                'boxBruto'			=> $paymentData->pd_box_total,
                                'insuranceBruto'	=> $paymentData->pd_insurance_total,
                                
                                'accountant_number'	=> $dataUser->accountant_number,
                                'reciboFiscal'		=> $reciboFiscal,
                        
                                'period'			=> $period,
                                'flag_payed'		=> $flagPaid,
                                'paragon'			=> $paragon,
                                'encabezado'		=> $encabezado,
                                'container'			=> $st->st_es_contenedor
                        ];
                        
                        $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                        $pdf = \App::make('dompdf.wrapper');
                        $pdf->loadHTML($view);
                        
                        $pdf->save(storage_path().'/app/invoices/'.$pdfName);
                        
                        
                        //GENERA COPIA
                        $this->info('Inicia generacion de copia ');
                        
                        if($paragon == 1){
                            $pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        }else{
                            $pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        }
                        //$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        
                        
                        $this->info('$billing->user_id: '.$billing->user_id );
                        $this->info('$dataUser->userType_id: '. $dataUser->userType_id);
                        
                        $data =  [
                                'insurance' => $paymentData->pd_insurance_price,
                                'vat' => $vat,
                                'vatDb' => $vatDb,
                                'place' => $placeDb,
                                'address' => $addressDb,
                                'nip' => $nipDb,
                                'bank' => $bankDb,
                                'account' => $accountDb,
                                'invoiceNumber' => $billing->bi_number_invoice,
                                'seller' => $seller,
                                '2y' => $billing->bi_year_invoice,
                                'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                                'clientName' => $clientName,
                                'clientAddress' => $fullAddress,
                                'clientNip' 		=> $clientNip,
                                'clientPesel'		=> $dataUser->peselNumber,
                                'clientType'		=> $dataUser->userType_id,
                                'grandTotal' 		=> $paymentData->pd_total,
                                'grandTotalString'	=> $totalString,
                                'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                                'mes'				=> $mes,
                                'year'				=> $year,
                                'storage'			=> $st   ,
                                'paymentType' 		=> $pt,
                                'dataUser'			=> $dataUser,
                                'vat'				=> $vat ,
                        
                                'box'				=> $paymentData->pd_box_price,
                                'vatTotalInsurance' => $paymentData->pd_box_vat,
                                'subtotal'			=> $paymentData->pd_subtotal,
                                
                                'vatTotal'			=> $paymentData->pd_total_vat,
                                'vatInsurance'		=> $paymentData->pd_insurance_vat,
                                
                                'boxBruto'			=> $paymentData->pd_box_total,
                                'insuranceBruto'	=> $paymentData->pd_insurance_total,
                                
                                'accountant_number'	=> $dataUser->accountant_number,
                                'reciboFiscal'		=> $reciboFiscal,
                        
                                'period'			=> $period,
                                'flag_payed'		=> $flagPaid,
                                'copy'				=> 1,
                                'paragon'			=> $paragon,
                                'encabezado'		=> $encabezado,
                                'container'			=> $st->st_es_contenedor
                        ];
                        
                        $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                        $pdf = \App::make('dompdf.wrapper');
                        $pdf->loadHTML($view);
                        
                        $pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
                        
                        //$this->info('Fin de genera copia');
                        
                        //FIN DE GENERA COPIA
                        //return ['success' => true, 'pdf' => $pdfName];
                        
                        //return $pdfName;

                }else{
                    ///ES UNA FACTURA DE PREPAY
                    
                    $paymentData = $st->StoragePaymentData->first();
                    
                    ////////////////
                    
                    $orderId = $paymentData->order_id;
                    $paymentType = $paymentData->payment_type_id;
                    
                        
                    //$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
                    if($paragon == 1){
                        $pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                        $fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                    }else{
                        $pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
                        $fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
                    }
                        
                    $this->info('PDF NAME: '.$pdfName);
                    
                    //ACTUALIZA NOMBRE DEL PDF
                    $billing->pdf  = $pdfName;
                    $billing->bi_status_impresion = "TERMINADO";
                    $billing->save();
                        
                    //crear el pdf
                    $ss = Storage::disk('invoices')->makeDirectory('1');
                    //dd($ss);
                    
                    $totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
                        
                    $totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
                    
                    $this->info("PaymentDataID: ". $paymentData->id . " ; Prepay: ". $paymentData->prepay);
                    $prepayMonths = $paymentData->prepay;
                    $this->info('PrepayMonths: ' . $prepayMonths);
                    
                    $prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
                    $desc2 = ($prepayMonths->off / 100);
                    $prepayMonths = $prepayMonths->months;


                    if($billing->bi_months_invoice != ""){
                        $prepayMonths = $billing->bi_months_invoice;    
                    }
                    
                    //FIX
                    //$prepayMonths = $st->st_meses_prepago;
                        
                    $vat = $paymentData->pd_vat * 100;
                    
                    $this->info('SE GENERA FACTURA DE PREPAY');
                    $this->info('PrepayMonths: ' . $prepayMonths);
                        
                    //$invoiceSecuence = new InvoiceSequence();
                    //$invoiceSecuence->save();
                        
                    //$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
                        
                    //$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
                    //$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
                    
                    //APLICA CUANDO ES DESDE ESTIMATOR
                    $rentStartBilling = $st->rent_start;
                    $this->info('$rentStartBilling: '. $rentStartBilling);
                    
                    $startPeriod = Carbon::parse($rentStartBilling)->startOfMonth()->addMonths(1)->toDateString();
                    $this->info('$startPeriod: '. $startPeriod);
                    
                    $startMonth = Carbon::parse($st->rent_start)->startOfMonth()->format('Y-m-d');
                    $this->info('$startMonth: '. $startMonth);
                    
                    if($startMonth == $st->rent_start){
                        //OJO ES PRIMERO DE MES
                        $startPeriod = Carbon::parse($st->rent_start)->startOfMonth()->toDateString();

                        $this->info("*** START PERIOD: ". $startPeriod);
                    }
                    
                    //SI ES POR ADD PREPAID MONTHS
                    if($billing->bi_batch == 2){
                        $this->info('++ SUMA UN MES ');
                        
                        $this->info("*** START PERIOD ANTES DE SUMAR ". $startPeriod);

                        $ultimoBilling = Billing::where([
                            'user_id'	=> $st->user_id,
                            'storage_id'=> $st->id
                        ])->where('cargo', '>', '0')
                        ->where('id','<>',$billing->id)
                        ->whereNull('id_billing_correccion')
                        ->orderBy('id','desc')->first();

                        $startPeriod = Carbon::parse($ultimoBilling->bi_end_date)->startOfMonth()->addMonths(1)->toDateString();

                        $this->info("*** START PERIOD DESPUES DE SUMAR ". $startPeriod);
                    }
                    
                    $this->info('StartPeriod: '. $startPeriod);
                    
                    $arrStartPeriod = explode('-', $startPeriod);

                    $endPeriod = Carbon::parse($startPeriod)->addMonths( $prepayMonths - 1)->toDateString();
                    $endPeriod = Carbon::parse($endPeriod)->endOfMonth()->toDateString();
                    
                    $this->info('endPeriod: '.$endPeriod);
                    
                    $arrEndPeriod = explode('-',$endPeriod);
                        
                    $period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
                    
                    $year = $arrEndPeriod[0];
                    
                    //OJO::: CALCULA EL MES EN QUE ACABA EL PREPAYMENT
                    $mes = "";
                    
                    switch($arrEndPeriod[1])
                    {
                        case '01':
                            $mes = 'styczeń';
                            break;
                        case '02':
                            $mes = 'luty';
                            break;
                        case '03':
                            $mes = 'marzec';
                            break;
                        case '04':
                            $mes = 'kwiecień';
                            break;
                        case '05':
                            $mes = 'maj';
                            break;
                        case '06':
                            $mes = 'czerwiec';
                            break;
                        case '07':
                            $mes = 'lipiec';
                            break;
                        case '08':
                            $mes = 'sierpień';
                            break;
                        case '09':
                            $mes = 'wrzesień';
                            break;
                        case '10':
                            $mes = 'październik';
                            break;
                        case '11':
                            $mes = 'listopad';
                            break;
                        case '12':
                            $mes = 'grudzień';
                            break;
                                
                    }
                        
                    $mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
                    //
                    
                        
                    $this->info('Period prepay: '.$period);
                    
                    //$boxNeto = round($paymentData->pd_box_price_prepay * $prepayMonths,2);
                    
                    //$suma1 = round($boxNeto + $paymentData->pd_insurance_price_prepay,2);
                    //$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
                    
                    //CALCULA EL VALOR NETO DE LA CAJA
                    
                    $this->info('prepayMonths: '. $prepayMonths);
                    //$this->info($prepayMonths);
                    
                    
                    
                    $cenaNetto = $paymentData->pd_box_price_prepay / $prepayMonths;
                    $suma1 = round($paymentData->pd_box_price_prepay + $paymentData->pd_insurance_price_prepay,2);
                    $suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
                    
                    $data =  [
                            'insurance_neto'	=> $paymentData->insurance,
                            'insurance' => $paymentData->pd_insurance_price_prepay,
                            'vat' => $vat,
                            'vatDb' => $vatDb,
                            'place' => $placeDb,
                            'address' => $addressDb,
                            'nip' => $nipDb,
                            'bank' => $bankDb,
                            'account' => $accountDb,
                            'invoiceNumber' => $billing->bi_number_invoice,
                            'seller' => $seller,
                            '2y' => $billing->bi_year_invoice,
                            'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                            'paymentType' => $pt,
                            'clientName' => $clientName,
                            'clientAddress' => $fullAddress,
                            'clientNip' 		=> $clientNip,
                            'clientPesel'		=> $dataUser->peselNumber,
                            'clientType'		=> $dataUser->userType_id,
                            'grandTotal' 		=> $paymentData->pd_total_prepay,
                            'grandTotalString'	=> $totalString,
                            'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                            'mes'				=> $mes,
                            'year'				=> $year,
                            'storage'			=> $st    ,
                                
                            'dataUser'			=> $dataUser,
                            'vat'				=> $vat ,
                            'cenaNetto'			=> $cenaNetto,
                            'box'				=> $paymentData->pd_box_price_prepay,
                            'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
                            'subtotal'			=> $paymentData->pd_subtotal_prepay,
                                
                            'vatTotal'			=> $paymentData->pd_total_vat_prepay,
                            'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
                                
                            'boxBruto'			=> $paymentData->pd_box_total_prepay,
                            'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
                                
                            'accountant_number'	=> $dataUser->accountant_number,
                            'reciboFiscal'		=> $reciboFiscal,
                                
                            'period'			=> $period,
                            'flag_payed'		=> $flagPaid,
                            'mesesPrepago'		=> $prepayMonths,
                            'suma1'				=> $suma1,
                            'suma2'				=> $suma2,
                            'paragon'			=> $paragon,
                            'encabezado'		=> $encabezado,
                            'container'			=> $st->st_es_contenedor
                    ];
                        
                    $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view);

                    
                    $pdf->save(storage_path().'/app/invoices/'.$pdfName);
                    
                    $this->info('PDF NAME: '.$pdfName);
                    $this->info('Finaliza pdf de prepay');
                    

                    //GENERA COPIA
                    $this->info('Inicia generacion de copia ');
                        
                    if($paragon == 1){
                        $pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                    }else{
                        $pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                    }
                    //$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
                        
                    $data =  [
                            'insurance_neto'	=> $paymentData->insurance,
                            'insurance' => $paymentData->pd_insurance_price_prepay,
                            'vat' => $vat,
                            'vatDb' => $vatDb,
                            'place' => $placeDb,
                            'address' => $addressDb,
                            'nip' => $nipDb,
                            'bank' => $bankDb,
                            'account' => $accountDb,
                            'invoiceNumber' => $billing->bi_number_invoice,
                            'seller' => $seller,
                            '2y' => $billing->bi_year_invoice,
                            'currentDate' 		=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
                            'paymentType' => $pt,
                            'clientName' => $clientName,
                            'clientAddress' => $fullAddress,
                            'clientNip' 		=> $clientNip,
                            'clientPesel'		=> $dataUser->peselNumber,
                            'clientType'		=> $dataUser->userType_id,
                            'grandTotal' 		=> $paymentData->pd_total_prepay,
                            'grandTotalString'	=> $totalString,
                            'date'				=> Carbon::parse($billing->pay_month)->addDays(7)->format('Y-m-d'),
                            'mes'				=> $mes,
                            'year'				=> $year,
                            'storage'			=> $st    ,
                                
                            'dataUser'			=> $dataUser,
                            'vat'				=> $vat ,
                            'cenaNetto'			=> $cenaNetto,
                            'box'				=> $paymentData->pd_box_price_prepay,
                            'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
                            'subtotal'			=> $paymentData->pd_subtotal_prepay,
                                
                            'vatTotal'			=> $paymentData->pd_total_vat_prepay,
                            'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
                                
                            'boxBruto'			=> $paymentData->pd_box_total_prepay,
                            'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
                                
                            'accountant_number'	=> $dataUser->accountant_number,
                            'reciboFiscal'		=> $reciboFiscal,
                                
                            'period'			=> $period,
                            'flag_payed'		=> $flagPaid,
                            'mesesPrepago'		=> $prepayMonths , 
                            'copy'				=> 1,
                            
                            'suma1'				=> $suma1,
                            'suma2'				=> $suma2,
                            'paragon'			=> $paragon,
                            'encabezado'		=> $encabezado,
                            'container'			=> $st->st_es_contenedor
                    ];
                        
                    $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view);
                        
                    $pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
                    
                }
                //FIN DE ESTIMATOR
            }
            
            $this->info('Correo del cliente al que se debe enviar: '. $dataUser->email);
                
            $email2 = $dataUser->email2;
                
            if($email2 == "")
                $email2 = $dataUser->email;

            if($paragon == 1){
                $tituloCorreo = 'Paragon'; 
            }else{
                $tituloCorreo = 'Faktura';
            }
            
            if(env('ENVIRONMENT') == 'DEV'){
                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* DEV */
                        'email' 				=> 'oscar@novacloud.systems',
                        'email2' 				=> 'oscarbar17@gmail.com',
                        'tituloCorreo'			=> $tituloCorreo,
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'period'				=> $mes,
                        'fakturaName'			=> $fakturaName,
                        'paragon'				=> $paragon
                ];

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_monthly_billing', $emailData, function ($message) use ($emailData, $tituloCorreo) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );

                    //DEV
                    $message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    $message->bcc ( ['myss.latam@gmail.com'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject ( $tituloCorreo. ' - ' . $emailData['fakturaName'] );
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            else{
                //PROD
                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* PROD */
                        
                        'email' 				=> $dataUser->email,
                        'email2' 				=> $email2,
                        
                        /* DEV */ 
                        /*
                        'email' 				=> 'oscar.sanchez@novacloud.mx',
                        'email2' 				=> 'abraham.hernandez@gmail.com',
                        */
                        'tituloCorreo'			=> $tituloCorreo,
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'period'				=> $mes,
                        'fakturaName'			=> $fakturaName,
                        'paragon'				=> $paragon
                ];
                

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_monthly_billing', $emailData, function ($message) use ($emailData) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );
                
                    //PROD
                    $message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    $message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl', 'karedys@celticproperty.pl'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            
            
            $this->info('Invoice sent');

            if(!is_null($startPeriod)){
                $billing->bi_start_date = $startPeriod;
                $billing->save();
            }

            if(!is_null($endPeriod)){
                $billing->bi_end_date = $endPeriod;
                $billing->save();
            }
            
        }

        

        $this->info('End: '. Carbon::now());
        $this->info('*******************************************');
    }
}
