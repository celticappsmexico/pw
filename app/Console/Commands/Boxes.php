<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\history_boxes;
use App\Storages;
use Carbon\Carbon;
use App\cata_months;

class Boxes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Boxes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $end = Carbon::now()->subMonth()->endOfMonth()->toDateString();
        $months = cata_months::select('month')->get();
        $now = Carbon::now();
        $currentMonth = $now->month;
        $currentYear = $now->year;

        foreach ($months as $key => $month) {
            $history_boxes = new history_boxes();

            $boxes_rented = Storages::where(\DB::raw('monthname(`rent_start`)'), '=', $month->month)
            ->where(\DB::raw('YEAR(`rent_start`)'), '=', $currentYear)
            ->where('flag_rent', '=', 1)
            ->count();

            $new_boxes = Storages::where(\DB::raw('monthname(`created_at`)'), '=', $month->month)
            ->where(\DB::raw('YEAR(`created_at`)'), '=', $currentYear)
            ->where('flag_rent', '=', 1)
            ->count();

            $history_boxes->boxes_rented = $boxes_rented;
            $history_boxes->new_boxes = $new_boxes;
            $history_boxes->month = $month->month;
            $history_boxes->created_at = $end;
            $history_boxes->updated_at = $end;
            $history_boxes->save();
        }

    }
}
