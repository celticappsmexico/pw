<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class Billing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:billing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Billing mensual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    	$this->info(Carbon::now().' - Inicia Billing...');
    	
    	$this->info(Carbon::now().' - Finaliza Billing...');
    }
}
