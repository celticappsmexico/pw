<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Storages;
use App\History;
use App\PaymentData;
use App\Library\BraintreeHelper;
use Carbon\Carbon;

class DoDetachStorages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_detach_storages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        /*
    	Storages::where('rent_end' , '<' ,Carbon::now())
    	->where('rent_end' , '<>', '0000-00-00')
    	->where(['active' => 1, 'flag_rent' => 1])
    	->update([
    			'flag_rent'			=> 0 ,
    			'user_id'			=> 0 ,
    			'payment_data_id'	=> 0 ,
    			'final_price'		=> 0 ,
    			'rent_start'		=> '0000-00-00',
    			'rent_end'			=> '0000-00-00',
    			'date_subscribe_braintree' => '0000-00-00',
    			'price_per_month'	=> 0,
    			'st_meses_prepago'	=> 0
    	]);
    	*/
    	$this->info("//-------------------");
    	$this->info('Iniciando Proceso...'. Carbon::now());
    	
    	$storages = Storages::where('rent_end' , '<' ,Carbon::now())
    					->where('rent_end' , '<>', '0000-00-00')
    					->where(['active' => 1, 'flag_rent' => 1])
    					->get();
    	
    	$this->info('Total de Storages...'. $storages->count());
    	
    	foreach($storages as $storage){
    		
    		$this->info('StorageID: '.$storage->id);
    		
    		//INSERTA HISTORICO
    		$history = new History();
    		$history->storage_id	= $storage->id;
    		$history->user_id		= $storage->user_id;
    		$history->payment_data_id =$storage->payment_data_id;
    		$history->rent_start	= $storage->rent_start;
    		$history->rent_end		= $storage->rent_end;
    		$history->price_per_month = $storage->price_per_month;
    		$history->save();
    
    		//CANCELA SUSCRIPCION BRAINTREE
    		$pd = PaymentData::find($storage->payment_data_id);
    		$suscriptionID = $pd->pd_suscription_id;
    		
    		if(!is_null($suscriptionID)){
    			$this->info('SE CANCELA SUSCRIPCION A BRAINTREE');
    			$this->info('SuscriptionID: '. $suscriptionID);
    			
	    		$data = BraintreeHelper::cancelSubscription($suscriptionID);
	    		$this->info('SuscriptionID: '. $suscriptionID);
	    		
	    		if($data['returnCode'] == 200){
	    			$pd->pd_suscription_id = null;
	    			$pd->pd_suscription_canceled 	= 1;
	    			$pd->save();
	    			
	    			$this->info('SUSCRIPCION CANCELADA CON EXITO');
	    		}else{
	    			$this->info('ERROR AL CANCELAR LA SUSCRIPCION');
	    		}
    		}
    		
    		//LIBERA CAJA
    		$storage->flag_rent			= 0 ;
        	$storage->user_id			= 0 ;
        	$storage->payment_data_id	= 0 ;
        	$storage->final_price		= 0 ;
        	$storage->rent_start		= '0000-00-00';
        	$storage->rent_end			= '0000-00-00';
        	$storage->date_subscribe_braintree = '0000-00-00';
        	$storage->price_per_month	= 0;
        	$storage->st_meses_prepago	= 0;
        	$storage->save();
        	
        	$this->info('Storage Liberado');
    	}
    	
    	$this->info('Fin de Proceso...'. Carbon::now());
    	$this->info("//-------------------");
    	
    }
}
