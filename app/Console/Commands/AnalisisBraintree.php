<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use Carbon\Carbon;
use App\PaymentData;
use App\Storages;

class AnalisisBraintree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:analisis_braintree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('******************************');
        $this->info('Inicia - '. Carbon::now());

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);


        $collection = $gateway->subscription()->search([
            Braintree_SubscriptionSearch::planId()->is(env('PLAN_ID')),
            Braintree_SubscriptionSearch::status()->in(
              [
                Braintree_Subscription::ACTIVE,
                Braintree_Subscription::PAST_DUE,
                Braintree_Subscription::PENDING
              ]
            )
          ]);
        
        //

        $contador = 0 ;
        foreach($collection as $subscription) {
            $contador++;
            $this->info('***************');
            $this->info('ID: '. $subscription->id);
            //$this->info('ALL: '. $subscription);
            $this->info('Balance: '. $subscription->balance);
            //$this->info('NextBillingDate: '. $subscription->nextBillingDate);
            //$this->info('NextBillAmount: '. $subscription->nextBillAmount);

            $pd = PaymentData::where([
                'pd_suscription_id' => $subscription->id
            ])->first();

            if(!is_null($pd)){
                $st = Storages::find($pd->storage_id);

                $this->info('Price Per Month: '. $st->price_per_month);
                $this->info('Next billing amount:'.$subscription->nextBillAmount);
                
                if($st->price_per_month != $subscription->nextBillAmount){
                    //$this->info('***************');
                    //$this->info('ID: '. $subscription->id);
                    $this->info("OJO:: HAY DIFERENCIA");
                    $this->info('***************');
                    //return false;
                }
            }
        }

        $this->info('Total de Suscripciones:'.$contador);

        $this->info('Fin - '. Carbon::now());
        $this->info('******************************');
        
    }
}
