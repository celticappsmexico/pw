<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use App\PaymentData;
use App\Storages;
use Carbon\Carbon;
use App\Billing;
use Illuminate\Support\Facades\Mail;

class CheckBraintreeSuscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:check_braintree_suscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('******************************');
        $this->info('Inicia - '. Carbon::now());

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);

        $collection = $gateway->subscription()->search([
            Braintree_SubscriptionSearch::planId()->is(env('PLAN_ID')),
            Braintree_SubscriptionSearch::status()->in(
              [Braintree_Subscription::ACTIVE]
            )
          ]);
        
        //
        
        foreach($collection as $subscription) {
            $this->info('ID: '. $subscription->id);

            //echo $subscription->billingDayOfMonth;

            $pd = PaymentData::where([
                'pd_suscription_id' => $subscription->id
            ])->first();

            if(is_null($pd)){
                $flag = true;
            }

            if(!is_null($pd)){
                $storage = Storages::where([
                    'payment_data_id'   => $pd->id
                ])->first();

                if(is_null($storage)){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }

            if($flag){
                $this->info('OJO NO EXISTE LA SUSCRIPCION EN SISTEMA: '. $subscription->id);

                $emailData = [
					'nombreDestinatario' 	=> 'ADMIN',
                    'titulo'			    => 'Suscripcion en Braintree y no en PW',
                    'msg'                   => 'NO EXISTE LA SUSCRIPCION EN SISTEMA: '. $subscription->id
                ];
                
                if(env('ENVIRONMENT') =='PROD'){
                    //ENVIO DE CORREO
                    $resp = Mail::send ( 'emails.msg_generico', $emailData, function ($message) use ($emailData) {
                        $message->to ( "oscar.sanchez@novacloud.mx" );
                        $message->cc ( "oscar@novacloud.systems" );
                            
                        $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                        $message->subject ( $emailData["titulo"] );
                        
                    } );
                }

            }

        }


        ///BUSCAR QUE LA SUSCRIPCION TENGA UN CARGO ESTE MES
        foreach($collection as $subscription) {
            //$this->info('ID: '. $subscription->id);

            //echo $subscription->billingDayOfMonth;

            $pd = PaymentData::where([
                'pd_suscription_id' => $subscription->id
            ])->first();

            if(is_null($pd)){
                $flag = true;
            }

            if(!is_null($pd)){
                $storage = Storages::where([
                    'payment_data_id'   => $pd->id
                ])->first();

                if(is_null($storage)){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }

            if($flag == false){

                $this->info("*********************");
                $this->info("User_ID : " .$storage->user_id);
                $this->info("Storage_ID: ". $storage->id);

                $ultimoBilling = Billing::where([
                    'user_id'	=> $storage->user_id,
                    'storage_id'=> $storage->id,
                    'bi_batch'  => 1
                ])->where('cargo', '>', '0')
                ->orderBy('id','desc')->first();
                
                $ultimaFecha = Carbon::parse($ultimoBilling->bi_start_date)->format('Y-m-d');
    
                if($ultimaFecha > Carbon::now()->startOfMonth()){
                    $this->info('Suscripcion OK : '. $subscription->id);
                    //return false;
                }else{
                    $this->info("***********************");
                    $this->info("UltimoBilling: " .$ultimoBilling->id );
                    $this->info("UserID: " .$ultimoBilling->id );
                    $this->info("StorageID: " .$ultimoBilling->id );
                    $this->info("Ultima Fecha: " .$ultimaFecha );
                    $this->info("***********************");
                    $this->info('Posible suscripcion adelantada: '. $subscription->id);
                }
                /*
                $emailData = [
					'nombreDestinatario' 	=> 'ADMIN',
                    'titulo'			    => 'Suscripcion no tiene un cargo este mes',
                    'msg'                   => 'POSIBLE SUSCRIPCION ERRONEA: '. $subscription->id
                ];
                
                if(env('ENVIRONMENT') =='PROD'){
                    //ENVIO DE CORREO
                    $resp = Mail::send ( 'emails.msg_generico', $emailData, function ($message) use ($emailData) {
                        $message->to ( "oscar.sanchez@novacloud.mx" );
                        $message->cc ( "oscar@novacloud.systems" );
                            
                        $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                        $message->subject ( $emailData["titulo"] );
                        
                    } );
                }*/

            }

        }
        $this->info('Fin - '. Carbon::now());
        $this->info('******************************');
    }
}
