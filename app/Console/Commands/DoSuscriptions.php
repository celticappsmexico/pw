<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Storages;
use App\PaymentData;
use App\CardUser;
use App\TransaccionBraintree;
use Carbon\Carbon;
use App\Billing;

class DoSuscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:do_suscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

    	//OBTIENE STORAGES QUE SE TENGAN QUE SUSCRIBIR A BRAINTREE
    	$this->info("//-------------------");
    	$this->info('Iniciando Proceso (Do Suscriptions Braintree)...'. Carbon::now());
    	 
    	$storagesSuscription = Storages::where('date_subscribe_braintree','<=',Carbon::now())
    				->where('date_subscribe_braintree','<>','0000-00-00')
    				->where('user_id','<>','0')
    				->get();
    	
    	$this->info('Total de suscripciones a realizar: '. $storagesSuscription->count());
    	
    	foreach($storagesSuscription as $st){
    		//SUSCRIBIR A BRAINTREE
			$this->info('StorageID: '. $st->id);

			$ultimoBilling = Billing::where([
				'user_id'	=> $st->user_id,
				'storage_id'=> $st->id
			])->where('cargo', '>', '0')
			->orderBy('id','desc')->first();

			if($ultimoBilling > Carbon::now()->startOfMonth()){
				$this->info('Ultimo billing es posterior... no procede la suscripcion ');
				//return false;
			}else{
				
				$paymentData = PaymentData::find($st->payment_data_id);
				$this->info('PaymentataID: '. $paymentData->id);
				$this->info('CardUserId(PD): '. $paymentData->card_user_id);
			
				$cardUser = CardUser::find($paymentData->card_user_id);
			
				$this->info('CardUserId:'.$cardUser->id);
				
				if(!is_null($cardUser))
				{
					
					$subscriptionData = array(
							'paymentMethodToken' => $cardUser->cu_token_card,
							'planId' => env("PLAN_ID"),
							'price'  => $st->price_per_month
					);
						
					$this->info('Monto de Suscripcion: '. $st->price_per_month);
					
					//$this->cancelSubscription();
					$subscription_result = \Braintree_Subscription::create($subscriptionData);
					
					$this->info($subscription_result);
					
					$tb = new TransaccionBraintree();
					$tb->cliente_id 	= $cardUser->user_id;
					$tb->tb_tipo 		= "createSuscription";
					$tb->tb_respuesta 	= $subscription_result;
					
					
						
					if(isset($subscription_result->subscription->id)){
						
						$tb->tb_estatus = 'SUCCESS';
						$tb->save();
						
						//dd($subscription_result);
						$this->info('Subscription id: ' . $subscription_result->subscription->id);
						
						$paymentData->pd_suscription_id = $subscription_result->subscription->id;
						$paymentData->save();
						
						//UPDATE
						$this->info("ID STORAGE: ". $st->id);
						$this->info("ALIAS: ". $st->alias);
						$this->info("USER ID : ". $st->user_id);

						$storage = Storages::find($st->id);
						$storage->date_subscribe_braintree 	= NULL;
						$storage->save();
						
					}else{
						$tb->tb_estatus = 'ERROR';
						$tb->save();
						
						$this->info('Error al suscribir');
					}
				
				}
				else{
					$this->info('Error: No esta ligado PaymentDaTa a un Card User');
				}
				
				$this->info('********************');
				
			}
		}
			
		$this->info('Fin de Proceso...'. Carbon::now());
		$this->info("//-------------------");
    }
}
