<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\history_incomes;
use Carbon\Carbon;
use App\cata_months;
use App\Billing;

class Incomes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Incomes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $end = Carbon::now()->subMonth()->endOfMonth()->toDateString();
        $months = cata_months::select('month')->get();
        $now = Carbon::now();
        $currentMonth = $now->month;
        $currentYear = $now->year;

        foreach ($months as $key => $month) {
            $history_incomes = new history_incomes();

            $billed = Billing::select(\DB::raw('SUM(`cargo`) as billed'))
            ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->first();
            if ($billed->billed == null) {
                $billed->billed = 0.00;
            }

            $payed = Billing::select(\DB::raw('SUM(`abono`) as payed'))
            ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->where('flag_payed', '=', 1)
            ->first();
            if ($payed->payed == null) {
                $payed->payed = 0.00;
            }

            $not_payed = Billing::select(\DB::raw('(sum(cargo)-sum(abono)) as not_payed'))
            ->where(\DB::raw('monthname(`pay_month`)'), '=', $month->month)
            ->where(\DB::raw('YEAR(`pay_month`)'), '=', $currentYear)
            ->first();
            if ($not_payed->not_payed == null or $not_payed->not_payed < 0) {
                $not_payed->not_payed = 0.00;
            }

            $history_incomes->billed = $billed->billed;
            $history_incomes->payed = $payed->payed;
            $history_incomes->not_payed = $not_payed->not_payed;
            $history_incomes->month = $month->month;
            $history_incomes->created_at = $end;
            $history_incomes->updated_at = $end;
            $history_incomes->save();
        }
    }
}
