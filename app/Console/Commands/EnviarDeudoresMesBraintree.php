<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;
use Braintree_Subscription;
use App\PaymentData;
use App\Billing;
use App\BillingDetail;
use App\Storages;
use App\User;
use App\InvoiceCorrectedSequence;
use App\Countries;
use Carbon\Carbon;

class EnviarDeudoresMesBraintree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:enviar_deudores_mes_braintree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        /*
        $gateway = new Braintree_Gateway([
            'environment' => 'production',
            'merchantId' => '4xmkjwnkssxn3zh7',
            'publicKey' => 'g5kjz28rdtc8tr6y',
            'privateKey' => '0b595020b2c904a124f08d495b40b14d'
        ]);
        */

        $subscriptions = PaymentData::whereNotNull('pd_suscription_id')
            //Excepcion
            ->whereNotIn('id',['186'])
        ->get();
        $ids = array();
        $billing_ids = array();

        foreach($subscriptions as $sub)
        {
            $this->info("**************");
            $this->info("SubID: ". $sub->pd_suscription_id);
            
            //--Buscamos el estatus de la suscripcion en Braintree..
            #$subscription = $gateway->subscription()->find($sub->pd_suscription_id);
            #$this->info("Info Braintree");
            #$this->info($subscription->status);


            $fechaInicio = Carbon::now()->firstOfMonth();
            $fechaFin = Carbon::now()->endOfMonth();

            $this->info("Fecha Inicio: ".$fechaInicio);
            $this->info("Fecha Fin: ". $fechaFin);
            

            $abono = Billing::whereBetween('pay_month',[$fechaInicio,$fechaFin])
                        ->where('abono', '>', '0')
                        ->where([
                                'user_id'       => $sub->user_id ,
                                'storage_id'    => $sub->storage_id
                        ])->first();

            if(is_null($abono)){
                #$this->info("No hay abono");

                array_push($ids, $sub->storage_id);

                $lastCharge = Billing::whereBetween('pay_month',[$fechaInicio,$fechaFin])
                            ->where('cargo', '>', '0')
                            ->where([
                                    'user_id'       => $sub->user_id ,
                                    'storage_id'    => $sub->storage_id,
                                    'bi_batch'      => 1
                            ])->first();

                $this->info("------");
                $this->info("Buscamos ultimo Cargo");
                $this->info('StorageID: '. $sub->storage_id);
                $this->info('UserID: '. $sub->user_id);

                if(!is_null($lastCharge)){
                    $this->info("LastCharge: ". $lastCharge->id);
                    array_push($billing_ids , $lastCharge->id);
                }
                
                $this->info("------");

            }else{
                #$this->info("Si hay abono");
            }

            $this->info("**************");
        }

        foreach($ids as $id){
           #$this->info("StorageID: ". $id);
        }

        /***ADICIONALES */
        /***ADICIONALES */
        /*
        array_push($ids , 54);        
        array_push($ids , 140);
        array_push($ids , 193);
        array_push($ids , 227);
        array_push($ids , 228);
        array_push($ids , 414);
        /**** */
        /*
        array_push($billing_ids , 37230);
        array_push($billing_ids , 37301);
        array_push($billing_ids , 37345);
        array_push($billing_ids , 37376);
        array_push($billing_ids , 37377);
        array_push($billing_ids , 37488);
        */
        /*******/
        $storages = Storages::whereIn(
            'id'    , $ids
        )->with([
            'StorageOwner'
        ])->get();

        $emailData = [
                'storages'  	=> $storages
        ];
        
        //ENVIO DE CORREO
        if(env('ENVIRONMENT') == 'DEV'){
            $this->info("DEV");
            $resp = Mail::send ( 'emails.2019_braintree_deudores', $emailData, function ($message) use ($emailData) {
                $message->to ( "oscar.sanchez@novacloud.mx" );
                
                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( "April debtors" );
                
            } );    
        }else{
            $this->info("PROD");
            $resp = Mail::send ( 'emails.2019_braintree_deudores', $emailData, function ($message) use ($emailData) {
                $message->to ("darbey@celticproperty.pl");
                $message->to ("oscar.sanchez@novacloud.mx");
                $message->to ("baldyga@celticproperty.pl");
                $message->to ("abraham@novacloud.systems");
                $message->to ("jones@celticproperty.pl");

                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( "April debtors" );
                
            } );

        }
        
        $generaFacturas = 1;
        if(!$generaFacturas){
            return false;

        }

        $this->info("Comienza generacion de facturas");

        //-------Genera facturas con el 20% adicional
        foreach($billing_ids as $bill){
            
            $originalBilling = Billing::with('billingDetail','storages')->find($bill);

            $originalBillingDetail = BillingDetail::where(['billing_id' => $bill])->first();
            
            $user = User::find($originalBilling->user_id);

            $bdBox = BillingDetail::where([
                    'billing_id'        => $bill,
                    'bd_tipo_partida'   => 'BOX'
                ])->first();

            $impuestoOriginalInvoice = $bdBox->bd_total_vat;
            $subtotalOriginalInvoice = $bdBox->bd_valor_neto;
            $totalOriginalInvoice = $bdBox->bd_total;

            $porcentaje = env('PORCENTAJE_PAGO_AUTOMATICO_FALLIDO');

            $impuestoNuevaInvoice = $impuestoOriginalInvoice * $porcentaje / 100;
            $subtotalNuevaInvoice = $subtotalOriginalInvoice * $porcentaje / 100;
            $totalNuevaInvoice = $totalOriginalInvoice * $porcentaje / 100;


            $this->info("###############");
            $this->info("BillingID: ". $bill);
            $this->info("Monto Factura Original (BOX): " . $totalOriginalInvoice);
            $this->info("---");
            $this->info("Porcentaje a Aumentar: " . $porcentaje);
            $this->info("Subtotal Factura Nueva ". $subtotalNuevaInvoice);
            $this->info("Impuesto Factura Nueva ". $impuestoNuevaInvoice);
            $this->info("Monto Factura Nueva ". $totalNuevaInvoice);
            $this->info("###############");
            
            //--Calcula saldo
            $billingUpdate = Billing::where([
                        'bi_flag_last' => 1, 
                        'user_id' => $originalBilling->user_id, 
                        'storage_id' => $originalBilling->storage_id
                    ])->first();
						
            if(!is_null($billingUpdate)){
                $saldo = $billingUpdate->saldo;
                $billingUpdate->bi_flag_last = 0;
                $billingUpdate->save();
            }else{
                $saldo = 0;
            }
            //--

            //--Genera consecutivo de correccion
            $invoiceNumberCorrected = new InvoiceCorrectedSequence();
            $invoiceNumberCorrected->save();
            $invoiceNumberCorrected = $invoiceNumberCorrected->id;
            //--Fin de consecutivo de Correccion

            $pdfName = date("y")."-PW-".$originalBilling->bi_number_invoice.'_'.$user->accountant_number.'_CORRECTED.pdf';

            $billing = Billing::create([
                'user_id'           => $originalBilling->user_id,
                'storage_id'        => $originalBilling->storage_id,
                'pay_month'         => Carbon::now()->format('Y-m-d'),
                'flag_payed'        => 0,
                'abono'             => 0,
                'cargo'             => $totalNuevaInvoice,
                'saldo'             => $saldo,
                'pdf'               => $pdfName,
                'pdf_corregido'     => $pdfName,
                'bi_flag_last'      => 1,
                'payment_type_id'   => 0,
                'payment_data_id'   => $originalBilling->payment_data_id,
                'bi_subtotal'       => $subtotalNuevaInvoice,
                'bi_porcentaje_vat' => $originalBilling->bi_porcentaje_vat,
                'bi_total_vat'      => $impuestoNuevaInvoice,
                'bi_total'          => $totalNuevaInvoice,
                'bi_number_invoice' => $originalBilling->bi_number_invoice,
                'bi_year_invoice'   => $originalBilling->bi_year_invoice,
                'bi_batch'          => 0,
                'bi_razon_correccion'           => 'BRAK PŁATNOŚCI AUTOMATYCZNYCH',
                'bi_number_invoice_corrected'   => $invoiceNumberCorrected,
                'bi_year_invoice_corrected'     => date("y"),
                'bi_start_date'                 => $originalBilling->bi_start_date,
                'bi_end_date'                   => $originalBilling->bi_end_date,
                'bi_months_invoice'             => $originalBilling->bi_months_invoice,
                'bi_status_impresion'           => 'TERMINADO',
                'bi_invoice_additional'         => 1
            ]);

            $billingDetail = BillingDetail::create([
                    'billing_id'            => $billing->id,
                    'bd_nombre'             => 'Wynajem powierzchni magazynowej '.$originalBilling->storages->sqm.' m2',
                    'bd_codigo'             => 'box '.$originalBilling->storages->sqm,
                    'bd_valor_neto'         => $subtotalNuevaInvoice,
                    'bd_porcentaje_vat'     => $porcentaje,
                    'bd_total_vat'          => $impuestoNuevaInvoice,
                    'bd_total'              => $totalNuevaInvoice,
                    'bd_tipo_partida'       => 'BOX',
                    'bd_numero'             => $originalBilling->bi_months_invoice
            ]);

            $originalBilling->pdf_corregido = $pdfName;
            $originalBilling->id_billing_correccion = $billing->id;
            $originalBilling->save();

            //--DATOS DEL CLIENTE
            //--FULL NAME
            if($user->userType_id == 1){
                isset($user->name) ? $userName = $user->name : $userName = '';
                isset($user->lastName) ? $userLast = $user->lastName : $userLast = '';
                $clientName = $userName.' '.$userLast;
            }else{
                isset($user->companyName) ? $userName = $user->companyName : $userName = '';
                $clientName = $userName;
            }
            //--FULL ADDRESS
            if($user->UserAddress[0]->street != '' ){
    	
                $street = $user->UserAddress[0]->street;
                $numExt = $user->UserAddress[0]->number;
                $numInt = $user->UserAddress[0]->apartmentNumber;
                $city = $user->UserAddress[0]->city;
                $country = $user->UserAddress[0]->country_id;
                $country = Countries::find($country);
                $country = $country->name;
                $cp = $user->UserAddress[0]->postCode;
            
                if($numInt != "")
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
                else
                    $fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
            
            }else{
                $fullAddress = '';
            }

            $data =  [
                'billing'	            => $billing,
                'billingDetail'         => $billingDetail,
                'originalBilling'       => $originalBilling,
                'originalBillingDetail' => $originalBillingDetail,
                'clientName'            => $clientName,
                'fullAddress'           => $fullAddress,
                'user'                  => $user
                    
            ];
                                                        
            $view =  \View::make('factura_corrected_v2', $data)->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
    
    
            $pdf->save(storage_path().'/app/invoices/'.$pdfName);
        
            $this->info('PDF NAME: '.$pdfName);

            /////////
            $mes = "";
            
            switch(Carbon::now()->format('m'))
            {
                case '01':
                    $mes = 'styczeń';
                    break;
                case '02':
                    $mes = 'luty';
                    break;
                case '03':
                    $mes = 'marzec';
                    break;
                case '04':
                    $mes = 'kwiecień';
                    break;
                case '05':
                    $mes = 'maj';
                    break;
                case '06':
                    $mes = 'czerwiec';
                    break;
                case '07':
                    $mes = 'lipiec';
                    break;
                case '08':
                    $mes = 'sierpień';
                    break;
                case '09':
                    $mes = 'wrzesień';
                    break;
                case '10':
                    $mes = 'październik';
                    break;
                case '11':
                    $mes = 'listopad';
                    break;
                case '12':
                    $mes = 'grudzień';
                    break;
                        
            }
                
            $mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');

            $this->info("Mes: " . $mes);
            
            $email2 = $user->email2;
                
            if($email2 == ""){
                $email2 = $user->email;
            }

            //ENVIO DE CORREO
            if(env('ENVIRONMENT') == 'DEV'){
                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* DEV */
                        'email' 				=> 'oscar@novacloud.systems',
                        'email2' 				=> 'oscar.sanchez@novacloud.mx',
                        'tituloCorreo'			=> 'Faktura',
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'period'				=> $mes,
                        'fakturaName'			=> $pdfName,
                        'paragon'				=> 0
                ];

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_monthly_billing', $emailData, function ($message) use ($emailData) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );

                    //DEV
                    $message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    $message->bcc ( ['myss.latam@gmail.com'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject (  'Faktura - ' . $emailData['fakturaName'] );
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            else{
                //PROD
                $emailData = [
                        'nombreDestinatario' 	=> $clientName,
                        /* PROD */
                        
                        'email' 				=> $user->email,
                        'email2' 				=> $email2,
                        
                        /* DEV */ 
                        /*
                        'email' 				=> 'oscar.sanchez@novacloud.mx',
                        'email2' 				=> 'abraham.hernandez@gmail.com',
                        */
                        'tituloCorreo'			=> 'Faktura',
                        'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
                        'nombrePDF'				=> $pdfName,
                        'period'				=> $mes,
                        'fakturaName'			=> $pdfName,
                        'paragon'				=> 0
                ];
                

                //ENVIO DE CORREO
                $resp = Mail::send ( 'emails.2019_monthly_billing', $emailData, function ($message) use ($emailData) {
                    $message->to ( $emailData['email'] );
                    $message->cc ( $emailData['email2'] );
                
                    //PROD
                    $message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
                    $message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl', 'karedys@celticproperty.pl'] );
                    //
                        
                    $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                    $message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
                    $message->attach ( $emailData ['pathPDF'], array (
                            'as' => $emailData ['nombrePDF'],
                            'mime' => 'application/pdf'
                    ) );
                } );
            }
            //

            $this->info("Se generó una nueva factura");
        }

    }
}
