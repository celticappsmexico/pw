<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\history_incomes;
use Carbon\Carbon;
use App\cata_months;
use App\Billing;
use App\User;
use App\Storages;
use App\notificaciones;
use App\Library\UtilNotificaciones;

class Notices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Notices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Notices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $today = Carbon::now()->toDateString();

        //** Notificación para cajas con 30 días faltates de expiración **//
        $oneMonth = Carbon::now()->addMonth()->toDateString();
        $storagesOneMonth = Storages::with(['StorageOwner'])->where('rent_end', '=', $oneMonth)->get();

        foreach ($storagesOneMonth as $key => $storageOneMonth) {
            $idUser = $storageOneMonth->StorageOwner->id;
            $boxNumber = $storageOneMonth->alias;
            $mensaje = "Your box #".$boxNumber." will expire in 1 month.";
            UtilNotificaciones::EnviarNotice($mensaje, $idUser);    

            $notificacion = new notificaciones();
            $notificacion->user_id = $idUser;
            $notificacion->storage_id = $storageOneMonth->id;
            $notificacion->notf_mensaje = $mensaje;
            $notificacion->save();
        }

        //** Notificación para cajas con 15 días faltates de expiración **//
        $fifteenDays = Carbon::now()->addDays(15)->toDateString();
        $storagesFifteenDays = Storages::with(['StorageOwner'])->where('rent_end', '=', $fifteenDays)->get();

        foreach ($storagesFifteenDays as $key => $storageFifteenDays) {
            $idUser = $storageFifteenDays->StorageOwner->id;
            $boxNumber = $storageFifteenDays->alias;
            $mensaje = "Your box #".$boxNumber." will expire in fifteen days.";
            UtilNotificaciones::EnviarNotice($mensaje, $idUser);    

            $notificacion = new notificaciones();
            $notificacion->user_id = $idUser;
            $notificacion->storage_id = $storageFifteenDays->id;
            $notificacion->notf_mensaje = $mensaje;
            $notificacion->save();
        }

        //** Notificación para cajas con 3 días faltates de expiración **//
        $threeDays = Carbon::now()->addDays(3)->toDateString();
        $storagesThreeDays = Storages::with(['StorageOwner'])->where('rent_end', '=', $threeDays)->get();

        foreach ($storagesThreeDays as $key => $storageThreeDays) {
            $idUser = $storageThreeDays->StorageOwner->id;
            $boxNumber = $storageThreeDays->alias;
            $mensaje = "Your box #".$boxNumber." will expire in three days.";
            UtilNotificaciones::EnviarNotice($mensaje, $idUser);    

            $notificacion = new notificaciones();
            $notificacion->user_id = $idUser;
            $notificacion->storage_id = $storageThreeDays->id;
            $notificacion->notf_mensaje = $mensaje;
            $notificacion->save();
        }

        //** Notificación para cajas con 1 día faltate de expiración **//
        $oneDay = Carbon::now()->addDays(1)->toDateString();
        $storagesOneDay = Storages::with(['StorageOwner'])->where('rent_end', '=', $oneDay)->get();

        foreach ($storagesOneDay as $key => $storageOneDay) {
            $idUser = $storageOneDay->StorageOwner->id;
            $boxNumber = $storageOneDay->alias;
            $mensaje = "Your box #".$boxNumber." will expire in one day.";
            UtilNotificaciones::EnviarNotice($mensaje, $idUser);    

            $notificacion = new notificaciones();
            $notificacion->user_id = $idUser;
            $notificacion->storage_id = $storageOneDay->id;
            $notificacion->notf_mensaje = $mensaje;
            $notificacion->save();
        }

    }
}
