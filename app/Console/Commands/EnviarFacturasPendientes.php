<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Billing;
use App\History;
use App\BillingDetail;
use Illuminate\Support\Facades\Log;
use App\ReciboSequence;
use Illuminate\Support\Facades\DB;
use App\Library\EstimatorHelper;
use App\Library\BraintreeHelper;
use App\Library\Formato;
use App\Storages;
use App\ConfigSystem;
use App\ExtraItems;
use App\CataPaymentTypes;
use App\Library\MoneyString;
use App\User;
use App\Countries;
use App\InvoiceSequence;
use App\ParagonSequence;
use DebugBar\Storage\StorageInterface;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use App\DetOrders;
use App\Orders;
use App\notificaciones;
use App\CataTerm;
use App\clientAddress;
use App\LegalRepresentative;
use App\CataPrepay;
use App\Http\Fecha;
use App\CardUser;
use App\App;
use App\Library\UtilNotificaciones;
use App\CataSecciones;
use App\Library\BillingHelper;



class EnviarFacturasPendientes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:enviar_facturas_pendientes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
    
}
