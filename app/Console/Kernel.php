<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    	//\App\Console\Commands\DoBilling::class,
    	\App\Console\Commands\DoNewBilling::class,
    	\App\Console\Commands\SendCollectiveCorrespondence::class,
        \App\Console\Commands\SendEmail::class,
        //\App\Console\Commands\EnviarFacturasPendientes::class,
    	\App\Console\Commands\DoSuscriptions::class,
    	\App\Console\Commands\DoReleaseOrders::class,
    	\App\Console\Commands\DoSubtractMonths::class,
    	\App\Console\Commands\DoDetachStorages::class,
        \App\Console\Commands\DoCopyBilling::class,
    	\App\Console\Commands\Notices::class,
    	\App\Console\Commands\DoTest::class,
        \App\Console\Commands\DoTruncateSequence::class,
        \App\Console\Commands\EnviarUsuariosAPP::class,
        Commands\OccupancyRateMonth::class,
        Commands\Boxes::class,
        Commands\Incomes::class,
        \App\Console\Commands\SendPendingInvoices::class,
        \App\Console\Commands\CheckBraintreeSuscriptions::class,
        \App\Console\Commands\GenerarContratos::class,
        \App\Console\Commands\CheckBraintreeBalances::class,
        \App\Console\Commands\AnalisisBraintree::class,
        \App\Console\Commands\actualizar_bt_id_billing::class,
        \App\Console\Commands\UpdateBraintreeNames::class,
        \App\Console\Commands\ActualizaLogBraintree::class,
        \App\Console\Commands\EnviarTransaccionesBraintree::class,
        \App\Console\Commands\CorrectInvoicesDate::class,
        \App\Console\Commands\EnviarDeudoresMesBraintree::class,
        \App\Console\Commands\SearchCreditCards::class,
        \App\Console\Commands\InsertAbonos::class,
    	
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void	
     */
    protected function schedule(Schedule $schedule)
    {
    	$hoy = Carbon::now()->format('Y-m-d');

    	//$schedule->command('command:send_users_app');
    	
        $schedule->command('inspire')
                 ->hourly();

        $schedule->command('OccupancyRateMonth')
                 ->monthly()
                 ->cron('* 5 1 * *');

        $schedule->command('Boxes')
            ->monthly();

        $schedule->command('Incomes')
            ->monthly();

        /****
		$schedule->command('command:do_billing')
			->monthly()->cron('0 7 1 * *')
			//->monthly()->cron('40 16 6 * *')
			->appendOutputTo(storage_path()."/logs/billing_".$hoy.".log");
		****/
		$schedule->command('command:do_truncate_sequence')
			->yearly()
			->appendOutputTo(storage_path()."/logs/truncate_sequence.log");
		
		/*
		$schedule->command('command:do_test')
			->everyThirtyMinutes()
			//->cron('30 13 * * *')
			->appendOutputTo(storage_path()."/logs/laravel.log");
		*/
		
        $schedule->command('Notices')
            ->daily();
        /*
        $schedule->command('command:do_subtract_months')
			        ->monthly()->cron('0 7 1 * *')
					->appendOutputTo(storage_path()."/logs/billing_".$hoy.".log");
	
		*/
        
        //DIARIOS
        $schedule->command('command:do_detach_storages')
        		->dailyAt('13:00')
        		->appendOutputTo(storage_path()."/logs/detach_storages.log");
        
        $schedule->command('command:do_suscriptions')
        		->dailyAt('14:00')
        		->appendOutputTo(storage_path()."/logs/do_suscriptions.log");
        
        $schedule->command('command:do_release_orders')
        		->dailyAt('15:00')
                ->appendOutputTo(storage_path()."/logs/release_orders.log");
                
        $schedule->command('command:update_braintree_names')
        		->dailyAt('08:00')
        		->appendOutputTo(storage_path()."/logs/updating_braintree_names.log");
        
        
        //MENSUALES
        $schedule->command('command:do_substract_months')
        		->monthly()->cron('0 7 1 * *')
        		->appendOutputTo(storage_path()."/logs/substract_months.log");
        
        $schedule->command('command:send_correspondence')
				        ->everyTenMinutes()
				        ->appendOutputTo(storage_path()."/logs/collective_correspondence.log");
        
        //ENVIO DE FACTURAS
        /*
        $schedule->command('command:enviar_facturas_pendientes')
            ->everyMinute()
            ->appendOutputTo(storage_path()."/logs/send_invoices.log");
        */
        $schedule->command('command:send_pending_invoices')
            ->everyMinute()
            ->appendOutputTo(storage_path()."/logs/send_pending_invoices.log");
            
        
        //SUSCRIPCIONES BRAINTREE
        $schedule->command('command:check_braintree_suscriptions')
            ->dailyAt('17:00')
            ->appendOutputTo(storage_path()."/logs/check_braintree_suscriptions.log");

    }

}
