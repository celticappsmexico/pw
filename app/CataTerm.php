<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CataTerm extends Model
{
  protected $table = 'cata_term';
  protected $primarykey = 'id';
  protected $fillable = [
      'months', 'off'
  ];
}
