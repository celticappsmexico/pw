<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingConfirmation extends Model
{
    //
    protected $table = 'billing_confirmation';
    
    protected $fillable = [
		    'billing_id',
    ];

}

