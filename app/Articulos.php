<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulos extends Model
{
    protected $table = 'articulos';
    protected $primarykey = 'id';
    protected $fillable = [
        'id', 'nombre', 'nombre_polaco', 'img_path', 'id_seccion', 'sqm', 'active'
    ];


}
