<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalRepresentative extends Model
{
    //
    protected $table = 'client_legal_representatives';
    protected $fillable = ['user_id',
                            'username',
                            'name',
                            'lastName',
                            'function',
                            'phone',
                            'email'
                          ];
}
