<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetOrders extends Model
{
  /**
 * The database table used by the model.
 *
 * @var string
 */
protected $table = 'det_order';

/**
 * The attributes that are mass assignable.
 *
 * @var array
 */

protected $fillable = [	'order_id',
                        'product_type',
                        'product_id',
                        'rent_starts',
                        'quantity',
                        'price',
                        'price_per_month'
                      ];


	public function paymentData(){
    	return $this->hasMany(PaymentData::class,'order_id','order_id');
  }

  public function articleData(){
    	return $this->hasMany(ExtraItems::class,'id','product_id');
  }


}
