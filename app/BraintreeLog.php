<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BraintreeLog extends Model
{
    //
    protected $table = 'braintree_log';
    
    protected $fillable = [
            'user_id',
            'storage_id',
            'bl_suscription_id',
            'bl_transaction_id',
            'bl_status',
            'bl_msg',
            'bl_amount',
            'bl_data',
            'bl_notified'
    ];
    
    protected $hidden = [
            'bl_data'
    ];

    public function user() {
		return $this->belongsTo(User::class);
    }
    
    public function storage() {
		return $this->belongsTo(Storages::class);
	}
}
