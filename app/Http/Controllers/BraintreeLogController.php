<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use App\CardUser;
use App\BraintreeLog;
use App\Storages;
use App\Billing;
use App\CataPrepay;
use Carbon\Carbon;

class BraintreeLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Log::info("****************************");
        Log::info("****************************");
        //Log::info("PAYLOAD:". $request->get('bt_payload'));
    	
    	$json = base64_decode($request->get('bt_payload'));
    	
    	//
    	$transactionID = "";
        $suscriptionID = "";
        $amount = "";
        $status = "";
    	//
    	
    	$xmlDoc = new \DOMDocument();
    	$xmlDoc->loadXML( $json );
    	
    	$searchNode = $xmlDoc->getElementsByTagName( "id" );
        $searchNodeSubs = $xmlDoc->getElementsByTagName( "subscription-id" );
        $searchNodeAmount = $xmlDoc->getElementsByTagName( "amount" );
        $searchNodekind= $xmlDoc->getElementsByTagName( "kind" );
    	
    	$i = 1;
    	
    	foreach( $searchNode as $search )
    	{    		
    		//$items = $search->getElementsByTagName('id');
    		if($i == 2){
	    		$transactionID = $search->nodeValue;
	    		
	    		//Log::info('Transaction ID: '. $transactionID);
    		}
    		
    		$i++;
    	}
    	
    	foreach($searchNodeSubs as $search){
    		$suscriptionID = $search->nodeValue;
    		
    		Log::info('Suscription ID: '. $suscriptionID);
        }
        
        foreach($searchNodeAmount as $search){
    		$amount = $search->nodeValue;
    		
    		Log::info('Amount: '. $amount);
    	}

        foreach($searchNodekind as $search){
    		$status = $search->nodeValue;
    		
    		Log::info('Status: '. $status);
    	}

        if($status == "subscription_charged_successfully"){
            $status = "SUCCESSFUL";
        }else{
            $status = "UNSUCCESSFUL";
        }

        

    	//BUSCA PAYMENT DATA CON ESE ID ...
    	if($suscriptionID == ""){
            Log::info('Suscription ID Empty' );
    	}
    	
    	$paymentData = PaymentData::where([
    			'pd_suscription_id'	=> $suscriptionID
    	])->first();
    	
    	if(!is_null($paymentData)){
    		
    		//Log::info('Se va a actualizar el pago correctamente ');
    		Log::info('SuscriptionID: '. $suscriptionID);
    		Log::info('TransactionID: '. $transactionID);
    		
            BraintreeLog::create([
                'user_id'           => $paymentData->user_id,
                'storage_id'        => $paymentData->storage_id,
                'bl_suscription_id' => $suscriptionID,
                'bl_transaction_id' => $transactionID,
                'bl_status'         => $status,
                'bl_msg'            => 'msg',
                'bl_amount'         => $amount,
                'bl_data'           => $json,
                'bl_notified'       => false,
                'bl_data'           => $json
            ]);
        }

        Log::info("****************************");
        Log::info("****************************");

        return [
            'returnCode'    => 200,
            'msg'           => 'success'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
