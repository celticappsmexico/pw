<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Warehouse;
use App\Levels;
use App\Storages;
use App\ConfigSystem;
use Illuminate\Support\Facades\Auth;
use App\ExtraItems;
use Illuminate\Support\Facades\Log;
use App\Library\EstimatorHelper;
use App\Orders;
use App\DetOrders;
//use BraintreeServiceProvider;

class CalculatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calculator = true;

        $warehouse = Warehouse::where('Active', '!=', '0')->get();
        return view('size-estimator/size-estimator',['buildings' => $warehouse,'calculator' => $calculator]);
    }

    public function calculate(Request $request)
    {
    	Log::info('TERM: '.$request->get('term'));
    	
    	$estimator = EstimatorHelper::estimate(
    						$request->get('idStorage'), 
    						$request->get('date_start'),
							$request->get('insurance'),
    						$request->get('extra_percentage'),
    						$request->get('prepay'),
    						$request->get('term'),
    						0,//WS
    						$request->get('cc')
    						);
    	
    	return ['estimator' => $estimator];
    	   
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $data)
    {
      if ($data['mobile'] == 1) {

      	Log::info('mobile = 1');
        
      	$user_id = $data['id'];
        $sqm =  $data['total'];
        $posible1 = Storages::where('flag_rent','!=',1)
                        ->where('active','=','1')
                        ->where('sqm', '>=', $sqm)
                        ->where('sqm', '<', ($sqm*1.20))
                        ->where('price', '>', '0')
                        ->orderBy('sqm', 'asc')
                        ->get();

        $num1 = count($posible1);
        if ($num1 == 0) {
          return ['success' => false];
        }
        $posible2 = Storages::where('flag_rent','!=',1)
                        ->where('active','=','1')
                        ->where('sqm', '>=', ($sqm*1.20))
                        ->where('sqm', '<', ($sqm*1.40))
                        ->where('price', '>', '0')
                        ->orderBy('sqm', 'asc')
                        ->get();

        $num2 = count($posible2);
        if ($num2 == 0) {
          $posible2 = $posible1;
        }
        $storage1 = '';
        $storage2 = '';

      }else{
      	Log::info('mobile != 1');
      	
      	//FN PARA EXCLUIR DE ORDENES ABIERTAS
      	$orders = Orders::where(['active' => '1'])->lists('id');
      	$detOrders = DetOrders::whereIn('order_id',$orders)->lists('product_id');
      	
      	Log::info('Storages en orden activa: ');
      	Log::info($detOrders->all());
      	
        $user_id = Auth::user()->id;
        $sqm =  $data['sqm'];
        $level = $data['level'];

        $posible1 = Storages::where('flag_rent','!=',1)
                  ->where('active','=','1')
                  ->where('sqm', '>=', $sqm)
                  ->where('sqm', '<', ($sqm*1.20))
                  ->where('level_id','=',$level)
                  ->where('price', '>', '0')
                  ->whereNotIn('id',$detOrders)
                  ->orderBy('sqm', 'asc')
                  ->orderBy('alias', 'asc')
                  ->get();

        $num1 = count($posible1);
        if ($num1 == 0) {
          return ['success' => false];
        }
        $posible2 = Storages::where('flag_rent','!=',1)
                  ->where('active','=','1')
                  ->where('sqm', '>=', ($sqm*(1.2)))
                  ->where('sqm', '<', ($sqm*3))
                  ->where('level_id','=',$level)
                  ->where('price', '>', '0')
                  ->whereNotIn('id',$detOrders)
                  ->orderBy('sqm', 'asc')
                  ->orderBy('alias', 'asc')
                  ->get();

        $num2 = count($posible2);
        if ($num2 == 0) {
          $posible2 = $posible1;
        }
        $storage1 = Storages::where('flag_rent','!=',1)->where('sqm', '>=', $sqm)->where('level_id','=',$level)->where('price', '>', '0')->orderBy('sqm', 'asc')->whereNotIn('id',$detOrders)->first();
        $storage2 = Storages::where('flag_rent','!=',1)->where('sqm', '>=', ($sqm*(1.2)))->where('level_id','=',$level)->where('price', '>', '0')->orderBy('sqm', 'asc')->whereNotIn('id',$detOrders)->first();
        $num3 = count($storage2);
        if ($num3 == 0) {
          $storage2 = $storage1;
        }
      }

      //return [$sqm];
      //dd($data);


      $storages = Storages::where('level_id', '=', $data['level'])->where('active', '!=', '0')->whereNotIn('id',$detOrders)->orderBy('sqm', 'asc')->get();

      $settings = ConfigSystem::all();
      $extraitems = ExtraItems::all();

      $vat = $settings[1]->value;
      $insurance = $settings[0]->value;
      
      
      //dd($posible2);


      return['success' => true,
          'storage1' => $storage1,
          'storage2' => $storage2,
          'storages' => $storages,
          'pos1' => $posible1,
          'pos2' => $posible2,
          'extra-items' => $extraitems,
          'vat' => $vat,
          'insurance' => $insurance
      ];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLevels(Request $request)
    {
      $column = 'warehouse_id';
      $data = Levels::where($column,'=', $request['id'])->where('Active', '!=', '0')->get();
      if ($request->ajax()) {
          return['success' => true,
              'levels' => $data
          ];
      }
    }


    public function findOneStorage(Request $request){

      $storage1 = Storages::where('price', '>', 0 )->where('alias','like', '%'.$request->term.'%')->orWhere('sqm', '>=', $request->term)->orderBy('sqm', 'asc')->get();

      if($storage1 == null){
        return json_encode(trans('calcualtor.findUserFail'));
      }else{
        $list = array();
        foreach ($storage1 as $key => $st) {
          $res = array('id' => $st->id, 'label' =>  'storage: '.$st->alias.', sqm:'.$st->sqm.', price: '.$st->price, 'value' => $st->alias, 'price' => $st->price, 'sqm' => $st->sqm);
          if($st->price > 0 and $st->flag_rent == 0){
            array_push($list, $res);
          }
        }

        return json_encode($list);
      }
    }
}
