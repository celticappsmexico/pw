<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Billing;
use Yajra\Datatables\Datatables;
use App\CataPaymentTypes;
use App\App;
use Carbon\Carbon;
use App\Library\Formato;
use Illuminate\Support\Facades\Log;
use Storage;
use Illuminate\Support\Facades\Response;
use App\Library\MoneyString;
use App\Storages;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Braintree_Gateway;
use App\CardUser;
use App\PaymentData;
use App\CataPrepay;
use App\User;
use App\Countries;
use App\ConfigSystem;
use App\DetOrders;
use App\Orders;
use App\CataTerm;
use App\Http\Fecha;
use App\InvoiceCorrectedSequence;
use App\Library\BraintreeHelper;
use App\BillingDetail;
use App\BillingConfirmation;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($storage_id, $client_id)
    {
		//	
		$pd = PaymentData::where([
			'storage_id'	=> $storage_id,
			'user_id'		=> $client_id
		])->orderBy('id','desc')->first();

		Log::info("PD ID : " . $pd->id);
		
		$suscription_id = "";

		if(env('ENVIRONMENT') == 'PROD')
		{
			if($pd->pd_suscription_id != NULL){
				$gateway = new Braintree_Gateway([
					'environment' => env('ENVIRONMENT_BRAINTREE'),
					'merchantId' => env('MERCHANT_ID_BRAINTREE'),
					'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
					'privateKey' => env('PRIVATE_KEY_BRAINTREE')
				]);
		
				$subscription = $gateway->subscription()->find($pd->pd_suscription_id);
				
				$braintreeBalance = $subscription->balance;
				$suscription_id = $subscription->id;

			}else{
				$braintreeBalance = 0;
			}
		}else{
			$braintreeBalance = -1000;
		}
		Log::info("BALANCE: " . $braintreeBalance);
		Log::info("SUSC ID: " . $suscription_id);

        return view('billing.billing_index',[
        		'storage_id'		=> $storage_id,
				'client_id'			=> $client_id,
				'braintreeBalance' 	=> $braintreeBalance ,
				'suscription_id' 	=> $suscription_id
        ]);
    }
    
    
    public function indexSaldo($storage_id, $client_id)
    {
    	$saldo = Billing::where(['user_id' => $client_id, 'bi_flag_last' => 1, 'storage_id' => $storage_id])->sum('saldo');
    	
    	return ['saldo' => $saldo , 'saldo_formateado' => Formato::formatear('ZLOTY', $saldo) ];
    }
    
    public function indexDT(Request $request)
    {
    	$billing = Billing::where([
    					'storage_id'	=> $request->get('storage_id'),
    					'user_id'		=> $request->get('client_id')
    				])
    				->orderBy('id','DESC')->get();
    	
    	return Datatables::of($billing)
    			->addColumn('payment_type',function($row){
    				if($row->payment_type_id == 0){
    					return '--';
    				}else{
    					$cata = CataPaymentTypes::find($row->payment_type_id);
    					
    					return $cata->name;
    				}
    					
    			})
		    	->editColumn('abono',function($row){
		    		return Formato::formatear('ZLOTY', $row->abono);
		    	})
		    	->editColumn('cargo',function($row){
		    		return Formato::formatear('ZLOTY', $row->cargo);
		    	})
    			->editColumn('saldo',function($row){
    				return Formato::formatear('ZLOTY', $row->saldo);
    			})
    			->editColumn('invoice_number',function($row){
    				if($row->bi_paragon == 1){
    					return $row->bi_year_invoice. "-PAR-".$row->bi_number_invoice;
    				}else{
    					return $row->bi_year_invoice. "-PW-".$row->bi_number_invoice;
    				}
    			})
    			->addColumn('download_pdf',function($row){

    				if($row->pdf == ""){

						if($row->cargo > 0){
							$total = Billing::where([
								'bi_status_impresion' => 'PENDIENTE PDF'
							])->count();

							//$total = round($total * .75);
							
							return '<b style="color:red">Invoice in progress. It will be ready in ' . $total . ' minutes.</b>';
						}
    					return '--';
    				}
    				else{
    					return '<a href='.route('billing.download_pdf',[$row->id]).' class="btn btn-default btn-pdf">
    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
    							</a>
    								';
    				}
    			})
    			->addColumn('pay_now',function($row){


					if($row->flag_payed == 1){
						return '--';
					}
					else{
						$pd = PaymentData::find($row->payment_data_id);	

						Log::info("PD Suscription ID: ". $pd->pd_suscription_id);
						Log::info("BT Suscription Row: ". $row->bi_braintree_suscription);

						if($pd->pd_suscription_id != $row->bi_braintree_suscription){
							if($row->cargo > 0){
								return '<a href='.route('billing_edit.pay',[$row->id]).' class="btn btn-info btn-sm btn-pay-now">
									<i class="fa fa-money" aria-hidden="true"></i> Pay now
								</a>';
							}
						}else{
							if(is_null($pd->pd_suscription_id) && is_null($row->bi_braintree_suscription)){
								if($row->cargo > 0){
									return '<a href='.route('billing_edit.pay',[$row->id]).' class="btn btn-info btn-sm btn-pay-now">
										<i class="fa fa-money" aria-hidden="true"></i> Pay now
									</a>';
								}
							}else{
								//RETRY CHARGE
								//return 'Only Braintree';
								if($row->cargo > 0){
									return '
										<a href="'.route('billing.pay.retry_charge',[$row->bi_braintree_suscription, $row->cargo, $row->id]).'" type="button" 
											class="btn btn-primary btn-retry-charge">
											Retry Charge
										</a>
									';
								}
							}
						}
					}
					
    				
    			})
    			->editColumn('print',function($row){
    				if($row->cargo > 0 && $row->bi_status_impresion == 'PENDIENTE'){
    					return '<a href="#" data-id="'.$row->id.'" class="btn btn-default printInvoicePrepay">
    								<i class="fa fa-print" aria-hidden="true"></i>
    							</a>';
    				}
    			})
    			->addColumn('fix',function($row){
    				if($row->cargo > 0 && $row->pdf_corregido == NULL){
	    				if($row->bi_paragon == 1){
	    					return '<a href='.route('billing.cancel_paragon',[$row->id]).' class="btn btn-warning btn-sm btn-cancel-paragon"
	    								data-user="'.$row->user_id.'"
	    							>
	    								<i class="fa fa-wrench" aria-hidden="true"></i> CANCEL PARAGON
	    							</a>';
	    				}else{
	    					return '<a href='.route('billing.edit_invoice',[$row->id]).' class="btn btn-warning btn-sm btn-edit-invoice">
	    								<i class="fa fa-wrench" aria-hidden="true"></i> '.trans('client.fix_invoice').'
	    							</a>';
	    				}
    				}else{
    					return '--';
    				}
    				
    				
    				
    				
    			})
    			->addColumn('download_pdf_corrected',function($row){
    				if($row->pdf_corregido == ""){
    					return '--';
    				}
    				else{
    					return '<a href='.route('billing.download_pdf_corregido',[$row->id]).' class="btn btn-default btn-pdf">
    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
    							</a>
    								';
    				}
    			})
    			->make(true);
	}
	

	public function indexCurrentMonth()
	{
		return view('billing.billing_current_month_index');
	}

	public function indexCurrentMonthDT()
	{
		$now = Carbon::now();
		
		$currentMonth = $now->month;
		$currentYear = $now->year;

		$bills = BillingDetail::where('bd_tipo_partida', 'BOX')
				->with('billing','billing.user','billing.storages')
				->whereHas('billing', function($q) use($currentMonth,$currentYear) {
						$q->whereMonth('pay_month','=',$currentMonth);
						$q->whereYear('pay_month','=',$currentYear);
						$q->whereNull('id_billing_correccion');
					})->get();//->sum('bd_valor_neto');

		return Datatables::of($bills)
					->addColumn('tenant', function($row){
						return $row->billing->user->name. " ". $row->billing->user->lastName. " - ". $row->billing->user->companyName;
					})
					->addColumn('storage', function($row){
						return $row->billing->storages->alias;
					})
					->make(true);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    public function editInvoice($id)
    {
    	$billing = Billing::with('billingDetail')->find($id);
    	
    	Log::info('BillingID: '. $id);
    	
    	$storage = Storages::with('StoragePaymentData')->find($billing->storage_id);
    	
    	$pd = PaymentData::find($billing->payment_data_id);
    	
    	Log::info($storage->StoragePaymentData[0]->prepay);	
    	$prepay = CataPrepay::find($storage->StoragePaymentData[0]->prepay);
    	Log::info('prepay: '. $prepay);
    	$prepayMonths = $prepay->months;
    	Log::info('PD ID : '. $billing->payment_data_id);
    	
    	
    	if($storage->st_meses_prepago != NULL && $storage->st_meses_prepago != 0){
    		$prepayMonths = $storage->st_meses_prepago;
    	}
    	
    	if($prepayMonths == 0){
    		$prepayMonths = 1;
    	}
    	
    	Log::info('prepayMonths: '. $prepayMonths);
    	 
    	
		return view('billing.billing_edit_invoice',[
				'billing'		=> 	$billing,
    			'prepayMonths'	=> $prepayMonths,
				'storage'		=> $storage,
				'pd'			=> $pd
		]);
    }
    
    public function editPay($id)
    {
    	Log::info('BillingID: '.$id);
    	
    	$billing = Billing::find($id);
    	
    	$storage = Storages::find($billing->storage_id);
				    	//	with('StoragePaymentData')
    	
    	Log::info('StorageID: '. $storage->id);
    	Log::info('BillingID: '. $billing->id);
    	
    	$paymentsType = CataPaymentTypes::where('active','1')->get();
    	
    	if($billing->id_billing_correccion != ""){
    		$billingCorreccion = Billing::find($billing->id_billing_correccion);
    	}else{
    		$billingCorreccion = null;
    	}
    	
    	$paymentData = PaymentData::find($billing->payment_data_id);
    	
    	Log::info('PD ID: '. $paymentData->id);
    	
    	return view('billing.billing_edit_pay',[
    			'id'				=> $id ,
    			'paymentsType' 		=> $paymentsType,
    			'storage'			=> $storage,
    			'billing'			=> $billing,
    			'billingCorreccion'	=> $billingCorreccion,
    			'paymentData'		=> $paymentData
    	]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    public function updateInvoice(Request $request)
    {
    	Log::info('BillingController@updateBilling');
    	
    	Log::info($request->all());
    	 
    	$billing_id = $request->get('billing_id');
    	
    	Log::info('Billing_id: '.$billing_id);
    	
		$billing = Billing::find($billing_id);
		
		if(is_null($billing->bi_start_date)){
			
			Log::info('ERROR: Contact your admin');

			return [
				'returnCode'	=> 500,
				'msg'			=> 'Error: Contact your admin',
				'type'			=> 'error'
			];
		}

    	$reciboFiscal = $billing->bi_recibo_fiscal;
    	
    	$rentStartBilling = $billing->pay_month;
    	
    	$flagPaid = $billing->flag_payed;
    	
    	//NUMERO QUE SE COLOCA EN EL PDF
    	$reciboFiscal = str_pad($reciboFiscal,6,"0",STR_PAD_LEFT);
    	$reciboFiscal = "W".$reciboFiscal;
    		
    	Log::info("Recibo Fiscal: ". $reciboFiscal);
    	
    	$dataUser = User::with(['UserAddress'])->find($billing->user_id);
    	
    	// id user is a client, the name is the real name if not, name is a company name
    	if($dataUser->userType_id == 1){
    		isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
    		isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
    		$clientName = $userName.' '.$userLast;
    	}else{
    		//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
    		isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
    		$clientName = $userName;
    	}
    	
    	Log::info('ClientName: '.$clientName);
    	
    	//NIP
    	$clientNip = $dataUser->nipNumber;
    	
    	//DATOS DE CORRECCION
    	Log::info('Datos de correccion');
    	
    	Log::info('llosc_correccion: '.$request->get('llosc_correccion'));
    	Log::info('cenna_netto_correccion:'. $request->get('cenna_netto_correccion'));
    	Log::info('wartosc_netto_correccion: '.$request->get('wartosc_netto_correccion'));
    	Log::info('kwota_vat_correccion: '. $request->get('kwota_vat_correccion'));
    	Log::info('wartosc_brutto_correccion: '. $request->get('wartosc_brutto_correccion'));
    	
    	Log::info('llosc_correccion_ins: '. $request->get('llosc_correccion_ins'));
    	Log::info('cenna_netto_correccion_ins:'. $request->get('cenna_netto_correccion_ins'));
    	Log::info('wartosc_netto_correccion_ins: '. $request->get('wartosc_netto_correccion_ins'));
    	Log::info('kwota_vat_correccion_ins: '. $request->get('kwota_vat_correccion_ins'));
    	Log::info('wartosc_brutto_correccion_ins: '. $request->get('wartosc_brutto_correccion_ins'));
    	
    	Log::info('llosc_corregido: '.$request->get('llosc_corregido'));
    	Log::info('cenna_netto_corregido:'. $request->get('cenna_netto_corregido'));
    	Log::info('wartosc_netto_corregido: '.$request->get('wartosc_netto_corregido'));
    	Log::info('kwota_vat_corregido: '.$request->get('kwota_vat_corregido'));
    	Log::info('wartosc_brutto_corregido: '.$request->get('wartosc_brutto_corregido'));
    	
    	Log::info('llosc_corregido_ins: '.$request->get('llosc_corregido_ins'));
    	Log::info('cenna_netto_corregido_ins:'. $request->get('cenna_netto_corregido_ins'));
    	Log::info('wartosc_netto_corregido_ins: '.$request->get('wartosc_netto_corregido_ins'));
    	Log::info('kwota_vat_corregido_ins: '.$request->get('kwota_vat_corregido_ins'));
    	Log::info('wartosc_brutto_corregido_ins: '.$request->get('wartosc_brutto_corregido_ins'));
    	
    	Log::info('razem_wartosc_netto_correccion: ' . $request->get('razem_wartosc_netto_correccion'));
    	Log::info('razem_kwota_vat_correccion: '	. $request->get('razem_kwota_vat_correccion'));
    	Log::info('razem_wartosc_brutto_correccion: '. $request->get('razem_wartosc_brutto_correccion'));
    	
    	Log::info('correction_price: '. $request->get('correction_price'));
    	
    	// Address
    	if($dataUser->UserAddress[0]->street != '' ){
    	
    		$street = $dataUser->UserAddress[0]->street;
    		$numExt = $dataUser->UserAddress[0]->number;
    		$numInt = $dataUser->UserAddress[0]->apartmentNumber;
    		$city = $dataUser->UserAddress[0]->city;
    		$country = $dataUser->UserAddress[0]->country_id;
    		$country = Countries::find($country);
    		$country = $country->name;
    		$cp = $dataUser->UserAddress[0]->postCode;
    	
    		if($numInt != "")
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
    		else
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
    	
    	}else{
    		$fullAddress = '';
    	}
    	
    	//GENERA PDF
    	//VALIDA SI ES POR BATCH O ESTIMATOR
    	$st = Storages::find($billing->storage_id);
    	
    	$paymentData = $st->StoragePaymentData->first();
    	
    	//PAYMENT DATA INFO
    	if ($paymentData->creditcard) {
    		$pt = CataPaymentTypes::find(2);
    		$pt = $pt->cpt_lbl_factura;
    	}else{
    		$pt = CataPaymentTypes::find(3);
    		$pt = $pt->cpt_lbl_factura;
    	}
    	
    	//DATA SYSTEM CONFIG
    	$data = ConfigSystem::all();
    	//$ins = $data[0]->value;
    		
    	$ins = $paymentData->insurance;
    	$vatTag = $paymentData->pd_vat + 1;
    	$vat = $paymentData->pd_vat * 100;
    	
    	//
    	$activePay = $paymentData->id;
    	
    	$vatDb = $data[1]->value;
    		
    	$data = ConfigSystem::all();
    	//
    	$placeDb = $data[2]->value;
    	$addressDb = $data[3]->value;
    	$nipDb = $data[4]->value;
    	$bankDb = $data[5]->value;
    	$accountDb = $data[6]->value;
    	$seller = $data[8]->value;
    	//END DATA SYSTEM CONFIG
    	
    	$invoiceNumberCorrected = new InvoiceCorrectedSequence();
    	$invoiceNumberCorrected->save();
    	
    	$invoiceNumberCorrected = $invoiceNumberCorrected->id;
    	
    	Log::info('*****************************************');
    	Log::info('InvoiceCorrectedID: '. $invoiceNumberCorrected);
    	Log::info('*****************************************');
    	
    	//OBITIENE MES
    	setlocale(LC_ALL, 'de_DE');
    	$mes = "";
    	
    	switch(Carbon::now()->format('m'))
    	{
    		case '01':
    			$mes = 'styczeń';
    		break;
    		case '02':
    			$mes = 'luty';
    		break;
    		case '03':
    			$mes = 'marzec';
    		break;
    		case '04':
    			$mes = 'kwiecień';
    		break;
    		case '05':
    			$mes = 'maj';
    		break;
    		case '06':
    			$mes = 'czerwiec';
    		break;
    		case '07':
    			$mes = 'lipiec';
    		break;
    		case '08':
    			$mes = 'sierpień';
    		break;
    		case '09':
				$mes = 'wrzesień';
    		break;
    		case '10':
    			$mes = 'październik';
    		break;
			case '11':
    			$mes = 'listopad';
    		break;
			case '12':
    			$mes = 'grudzień';
    		break;
    													
    	}
    			
    	$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    	
		$year = Carbon::now()->year;

		Log::info("PaymentDataID: ". $paymentData->id);
    	
    	if($billing->bi_batch == 1){
			//BATCH
			
			Log::info("SE CORRIGE UNA FACTURA DE BATCH");
    	
    		if ($paymentData != null) {
    				
    			//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
    			$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';
    					
    			Log::info('PDF NAME: '.$pdfName);
    	
   				//crear el pdf
    			$ss = Storage::disk('invoices')->makeDirectory('1');
    					
				$totalString = MoneyString::transformQtyCurrency($request->get('razem_wartosc_brutto_correccion') * -1);
    					
    			$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    	
    			Log::info('Total String: '.$totalString);
    					
    			//CALUCLO D SUBTOTAL VAT
    			$subtotal = round( $st->price_per_month / $vatTag , 2 );
    			$vatTotal = round( $st->price_per_month - $subtotal , 2 );
    					
    			$box = $subtotal - $ins;
    			$vatInsurance = round($ins * ($vatTag - 1), 2);
    			$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
    			$insuranceBruto = round($ins + $vatInsurance, 2);
    	
    			$boxBruto = round( $box + $vatTotalInsurance,2);
    					
    			Log::info('***** Calculando totales');
    			Log::info('Subtotal: '.$subtotal);
    			Log::info('vat total: '.$vatTotal);
    			Log::info('box: '. $box);
    			Log::info('vatInsurance: '. $vatInsurance);
    			Log::info('vatTotalInsurance: '. $vatTotalInsurance);
    			Log::info('vat '. $vat );
    			Log::info('***** ');
    			//FIN DE CALCULO
    			
    			//NUEVOS
    			$bdBox = BillingDetail::where([
    							'billing_id'		=> $billing_id,
    							'bd_tipo_partida' 	=> 'BOX'
    						])->first();
    			
    			$box = $bdBox->bd_valor_neto;
    			$vatTotalInsurance = $bdBox->bd_total_vat;
    			$boxBruto = $bdBox->bd_total;
    			
    			$subtotal = $billing->bi_subtotal;
    			$vatTotal = $billing->bi_total_vat;
    			$grandTotal = $billing->bi_total;
    					
    			$data =  [
    				'insurance' => $ins,
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' 				=> $nipDb,
    				'bank' 				=> $bankDb,
    				'account' 			=> $accountDb,
    				'invoiceNumber' 	=> $billing->bi_number_invoice,
    				'invoiceCorrected' 	=> $invoiceNumberCorrected,
    				'seller' 			=> $seller,
    				'2y' 				=> date("y"),
					'currentDate' 		=> date("Y-m-d"),
    				'paymentType' 		=> $pt,
    				'clientName' 		=> $clientName,
    				'clientAddress' 	=> $fullAddress,
    				'clientNip' 		=> $clientNip,
    				'clientPesel'		=> $dataUser->peselNumber,
    				'clientType'		=> $dataUser->userType_id,
    				'grandTotal' 		=> $grandTotal,
    				'grandTotalString'  => $totalString,
    				'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
					'dataUser'			=> $dataUser,
    				'mes'				=> $mes,
    				'year'				=> $year,
    				'storage'			=> $st ,
    				'vat'				=> $vat ,
					'box'				=> $box,
					'vatTotalInsurance' => $vatTotalInsurance,
    				'subtotal'			=> $subtotal ,
    				'vatTotal'			=> $vatTotal,
    				'vatInsurance'		=> $vatInsurance ,
    				'boxBruto'			=> $boxBruto,
    				'insuranceBruto'	=> $insuranceBruto,
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal,
    				'flag_payed'		=> $flagPaid,
    				
    				'llosc_correccion'			=> $request->get('llosc_correccion'),
    				'cenna_netto_correccion'	=> $request->get('cenna_netto_correccion'),
    				'wartosc_netto_correccion'	=> $request->get('wartosc_netto_correccion'),
    				'kwota_vat_correccion'		=> $request->get('kwota_vat_correccion'),
    				'wartosc_brutto_correccion'	=> $request->get('wartosc_brutto_correccion'),
    				 
    				'llosc_correccion_ins'			=> $request->get('llosc_correccion_ins'),
    				'cenna_netto_correccion_ins'	=> $request->get('cenna_netto_correccion_ins'),
    				'wartosc_netto_correccion_ins'	=> $request->get('wartosc_netto_correccion_ins'),
    				'kwota_vat_correccion_ins'		=> $request->get('kwota_vat_correccion_ins'),
    				'wartosc_brutto_correccion_ins'	=> $request->get('wartosc_brutto_correccion_ins'),
    					 
    				'llosc_corregido'				=> $request->get('llosc_corregido'),
    				'cenna_netto_corregido'		=> $request->get('cenna_netto_corregido'),
    				'wartosc_netto_corregido'		=> $request->get('wartosc_netto_corregido'),
    				'kwota_vat_corregido'			=> $request->get('kwota_vat_corregido'),
    				'wartosc_brutto_corregido'		=> $request->get('wartosc_brutto_corregido'),
    					 
    				'llosc_corregido_ins'			=> $request->get('llosc_corregido_ins'),
    				'cenna_netto_corregido_ins'	=> $request->get('cenna_netto_corregido_ins'),
    				'wartosc_netto_corregido_ins'	=> $request->get('wartosc_netto_corregido_ins'),
    				'kwota_vat_corregido_ins'		=> $request->get('kwota_vat_corregido_ins'),
    				'wartosc_brutto_corregido_ins'	=> $request->get('wartosc_brutto_corregido_ins'),

    				//RAZEM
    				'razem_wartosc_netto_correccion'=> $request->get('razem_wartosc_netto_correccion'),
		    		'razem_kwota_vat_correccion'	=> $request->get('razem_kwota_vat_correccion'),
					'razem_wartosc_brutto_correccion'=> $request->get('razem_wartosc_brutto_correccion'),

    					
    				'razem_wartosc_netto_corregido'	=> $request->get('razem_wartosc_netto_corregido'),
    				'razem_kwota_vat_corregido'		=> $request->get('razem_kwota_vat_corregido'),
    				'razem_wartosc_brutto_corregido'=> $request->get('razem_wartosc_brutto_corregido'),
    					
    				'correction_price'	=> $request->get('correction_price'),
    					
    				'fechaOriginal'			=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
		    				
		    		'reason_correction'				=> $request->get('reason_correction')
    					
    			];
    																			
    																	
				$view =  \View::make('factura_corrected', compact('data', 'date', 'invoice'))->render();
    																			
    			$pdf = \App::make('dompdf.wrapper');
    			$pdf->loadHTML($view);
    			$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    			//FIN DE BATCH    	
    		}
		}
    	else{
    		//ESTIMATOR
    																	
    		if($billing->bi_flag_prepay == 0){

				Log::info("SE CORRIGE UNA FACTURA DE ESTIMATOR");
    			
    			Log::info('Billing ID '. $billing->id);
    			
				$paymentData = PaymentData::find($billing->payment_data_id);
    																	
    			$orderId = $paymentData->order_id;
    			$paymentType = $paymentData->payment_type_id;
    																	
    			Log::info('OrderID: ' . $orderId );
    																	
    			$items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
    			$order = Orders::where('id','=',$orderId)->first();
    			$userId = $order->user_id;
    			
    			//return [$dataUser];
    			// id user is a client, the name is the real name if not, name is a company name
    																	
    			// NIP
    			$clientNip = $dataUser->nipNumber;
    																	
    			$subtotal = 0;
    																	
    			//return ['items' => $items];
    			$idStorage = 0;
    																	
    			foreach ($items as $key => $item) {
					if($item->product_type == 'box'){
    					$idStorage = $item->product_id;
    					$desc3 = 0;
    					$nextMonth = 0;
    																	
    					Log::info('idStorage: '.$idStorage);
    																	
    					$term = $item->paymentData[0]->term;
    					$term = CataTerm::where('id','=',$term)->first();
    					$desc1 = ($term->off / 100);
    					$term = $term->months;
    					//return [$term,$desc1];
    																	
    					$prepayMonths = $item->paymentData[0]->prepay;
    					$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
    					$desc2 = ($prepayMonths->off / 100);
    					$prepayMonths = $prepayMonths->months;
    					
    					Log::info('desc2: '.$desc2);
    					Log::info('prepayMonths: '.$prepayMonths);
    																	
    					//return [$desc2,$prepayMonths];
    																	
    					$cc = $item->paymentData[0]->creditcard;
    					
    					if ($cc == 1) {
    						$ext = 1;
    					}else{
							$ext = ($item->paymentData[0]->ext /100 ) + 1;
    					}    																	
						//return [$ext];
    																	
						$ins = $item->paymentData[0]->insurance;
    									
						//return [$ins];
    									
						$fecha = $item->rent_starts;
    					$price = $item->price;
    					$utilFecha = new Fecha();
    					$lastDay = $utilFecha->getUltimoDiaMes($fecha);
    					$lastDay = $utilFecha->getDia($lastDay);
    					//return [$lastDay];
    					$currentDay = $utilFecha->getDia($fecha);
    					//return [$lastDay,$currentDay];
    					$daysCurrent = $lastDay-$currentDay;
    					$pricePerDay = $price / $lastDay;
    					//return [$fecha,$lastDay,$daysCurrent,$pricePerDay];
    					$totalCurrentMonth = ($daysCurrent + 1) * $pricePerDay;
    					//return [$daysCurrent,$price,$pricePerDay,$totalCurrentMonth];
    									
    					//calculo del current insurance
    					$insurencePerDay =  $ins / $lastDay;
    					$currentInsurence = $insurencePerDay * ($daysCurrent + 1);
    									
    					Log::info('insurencePerDay: '.$insurencePerDay);
    					Log::info('currentInsurence: '.$currentInsurence);
    									
    					//caclculo primer descuento:
    					$descTermCurrent = $totalCurrentMonth - ($totalCurrentMonth * $desc1);
    					$descTermNext = $price - ($price * $desc1);
    					//return [$descTermCurrent,$descTermNext];
    									
    					Log::info('descTermCurrent: '.$descTermCurrent);
    					Log::info('descTermNext: '.$descTermNext);
    									
    					//calculo segundo desc
    					$descPrepayCurrent = $descTermCurrent - ($descTermCurrent * $desc2);
    					$descPrepayNext = $descTermNext - ($descTermNext * $desc2);
    					//return [$descPrepayCurrent,$descPrepayNext];
    									
    					//calculo tercer desc
    					$descCCCurrent = $descPrepayCurrent * $ext;
    					$descCCNext = $descPrepayNext * $ext;
    					//return [$descCCCurrent,$descCCNext];
    					Log::info('$descCCCurrent: '  . $descCCCurrent);
    					Log::info('$descCCNext: '. $descCCNext);
    	
    					//Mes extra antes de impuestos
    					//$deposit = $descCCCurrent + $descCCNext;
    					$deposit = $descCCNext;
    					Log::info('Deposit1: '.$deposit);
    					//return [$deposit];
    										
    					//sumamos el seguro
    					//$deposit = $deposit + $ins + $currentInsurence;
    					//Log::info('Deposit2: '.$deposit);
    					//return [$deposit];
    										
    					// calculamos los mese de prepago
    					$prepayTotal = ($descCCNext * $prepayMonths) + ($ins * $prepayMonths);
    					Log::info('PrepayTotal'. $prepayTotal);
    					//return [$prepayTotal];
    									
    					// calculamos el subtotal
    					//QUITAMOS EL DEPOSIT
    					$total = $prepayTotal; //+ $deposit;
    					Log::info('PrepayTotal'. $total);
    					//return [$total];
    									
    					//sacamos el precio total del next
    					$subtotalNextMonth = $descCCNext + $ins;
    					$vatNextMonth = $subtotalNextMonth * $vat;
    					$totalNextMonth = $subtotalNextMonth + $vatNextMonth;
    					//return [$totalNextMonth];
    									
    					$subtotal += $total;
    			}else{
			    	//Entonces es un item
			    	$price = $item->price;
										$qt = $item->quantity;
			    	$art = $item->articleData[0]->nombre;
			    	$total = $price * $qt;
			    		
			    	//return ['Art' => $art,'Price' => $price, 'Quantity' => $qt,'Total' => $total];
			    	$subtotal += $total;
			    }
    		}
    		
	    	// El subtotal y el vat se calcula junto con los demas articulos.
	    	$vat = $subtotal * $vat;
	    	$granTotal = $subtotal + $vat;
	    	//return ['Subtotal' => $subtotal,'vat' => $vat,'Total' => $granTotal];
    									
    								
    		$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';

    		$paymentData = PaymentData::find($billing->payment_data_id);
    									
    		$ins = $paymentData->insurance;
    		$vatTag = $paymentData->pd_vat + 1;
    		$vat = $paymentData->pd_vat * 100;
    									
    		$subtotal = 0;
    		$vatTotal = 0;
    									
    		$box = $subtotal - $ins;
    		$vatInsurance = round($ins * ($vatTag - 1), 2);
    		//$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
    		$vatTotalInsurance = 0;
    									
    		$insuranceBruto = round($ins + $vatInsurance, 2);
    	
    		$boxBruto = round( $box + $vatTotalInsurance,2);
    									
    		// generar el pdf
    		//$data = $this->getData();
    		$ss = Storage::disk('invoices')->makeDirectory('1');
    		//dd($ss);
			
    		$totalString = MoneyString::transformQtyCurrency($request->get('razem_wartosc_brutto_correccion') * -1);
    	
    	
    		$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');

    		//RENT START
    		$startPeriod = $st->rent_start;
    	
    		$startPeriod = Carbon::parse($startPeriod)->toDateString();
    	
    		Log::info('PayMonth: '. $billing->pay_month);
    		
    		if($st->rent_start == '0000-00-00'){
    			$startPeriod = Carbon::parse($billing->pay_month)->addMonth(1)->firstOfMonth()->toDateString();
    		}
    		Log::info('Start period invoice: '. $startPeriod);
    		
    															
			$arrStartPeriod = explode('-', $startPeriod);
    	
			$endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->toDateString();
    		$arrEndPeriod = explode('-',$endPeriod);
    	
    		$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
    											
			$data =  [
    				'insurance' => $paymentData->pd_insurance_price,
    				'vat' => $vat,
    				'vatDb' => $vatDb,
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' => $nipDb,
    				'bank' => $bankDb,
    				'account' => $accountDb,
    				'invoiceNumber' => $billing->bi_number_invoice,
					'invoiceCorrected' => $invoiceNumberCorrected,
    				'seller' => $seller,
					'2y' => date("y"),
    				'currentDate' => date("Y-m-d"),
    				'clientName' => $clientName,
    				'clientAddress' => $fullAddress,
    				'clientNip' 		=> $clientNip,
    				'clientPesel'		=> $dataUser->peselNumber,
					'clientType'		=> $dataUser->userType_id,
					'grandTotal' 		=> $paymentData->pd_total,
    				'grandTotalString'	=> $totalString,
    				'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
					'mes'				=> $mes,
    				'year'				=> $year,
    				'storage'			=> $st    ,
    				'paymentType' 		=> $pt,
    				'dataUser'			=> $dataUser,
    				'vat'				=> $vat ,
    				'box'				=> $paymentData->pd_box_price,
    				'vatTotalInsurance' => $paymentData->pd_box_vat,
    				'subtotal'			=> $paymentData->pd_subtotal,
    				'vatTotal'			=> $paymentData->pd_total_vat,
    				'vatInsurance'		=> $paymentData->pd_insurance_vat,
    				'boxBruto'			=> $paymentData->pd_box_total,
					'insuranceBruto'	=> $paymentData->pd_insurance_total,
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal,
    				'period'			=> $period,
    				'flag_payed'		=> $flagPaid,

					'llosc_correccion'			=> $request->get('llosc_correccion'),
    				'cenna_netto_correccion'	=> $request->get('cenna_netto_correccion'),
    				'wartosc_netto_correccion'	=> $request->get('wartosc_netto_correccion'),
    				'kwota_vat_correccion'		=> $request->get('kwota_vat_correccion'),
    				'wartosc_brutto_correccion'	=> $request->get('wartosc_brutto_correccion'),
    				 
    				'llosc_correccion_ins'			=> $request->get('llosc_correccion_ins'),
    				'cenna_netto_correccion_ins'	=> $request->get('cenna_netto_correccion_ins'),
    				'wartosc_netto_correccion_ins'	=> $request->get('wartosc_netto_correccion_ins'),
    				'kwota_vat_correccion_ins'		=> $request->get('kwota_vat_correccion_ins'),
    				'wartosc_brutto_correccion_ins'	=> $request->get('wartosc_brutto_correccion_ins'),
    					 
    				'llosc_corregido'				=> $request->get('llosc_corregido'),
    				'cenna_netto_corregido'		=> $request->get('cenna_netto_corregido'),
    				'wartosc_netto_corregido'		=> $request->get('wartosc_netto_corregido'),
    				'kwota_vat_corregido'			=> $request->get('kwota_vat_corregido'),
    				'wartosc_brutto_corregido'		=> $request->get('wartosc_brutto_corregido'),
    					 
    				'llosc_corregido_ins'			=> $request->get('llosc_corregido_ins'),
    				'cenna_netto_corregido_ins'		=> $request->get('cenna_netto_corregido_ins'),
    				'wartosc_netto_corregido_ins'	=> $request->get('wartosc_netto_corregido_ins'),
    				'kwota_vat_corregido_ins'		=> $request->get('kwota_vat_corregido_ins'),
    				'wartosc_brutto_corregido_ins'	=> $request->get('wartosc_brutto_corregido_ins'),

					//RAZEM
    				'razem_wartosc_netto_correccion'=> $request->get('razem_wartosc_netto_correccion'),
		    		'razem_kwota_vat_correccion'	=> $request->get('razem_kwota_vat_correccion'),
					'razem_wartosc_brutto_correccion'=> $request->get('razem_wartosc_brutto_correccion'),
    					
    				'razem_wartosc_netto_corregido'	=> $request->get('razem_wartosc_netto_corregido'),
    				'razem_kwota_vat_corregido'		=> $request->get('razem_kwota_vat_corregido'),
    				'razem_wartosc_brutto_corregido'=> $request->get('razem_wartosc_brutto_corregido'),
					
					'correction_price'	=> $request->get('correction_price'),
    					
    				'fechaOriginal'			=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
		    				
		    			'reason_correction'				=> $request->get('reason_correction')
					 
					
    			];
			
				$view =  \View::make('factura_corrected', compact('data', 'date', 'invoice'))->render();
    			$pdf = \App::make('dompdf.wrapper');
    			$pdf->loadHTML($view);
			
    			$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    			
    		}else{
				//ES UNA FACTURA DE PREPAY
				
				Log::info("SE CORRIGE UNA FACTURA DE PREPAY");
    	
    			$paymentData = PaymentData::find($billing->payment_data_id);
    	
    			$orderId = $paymentData->order_id;
    			$paymentType = $paymentData->payment_type_id;
    	
    			$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';
    			$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
			
    			//crear el pdf
    			$ss = Storage::disk('invoices')->makeDirectory('1');
    			
				$totalString = MoneyString::transformQtyCurrency($request->get('razem_wartosc_brutto_correccion') * -1);
    																	
    			$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    	
    			
    			$prepayMonths = $paymentData->prepay;
    			$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
    			$desc2 = ($prepayMonths->off / 100);
    			$prepayMonths = $prepayMonths->months;
    			
    			if($prepayMonths == 0){
    				$prepayMonths = $st->st_meses_prepago;
    			}
    			
    	
    			//FIX
    			//$prepayMonths = $st->st_meses_prepago;
    	
    			$vat = $paymentData->pd_vat * 100;

				//$invoiceSecuence = new InvoiceSequence();
    			//$invoiceSecuence->save();
    																			
    	
    			$startPeriod = Carbon::parse($rentStartBilling)->startOfMonth()->addMonths(1)->toDateString();
    	
    			$startMonth = Carbon::parse($st->rent_start)->startOfMonth()->format('Y-m-d');
    	
    			if($startMonth == $st->rent_start){
    				//OJO ES PRIMERO DE MES
    				$startPeriod = Carbon::parse($st->rent_start)->startOfMonth()->toDateString();
    			}	
    	
    			Log::info('StartPeriod: '. $startPeriod);
		
				$startPeriod = $billing->bi_start_date;
		    	$arrStartPeriod = explode('-', $startPeriod);
		    	
		    	$endPeriod = Carbon::parse($startPeriod)->addMonths( $prepayMonths - 1)->toDateString();
		    	$endPeriod = Carbon::parse($endPeriod)->endOfMonth()->toDateString();
		    	
		    	Log::info('endPeriod: '.$endPeriod);
				
				$endPeriod = $billing->bi_end_date;
		    	$arrEndPeriod = explode('-',$endPeriod);
		    		
		    	$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
		    	
				$year = $arrEndPeriod[0];
		    	
		    	//OJO::: CALCULA EL MES EN QUE ACABA EL PREPAYMENT
		    	$mes = "";
		    	
		    	switch($arrEndPeriod[1])
				{
		    			case '01':
		    				$mes = 'styczeń';
		    			break;
		    			case '02':
		    				$mes = 'luty';
		    			break;
		    			case '03':
		    				$mes = 'marzec';
		    			break;
		    			case '04':
							$mes = 'kwiecień';
		    			break;
						case '05':
		    				$mes = 'maj';
		    			break;
		    			case '06':
		    				$mes = 'czerwiec';
		    			break;
		    			case '07':
		    				$mes = 'lipiec';
		    			break;
		    			case '08':
							$mes = 'sierpień';
		    			break;
		    			case '09':
							$mes = 'wrzesień';
		    			break;
		    			case '10':
		    				$mes = 'październik';
		    			break;
		    			
						case '11':
		    				$mes = 'listopad';
						break;
						
		    			case '12':
		    				$mes = 'grudzień';
		    			break;
		    											
		    		}
		    		
		    		$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
			    	//
		    	
		    		
			    	Log::info('Period prepay: '.$period);
		    	
		   			//$boxNeto = round($paymentData->pd_box_price_prepay * $prepayMonths,2);
		    	
		    		//$suma1 = round($boxNeto + $paymentData->pd_insurance_price_prepay,2);
		    		//$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
		    	
					//CALCULA EL VALOR NETO DE LA CAJA
					
			    	Log::info('pd_box_price_prepay: ' . $paymentData->pd_box_price_prepay);
			    	Log::info('prepayMonths: ' . $prepayMonths);
			    	
					$cenaNetto = $paymentData->pd_box_price_prepay / $prepayMonths;
					$suma1 = round($paymentData->pd_box_price_prepay + $paymentData->pd_insurance_price_prepay,2);
					$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
		    	
		    		$data =  [
						'insurance_neto'	=> $paymentData->insurance,
		    			'insurance' => $paymentData->pd_insurance_price_prepay,
		    			'vat' => $vat,
		    			'vatDb' => $vatDb,
		    			'place' => $placeDb,
		    			'address' => $addressDb,
		    			'nip' => $nipDb,
		    			'bank' => $bankDb,
		    			'account' => $accountDb,
		    			'invoiceNumber' => $billing->bi_number_invoice,
		    			'invoiceCorrected' => $invoiceNumberCorrected,
		    			'seller' => $seller,
		    			'2y' => date("y"),
		    			'currentDate' => date("Y-m-d"),
						'paymentType' => $pt,
		    			'clientName' => $clientName,
		    			'clientAddress' => $fullAddress,
		    			'clientNip' 		=> $clientNip,
		    			'clientPesel'		=> $dataUser->peselNumber,
		    			'clientType'		=> $dataUser->userType_id,
		    			'grandTotal' 		=> $paymentData->pd_total_prepay,
		    			'grandTotalString'	=> $totalString,
		    			'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
		    			'mes'				=> $mes,
		    			'year'				=> $year,
		    			'storage'			=> $st    ,
		    				
		    			'dataUser'			=> $dataUser,
		    			'vat'				=> $vat ,
		    			'cenaNetto'			=> $cenaNetto,
		    			'box'				=> $paymentData->pd_box_price_prepay,
						'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
		    			'subtotal'			=> $paymentData->pd_subtotal_prepay,
		    								
		    			'vatTotal'			=> $paymentData->pd_total_vat_prepay,
		    			'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
						
		    			'boxBruto'			=> $paymentData->pd_box_total_prepay,
		    			'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
						
		    			'accountant_number'	=> $dataUser->accountant_number,
		    			'reciboFiscal'		=> $reciboFiscal,
		    										
		    			'period'			=> $period,
		    			'flag_payed'		=> $flagPaid,
		    			'mesesPrepago'		=> $prepayMonths,
		    			'suma1'				=> $suma1,
		    			'suma2'				=> $suma2,

	    				'llosc_correccion'			=> $request->get('llosc_correccion'),
	    				'cenna_netto_correccion'	=> $request->get('cenna_netto_correccion'),
	    				'wartosc_netto_correccion'	=> $request->get('wartosc_netto_correccion'),
	    				'kwota_vat_correccion'		=> $request->get('kwota_vat_correccion'),
	    				'wartosc_brutto_correccion'	=> $request->get('wartosc_brutto_correccion'),
	    				 
	    				'llosc_correccion_ins'			=> $request->get('llosc_correccion_ins'),
	    				'cenna_netto_correccion_ins'	=> $request->get('cenna_netto_correccion_ins'),
	    				'wartosc_netto_correccion_ins'	=> $request->get('wartosc_netto_correccion_ins'),
	    				'kwota_vat_correccion_ins'		=> $request->get('kwota_vat_correccion_ins'),
	    				'wartosc_brutto_correccion_ins'	=> $request->get('wartosc_brutto_correccion_ins'),
	    					 
	    				'llosc_corregido'				=> $request->get('llosc_corregido'),
	    				'cenna_netto_corregido'		=> $request->get('cenna_netto_corregido'),
	    				'wartosc_netto_corregido'		=> $request->get('wartosc_netto_corregido'),
	    				'kwota_vat_corregido'			=> $request->get('kwota_vat_corregido'),
	    				'wartosc_brutto_corregido'		=> $request->get('wartosc_brutto_corregido'),
	    					 
	    				'llosc_corregido_ins'			=> $request->get('llosc_corregido_ins'),
	    				'cenna_netto_corregido_ins'		=> $request->get('cenna_netto_corregido_ins'),
	    				'wartosc_netto_corregido_ins'	=> $request->get('wartosc_netto_corregido_ins'),
	    				'kwota_vat_corregido_ins'		=> $request->get('kwota_vat_corregido_ins'),
	    				'wartosc_brutto_corregido_ins'	=> $request->get('wartosc_brutto_corregido_ins'),
    					
    					'razem_wartosc_netto_correccion'=> $request->get('razem_wartosc_netto_correccion'),
		    			'razem_kwota_vat_correccion'	=> $request->get('razem_kwota_vat_correccion'),
		    			'razem_wartosc_brutto_correccion'=> $request->get('razem_wartosc_brutto_correccion'),
    					
	    				'razem_wartosc_netto_corregido'	=> $request->get('razem_wartosc_netto_corregido'),
	    				'razem_kwota_vat_corregido'		=> $request->get('razem_kwota_vat_corregido'),
	    				'razem_wartosc_brutto_corregido'=> $request->get('razem_wartosc_brutto_corregido'),
					
						'correction_price'	=> $request->get('correction_price'),
    					
    					'fechaOriginal'			=> Carbon::parse($billing->pay_month)->format('Y-m-d'),
		    				
		    			'reason_correction'				=> $request->get('reason_correction')
		    		];
		    										
		    		$view =  \View::make('factura_corrected', compact('data', 'date', 'invoice'))->render();
					$pdf = \App::make('dompdf.wrapper');
		    		$pdf->loadHTML($view);
		    	
		    	
		    		$pdf->save(storage_path().'/app/invoices/'.$pdfName);
		    	
		    		Log::info('PDF NAME: '.$pdfName);
		    		Log::info('Finaliza pdf de prepay corregido');
		    
    		}
    		//FIN DE ESTIMATOR
		}

		$billing->pdf_corregido = $pdfName;
		$billing->save();
		
		//GENERA ABONO
		
		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $billing->user_id, 'storage_id' => $billing->storage_id])->first();
		if(!is_null($billingUpdate)){
			$saldo = $billingUpdate->saldo;
			$billingUpdate->bi_flag_last = 0;
			$billingUpdate->save();
			
			Log::info('El flag last era el billing_id : '. $billingUpdate->id);
		}
		else{
			$saldo = 0;
			Log::info('No cuenta con flag last ');
		}
		$abono = $request->get('razem_wartosc_brutto_correccion')  * -1;
		$nuevoSaldo = $abono + $saldo;
		
		Log::info('Saldo antes de hacer un abono al cliente es de: '. $saldo);
		Log::info('El abono sera de : '. $abono);
		Log::info('El nuevo de saldo: '. $nuevoSaldo);
		
		$newbilling = new Billing();
		$newbilling->user_id 		= $billing->user_id;
		$newbilling->storage_id		= $billing->storage_id;
		$newbilling->pay_month		= Carbon::now();
		$newbilling->flag_payed		= 1;
		
		$newbilling->abono				= $abono;
		$newbilling->cargo 				= 0;
		$newbilling->saldo 				= $nuevoSaldo;
		
		$newbilling->pdf		= "";
		$newbilling->bi_flag_last = 1;
		$newbilling->bi_subtotal	= "";
		$newbilling->bi_porcentaje_vat		= "";
		$newbilling->bi_total_vat		= "";
		$newbilling->bi_total		= "";
		$newbilling->bi_status_impresion = "TERMINADO";
		$newbilling->bi_recibo_fiscal	= $billing->bi_recibo_fiscal;
		$newbilling->bi_number_invoice	= $billing->bi_number_invoice;
		$newbilling->bi_year_invoice	= $billing->bi_year_invoice;
		$newbilling->payment_data_id	= $billing->payment_data_id;
		$newbilling->bi_flag_prepay		= $billing->bi_flag_prepay;
		$newbilling->pdf_corregido		= $pdfName;
		$newbilling->reference			= "CORRECTION";
		$newbilling->bi_number_invoice_corrected = $invoiceNumberCorrected;
		$newbilling->bi_year_invoice_corrected = date("y");
		$newbilling->bi_razon_correccion = $request->get('reason_correction'); 
		$newbilling->save();
		
		
		$billing->id_billing_correccion	= $newbilling->id;
		$billing->save();
		
		Log::info('Actualizamos el id billing correccion: '. $newbilling->id);
		
		//FIN DE GENERA ABONO
    	
   		return ['returnCode'	=> 200 , 'msg' 	=> 'Invoice updated'];
    }
    
    public function updatePay(Request $request)
    {
    	Log::info('Se registra el pago manualmente');
    	
    	$billing = Billing::find($request->get('id'));
    	
    	//GET SALDO
    	$saldo = Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])->sum('saldo');

    	if(is_null($saldo))
    		$saldo = 0;
    	
    	Log::info('Datos: ');
    	Log::info('Saldo Actual: '. $saldo);
    	Log::info('Userid : '. $billing->user_id);
    	Log::info('StorageId: '.$billing->storage_id);
    	
    	if($billing->id_billing_correccion != ""){
    		//OJO ES UNA CORRECCION
    		$billingCorreccion = Billing::find($billing->id_billing_correccion);
    	
    		$amount = round($billing->bi_total - $billingCorreccion->abono, 2);
    	}
    	else{
    		$amount = $billing->bi_total;
    	}
    	
    	$cataPayment = CataPaymentTypes::find($request->get('payment_type_id'));
    	
    	
    	if($request->get('payment_type_id') == 2){
    		Log::info('VA A BRAINTREE A HACER TRANSACCION');
    		
    		$storage = Storages::with('StoragePaymentData')->find($billing->storage_id);
    		
    		Log::info('StorageID:'. $storage->id);
    		Log::info('PaymentDataID: '. $storage->payment_data_id);
    		Log::info('idCard: '.$request->get('idCardUser'));
    		
    		$cardUser = CardUser::find($request->get('idCardUser'));
    		
    		//$data = $this->createTransaction($cardUser->cu_token_card, $cardUser->cu_customer_id, env('PLAN_ID'), 0, 
    			//				$billing->storage_id, $storage->payment_data_id ,1, $billing->id);
    		
    		$data = BraintreeHelper::createTransaction($cardUser->cu_token_card, $cardUser->cu_customer_id, $amount, $storage->StoragePaymentData[0]->order_id,false,$billing->user_id);
    		
    		if($data['returnCode'] == 200){
    			$billing->reference 		= "Paid with: ".$request->get('reference') . "<br/> ID: ".$request->get('id'). " <br/> Paid date: ".Carbon::now()->format('Y-m-d') . "<br/> TransationID: ". $data['transaction_id'];
    			$billing->flag_payed 		= 1;
    			$billing->bi_flag_last		= 0;
    			$billing->payment_type_id	= $request->get('payment_type_id');
    			$billing->save();
    			 
    			//New row
    			Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])
    			->update(['bi_flag_last' => '0']);
    			 
    			$saldoNuevo = $saldo + $billing->cargo;
    			Log::info('El cargo en el nuevo registro es de : '.$saldoNuevo);
    			Log::info('Operacion aritmetica realizada : '.$saldo .'+'. $billing->cargo);
    			 
    			
    			$newBilling = new Billing();
    			$newBilling->user_id 	= $billing->user_id;
    			$newBilling->storage_id	= $billing->storage_id;
    			$newBilling->pay_month	= Carbon::now()->format('Y-m-d');
    			$newBilling->flag_payed = 1;
    			$newBilling->abono		= $billing->cargo;
    			$newBilling->cargo		= 0;
    			$newBilling->saldo		= $saldoNuevo;
    			$newBilling->pdf		= "";
    			$newBilling->reference	= $request->get('reference');
    			$newBilling->bi_flag_last= 1;
    			$newBilling->payment_type_id 	= $request->get('payment_type_id');
    			
    			$newBilling->save();
    			
    			Log::info("*************+Finaliza***********************");
    				
    			return ['returnCode' => 200 , 
    							'transaction_id'	=> $data['transaction_id'] , 
    							'amount'			=> $amount,
    							'payment_method'	=> $cataPayment->name,
    							'payment_date'		=> Carbon::now()->format('Y-m-d'),
    							'status'			=> $data['status'],
    							'cardType'			=> $data['cardType'],
    							'error'				=> ""
    			];
    		}
    		else
    		{
    			return ['returnCode' => 100, 'msg'	=> $data['msg'],
    							'error'	=> $data['msg']
    					
    			];
    		}
    		
    	}else{
    		

    		$billing->reference 		= "Paid with: ".$request->get('reference') . "<br/> ID: ".$request->get('id'). " <br/> Paid date: ".Carbon::now()->format('Y-m-d');;
    		$billing->flag_payed 		= 1;
    		$billing->bi_flag_last		= 0;
    		$billing->payment_type_id	= $request->get('payment_type_id');
    		$billing->save();
    		 
    		//New row
    		Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])
    		->update(['bi_flag_last' => '0']);
    		 
    		$saldoNuevo = $saldo + $amount;
    		Log::info('El cargo en el nuevo registro es de : '.$saldoNuevo);
    		Log::info('Operacion aritmetica realizada : '.$saldo .'+'. $amount);
    		 
    		
	    	$newBilling = new Billing();
	    	$newBilling->user_id 	= $billing->user_id;
	    	$newBilling->storage_id	= $billing->storage_id;
	    	$newBilling->pay_month	= Carbon::now()->format('Y-m-d');
	    	$newBilling->flag_payed = 1;
	    	$newBilling->abono		= $amount;
	    	$newBilling->cargo		= 0;
	    	$newBilling->saldo		= $saldoNuevo;
	    	$newBilling->pdf		= "";
	    	$newBilling->reference	= $request->get('reference');
	    	$newBilling->bi_flag_last= 1;
	    	$newBilling->payment_type_id 	= $request->get('payment_type_id');
	    	
	    	$newBilling->save();
	    	
	    	Log::info("*************+Finaliza***********************");
	    	
    		return ['returnCode' => 200, 'msg'	=> 'Payment Successful', 
    						'amount'	=> $amount,
    						'payment_method'	=> $cataPayment->name,
    						'payment_date'		=> Carbon::now()->format('Y-m-d')
    		];
    	}
    }
    
    
    public function cancelParagon($id)
    {
    	$billing = Billing::find($id);
    	
    	//OBTIENE SALDO
    	$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $billing->user_id, 'storage_id' => $billing->storage_id])->first();
    	if(!is_null($billingUpdate)){
    		$saldo = $billingUpdate->saldo;
    		$billingUpdate->bi_flag_last = 0;
    		$billingUpdate->save();
    	}
    	else{
    		$saldo = 0;
    	}
    	
    	//INSERTA ABONO
    	$abono = new Billing();
    	$abono->user_id 		=	$billing->user_id;
    	$abono->storage_id		= 	$billing->storage_id; 
		$abono->pay_month		= 	$billing->pay_month;
		$abono->flag_payed		= 	1;
		$abono->abono			= 	$billing->cargo;
		$abono->cargo			= 	0;
		$abono->saldo			= 	round($saldo + $billing->cargo,2);
    	//$abono->pdf				= 	"FALTA GENERAR EL PDF";
    	$abono->reference		= 	"CANCELLATION";
    	$abono->bi_flag_last	= 	1;
    	$abono->payment_type_id = 	$billing->payment_type_id;
    	$abono->bi_subtotal		= 	$billing->bi_subtotal;
    	$abono->bi_porcentaje_vat = $billing->bi_porcentaje_vat;
    	$abono->bi_total_vat	= 	$billing->bi_total_vat;
    	$abono->bi_total		= 	$billing->bi_total;
    	$abono->bi_status_impresion	= "TERMINADO";
    	$abono->bi_recibo_fiscal = $billing->bi_recibo_fiscal;
    	$abono->bi_number_invoice = $billing->bi_number_invoice;
    	$abono->bi_year_invoice	= 	$billing->bi_year_invoice;
    	$abono->payment_data_id	= 	$billing->payment_data_id;
    	$abono->bi_flag_prepay	= 	$billing->bi_flag_prepay;
    	$abono->bi_batch		= 	$billing->bi_batch;
    	//$abono->pdf_corregido	=  	"FALTA GENERAR EL PDF";
    	//$abono->id_billing_correccion	= 
    	//$abono->bi_razon_correccion =
    	//$abono->bi_number_invoice_corrected = 
    	//$abono->bi_year_invoice_corrected	= 
    	$abono->bi_paragon		= $billing->bi_paragon;
    	$abono->save();
    	
    	$billing->id_billing_correccion	= $abono->id;
    	
    	//------------------------------
    	//------GENERA PDF IGUALITO
    	//------------------------------
    	$billing_id = $billing->id;
    	$recibo_fiscal = $billing->bi_recibo_fiscal;
    	
    	$rentStartBilling = $billing->pay_month;
    	
    	$flagPaid = $billing->flag_payed;
    	
    	//NUMERO QUE SE COLOCA EN EL PDF
    	$reciboFiscal = str_pad($recibo_fiscal,6,"0",STR_PAD_LEFT);
    	$reciboFiscal = "W".$reciboFiscal;
    	
    	$paragon = $billing->bi_paragon;
    	
    	$encabezado = "";
    	if($paragon == 1)
    		$encabezado = "PAR";
    	else
    		$encabezado = "PW";
    		
    	$dataUser = User::with(['UserAddress'])->find($billing->user_id);
    	
    	// id user is a client, the name is the real name if not, name is a company name
    	if($dataUser->userType_id == 1){
    		isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
    		isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
    		$clientName = $userName.' '.$userLast;
    	}else{
    		//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
    		isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
    		$clientName = $userName;
    	}
    	
    	Log::info('ClientName: '.$clientName);
    	
    	//NIP
    	$clientNip = $dataUser->nipNumber;
    		
    	// Address
    	if($dataUser->UserAddress[0]->street != '' ){
    	
    		$street = $dataUser->UserAddress[0]->street;
    		$numExt = $dataUser->UserAddress[0]->number;
    		$numInt = $dataUser->UserAddress[0]->apartmentNumber;
    		$city = $dataUser->UserAddress[0]->city;
    		$country = $dataUser->UserAddress[0]->country_id;
    		$country = Countries::find($country);
    		$country = $country->name;
    		$cp = $dataUser->UserAddress[0]->postCode;
    	
    		if($numInt != "")
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
    		else
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
    	
    	}else{
    		$fullAddress = '';
    	}
    	
    	//GENERA PDF
    	//VALIDA SI ES POR BATCH O ESTIMATOR
    	$st = Storages::find($billing->storage_id);
    	
    	$paymentData = $st->StoragePaymentData->first();
    	
    	//PAYMENT DATA INFO
    	if ($paymentData->creditcard) {
    		$pt = CataPaymentTypes::find(2);
    		$pt = $pt->cpt_lbl_factura;
    	}else{
    		$pt = CataPaymentTypes::find(3);
    		$pt = $pt->cpt_lbl_factura;
    	}
    	
    	//DATA SYSTEM CONFIG
    	$data = ConfigSystem::all();
    	//$ins = $data[0]->value;
    		
    	$ins = $paymentData->insurance;
    	$vatTag = $paymentData->pd_vat + 1;
    	$vat = $paymentData->pd_vat * 100;
    	
    	//
    	$activePay = $paymentData->id;
    	
    	$vatDb = $data[1]->value;
    		
    	$data = ConfigSystem::all();
    	//
    	$placeDb = $data[2]->value;
    	$addressDb = $data[3]->value;
    	$nipDb = $data[4]->value;
    	$bankDb = $data[5]->value;
    	$accountDb = $data[6]->value;
    	$seller = $data[8]->value;
    	//END DATA SYSTEM CONFIG
    	
    	
    	//OBITIENE MES
    	setlocale(LC_ALL, 'de_DE');
    	$mes = "";
    	
    	switch(Carbon::now()->format('m'))
    	{
    		case '01':
    		$mes = 'styczeń';
    		break;
    			case '02':
    			$mes = 'luty';
    					break;
    					case '03':
    					$mes = 'marzec';
    					break;
    					case '04':
    						$mes = 'kwiecień';
    						break;
    					case '05':
    					$mes = 'maj';
    					break;
    					case '06':
    						$mes = 'czerwiec';
    						break;
    						case '07':
    						$mes = 'lipiec';
    						break;
    						case '08':
    						$mes = 'sierpień';
    						break;
    						case '09':
				$mes = 'wrzesień';
    						break;
    						case '10':
    								$mes = 'październik';
    										break;
			case '11':
    										$mes = 'listopad';
    										break;
			case '12':
    										$mes = 'grudzień';
    										break;
    													
    		}
    			
    		$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    	
			$year = Carbon::now()->year;
    	
    		if($billing->bi_batch == 1){
    			//BATCH
    	
    			if ($paymentData != null) {
    				
    			//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
    				$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';
    				$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
    					
    				Log::info('PDF NAME: '.$pdfName);
    	
    				//crear el pdf
    				$ss = Storage::disk('invoices')->makeDirectory('1');
    					
    				$totalString = MoneyString::transformQtyCurrency($st->price_per_month);
    					
    				$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    	
    				Log::info('Total String: '.$totalString);
    					
    				//CALUCLO D SUBTOTAL VAT
    				$subtotal = round( $st->price_per_month / $vatTag , 2 );
    				$vatTotal = round( $st->price_per_month - $subtotal , 2 );
    					
    				$box = $subtotal - $ins;
    				$vatInsurance = round($ins * ($vatTag - 1), 2);
    				$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
    				$insuranceBruto = round($ins + $vatInsurance, 2);
    	
    				$boxBruto = round( $box + $vatTotalInsurance,2);
    					
    				Log::info('***** Calculando totales');
    				Log::info('Subtotal: '.$subtotal);
    				Log::info('vat total: '.$vatTotal);
    				Log::info('box: '. $box);
    				Log::info('vatInsurance: '. $vatInsurance);
    				Log::info('vatTotalInsurance: '. $vatTotalInsurance);
    				Log::info('vat '. $vat );
    				Log::info('***** ');
    				//FIN DE CALCULO
    					
    				Log::info("RFISCAL: ".$reciboFiscal);
					$data =  [
    				'insurance' => $ins,
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' 				=> $nipDb,
    				'bank' 				=> $bankDb,
    				'account' 			=> $accountDb,
    				'invoiceNumber' 	=> $billing->bi_number_invoice,
    				'seller' 			=> $seller,
					'2y' 				=> date("y"),
    				'currentDate' 		=> date("Y-m-d"),
    				'paymentType' 		=> $pt,
    				'clientName' 		=> $clientName,
    				'clientAddress' 	=> $fullAddress,
    				'clientNip' 		=> $clientNip,
    				'clientPesel'		=> $dataUser->peselNumber,
					'clientType'		=> $dataUser->userType_id,
    				'grandTotal' 		=> $st->price_per_month,
    				'grandTotalString'  => $totalString,
    				'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    				'dataUser'			=> $dataUser,
    				'mes'				=> $mes,
    				'year'				=> $year,
    				'storage'			=> $st ,
    				'vat'				=> $vat ,
    				'box'				=> $box,
    				'vatTotalInsurance' => $vatTotalInsurance,
    				'subtotal'			=> $subtotal ,
    				'vatTotal'			=> $vatTotal,
    				'vatInsurance'		=> $vatInsurance ,
    				'boxBruto'			=> $boxBruto,
    				'insuranceBruto'	=> $insuranceBruto,
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal,
    				'flag_payed'		=> $flagPaid,
					'paragon'			=> $paragon,
    				'encabezado'		=> $encabezado,
					'paragon_cancel'	=> 1,
					'container'			=> $st->st_es_contenedor
    			];
				
		
    			$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    											
				$pdf = \App::make('dompdf.wrapper');
    			$pdf->loadHTML($view);
    			$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    									
    			//GENERA COPIA
    			/*
    			Log::info('Inicia generacion de copia ');
    	
    			$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
    	
    			$data =  [
    				'insurance' => $ins,
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' 				=> $nipDb,
    				'bank' 				=> $bankDb,
    				'account' 			=> $accountDb,
    				'invoiceNumber' 	=> $billing->bi_number_invoice,
    				'seller' 			=> $seller,
    				'2y' 				=> date("y"),
    				'currentDate' 		=> date("Y-m-d"),
					'paymentType' 		=> $pt,
					'clientName' 		=> $clientName,
					'clientAddress' 	=> $fullAddress,
					'clientNip' 		=> $clientNip,
					'clientPesel'		=> $dataUser->peselNumber,
					'clientType'		=> $dataUser->userType_id,
					'grandTotal' 		=> $st->price_per_month,
					'grandTotalString'  => $totalString,
					'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
					'dataUser'			=> $dataUser,
					'mes'				=> $mes,
					'year'				=> $year,
					'storage'			=> $st ,
					'vat'				=> $vat ,
					'box'				=> $box,
					'vatTotalInsurance' => $vatTotalInsurance,
					'subtotal'			=> $subtotal ,
    				'vatTotal'			=> $vatTotal,
    				'vatInsurance'		=> $vatInsurance ,
    				'boxBruto'			=> $boxBruto,
    				'insuranceBruto'	=> $insuranceBruto,
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal,
    				'flag_payed'		=> $flagPaid,
    				'copy'				=> 1,
    				'paragon'			=> $paragon,
    				'encabezado'		=> $encabezado,
    				'paragon_cancel'	=> 1
    			];
    	
    			$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    			$pdf = \App::make('dompdf.wrapper');
    			$pdf->loadHTML($view);
    	
    			$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
    			
    			$billing->bi_status_impresion = 'TERMINADO';
    			$billing->save();
    			*/						
    	
    		}
    			//FIN DE BATCH
    	}
    			else{
    				//ESTIMATOR
    					
    				if($billing->bi_flag_prepay == 0){
    	
    					$paymentData = PaymentData::find($billing->payment_data_id);
    						
    					$orderId = $paymentData->order_id;
    					$paymentType = $paymentData->payment_type_id;
    						
    					Log::info('OrderID: ' . $orderId );
    						
    					$items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
    					$order = Orders::where('id','=',$orderId)->first();
    					$userId = $order->user_id;
    					//return [$dataUser];
    					// id user is a client, the name is the real name if not, name is a company name
    						
    					// NIP
    					$clientNip = $dataUser->nipNumber;
    						
    					$subtotal = 0;
    						
    					//return ['items' => $items];
    					$idStorage = 0;
    						
    					foreach ($items as $key => $item) {
    						if($item->product_type == 'box'){
    							Log::info('IF');
    	
    							$idStorage = $item->product_id;
    							$desc3 = 0;
    							$nextMonth = 0;
    								
    							Log::info('idStorage: '.$idStorage);
    								
    							$term = $item->paymentData[0]->term;
    							$term = CataTerm::where('id','=',$term)->first();
    							$desc1 = ($term->off / 100);
    							$term = $term->months;
    							//return [$term,$desc1];
    								
    							$prepayMonths = $item->paymentData[0]->prepay;
    							$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
    							$desc2 = ($prepayMonths->off / 100);
    							$prepayMonths = $prepayMonths->months;
    								
    							Log::info('desc2: '.$desc2);
    							Log::info('prepayMonths: '.$prepayMonths);
    								
    							//return [$desc2,$prepayMonths];
    								
    							$cc = $item->paymentData[0]->creditcard;
    							if ($cc == 1) {
    								$ext = 1;
    							}else{
    								$ext = ($item->paymentData[0]->ext /100 ) + 1;
    							}
    								
    							//return [$ext];
    								
    							$ins = $item->paymentData[0]->insurance;
    								
    							Log::info('Ins. '.$ins);
    								
    							//return [$ins];
    								
    							$fecha = $item->rent_starts;
    							$price = $item->price;
    							$utilFecha = new Fecha();
    							$lastDay = $utilFecha->getUltimoDiaMes($fecha);
    							$lastDay = $utilFecha->getDia($lastDay);
    							//return [$lastDay];
    							$currentDay = $utilFecha->getDia($fecha);
    							//return [$lastDay,$currentDay];
    							$daysCurrent = $lastDay-$currentDay;
    							$pricePerDay = $price / $lastDay;
    							//return [$fecha,$lastDay,$daysCurrent,$pricePerDay];
    							$totalCurrentMonth = ($daysCurrent + 1) * $pricePerDay;
    							//return [$daysCurrent,$price,$pricePerDay,$totalCurrentMonth];
    								
    							//calculo del current insurance
    							$insurencePerDay =  $ins / $lastDay;
    							$currentInsurence = $insurencePerDay * ($daysCurrent + 1);
    								
    							Log::info('insurencePerDay: '.$insurencePerDay);
    							Log::info('currentInsurence: '.$currentInsurence);
    								
    							//caclculo primer descuento:
    							$descTermCurrent = $totalCurrentMonth - ($totalCurrentMonth * $desc1);
    							$descTermNext = $price - ($price * $desc1);
    							//return [$descTermCurrent,$descTermNext];
    								
    							Log::info('descTermCurrent: '.$descTermCurrent);
    							Log::info('descTermNext: '.$descTermNext);
    								
    							//calculo segundo desc
    							$descPrepayCurrent = $descTermCurrent - ($descTermCurrent * $desc2);
    							$descPrepayNext = $descTermNext - ($descTermNext * $desc2);
    							//return [$descPrepayCurrent,$descPrepayNext];
    								
    							//calculo tercer desc
    							$descCCCurrent = $descPrepayCurrent * $ext;
    							$descCCNext = $descPrepayNext * $ext;
    							//return [$descCCCurrent,$descCCNext];
    							Log::info('$descCCCurrent: '  . $descCCCurrent);
    							Log::info('$descCCNext: '. $descCCNext);
    	
    							//Mes extra antes de impuestos
    							//$deposit = $descCCCurrent + $descCCNext;
    							$deposit = $descCCNext;
    							Log::info('Deposit1: '.$deposit);
    							//return [$deposit];
    								
    							//sumamos el seguro
    							//$deposit = $deposit + $ins + $currentInsurence;
    							//Log::info('Deposit2: '.$deposit);
    							//return [$deposit];
    								
    							// calculamos los mese de prepago
    							$prepayTotal = ($descCCNext * $prepayMonths) + ($ins * $prepayMonths);
    							Log::info('PrepayTotal'. $prepayTotal);
    							//return [$prepayTotal];
    								
    							// calculamos el subtotal
    							//QUITAMOS EL DEPOSIT
    							$total = $prepayTotal; //+ $deposit;
    							Log::info('PrepayTotal'. $total);
    							//return [$total];
    								
    							//sacamos el precio total del next
    							$subtotalNextMonth = $descCCNext + $ins;
    							$vatNextMonth = $subtotalNextMonth * $vat;
    							$totalNextMonth = $subtotalNextMonth + $vatNextMonth;
    							//return [$totalNextMonth];
    								
    							$subtotal += $total;
    	
    								
    						}else{
    							//Entonces es un item
    							$price = $item->price;
    							$qt = $item->quantity;
    							$art = $item->articleData[0]->nombre;
    							$total = $price * $qt;
    								
    							//return ['Art' => $art,'Price' => $price, 'Quantity' => $qt,'Total' => $total];
    							$subtotal += $total;
    						}
    					}
    						
    					// El subtotal y el vat se calcula junto con los demas articulos.
    					$vat = $subtotal * $vat;
    					$granTotal = $subtotal + $vat;
    					//return ['Subtotal' => $subtotal,'vat' => $vat,'Total' => $granTotal];
    						
    					Log::info('***** Info recibida...');
    					Log::info('Subtotal: '. $subtotal);
    					Log::info('Vat: '.$vat);
    					Log::info('granTotal: '. $granTotal);
    						
    					//Generamos el nombre del pdf
    						
    					$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';
    					$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
    						
    					//CALUCLO D SUBTOTAL VAT
    					Log::info('***** Calculando totales');
    						
    					$paymentData = $st->StoragePaymentData->first();
    						
    					$ins = $paymentData->insurance;
    					$vatTag = $paymentData->pd_vat + 1;
    					$vat = $paymentData->pd_vat * 100;
    						
    					Log::info('Paymentdata ID: '.$paymentData->id);
    					Log::info('ins: '.$ins);
    					Log::info('vatTag: '.$vatTag);
    					Log::info('vat: '.$vat);
    						
    						
    					//$subtotal = round( $st->price_per_month / $vatTag , 2 );
    					//$vatTotal = round( $st->price_per_month - $subtotal , 2 );
    						
    					$subtotal = 0;
    					$vatTotal = 0;
    						
    	
    					$box = $subtotal - $ins;
    					$vatInsurance = round($ins * ($vatTag - 1), 2);
    					//$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
    					$vatTotalInsurance = 0;
    						
    					$insuranceBruto = round($ins + $vatInsurance, 2);
    	
    					$boxBruto = round( $box + $vatTotalInsurance,2);
    						
    					Log::info('Subtotal: '.$subtotal);
    					Log::info('vat total: '.$vatTotal);
    					Log::info('box: '. $box);
    					Log::info('vatInsurance: '. $vatInsurance);
    					Log::info('vatTotalInsurance: '. $vatTotalInsurance);
    					Log::info('vat '. $vat );
    					Log::info('***** ');
    					//FIN DE CALCULO
    	
    					// generar el pdf
    					//$data = $this->getData();
    					$ss = Storage::disk('invoices')->makeDirectory('1');
    					//dd($ss);
    						
    					$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total);
    	
    					$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    						
    						
    					//dd.mm-dd.mm.yyyy
    					//$startPeriod = $rentStartBilling;
    						
    					//RENT START
    					$startPeriod = $st->rent_start;
    	
    					$startPeriod = Carbon::parse($startPeriod)->toDateString();
    					Log::info('Start period invoice: '. $startPeriod);
    						
    					$arrStartPeriod = explode('-', $startPeriod);
    	
    					$endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->toDateString();
    					$arrEndPeriod = explode('-',$endPeriod);
    	
    					$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
    	
    					Log::info('Period: '.$period);
    					$year = $arrEndPeriod[0];
    						
    					switch($arrEndPeriod[1])
    					{
    						case '01':
    							$mes = 'styczeń';
    							break;
    						case '02':
    							$mes = 'luty';
    							break;
    						case '03':
    							$mes = 'marzec';
    							break;
    						case '04':
    							$mes = 'kwiecień';
    							break;
    						case '05':
    							$mes = 'maj';
    							break;
    						case '06':
    							$mes = 'czerwiec';
    							break;
    						case '07':
    							$mes = 'lipiec';
    							break;
    						case '08':
    							$mes = 'sierpień';
    							break;
    						case '09':
    							$mes = 'wrzesień';
    							break;
    						case '10':
    							$mes = 'październik';
    							break;
    						case '11':
    							$mes = 'listopad';
    							break;
    						case '12':
    							$mes = 'grudzień';
    							break;
    	
    					}
    	
    					$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    					//
    						
    					$data =  [
    							'insurance' => $paymentData->pd_insurance_price,
    							'vat' => $vat,
    							'vatDb' => $vatDb,
    							'place' => $placeDb,
    							'address' => $addressDb,
    							'nip' => $nipDb,
    							'bank' => $bankDb,
    							'account' => $accountDb,
    							'invoiceNumber' => $billing->bi_number_invoice,
    							'seller' => $seller,
    							'2y' => date("y"),
    							'currentDate' => date("Y-m-d"),
    							'clientName' => $clientName,
    							'clientAddress' => $fullAddress,
    							'clientNip' 		=> $clientNip,
    							'clientPesel'		=> $dataUser->peselNumber,
    							'clientType'		=> $dataUser->userType_id,
    							'grandTotal' 		=> $paymentData->pd_total,
    							'grandTotalString'	=> $totalString,
    							'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    							'mes'				=> $mes,
    							'year'				=> $year,
    							'storage'			=> $st    ,
							'paymentType' 		=> $pt,
							'dataUser'			=> $dataUser,
							'vat'				=> $vat ,
    								
    							'box'				=> $paymentData->pd_box_price,
    							'vatTotalInsurance' => $paymentData->pd_box_vat,
    							'subtotal'			=> $paymentData->pd_subtotal,
    	
    							'vatTotal'			=> $paymentData->pd_total_vat,
    							'vatInsurance'		=> $paymentData->pd_insurance_vat,
    	
    							'boxBruto'			=> $paymentData->pd_box_total,
    							'insuranceBruto'	=> $paymentData->pd_insurance_total,
    	
    							'accountant_number'	=> $dataUser->accountant_number,
    							'reciboFiscal'		=> $reciboFiscal,
    								
							'period'			=> $period,
    							'flag_payed'		=> $flagPaid,
    							'paragon'			=> $paragon,
    							'encabezado'		=> $encabezado,
    							'paragon_cancel'	=> 1,
    							'container'			=> $st->st_es_contenedor
    					];
    						
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    						$pdf = \App::make('dompdf.wrapper');
    					$pdf->loadHTML($view);
			
					$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    							
    							
    						//GENERA COPIA
    						/*
    						Log::info('Inicia generacion de copia ');
    						
    					$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
    								
    						
    					Log::info('$billing->user_id: '.$billing->user_id );
    					Log::info('$dataUser->userType_id: '. $dataUser->userType_id);
			
					$data =  [
    								'insurance' => $paymentData->pd_insurance_price,
							'vat' => $vat,
    								'vatDb' => $vatDb,
    								'place' => $placeDb,
    										'address' => $addressDb,
    										'nip' => $nipDb,
    												'bank' => $bankDb,
    														'account' => $accountDb,
    														'invoiceNumber' => $billing->bi_number_invoice,
    														'seller' => $seller,
							'2y' => date("y"),
    								'currentDate' => date("Y-m-d"),
    										'clientName' => $clientName,
    												'clientAddress' => $fullAddress,
    												'clientNip' 		=> $clientNip,
    												'clientPesel'		=> $dataUser->peselNumber,
    												'clientType'		=> $dataUser->userType_id,
    												'grandTotal' 		=> $paymentData->pd_total,
    												'grandTotalString'	=> $totalString,
    												'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    														'mes'				=> $mes,
    														'year'				=> $year,
    														'storage'			=> $st   ,
    														'paymentType' 		=> $pt,
    														'dataUser'			=> $dataUser,
							'vat'				=> $vat ,
    															
    														'box'				=> $paymentData->pd_box_price,
    														'vatTotalInsurance' => $paymentData->pd_box_vat,
    														'subtotal'			=> $paymentData->pd_subtotal,
    	
    														'vatTotal'			=> $paymentData->pd_total_vat,
    														'vatInsurance'		=> $paymentData->pd_insurance_vat,
    	
    														'boxBruto'			=> $paymentData->pd_box_total,
    														'insuranceBruto'	=> $paymentData->pd_insurance_total,
    	
							'accountant_number'	=> $dataUser->accountant_number,
    														'reciboFiscal'		=> $reciboFiscal,
			
    														'period'			=> $period,
    														'flag_payed'		=> $flagPaid,
    														'copy'				=> 1,
    														'paragon'			=> $paragon,
    														'encabezado'		=> $encabezado
    														];
			
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
					$pdf = \App::make('dompdf.wrapper');
    														$pdf->loadHTML($view);
    															
    														$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
    						
    														//Log::info('Fin de genera copia');
    																
    														//FIN DE GENERA COPIA
    														 * */
    														 
    														//return ['success' => true, 'pdf' => $pdfName];
    															
    														//return $pdfName;
    	
    														}else{
    														///ES UNA FACTURA DE PREPAY
    	
    														$paymentData = $st->StoragePaymentData->first();
    	
    				////////////////
    	
					$orderId = $paymentData->order_id;
    				$paymentType = $paymentData->payment_type_id;
    	
    																			
    				//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
					$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_CORRECTED.pdf';
    				$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
    																			
    				Log::info('PDF NAME: '.$pdfName);
    				
    				//ACTUALIZA NOMBRE DEL PDF
    				
    				//crear el pdf
    				$ss = Storage::disk('invoices')->makeDirectory('1');
    				//dd($ss);
    	
    				$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
			
					$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    	
    				$prepayMonths = $paymentData->prepay;
    				$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
    				$desc2 = ($prepayMonths->off / 100);
    				$prepayMonths = $prepayMonths->months;
    	
    				//FIX
    				$prepayMonths = $st->st_meses_prepago;
    				$vat = $paymentData->pd_vat * 100;
    	
    				Log::info('SE GENERA FACTURA DE PREPAY');
    				Log::info('PrepayMonths: ' . $prepayMonths);
    															
    				//$invoiceSecuence = new InvoiceSequence();
    				//$invoiceSecuence->save();
    															
    				//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
    															
    				//$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
    				//$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    	
    				//APLICA CUANDO ES DESDE ESTIMATOR
    				$rentStartBilling = $st->rent_start;
    				Log::info('$rentStartBilling: '. $rentStartBilling);
    	
    				$startPeriod = Carbon::parse($rentStartBilling)->startOfMonth()->addMonths(1)->toDateString();
    				Log::info('$startPeriod: '. $startPeriod);
    	
    				$startMonth = Carbon::parse($st->rent_start)->startOfMonth()->format('Y-m-d');
    				Log::info('$startMonth: '. $startMonth);
    	
    				if($startMonth == $st->rent_start){
    					//OJO ES PRIMERO DE MES
    					$startPeriod = Carbon::parse($st->rent_start)->startOfMonth()->toDateString();
    				}
    	
    				//SI ES POR ADD PREPAID MONTHS
    				if($billing->bi_batch == 2){
    					$startPeriod = Carbon::now()->startOfMonth()->addMonths(1)->toDateString();
    				}
    	
    				Log::info('StartPeriod: '. $startPeriod);
    	
    				$arrStartPeriod = explode('-', $startPeriod);
    	
    				$endPeriod = Carbon::parse($startPeriod)->addMonths( $prepayMonths - 1)->toDateString();
    				$endPeriod = Carbon::parse($endPeriod)->endOfMonth()->toDateString();
    	
					Log::info('endPeriod: '.$endPeriod);

					$arrEndPeriod = explode('-',$endPeriod);
    								
    				$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
    	
    				$year = $arrEndPeriod[0];
    	
    				//OJO::: CALCULA EL MES EN QUE ACABA EL PREPAYMENT
    				$mes = "";
    	
    				switch($arrEndPeriod[1])
    				{
    					case '01':
    						$mes = 'styczeń';
    					break;
    					case '02':
    						$mes = 'luty';
    					break;
    					case '03':
    						$mes = 'marzec';
    					break;
    					case '04':
    						$mes = 'kwiecień';
    					break;
    					case '05':
    						$mes = 'maj';
    					break;
    					case '06':
    						$mes = 'czerwiec';
    					break;
    					case '07':
    						$mes = 'lipiec';
    					break;
    					case '08':
    						$mes = 'sierpień';
    					break;
    					case '09':
    						$mes = 'wrzesień';
    					break;
    					case '10':
    						$mes = 'październik';
    					break;
    					case '11':
    						$mes = 'listopad';
    					break;
    					case '12':
    						$mes = 'grudzień';
						break;
    				}
    															
				$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    							//
    	
    								
    			Log::info('Period prepay: '.$period);

				//$boxNeto = round($paymentData->pd_box_price_prepay * $prepayMonths,2);
   	
				//$suma1 = round($boxNeto + $paymentData->pd_insurance_price_prepay,2);
				//$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
    	
				//CALCULA EL VALOR NETO DE LA CAJA
    			$cenaNetto = $paymentData->pd_box_price_prepay / $prepayMonths;
				$suma1 = round($paymentData->pd_box_price_prepay + $paymentData->pd_insurance_price_prepay,2);
    			$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
    	
    			$data =  [
    				'insurance_neto'	=> $paymentData->insurance,
    				'insurance' => $paymentData->pd_insurance_price_prepay,
    				'vat' => $vat,
    				'vatDb' => $vatDb,
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' => $nipDb,
    				'bank' => $bankDb,
    				'account' => $accountDb,
    				'invoiceNumber' => $billing->bi_number_invoice,
    				'seller' => $seller,
    				'2y' => date("y"),
    				'currentDate' => date("Y-m-d"),
    				'paymentType' => $pt,
    				'clientName' => $clientName,
    				'clientAddress' => $fullAddress,
					'clientNip' 		=> $clientNip,
    				'clientPesel'		=> $dataUser->peselNumber,
    				'clientType'		=> $dataUser->userType_id,
					'grandTotal' 		=> $paymentData->pd_total_prepay,
					'grandTotalString'	=> $totalString,
					'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    				'mes'				=> $mes,
    				'year'				=> $year,
    				'storage'			=> $st    ,
    														
    				'dataUser'			=> $dataUser,
    				'vat'				=> $vat ,
    				'cenaNetto'			=> $cenaNetto,
					'box'				=> $paymentData->pd_box_price_prepay,
    				'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
    				'subtotal'			=> $paymentData->pd_subtotal_prepay,
				
					'vatTotal'			=> $paymentData->pd_total_vat_prepay,
					'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
    										
    				'boxBruto'			=> $paymentData->pd_box_total_prepay,
					'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
    								
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal,
				
    				'period'			=> $period,
    				'flag_payed'		=> $flagPaid,
    				'mesesPrepago'		=> $prepayMonths,
    				'suma1'				=> $suma1,
    				'suma2'				=> $suma2,
    				'paragon'			=> $paragon,
    				'encabezado'		=> $encabezado,
    				'paragon_cancel'	=> 1,
    				'container'			=> $st->st_es_contenedor	
    					
    			];
    														
    			$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    			$pdf = \App::make('dompdf.wrapper');
				$pdf->loadHTML($view);
    	
    	
    			$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    		
    			Log::info('PDF NAME: '.$pdfName);
    			Log::info('Finaliza pdf de prepay');
    	
    	
	    		//	GENERA COPIA
	    		/*
    			Log::info('Inicia generacion de copia ');
    																						
				$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
    																						
    			$data =  [
    					'insurance_neto'	=> $paymentData->insurance,
    					'insurance' => $paymentData->pd_insurance_price_prepay,
    					'vat' => $vat,
    					'vatDb' => $vatDb,
    					'place' => $placeDb,
    					'address' => $addressDb,
    					'nip' => $nipDb,
    					'bank' => $bankDb,
    					'account' => $accountDb,
    					'invoiceNumber' => $billing->bi_number_invoice,
						'seller' => $seller,
    					'2y' => date("y"),
    					'currentDate' => date("Y-m-d"),
    					'paymentType' => $pt,
    					'clientName' => $clientName,
    					'clientAddress' => $fullAddress,
    					'clientNip' 		=> $clientNip,
    					'clientPesel'		=> $dataUser->peselNumber,
    					'clientType'		=> $dataUser->userType_id,
    					'grandTotal' 		=> $paymentData->pd_total_prepay,
    					'grandTotalString'	=> $totalString,
    					'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    					'mes'				=> $mes,
    					'year'				=> $year,
    					'storage'			=> $st    ,
		    			'dataUser'			=> $dataUser,
    					'vat'				=> $vat ,
    					'cenaNetto'			=> $cenaNetto,
    					'box'				=> $paymentData->pd_box_price_prepay,
    					'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
    					'subtotal'			=> $paymentData->pd_subtotal_prepay,
    					'vatTotal'			=> $paymentData->pd_total_vat_prepay,
    					'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
    					
    					'boxBruto'			=> $paymentData->pd_box_total_prepay,
    					'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
    					'accountant_number'	=> $dataUser->accountant_number,
    					'reciboFiscal'		=> $reciboFiscal,
    					
    					'period'			=> $period,
    					'flag_payed'		=> $flagPaid,
    					'mesesPrepago'		=> $prepayMonths ,
    					'copy'				=> 1,
    
    					'suma1'				=> $suma1,
    					'suma2'				=> $suma2,
    					'paragon'			=> $paragon,
    					'encabezado'		=> $encabezado
    				];
    				
    				$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    				$pdf = \App::make('dompdf.wrapper');
    				$pdf->loadHTML($view);
    				
    				$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
    	*/
    				}
    				//FIN DE ESTIMATOR
    			}
    	
    	
    	
    	
    	//-------------------------
    	//--- FIN DE PDF
		//---------
    	
    	$abono->pdf 		= $pdfName;
    	$abono->save();
    	
    	$billing->id_billing_correccion	= $abono->id;
    	$billing->pdf_corregido			= $pdfName;
    	$billing->flag_payed			= 	1;
    	$billing->save();
    	
    	return ['returnCode'	=> 200 	, 'msg'		=> 'Paragon cancelled'];
    }
    //
    
    public function downloadPDF($id)
    {
    	
    	$mimeType = "application/pdf";
    	
    	$billing = Billing::find($id);
    	
    	//$filePath = Storage::disk('app')->get('/invoices');
    	//$filePath  = 'storages/app/invoices';
		$fileName = $billing->pdf;
		
		$filePath = storage_path('app/invoices/'.$fileName);
    	
    	//return (new Response($filePath, 200))->header('Content-Type', $mimeType)->header('Content-Disposition', 'attachment; filename="'.$fileName.'"');
    	
    	return Response::make(file_get_contents($filePath), 200, [
    			'Content-Type' => $mimeType,
    			'Content-Disposition' => 'attachment; filename="'.$fileName.'"']);
    	
    	
    	
    	/*
    	$comprobante = Comprobante::where($match)->first();
    	
    	if($type=="XML"){
    		$mimeType = "applicaction/xml";
    		$filePath = $comprobante->cm_path_xml;
    		$fileName = basename($filePath).".xml";
    	}else if($type == "PDF"){
    		$mimeType = "application/pdf";
    		$filePath = $comprobante->cm_path_pdf;
    		$fileName = basename($filePath).".pdf";
    	}
    	
    	return Response::make(file_get_contents($filePath), 200, [
    			'Content-Type' => $mimeType,
    			'Content-Disposition' => 'attachment; filename="'.$fileName.'"']);
    			
    			*/
    }
    
    public function downloadPDFCorrected($id)
    {
    	 
    	$mimeType = "application/pdf";
    	 
    	$billing = Billing::find($id);
    	 
    	//$filePath = Storage::disk('app')->get('/invoices');
    	//$filePath  = 'storages/app/invoices';
    	$fileName = $billing->pdf_corregido;
    
    	$filePath = storage_path('app/invoices/'.$fileName);
    	 
    	//return (new Response($filePath, 200))->header('Content-Type', $mimeType)->header('Content-Disposition', 'attachment; filename="'.$fileName.'"');
    	 
    	return Response::make(file_get_contents($filePath), 200, [
    			'Content-Type' => $mimeType,
    			'Content-Disposition' => 'attachment; filename="'.$fileName.'"']);
    }
    
    public function downloadPDFCopy($id)
    {
    	 
    	$mimeType = "application/pdf";
    	 
    	$billing = Billing::find($id);
    	 
    	$pdfName = $billing->pdf;
    	$arrPDFName = explode('.',$pdfName);
    	$pdfNameCopy = $arrPDFName[0].'_COPY'.'.'.$arrPDFName[1];
    
    	$filePath = storage_path('app/invoices/'.$pdfNameCopy);
    	 
    	//return (new Response($filePath, 200))->header('Content-Type', $mimeType)->header('Content-Disposition', 'attachment; filename="'.$fileName.'"');
    	 
    	return Response::make(file_get_contents($filePath), 200, [
    			'Content-Type' => $mimeType,
    			'Content-Disposition' => 'attachment; filename="'.$pdfNameCopy.'"']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
	}
	
	public function payRetryCharge($suscription_id, $amount, $billing_id = false)
	{
		Log::info("*******************************");
		Log::info("***RETRY CHARGE****************");

		$result = BraintreeHelper::retryCharge($suscription_id, $amount);

		Log::info('Returned: '. $result);

		if($result->success){
			$msg	= "Charged successfully";

			if(!is_null($billing_id)){
				$billing = Billing::find($billing_id);
				$billing->flag_payed = 1;
				$billing->save();
			}

		}else{
			$msg 	= $result->message;
		}
		
		Log::info("*******************************");

		return [
			'result'	=> $result , 
			'msg'		=> $msg
		];		
	}

	public function confirmCorrectInvoice($id_encriptado)
	{
		$id = base64_decode($id_encriptado);

		$bill = BillingConfirmation::where([
			'billing_id'	=> $id
		])->first();

		if(is_null($bill)){
			//STORE
			BillingConfirmation::create([
				'billing_id'	=> $id
			]);

			return view('confirmation_pw',[
				'success'	=> '1'
			]);
		}else{
			return view('confirmation_pw',[
				'success'	=> '0'
			]);
		}
	}
}
