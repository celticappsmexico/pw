<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\History;
use App\Http\Fecha;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pw.history');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $data){

        $username = $data['username'];
        $name = $data['name'];
        $lastName = $data['lastName'];
        $company = $data['company'];
        $alias = $data['alias'];
        $sqm = $data['sqm'];
        $price = $data['price'];
        $start = $data['rent_start'];
        $end = $data['rent_end'];

        $his = History::with(['HistoryClient','HistoryStorage'])
                        ->whereHas('HistoryClient',function($query) use ($username){
                                $query->where('username','LIKE','%'.$username.'%');
                            })
                        ->whereHas('HistoryClient',function($query) use ($name, $lastName){
                                if ($name == "") {
                                    $query->where('name','LIKE','%'.$name.'%');
                                }else {
                                    if ($name != '' and $lastName != '') {
                                        $query->where('name','LIKE','%'.$name.'%')
                                        ->where('lastName','LIKE','%'.$lastName.'%');
                                    }elseif ($name != '') {
                                        $query->where('name','LIKE','%'.$name.'%')
                                        ->orWhere('lastName','LIKE','%'.$name.'%');
                                    }
                                }
                            })
                        ->whereHas('HistoryClient',function($query) use ($company){
                                $query->where('companyName','LIKE','%'.$company.'%');
                            })
                        ->whereHas('HistoryStorage',function($query) use ($alias){
                                if ($alias != "") {
                                    $query->where('alias','=', $alias);
                                }else {
                                    $query->where('alias','LIKE','%'.$alias.'%');
                                }
                            })
                        ->whereHas('HistoryStorage',function($query) use ($sqm){
                                if ($sqm != '') {
                                    $query->where('sqm','=',$sqm);
                                }
                            })
                        ->where(function($query) use ($price){
                            if ($price !='') {
                                $query->where('price_per_month','=',$price);
                            }
                        })
                        ->where(function($query) use ($start,$end){
                                    if ($start != '') {
                                        $fechaUtil = new Fecha();
                                        $start = $fechaUtil->fechaTipoMysql($start);
                                        $end = $fechaUtil->fechaTipoMysql($end);
                                        //dd($start);
                                        $query->whereBetween(\DB::raw('DATE(`rent_start`)'),array($start,$end));
                                    }
                                })
                        ->get();

        $cad = '';
        foreach ($his as $key => $h) {
            $cad .= '<tr>';
            $cad .= '<td>'.$h->HistoryStorage[0]->alias.'</td>';
            $cad .= '<td>'.$h->HistoryStorage[0]->sqm.'</td>';
            $cad .= '<td>'.$h->price_per_month.'</td>';
            $cad .= '<td>'.$h->HistoryClient[0]->username.'</td>';
            $cad .= '<td>'.$h->HistoryClient[0]->name.' '.$h->HistoryClient[0]->lastName.'</td>';
            $cad .= '<td>'.$h->HistoryClient[0]->companyName.'</td>';
            $cad .= '<td>'.$h->rent_start.'</td>';
            $cad .= '<td>'.$h->rent_end.'</td>';
            $cad .= '<td>nova.admin</td>';
            $cad .= '</tr>';
        }

        return $cad;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
