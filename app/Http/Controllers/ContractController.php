<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contract;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contracts.contracts_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if( $request->hasFile('contract_file') ){
    		
    		$file = Input::file('contract_file');
            
            $destinationPath = storage_path()."/app/app_contract"; // upload path
    		$extension = Input::file('contract_file')->getClientOriginalExtension(); // getting image extension
            
            if($extension != "pdf"){
                return [    
                        'returnCode'    => '400',
                        'msg'           => 'Invalid format'
                ];
            }

            Contract::where('co_current', true)->update(['co_current' => false]);
            
            $contract = Contract::create([
                'co_current'    => true,
                'co_path'       => "Pending"
            ]);
            
            $fileName = 'Contract_'.$contract->id.'.'.$extension; // renameing image

            
    		$uploadSuccess = $file->move($destinationPath, $fileName); // uploading file to given path
    		
    		if($uploadSuccess){
                
                $contract->co_path = $fileName;
                $contract->save();
                
	    		return [    
                        'returnCode'    => '200',
                        'msg'           => 'Contract updated'
                ];
    		}
    		else{

    			return ['returnCode' => '200' , 'msg' => "Somenthing was wrong",
    					'msgType' => 'error'];
    			
    		}
    	}
    	else {
    		 
    		return ['returnCode' => '200' , 'msg' => "Somenthing was wrong",
    				'msgType' => 'error'];
    	}

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function currentContract(Request $request)
	{

		
		$mimeType = "application/pdf";
			
		$contract = Contract::where([
						'co_current'	=> true
					])->first();
			
		$fileName = $contract->co_path;
		
		$filePath = storage_path('app/app_contract/'.$fileName);
		
		//return (new Response($filePath, 200))->header('Content-Type', $mimeType)->header('Content-Disposition', 'attachment; filename="'.$fileName.'"');
		
		return Response::make(file_get_contents($filePath), 200, [
				'Content-Type' => $mimeType,
				'Content-Disposition' => 'attachment; filename="'.$fileName.'"']);
		
	}
	
}
