<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('emails.plantilla');
        //$token = csrf_token();
        //return $token;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function sendRegisterMail($id){
      //guarda el valor de los campos enviados desde el form en un array
      //$data = $request->all();
      //$solicitud_id = $request->get('solicitud_id_correo');

      $fechaMail= \Carbon\Carbon::now();
      $userInfo = User::where('id', '=', $id)->first();

      $send = \Mail::send('emails.plantilla', ['userInfo' => $userInfo,'fecha'=>$fechaMail], function($message) use($userInfo)
       {
            $subject = 'Welcome to PW';
           //remitente
           $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));

           //asunto
           $message->subject($subject);

           //receptor
           //$message->to('nalonsor@outlook.com', $userInfo->name);

       });
       //return \View::make('success');

       return ['success' => 'true','sender' => $send];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
