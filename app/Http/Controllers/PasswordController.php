<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\User;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postReset(Request $request)
    {
        Log::info('*******************************');
        Log::info('PasswordController@postReset');
        //Log::info($request);
        Log::info("Email: ". $request->get('email'));
        Log::info('Password: '. $request->get('password'));
        Log::info('Password Confirmation: '. $request->get('password_confirmation'));
        Log::info("Token: ". $request->get('token'));
        Log::info("---------------------------");

        if($request->get('password') != $request->get('password_confirmation')){
            return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => "Passwords do not match"]);            
        }

        $user = User::where([
                    'email'  => $request->get('email'),
                    'active' => true
                ])->first();

        if(is_null($user)){
            return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => "Email not registered"]);            
        }

        $user->password = bcrypt("password");
        $user->save();
        
        return redirect()->route('password/response')
                ->with('success', 'It works!');
                //->withSuccess(['email' => "Password reset successfully"]);
                //->with('status', "Success");
                    
        //return redirect()->route('home')->with('status', "Success");
/*

        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        Log::info("Response: ". $response);

        switch ($response) {
            case Password::PASSWORD_RESET:
                Log::info($this->redirectPath());
                Log::info($response);
                //return redirect($this->redirectPath())->with('status', trans($response));
                return redirect()->route('home')->with('status', trans($response));
            default:
                Log::info('Default');
                Log::info('Response: ' . trans($response));

                return redirect()->back()
                            ->withInput($request->only('email'))
                            ->withErrors(['email' => trans($response)]);
        }*/
    }

    public function response()
    {
        return view('auth.password_response');
    }

}
