<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Levels;

class LevelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        return view('pw.levels',['idLevel' => $id]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        //dd($data);
        $success = Levels::create([
            'name' => $data['name'],
            'warehouse_id' => $data['idBuilding'],
            'active'=> '1'
        ]);

        if ($success->id) {
        return['success' => true,
                'level' => $data
              ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $column = 'id';
        $data = Levels::where($column,'=', $request['id'])->where('Active', '!=', '0')->first();
        if ($request->ajax()) {
            return['success' => true,
                'building' => $data
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $column = 'id';
        $level = Levels::where($column,'=', $request['idLevel'])->first();

        $level->name = $request['name'];
        $res = $level->save();

        if ($request->ajax()) {
            return ['res' => $res];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $column = 'id';
        $data = Levels::where($column,'=', $request['id'])->first();

        $data->active = 0;
        $res = $data->save();
        return ['res'=>$res];
    }

    public function getLevelsList(Request $data){
      $levels = Levels::where('warehouse_id', '=', $data['id'])->where('active', '!=', '0')->get();

      $cad = '';
      foreach ($levels as $key => $level) {
        $cad .= '<tr>';
        $cad .= '<td>'.$level->name.'</td>';
        $cad .= '<td><a href="javascript:void(0);" id="'.$level->id.'" class="btn btn-default btn-xs editLevel"><i class="fa fa-pencil" aria-hidden="true"></i> </a>';
        $cad .= '<a href="javascript:void(0);" id="'.$level->id.'" class="btn btn-primary btn-xs btnDelLevel"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
        $cad .= '</tr>';
      }

      return $cad;
    }

}
