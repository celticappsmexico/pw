<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Storages;
use App\PaymentData;
use Carbon\Carbon;
use App\Billing;
use App\BillingDetail;
use App\Http\Fecha;
use App\CataTerm;
use App\CataPrepay;
use App\Deposito;
use Illuminate\Support\Facades\Auth;
use App\History;
use Illuminate\Support\Facades\Log;
use App\Library\Formato;
use Illuminate\Support\Facades\DB;
use App\CataPaymentTypes;
use App\User;
use App\ConfigSystem;
use App\Library\MoneyString;
use App\Countries;
use App\InvoiceSequence;
use App\ParagonSequence;
use Illuminate\Support\Facades\Storage;
use App\ReciboSequence;
use App\Library\EstimatorHelper;
use App\DetOrders;
use App\Orders;
use App\Library\BraintreeHelper;
use Yajra\Datatables\Datatables;

class StoragesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $storages = Storages::with('StorageOwner')
                              ->where('level_id', '=', $id)->where('active', '!=', '0')
                              ->get();
        return view('pw.storages',['idLevel' => $id,'storages' => $storages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {

        //dd($data);

        isset($data['text_posx']) ? $text_posx = $data['text_posx'] : $text_posx = '';
        isset($data['text_posy']) ? $text_posy = $data['text_posy'] : $text_posy = '';
        isset($data['size']) ? $size = $data['size'] : $size = '';
        isset($data['rect_posx']) ? $rect_posx = $data['rect_posx'] : $rect_posx = '';
        isset($data['rect_posy']) ? $rect_posy = $data['rect_posy'] : $rect_posy = '';
        isset($data['rect_width']) ? $rect_width = $data['rect_width'] : $rect_width = '';
        isset($data['rect_height']) ? $rect_height = $data['rect_height'] : $rect_height = '';
        isset($data['rect_background']) ? $rect_background = $data['rect_background'] : $rect_background = '';
        isset($data['class']) ? $class = $data['class'] : $class = '';
        isset($data['user_id']) ? $user_id = $data['user_id'] : $user_id = '';
        isset($data['rent_start']) ? $rent_start = $data['rent_start'] : $rent_start = '';
        isset($data['rent_end']) ? $rent_end = $data['rent_end'] : $rent_end = '';
        isset($data['comments']) ? $comments = $data['comments'] : $comments = '';

        $success = Storages::create([
                    'alias' => $data['alias'],
                    'text_posx' => $text_posx,
                    'text_posy' => $text_posy,
                    'size' => $size,
                    'rect_posx' => $rect_posx,
                    'rect_posy' => $rect_posy,
                    'rect_width' => $rect_width,
                    'rect_height' => $rect_height,
                    'rect_background' => $rect_background,
                    'class' => $class,
                    'level_id' => $data['idLevel'],
                    'user_id' => $user_id,
                    'sqm' => $data['sqm'],
                    'price' => $data['price'],
                    'rent_start' => $rent_start,
                    'rent_end' => $rent_end,
                    'comments' => $comments,
                    'active' => 1
        ]);

        if ($success->id) {
        return['success' => true,
                'level' => $data
              ];
        }

    }
    
    public function createPrepay($storage_id)
    {
    	$storage = Storages::find($storage_id);
    	
    	return view('storages.storage_prepay_create',[
    			'storage_id'	=> 	$storage_id ,
    			'storage'		=> 	$storage
    	]);
    }
    
    public function updatePrepay(Request $request)
    {
        Log::info('StoragesControllers@updatePrepay');
    	$st = Storages::find($request->get('storage_id'));
    	$st->st_meses_prepago = $request->get('st_meses_prepago');
    	$st->save();
    	
    	//LOGICA PARA GENERAR FACTURA
    	//
    	Log::info('StoragesController@updatePrepay');
		Log::info('Storage ID '. $st->id);
		Log::info('Meses Prepago: '. $request->get('st_meses_prepago'));
		if($request->get('mark_paid') == 'on'){
			$mark_paid = 2;
		}else{
			$mark_paid = 0;
		}
		
		Log::info('Mark Paid: '. $mark_paid );
    	
    	$paymentData = $st->StoragePaymentData->first();
    	
    	if ($paymentData->creditcard) {
    		$pt = CataPaymentTypes::find(2);
    		$pt = $pt->cpt_lbl_factura;
    	}else{
    		$pt = CataPaymentTypes::find(3);
    		$pt = $pt->cpt_lbl_factura;
    	}
    	
    	Log::info('PT: '.$pt);
    	
    	$dataUser = User::with(['UserAddress'])->find($st->user_id);
    	
    	$paragon = 0;
    	 
    	if($dataUser->userType_id == 1){
    		//GENERA FISCAL RECIPT
    		$paragon = 1;
    	}
    	
    	$paragon = 0;
    	
    	//return [$dataUser];
    	// id user is a client, the name is the real name if not, name is a company name
    	if($dataUser->userType_id == 1){
    		isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
    		isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
    		$clientName = $userName.' '.$userLast;
    	}else{
    		//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
    		isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
    		$clientName = $userName;
    	}
    	
    	Log::info('Client name: '. $clientName);
    	
    	// NIP
    	$clientNip = $dataUser->nipNumber;
    	
    	Log::info('Nip number: '.$clientNip);
    	
    	// Address
    	if($dataUser->UserAddress[0]->street != '' ){
    		 
    		$street = $dataUser->UserAddress[0]->street;
    		$numExt = $dataUser->UserAddress[0]->number;
    		$numInt = $dataUser->UserAddress[0]->apartmentNumber;
    		$city = $dataUser->UserAddress[0]->city;
    		$country = $dataUser->UserAddress[0]->country_id;
    		$country = Countries::find($country);
    		$country = $country->name;
    		$cp = $dataUser->UserAddress[0]->postCode;
    	
    		if($numInt != "")
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt. ' , '.$country;
    		else
    			$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ' , '.$country;
    		 
    		
    	}else{    		
    		$fullAddress = '';
    	}
    	
    	Log::info('Full Address: '.$fullAddress);
    	
    	if ($paymentData != null) {
    	
    		$activePay = $paymentData->id;
    	
    		Log::info('Active Pay: '.$activePay);
    	
    		$data = ConfigSystem::all();
    	
    		//$ins = $data[0]->value;
    		$quantity = $request->get('st_meses_prepago');
    		
    		$ins = $paymentData->insurance;
    		
    		$vatTag = $paymentData->pd_vat + 1;
    		$vat = $paymentData->pd_vat * 100;
    	
    		$placeDb = $data[2]->value;
    		$addressDb = $data[3]->value;
    		$nipDb = $data[4]->value;
    		$bankDb = $data[5]->value;
    		$accountDb = $data[6]->value;
    		$invoiceNumberDb = $data[7]->value;
    		$seller = $data[8]->value;
    	
    		// increments invoice number:
    		$invoiceNumberDb = (int)$invoiceNumberDb;
    		//return [$invoiceNumberDb];
    		$invoiceNumberDb = $invoiceNumberDb + 1;
    		//return [$invoiceNumberDb];
    		$newInvoiceNumberDb = sprintf( '%04d', $invoiceNumberDb );
    		$data[7]->value = $newInvoiceNumberDb;
    		$res = $data[7]->save();
    		 
    		//$invoiceSecuence = new InvoiceSequence();
    		//$invoiceSecuence->save();
    	
    		//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
    		//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
    	
    		//crear el pdf
    		$ss = Storage::disk('invoices')->makeDirectory('1');
    		//dd($ss);
    		 
    		$granTotal = $st->price_per_month * $quantity;
    		
    		$totalString = MoneyString::transformQtyCurrency($granTotal);
    		 
    		$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
    		 
    		//Log::info('Formula Gran total : '. $st->price_per_month .'*'. $request->get('st_meses_prepago'));
    		Log::info("Gran Total : ". $granTotal);
    		Log::info("Gran Total String: ". $totalString);
    		
    		//OBITIENE MES
    		setlocale(LC_ALL, 'de_DE');
    		 
    		$mes = "";
    		 
    		switch(Carbon::now()->format('m'))
    		{
    			case '01':
    				$mes = 'styczeń';
    				break;
    			case '02':
    				$mes = 'luty';
    				break;
    			case '03':
    				$mes = 'marzec';
    				break;
    			case '04':
    				$mes = 'kwiecień';
    				break;
    			case '05':
    				$mes = 'maj';
    				break;
    			case '06':
    				$mes = 'czerwiec';
    				break;
    			case '07':
    				$mes = 'lipiec';
    				break;
    			case '08':
    				$mes = 'sierpień';
    				break;
    			case '09':
    				$mes = 'wrzesień';
    				break;
    			case '10':
    				$mes = 'październik';
    				break;
    			case '11':
    				$mes = 'listopad';
    				break;
    			case '12':
    				$mes = 'grudzień';
    				break;
    				 
    		}
    		
    	
    		$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
    		 
    		$year = Carbon::now()->year;
    		 
    		//CALUCLO D SUBTOTAL VAT
    		$subtotal = round( $st->price_per_month / $vatTag , 2 );
    		$vatTotal = round( $st->price_per_month - $subtotal , 2 );
    		 
    		$box = $subtotal - $ins;
    		$boxNeto = round( $box * $quantity , 2 );
    		
    		$vatInsurance = round($ins * ($vatTag - 1), 2);
    		$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
    		$vatTotalInsurance = round($vatTotalInsurance * $quantity , 2);
    		
    		$insuranceBruto = round($ins + $vatInsurance, 2);
    		 
    		$boxBruto = round( $boxNeto + $vatTotalInsurance ,2);
    		
    		Log::info('Formula Box Bruto :  round( ('.$box.' + '.$vatTotalInsurance.' ) * '.$quantity.' ,2)');
    		
    		Log::info('***** Calculando totales');
    		Log::info('Subtotal: '.$subtotal);
    		Log::info('vat total: '.$vatTotal);
    		Log::info('box: '. $box);
    		Log::info('vatInsurance: '. $vatInsurance);
    		Log::info('vatTotalInsurance: '. $vatTotalInsurance);
    		Log::info('vat '. $vat );
    		Log::info('***** ');
    		//FIN DE CALCULO
    		
    		$reciboSequence = new ReciboSequence();
    		$reciboSequence->save();
    		
    		$reciboFiscal = str_pad($reciboSequence->id,6,"0",STR_PAD_LEFT);
    		
    		$reciboFiscal = "W".$reciboFiscal;
    		 
    		Log::info("Recibo Fiscal: ". $reciboFiscal);
    		 
    		if($paragon == 0)
    		{
    			$invoiceSecuence = new InvoiceSequence();
    			$invoiceSecuence->is_contador =  $reciboSequence->id;
    			$invoiceSecuence->save();
    		}
    		else{
    			$invoiceSecuence = new ParagonSequence();
    			$invoiceSecuence->save();
    		}
    		
    		$paymentData->pd_box_price_prepay		=	$boxNeto;
    		$paymentData->pd_box_vat_prepay			=	$vatTotalInsurance;
    		$paymentData->pd_box_total_prepay		=	$boxBruto;
    		$paymentData->pd_insurance_price_prepay	=	round($ins * $quantity, 2);
    		$paymentData->pd_insurance_vat_prepay	=	round($vatInsurance * $quantity, 2 );
    		$paymentData->pd_insurance_total_prepay	=	round($insuranceBruto * $quantity, 2);

            $subtotal_billing = $boxNeto+round($ins * $quantity, 2);
            $vat_billing = $vatTotalInsurance+round($vatInsurance * $quantity, 2 );

    		$paymentData->pd_subtotal_prepay		=	$subtotal_billing;
    		$paymentData->pd_total_vat_prepay		=	$vat_billing;
    		$paymentData->pd_total_prepay			=	$granTotal;
    		$paymentData->save();
    		 
    		
    		$data =  [
    				'insurance' 		=> round($ins * $quantity, 2),
    				'place' => $placeDb,
    				'address' => $addressDb,
    				'nip' 				=> $nipDb,
    				'bank' 				=> $bankDb,
    				'account' 			=> $accountDb,
    				'invoiceNumber' 	=> "",
    				'seller' 			=> $seller,
    				'2y' 				=> date("y"),
    				'currentDate' 		=> date("Y-m-d"),
    				'paymentType' 		=> $pt,
    				'clientName' 		=> $clientName,
    				'clientAddress' 	=> $fullAddress,
    				'clientNip' 		=> $clientNip,
    				'clientPesel'		=> $dataUser->peselNumber,
    				'clientType'		=> $dataUser->userType_id,
    				'grandTotal' 		=> $granTotal,
    				'grandTotalString' 	=> $totalString,
    				'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
    				'dataUser'			=> $dataUser,
    				'mes'				=> $mes,
    				'year'				=> $year,
    				'storage'			=> $st ,
    				'vat'				=> $vat ,
    	
    				'box'				=> $box,
    				'boxNeto'			=> $boxNeto,
    				'vatTotalInsurance' => $vatTotalInsurance,
    				'subtotal'			=> $subtotal ,
    					
    				'vatTotal'			=> $vatTotal,
    				'vatInsurance'		=> round($vatInsurance * $quantity, 2 ) ,
    					
    				'boxBruto'			=> $boxBruto,
    				'insuranceBruto'	=> round($insuranceBruto * $quantity, 2),
    				
    				'mesesPrepago'		=> $request->get('st_meses_prepago'),
    				
    				'accountant_number'	=> $dataUser->accountant_number,
    				'reciboFiscal'		=> $reciboFiscal
    					
    		];
    		 
    	
    		///$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
    		
    		//$pdf = \App::make('dompdf.wrapper');
    		//$pdf->loadHTML($view);
    		//$pdf->save(storage_path().'/app/invoices/'.$pdfName);
    	
    		//
    		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $st->user_id, 'storage_id' => $st->id])->first();
    		Log::info('Search for: user_id '.$st->user_id . ', storage_id : '.$st->id);
    	
    		if(!is_null($billingUpdate))
    		{
    			$saldo = $billingUpdate->saldo;
    			$billingUpdate->bi_flag_last = 0;
    			$billingUpdate->save();
    		}
    		else{
    			$saldo = 0;
    		}
    		//
    	
    		//GENERA CARGO
    		$cargo = $st->price_per_month * $quantity;
    		/*
    		$billing = new Billing();
    		$billing->user_id = $st->user_id;
    		$billing->storage_id = $st->id;
    		$billing->pay_month = Carbon::now()->format("Y-m-d");
    		$billing->flag_payed = 0;
    		$billing->abono = 0;
    		$billing->cargo = $cargo;
    		$billing->saldo = ($cargo * -1) + $saldo;
    		//$billing->pdf = $pdfName;
    		$billing->reference = '';
    		$billing->bi_flag_last = 0;
    		$billing->bi_subtotal = $subtotal;
    		$billing->bi_porcentaje_vat = $vat;
    		$billing->bi_total_vat = $vatTotal;
    		$billing->bi_total = $st->price_per_month;
    		$billing->bi_flag_prepay = 1;
    		$billing->save();
    		*/
    		
    		Log::info('');
    		
    		//GENERAMOS EL CARGO DE PREPAY
    		$billing = new Billing();
    		$billing->user_id = $st->user_id;
    		$billing->storage_id = $st->id;
    		$billing->pay_month = date("Y-m-d");
    		$billing->flag_payed = $mark_paid;
    		$billing->abono = 0;
    		$billing->cargo = $paymentData->pd_total_prepay;
    		$billing->saldo = ($paymentData->pd_total_prepay * -1) + $saldo;
    		//$billing->pdf = $pdfName;
    		$billing->reference = '';
    		$billing->bi_flag_last	= 1;
    		$billing->bi_subtotal 			= $paymentData->pd_subtotal_prepay;
    		$billing->bi_porcentaje_vat 	= $vat;
    		$billing->bi_total_vat 			= $paymentData->pd_total_vat_prepay;
    		$billing->bi_total 				= $paymentData->pd_total_prepay;
    		$billing->bi_status_impresion 	= 'PENDIENTE PDF';
    		$billing->bi_recibo_fiscal 		= 0;
    		$billing->bi_number_invoice 	= $invoiceSecuence->id;
    		$billing->bi_year_invoice		= date("y");
    		$billing->bi_flag_prepay		= "1";
    		$billing->bi_batch				= 2;
    		$billing->payment_data_id		= $paymentData->id;
			$billing->bi_paragon 			= $paragon;
			$billing->bi_months_invoice 	= $request->get('st_meses_prepago');
    		$billing->save();
    		
    		
			//GENERA ABONO
			
    		$saldoNuevo = $saldo + $cargo; 
			
    		/***
    		$billingAbono = new Billing();
    		$billingAbono->user_id = $st->user_id;
    		$billingAbono->storage_id = $st->id;
    		$billingAbono->pay_month = Carbon::now()->format("Y-m-d");
    		$billingAbono->flag_payed = 1;
    		$billingAbono->abono = $cargo;
    		$billingAbono->cargo = 0;
    		$billingAbono->saldo = $saldoNuevo - $cargo;
    		$billingAbono->pdf = '';
    		$billingAbono->reference = '';
    		$billingAbono->bi_flag_last = 1;
    		$billingAbono->bi_subtotal = 0;
    		$billingAbono->bi_porcentaje_vat = 0;
    		$billingAbono->bi_total_vat = 0;
    		$billingAbono->bi_total = 0;
    		$billingAbono->save();
    		--*/
    		 
    		//GENERA BILLING DETAIL
    		//BOX
    		$billingDetail = new BillingDetail();
    		$billingDetail->billing_id			= $billing->id;
    		$billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$st->sqm ."m2";
    		$billingDetail->bd_codigo			= "box ".$st->alias;
    		$billingDetail->bd_numero			= "1";
    		$billingDetail->bd_valor_neto		= $paymentData->pd_box_price_prepay;
    		$billingDetail->bd_porcentaje_vat 	= $vat;
    		$billingDetail->bd_total_vat		= $paymentData->pd_box_vat_prepay;
    		$billingDetail->bd_total			= $paymentData->pd_box_total_prepay;
    		$billingDetail->bd_tipo_partida		= "BOX";
            Log::info('LOG BillingDetail');
            Log::info($billingDetail);
    		$billingDetail->save();

    		 
    		//INSURANCE
    		if($ins > 0){
    			$billingDetail = new BillingDetail();
    			$billingDetail->billing_id			= $billing->id;
    			$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
    			$billingDetail->bd_codigo			= "ubezpieczenie";
    			$billingDetail->bd_numero			= "1";
    			$billingDetail->bd_valor_neto		= $paymentData->pd_insurance_price_prepay;
    			$billingDetail->bd_porcentaje_vat 	= $vat;
    			$billingDetail->bd_total_vat		= $paymentData->pd_insurance_vat_prepay;
    			$billingDetail->bd_total			= $paymentData->pd_insurance_total_prepay;
    			$billingDetail->bd_tipo_partida		= "INSURANCE";
    			$billingDetail->save();
    		}
    	
    		Log::info('New Billing id: '.$billing->id);   

    		Log::info('Fin prepay');
    		
    		
    	return['success' => true];
    	
    		/*
    		$detOrder = DetOrders::where('order_id',$paymentData->order_id)->first();
    		
    		$cataPrepay = CataPrepay::where(['months' => $request->get('st_meses_prepago')])->first();
    		
    		$data = EstimatorHelper::estimate($st->id, $detOrder->rent_starts, $paymentData->insurance, 
    				$paymentData->ext, $cataPrepay->id, $paymentData->term);
    		
    		$vat = $paymentData->pd_vat * 100;
    		$ins = $paymentData->insurance;
    		
    		$paymentData->pd_box_price_prepay		=	$data['boxSubtotalPrepay'];
    		$paymentData->pd_box_vat_prepay			=	$data['boxVATPrepay'];
    		$paymentData->pd_box_total_prepay		=	$data['boxTotalPrepay'];
    		$paymentData->pd_insurance_price_prepay	=	$data['insuranceSubtotalPrepay'];
    		$paymentData->pd_insurance_vat_prepay	=	$data['insuranceVATPrepay'];
    		$paymentData->pd_insurance_total_prepay	=	$data['insuranceTotalPrepay'];
    		$paymentData->pd_subtotal_prepay		=	$data['subtotalPrepay'];
    		$paymentData->pd_total_vat_prepay		=	$data['vatPrepay'];
    		$paymentData->pd_total_prepay			=	$data['totalPrepay'];
    		$paymentData->save();
    		 
    		//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
    		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $st->user_id, 'storage_id' => $st->id])->first();
    		if(!is_null($billingUpdate)){
    			$saldo = $billingUpdate->saldo;
    			$billingUpdate->bi_flag_last = 0;
    			$billingUpdate->save();
    		}
    		else{
    			$saldo = 0;
    		}
    		
    		$invoiceSecuence = new InvoiceSequence();
    		$invoiceSecuence->save();
    		 
    		//GENERAMOS EL CARGO DE PREPAY
    		$billing = new Billing();
    		$billing->user_id = $st->user_id;
    		$billing->storage_id = $st->id;
    		$billing->pay_month = date("Y-m-d");
    		$billing->flag_payed = 0;
    		$billing->abono = 0;
    		$billing->cargo = $paymentData->pd_total_prepay;
    		$billing->saldo = ($paymentData->pd_total_prepay * -1) + $saldo;
    		//$billing->pdf = $pdfName;
    		$billing->reference = '';
    		$billing->bi_flag_last	= 1;
    		$billing->bi_subtotal 			= $paymentData->pd_subtotal_prepay;
    		$billing->bi_porcentaje_vat 	= $vat;
    		$billing->bi_total_vat 			= $paymentData->pd_total_vat_prepay;
    		$billing->bi_total 				= $paymentData->pd_total_prepay;
    		$billing->bi_status_impresion 	= 'PENDIENTE PDF';
    		$billing->bi_recibo_fiscal 		= "";
    		$billing->bi_number_invoice 	= $invoiceSecuence->id;
    		$billing->bi_year_invoice		= date("y");
    		$billing->bi_flag_prepay		= "1";
    		$billing->payment_data_id		= $paymentData->id;
    		$billing->save();
    		 
    		//GENERA BILLING DETAIL
    		//BOX
    		$billingDetail = new BillingDetail();
    		$billingDetail->billing_id			= $billing->id;
    		$billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$st->sqm ."m2";
    		$billingDetail->bd_codigo			= "box ".$st->alias;
    		$billingDetail->bd_numero			= "1";
    		$billingDetail->bd_valor_neto		= $paymentData->pd_box_price_prepay;
    		$billingDetail->bd_porcentaje_vat 	= $vat;
    		$billingDetail->bd_total_vat		= $paymentData->pd_box_vat_prepay;
    		$billingDetail->bd_total			= $paymentData->pd_box_total_prepay;
    		$billingDetail->bd_tipo_partida		= "BOX";
    		$billingDetail->save();
    		
    		//INSURANCE
    		if($ins > 0){
    			
    			Log::info('Registra billing detail de insurance, billing_id: '. $billing->id);
    			 
    			$billingDetail = new BillingDetail();
    			$billingDetail->billing_id			= $billing->id;
    			$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
    			$billingDetail->bd_codigo			= "ubezpieczenie";
    			$billingDetail->bd_numero			= "1";
    			$billingDetail->bd_valor_neto		= $paymentData->pd_insurance_price_prepay;
    			$billingDetail->bd_porcentaje_vat 	= $vat;
    			$billingDetail->bd_total_vat		= $paymentData->pd_insurance_vat_prepay;
    			$billingDetail->bd_total			= $paymentData->pd_insurance_total_prepay;
    			$billingDetail->bd_tipo_partida		= "INSURANCE";
    			$billingDetail->save();
    		}
    		 */
    		return['success' => true];
    	}
    	
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function map()
    {
      return view('pw/maps');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        isset($request['mobile']) ? $mobile = $request['mobile'] : $mobile = 0;

        $column = 'id';
        $data = Storages::where($column,'=', $request['id'])->where('Active', '!=', '0')->first();

        if ($mobile == 1) {
          return['success' => true,
              'storage' => $data
          ];
        }

        if ($request->ajax()) {
            return['success' => true,
                'storage' => $data
            ];
        }
    }
    
    public function editRecurrentBilling($id)
    {
    	$storage = Storages::find($id);
    	
    	return view('storages.storage_recurrent_billing_edit',[
    			'storage'	=> $storage
    	]);
    }
    
    public function updateRecurrentBilling(Request $request)
    {
    	$storage = Storages::find($request->get('id'));
    	$storage->price_per_month = $request->get('price_per_month');
    	$storage->save();
    	
    	return [
    			'returnCode'	=> 200,
    			'msg'			=> 'Recurrent billing updated',
    			'type'			=> 'success'
    	];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $column = 'id';
        $storage = Storages::where($column,'=', $request['idLevel'])->first();

        $storage->alias = $request['alias'];
        $storage->sqm = $request['sqm'];
        $storage->price = $request['price'];
        $storage->comments = $request['comments'];
        
        if($request['st_es_contenedor'] == "on")
        	$storage->st_es_contenedor = 1;
        else 
			$storage->st_es_contenedor = 0;
			

		if($request['st_mostrar_app'] == "on"){
			$storage->st_mostrar_app = 1;
		}else{
			$storage->st_mostrar_app = 0;
		}
        
        $res = $storage->save();

        if ($request->ajax()) {
            return ['res' => $res];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $column = 'id';
        $data = Storages::where($column,'=', $request['id'])->first();

        $data->active = 0;
        $res = $data->save();
        return ['res'=>$res];
    }

    public function getStoragesList(Request $data){
    	
        $alias = $data['alias'];
        $sqm = $data['sqm'];
        $price = $data['price'];
        $start = $data['dateStart'];
        $end = $data['dateEnd'];


        //ORDERS con active = 1
        //

        $storages = Storages::with('StorageOwner')->where('level_id', '=', $data['id'])
                                ->where('active', '!=', '0')
                                ->where(function($query) use ($alias){
                                    if ($alias != '') {
                                        $query->where(['alias' => $alias]);
                                    }
                                })
                                ->where(function($query) use ($sqm){
                                    if ($sqm != '') {
                                        $query->where(['sqm' => $sqm]);
                                    }
                                })
                                ->where(function($query) use ($start,$end){
                                    if ($start != '') {
                                        $fechaUtil = new Fecha();
                                        $start = $fechaUtil->fechaTipoMysql($start);
                                        $end = $fechaUtil->fechaTipoMysql($end);
                                        //dd($start);
                                        $query->whereBetween(\DB::raw('DATE(`rent_start`)'),array($start,$end));
                                    }
                                })
                                ->where(function($query) use ($price){
                                    if ($price != '') {
                                        $query->where(['price' => $price]);
                                    }
                                })
                                ->get();

        //dd($storages[0]);
        $cad = '';
        foreach ($storages as $key => $st) {
        $cad .= '<tr>';
        $cad .= '<td>'.$st->alias.'</td>';
        $cad .= '<td>'.$st->sqm.'</td>';
        $cad .= '<td>'.$st->price.'</td>';

        if (isset($st->StorageOwner->name)) {
          $cad .= '<td>'.$st->StorageOwner->name.' '.$st->StorageOwner->lastName.'</td>';
        }
        else{
        	$cad .= '<td></td>';
        }

        if (isset($st->StorageOwner->companyName)) {
          $cad .= '<td>'.$st->StorageOwner->companyName.'</td>';
        }
        else{
        	$cad .= '<td></td>';
        }

        $cad .= '<td>'.$st->rent_start.'</td>';
        $cad .= '<td>'.$st->rent_end.'</td>';
        $cad .= '<td><a href="javascript:void(0);" id="'.$st->id.'" class="btn btn-default btn-xs editStorage"><i class="fa fa-pencil" aria-hidden="true"></i> </a>';
        $cad .= '<a href="javascript:void(0);" id="'.$st->id.'" class="btn btn-default btn-xs rentStorage"><i class="fa fa-star-o" aria-hidden="true"></i> </a>';
        $cad .= '<a href="javascript:void(0);" id="'.$st->id.'" class="btn btn-primary btn-xs delStorage"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
        $cad .= '</tr>';
        }

        return $cad;
    }

    public function assignStorage(Request $data){
        //dd($data['rentDate']);
        isset($data['flag_assign']) ? $flag_assign = $data['flag_assign'] : $flag_assign = 0;

        // assign storage to clien        t
        $st = Storages::find($data['idStorage']);

        Log::info('Asignando storage');
        Log::info('ID STORAGE: '.$data['idStorage']);
        Log::info('ID CLIENTE: '.$data['idClient']);

        if ($st == null) {
            return ['success' => false, 'msj' => trans('storages.incomplete_data')];
        }


        $cataPrepay = CataPrepay::find($data['prepay']);

        Log::info('Meses: '. $cataPrepay->months);

        $st->flag_rent = 1;
        $st->user_id = $data['idClient'];
        $st->final_price = $data['price'];
        $st->rent_start = $data['rentDate'];
        $st->price_per_month = $data['total'];
        $st->st_meses_prepago = $cataPrepay->months;
        $res = $st->save();

        // save the payment data
        if ($res) {

            $pd = new PaymentData();
            $pd->user_id		= $data['idClient'];
            $pd->storage_id		= $data['idStorage'];
            $pd->term			= $data['term'];
            $pd->prepay			= $data['prepay'];
            $pd->creditcard		= $data['cc'];
            $pd->insurance		= $data['ins'];
            $pd->pd_vat			= env('VAT');

            //NUEVOS CAMPOS DE PAYMENT DATA
            
            $pd->pd_box_price	= $data['pd_box_price'];
            $pd->pd_box_vat		= $data['pd_box_vat'];
            $pd->pd_box_total	= $data['pd_box_total'];
            
            $pd->pd_insurance_price	= $data['pd_insurance_price'];
            $pd->pd_insurance_vat	= $data['pd_insurance_vat'];
            $pd->pd_insurance_total	= $data['pd_insurance_total'];
            
            $pd->pd_subtotal	= $data['pd_subtotal'];
            $pd->pd_total_vat	= $data['pd_total_vat'];
            $pd->pd_total		= $data['pd_total'];
            
            $pd->pd_total_next_month = $data['pd_total_next_month'];
            
            $pd->save();

            $st->payment_data_id	= $pd->id;
            $st->save();

            Log::info('Payment data generado: '. $pd->id);

            if ($pd) {

                $dt = Carbon::now()->toDateString();

                $billing = new Billing();
                $billing->user_id 	= $data['idClient'];
                $billing->storage_id= $data['idStorage'];
                $billing->pay_month	= $dt;
                $billing->flag_payed= $flag_assign;
                $billing->save();

                if($data->get('deposit_month') > 0 ){
                	$deposito = new Deposito();

                	$deposito->storage_id 		= $data['idStorage'];
                	$deposito->user_id			= $data['idClient'];
                	$deposito->de_monto_deposito= $data->get('deposit_month');

                	$deposito->save();

                	Log::info('Se guardará un deposito');
                	Log::info('Storage: ' . $data['idStorage']);
                	Log::info('Userid: ' . $data['idClient']);
                	Log::info('Monto: ' . $data->get('deposit_month'));
                }

                if ($billing) {
                    return ['success' => true, 'msj' => trans('storages.done')];
                }
            
            }
        }
    }

    


    public function unAssignStorage(Request $data){
        //dd($data['rentDate']);
        //isset($data['flag_assign']) ? $flag_assign = $data['flag_assign'] : $flag_assign = 0;

        // assign storage to client
        /*$st = Storages::
                    with(['StoragePaymentData','StorageBilling'])
                    ->where('id','=', $data['idStorage'])
                    ->first();
        */

    	Log::info('Detach Storage');
    	Log::info('ID: '. $data->get('idStorage'));

        $st = Storages::find($data->get('idStorage'));

        //return ['data' => $st];

        if ($st == null) {
            return ['success' => false, 'msj' => trans('storages.incomplete_data')];
        }

        //INSERTAR HISTORICO
        $history = new History();
        $history->storage_id = $st->id;
        $history->user_id = $st->user_id;
        $history->payment_data_id = $st->payment_data_id;
        $history->rent_start = $st->rent_start;
        //$history->rent_end = $st->rent_end;
        $history->rent_end = Carbon::now()->format('Y-m-d');
        $history->price_per_month = $st->price_per_month;
        $history->save();

        Log::info('Inserta historico con id..'.$history->id);

        //CANCELAR SUSCRIPCION BRAINTREE
        
        $paymentData = PaymentData::find($st->payment_data_id);
        Log::info('PaymentDataID (StoragesController): '. $st->payment_data_id);
        
        $suscriptionID = $paymentData->pd_suscription_id;
        if(!is_null($suscriptionID)){
	        Log::info('SuscriptionID (StoragesController): '. $paymentData->pd_suscription_id);
	        $data = BraintreeHelper::cancelSubscription($suscriptionID);
	        
	        if($data['returnCode'] == 200){
	        	$paymentData->pd_suscription_id = null;
	        	$paymentData->pd_suscription_canceled 	= 1;
	        	$paymentData->save();
	        	
	        }
        }
        
        //RESET STORAGE
        $st->flag_rent 			= 0;
        $st->user_id 			= 0;
        $st->payment_data_id 	= 0;
        $st->final_price 		= 0;
        $st->rent_start 		= '0000-00-00';
        $st->rent_end 			= '0000-00-00';
        $st->price_per_month 	= 0;
        $st->st_meses_prepago 	= 0;
        $res = $st->save();

        Log::info('Reset storage');
        Log::info('End detach');


        if ($res) {
            return ['success' => true, 'msj' => 'Detach success!'];
        }

        // save the payment data
        /*if ($res) {
            $pd = PaymentData::create([
                        'user_id' => $data['idClient'],
                        'storage_id' => $data['idStorage'],
                        'term' => $data['term'],
                        'prepay' => $data['prepay'],
                        'creditcard' => $data['cc'],
                    ]);

            if ($pd) {

                $dt = Carbon::now()->toDateString();

                $billing = Billing::create([
                        'user_id' => $data['idClient'],
                        'storage_id' => $data['idStorage'],
                        'pay_month' => $dt,
                        'flag_payed' => $flag_assign,
                    ]);

                if ($billing) {
                    return ['success' => true, 'msj' => trans('storages.done')];
                }
            }
        }*/
    }


    public function getUserStorages(Request $data){
        //dd($data);

    	Log::info('StoragesController@getUserStorages');

    	$saldo = Billing::where(['user_id' => $data['idClient'], 'bi_flag_last' => 1])->sum('saldo');

    	Log::info('ID CLIENTE: '. $data['idClient']);
    	Log::info('SALDO: '.$saldo);

        $st = Storages::with('StoragePaymentData','StoragePaymentData.paymentTerm')
        			->with(['StorageBilling' => function($a) use ($data) {
                        $a->where(['user_id' => $data['idClient']]);
                    }])->where([
                    		'user_id'	=> 	$data['idClient'],
                    		'flag_rent'	=> 1
                    ])
                    ->select('*',
                    	DB::raw('(SELECT COALESCE(SUM(saldo),0) FROM billing WHERE user_id = '.$data['idClient'].' AND bi_flag_last = 1 AND storage_id = storages.id ) as balance'),
                    	DB::raw('(SELECT COALESCE(de_monto_deposito,0) 
                    					FROM depositos 
                    					WHERE user_id = '.$data['idClient'].' AND storage_id = storages.id ORDER BY id DESC LIMIT 0,1 ) as deposito'),
                    	DB::raw('(SELECT de_regresado FROM depositos WHERE user_id = '.$data['idClient'].' AND storage_id = storages.id ORDER BY id DESC LIMIT 0,1 ) as deposito_regresado'),
                    	DB::raw('(SELECT de_pagado FROM depositos WHERE user_id = '.$data['idClient'].' AND storage_id = storages.id ORDER BY id DESC LIMIT 0,1) as deposito_pagado'),
                    	DB::raw('(SELECT id FROM depositos WHERE user_id = '.$data['idClient'].' AND storage_id = storages.id ORDER BY id DESC LIMIT 0,1) as id_deposito'),
						DB::raw('(SELECT updated_at FROM depositos WHERE user_id = '.$data['idClient'].' AND storage_id = storages.id ORDER BY id DESC LIMIT 0,1) as fecha_devolucion_deposito')
					   )->get();

		Log::info("*******");
		Log::info($st);
		Log::info("*******"); 
		
        $storages = Storages::where([
        						'user_id'	=> $data['idClient'],
        						'flag_rent'	=> 1
							])->lists('id');
							
				   
		Log::info("*******");
		Log::info($storages);
		Log::info("*******"); 
		

        $billingStorages = Billing::where([
        						'user_id'	=> $data['idClient']
        						])->whereNotIn('storage_id',$storages)
        						->with('storages')
        						->select('*',
				                    	DB::raw('(SELECT COALESCE(SUM(saldo),0) FROM billing b WHERE user_id = '.$data['idClient'].' AND bi_flag_last = 1 AND b.storage_id = billing.storage_id ) as balance'),
        								
        								DB::raw('(SELECT de_regresado FROM depositos de WHERE user_id = '.$data['idClient'].' AND de.storage_id = billing.storage_id ORDER BY id DESC LIMIT 0,1 ) as deposito_regresado'),
        								DB::raw('(SELECT de_pagado FROM depositos de WHERE user_id = '.$data['idClient'].' AND de.storage_id = billing.storage_id ORDER BY id DESC LIMIT 0,1 ) as deposito_pagado'),
        								DB::raw('(SELECT id FROM depositos de WHERE user_id = '.$data['idClient'].' AND de.storage_id = billing.storage_id ORDER BY id DESC LIMIT 0,1 ) as id_deposito'),
        								DB::raw('(SELECT updated_at FROM depositos de WHERE user_id = '.$data['idClient'].' AND de.storage_id = billing.storage_id ORDER BY id DESC LIMIT 0,1 ) as fecha_devolucion_deposito'),
										DB::raw('(SELECT creditcard FROM payment_data de WHERE id = billing.payment_data_id) as creditCard'),
										DB::raw('(SELECT id FROM payment_data pd WHERE pd.id = billing.payment_data_id) as pd_id')
				                   	)
        						->groupBy('storage_id')
								->get();
								
		Log:info($billingStorages);

        Log::info('st->count(): '. $st->count());
        Log::info('billingStorages->count(): '. $billingStorages->count());

        //Log::info('BILLING STORAGES: '.$billingStorages );

        if ($st->count() < 1 && $billingStorages->count() < 1) {
            return [
            		'success' => false, 'msj' => trans('storages.nothing_to_show')
            ];
        }

        Log::info('END----StoragesController@getUserStorages');

        return ['success' => true, 'storages' => $st , 'billingStorages' => $billingStorages ,
        		'saldo' => $saldo , 'saldo_formateado' => Formato::formatear('ZLOTY', $saldo)
        ];
    }

    public function setTermNotice(Request $data){
        // get and calculate term date
        $st = Storages::where('id','=', $data['idSt'])
                    ->where('flag_rent','=', 1)
                    ->first();

        $dt = Carbon::now()->toDateString();

        $term_date = $data['dateTerm'];

        if ($term_date >= $dt) {
            $st->rent_end = $term_date;
            $res = $st->save();
        }else {
            return ['success' => false, 'msj' => 'Something was wrong!'];
        }

        if ($res) {
            return ['success' => true, 'msj' => 'Term notice success!'];
        }else{
            return ['success' => false, 'msj' => 'Something was wrong!'];
        }
    }

    public function setTermNoticeAutomatic(Request $data){
        // get and calculate term date
        $st = Storages::with(['StoragePaymentData' => function($a) use ($data) {
                        $a->where(['user_id' => $data['userId']]);
                    }])
                    ->with(['StorageBilling' => function($b) use ($data) {
                        $b->where(['user_id' => $data['userId']]);
                    }])
                    ->where('id','=', $data['idSt'])
                    ->where('flag_rent','=', 1)
                    ->get();

        Log::info('ST'. $st);
        Log::info('User id: '.$data->get('userId'));

        /**
        *
        * Term:
        * 1 = 1 month
        * 2 = 3
        * 3 = 6
        * 4 = 12
        *
        **/

        $term = $st[0]->StoragePaymentData[0]->term;
        //return [$st[0]->StoragePaymentData[0]->term];
        $months = 0;
        $start_date = $st[0]->StorageBilling[0]->pay_month;
        //return [$start_date];

        switch ($term) {
            case 2:
                $months = 3;
                break;

            case 3:
                $months = 6;
                break;

            case 4:
                $months = 12;
                break;

            default:
                $months = 1;
                break;
        }

        //return [$months];

        $term_date = Carbon::parse($start_date)->addMonths($months)->subDays(1)->toDateString();

        Log::info('MONTHS : '.$months);
        Log::info('TERM DATE: '.$term_date);

        $utilFecha = new Fecha();
        $lastday = $utilFecha->getUltimoDiaMes($term_date);

        Log::info('LAST DAY: '.$lastday);
        //return [$lastday];

        $st[0]->rent_end = $lastday;

        Log::info('Indice: '.$st[0]);

        $res = $st[0]->save();

        if ($res) {
            return ['success' => true, 'term_date' => $term_date];
        }else{
            return ['success' => false, 'msj' => 'Something was wrong!'];
        }
    }

    public function genInvoice(){
        return view('factura');
    }

    public function getStorages(Request $data){
      $storages = Storages::where('level_id', '=', $data['level'])->where('active', '!=', '0')->get();
      return['success' => true,'storages' => $storages];

    }

    public function getStoragesSizes(Request $data){
      isset($data['lvlid']) ? $lvlid = $data['lvlid'] : $lvlid = '';
      
      $orders = Orders::where(['active' => '1'])->lists('id');
      $detOrders = DetOrders::whereIn('order_id',$orders)->lists('product_id');
       
      Log::info('Storages en orden activa: ');
      Log::info($detOrders->all());

      $storages = Storages::where('sqm', '>', 0)
					->where('active','=',1)
					->where('flag_rent','=',0)
				  
                  ->where(function($query) use ($lvlid){
                      if ($lvlid != '') {
                          $query->where(['level_id' => $lvlid]);
                      }
                  })
                  ->whereNotIn('id',$detOrders)
                  ->select('sqm')
                  ->groupBy('sqm')->get();

      $num = count($storages);
      if ($num > 0) {
        return['success' => true,'sizes' => $storages];
      }else{
        return['success' => false,'mjs' => 'Nothing to show.'];
      }


    }

    public function cataterm(){
      $cata = CataTerm::all();
      return['success' => true,'cata' => $cata];

    }

    public function cataprepay(){
      $cata = CataPrepay::all();
      return['success' => true,'cata' => $cata];

    }
    
    public function updateCreditCardPaymentData($id,$cc)
    {
    	Log::info('StoragesController@updateCreditCardPaymentData');
    	Log::info('id: '.$id);
    	Log::info('cc: '.$cc);
    	
    	$paymentData = PaymentData::find($id);
    	$paymentData->creditcard 	= $cc;
    	$paymentData->save();
    	
    	return ['returnCode'	=> 200 , 'msg'	=> 'Updated'];
	}
	
	public function indexExpires($thisMonth)
	{
		if($thisMonth == 1){
			$rentStart = Carbon::now()->firstOfMonth()->format('Y-m-d');
			$rentEnd = Carbon::now()->lastOfMonth()->format('Y-m-d');
		}else{

			$rentStart = new Carbon ('first day of next month');
			$rentEnd = new Carbon('last day of next month');

			$rentStart = Carbon::parse($rentStart)->format('Y-m-d');
			$rentEnd = Carbon::parse($rentEnd)->format('Y-m-d');
		}

		return view('storages.index_expires',[
			'rentStart'	=> $rentStart ,
			'rentEnd'	=> $rentEnd
		]);
	}

	public function indexExpiresDT(Request $request)
	{
		$filtros = [];

		$filtros['active'] = 1;

		$boxesExpires = Storages::whereBetween(
				'rent_end' , [$request->get('rentStart'),$request->get('rentEnd')]
		)->where($filtros)->with('StorageOwner')->get();

		return Datatables::of($boxesExpires)
				->addColumn('tenant', function($row){
					return $row->StorageOwner->name. " ". $row->StorageOwner->lastName. " - ". $row->StorageOwner->companyName;
				})
				->make(true);
	}
}
