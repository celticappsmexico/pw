<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Illuminate\Support\Facades\Log;
use App\Storages;
use App\PaymentData;
use App\CardUser;
use Illuminate\Support\Facades\Mail;
use App\Billing;
use Carbon\Carbon;
use App\CataPrepay;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function createTransaction($creditCardToken,$customerId,$planId,$subscribed,$storage_id,$paymentid,$transaction = false){
    	
		Log::info('PaymentController@createTransaction');
		Log::info('now here');
    	
    	Log::info('$creditCardToken: '. $creditCardToken);
    	Log::info('$customerId: '. $customerId);
    	Log::info('$planId: '. $planId);
    	Log::info('$subscribed: '. $subscribed);
    	Log::info('$storage: '. $storage_id);
    	
    	$storage = Storages::find($storage_id);
    	$paymentData = PaymentData::find($paymentid);
    	
    	$prepay = $paymentData->prepay;
    	
    	$cardUser = CardUser::where(['cu_token_card' => $creditCardToken ])->first();
    	
		$paymentData->card_user_id = $cardUser->id;
		
		///ULTIMO BILLING DEL STORAGE Y USER..
		//SI SU ULTIMO BILLING ES MAYOR A HOY.. NO LO SUSCRIBE LO PROGRAMA
		//SI SU ULTIMO BILLING ES MENOR A HOY..LO SUSCRIBE AHORA

		$lastBilling = Billing::where([
						'storage_id' => $storage_id,
						'user_id'	 => $storage->user_id
					])->where('cargo','>','0')->orderBy('id','desc')->first();

		$ultimaFechaBilling = $lastBilling->bi_end_date;

		Log::info('ultimaFechaBilling: '. $ultimaFechaBilling );

		if($ultimaFechaBilling > Carbon::now()){
			$prepay = 1;
			//Para que no entre al sig if
		}else{
			$prepay = 0;
		}

		if(is_null($ultimaFechaBilling)){
			return ['returnCode' => 500  , 'msg' => 'Contact your admin!' ];
 
		}
    	
    	if($subscribed && $prepay == 0)
    	{
    		$subscriptionData = array(
    				'paymentMethodToken' => $cardUser->cu_token_card,
    				'planId' => $planId,
    				'price'  => $storage->price_per_month
    		);
    			
    		//$this->cancelSubscription();
    		
    		Log::info('TOken Card: '. $cardUser->cu_token_card);
    		
    		$subscription_result = Braintree_Subscription::create($subscriptionData);
    
    		Log::info('Suscription Result');
    		Log::info($subscription_result);
    		
    		//dd($subscription_result);
    		Log::info('Subscription id: ' . $subscription_result->subscription->id);
    		
    		if(is_null($subscription_result->subscription->id)){
    			return ['returnCode'	=> 100, 'msg'	=> 'Suscription error'];
    		}
    		
    		$storage->date_subscribe_braintree	= null;
    		$storage->save();

    		$paymentData->pd_suscription_id = $subscription_result->subscription->id;
    		$paymentData->pd_suscription_canceled = 0;
    		//echo '<br/> Subscription id: '. $subscription_result->subscription->id;
    	}
    	else {
    		//$this->cancelSubscription();
		}
		
		$msg = trans('cart.suscription_success');
    	
    	
    	if($subscribed && $prepay >= 1)
    	{
    		$cataPrepay = CataPrepay::find($prepay);
    		$months = $cataPrepay->months;
    		
    		//PROGRAMA UNA FUTURA SUSCRIPCION
			//$fechaProgramada = Carbon::now()->addMonths($months)->startOfMonth();
			$fechaProgramada = Carbon::parse($ultimaFechaBilling)->addDays(1)->startOfMonth()->format('Y-m-d');
    		$storage->date_subscribe_braintree	= $fechaProgramada;
    		$storage->save();
    		
			$paymentData->pd_suscription_canceled = 0;
			
			$msg = "Subscription scheduled to: ". $fechaProgramada;
    	}
    	
    	$paymentData->save();
    	
    	Log::info('END---PaymentController@createTransaction');
    	
    	return ['returnCode' => 200  , 'msg' => $msg ];
    	
    }
    
    
    public function cancelSubscription($suscription_id)
    {
    	Log::info('Suscription id: '. $suscription_id);
    	
    	$result = Braintree_Subscription::cancel($suscription_id);
    	
    	if($result->success == true){
    		
    		$paymentData = PaymentData::where(['pd_suscription_id' => $suscription_id])->first();
    		$paymentData->pd_suscription_id 	= null;
    		$paymentData->pd_suscription_canceled 	= 1;
    		$paymentData->save();
    		
    		return ['returnCode' => 200, 'msg'	=> trans('cart.suscription_canceled')];
    	}
    }
    
    
    //// for subscription Braintree_WebhookNotification
    public function subscription(Request $request)
    {
    	Log::info('Payload');
    	Log::info($request->get('bt_payload'));
    	Log::info('******');
    	Log::info('bt_signature');
    	Log::info($request->get('bt_signature'));
    
    	try{
    		if(!is_null($request->get('bt_signature')) && !is_null($request->get('bt_payload'))) {
    
    			$webhookNotification = Braintree_WebhookNotification::parse(
    					$request->get('bt_signature'), $request->get('bt_payload')
    			);
    
    			// $message =
    			// "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
    			// . "Kind: " . $webhookNotification->kind . " | "
    			// . "Subscription: " . $webhookNotification->subscription->id . "\n";Log::info("msg " . Log::info("subscription " . json_encode($webhookNotification->subscription));
    			Log::info("transactions " . json_encode($webhookNotification->subscription->transactions));
    			Log::info("transactions_id " . json_encode($webhookNotification->subscription->transactions[0]->id));
    			Log::info("customer_id " . json_encode($webhookNotification->subscription->transactions[0]->customerDetails->id));
    			Log::info("amount " . json_encode($webhookNotification->subscription->transactions[0]->amount));
    		}
    		else{
    			Log::info('Sin informacion a procesar');
    		}
    	}
    	catch (\Exception $ex) {
    		Log::error("PaymentController::subscription() " . $ex->getMessage());
    	}
    }
    
    
    public function recurrentBilling(Request $request)
    {
    	Log::info('paymentController@recurrentBilling');

    	Log::info('Webhook');
    	
    	//Log::info('Request: ' . $request);
    	$environment = ENV('ENVIRONMENT');
    	
    	$json = base64_decode($request->get('bt_payload'));
    	
    	//
    	$transactionID = "";
    	$suscriptionID = "";
    	//
    	
    	$emailData = [
    			'nombreDestinatario' 	=> 'Oscar',
    			'email' 				=> 'oscar.sanchez@novacloud.mx',
    			'email2' 				=> 'oscarbar17@gmail.com',
    			 
    			'tituloCorreo'			=> 'Webhook',
    			'request'				=> 	'IN'
    	];
    	
    	/*
    	$resp = Mail::send ( 'emails.webhook_email', $emailData, function ($message) use ($emailData, $environment) {
    		$message->to ( $emailData['email'] );
    		$message->cc ( 'oscarbar17@gmail.com' );
    	
    		$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
    		$message->subject ( 'Webhook - ' . $environment);
    		 
    	} );
    	*/
    	//
    	
    	$xmlDoc = new \DOMDocument();
    	$xmlDoc->loadXML( $json );
    	
    	$searchNode = $xmlDoc->getElementsByTagName( "id" );
    	$searchNodeSubs = $xmlDoc->getElementsByTagName( "subscription-id" );
    	
    	$i = 1;
    	
    	foreach( $searchNode as $search )
    	{    		
    		//$items = $search->getElementsByTagName('id');
    		if($i == 2){
	    		$transactionID = $search->nodeValue;
	    		
	    		Log::info('Transaction ID: '. $transactionID);
    		}
    		
    		$i++;
    	}
    	
    	foreach($searchNodeSubs as $search){
    		$suscriptionID = $search->nodeValue;
    		
    		Log::info('Suscription ID: '. $suscriptionID);
    	}
    	
    	
    	//BUSCA PAYMENT DATA CON ESE ID ...
    	if($suscriptionID == ""){
    		return ['returnCode'	=> 500, 'msg'	=> 'Suscription ID Empty' ];
    	}
    	
    	
    	
    	$paymentData = PaymentData::where([
    			'pd_suscription_id'	=> $suscriptionID
    	])->first();
    	
    	
    	
    	if(!is_null($paymentData)){
    		
    		Log::info('Se va a actualizar el pago correctamente ');
    		Log::info('SuscriptionID: '. $suscriptionID);
    		Log::info('TransactionID: '. $transactionID);
    		
    		//LOGICA DE MARCAR COMO PAGADO
    		
    		//OBTIENE SALDO
    		$saldo = Billing::where(['user_id' => $paymentData->user_id, 'bi_flag_last' => 1, 'storage_id' => $paymentData->storage_id])->sum('saldo');
    		
    		if(is_null($saldo))
    			$saldo = 0;
    		
    		//OBTIENE BILLING DEL MES
    		$billing = Billing::where([
    						'payment_data_id' 	=> $paymentData->id,
							'flag_payed'		=> '0',
							'bi_invoice_additional'	=> 0
							])->whereMonth('pay_month', '=' , Carbon::now()->month)
	    					->first();
    		
    		Log::info('PaymentDataID: '. $paymentData->id);
    		Log::info('BillingID: '. $billing->id);
    		
    		//ACTUALIZA BILLING DEL MES
    		$billing->flag_payed 		= 1;
    		$billing->bi_flag_last		= 0;
    		$billing->payment_type_id	= 2;
    		$billing->bi_transaction_braintree_id = $transactionID;
			$billing->save();
			
			//ACTUALIZA TODOS LOS BILLING PORQUE YA HIZO EL CARGO DE TODO LO PENDIENTE...
			//TODO
    		
    		//ABONO
    		Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])
    		->update(['bi_flag_last' => '0']);
    		
    		$saldoNuevo = $saldo + $billing->cargo;
    		Log::info('El cargo en el nuevo registro es de : '.$saldoNuevo);
    		Log::info('Operacion aritmetica realizada : '.$saldo .'+'. $billing->cargo);
    		 
    		$newBilling = new Billing();
    		$newBilling->user_id 			= $billing->user_id;
    		$newBilling->storage_id			= $billing->storage_id;
    		$newBilling->pay_month			= Carbon::now()->format('Y-m-d');
    		$newBilling->flag_payed 		= 1;
    		$newBilling->abono				= $billing->cargo;
    		$newBilling->cargo				= 0;
    		$newBilling->saldo				= $saldoNuevo;
    		$newBilling->pdf				= "";
    		$newBilling->reference			= $transactionID;
    		$newBilling->bi_flag_last		= 1;
    		$newBilling->payment_data_id 	= $paymentData->id;
    		$newBilling->payment_type_id 	= "2";
    		$newBilling->bi_number_invoice	= $billing->bi_number_invoice;
    		$newBilling->bi_year_invoice	= $billing->bi_year_invoice;
    		$newBilling->bi_webhook			= 1;
    		$newBilling->save();
    		
	    	/*
	    	Billing::where(['payment_data_id' 	=> $paymentData->id,
	    					'flag_payed'		=> '0'])
	    				->whereMonth('pay_month' , Carbon::now()->month)
	    				->update([
	    							'flag_payed' 					=> '1',
	    							'payment_type_id'				=> '2',
	    							'bi_transaction_braintree_id' 	=> ''
	    						]
	    				);
	    	*/		
	    	$emailData = [
	    			'nombreDestinatario' 	=> 'Oscar',
	    			'email' 				=> 'oscar.sanchez@novacloud.mx',
	    			'email2' 				=> 'oscarbar17@gmail.com',
	    				
	    			'tituloCorreo'			=> 'Webhook',
	    			'request'				=> 	$json
	    	];
	    	
	    	$title = $environment. " - SuscriptionID: ".$suscriptionID .", "." TransactionID:  ".$transactionID;
	    	 
	    	$resp = Mail::send ( 'emails.webhook_email', $emailData, function ($message) use ($emailData, $title) {
	    		$message->to ( $emailData['email'] );
	    		//$message->cc ( $emailData['email2'] );
	    	
	    		$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
	    		$message->subject ( 'Webhook - ' . $title );
	    		
	    	} );
	    	
    	}else{
    		$emailData = [
    				'nombreDestinatario' 	=> 'Oscar',
    				'email' 				=> 'oscar.sanchez@novacloud.mx',
    				'email2' 				=> 'oscarbar17@gmail.com',
    				 
    				'tituloCorreo'			=> 'Webhook',
    				'request'				=> 	$json
    		];
    		
    		
    		
    		$resp = Mail::send ( 'emails.webhook_email', $emailData, function ($message) use ($emailData, $suscriptionID, $environment) {
    			$message->to ( $emailData['email'] );
    			//$message->cc ( $emailData['email2'] );
    		
    			$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
    			$message->subject ( 'Webhook - '. $environment .' - No existe Payment data con suscription ID: '.$suscriptionID );
    			 
    		} );
    		Log::info('PaymentData no existe con ese suscriptionID');
    	}
    	
    	Log::info('---END---paymentController@recurrentBilling');

    }
    
}
