<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Library\UtilNotificaciones;
use App\notificaciones;
use App\User;
use App\Library\Notificacion;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Log::info('NotificationsController@create');
        return view('notifications.create_notification');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    	$mensaje = $request->get('message');
    	
    	$users = User::where('movil_dispositive','<>','0')
    				->where('active','1')
    				->get();
    	
    	foreach($users as $user){
    		$idUser = $user->id; 
    		
    		UtilNotificaciones::Enviar($mensaje, $idUser);
    		
    		$notificacion = new notificaciones();
    		$notificacion->user_id = $idUser;
    		$notificacion->storage_id = 0;
    		$notificacion->notf_mensaje = $mensaje;
    		$notificacion->save();
    		
    	} 
    	
    	return ['returnCode'	=> 200, 'msg'	=> 'Notification sent ('.$users->count().')'];
    }
    
    public function getNotificacionesAdmin()
    {
    	$notificaciones = notificaciones::with('user','storage')
    					->where([
    						'notf_admin'	=> 1,
    						'notf_eliminado'=> false 
    					])->orderBy('id','DESC')
    					->get();
    	
		//Log::info($notificaciones->all());    					
    					
    	$count = count($notificaciones);
    	
    	Log::info('count notif: ' . $count);
    	
    	return ['success' => true, 'count' => $count, 'notif' => $notificaciones];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyNotifAdmin($id)
    {
        $notificacion = notificaciones::find($id);
        $notificacion->notf_eliminado = true;
        $notificacion->save();

        return [
            'returnCode'    => '200',
            'msg'           => 'Notification Read',
            'storage_id'    => $notificacion->storage_id,
            'user_id'       => $notificacion->user_id
        ];

    }
}
