<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Storages;
use App\User;

class ReportBoxesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pw.reports.boxes.index_boxes_report');
    }

    public function showBoxes($days) {
        $now = Carbon::now()->toDateString();

        $nowDays = Carbon::now()->addDays($days)->toDateString();
        $tenants = User::
        leftJoin('storages', 'users.id', '=', 'storages.user_id')
            ->join('levels', 'storages.level_id', '=', 'levels.id')
            ->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
            ->select(
                'storages.alias as storageAlias',
                'levels.name as levelName',
                'warehouse.name as warehouseName',
                'users.name as userName',
                'users.lastName as userLN',
                'users.companyName as companyName',
                'storages.rent_start as rentStart',
                'storages.rent_end as rentEnd'
            )
            ->where('rent_end', '=', $nowDays)
            ->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
            ->get();

        foreach ($tenants as $key => $tenant) {
            $tenant->remaining_days = $days;
        }

        return view('pw.reports.boxes.boxes_report', ['tenants'=>$tenants, 'days'=>$days]);
    }

    public function generarExcel($days) {

        \Excel::create('Boxes Report CSV', function($excel) use($days){

            $excel->sheet('History', function($sheet) use($days){
                $now = Carbon::now()->toDateString();

                $nowDays = Carbon::now()->addDays($days)->toDateString();
                $tenants = User::
                leftJoin('storages', 'users.id', '=', 'storages.user_id')
                    ->join('levels', 'storages.level_id', '=', 'levels.id')
                    ->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
                    ->select(
                        'storages.alias as storageAlias',
                        'levels.name as levelName',
                        'warehouse.name as warehouseName',
                        'users.name as userName',
                        'users.lastName as userLN',
                        'users.companyName as companyName',
                        'storages.rent_start as rentStart',
                        'storages.rent_end as rentEnd'
                    )
                    ->where('rent_end', '=', $nowDays)
                    ->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
                    ->get();

                foreach ($tenants as $key => $tenant) {
                    $tenant->remaining_days = $days;
                }

                $sheet->row(1, [
                    'Storage', 'Level', 'Warehouse', 'Name', 'Company Name', 'Rent Start', 'Rent End', 'Remaining Days'
                ]);

                foreach($tenants as $index => $tenant) {
                    $sheet->row($index+2, [
                        $tenant->storageAlias, $tenant->levelName, $tenant->warehouseName,
                        $tenant->userName." ".$tenant->userLN, $tenant->companyName, $tenant->rentStart,
                        $tenant->rentEnd, $tenant->remaining_days
                    ]);
                }

            });
        })->export('csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
