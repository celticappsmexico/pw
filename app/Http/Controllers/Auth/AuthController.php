<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Roles;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\clientAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $redirectTo ='home';
    protected $username = 'username';

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $login = $request->get('username');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        return [
            $field => $login,
            'password' => $request->get('password'),
        ];

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'usuario' => 'max:255',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //dd($data);
        isset($data['email']) ? $mail = $data['email'] : $mail = '';
        isset($data['Role_id']) ? $role = $data['Role_id'] : $role = '';
        isset($data['userType_id']) ? $userType = $data['userType_id'] : $userType = '';
        isset($data['phone']) ? $phone = $data['phone'] : $phone = '';
        isset($data['active']) ? $active = $data['active'] : $active = '';
        isset($data['companyName']) ? $company = $data['companyName'] : $company = '';
        isset($data['nipNumber']) ? $nip = $data['nipNumber'] : $nip = '';
        isset($data['regonNumber']) ? $regon = $data['regonNumber'] : $regon = '';
        isset($data['courtNumber']) ? $courtNumber = $data['courtNumber'] : $courtNumber = '';
        isset($data['courtPlace']) ? $courtPlace = $data['courtPlace'] : $courtPlace = '';
        isset($data['krsNumber']) ? $krsNumber = $data['krsNumber'] : $krsNumber = '';
        isset($data['Legal_person1']) ? $person1 = $data['Legal_person1'] : $person1 = '';
        isset($data['Legal_person2']) ? $person2 = $data['Legal_person2'] : $person2 = '';
        isset($data['peselNumber']) ? $pesel = $data['peselNumber'] : $pesel = '';
        isset($data['idNumber']) ? $idNumber = $data['peselNumber'] : $idNumber = '';
        isset($data['name']) ? $name = $data['name'] : $name = '';
        isset($data['lastName']) ? $lastName = $data['lastName'] : $lastName = '';
        isset($data['birthday']) ? $birthday = $data['birthday'] : $birthday = '';
        isset($data['hdfut']) ? $ut = $data['hdfut'] : $ut = '9';
        isset($data['hdfr']) ? $ur = $data['hdfr'] : $ur = '9';
        isset($data['street']) ? $street = $data['street'] : $street = '';
        isset($data['number']) ? $number = $data['number'] : $number = '';
        isset($data['apartmentNumber']) ? $apartment = $data['apartmentNumber'] : $apartment = '';
        isset($data['postCode']) ? $postCode = $data['postCode'] : $postCode = '';
        isset($data['city']) ? $city = $data['city'] : $city = '';
        isset($data['country']) ? $country = $data['country'] : $country = '';
        isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
        isset($data['id_fb']) ? $id_fb = $data['id_fb'] : $id_fb = 0;
        isset($data['avatar']) ? $avatar = $data['avatar'] : $avatar = 'assets/img/avatar/default.jpg';



        $success = User::create([
            'username' => $data['username'],
            'name' => $name,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'lastName'=> $lastName,
            'companyName'=> $company,
            'peselNumber'=> $pesel,
            'idNumber'=> $idNumber,
            'nipNumber'=> $nip,
            'regonNumber'=> $regon,
            'courtNumber'=> $courtNumber,
            'courtPlace'=> $courtPlace,
            'krsNumber'=> $krsNumber,
            'Legal_person1'=> $person1,
            'Legal_Person2'=> $person2,
            'Role_id'=> '3',
            'userType_id'=> '1',
            'phone'=> $phone,
            'birthday' => $birthday,
            'active' => '1',
            'validate' => '0',
            'flag_oportunity' => 1,
            'remember_token' => str_random(10),
            'fb_id' => $id_fb,
            'avatar_tocken' => uniqid(),
            'avatar' => $avatar,

        ]);

        // send mail
        //$mail = app('App\Http\Controllers\MailController')->sendRegisterMail($success->id);

        if ($success->id) {
          $address = clientAddress::create([
            'user_id' => $success->id,
            'street' => $street,
            'number' => $number,
            'apartmentNumber' => $apartment,
            'postCode' => $postCode,
            'city' => $city,
            'country_id' => $country
          ]);
        }

        if($mobile == 1){
          return ['success' => 'true','id' => $success->id,'user_type' => $success->userType_id];
        }else{
          return $success;
        }

    }

}
