<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\HistoryOccupancyRate;
use App\Billing;
use App\cata_months;
use Carbon\Carbon;

class HistoryOccupancyRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fecha = Carbon::now();
        $records = HistoryOccupancyRate::get();
        $warehouses = HistoryOccupancyRate::select('warehouse')->groupBy('warehouse')->orderBy('warehouse', 'desc')->get();
        foreach ($warehouses as $key => $warehouse) {
            $sqms = HistoryOccupancyRate::select('sqm')->distinct('sqm')->where('warehouse', '=', $warehouse->warehouse)->get();
            // dd($sqms);
            $warehouse->sqms=$sqms;
        }
        // dd($warehouses);

        return view('pw.history.history_occupancy_rate', ['year'=>$fecha->format('Y'), 'warehouses' => $warehouses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filterHistoryOccupancyRate($year, $sqm, $warehouse) {
        $warehouses = explode(',', $warehouse);
        $sqms = explode(',', $sqm);
        // dd($sqms[0]);

        // foreach ($warehouses as $key => $warehouse) {
        //     $warehouse = str_replace('_', ' ', $warehouse);
        // }
        // $warehouse = str_replace('_', ' ', $warehouses[0]);
        // $sqm = $sqm[0];
        $records = [];

        foreach ($sqms as $key => $sqm) {
            // dd($sqm);
            $warehouse = str_replace('_', ' ', $warehouses[$key]);
            $results = \DB::select('SELECT h.warehouse, h.level, h.sqm, m.month, h.occupancy_rate FROM
            history_occupancy_rate h right join cata_months m
            on monthname(h.created_at) = m.month
            and year(h.created_at) = :year
            and h.sqm = :sqm
            and h.warehouse = :warehouse',
            ['year' => $year, 'sqm' => $sqm, 'warehouse' => $warehouse]);
            $occupancy_rate = [];
            $i = 0;
            foreach ($results as $result) {
                if ($result->occupancy_rate == null) {
                    $result->occupancy_rate = "0.00";
                }
                $occupancy_rate[$i] = $result->occupancy_rate;
                $i++;
            }
            $occupancy_rate = implode(",",$occupancy_rate);
            // dd($occupancy_rate);
            $records[$key] = [
                'occupancy_rate' => $occupancy_rate,
                'warehouse' => $warehouse,
                'sqm' => $sqm,
            ];
        }

        $traMonth = trans('billing_reports.month');
        $January = trans('billing_reports.January');
        $February = trans('billing_reports.February');
        $March = trans('billing_reports.March');
        $April = trans('billing_reports.April');
        $May = trans('billing_reports.May');
        $June = trans('billing_reports.June');
        $July = trans('billing_reports.July');
        $August = trans('billing_reports.August');
        $September = trans('billing_reports.September');
        $October = trans('billing_reports.October');
        $November = trans('billing_reports.November');
        $December = trans('billing_reports.December');

        return view('pw.history.history_occupancy_rate_filter', [
            // 'occupancy_rate'=>$occupancy_rate,
            // 'warehouse'=>$warehouse,
            // 'sqm'=>$sqm,
            'records'=>$records,
            'January'=>$January,
            'February'=>$February,
            'March'=>$March,
            'April'=>$April,
            'May'=>$May,
            'June'=>$June,
            'July'=>$July,
            'August'=>$August,
            'September'=>$September,
            'October'=>$October,
            'November'=>$November,
            'December'=>$December,
            'traMonth'=>$traMonth,
        ]);
    }
}
