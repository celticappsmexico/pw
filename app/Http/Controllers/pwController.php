<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\clientAddress;
use App\LegalRepresentative;
use Illuminate\Support\Facades\Session;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use App\Warehouse;
use App\Storages;
use App\Http\Fecha;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Contract;

class pwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //dd(User::with(['role'])->find(1));
        return view('pw.storagesmap');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {

      //dd($data['hdfut']);
      //dd($data);
      
    	Log::info('pwController@store');
    	//Log::info('data: '. $data);
    	

      isset($data['companyName']) ? $company = $data['companyName'] : $company = '';
      isset($data['nipNumber']) ? $nip = $data['nipNumber'] : $nip = '';
      isset($data['regonNumber']) ? $regon = $data['regonNumber'] : $regon = '';
      isset($data['courtNumber']) ? $courtNumber = $data['courtNumber'] : $courtNumber = '';
      isset($data['courtPlace']) ? $courtPlace = $data['courtPlace'] : $courtPlace = '';
      isset($data['krsNumber']) ? $krsNumber = $data['krsNumber'] : $krsNumber = '';
      isset($data['Legal_person1']) ? $person1 = $data['Legal_person1'] : $person1 = '';
      isset($data['Legal_person2']) ? $person2 = $data['Legal_person2'] : $person2 = '';
      isset($data['peselNumber']) ? $pesel = $data['peselNumber'] : $pesel = '';
      isset($data['idNumber']) ? $idNumber = $data['idNumber'] : $idNumber = '';
      isset($data['name']) ? $name = $data['name'] : $name = '';
      isset($data['lastName']) ? $lastName = $data['lastName'] : $lastName = '';
      isset($data['birthday']) ? $birthday = $data['birthday'] : $birthday = '';
      isset($data['hdfut']) ? $ut = $data['hdfut'] : $ut = '1';
      isset($data['hdfr']) ? $ur = $data['hdfr'] : $ur = '3';
      isset($data['chkOportunity']) ? $oportunity = $data['chkOportunity'] : $oportunity = '0';
      isset($data['oportunity_reminder']) ? $oportunity_reminder = $data['oportunity_reminder'] : $oportunity_reminder = '00-00-00';
      isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
      isset($data['notes']) ? $notes = $data['notes'] : $notes = '';
      isset($data['id_fb']) ? $id_fb = $data['id_fb'] : $id_fb = 0;
      isset($data['avatar']) ? $avatar = $data['avatar'] : $avatar = 'assets/img/avatar/default.jpg';
      isset($data['phone']) ? $phone = $data['phone'] : $phone = '';

      isset($data['street']) ? $street = $data['street'] : $street = '';
      isset($data['number']) ? $number = $data['number'] : $number = '';
      isset($data['apartmentNumber']) ? $apartment = $data['apartmentNumber'] : $apartment = '';
      isset($data['postCode']) ? $postCode = $data['postCode'] : $postCode = '';
      isset($data['city']) ? $city = $data['city'] : $city = '';
      isset($data['country']) ? $country = $data['country'] : $country = '';
      isset($data['phone2']) ? $phone2 = $data['phone2'] : $phone2 = '';
      isset($data['email2']) ? $email2 = $data['email2'] : $email2 = '';
      $av_token = uniqid();
      
      $user = User::where('username' , $data['email'])->orWhere('email' , $data['email'])->first();
      
      /*
      ///SE OMITE LA VALIDACION
      if(!is_null($user)){
      	Log::info('Ya existe el cliente con correo: ' . $data['email']  . " , nombre: ".$name . " , apellidos: ". $lastName );
      	return ['success' => 'false'];
      }*/

      $success = User::create([
            'username' => $data['email'],
            'name' => $name,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'lastName'=> $lastName,
            'companyName'=> $company,
            'peselNumber'=> $pesel,
            'idNumber'=> $idNumber,
            'nipNumber'=> $nip,
            'regonNumber'=> $regon,
            'courtNumber'=> $courtNumber,
            'courtPlace'=> $courtPlace,
            'krsNumber'=> $krsNumber,
            'Role_id'=> $ur,
            'userType_id'=> $ut,
            'phone'=> $phone,
            'birthday' => $birthday,
            'active' => '1',
            'flag_oportunity' => $oportunity,
            'oportunity_reminder' => $oportunity_reminder,
            'notes' => $notes,
            'remember_token' => str_random(10),
            'fb_id' => $id_fb,
            'avatar_tocken' => $av_token,
            'avatar' => $avatar,
            'phone2' => $phone2,
            'email2' => $email2,
        ]);

      	Log::info('Success: '. $success );

        //return $success->id;

        if ($success->id) {
          $address = clientAddress::create([
            'user_id' => $success->id,
            'street' => $street,
            'number' => $number,
            'apartmentNumber' => $apartment,
            'postCode' => $postCode,
            'city' => $city,
            'country_id' => $country

            /*'street' => $data['street'],
            'number' => $data['number'],
            'apartmentNumber' => $data['apartmentNumber'],
            'postCode' => $data['postCode'],
            'city' => $data['city'],
            'country_id' => $data['country']*/
          ]);
        }
        
        ///ENVIO DE CORREO

        $userApp = "";
         
        if($ut == 1){
        	//PERSON
        	$userApp = $pesel;
        }else{
        	//ONE PERSON COMPANY Y COMPANY
        	$userApp = $nip;
        }
        
        
        $password = uniqid();
        $passwordBcrypt = bcrypt($password);
        
        $user = User::find($success->id);
        $user->username = $userApp;
        $user->password	 = $passwordBcrypt;
        $user->save();

        $contract = Contract::where([
                'co_current'	=> true
              ])->first();
          
        $fileContract = $contract->co_path;
        $filePathContract = storage_path('app/app_contract/'.$fileContract);
         
        if(env('ENVIRONMENT') == 'DEV'){
        	$emailData = [
        			'name' 		=> $name . " ". $lastName,
              /*'email' 	=> $data['email'],*/
              'email'   => 'oscar.sanchez@novacloud.mx',
        			'user'		=> $userApp,
        			'pass'		=> $password,
              'contract'=> $filePathContract
        	];
        }
        else{
        	//PROD
        	$emailData = [
        			'name' 		=> $name . " ". $lastName,
              /*'email' 	=> $data['email'],*/
              'email'   => 'biuro@przechowamy-wszystko.pl',
        			'user'		=> $userApp,
              'pass'		=> $password,
              'contract'=> $filePathContract
        	];
        }
         
        //ENVIO DE CORREO
        $resp = Mail::send ( 'emails.2019_welcome_pw', $emailData, function ($message) use ($emailData) {
        
          $message->to ( $emailData['email'] );
          //$message->bcc ( 'oscar.sanchez@novacloud.mx' );
        	 
        	$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
          $message->subject ( 'Welcome to PW ' );
          $message->attach ( $emailData ['contract'], array (
              'as' => 'Regulamin do najmu.pdf',
              'mime' => 'application/pdf'
          ) );
        } );
        	 
        	 
        Log::info('Resp Email: '.$resp);
        
        //--FIN ENVIO DE CORREO

        if ($ut == 3) {

          $legal1 = LegalRepresentative::create([
            'user_id' => $success->id,
            'name' => $data['legalNameP1'],
            'lastName' => $data['legalLastNameP1'],
            'function' => $data['legalFunctionP1'],
            'phone' => $data['legalPhoneP1'],
            'email' => $data['legalEmailP1'],
          ]);

          $legal2 = LegalRepresentative::create([
            'user_id' => $success->id,
            'name' => $data['legalNameP2'],
            'lastName' => $data['legalLastNameP2'],
            'function' => $data['legalFunctionP2'],
            'phone' => $data['legalPhoneP2'],
            'email' => $data['legalEmailP2'],
          ]);

          if ($legal1 != null && $legal2 != null) {
            return $legal2;
          }

        }else{

          if($mobile == 1){
            return ['success' => 'true','id' => $success->id,'username' => $data['username'], 'avatar' => $avatar, 'avatar_token' => $av_token];
          }else{
            return $address->id;
          }
        }
       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id = \Auth::user()->id;
        $data = User::with(['userAddress','UserLegal'])->where('id','=', $id )->first();
        $countries = \Countries::all()->sortBy('name');
        //dd($data);
        return view('pw.user_profile',['udata' => $data,'countries' => $countries]);
    }

    public function showWs(Request $data)
    {
        $id = $data['id'];
        $data = User::with(['userAddress','UserLegal'])->where('id','=', $id )->first();
        //$countries = \Countries::all()->sortBy('name');

        //return view('pw.user_profile',['udata' => $data,'countries' => $countries]);
        return ['success' => true, 'user_data' => $data];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $column = 'id';
      $data = User::with(['userAddress','UserLegal'])->where($column,'=', $request['id'])->first();
      if ($request->ajax()) {
        return['success' => true,
                'client' => $data
              ];
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data)
    {
    	Log::info('pwController@update');
    	
      //dd($data['hdfut']);
      $column = 'id';
      $client = User::find($data->get('id'));

      //$data->active = 0;
      //$res = $data->save();

      isset($data['companyName']) ? $company = $data['companyName'] : $company = '';
      isset($data['nipNumber']) ? $nip = $data['nipNumber'] : $nip = '';
      isset($data['regonNumber']) ? $regon = $data['regonNumber'] : $regon = '';
      isset($data['courtNumber']) ? $courtNumber = $data['courtNumber'] : $courtNumber = '';
      isset($data['courtPlace']) ? $courtPlace = $data['courtPlace'] : $courtPlace = '';
      isset($data['krsNumber']) ? $krsNumber = $data['krsNumber'] : $krsNumber = '';
      isset($data['Legal_person1']) ? $person1 = $data['Legal_person1'] : $person1 = '';
      isset($data['Legal_person2']) ? $person2 = $data['Legal_person2'] : $person2 = '';
      isset($data['peselNumber']) ? $pesel = $data['peselNumber'] : $pesel = '';
      isset($data['idNumber']) ? $idNumber = $data['idNumber'] : $idNumber = '';
      isset($data['name']) ? $name = $data['name'] : $name = '';
      isset($data['lastName']) ? $lastName = $data['lastName'] : $lastName = '';
      isset($data['birthday']) ? $birthday = $data['birthday'] : $birthday = '';
      isset($data['hdfut']) ? $ut = $data['hdfut'] : $ut = '1';
      isset($data['hdfr']) ? $ur = $data['hdfr'] : $ur = '3';
      isset($data['chkOportunity']) ? $oportunity = $data['chkOportunity'] : $oportunity = '0';
      isset($data['oportunity_reminder']) ? $oportunity_reminder = $data['oportunity_reminder'] : $oportunity_reminder = '00-00-00';
      isset($data['profileEdit']) ? $profile = $data['profileEdit'] : $profile = 0;
      isset($data['notes']) ? $notes = $data['notes'] : $notes = '';
      isset($data['email2']) ? $email2 = $data['email2'] : $email2 = '';
      isset($data['phone2']) ? $phone2 = $data['phone2'] : $phone2 = '';
      isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
      
      //$user = User::where(['email' => $data->get('email')])->where('id','<>',$data->get('id'))->first();
      $user = null;
      Log::info($user);
      
      if(!is_null($user)){
      	return ['success' => false];
      }
      
      $client->email 		= $data->get('email');
      $client->name 		= $name;
      $client->lastName 	= $lastName;
      $client->companyName 	= $company;
      $client->peselNumber 	= $pesel;
      $client->idNumber 	= $idNumber;
      $client->nipNumber 	= $nip;
      $client->regonNumber 	= $regon;
      $client->courtNumber 	= $courtNumber;
      $client->courtPlace 	= $courtPlace;
      $client->krsNumber 	= $krsNumber;
      $client->phone	 	= $data['phone'];
      $client->birthday 	= $birthday;
      $client->accountant_number = $data->get('accountant_number');
      $client->accountant_code = $data->get('accountant_code');
      
      if ($profile == 0) {
        $client->Role_id = $ur;
        $client->userType_id = $ut;
        $client->flag_oportunity = $oportunity;
        $client->oportunity_reminder = $oportunity_reminder;
        $client->notes = $notes;
        $client->phone2 = $phone2;
        $client->email2 = $email2;
      }
      $res = $client->save();

        //return $success->id;

        if ($res) {
          $column = 'user_id';
          $address = clientAddress::where($column,'=', $data['id'])->first();
          //dd($data['id']);
          //dd($data['street']);
            $address->street = $data['street'];
            $address->number = $data['number'];
            $address->apartmentNumber = $data['apartmentNumber'];
            $address->postCode = $data['postCode'];
            $address->city = $data['city'];
            $address->country_id = $data['country'];
            $add = $address->save();

        }

        if ($add && $ut == 3) {
          $column = 'id';
          $legal1 = LegalRepresentative::find($data->get('legalIdP1'));
          //dd($legal1);

            $legal1->name = $data['legalNameP1'];
            $legal1->lastName = $data['legalLastNameP1'];
            $legal1->function = $data['legalFunctionP1'];
            $legal1->phone = $data['legalPhoneP1'];
            $legal1->email = $data['legalEmailP1'];
            $l1 = $legal1->save();

          $legal2 = LegalRepresentative::find($data->legalIdP2);

            $legal2->name = $data['legalNameP2'];
            $legal2->lastName = $data['legalLastNameP2'];
            $legal2->function = $data['legalFunctionP2'];
            $legal2->phone = $data['legalPhoneP2'];
            $legal2->email = $data['legalEmailP2'];
            $l2 = $legal2->save();

          if ($l2 != null && $l1 != null) {
            return ['res' => $l2];
          }

        }else{

          if ($mobile) {
            if ($add) {
              return ['success' => true];
            }else{
              return ['success' => false];
            }
          }

          if ($data->ajax()) {
            return ['res' => $add];
          }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $column = 'id';
      $data = User::where($column,'=', $request['id'])->first();

      $data->active = 0;
      $res = $data->save();
      return ['res'=>$res];

    }

    public function checkUsername(Request $request){
      //define_syslog_variables();
      /*openlog("wp_app", 0, LOG_LOCAL0);
      syslog(LOG_DEBUG,"searching for: ".$request['username']);
      closelog();*/

      $column = 'username';
      $data = User::where($column,'=', $request['username'])->first();

      //dd($data);

      if($data == null || $data->id == 0 || $data->id == '' ){

        return json_encode(true);
      }else{

        return json_encode(trans('messages.usernameCheckFail'));
      }
    }

    public function checkEmail(Request $request){
      $column = 'email';
      $data = User::where($column,'=', $request['email'])->first();

      //dd($data);

      if($data == null || $data->id == 0 || $data->id == '' ){

        return json_encode(true);
      }else{

        return json_encode(trans('messages.mailCheckFail'));
      }
    }

    
    public function oportunitiesList(Request $request){
      return view('pw.users_oportunities');
    }

    public function tenantsList(Request $request){
      return view('pw.users_tenants');
    }

    public function getOpotunitiesList(Request $data){
      $username = $data['username'];
      $email = $data['email'];
      $name = $data['name'];
      $last = $data['last'];
      $company = $data['company'];
      $startDate = $data['start_date'];
      $endDate = $data['end_date'];

      if($startDate == "" && $endDate == ""){
      	$clients = User::with(['RoleDesc','UserType'])->where('id', '!=', '1')->where('active', '!=', '0')->where('username', 'LIKE', '%'.$username.'%')->where('email', 'LIKE', '%'.$email.'%')->where('name', 'LIKE', '%'.$name.'%')->where('lastName', 'LIKE', '%'.$last.'%')->where('companyName', 'LIKE', '%'.$company.'%')->where('flag_oportunity','=','1')->get();
      }elseif($startDate != "" && $endDate == ""){
      	
      	$clients = User::with(['RoleDesc','UserType'])->where('id', '!=', '1')->where('active', '!=', '0')
      				->where('username', 'LIKE', '%'.$username.'%')->where('email', 'LIKE', '%'.$email.'%')->where('name', 'LIKE', '%'.$name.'%')->where('lastName', 'LIKE', '%'.$last.'%')->where('companyName', 'LIKE', '%'.$company.'%')
      				->where('flag_oportunity','=','1')
      				->where('oportunity_reminder','>=',$startDate)
      				->get();
      }elseif($startDate == "" && $endDate != ""){
      	
      	$clients = User::with(['RoleDesc','UserType'])->where('id', '!=', '1')->where('active', '!=', '0')
      				->where('username', 'LIKE', '%'.$username.'%')->where('email', 'LIKE', '%'.$email.'%')->where('name', 'LIKE', '%'.$name.'%')->where('lastName', 'LIKE', '%'.$last.'%')->where('companyName', 'LIKE', '%'.$company.'%')
      				->where('flag_oportunity','=','1')
      				->where('oportunity_reminder','<=',$endDate)
      				->get();
      }else{
      	
      	$clients = User::with(['RoleDesc','UserType'])->where('id', '!=', '1')->where('active', '!=', '0')
      				->where('username', 'LIKE', '%'.$username.'%')->where('email', 'LIKE', '%'.$email.'%')->where('name', 'LIKE', '%'.$name.'%')->where('lastName', 'LIKE', '%'.$last.'%')->where('companyName', 'LIKE', '%'.$company.'%')
      				->where('flag_oportunity','=','1')
      				->where('oportunity_reminder','>=',$startDate)
      				->where('oportunity_reminder','<=',$endDate)
      				->get();
      }
      

      $date = \Carbon\Carbon::now()->format('Y-m-d');
      //$date = $date->year.'-'.$date->month.'-'.$date->day;
      //dd($date);

      $cad = '';
      foreach ($clients as $key => $client) {

        $date2 = \Carbon\Carbon::parse($client->oportunity_reminder)->format('Y-m-d');

        if($date > $date2){
          $class = 'danger';
        }elseif($date == $date2){
          $class = 'success';
        }else{
          $class = 'active';
        }

        ($client->companyName != '') ? $company = $client->companyName : $company = '--------';
        ($client->name != '') ? $name = $client->name : $name = '--------';
        ($client->lastName != '') ? $lastName = $client->lastName : $lastName = '--------';
        $cad .= '<tr class="'.$class.'">';
        $cad .= '<td>'.$client->username.'</td>';
        $cad .= '<td>'.$name.'</td>';
        $cad .= '<td>'.$lastName.'</td>';
        $cad .= '<td>'.$client->oportunity_reminder.'</td>';
        $cad .= '<td>'.$client->UserType->name.'</td>';
        $cad .= '<td>'.$client->RoleDesc->name.'</td>';
        $cad .= '<td>'.$company.'</td>';
        $cad .= '<td><a href="javascript:void(0);" id="'.$client->id.'" level="'.$client->Role_id.'" dir="'.$client->userType_id.'" class="btn btn-default btn-xs editClient"><i class="fa fa-pencil" aria-hidden="true"></i> </a>';
        $cad .= '<a href="javascript:void(0);" id="'.$client->id.'" dir="'.$client->userType_id.'" class="btn btn-primary btn-xs btnDelClient"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
        $cad .= '</tr>';
      }

      return $cad;
    }

    public function getTenantsList(Request $data){
      $username = $data['username'];
      $email = $data['email'];
      $name = $data['name'];
      $last = $data['last'];
      $company = $data['company'];
      $st = $data['st'];

      $arrValues = ['username' => $username,'email' => $email,'name' => $name,'last' => $last,'company' => $company];

      $storages = Storages::with(['StorageOwner','StorageLastBilling'])
                    ->whereHas('StorageOwner',function($query) use ($arrValues){
                        if ($arrValues['username'] !='') {
                          $query->where('username','LIKE','%'.$arrValues['username'].'%');
                        }
                        if ($arrValues['email'] !='') {
                          $query->where('email','LIKE','%'.$arrValues['email'].'%');
                        }
                        if ($arrValues['name'] !='') {
                          $query->where('name','LIKE','%'.$arrValues['name'].'%');
                        }
                        if ($arrValues['last'] !='') {
                          $query->where('lastName','LIKE','%'.$arrValues['last'].'%');
                        }
                        if ($arrValues['company'] !='') {
                          $query->where('companyName','LIKE','%'.$arrValues['company'].'%');
                        }
                    })
                    ->where(function($query) use ($st){
                      if ($st !='') {
                        $query->where('alias','=',$st);
                      }
                    })
                    ->where('flag_rent','=',1)
                    ->get();


      //return [$storages[0]->StorageLastBilling];

      //$clients = User::with(['RoleDesc','UserType'])->where('id', '!=', '1')->where('active', '!=', '0')->where('username', 'LIKE', '%'.$username.'%')->where('email', 'LIKE', '%'.$email.'%')->where('name', 'LIKE', '%'.$name.'%')->where('lastName', 'LIKE', '%'.$last.'%')->where('companyName', 'LIKE', '%'.$company.'%')->where('flag_oportunity','=','1')->get();

      $date = \Carbon\Carbon::now()->format('Y-m-d');
      //$date = $date->year.'-'.$date->month.'-'.$date->day;
      //dd($date);

      $cad = '';
      foreach ($storages as $key => $client) {
        //obtenemos el ultimo pago
        $var = $client->StorageBilling->sortByDesc('id')->first();
        $utilFecha = new Fecha();
        //sacar los dias de retraso para pintar la clase
        // a- checamos el mes del ultimo pago
        $now = new \DateTime('now');
        $currentDate = $now->format('Y-m-d');
        $month = $now->format('m');
        //$daysLate = $month;
        $payMonth = $utilFecha->getMes($var->pay_month);
        $daysLate = $payMonth;

        if ($var->flag_payed == 1) {
          if ($payMonth == $month) {
            $class = 'default';
            $daysLate = 0;
          }else{
            $class = 'warning';
            $daysLate = $utilFecha->getDaysBetween($currentDate,$var->pay_month);
          }
        }else{
          $class = 'warning';
          $daysLate = $utilFecha->getDaysBetween($currentDate,$var->pay_month);
        }

        //return [$var];

        ($client->StorageOwner->companyName != '') ? $company = $client->StorageOwner->companyName : $company = '--------';
        ($client->StorageOwner->name != '') ? $name = $client->StorageOwner->name : $name = '--------';
        ($client->StorageOwner->lastName != '') ? $lastName = $client->StorageOwner->lastName : $lastName = '--------';
        $cad .= '<tr class="'.$class.'">';
        $cad .= '<td>'.$client->StorageOwner->username.'</td>';
        $cad .= '<td>'.$name.'</td>';
        $cad .= '<td>'.$lastName.'</td>';
        $cad .= '<td>'.$company.'</td>';
        $cad .= '<td>'.$client->alias.'</td>';
        $cad .= '<td>'.$var->pay_month.'</td>';
        $cad .= '<td>'.$daysLate.'</td>';
        //$cad .= '<td><a href="javascript:void(0);" id="'.$client->id.'" level="'.$client->Role_id.'" dir="'.$client->userType_id.'" class="btn btn-default btn-xs editClient"><i class="fa fa-pencil" aria-hidden="true"></i> </a>';
        $cad .= '<td><a href="javascript:void(0);" id="'.$client->id.'" dir="'.$client->userType_id.'" class="btn btn-info btn-xs viewInvoices" data-toggle="tooltip" data-original-title="Invoices"><i class="fa fa-list-ol" aria-hidden="true"></i></a></td>';
        $cad .= '</tr>';
      }

      return $cad;
    }

    public function findUser(Request $data){

    	$term = $data['term'];
    	
        $users = User::where([
        					'flag_oportunity' 	=> '0' ,
        					'active'			=> '1' 
        				])->where(function ($query) use($term) {
	        				$query->where('username', '=', $term);
	        				$query->orWhere('name', 'like','%'.$term.'%');
	        				$query->orWhere('lastName','like','%'.$term.'%');
	        				$query->orWhere('companyName','like','%'.$term.'%');
	        				$query->orWhere('email','like','%'.$term.'%');
	        			})->get();

        if($users == null){

          return json_encode(trans('calculator.findUserFail'));
        }else{

          return json_encode($users);
        }
    }


    public function getToken(){
      $token = csrf_token();

      if($token != ''){
        //return json_encode(array('success' => 'true', 'token' => $token));
        return ['success' => 'true', 'token' => $token];
      }else{
        //return json_encode(array('success' => 'false'));
        return ['success' => 'false'];
      }
    }


    public function getLogin(Request $request){

      isset($request['id_fb']) ? $id_fb = $request['id_fb'] : $id_fb = 0;

      // verifivamos si el id de facebook existe en la bd
      if ($id_fb != 0) {
        $user = User::where('fb_id', '=', $id_fb)->first();

        if($user == null){
          return ['success' => false, 'message' => 'User not valid'];
        }else{
          return ['success' => true, 'message' => 'Ok','id' => $user->id, 'username' => $user->username,'email' => $user->email, 'avatar' => $user->avatar, 'avatar_token' => $user->avatar_tocken, 'idNumber' => $user->idNumber, 'user_type' => $user->userType_id ];
        }

      }else{
        $user = $request->get('user');
        $pwd = $request->get('password');
        $user = User::where('username', '=', $user)->first();

        if($user == null){
          return ['success' => false, 'message' => 'User not valid'];
        }else{

          if(\Hash::check($pwd, $user->password)){
            return ['success' => true, 'message' => 'Ok','id' => $user->id, 'username' => $user->username, 'avatar' => $user->avatar, 'avatar_token' => $user->avatar_tocken,'user_type' => $user->userType_id ];
          }else{
            return ['success' => false, 'message' => 'User or password not valid'];
          }
        }

      }


/*
      $user = User::where('username', '=', $user)->where('password', '=', $pwd)->first();

      if($user == null){
        return ['success' => 'false'];
      }else{
        return ['success' => 'true'];
      }
*/

    }


    public function update_avatar(Request $request) {

		    $id = $request->get ('id');
        $mobile = $request->get ('mobile');

		    if ($request->hasFile ('fileAvatar')) {

			      $user = User::where (["id" => $id])->first();

			      $file = \Input::file('fileAvatar');

			      $destinationPath = 'assets/img/avatar';

            $code = uniqid();

			      $extension = \Input::file('fileAvatar')->getClientOriginalExtension();
			      $fileName = 'avatar'.csrf_token().'_'.$user->id.'_'.$code.'.'. $extension;

			      \Input::file('fileAvatar')->move( $destinationPath,$fileName);
			      $user->avatar = $destinationPath . "/" . $fileName;
            $user->avatar_tocken = uniqid();

			      $fullPath = $destinationPath . "/" . $fileName;

			      if (File::exists ( $fullPath )) {

				          // primero cambiamos el background insertando en una nueva imagaen blanca..
				          $width = Image::make($fullPath)->width();
				          $height = Image::make($fullPath)->height();
				          Image::canvas($width,$height, '#FFFFFF')->insert($fullPath)->save($fullPath);

				          $img = Image::make($fullPath);
				          $img->fit(300)->save();

				          // si es mobile, cambiamos orientacion correspondientemente
				          $useragent = $_SERVER ['HTTP_USER_AGENT'];

				          if (preg_match ( '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent ) || preg_match ( '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr ( $useragent, 0, 4 ) )) {
					              $absolutePath = public_path () . "/img/avatars/$fileName";
					              Log::info ( 'Imagen es de Mobile.. ajustando orientacion... PATH: ' . $absolutePath );

					              $rotate = new imageRotate ();
					              $rotate->fixOrientation ( $absolutePath );
				          }
          				/*
          				$resize = new ResizeImage ( $fullPath );
          				$resize->resizeTo ( 300, 300, 'maxwidth' );

          				$archivador = $fullPath;
          				$resize->saveImage ( $archivador );

          				$absolutePath = public_path () . "/img/avatars/$fileName";
          				// Intervention\Image\Facades\Image::crop(300,300)->save();
          				\Intervention\Image\Facades\Image::make ( $absolutePath )->crop ( 300, 300 )->save (); */


			      }

			      $user->save ();

			      return ['success' => 'true', 'path_img' => asset ( $destinationPath . "/" . $fileName )];
		    } else {
			       return ['success' => 'false'];
		    }
    }

    public function cata_countries(){
      $countries = \Countries::all()->sortBy('name');
      return ['countries' => $countries];
    }

//end class
}
