<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BillingDetail;
use App\Http\Controllers\Controller;

class InsuranceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurances = BillingDetail::with(['billing.user', 'billing.storages'])->where('bd_tipo_partida', '=', 'INSURANCE')->get(); 

        //dd($insurances);  

        return view('pw.reports.Insurance.insurance_report', ['insurances'=>$insurances]);
    }

    public function dateFilter($año, $mes, $dia) {
        $date = $año.'-'.$mes.'-'.$dia;
        $insurances = BillingDetail::with(['billing' => function($qry) use ($date) {
            $qry->where('pay_month', '=', $date);
        }, 'billing.user', 'billing.storages'])->where('bd_tipo_partida', '=', 'INSURANCE')->get();

        //dd($insurances);  

        return view('pw.reports.Insurance.insurance_report', ['insurances'=>$insurances]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
