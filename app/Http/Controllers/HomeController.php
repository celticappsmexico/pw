<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //Auth::user()->order=1;
        //session::put('order', 1);
        //dd(Auth::user());
        \Session::put('order', 0);
        return \View::make('home');
        //return \View::make('successPay');
    }

    public function fac() {
        return view('factura');
    }
}
