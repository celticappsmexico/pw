<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Deposito;
use Illuminate\Support\Facades\Log;
use App\CataPaymentTypes;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use App\CardUser;
use App\Storages;
use App\PaymentData;

class DepositosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($storage_id, $client_id)
    {
        //
        $storage = Storages::find($storage_id);
        
        return view('deposits.create_deposit',[
        		'storage_id'	=> $storage_id,
        		'client_id'		=> $client_id,
        		'storage'		=> $storage
        ]);
    }
    
    public function createPago($id)
    {
    	
    	$paymentsType = CataPaymentTypes::where('active','1')->get();
    	
    	$deposito = Deposito::find($id);
    	$storage = Storages::with('StoragePaymentData')
    				->find($deposito->storage_id);
    	
    	$creditCard = $storage->StoragePaymentData[0]->creditcard;
    	
    	return view('deposits.pay_deposit',[
    			'id'				=> $id ,
    			'catapaymenttype' 	=> $paymentsType,
    			'storage'			=> $storage,
    			'deposito'			=> $deposito,
    			'creditCard'		=> $creditCard
    			
    	]);
    }
    
    public function storePago(Request $request)
    {
    	$deposito = Deposito::find($request->get('id'));
    	$deposito->de_pagado = 1;
    	$deposito->payment_type_id 	= $request->get('paymentType');
    	$deposito->de_notas 		= $request->get('de_notas');
    	
    	if($request->get('paymentType') == 2)
    	{
    		Log::info('Paga deposito en braintree');
    		//IR A BRAINTREE
    		$cardUser = CardUser::find($request->get('card_user_id'));
    		
    		$storage = Storages::find($deposito->storage_id);
    		$paymentData = PaymentData::find($storage->payment_data_id);
    		
    		$result = Braintree_Transaction::sale(
    				[
    						'customerId' 	=> $cardUser->cu_customer_id,
    						'amount' 		=> $deposito->de_monto_deposito,
    						'orderId' 		=> $paymentData->order_id
    				]
    		);
    		 
    		if ($result->success) {
    			Log::info('Transaction ID Deposito: '. $result->transaction->id);
    			$deposito->de_transaction_id_braintree = $result->transaction->id;
    			
    			//return $result->transaction->id;
    		} else {
    			$errorFound = '';
    			foreach ($result->errors->deepAll() as $error1) {
    				$errorFound .= $error1->message . "<br />";
    			}
    		}
    	}
    	
    	$deposito->save();
    	
    	return ['returnCode' => 200, 'msg' 	=> trans('cart.deposit_paid')];
    	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Log::info('DepositosController@store');
        
    	$deposito = new Deposito();
        
        $deposito->storage_id 		= $request->get('storage_id');
        $deposito->user_id			= $request->get('user_id');
        $deposito->de_monto_deposito= $request->get('de_monto_deposito');  
        
        $deposito->save();
        
        Log::info('INFO DEL DEPOSITO... ');
        Log::info('STORAGE ID: '. $request->get('storage_id'));
        Log::info('USER ID: '.$request->get('user_id'));
        Log::info('MONTO DEL DEPOSITO: '. $request->get('de_monto_deposito'));
        Log::info('ID DEL DEPOSITO: '. $deposito->id);
        
        //return ['success' => true];
        
        return redirect()->route('users'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    public function updateRegresarDeposito($id)
    {
    	$deposito = Deposito::find($id);
    	$deposito->de_regresado = 1;
    	$deposito->save();
    	
    	return ['returnCode' => 200, 'msg' => trans('client.returned_deposit')];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
