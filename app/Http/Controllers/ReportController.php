<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\utilBillingFilters;
use Carbon\Carbon;
use App\Storages;
use App\Billing;
use Yajra\Datatables\Datatables;
use App\Library\Formato;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Lang;
use App\Library\FileSystem;
use App\BillingDetail;
use App\User;
use Illuminate\Support\Facades\DB;
use App\TransaccionBraintree;
use App\History;
use App\PaymentData;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use App\Countries;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pw.reports');
    }
    
    public function indexBadPayers()
    {
    	return view('pw.reports.bad_payers.bad_payers_index');
    }
    
    public function indexBadPayersDT(Request $request)
    {
    	$billings = Billing::join('users','user_id','=','users.id')
    					->where('cargo' ,'>', '0')
    					->where('flag_payed','0')
    					->select('*',
    							DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname'),
    							DB::raw('DATE_FORMAT(billing.created_at, "%Y-%m-%d") as fecha_documento')
    							
    							)
    					->get();

    	
    	return Datatables::of($billings)
    			/*->editColumn('accountant_number',function($row){
    				/*
    				if($row->user->accountant_number == ""){
    					return "---";
    				}else{
    					return $row->user->accountant_number;
    				}
    				
    				$user = User::find($row->id);
    				return $user->accountant_number;
    				
    			})*/
    			->editColumn('fecha_pago',function($row){
    				return Carbon::parse($row->created_at)->addDay(7)->format('Y-m-d');
    			})
    			->editColumn('cargo',function($row){
    				return Formato::formatear('ZLOTY', $row->cargo);
    			})
    			->editColumn('dias_retraso', function($row){
    				$inicio = Carbon::parse($row->created_at)->addDay(7);
    				$fin = Carbon::now();

    				$dif = $inicio->diffInDays($fin);
    				
    				//1-30,31-60,61-90,91-180,181-365
    				
    				$result = "";
    				
    				switch($dif){
    					case ($dif > 1 && $dif <= 30):
    						$result = "1-30";
    						break;
    						
    					case ($dif > 30 && $dif <= 60):
    						$result = "31-60";
    						break;
    					
    					case ($dif > 60 && $dif <= 90):
    						$result = "61-90";
    						break;
    					
    					case ($dif > 90 && $dif <= 180):
    						$result = "91-180";
    						break;
    						
    					case ($dif > 180 && $dif <= 365):
    						$result = "181-365";
    						break;
    						
    					default:
    						$result = ">365";
    						break;
    				}
    				
    				// show difference in days between now and end dates
    				//return $inicio->diffInDays($fin);
    				//Log::info($result);
    				return $result;
    			})
    			->addColumn('download_pdf',function($row){
    				if($row->pdf == ""){
    					return '--';
    				}
    				else{
    					return '<a href='.route('billing.download_pdf',[$row->id]).' class="btn btn-primary btn-pdf btn-sm">
    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$row->pdf.'
    							</a>';
    				}
    			})
    			->addColumn('ver_cliente',function($row){
    				return '<a href='.route('users',[$row->user_id]).' class="btn btn-default btn-sm">
    								<i class="fa fa-search" aria-hidden="true"></i>
    							</a>';
    			})
    			->make(true);
    }
    
    public function indexBadPayersDTCSV()
    {
    	$billings = Billing::where('cargo' ,'>', '0')
			    	->where('flag_payed','0')
			    	->with('user')
			    	->get();
    	
    	$headers = array(
    			"Content-type" => "text/csv",
    			"Content-Disposition" => "attachment; filename=BadPayers_".Carbon::now()->format('Y-m-d_H-m-s').".csv",
    			"Pragma" => "no-cache",
    			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
    			"Expires" => "0"
    	);
    	
    	$accountant_number = Lang::get('client.accountant_number');
    	
    	$columns = array('"'.$accountant_number.'"','"'.Lang::get('cart.client').'"', '"'.Lang::get('reports.date_document').'"', 
    							'"'.Lang::get('reports.payment_date').'"', '"'.Lang::get('reports.charge').'"', '"'.Lang::get('reports.days_late').'"', 
    							'"'.Lang::get('client.pdf_label').'"');
    		
    	$content = "";
    	
    	$content = implode(',', $columns)."\n";
    	
    	foreach($billings as $billing){
    		$fullName 		= $billing->user->name . " ". $billing->user->lastName . " ". " - ". $billing->user->companyName;
    		$fechaDocumento = Carbon::parse($billing->created_at)->format('Y-m-d');
    		$fechaPago 		= Carbon::parse($billing->created_at)->addDay(7)->format('Y-m-d');
    		$cargo 			= Formato::formatear('ZLOTY', $billing->cargo);
    		
    		$inicio = Carbon::parse($billing->created_at)->addDay(7);
    		$fin = Carbon::now();
    		
    		$dif = $inicio->diffInDays($fin);
    		
    		$result = "";
    		
    		switch($dif){
    			case ($dif > 1 && $dif <= 30):
    				$result = "1-30";
    				break;
    		
    			case ($dif > 30 && $dif <= 60):
    				$result = "31-60";
    				break;
    					
    			case ($dif > 60 && $dif <= 90):
    				$result = "61-90";
    				break;
    					
    			case ($dif > 90 && $dif <= 180):
    				$result = "91-180";
    				break;
    		
    			case ($dif > 180 && $dif <= 365):
    				$result = "181-365";
    				break;
    		
    			default:
    				$result = ">365";
    				break;
    		}
    		
    		$content.= '"'.$billing->user->accountant_number.'","'.$fullName.'","'.$fechaDocumento.'","'.$fechaPago.'","'.$cargo.'","'.$result.'","'.$billing->pdf.'"'."\n";
    	}
    
    	
    	return \Illuminate\Support\Facades\Response::make($content, 200, $headers);
    }
    
    public function indexInvoices($paragon)
    {
    	$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();

    	return view('pw.reports.invoices.invoices_report',[
    					'start'		=> $start,
    					'end'		=> $end,
    					'paragon'	=> $paragon
    			]);
    }
    
    public function indexInvoicesDT(Request $request)
    {
    	if($request->get('client') == "" && $request->get('number_invoices') == ""){
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon', $request->get('paragon'))
    		->where('billing.created_at','>=',$request->get('fechaInicio'))
    		->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento')
    				)
    		 
    		->with('user','storages')
    		->get();
    	}
    	elseif($request->get('number_invoices') != "" && $request->get('client') != ""){
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$request->get('number_invoices'));
    		
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}
    		
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon', $request->get('paragon'))
    		->where('billing.created_at','>=',$request->get('fechaInicio'))
    		->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    		->where('name',  'LIKE', '%'.$request->get('client').'%')
    		->orWhere('lastName',  'LIKE', '%'.$request->get('client').'%')
    		->orWhere('companyName',  'LIKE', '%'.$request->get('client').'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->with('user','storages')
    		->get();
    	}
    	elseif($request->get('number_invoices') != ""){
    		
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$request->get('number_invoices'));
    		
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}    	
    		
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon', $request->get('paragon'))
    		->where('billing.created_at','>=',$request->get('fechaInicio'))
    		->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->with('user','storages')
    		->get();
    	}
    	elseif($request->get('client') != ""){

    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon', $request->get('paragon'))
    		->where('billing.created_at','>=',$request->get('fechaInicio'))
    		->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    		->where('name',  'LIKE', '%'.$request->get('client').'%')
    		->orWhere('lastName',  'LIKE', '%'.$request->get('client').'%')
    		->orWhere('companyName',  'LIKE', '%'.$request->get('client').'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->with('user','storages')
    		->get();
    		
    	}
    	
    	//OPTIMIZAR CODIGO
    	if($request->get('from_invoice') != ""){
    		if($request->get('client') == "" && $request->get('number_invoices') == ""){
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon', $request->get('paragon'))
    			->where('billing.created_at','>=',$request->get('fechaInicio'))
    			->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    			->where('bi_number_invoice','>=',$request->get('from_invoice'))
    			->where('bi_number_invoice','<=',$request->get('to_invoice'))
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			 
    			->with('user','storages')
    			->get();
    		}
    		elseif($request->get('number_invoices') != "" && $request->get('client') != ""){
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$request->get('number_invoices'));
    		
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    		
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon', $request->get('paragon'))
    			->where('billing.created_at','>=',$request->get('fechaInicio'))
    			->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    			->where('name',  'LIKE', '%'.$request->get('client').'%')
    			->orWhere('lastName',  'LIKE', '%'.$request->get('client').'%')
    			->orWhere('companyName',  'LIKE', '%'.$request->get('client').'%')
    			->where('bi_number_invoice','>=',$request->get('from_invoice'))
    			->where('bi_number_invoice','<=',$request->get('to_invoice'))
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('user','storages')
    			->get();
    		}
    		elseif($request->get('number_invoices') != ""){
    		
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$request->get('number_invoices'));
    		
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    		
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon', $request->get('paragon'))
    			->where('billing.created_at','>=',$request->get('fechaInicio'))
    			->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    			->where('bi_number_invoice','>=',$request->get('from_invoice'))
    			->where('bi_number_invoice','<=',$request->get('to_invoice'))
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('user','storages')
    			->get();
    		}
    		elseif($request->get('client') != ""){
    		
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon', $request->get('paragon'))
    			->where('billing.created_at','>=',$request->get('fechaInicio'))
    			->where('billing.created_at','<=',Carbon::parse($request->get('fechaFin'))->addDays(1))
    			->where('name',  'LIKE', '%'.$request->get('client').'%')
    			->orWhere('lastName',  'LIKE', '%'.$request->get('client').'%')
    			->orWhere('companyName',  'LIKE', '%'.$request->get('client').'%')
    			->where('bi_number_invoice','>=',$request->get('from_invoice'))
    			->where('bi_number_invoice','<=',$request->get('to_invoice'))
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.id as idBilling'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->with('user','storages')
    			->get();
    		
    		}	
    	}
    	
    	//->where('username', 'LIKE', '%'.$request->get('username').'%')
			    	 
    	return Datatables::of($billings)
		    	->addColumn('accountant_number',function($row){
		    		if(!isset($row->user->accountant_number)){
		    			return "";
		    		}else{
		    			return $row->user->accountant_number;
		    		}
		    	})
		    	->addColumn('fullname',function($row){
		    		return $row->user->name . " ". $row->user->lastName . " ". " - ". $row->user->companyName;
		    	})
		    	->editColumn('fecha_documento',function($row){
		    		return Carbon::parse($row->fechaDocumento)->format('Y-m-d');
		    	})
		    	->editColumn('fecha_pago',function($row){
		    		return Carbon::parse($row->fechaDocumento)->addDay(7)->format('Y-m-d');
		    	})
		    	->editColumn('cargo',function($row){
		    		return Formato::formatear('ZLOTY', $row->cargo);
		    	})
		    	->editColumn('dias_retraso', function($row){
		    		$inicio = Carbon::parse($row->fechaDocumento)->addDay(7);
		    		$fin = Carbon::now();
		    	
		    		$dif = $inicio->diffInDays($fin);
		    	
		    		$result = "";
		    	
		    		switch($dif){
		    			case ($dif > 1 && $dif <= 30):
		    				$result = "1-30";
		    				break;
		    	
		    			case ($dif > 30 && $dif <= 60):
		    				$result = "31-60";
		    				break;
		    					
		    			case ($dif > 60 && $dif <= 90):
		    				$result = "61-90";
		    				break;
		    					
		    			case ($dif > 90 && $dif <= 180):
		    				$result = "91-180";
		    				break;
		    	
		    			case ($dif > 180 && $dif <= 365):
		    				$result = "181-365";
		    				break;
		    	
		    			default:
		    				$result = ">365";
		    				break;
		    		}
		    	
		    		// show difference in days between now and end dates
		    		//return $inicio->diffInDays($fin);
		    		return $result;
		    	})
		    	->addColumn('download_pdf',function($row){
		    		if($row->pdf == ""){
		    			return '--';
		    		}
		    		else{
		    			return '<a href='.route('billing.download_pdf',[$row->idBilling]).' class="btn btn-primary btn-pdf btn-sm">
		    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$row->pdf.'
		    							</a>';
		    		}
		    	})
		    	->addColumn('download_pdf_copy',function($row){
		    		if($row->pdf == ""){
		    			return '--';
		    		}
		    		else{
		    			$pdfName = $row->pdf;
		    			$arrPDFName = explode('.',$pdfName);
		    			$pdfNameCopy = $arrPDFName[0].'_COPY'.'.'.$arrPDFName[1];
		    			
		    			return '<a href='.route('billing.download_pdf_copy',[$row->idBilling]).' class="btn btn-primary btn-pdf btn-sm">
		    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download Copy
		    							</a>';
		    		}
		    	})
		    	->addColumn('ver_cliente',function($row){
		    		return '<a href='.route('users',[$row->user_id]).' class="btn btn-default btn-sm">
		    								<i class="fa fa-search" aria-hidden="true"></i>
		    							</a>';
		    	})
		    	->addColumn('pagado',function($row){
		    		if($row->flag_payed == 1){
		    			return '<i class="fa fa-check-circle fa-2x" aria-hidden="true" style="color:green"></i>';
		    		}else{
		    			return '<i class="fa fa-times-circle fa-2x bg-red" aria-hidden="true" style="color:red"></i>';
		    		}
		    	})
		    	->editColumn('bi_subtotal', function($row){
		    		return Formato::formatear('ZLOTY', $row->bi_subtotal) ;
		    	})
		    	->editColumn('bi_total_vat', function($row){
		    		return Formato::formatear('ZLOTY', $row->bi_total_vat) ;
		    	})
		    	->editColumn('bi_total', function($row){
		    		return Formato::formatear('ZLOTY', $row->bi_total) ;
		    	})
		    	->addRowAttr ( 'class', function ($row) {
		    		if($row->user->accountant_number == ""){
		    			return 'red';
		    		}
		    	})
		    	->make(true);
    }
    
    public function indexInvoicesDTCSV($fechaInicio,$fechaFin,$client = false,$numberInvoices = false,$from = false, $to = false,$paragon = false)
    {
    	/*
    	$billings = Billing::where('cargo' ,'>', '0')
			    	->whereBetween('created_at', [$fechaInicio, $fechaFin])
			    	->with('user','storages')
			    	->get();
    	*/
    	
    	Log::info('indexInvoicesDTCSV');
    	Log::info('$client: '. $client);
    	Log::info('$numberInvoices: '. $numberInvoices);
    	Log::info('$from: '. $from);
    	Log::info('$to: '. $to);
    	Log::info('$paragon: '. $paragon);
    	
    	if($client == "0" && $numberInvoices == "0"){
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->with('user','storages')
    		->get();
    	}
    	elseif($numberInvoices != "0"){
    	
    		Log::info('IF NUMBER INVOICES');
    		
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$numberInvoices);
    	
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}
    	
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->with('user','storages')
    		->get();
    	}
    	elseif($client != ""){
    	
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('name',  'LIKE', '%'.$client.'%')
    		->orWhere('lastName',  'LIKE', '%'.$client.'%')
    		->orWhere('companyName',  'LIKE', '%'.$client.'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    		->with('user','storages')
    		->get();
    	}
    	
    	if($from != "0"){
    		Log::info('IF FROM');
    		if($client == "0" && $numberInvoices == "0"){
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->with('user','storages')
    			->get();
    		}
    		elseif($numberInvoices != "0"){
    			 
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$number_invoices);
    			 
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    			 
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('user','storages')
    			->get();
    		}
    		elseif($client != "0"){
    			 
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('name',  'LIKE', '%'.$client.'%')
    			->orWhere('lastName',  'LIKE', '%'.$client.'%')
    			->orWhere('companyName',  'LIKE', '%'.$client.'%')
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    				DB::raw('billing.created_at as fechaDocumento'))
    			->with('user','storages')
    			->get();
    		}
    		
    		
    	}
    	
    	$headers = array(
    			"Content-type" => "text/csv",
    			"Content-Disposition" => "attachment; filename=Invoices_".Carbon::now()->format('Y-m-d_H-m-s').".csv",
    			"Pragma" => "no-cache",
    			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
    			"Expires" => "0"
    	);
    	 
    	$accountant_number = Lang::get('client.accountant_number');
    	
    	
    	$columns = array('"'.trans('reports.year_invoice').'"','"'.trans('reports.number_invoice').'"','"'.$accountant_number.'"','"'.Lang::get('cart.client').'"','"'.Lang::get('storage').'"', '"'.Lang::get('reports.date_document').'"',
    			'"'.Lang::get('reports.payment_date').'"', '"'.Lang::get('reports.charge').'"', '"'.Lang::get('reports.days_late').'"',
    			'"'.Lang::get('reports.paid').'"',
    			'"'.Lang::get('reports.subtotal').'"','"'.Lang::get('reports.vat').'"','"'.Lang::get('reports.total').'"',
    			'"'.Lang::get('client.pdf_label').'"');
    	
    	$content = "";
    	 
    	$content = implode(',', $columns)."\n";
    	 
    	foreach($billings as $billing){
    		$fullName 		= $billing->user->name . " ". $billing->user->lastName . " ". " - ". $billing->user->companyName;
    		$fechaDocumento = Carbon::parse($billing->fechaDocumento)->format('Y-m-d');
    		$fechaPago 		= Carbon::parse($billing->fechaDocumento)->addDay(7)->format('Y-m-d');
    		$cargo 			= Formato::formatear('ZLOTY', $billing->cargo);
    	
    		$inicio = Carbon::parse($billing->fechaDocumento)->addDay(7);
    		$fin = Carbon::now();
    	
    		$dif = $inicio->diffInDays($fin);
    	
    		$result = "";
    	
    		switch($dif){
    			case ($dif > 1 && $dif <= 30):
    				$result = "1-30";
    				break;
    	
    			case ($dif > 30 && $dif <= 60):
    				$result = "31-60";
    				break;
    					
    			case ($dif > 60 && $dif <= 90):
    				$result = "61-90";
    				break;
    					
    			case ($dif > 90 && $dif <= 180):
    				$result = "91-180";
    				break;
    	
    			case ($dif > 180 && $dif <= 365):
    				$result = "181-365";
    				break;
    	
    			default:
    				$result = ">365";
    				break;
    		}
    		
    		if($billing->flag_payed == 1){
    			$paid = Lang::get('calculator.yes');
    		}else{
    			$paid = Lang::get('calculator.no');
    		}
    		
    		$arrPdf = explode(".",$billing->pdf);
    		$pdf = $arrPdf[0];
    		
    		$subtotal = Formato::formatear('ZLOTY', $billing->bi_subtotal) ;
    		$vat 	= Formato::formatear('ZLOTY', $billing->bi_total_vat) ;
    		$total =	Formato::formatear('ZLOTY', $billing->bi_total) ;
    		 
    		$content.= '"'.$billing->bi_year_invoice.'",'.'"'.$billing->bi_number_invoice.'","'.$billing->user->accountant_number.'","'.$fullName.'","'.$billing->storages->alias.
    					'","'.$fechaDocumento.'","'.$fechaPago.'","'.$cargo.'","'.$result.'","'.$paid
    					.'","'.$subtotal.'","'.$vat.'","'.$total
    					.'","'.$pdf.'"'."\n";
    	}
    	
    	 
    	return \Illuminate\Support\Facades\Response::make($content, 200, $headers);
    }
    
    public function indexInvoicesDTZIP($fechaInicio,$fechaFin,$client = false,$numberInvoices = false,$from = false, $to = false, $paragon = false)
    {

    	/*
    	 $tmp = explode("/", $mesanio);
    	
    	 $mes 		= $tmp[0];
    	 $anio 		= $tmp[1]; */
    	
    	$billings = Billing::where('cargo' ,'>', '0')
					    	->whereBetween('created_at', [$fechaInicio, $fechaFin])
					    	->where('bi_paragon',$paragon)
					    	->with('user')->get();
    	
    	if($client == "0" && $numberInvoices == "0"){
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    		 
    		->with('user','storages')
    		->get();
    	}
    	elseif($numberInvoices != "0"){
    		 
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$numberInvoices);
    		 
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}
    		 
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->with('user','storages')
    		->get();
    	}
    	elseif($client != "0"){
    		 
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('name',  'LIKE', '%'.$client.'%')
    		->orWhere('lastName',  'LIKE', '%'.$client.'%')
    		->orWhere('companyName',  'LIKE', '%'.$client.'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    		->with('user','storages')
    		->get();
    	}
    	
    	
    	if($from != "0"){
    		Log::info('IF FROM');
    		if($client == "0" && $numberInvoices == "0"){
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    			->with('user','storages')
    			->get();
    		}
    		elseif($numberInvoices != "0"){
    	
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$numberInvoices);
    	
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    	
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('user','storages')
    			->get();
    		}
    		elseif($client != "0"){
    	
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('name',  'LIKE', '%'.$client.'%')
    			->orWhere('lastName',  'LIKE', '%'.$client.'%')
    			->orWhere('companyName',  'LIKE', '%'.$client.'%')
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'))
    			->with('user','storages')
    			->get();
    		}
    	
    	
    	}
    	 
    	
					    	
    	$archivos = [];
    	
    	foreach ($billings as $billing){
    		array_push($archivos, storage_path().'/app/invoices/'.$billing->pdf);
    	}
    	
    	$nombreZip = 'ZIP_Invoices_'.$fechaInicio.'-'.$fechaFin.'_'.rand(100000000,9999999).'.zip';
    	
    	$zipPath = storage_path().'/app/tmp/'.$nombreZip;
    	
    	FileSystem::crearZIPFiles($zipPath, $archivos);
    	
    	return response()->download($zipPath);
    }
    
    
    public function indexInvoicesDTZIPCopies($fechaInicio,$fechaFin,$client = false,$numberInvoices = false,$from = false, $to = false, $paragon = false)
    {
    
    	/*
    	 $tmp = explode("/", $mesanio);
    	  
    	 $mes 		= $tmp[0];
    	 $anio 		= $tmp[1]; */
    	 
    	$billings = Billing::where('cargo' ,'>', '0')
    	->whereBetween('created_at', [$fechaInicio, $fechaFin])
    	->where('bi_paragon',$paragon)
    	->with('user')->get();
    	 
    	if($client == "0" && $numberInvoices == "0"){
    		
    		Log::info("indexInvoicesDTZIPCopies@1");
    		
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('pdf','<>','NULL')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    		 
    		->with('user','storages')
    		->get();
    	}
    	elseif($numberInvoices != "0"){
    		Log::info("indexInvoicesDTZIPCopies@2");
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$numberInvoices);
    		 
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}
    		 
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('pdf','<>','NULL')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->with('user','storages')
    		->get();
    	}
    	elseif($client != "0"){
    		Log::info("indexInvoicesDTZIPCopies@3");
    		
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('name',  'LIKE', '%'.$client.'%')
    		->where('pdf','<>','NULL')
    		->orWhere('lastName',  'LIKE', '%'.$client.'%')
    		->orWhere('companyName',  'LIKE', '%'.$client.'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    		->with('user','storages')
    		->get();
    	}
    	 
    	 
    	if($from != "0"){
    		//Log::info('IF FROM');
    		if($client == "0" && $numberInvoices == "0"){
    			
    			Log::info("indexInvoicesDTZIPCopies@4");
    			
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->where('pdf','<>','NULL')
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    			->with('user','storages')
    			->get();
    		}
    		elseif($numberInvoices != "0"){
    			
    			Log::info("indexInvoicesDTZIPCopies@5");
    			 
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$numberInvoices);
    			 
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    			 
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->where('pdf','<>','NULL')
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('user','storages')
    			->get();
    		}
    		elseif($client != "0"){
    			
    			Log::info("indexInvoicesDTZIPCopies@6");
    			
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('name',  'LIKE', '%'.$client.'%')
    			->orWhere('lastName',  'LIKE', '%'.$client.'%')
    			->orWhere('companyName',  'LIKE', '%'.$client.'%')
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->where('pdf','<>','NULL')
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'),
    					DB::raw('billing.id as idBilling'))
    			->with('user','storages')
    			->get();
    		}
    	}
    	 
    
    	$archivos = [];
    	 
    	foreach ($billings as $billing){
    		
    		Log::info('BillingID: '.$billing->idBilling);
    		
    		$pdfName = $billing->pdf;
    		$arrPDFName = explode('.',$pdfName);
    		$pdfNameCopy = $arrPDFName[0].'_COPY'.'.'.$arrPDFName[1];
    		
    		array_push($archivos, storage_path().'/app/invoices/'.$pdfNameCopy);
    	}
    	 
    	$nombreZip = 'ZIP_Invoices_'.$fechaInicio.'-'.$fechaFin.'_'.rand(100000000,9999999).'.zip';
    	 
    	$zipPath = storage_path().'/app/tmp/'.$nombreZip;
    	 
    	FileSystem::crearZIPFiles($zipPath, $archivos);
    	 
    	return response()->download($zipPath);
    }
    
    public function indexInvoicesTXT($fechaInicio,$fechaFin,$client = false,$numberInvoices=false,$from=false,$to=false,$paragon = false)
    {
    	Log::info('ReportController@indexInvoicesTXT');
    	Log::info('Client: '.$client);
    	Log::info('numberInvoices: '.$numberInvoices);
    	Log::info('from: '.$from);
    	Log::info('to: '.$to);
    	
    	$nombreTxt = 'Txt_Invoices_'.$fechaInicio.'-'.$fechaFin.'_'.rand(100000000,9999999).'.txt';
    	
    	$path = storage_path().'/app/tmp/'.$nombreTxt;
    	
    	//HEADER
    	$content = "INFO{\n\tNazwa programu ='Sage Symfonia 2.0 Handel 2017.d' Symfonia 2.0 Handel 2017.d".		
						"\n\tWersja_programu =41".
						"\n\tWersja szablonu =2".		
						"\n\tKontrahent{".		
							"\n\t\tid =-773871487".
							"\n\t\tkod =Hertford".	
							"\n\t\tnazwa =PRZECHOWAMY WSZYSTKO Sp. z o.o.".	
							"\n\t\tmiejscowosc =Piaseczno".	
							"\n\t\tulica =Kineskopowa".	
							"\n\t\tdom =1".	
							"\n\t\tlokal =".	
							"\n\t\tkodpocz =05-500".
							"\n\t\trejon =".	
							"\n\t\tnip =5272703362".	
							"\n\t\ttel1 =".	
							"\n\t\ttel2 =".	
							"\n\t\tfax =".	
							"\n\t\temail =".	
							"\n\t\twww =".	
						"\n\t}".		
					"\n}";

    	$billing = Billing::whereBetween('created_at',[$fechaInicio,$fechaFin])
    						->where('pdf','<>','') //TODO ESTO VA????
    						->where('cargo','>','0')
    						->where('bi_paragon',$paragon)
    						///->whereIn('id',['395','396','397','398'])
    						->with('billingDetail','user','user.UserAddress')->get();
    	
    	
    	if($client == "0" && $numberInvoices == "0"){
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('pdf','<>','') //TODO ESTO VA????
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling')
    				)
    		 
    		->with('billingDetail','user','user.UserAddress')
    		->get();
    	}
    	elseif($numberInvoices != "0"){
    		 
    		
    		$arrayInvoices = array();
    		$arrInvoices = explode(',',$numberInvoices);
    		 
    		foreach($arrInvoices as $item){
    			array_push($arrayInvoices,$item);
    		}
    		 
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('pdf','<>','') //TODO ESTO VA????
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->whereIn('bi_number_invoice',$arrayInvoices)
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling'))
    		->with('billingDetail','user','user.UserAddress')
    		->get();
    		
    	}
    	elseif($client != "0"){
    		 
    		$billings = Billing::join('users','user_id','=','users.id')
    		->where('pdf','<>','') //TODO ESTO VA????
    		->where('cargo' ,'>', '0')
    		->where('bi_paragon',$paragon)
    		->where('billing.created_at','>=',$fechaInicio)
    		->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    		->where('name',  'LIKE', '%'.$client.'%')
    		->orWhere('lastName',  'LIKE', '%'.$client.'%')
    		->orWhere('companyName',  'LIKE', '%'.$client.'%')
    		->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling'))
    		->with('billingDetail','user','user.UserAddress')
    		->get();
    	}
    	 
    	 
    	if($from != "0"){
    		Log::info('IF FROM');
    		if($client == "0" && $numberInvoices == "0"){
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('pdf','<>','') //TODO ESTO VA????
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling'))
    			->with('billingDetail','user','user.UserAddress')
    			->get();
    		}
    		elseif($numberInvoices != "0"){
    			 
    			$arrayInvoices = array();
    			$arrInvoices = explode(',',$numberInvoices);
    			 
    			foreach($arrInvoices as $item){
    				array_push($arrayInvoices,$item);
    			}
    			 
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('pdf','<>','') //TODO ESTO VA????
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling'))
    			->whereIn('bi_number_invoice',$arrayInvoices)
    			->with('billingDetail','user','user.UserAddress')
    			->get();
    		}
    		elseif($client != "0"){
    			 
    			$billings = Billing::join('users','user_id','=','users.id')
    			->where('pdf','<>','') //TODO ESTO VA????
    			->where('cargo' ,'>', '0')
    			->where('bi_paragon',$paragon)
    			->where('billing.created_at','>=',$fechaInicio)
    			->where('billing.created_at','<=',Carbon::parse($fechaFin)->addDays(1))
    			->where('name',  'LIKE', '%'.$client.'%')
    			->orWhere('lastName',  'LIKE', '%'.$client.'%')
    			->orWhere('companyName',  'LIKE', '%'.$client.'%')
    			->where('bi_number_invoice','>=',$from)
    			->where('bi_number_invoice','<=',$to)
    			->select('*', DB::raw('CONCAT(name, " ", lastName, " " , companyName) as Full'), 
    					DB::raw('billing.created_at as DateBilling'),
    					DB::raw('billing.id as idBilling'))
    			->with('billingDetail','user','user.UserAddress')
    			->get();
    		}
    		 
    		 
    	}
    	
    	//dd($billings);
    	
    	$contadorAccountantCode = 90001;
    	
    	foreach($billings as $bill){
    		
    		if($bill->user['accountant_number'] != "")
    		{
    			//Log::info("GENERA : ".$bill->id);
    			
    			$accountant_number = $bill->user['accountant_number'];
    			$accountant_code = $bill->user['accountant_code'];
    			
    			if($accountant_code == ""){
    				$accountant_code = $contadorAccountantCode ;
    				$contadorAccountantCode = $contadorAccountantCode + 1;
    			}
    			
    			//Log::info('Userid: '. $bill->user['id']);
    			 
    			//OBTIENE DATOS
    			if($bill->user['userType_id'] == 1){
    				$kod = $bill->user['name'] . " ". $bill->user['lastName'];
    				//ES LO MISMO?
    				$nazwa = $bill->user['name'] . " ". $bill->user['lastName'];
    				
    				$fullName= $bill->user['name'] . " ". $bill->user['lastName'];
    			}else{
    				$kod = $bill->user['companyName'];
    				$nazwa = $bill->user['companyName'];
    				
    				$fullName = "";
				}
				
				Log::info('KOD: '.$kod);
				Log::info('NAZWA: '.$nazwa);
				
    			//$kod = utf8_decode($kod);
				//$nazwa = utf8_decode($nazwa);
				
				Log::info('KOD: '.$kod);
				Log::info('NAZWA: '.$nazwa);
    			
    			$city = $bill->user->UserAddress[0]->city;
    			$street = $bill->user->UserAddress[0]->street;
    			$number = $bill->user->UserAddress[0]->number;
    			$apartmentNumber = $bill->user->UserAddress[0]->apartmentNumber;
    			$postCode = $bill->user->UserAddress[0]->postCode;
    			
    			if($bill->user['userType_id'] == 1){
    				$nip	=  "P:".$bill->user['peselNumber'];
    			}else{
    				$nip	=  $bill->user['nipNumber'];
    			}
    			
    			$pesel = $bill->user['peselNumber'];
    			$regon = $bill->user['regonNumber'];
    			
    			$phone  = $bill->user['phone'];
    			$phone2 = $bill->user['phone2'];
    			
    			$email = $bill->user['email'];
    			
    			$name = $bill->user['name'];
    			$lastname = $bill->user['lastName'];
    			
    			if($bill->user['userType_id'] == 1){
    				$podatnikVAT = 0;
    			}
    			else{
    				$podatnikVAT = 1;
    			}
    			
    			
    			//DATOS DE LA CAJA
    			$desc_box = "";
    			$partida_caja = "";
    			$total_partida_box = "";
    			$subtotal_partida_box = "";
    			$vat_partida_box = "";
    			
    			//DATOS DEL SEGURO
    			$desc_seguro = "";
    			$partida_seguro = "";
    			$total_partida_seguro = "";
    			$subtotal_partida_seguro = "";
    			$vat_partida_seguro = "";
    			
    			//FLAG PARA IDENTIFICAR IS TIENE SEGURO
    			$flag_seguro = 0;
    			//FIN OBTENER DATOS
    			
    			$content .= "\nKontrahent{".
								"\n\tNotatka_Dl{".
									"\n\t\topis =".
								"\n\t}".
								"\n\tid =".$accountant_code.
								"\n\tguid =".
								"\n\tflag =0".
								"\n\tsubtyp =0".
								"\n\tznacznik =0".
								"\n\tinfo =N".
								"\n\tosoba =".
								"\n\tkod =".$kod.
								"\n\tnazwa =".$nazwa.
								"\n\tmiejscowosc =".$city.
								"\n\tulica =".$street.
								"\n\tdom =".$number.
								"\n\tlokal =".$apartmentNumber.
								"\n\tkodpocz =".$postCode.
								"\n\trejon =mazowieckie".
								"\n\tnip =".$nip.
								"\n\tstatusUE =0".
								"\n\tregon =".$regon.
								"\n\tpesel =".$pesel.
								"\n\tosfiz =0".
								"\n\ttel1 =".$phone.
								"\n\ttel2 =".$phone2.
								"\n\tfax =".
								"\n\temail =".$email.
								"\n\twww =".
								"\n\tnaglowek =".
								"\n\tnazwisko =".$name.
								"\n\timie =".$lastname.
								"\n\tbnazwa =".
								"\n\tbkonto =".
								"\n\tnegoc =T".
								"\n\tgrupacen =255".
								"\n\ttypks =Krajowy".
								"\n\ttyp_naliczania =".
								"\n\ttyp_ceny =".
								"\n\tupust =0.00".
								"\n\tlimit =0".
								"\n\tlimitkwota =0.00".
								"\n\tlimitwaluta =".
								"\n\trejestr_platnosci =BANK".
								"\n\tforma_platnosci =przelew 7 dni".
								"\n\tstanpl =0.00".
								"\n\tstannl =0.00".
								"\n\tkhfk =".$accountant_number.
								"\n\tzapas =".
								"\n\tkrajKod =PL".
								"\n\tkrajNazwa =Polska".
								"\n\taktywny =1".
								"\n\tNazwaRodzaju =Kontrahenciâ€ ".
								"\n\tNazwaKatalogu =\@Kontrahenci".
								"\n\tpoledod1 =".
								"\n\tpoledod2 =".
								"\n\tpoledod3 =".
								"\n\tpoledod4 =".
								"\n\tpoledod5 =".
								"\n\tpoledod6 =".
								"\n\tpoledod7 =".
								"\n\tpoledod8 =".
								"\n\tpodatnikVAT =".$podatnikVAT.
							"\n}";   

    			//SOLO PARA COMPANYS
    			if($bill->user['userType_id'] != 1){
    				$porcentajeVAT = $bill->bi_porcentaje_vat;
    				 
    				$content .= "\nStawkaVAT{".	
									"\n\taktywny =1".
									"\n\twartosc =0.".$porcentajeVAT."00".
									"\n\tnazwa =".$porcentajeVAT."%".
									"\n\topis =Stawka ".$porcentajeVAT."%".
									"\n\tstawka =".$porcentajeVAT.".00".
									"\n\tdataod =2011-01-01".
									"\n\tdatado =".
									"\n\tvatrr =0".
									"\n\tlong =97700".
									"\n\tsubtyp =8".
								"\n}";
    			}
    			
    			Log::info('Before foreach');
    			Log::info('Billing id : '. $bill->id);
    			
    			$billingDetail = BillingDetail::where(['billing_id' => $bill->idBilling])->get();
    			
    			foreach($billingDetail as $item){
    			//TOWAR PARA BOX
    			
    				Log::info('IN foreach');
    				
    				if($item->bd_tipo_partida == "BOX"){
    					$desc_box = $item->bd_codigo;
    					$partida_caja = $item->bd_nombre;
    					
    					$total_partida_box = $item->bd_total;
    					$subtotal_partida_box = $item->bd_valor_neto;
    					
    					Log::info('Billing id : '. $item->billing_id);
    					Log::info('Valor neto: '. $item->bd_valor_neto);
    					
    					$vat_partida_box = $item->bd_total_vat;
    					
    					$content .= "\nTowar{".		
									"\n\tNotatka_Dl{".
										"\n\t\topis =".
									"\n}".
									"\n\tid =65884".	
									"\n\tflag =0".
									"\n\tsubtyp =0".	
									"\n\tznacznik =0".	
									"\n\tinfo =N".	
									"\n\tosoba =".
									"\n\tkod =".$item->bd_codigo.
									"\n\tnazwa =".$item->bd_nombre.
									"\n\tkodpaskowy =".	
									"\n\tnegoc =N".	
									"\n\tnegocjacjacen =0".	
									"\n\tpcn =".
									"\n\tprzelkg =0.0000".
									"\n\tprzeluz =0.0000".
									"\n\tprzelit =0".
									"\n\tjmuzup =".
									"\n\tstawkaVAT =".$item->bd_porcentaje_vat.".00".
									"\n\tsww =".	
									"\n\tcbazowa =".$item->bd_valor_neto.
									"\n\tcbazowawal =".
									"\n\tzaokrag =0".
									"\n\tmarzaa =0.00".
									"\n\tcenaa =".$item->bd_valor_neto.
									"\n\tcenaawal =".
									"\n\tnettoa =1".
									"\n\tmarzab =0.00".	
									"\n\tcenab =".$item->bd_valor_neto.	
									"\n\tcenabwal =".	
									"\n\tnettob =1".	
									"\n\tmarzac =0.00".	
									"\n\tcenac =".$item->bd_valor_neto.	
									"\n\tcenacwal =".	
									"\n\tnettoc =1".	
									"\n\tmarzad =0.00".	
									"\n\tcenad =".$item->bd_total.	
									"\n\tcenadwal =".	
									"\n\tnettod =0".	
									"\n\tjm =szt".	
									"\n\tszablon =".	
									"\n\tjmdod1 =".	
									"\n\tprzeljmdod1 =1.0000".	
									"\n\tjmdod2 =".	
									"\n\tprzeljmdod2 =0.0000".	
									"\n\tkontofk =".	
									"\n\ttypks =Box".	
									"\n\tjmdef =255".	
									"\n\taktywny =1".	
									"\n\tNazwaRodzaju =Towaryâ€ ".	
									"\n\tNazwaKatalogu =\@Towary".	
									"\n\tpoledod1 =".	
									"\n\tpoledod2 =".	
									"\n\tpoledod3 =".	
									"\n\tpoledod4 =".	
									"\n\tpoledod5 =".	
									"\n\tpoledod6 =".	
									"\n\tpoledod7 =".	
									"\n\tpoledod8 =".	
									"\n\tflagcen =15".	
									"\n\todwrotneObc =0".	
									"\n\tvat50 =0".	
								"\n}";
    				}
    				
    				if($item->bd_tipo_partida == "INSURANCE"){
    					//ACTIVA FLAG DE SEGURO
    					$flag_seguro = 1;
    					
    					$desc_seguro = $item->bd_codigo;
    					$partida_seguro = $item->bd_nombre;
    					$total_partida_seguro = $item->bd_total;
    					$subtotal_partida_seguro = $item->bd_valor_neto;
    					$vat_partida_seguro = $item->bd_total_vat;
    				}
    				
    				// SEA INSURANCE Y TIPO DE USUARIO DIFERENTE DE PERSONA
    				if($item->bd_tipo_partida == "INSURANCE" && $bill->user['userType_id'] != 1){
    					$content .= "\nTowar{".		
										"\n\tNotatka_Dl{".	
											"\n\t\topis =".
										"\n\t}".	
										"\n\tid =65539".	
										"\n\tflag =0".	
										"\n\tsubtyp =1".	
										"\n\tznacznik =0".	
										"\n\tinfo =N".	
										"\n\tosoba =".	
										"\n\tkod =".$item->bd_codigo.	
										"\n\tnazwa =".$item->bd_nombre.	
										"\n\tkodpaskowy =".	
										"\n\tnegoc =N".	
										"\n\tnegocjacjacen =0".	
										"\n\tpcn =".	
										"\n\tprzelkg =0.0000".	
										"\n\tprzeluz =0.0000".	
										"\n\tprzelit =0".	
										"\n\tjmuzup =".	
										"\n\tstawkaVAT =".$item->bd_porcentaje_vat.".00".	
										"\n\tsww =".	
										"\n\tcbazowa =".$item->bd_valor_neto.	
										"\n\tcbazowawal =".	
										"\n\tzaokrag =0".	
										"\n\tmarzaa =0.00".	
										"\n\tcenaa =".$item->bd_valor_neto.	
										"\n\tcenaawal =".	
										"\n\tnettoa =1".	
										"\n\tmarzab =0.00".	
										"\n\tcenab =".$item->bd_valor_neto.	
										"\n\tcenabwal =".	
										"\n\tnettob =1".	
										"\n\tmarzac =0.00".	
										"\n\tcenac =".$item->bd_valor_neto.	
										"\n\tcenacwal =".	
										"\n\tnettoc =1".	
										"\n\tmarzad =0.00".	
										"\n\tcenad =".$item->bd_total.	
										"\n\tcenadwal =".	
										"\n\tnettod =0".	
										"\n\tjm =szt".	
										"\n\tszablon =".	
										"\n\tjmdod1 =".	
										"\n\tprzeljmdod1 =1.0000".	
										"\n\tjmdod2 =".	
										"\n\tprzeljmdod2 =0.0000".	
										"\n\tkontofk =".	
										"\n\ttypks =Ubezp".	
										"\n\tjmdef =0".	
										"\n\taktywny =1".	
										"\n\tNazwaRodzaju =Towaryâ€ ".	
										"\n\tNazwaKatalogu =\@Towary".	
										"\n\tpoledod1 =".	
										"\n\tpoledod2 =".	
										"\n\tpoledod3 =".	
										"\n\tpoledod4 =".	
										"\n\tpoledod5 =".	
										"\n\tpoledod6 =".	
										"\n\tpoledod7 =".	
										"\n\tpoledod8 =".	
										"\n\tflagcen =15".	
										"\n\todwrotneObc =0".	
										"\n\tvat50 =0".	
									"\n}";
    				}
    			}//FIN FOREACH
    				
    				//DOCUMENT
    				
    				$pdf = explode(".",$bill->pdf);
    				$pdfFactura = explode("_",$pdf[0]);
    				
    				$factura = $pdfFactura[0];
    				
    				
    				$arrFactura = explode("-",$factura);
    				$serie = $arrFactura[1];
    				$num_factura = $arrFactura[2];
    				$anio = $arrFactura[0];
    				$fecha_factura = Carbon::parse($bill->DateBilling)->format('Y-m-d');
    				$fecha_pago = Carbon::parse($bill->DateBilling)->addDay(7)->format('Y-m-d');
    				
    				$primer_dia_mes_factura = Carbon::parse($bill->DateBilling)->startOfMonth()->format('Y-m-d');
    				
    				$content .= "\nDokument{".
										"\n\tanulowany =0".				
										"\n\trodzaj_dok =sprzedaÃ¸y".				
										"\n\tmetoda_VAT =0".				
										"\n\tnaliczenie_VAT =0".				
										"\n\tdozaplaty =".$bill->bi_total.
										"\n\twdozaplaty =".$bill->bi_total.
										"\n\tRozrachunek{".				
											"\n\t\tkwota =".$bill->bi_total.			
											"\n\t\twkwota =".$bill->bi_total.			
											"\n\t\twaluta =".			
											"\n\t\tkurs =1.000000000000".			
										"\n\t}".		
												
										"\n\tNotatka_Dl{".				
											"\n\t\topis =".			
										"\n\t}".
														
										"\n\tkodKH =".$kod.				
										"\n\tkodOdKH =".$kod.				
										"\n\tid =".				
										"\n\tflag =0".				
										"\n\tsubtypi =40".				
										"\n\ttypi =0".				
										"\n\tznaczniki =0".				
										"\n\trodzaj =3300".				
										"\n\tkatalog =65917".				
										"\n\tinfo =N".				
										"\n\tosoba =Admin".				
										"\n\tkod =".$factura.			
										"\n\tseria =s".$serie.				
										"\n\tserianr =".$num_factura.
										"\n\tokres =3".$anio.				
										"\n\tnazwa =Faktura".				
										"\n\tdata =". $fecha_factura .		
										"\n\tdatasp =". $fecha_factura .
										"\n\topis =Paragon nr: W003614".				
										"\n\tkhid =".$accountant_code.				
										"\n\tkhkod =".$kod.				
										"\n\tkhnazwa =".$kod.				
										"\n\tkhadres =".$street.				
										"\n\tkhdom =".$number.
										"\n\tkhlokal =".$apartmentNumber.			
										"\n\tkhmiasto =".$city.				
										"\n\tkhkodpocz =".$postCode.
										"\n\tkhnip =".$nip.	
										"\n\tidkraju =5801".				
										"\n\todid =".$accountant_code.				
										"\n\todkod =".$kod.				
										"\n\todnazwa =".$kod.
										"\n\todadres =".$street.				
										"\n\toddom =".$number.				
										"\n\todlokal =".$apartmentNumber.			
										"\n\todmiasto =".$city.				
										"\n\todkodpocz =".$postCode.			
										"\n\tok =0".				
										"\n\tpuste =0".				
										"\n\twplaty =0.000000000000".				
										"\n\trabat =0.000000000000".				
										"\n\tplattypi =131".				
										"\n\tplattermin =".$fecha_pago.		
										"\n\tnetto =".$bill->bi_subtotal.				
										"\n\tvat =".$bill->bi_total_vat.
										"\n\todebrane =".$fullName.				
										"\n\ttyp_dk =".$serie.				
										"\n\tgrupaceni =3".				
										"\n\trejestrvati =1".				
										"\n\trejestrvat2 =0".				
										"\n\tiddokkoryg =0".				
										"\n\twartoscsp =".$bill->bi_subtotal.				
										"\n\tidkorekt =0".				
										"\n\texp_fki =0".				
										"\n\tstatusfk =0".				
										"\n\twaluta =".				
										"\n\tkurs =1.000000000000".				
										"\n\tzyskdod =0.000000000000".				
										"\n\tparagon =0".				
										"\n\tkod_obcy =".				
										"\n\tmagazyn =2".				
										"\n\trozlmgi =0".				
										"\n\tdzial =0".				
										"\n\tformaplatn =65536".				
										"\n\tschemat =SP".
										"\n\tdatarej =".$fecha_factura.		
										"\n\tdatawp =".				
										"\n\twalnetto =".$bill->bi_subtotal.				
										"\n\twalbrutto =".$bill->bi_total.				
										"\n\twartprzychod =".$bill->bi_subtotal.				
										"\n\tkursdoch =1.000000000000".				
										"\n\tguid =".				
										"\n\te_status =0".				
										"\n\toki =0".				
										"\n\tod_brutto =0".				
										"\n\tdomyÃºlny rabat =0.00".				
										"\n\trejestr_vat =rSPV".				
										"\n\tnazwa_rejestru_vat =SprzedaÃ¸ VAT".				
										"\n\ttyp_rejestru_vat =1".				
										"\n\trejestr_platnosci =BANK".				
										"\n\tforma_platnosci =przelew 7 dni".				
										"\n\tkhKrajKod =PL".
										"\n\tkhKrajNazwa =Polska".				
										"\n\tDane nabywcy{".				
											"\n\t\tkhid =".$accountant_code.			
											"\n\t\tfk_ident =".$accountant_number.			
											"\n\t\tkhkod =".$kod.			
											"\n\t\tkhnazwa =".$kod.			
											"\n\t\tkhadres =".$street.			
											"\n\t\tkhdom =".$number.			
											"\n\t\tkhlokal =".$apartmentNumber.			
											"\n\t\tkhmiasto =".$city.
											"\n\t\tkhkodpocz =".$postCode.		
											"\n\t\tkhnip =".$nip.
											"\n\t\tkhKrajKod =PL".			
											"\n\t\tkhKrajNazwa =Polska".			
										"\n\t}".				
										"\n\tDane odbiorcy{".				
											"\n\t\todid =".$accountant_code.			
											"\n\t\todkod =".$kod.			
											"\n\t\todnazwa =".$kod.			
											"\n\t\todadres =".$street.			
											"\n\t\toddom =".$number.			
											"\n\t\todlokal =".$apartmentNumber.
											"\n\t\todmiasto =".$city.			
											"\n\t\todkodpocz =".$postCode.		
										"\n\t}".				
										"\n\tkwota =".$bill->bi_total.				
										"\n\tsymbol FK =".$serie.				
										"\n\tobsluguj jak =".$serie.				
										"\n\tNazwaKor =".$serie.				
										"\n\tDataKor =".				
										"\n\tNazwaKatalogu =\@Dokumenty\@2017\@05 maj".				
										"\n\tNazwaRodzaju =Dokumentyâ€ ".				
										"\n\tPozycja dokumentu{".				
											"\n\t\tkodTW =".$desc_box.			
											"\n\t\tkodKH =".$kod.			
											"\n\t\tid =".			
											"\n\t\tflag =0".			
											"\n\t\tsubtypi =0".			
											"\n\t\ttypi =0".			
											"\n\t\tsuper =".			
											"\n\t\tlp =1".			
											"\n\t\tidkh =".$accountant_code.
											"\n\t\tidtw =65887".			
											"\n\t\tkod =".$desc_box.			
											"\n\t\tdata =".$fecha_factura.	
											"\n\t\topis =".$partida_caja.			
											"\n\t\topisdod =".			
											"\n\t\tilosc =1.000000000000".			
											"\n\t\tjm =szt".			
											"\n\t\tstvati =8".			
											"\n\t\tgrupaceni =3".			
											"\n\t\tcenabaza =".$subtotal_partida_box.			
											"\n\t\twaluta =".			
											"\n\t\tcenabazapl =".$total_partida_box.			
											"\n\t\tcena =".$total_partida_box.			
											"\n\t\twartnetto =".$subtotal_partida_box.			
											"\n\t\twartvat =".$vat_partida_box.			
											"\n\t\twarttowaru =".$subtotal_partida_box.			
											"\n\t\tidpozkoryg =0".			
											"\n\t\tcenabaztow =".$subtotal_partida_box.			
											"\n\t\twalcenybaz =".			
											"\n\t\trejestrvati =1".			
											"\n\t\trejestrvat2 =0".			
											"\n\t\tidlongname =0".			
											"\n\t\twartstvat =".$bill->bi_porcentaje_vat."00".			
											"\n\t\tzyskdod =".$subtotal_partida_box.			
											"\n\t\tiloscjedn =0.000000000000".			
											"\n\t\tosoba =Admin".			
											"\n\t\tiloscwp =1.000000000000".			
											"\n\t\tidhandl =0".			
											"\n\t\trezerwacja =0".			
											"\n\t\tiloscdorozl =0.000000000000".			
											"\n\t\trozlhandl =0.000000000000".			
											"\n\t\tmagazyn =2".			
											"\n\t\tcenawal =".$total_partida_box.			
											"\n\t\tschemat =".			
											"\n\t\tdatarej =".$fecha_factura.	
											"\n\t\tjmwp =szt".			
											"\n\t\tkosztmarzy =0.000000000000".			
											"\n\t\twalnetto =".$subtotal_partida_box.			
											"\n\t\twalbrutto =".$total_partida_box.			
											"\n\t\twartprzychod =".$subtotal_partida_box.			
											"\n\t\todwrotneobc =9".			
											"\n\t\tvat50 =9".			
											"\n\t\tstawkaVAT =".$bill->bi_porcentaje_vat.".00".			
											"\n\t\trejestr_vat =rSPV".			
											"\n\t\tnazwa_rejestru_vat =SprzedaÃ¸ VAT".			
											"\n\t\ttyp_rejestru_vat =1".			
											"\n\t\tSWW/KWiU =".			
											"\n\t\ttyp_pozycji =0".			
										"\n\t}";
    				if($flag_seguro == 1){				
							$content .=	"\n\tPozycja dokumentu{";

							if($bill->user['userType_id'] == 1){
								//PERSONA
								$content.= "\n\t\tNazwa_Dl{".	
												"\n\t\t\topis =ubezpieczenie pomieszczenia magazynowego". 
											"\n\t\t}";
								$cenawal = $total_partida_seguro;
							}
							else{
								$cenawal = $subtotal_partida_seguro;
							}
    				
							
							
							
							$content.=	"\n\t\tkodTW =ubezpieczenie".			
											"\n\t\tkodKH =".$kod.			
											"\n\t\tid =".			
											"\n\t\tflag =0".			
											"\n\t\tsubtypi =0".			
											"\n\t\ttypi =0".			
											"\n\t\tsuper =".			
											"\n\t\tlp =2".			
											"\n\t\tidkh =".$accountant_code.
											"\n\t\tidtw =".			
											"\n\t\tkod =ubezpieczenie".			
											"\n\t\tdata =".$fecha_factura.	
											"\n\t\topis =ubezpieczenie pomieszczenia magazynowego".			
											"\n\t\topisdod =".			
											"\n\t\tilosc =1.000000000000".			
											"\n\t\tjm =szt".			
											"\n\t\tstvati =8".			
											"\n\t\tgrupaceni =3".			
											"\n\t\tcenabaza =".$subtotal_partida_seguro.			
											"\n\t\twaluta =".			
											"\n\t\tcenabazapl =".$total_partida_seguro.			
											"\n\t\tcena =".$total_partida_seguro.			
											"\n\t\twartnetto =".$subtotal_partida_seguro.			
											"\n\t\twartvat =".$vat_partida_seguro.			
											"\n\t\twarttowaru =".$subtotal_partida_seguro.			
											"\n\t\tidpozkoryg =0".			
											"\n\t\tcenabaztow =".$subtotal_partida_seguro.			
											"\n\t\twalcenybaz =".			
											"\n\t\trejestrvati =1".			
											"\n\t\trejestrvat2 =0".			
											"\n\t\tidlongname =0".			
											"\n\t\twartstvat =".$bill->bi_porcentaje_vat."00".			
											"\n\t\tzyskdod =".$subtotal_partida_seguro.			
											"\n\t\tiloscjedn =0.000000000000".			
											"\n\t\tosoba =Admin".			
											"\n\t\tiloscwp =1.000000000000".			
											"\n\t\tidhandl =0".			
											"\n\t\trezerwacja =0".			
											"\n\t\tiloscdorozl =0.000000000000".			
											"\n\t\trozlhandl =0.000000000000".			
											"\n\t\tmagazyn =2".			
											"\n\t\tcenawal =".$cenawal.			
											"\n\t\tschemat =".			
											"\n\t\tdatarej =".$fecha_factura.	
											"\n\t\tjmwp =szt".			
											"\n\t\tkosztmarzy =0.000000000000".			
											"\n\t\twalnetto =".$subtotal_partida_seguro.			
											"\n\t\twalbrutto =".$total_partida_seguro.			
											"\n\t\twartprzychod =".$subtotal_partida_seguro.
											"\n\t\todwrotneobc =0".			
											"\n\t\tvat50 =0".			
											"\n\t\tstawkaVAT =".$bill->bi_porcentaje_vat.".00".			
											"\n\t\trejestr_vat =rSPV".			
											"\n\t\tnazwa_rejestru_vat =SprzedaÃ¸ VAT".			
											"\n\t\ttyp_rejestru_vat =1".			
											"\n\t\tSWW/KWiU =".			
											"\n\t\ttyp_pozycji =0".
										"\n\t}";
    				}
    				
    				$storage = Storages::find($bill->storage_id);
    				
    				if($storage->st_es_contenedor == 1){
    					$codeAccount = "700-103020";
    				}else{
    					$codeAccount = "700-102010";
    				}

						$content .=	"\n\tFK nazwa =".$factura.			
										"\n\topis FK =".$kod." ".$factura." Wynajem pomieszczenia magazynowego".			
										"\n\tZapis{".				
											"\n\t\tstrona =WN".			
											"\n\t\tkwota =".$bill->bi_total.			
											"\n\t\tkonto =201-".$accountant_number.		
											"\n\t\tIdDlaRozliczen =1".			
											"\n\t\topis =".$kod."	".$factura."  Wynajem pomieszczenia magazynowego".		
											"\n\t\tNumerDok =".$factura.		
											"\n\t\tPozycja =0".			
											"\n\t\tZapisRownolegly =0".			
										"\n\t}".				
										"\n\tZapis{".				
											"\n\t\tstrona =MA".			
											"\n\t\tkwota =".$subtotal_partida_box.			
											"\n\t\tkonto =".$codeAccount.		
											"\n\t\tIdDlaRozliczen =2".			
											"\n\t\topis =".$kod." ".$factura."  Wynajem pomieszczenia magazynowego".		
											"\n\t\tNumerDok =".$factura.		
											"\n\t\tPozycja =0".			
											"\n\t\tZapisRownolegly =0".			
										"\n\t}".				
										"\n\tZapis{".				
											"\n\t\tstrona =MA".			
											"\n\t\tkwota =".$bill->bi_total_vat.			
											"\n\t\tkonto =221-102300".		
											"\n\t\tIdDlaRozliczen =3".			
											"\n\t\topis =".$kod." ".$factura."  Wynajem pomieszczenia magazynowego".		
											"\n\t\tNumerDok =".$factura.		
											"\n\t\tPozycja =0".			
											"\n\t\tZapisRownolegly =0".			
										"\n\t}".				
										"\n\tZapis{".				
											"\n\t\tstrona =MA".			
											"\n\t\tkwota =".$subtotal_partida_seguro.			
											"\n\t\tkonto =700-102020".		
											"\n\t\tIdDlaRozliczen =4".			
											"\n\t\topis =".$kod." ".$factura."  Wynajem pomieszczenia magazynowego".		
											"\n\t\tNumerDok =".$factura.		
											"\n\t\tPozycja =0".			
											"\n\t\tZapisRownolegly =0".			
										"\n\t}".				
										"\n\tRejestr{".				
											"\n\t\tSkrot =rSPV".			
											"\n\t\tNazwa =SprzedaÃ¸ VAT".			
											"\n\t\tRodzaj =1".			
											"\n\t\tABC =1".			
											"\n\t\tmetoda_VAT =0".			
											"\n\t\tdatarej =".$fecha_factura.	
											"\n\t\tokres =".$primer_dia_mes_factura.	
											"\n\t\tstawka =".$bill->bi_porcentaje_vat.".00".			
											"\n\t\tbrutto =".$bill->bi_total.			
											"\n\t\tnetto =".$bill->bi_subtotal.			
											"\n\t\tvat =".$bill->bi_total_vat.			
											"\n\t\tPozycja VAT{".			
												"\n\t\t\tStawka =".$bill->bi_porcentaje_vat.".00".		
												"\n\t\t\tStawka_pl =".$bill->bi_porcentaje_vat.".00".		
												"\n\t\t\tOpis =".$desc_box.		
												"\n\t\t\tWartosc =".$subtotal_partida_box.		
												"\n\t\t\trejestrVat =rSPV".		
												"\n\t\t\tUsluga =0".		
												"\n\t\t\tUE =0".		
												"\n\t\t\todwrotneObc =0".		
											"\n\t\t}".			
											"\n\t\tPozycja VAT{".			
												"\n\t\t\tStawka =".$bill->bi_porcentaje_vat.".00".		
												"\n\t\t\tStawka_pl =".$bill->bi_porcentaje_vat.".00".		
												"\n\t\t\tOpis =".$desc_seguro.		
												"\n\t\t\tWartosc =".$subtotal_partida_seguro.		
												"\n\t\t\trejestrVat =rSPV".		
												"\n\t\t\tUsluga =0".		
												"\n\t\t\tUE =0".		
												"\n\t\t\todwrotneObc =0".		
											"\n\t\t}".			
										"\n\t}".				
										"\n\tTransakcja{".				
											"\n\t\tIdDlaRozliczen =-1".		
											"\n\t\ttermin =".$fecha_pago.	
										"\n\t}".				
									"\n}";    				
    		}
    		//$content .= "\n FIN DE FACTURA********************";
    		
    	}
    	
    	
    	FileSystem::crearArchivo($path, $content);
    	
    	Log::info('END --- ReportController@indexInvoicesTXT');
    	
    	return response()->download($path);
    }
    
    public function indexCorrectedInvoices()
    {
    	$start = Carbon::now()->startOfMonth();
    	$end = Carbon::now()->endOfMonth();
    	$years = Billing::select('bi_year_invoice_corrected')
    				->where('bi_year_invoice_corrected','<>','null')
    				->groupBY('bi_year_invoice_corrected')->lists('bi_year_invoice_corrected'); 
    	
    	return view('pw.reports.corrected_invoices.corrected_invoices_index',
    			[
    					'start'	=> $start,
    					'end'	=> $end,
    					'years'	=> $years
    			]);	
    }
    
    public function indexCorrectedInvoicesDT(Request $request)
    {
    	Log::info('fromInvoice:'. $request->get('fromInvoice'));
    	Log::info('toInvoice:'. $request->get('toInvoice'));
    	Log::info('yearInvoice: '. $request->get('yearInvoice'));
    	
    	 $billing = Billing::with('user','storages')
    	 			->where('bi_number_invoice_corrected' , '<>', 'NULL')
    	 			->where('bi_year_invoice_corrected' , $request->get('yearInvoice'))
    	 			->get();
    	 
    	 if($request->get('fromInvoice') != "" && $request->get('toInvoice') != ""){
    	 	$billing = Billing::with('user','storages')
			    	 	->where('bi_number_invoice_corrected' , '<>', 'NULL')
			    	 	->where('bi_year_invoice_corrected' , $request->get('yearInvoice'))
			    	 	->where('bi_number_invoice_corrected','>=',$request->get('fromInvoice'))
			    	 	->where('bi_number_invoice_corrected','<=',$request->get('toInvoice'))
						 ->get();
			
			Log::info("IF");

    	 }elseif($request->get('fromInvoice') != "" && $request->get('toInvoice') == ""){
    	 	$billing = Billing::with('user','storages')
			    	 	->where('bi_number_invoice_corrected' , '<>', 'NULL')
			    	 	->where('bi_year_invoice_corrected' , $request->get('yearInvoice'))
			    	 	->where('bi_number_invoice_corrected','>=',$request->get('fromInvoice'))
						->get();
			Log::info("1stELSEIF");

    	 }elseif($request->get('fromInvoice') == "" && $request->get('toInvoice') != ""){
    	 	$billing = Billing::with('user','storages')
			    	 	->where('bi_number_invoice_corrected' , '<>', 'NULL')
			    	 	->where('bi_year_invoice_corrected' , $request->get('yearInvoice'))
			    	 	->where('bi_number_invoice_corrected','<=',$request->get('toInvoice'))
						 ->get();
			Log::info("2ndELSEIF");
		 }
		 
		 Log::info("********************");
		 Log::info("Billings: ");
		 Log::info($billing);
		 Log::info("********************");
    	 
    	 
    	 return Datatables::of($billing)
    	 			->addColumn('fullname',function($row){
    	 				return $row->user->name . " ". $row->user->lastName . " - " . $row->user->companyName;
    	 			})
    	 			->editColumn('cargo',function($row){
    	 				$bill = Billing::where('id_billing_correccion',$row->id)->first();
    	 				return Formato::formatear('ZLOTY', $row->cargo);
    	 			})
    	 			->editColumn('abono_corregido', function($row){
    	 				return Formato::formatear('ZLOTY', $row->abono);
    	 			})
    	 			->editColumn('correction_date',function($row){
    	 				return Carbon::parse($row->created_at)->format('Y-m-d');
    	 			})
    	 			->editColumn('number_invoice_corrected',function($row){
    	 				return $row->bi_number_invoice_corrected;
    	 			})
    	 			->editColumn('year_invoice_corrected',function($row){
    	 				return $row->bi_year_invoice_corrected;
    	 			})
    	 			->editColumn('razon_correccion',function($row){
    	 				return $row->bi_razon_correccion;
    	 			})
    	 			->addColumn('pdf',function($row){
    	 				return '<a href='.route('billing.download_pdf_corregido',[$row->id]).' class="btn btn-primary btn-pdf btn-sm">
    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$row->pdf_corregido.'
    							</a>';
    	 			})
    	 			->make(true);
    }
    
    public function indexParagonCancelledReport()
    {
    	return view('pw.reports.paragon_cancelled.paragon_cancelled');
    }
    
    public function indexParagonCancelledReportDT()
    {
    	 $billings = Billing::where([
    	 					'bi_paragon'	=> 1
    	 				])
    	 			->where('id_billing_correccion','<>','NULL')
    	 			->with('user','storages')
					->get();
    	 
    	 return Datatables::of($billings)
			    	 ->addColumn('fullname',function($row){
			    	 	return $row->user->name . " ". $row->user->lastName . " - " . $row->user->companyName;
			    	 })
			    	 ->editColumn('cargo',function($row){
			    	 	$bill = Billing::find($row->id);
			    	 	
			    	 	return Formato::formatear('ZLOTY', $bill->cargo);
			    	 })
			    	 ->editColumn('abono_corregido', function($row){
			    	 	return Formato::formatear('ZLOTY', $row->abono);
			    	 })
			    	 ->editColumn('correction_date',function($row){
			    	 	return Carbon::parse($row->created_at)->format('Y-m-d');
			    	 })
			    	 ->addColumn('pdf',function($row){
			    	 	return '<a href='.route('billing.download_pdf_corregido',[$row->id]).' class="btn btn-primary btn-pdf btn-sm">
    								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$row->pdf_corregido.'
    							</a>';
			    	 })
    	 			->make(true);
    }
    //metodo para Insurance Report  http://pw.des:8080/insuranceDetails
    public function indexInsuranceReportDetailsCSV($fechaInicio,$fechaFin)
    {
           $billings = BillingDetail::join('billing','billing_id','=','billing.id')
                ->join('users','billing.user_id','=','users.id')
                ->join('storages','billing.storage_id','=','storages.id')
                ->where([
                    'bd_tipo_partida'   => 'INSURANCE'
                ])
                ->whereBetween('pay_month',[$fechaInicio,$fechaFin])
                ->select('*',
                        DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname'),
                        DB::raw('billing_detail.id as idBD'),
                        DB::raw('billing.user_id as idUser'),
                        DB::raw('billing.payment_data_id as idPaymentData')
                )->groupBy('billing.user_id')
                ->groupBy('billing.storage_id')
                ->get();
                //echo json_encode($billings);

        \Excel::create('Insurance Report-'.$fechaInicio."-".$fechaFin, function($excel) use($billings,$fechaInicio,$fechaFin) {
        
            $excel->sheet('History', function($sheet) use ($billings,$fechaInicio,$fechaFin) {
            //Encabezados del excel
            $sheet->row(1, [
                        '#','Client', 'Storage',   'Annual Cost',   'Daily Cost', 'Date Start' , 'Date End' , 'Days Period', 'Total'
                ]);

                foreach($billings as $index => $bill) {
                    //fechaInicio
                    $dateStart="";
                    if($bill->rent_start< $fechaInicio){
                        $dateStart = $fechaInicio;
                    }else{
                        $dateStart = $bill->rent_start; 
                    }

                    $dateStartColumn=Carbon::parse($dateStart)->format('Y-m-d');

                    //fecha fin VALIDA SI ESTA ACTIVA CON EL CLIENTE
                    $storage = Storages::where([
                                        'flag_rent' =>  '1',
                                        'user_id'   => $bill->user_id,
                                        'id'        => $bill->storage_id
                                    ])->first();

                      if(!is_null($storage)){
                            $dateEnd=Carbon::parse($fechaFin)->format('Y-m-d');
                        }else{
                            
                            $history = History::where([
                                            'user_id'   => $bill->idUser ,
                                            'storage_id'=> $bill->storage_id
                            ])->first();
                            
                            if(!is_null($history)){
                                //226   Robert Walicki - R&B Robert Walicki 417 192 0.53    2017-09-01  -0001-11-30 31  16  390 340
                                //if($history->rent_end=="0000-00-00")
                                //{
                                //    $dateEnd="sin fecha";
                                //}
                                //else{
                                $dateEnd = Carbon::parse($history->rent_end)->format('Y-m-d');
                                //$dateEnd="Diferente";
                                //}

                                
                            }
                            else{
                                $dateEnd='No history';
                            }
                            
                        }

                    //dias de Periodo depende de dateStart
                        $fechaI = Carbon::parse($dateStart);
                        $fechaF = Carbon::parse($fechaFin);
                        
                        $dias = $fechaF->diffInDays($fechaI);
                        $dias=$dias+1;
                    //anual cost   
                        $billingDetail = BillingDetail::find($bill->idBD);
                        $billing = Billing::find($billingDetail->billing_id);

                        $storageID = $billing->storage_id;
                        $userID = $billing->user_id;

                        $billingConsult = Billing::where([
                                'user_id'   => $userID,
                                'storage_id'    => $storageID
                        ])->whereBetween('created_at',[$fechaI,$fechaF])
                        ->get();
                        
                        $costo_anual = 0;
                        $total=0;
                        foreach($billingConsult as $bi){
                            $bd = BillingDetail::where([
                                    'billing_id'        => $bi->id,
                                    'bd_tipo_partida'   => 'INSURANCE'
                            ])->first();
                                
                            if(!is_null($bd )){
                                $costo_anual += $bd->bd_valor_neto;
                                $total += $bd->bd_valor_neto;
                            }
                        }
                        
                        $costo_anual=($costo_anual * 12);
                        $costo_diario = $costo_anual / 365; 
                        $costo_diario=round($costo_diario,2);
                    //filas del archivo excel
                    $sheet->row($index+2, [
                        $index+1,
                        $bill->name. " ".$bill->lastName . " - " . $bill->companyName ,
                        $bill->alias,
                        $costo_anual,
                        $costo_diario,
                        $dateStartColumn,
                        $dateEnd,
                        $dias,
                        $total
                        //$bill->st_meses_prepago
                        ]);
                }
            });
        })->export('xls');
        ///////////////
    }



    public function indexCorrectedInvoicesDTCSV($fromInvoice,$toInvoice,$yearInvoice)
    {
        
        $billings = Billing::with('user','storages')
        ->where('bi_number_invoice_corrected' , '<>', 'NULL')
        ->where('bi_year_invoice_corrected' , $yearInvoice)
        ->get();
        
        if($fromInvoice != "0" && $toInvoice != "0"){
            $billings = Billing::with('user','storages')
            ->where('bi_number_invoice_corrected' , '<>', 'NULL')
            ->where('bi_year_invoice_corrected' , $yearInvoice)
            ->where('bi_number_invoice_corrected','>=',$fromInvoice)
            ->where('bi_number_invoice_corrected','<=',$toInvoice)
            ->get();
        }elseif($fromInvoice != "0" && $toInvoice == "0"){
            $billings = Billing::with('user','storages')
            ->where('bi_number_invoice_corrected' , '<>', 'NULL')
            ->where('bi_year_invoice_corrected' , $yearInvoice)
            ->where('bi_number_invoice_corrected','>=',$fromInvoice)
            ->get();
        }elseif($fromInvoice == "0" && $toInvoice != "0"){
            $billings = Billing::with('user','storages')
            ->where('bi_number_invoice_corrected' , '<>', 'NULL')
            ->where('bi_year_invoice_corrected' , $yearInvoice)
            ->where('bi_number_invoice_corrected','<=',$toInvoice)
            ->get();
        }
        
                
        \Excel::create('Corrected Invoices Report', function($excel) use($billings) {
        
            $excel->sheet('History', function($sheet) use ($billings) {
                
                $sheet->row(1, [
                        'ID','Client', 'Storage',   'Number Invoice',   'Year Invoice', 'Debit' , 'Payment' , 'Correction Date', 
                        'Number Invoice Corrected', 'Year Invoice Corrected','Reason for Correction'
                ]);
        
                foreach($billings as $index => $bill) {
                    
                    $billOriginal = Billing::where('id_billing_correccion',$bill->id)->first();
                    $cargo = Formato::formatear('ZLOTY', $billOriginal->cargo);
                    
                    $abono_corregido = Formato::formatear('ZLOTY', $bill->abono);
                    $correction_date = Carbon::parse($bill->created_at)->format('Y-m-d');
                    $number_invoice_corrected = $bill->bi_number_invoice_corrected;
                    $year_invoice_corrected = $bill->bi_year_invoice_corrected;
                    $razon_correccion = $bill->bi_razon_correccion;
                    
                    $sheet->row($index+2, [
                            /*
                                <th>ID</th>
                                <th>Client</th>
                                <th>Storage</th>
                                <th>Number Invoice</th>
                                <th>Year Invoice</th>
                                <th>Debit</th>
                                <th>Payment</th>
                                <th>Correction Date</th>
                                <th>Number Invoice Corrected</th>
                                <th>Year Invoice Corrected</th>
                                <th>Reason for Correction</th>
                                <th>PDF</th>
                            */
                            $bill->id,
                            $bill->user->name. " ".$bill->user->lastName . " - " . $bill->user->companyName ,
                            $bill->storages->alias,
                            $bill->bi_number_invoice,
                            $bill->bi_year_invoice,
                            $cargo,
                            $abono_corregido,
                            $correction_date,
                            $number_invoice_corrected ,
                            $year_invoice_corrected, 
                            $razon_correccion
                    ]);
                }
        
            });
        })->export('xls');
        ///////////////
    }

    
    public function indexPrepayment()
    {
    	return view('pw.reports.prepayments.prepayment_report_index');
    }
    
    public function indexPrepaymentDT(Request $request)
    {
    	$storage = $request->get('storage'); 
    	$client = $request->get('client');
    	
    	$storages = Storages::with('level')
    				->join('users','users.id','=','user_id')
    				->where('st_meses_prepago','>','0')
    				->where([
    						'flag_rent'	=> '1',
    						'storages.active'	=> '1'
    				])
    				->where('alias','like', '%'.$storage.'%')
    				->select('*',
    						DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname')
    						)
    				->get();
    	
    	return Datatables::of($storages)
    			->make(true);
    }
      public function indexPrepaymentReportCSV($storage)
    {

        $storage = $storage; 
        //no es necesario $client = $request->get('client');
        
        $storages = Storages::with('level')
                    ->join('users','users.id','=','user_id')
                    ->where('st_meses_prepago','>','0')
                    ->where([
                            'flag_rent' => '1',
                            'storages.active'   => '1'
                    ])
                    ->where('alias','like', '%'.$storage.'%')
                    ->select('*',
                            DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname')
                            )
                    ->get();

        //echo json_encode($storages);
        //return Datatables::of($storages)
        //->make(true);

        \Excel::create('Prepayment Report-'.$storage, function($excel) use($storages) {
        
            $excel->sheet('History', function($sheet) use ($storages) {
                
                $sheet->row(1, [
                        'Storage','Client', 'Rent Start',   'Prepay Months',   'Level'
                
                ]);

                //foreach de la consulta para generar las filas
                foreach ($storages as $index => $storage) {
                    $sheet->row($index+2, [

                     
                        ]);
                }


        });
        })->export('xls');
        ///////////////


    }

    public function indexBraintree()
	{
		return view('pw.reports.braintree.braintree_report_index');
	}
	
	public function indexBraintreeDT(Request $request)
	{
		$txns = TransaccionBraintree::join('users','users.id','=','cliente_id')
				->where([
						'tb_estatus'	=> 'ERROR'
				])->select('*',
    					DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname')
    			)->get();
		
		return Datatables::of($txns)
					->make(true);
	}
	
	public function indexInsurance()
	{
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		
		return view('pw.reports.insurance_report.insurance_report_index',[
				'start'	=> $start,
				'end'	=> $end
		]);
	}
	
	public function indexInsuranceDT(Request $request)
	{
		
		$data = BillingDetail::join('billing','billing_id','=','billing.id')
				//->whereNull('bi_number_invoice_corrected')
				//->whereNull('id_billing_correccion')
				->join('users','billing.user_id','=','users.id')
				->join('storages','billing.storage_id','=','storages.id')
				->where([
					'bd_tipo_partida'	=> 'INSURANCE'
				])
				->whereBetween('pay_month',[$request->get('fechaInicio'),$request->get('fechaFin')])
				->select('*',
    					DB::raw('CONCAT (users.name , " ", users.lastName , " - ", users.companyName ) as fullname'),
    					DB::raw('billing_detail.id as idBD'),
						DB::raw('billing.user_id as idUser'),
						DB::raw('billing.payment_data_id as idPaymentData')
    			)->groupBy('billing.user_id')
    			->groupBy('billing.storage_id')
				->get();
		
		return Datatables::of($data)
					->addIndexColumn()
					->addColumn('fecha_inicio',function($row) use($request){
						
						$returnDate = "";
						
						$rentStart = $row->rent_start;
						
						if($rentStart < $request->get('fechaInicio')){
							$returnDate = $request->get('fechaInicio');
						}else{
							$returnDate = $rentStart;
						}
						
						return Carbon::parse($returnDate)->format('Y-m-d');
					})
					->addColumn('fecha_fin',function($row) use($request){
						
						//VALIDA SI ESTA ACTIVA CON EL CLIENTE
						$storage = Storages::where([
										'flag_rent'	=> 	'1',
										'user_id'	=> $row->user_id,
										'id'		=> $row->storage_id
									])->first();
						
						if(!is_null($storage)){
							return Carbon::parse($request->get('fechaFin'))->format('Y-m-d');
						}else{
							
							$history = History::where([
											'user_id'	=> $row->idUser ,
											'storage_id'=> $row->storage_id
							])->first();
							
							if(!is_null($history)){
								return Carbon::parse($history->rent_end)->format('Y-m-d');
							}
							else{
								return 'No history';
							}
							
						}
						
						//return Carbon::parse($request->get('fechaFin'))->format('Y-m-d');
					})
					->addColumn('dias_periodo',function($row) use($request){
						$fechaInicio = "";
						
						$rentStart = $row->rent_start;
						
						if($rentStart < $request->get('fechaInicio')){
							$fechaInicio = $request->get('fechaInicio');
						}else{
							$fechaInicio = $rentStart;
						}
						//
						
						$fechaInicio = Carbon::parse($fechaInicio);
						$fechaFin = Carbon::parse($request->get('fechaFin'));
						
						$dias = $fechaFin->diffInDays($fechaInicio);
						
						return $dias + 1;
					})
					->addColumn('total_insurance',function($row) use($request){
						//$bdID = $row->idBD;
						///OBTIENE FECHAS
						$fechaInicio = "";
						
						$rentStart = $row->rent_start;
						
						if($rentStart < $request->get('fechaInicio')){
							$fechaInicio = $request->get('fechaInicio');
						}else{
							$fechaInicio = $rentStart;
						}
						//
						
						$fechaInicio = Carbon::parse($fechaInicio);
						$fechaFin = Carbon::parse($request->get('fechaFin'));
						
						///
						
						$billingDetail = BillingDetail::find($row->idBD);
						$billing = Billing::find($billingDetail->billing_id);

						$storageID = $billing->storage_id;
						$userID = $billing->user_id;
						
						$billings = Billing::where([
								'user_id'	=> $userID,
								'storage_id'	=> $storageID
						])->whereBetween('created_at',[$fechaInicio,$fechaFin])
						->get();
						
						$total = 0;
						
						foreach($billings as $bi){
							$bd = BillingDetail::where([
										'billing_id'		=> $bi->id, 
										'bd_tipo_partida'	=> 'INSURANCE'
									])->first();
							
							if(!is_null($bd	)){
								$total += $bd->bd_valor_neto;
							}
						}
						
						return $total; 
					})
					->addColumn('costo_anual',function($row) use($request){
						//$bdID = $row->idBD;
						///OBTIENE FECHAS
						$fechaInicio = "";
						
						$rentStart = $row->rent_start;
						
						if($rentStart < $request->get('fechaInicio')){
							$fechaInicio = $request->get('fechaInicio');
						}else{
							$fechaInicio = $rentStart;
						}
						//
						
						$fechaInicio = Carbon::parse($fechaInicio);
						$fechaFin = Carbon::parse($request->get('fechaFin'));
						
						///
						
						$billingDetail = BillingDetail::find($row->idBD);

						$billing = Billing::find($billingDetail->billing_id);
						
						$storageID = $billing->storage_id;
						$userID = $billing->user_id;
						
						$billings = Billing::where([
								'user_id'	=> $userID,
								'storage_id'	=> $storageID
						])->whereBetween('created_at',[$fechaInicio,$fechaFin])
						->get();
						
						$total = 0;
						
						foreach($billings as $bi){
							$bd = BillingDetail::where([
									'billing_id'		=> $bi->id,
									'bd_tipo_partida'	=> 'INSURANCE'
							])->first();
								
							if(!is_null($bd	)){
								$total += $bd->bd_valor_neto;
							}
						}
						
						return ($total * 12);
					})
					->addColumn('costo_diario',function($row) use($request){
						//$bdID = $row->idBD;
						///OBTIENE FECHAS
						$fechaInicio = "";
					
						$rentStart = $row->rent_start;
					
						if($rentStart < $request->get('fechaInicio')){
							$fechaInicio = $request->get('fechaInicio');
						}else{
							$fechaInicio = $rentStart;
						}
						//
					
						$fechaInicio = Carbon::parse($fechaInicio);
						$fechaFin = Carbon::parse($request->get('fechaFin'));
					
						///
					
						$billingDetail = BillingDetail::find($row->idBD);
						$billing = Billing::find($billingDetail->billing_id);
					
						$storageID = $billing->storage_id;
						$userID = $billing->user_id;
					
						$billings = Billing::where([
								'user_id'	=> $userID,
								'storage_id'	=> $storageID
						])->whereBetween('created_at',[$fechaInicio,$fechaFin])
						->get();
					
						$total = 0;
					
						foreach($billings as $bi){
							$bd = BillingDetail::where([
									'billing_id'		=> $bi->id,
									'bd_tipo_partida'	=> 'INSURANCE'
							])->first();
					
							if(!is_null($bd	)){
								$total += $bd->bd_valor_neto;
							}
						}
						
						$costo_anual = ($total * 12);
						$costo_diario = $costo_anual / 365; 
					
						return round($costo_diario,2);
					})
					->make(true);
	}
	
	public function indexInsuranceDTXLS()
	{
		\Excel::create('Insurance Report XLS', function($excel) {
		
			$excel->sheet('History', function($sheet) {
				$tenants = User::
				leftJoin('storages', 'users.id', '=', 'storages.user_id')
				->join('levels', 'storages.level_id', '=', 'levels.id')
				->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
				->select(
						'storages.alias as storageAlias',
						'levels.name as levelName',
						'warehouse.name as warehouseName',
						'users.name as userName',
						'users.lastName as userLN',
						'users.companyName as companyName',
						'storages.rent_start as rentStart',
						'storages.rent_end as rentEnd'
				)
				->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
				->get();
		
				$sheet->row(1, [
						'Storage', 'Level', 'Warehouse', 'Name', 'Company Name', 'Rent Start', 'Rent End'
				]);
		
				foreach($tenants as $index => $tenant) {
					$sheet->row($index+2, [
							$tenant->storageAlias, $tenant->levelName, $tenant->warehouseName,
							$tenant->userName." ".$tenant->userLN, $tenant->companyName, $tenant->rentStart, $tenant->rentEnd
					]);
				}
		
			});
		})->export('xls'); \Excel::create('Tenants Report CSV', function($excel) {

            $excel->sheet('History', function($sheet) {
                $tenants = User::
                leftJoin('storages', 'users.id', '=', 'storages.user_id')
                    ->join('levels', 'storages.level_id', '=', 'levels.id')
                    ->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
                    ->select(
                        'storages.alias as storageAlias',
                        'levels.name as levelName',
                        'warehouse.name as warehouseName',
                        'users.name as userName',
                        'users.lastName as userLN',
                        'users.companyName as companyName',
                        'storages.rent_start as rentStart',
                        'storages.rent_end as rentEnd'
                    )
                    ->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
                    ->get();

                $sheet->row(1, [
                    'Storage', 'Level', 'Warehouse', 'Name', 'Company Name', 'Rent Start', 'Rent End'
                ]);

                foreach($tenants as $index => $tenant) {
                    $sheet->row($index+2, [
                        $tenant->storageAlias, $tenant->levelName, $tenant->warehouseName,
                        $tenant->userName." ".$tenant->userLN, $tenant->companyName, $tenant->rentStart, $tenant->rentEnd
                    ]);
                }

            });
        })->export('xls');
	}
	
	public function indexJPKFA()
	{
	    $start = Carbon::now()->startOfMonth();
	    $end = Carbon::now()->endOfMonth();
	    
	    return view('pw.reports.jpk_fa.jpk_fa_index',[
	        'start'    => $start,
	        'end'      => $end
	    ]);
	}
	
	public function indexJPKFADT(Request $request)
	{
	    $fechaInicio = $request->get('fechaInicio');
	    $fechaFin = $request->get('fechaFin')." 23:59:59";
	    
	    $invoices = Billing::whereBetween(
                	        'pay_month',[$fechaInicio,$fechaFin]
                	    )->where('cargo','>','0')
                	    ->with(['user','storages'])
	                    ->get();
	    
	    return Datatables::of($invoices)
            	    ->addColumn('fullname',function($row){
            	       return $row->user->name . " " . $row->user->lastName . " - " . $row->user->companyName;  
            	    })
            	    ->addColumn('fecha_documento',function($row){
            	        return Carbon::parse($row->created_at)->format('Y-m-d');
            	    })
	                ->make(true);
	}
	
	public function generarJPKFA_File($fechaInicio,$fechaFin)
	{
	    $invoices = Billing::whereBetween(
        	           'pay_month',[$fechaInicio,$fechaFin]
        	        )->where('cargo','>','0')
        	        ->with(['user','user.UserAddress','storages'])
        	        ->get();
	    
	    $xmlString = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xmlString .= '<tns:JPK 
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                            xmlns:tns="http://jpk.mf.gov.pl/wzor/2016/03/09/03095/" 
                            xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/">';
	    
		$xmlString .= '<tns:Naglowek>
						<tns:KodFormularza kodSystemowy="JPK_FA (1)" wersjaSchemy="1-0">JPK_FA</tns:KodFormularza>
                    		<tns:WariantFormularza>1</tns:WariantFormularza>
                    		<tns:CelZlozenia>1</tns:CelZlozenia>
                    		<tns:DataWytworzeniaJPK>'.Carbon::now()->format('Y-m-dTH:i:s').'</tns:DataWytworzeniaJPK>
                    		<tns:DataOd>'.$fechaInicio.'</tns:DataOd>
                    		<tns:DataDo>'.$fechaFin.'</tns:DataDo>
                    		<tns:DomyslnyKodWaluty>PLN</tns:DomyslnyKodWaluty>
                    		<tns:KodUrzedu>1473</tns:KodUrzedu>
                    	</tns:Naglowek>

                        <tns:Podmiot1>
                        		<tns:IdentyfikatorPodmiotu>
                        			<etd:NIP>5272703362</etd:NIP>
                        			<etd:PelnaNazwa>PRZECHOWAMY WSZYSTKO Sp. z o.o.</etd:PelnaNazwa>
                        		</tns:IdentyfikatorPodmiotu>
                        		<tns:AdresPodmiotu>
                        			<etd:KodKraju>PL</etd:KodKraju>
                        			<etd:Wojewodztwo>BRAK</etd:Wojewodztwo>
                        			<etd:Powiat>BRAK</etd:Powiat>
                        			<etd:Gmina>BRAK</etd:Gmina>
                        			<etd:Ulica>Kineskopowa</etd:Ulica>
                        			<etd:NrDomu>1</etd:NrDomu>
                        			<etd:Miejscowosc>Piaseczno</etd:Miejscowosc>
                        			<etd:KodPocztowy>05-500</etd:KodPocztowy>
                        			<etd:Poczta>BRAK</etd:Poczta>
                        		</tns:AdresPodmiotu>
                        </tns:Podmiot1>';
	    
	    foreach($invoices as $invoice){
	        
	        if($invoice->bi_paragon == 1){
	            $invoiceNumber = $invoice->bi_year_invoice. "-PAR-".$invoice->bi_number_invoice;
	        }else{
	            $invoiceNumber = $invoice->bi_year_invoice. "-PW-".$invoice->bi_number_invoice;
	        }
	        
			$tenant = $invoice->user->name . ' '. $invoice->user->lastName . ' - '. $invoice->user->companyName;
			$tenant = str_replace('&','',$tenant);
	        
	        $country = Countries::find($invoice->user->UserAddress[0]->country_id);
	        
	        $fullAddress = $invoice->user->UserAddress[0]->postCode.' '.
	   	                   $invoice->user->UserAddress[0]->city. ', '.
	   	                   $invoice->user->UserAddress[0]->street.' '.
	   	                   $invoice->user->UserAddress[0]->number. ', '.
	                       $country->name;
	        
	        $nip = $invoice->user->nipNumber; 
	        
	        $xmlString .= '<tns:Faktura typ="G">
                            		<tns:P_1>'.$invoice->pay_month.'</tns:P_1>
                            		<tns:P_2A>'.$invoiceNumber.'</tns:P_2A>
                            		<tns:P_3A>'.$tenant.'</tns:P_3A>
                            		<tns:P_3B>'.$fullAddress.'</tns:P_3B>
                            		<tns:P_3C>PRZECHOWAMY WSZYSTKO Sp. z o.o.</tns:P_3C>
                            		<tns:P_3D>ul. Kineskopowa 1, 05-500 Piaseczno</tns:P_3D>
                            		<tns:P_4B>5272703362</tns:P_4B>
                            		<tns:P_5B>'.$nip.'</tns:P_5B>
                            		<tns:P_6>'.$invoice->pay_month.'</tns:P_6>
                            		<tns:P_13_1>'.$invoice->bi_total.'</tns:P_13_1>
                            		<tns:P_14_1>'.$invoice->bi_total_vat.'</tns:P_14_1>
                            		<tns:P_13_2>0</tns:P_13_2>
                            		<tns:P_14_2>0</tns:P_14_2>
                            		<tns:P_13_3>0</tns:P_13_3>
                            		<tns:P_14_3>0</tns:P_14_3>
                            		<tns:P_13_4>0</tns:P_13_4>
                            		<tns:P_14_4>0</tns:P_14_4>
                            		<tns:P_13_5>0</tns:P_13_5>
                            		<tns:P_14_5>0</tns:P_14_5>
                            		<tns:P_15>150</tns:P_15>
                            		<tns:P_16>false</tns:P_16>
                            		<tns:P_17>false</tns:P_17>
                            		<tns:P_18>false</tns:P_18>
                            		<tns:P_19>false</tns:P_19>
                            		<tns:P_20>false</tns:P_20>
                            		<tns:P_21>false</tns:P_21>
                            		<tns:P_23>false</tns:P_23>
                            		<tns:P_106E_2>false</tns:P_106E_2>
                            		<tns:P_106E_3>false</tns:P_106E_3>
                            		<tns:RodzajFaktury>VAT</tns:RodzajFaktury>
                            		<tns:ZALZaplata>0</tns:ZALZaplata>
                            		<tns:ZALPodatek>0</tns:ZALPodatek>
                            	</tns:Faktura>';
	        
	        $xmlStringComplement = '<tns:FakturaWiersz typ="G">
                                    		<tns:P_2B>'.$invoiceNumber.'</tns:P_2B>
                                    		<tns:P_7>Koszt przeniesienia rzeczy z magazynu do magazynu zastępczego</tns:P_7>
                                    		<tns:P_8A>szt</tns:P_8A>
                                    		<tns:P_8B>1</tns:P_8B>
                                    		<tns:P_9B>150</tns:P_9B>
                                    		<tns:P_11A>150</tns:P_11A>
                                    		<tns:P_12>23</tns:P_12>
                                    	</tns:FakturaWiersz>';
	    }
	    
	    $xmlString .= '<tns:FakturaCtrl>
                    		<tns:LiczbaFaktur>'.$invoices->count().'</tns:LiczbaFaktur>
                    		<tns:WartoscFaktur>'.$invoices->sum('bi_total').'</tns:WartoscFaktur>
                    	</tns:FakturaCtrl>';  
	    
	    $xmlString .= '<tns:StawkiPodatku>
                    		<tns:Stawka1>0'.env('VAT').'</tns:Stawka1>
                    		<tns:Stawka2>0.08</tns:Stawka2>
                    		<tns:Stawka3>0.05</tns:Stawka3>
                    		<tns:Stawka4>0.00</tns:Stawka4>
                    		<tns:Stawka5>0.00</tns:Stawka5>
                    	</tns:StawkiPodatku>'; 
	    
	    $xmlString .= $xmlStringComplement;
	    
	    $xmlString .= '<tns:FakturaWierszCtrl>
		                  <tns:LiczbaWierszyFaktur>'.$invoices->count().'</tns:LiczbaWierszyFaktur>
                		  <tns:WartoscWierszyFaktur>'.$invoices->sum('bi_total').'</tns:WartoscWierszyFaktur>
                	</tns:FakturaWierszCtrl>';

	    $xmlString .= '</tns:JPK>';
	    
	    
	    $fileName = 'JPK_FA_'.Carbon::now()->format('Y-m-d H.i.s').'.xml';
	    $filePath = storage_path('app/'.$fileName);
	    
	    
	    //$myfile = fopen($fileName, "w") or die("Unable to open file!");
	    //fwrite($myfile, $filePath);
	    
	    Log::info('Filepath: '. $filePath);
	    
	    File::put($filePath,$xmlString);
	    
	    
	    //$xml = new \SimpleXMLElement($filePath);
	    //$xml = new \SimpleXMLElement(file_get_contents(storage_path('app/'.'test.xml')));

	    /*
	    $xml->addAttribute('version', '1.0');
	    $xml->addAttribute('encoding', 'UTF-8');
	    
	    /*
	     <tns:JPK
    	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    	xmlns:tns="http://jpk.mf.gov.pl/wzor/2016/03/09/03095/"
    	xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/">
	     * */
	    /*
	    $jpk = $xml->addChild('tns:JPK');
	    $jpk->addAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
	    $jpk->addAttribute('xmlns:tns','http://jpk.mf.gov.pl/wzor/2016/03/09/03095/');
	    $jpk->addAttribute('xmlns:etd','http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/');
	    */
	    
	    
	    //$xml->saveXML($filePath);
	    /*
	    $response = Response::make($xml->asXML(), 200);
	    $response->header('Content-Type', 'text/xml');
	    $response->header('Cache-Control', 'public');
	    $response->header('Content-Description', 'File Transfer');
	    $response->header('Content-Disposition', 'attachment; filename='.$fileName);
	    $response->header('Content-Transfer-Encoding', 'binary');
	    $response->header('Content-Type', 'text/xml');
	    
	    return $response;
	    */
	    return Response::make(file_get_contents($filePath), 200, [
	        'Content-Type' => 'text/xml',
	        'Content-Disposition' => 'attachment; filename="'.$fileName.'"']);
	    
	}

	public function indexPayments()
	{
	    $start = Carbon::now()->startOfMonth();
	    $end = Carbon::now()->endOfMonth();
	    
	    return view('pw.reports.payments.payments_index',[
	        'start'    => $start,
	        'end'      => $end
	    ]);
	}

	public function indexPaymentsDT(Request $request)
	{
		$dateStart = $request->get("fechaInicio"). " 00:00";
		$dateEnd = $request->get("fechaFin"). " 23:59";

		$filtros = [];

		if($request->get('paymentTypeId') != ""){
			$filtros['payment_type_id'] = $request->get('paymentTypeId');
		}
		
		$billing = Billing::where('abono','>','0')
					->whereBetween('pay_month',[$dateStart,$dateEnd])
					->whereNull('bi_razon_correccion')
					->where($filtros)
					->with(['user','storages','paymentType'])
					->get();

		#Log::info($filtros);
		
		return Datatables::of($billing)
				->addColumn('tenant',function($row){
					return $row->user->name . " ". $row->user->lastName . " ". " - ". $row->user->companyName;
				})
				->addColumn('automatic_pay', function($row){
					if($row->bi_webhook == 1){
						return "YES";
					}else{
						return "NO";
					}
				})
				->addColumn('amount',function($row){
					return Formato::formatear("ZLOTY", $row->abono);
				})
				->make(true);
	}

	public function indexPaymentsDTCSV($fechaInicio,$fechaFin,$paymentType)
    {
    	$dateStart = $fechaInicio. " 00:00";
		$dateEnd = $fechaFin. " 23:59";

		$filtros = [];

		if($paymentType != "0"){
			$filtros['payment_type_id'] = $paymentType;
		}
		
		$billing = Billing::where('abono','>','0')
					->whereBetween('pay_month',[$dateStart,$dateEnd])
					->whereNull('bi_razon_correccion')
					->where($filtros)
					->with(['user','storages','paymentType'])
					->get();


    	$headers = array(
    			"Content-type" => "text/csv",
    			"Content-Disposition" => "attachment; filename=Payments_".Carbon::now()->format('Y-m-d_H-m-s').".csv",
    			"Pragma" => "no-cache",
    			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
    			"Expires" => "0"
    	);
    	
    	$columns = array('"ID"','"Tenant"', '"Storage / Box"', '"Payment Date"', '"Automatic Pay"', '"Payment Type"', 
    							'"Amount"');
    		
    	$content = "";
    	
		$content = implode(',', $columns)."\n";
		
		//Log::info($billing);
    	
    	foreach($billing as $bill){

			$tenant 		= $bill->user->name . " ". $bill->user->lastName . " ". " - ". $bill->user->companyName;
			if($bill->bi_webhook == 1){
				$automatic_pay 	=  "YES";
			}else{
				$automatic_pay 	= "NO";
			}

			#Log::info("ID : " . $bill->id);
			#Log::info("PAYMONTH : " . $bill->pay_month);
			#Log::info("STORAGE : " . $bill->storages->alias);
			

			if(!is_null($bill->paymentType)){
				$payment_type = $bill->paymentType->name;
			}
			else{
				$payment_type = "--";
			}
			#Log::info("PAYMENT TYPE : " . $payment_type);
			$amount = Formato::formatear("ZLOTY", $bill->abono);
			
			//Log::info("PAY NAME : " . );

    		$content.= '"'.$bill->id.'","'.$tenant.'","'.$bill->storages->alias.'","'.$bill->pay_month.'","'.$automatic_pay.'","'.$payment_type.'","'.$amount.'"'."\n";
    	}
    
    	
    	return \Illuminate\Support\Facades\Response::make($content, 200, $headers);
	}
	
	public function indexWeekly()
	{
		$start = Carbon::now()->startOfWeek();
	    $end = Carbon::now()->endOfWeek();
	    
	    return view('pw.reports.weekly.weekly_index',[
	        'start'    => $start,
	        'end'      => $end
	    ]);
	}
	
	public function indexWeeklyDT(Request $request)
	{
		Log::info("HIN");
		$dateStart = $request->get("fechaInicio"). " 00:00";
		$dateEnd = $request->get("fechaFin"). " 23:59";

		$filtros = [];
		$filtros['active']	= 1;

		$storages = Storages::whereBetween('rent_start',[$dateStart,$dateEnd])
					->where($filtros)
					->select(['id','alias','sqm','user_id','rent_start','rent_end','price_per_month','payment_data_id']);

		$history = History::join('storages','storage_id','=','storages.id')
					->whereBetween('history.rent_start',[$dateStart,$dateEnd])
					->union($storages)
					->select(['storage_id as id','alias','sqm','history.user_id','history.rent_start','history.rent_end','history.price_per_month','history.payment_data_id'])
					->get();

		
		return Datatables::of($history)
				->addColumn('tenant',function($row){
					if($row->user_id != 0){
						$user = User::find($row->user_id);

						return $user->name . " ". $user->lastName . " ". " - ". $user->companyName;
					}
				})
				->addColumn('date_end',function($row){
					if($row->rent_end == "0000-00-00"){
						return "--";
					}
					return $row->rent_end;
				})
				->addColumn('price',function($row){
					return Formato::formatear("ZLOTY",$row->price_per_month);
				})
				->addColumn('sign_from',function($row){
					$pd = PaymentData::find($row->payment_data_id);

					return $pd->pd_method;
				})
				->make(true);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Function to return the Billings Reports view with the current date.
     * @return [type] [Return the view with the current date]
     */
    public function billingReports() {
        $fecha = Carbon::now();
        // dd($fecha->format('Y'));
        return view('pw.billing_reports_filter', ['year'=>$fecha->format('Y')]);
    }

    /**
     * Function to charge the Billing Reports view divided per month.
     * @param  [type] $anio [year selected]
     * @return [type]       [Return the Billing reports view with the months and the sum of his amounts and charges]
     */
    public function filterYear($anio, $levels) {
        $meses = utilBillingFilters::getMesesMontoAnio($anio, $levels);
        // dd($meses);
        //Traducciones
        $traBillingReports = trans('billing_reports.billing_reports');
        $traAmounts = trans('billing_reports.amounts');
        $traAmount = trans('billing_reports.payed');
        $traCharges = trans('billing_reports.charges');
        $traCharge = trans('billing_reports.billed');
        $traMonth = trans('billing_reports.month');
        $January = trans('billing_reports.January');
        $February = trans('billing_reports.February');
        $March = trans('billing_reports.March');
        $April = trans('billing_reports.April');
        $May = trans('billing_reports.May');
        $June = trans('billing_reports.June');
        $July = trans('billing_reports.July');
        $August = trans('billing_reports.August');
        $September = trans('billing_reports.September');
        $October = trans('billing_reports.October');
        $November = trans('billing_reports.November');
        $December = trans('billing_reports.December');
        //Fin traduciones
        $totalAmounts = 0;
        $totalCharges = 0;
        foreach ($meses as $key => $mes) {
            $totalAmounts += $mes->sum_abonos;
            $totalCharges += $mes->sum_cargos;
        }
          // dd($meses);
        return view(
            'pw.billing_reports',
            [
                'meses' => $meses,
                'totalAmounts'=>$totalAmounts,
                'totalCharges'=>$totalCharges,
                'traAmount'=>$traAmount,
                'traCharge'=>$traCharge,
                'traMonth'=>$traMonth,
                'traBillingReports'=>$traBillingReports,
                'traAmount'=>$traAmount,
                'January'=>$January,
                'February'=>$February,
                'March'=>$March,
                'April'=>$April,
                'May'=>$May,
                'June'=>$June,
                'July'=>$July,
                'August'=>$August,
                'September'=>$September,
                'October'=>$October,
                'November'=>$November,
                'December'=>$December,
            ]
        );
    }

    /**
     * Function to display the amounts and charges made by clients in the selected month and year
     * @param  [type] $year  [year selected]
     * @param  [type] $month [month selected]
     * @return [type]        [Return the Detail billing view with the clients and their sum of amounts and charges]
     */
    public function filterYearClient($year, $month, $levels) {
        $clients = utilBillingFilters::getClients($year, $month, $levels);
        $totalAmounts = 0;
        $totalCharges = 0;
        foreach ($clients as $key => $client) {
            $totalAmounts += $client->sumAbonos;
            $totalCharges += $client->sumCargos;
        }
        // dd($totalCharges);
        return view('pw.detail_billing', ['clients'=>$clients, 'year'=>$year, 'month'=>$month, 'totalAmounts'=>$totalAmounts, 'totalCharges'=>$totalCharges]);
    }

    /**
     * Function to display all the amounts and charges made by the selected client.
     * @param  [type] $id [id of client]
     * @return [type]     [Return the Detail billing client view with all the amonunts and charges]
     */
    public function getBillingClientDetails($id, $year, $month) {
        $billings = utilBillingFilters::getBillingClientDetails($id, $year, $month);
        $totalAmounts = 0;
        $totalCharges = 0;
        foreach ($billings as $key => $billing) {
            $totalAmounts += $billing->abono;
            $totalCharges += $billing->cargo;
        }
        // dd($billings);
        return view('pw.detail_billings_client', ['billings'=>$billings, 'totalAmounts'=>$totalAmounts, 'totalCharges'=>$totalCharges]);
	}
	

	public function indexAppUsers()
	{
		Log::info(\Session::all());
		return view('pw.reports.app_users.app_users_index');
	}

	public function indexAppUsersDT(Request $request)
	{
		$users = User::where([
					'active'	=> '1'
				])->where('movil_dispositive' , '<>', '0')
				->where('id' , '<>', '1')->get();

		return Datatables::of($users)
				->addColumn('fullname',function($row){
					return $row->name . " ". $row->lastName . " ". " - ". $row->companyName;
				})
				->addColumn('platform', function($row){
					if($row->movil_dispositive == "1"){
						return "iOS";
					}elseif($row->movil_dispositive == "2"){
						return "Android";
					}else{
						return "Web Client";
					}
				})
				->addColumn('storages_rented',function($row){
					$total = Storages::where([
						'user_id'	=> $row->id,
						'flag_rent'	=> '1'
					])->count();

					return '<a href="'.route('app_users.storages.index',[$row->id]).'" class="btn btn-default btn-storages-user">'.$total.'</a>';
				})
				->addColumn('registration_date',function($row){
					return Carbon::parse($row->created_at)->format('Y-m-d');
				})
				->make(true);
	}

	public function indexAppUsersStorages($id)
	{
		return view('pw.reports.app_users.app_users_storages_index',[
			'id'	=> $id
		]);
	}

	public function indexAppUsersStoragesDT(Request $request)
	{
		$user_id = $request->get('user_id');

		$storages = Storages::where([
			'user_id'	=> $user_id,
			'active'	=> '1'
		])->get();

	
		return Datatables::of($storages)
					->addColumn('finish_date', function($row){
						if($row->rent_end == "0000-00-00"){
							return '--';
						}else{
							return $row->rent_end;
						}
					})
					->addColumn('prepayment', function($row){
						return $row->st_meses_prepago. ' Months';
					})
					->addColumn('user_transaction', function($row){
						Log::info("PD ID : ". $row->payment_data_id);
						$paymentData = PaymentData::find($row->payment_data_id);
						
						Log::info("TRN ID : ". $paymentData->user_transaction_id);
						$user = User::find($paymentData->user_transaction_id);
						
						return $user->email;
					})
					->addColumn('credit_card', function($row){
						$paymentData = PaymentData::find($row->payment_data_id);
						
						if($paymentData->creditcard == 1) {
							return "YES";
						}else{
							return "NO";
						}
					})
					->addColumn('rent_method', function($row){
						$paymentData = PaymentData::find($row->payment_data_id);

						return $paymentData->pd_method;
						
					})
					->addColumn('rent_date', function($row){
						$paymentData = PaymentData::find($row->payment_data_id);

						return $paymentData->created_at;
					})
					->addColumn('balance', function($row){
						$billing = Billing::where([
								'user_id'	=> $row->User_id,
								'storage_id'=> $row->id
							])->orderBy('id','desc')->first();

						if(is_null($billing)){
							return Formato::formatear('ZLOTY', 0);	
						}

						return Formato::formatear('ZLOTY', $billing->saldo);
					})
					->addColumn('braintree_suscription', function($row){
						$paymentData = PaymentData::find($row->payment_data_id);

						return $paymentData->pd_suscription_id;
					})
					->make(true);

	}
}
