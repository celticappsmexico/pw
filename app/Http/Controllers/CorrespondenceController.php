<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Storages;
use App\User;
use App\Correspondences;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Auth;

class CorrespondenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Log::info('CorrespondenceController@create');
        return view('correspondence.create_correspondence');
    }

//

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$mensaje=$request->get('message');
		$subject =$request->get('subject');
		
		$idsUsuarios=array();
		
		if($request->get('only_admin') == 'on'){
			//396
			array_push($idsUsuarios,'396');
			
			$total = 1;
		}else{
	       	$storages=Storages::where('user_id','<>','0')
	       		->where([
	       			'active'	=> '1'	,
	       			'flag_rent'	=> '1'
	       		])->with('StorageOwner')->groupBy('user_id')
	       	->get();
	       	
	       	foreach ($storages as $storage) {
	       			
	       		$email 	=	$storage->StorageOwner->email;
	       	
	       		$idUser	=	$storage->user_id;
	       	
	       		array_push($idsUsuarios,$idUser);
	       	}
	       	 
	       	$total = $storages->count();
		}
        
        $arregloIds=json_encode($idsUsuarios);
       
	    $correspondence = new Correspondences();
	    $correspondence->users_ids = $arregloIds;
	    $correspondence->asunto = $subject;
	    $correspondence->corresp_mensaje = $mensaje;
	    $correspondence->mail_from = "cwbiuro@przechowamy-wszystko.pl";
	    $correspondence->save();
	    
       
       	return ['returnCode'    => 200, 'msg'   => 'correspondence sent ('.$total.')'];
   
    }
    
    public function getCorrespondenceAdmin()
    {
    	
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
