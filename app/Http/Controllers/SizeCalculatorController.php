<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CataSecciones;
use App\Articulos;
use App\Storages;
use App\User;
use App\Warehouse;
use App\clientAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Mail;
use App\ExtraItems;
use App\Http\Fecha;

class SizeCalculatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utilFecha = new Fecha();
        $calculator = false;
        $hoy = date("Y-m-d");
        $monthDays = $utilFecha->getUltimoDiaMes($hoy);

        $warehouse = Warehouse::where('Active', '!=', '0')->get();
        return view('size-estimator/size-estimator',['buildings' => $warehouse,'calculator' => $calculator,'days' => $monthDays]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicioComplete()
    {
        return view('auth/completeUser');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getStorage(Request $data){
        $column = 'sqm';
        $storage1 = Storages::where('flag_rent','!=',1)->where($column, '>=', $data['total'])->where('price', '>', '0')->orderBy($column, 'asc')->first();
        $storage2 = Storages::where('flag_rent','!=',1)->where($column, '>=', ($data['total']*1.20))->where('price', '>', '0')->orderBy($column, 'asc')->first();

        if ($data['mobile'] == 1) {
          $user_id = $data['id'];
        }else{
          $user_id = Auth::user()->id;
        }

        //**Pintar modal rent storage**//
        $cad = '';
        $cad .= '<div class="col-md-12">';
            //**Imagen**//
            $cad .= '<div class="row">';
                $cad .= '<div class="col-md-6" id="div-box-1">';
                    $cad .= '<img src="../assets/images/size-estimator/storage-box.png" id="img_storage_box">';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-box-2">';
                    $cad .= '<img src="../assets/images/size-estimator/storage-box.png" id="img_storage_box">';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Sqm**//
            $cad .= '<div class="row row-1" id="div-notice">';
                $cad .= '<div class="col-md-6" id="div-sqm-1">';
                    $cad .= '<span class="price">'.$storage1->sqm.' '.trans('storages.sqm').'</span>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-sqm-2">';
                    $cad .= '<span class="price">'.$storage2->sqm.' '.trans('storages.sqm').'</span>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Price**//
            $cad .= '<div class="row row-2">';
                $cad .= '<div class="col-md-6" id="div-price-1">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('storages.price').':</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span class="" id="span-price-1">'.$storage1->price.'</span> pln';
                    $cad .= '</div>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-price-2">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('storages.price').':</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span class="" id="span-price-2">'.$storage2->price.'</span> pln';
                    $cad .= '</div>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Notice**/
            $cad .= '<div class="row row-1" id="row-notice">';
                $cad .= '<div class="col-md-3" id="div-label-notice">';
                    $cad .= '<label id="label-notice">'.trans('calculator.term_notice').'</label>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-9">';
                    $cad .= '<select class="select-style show-tick show-menu-arrow" id="select-notice">';
                        $cad .= '<option value="def" default>'.trans('storages.choose_notice').'</option>';
                        $cad .= '<option value="1" data-subtext="- 0% '.trans('storages.off').'">1</option>';
                        $cad .= '<option value="3" data-subtext="- 5% '.trans('storages.off').'">3</option>';
                        $cad .= '<option value="6" data-subtext="- 10% '.trans('storages.off').'">6</option>';
                        $cad .= '<option value="12" data-subtext="- 15% '.trans('storages.off').'">12</option>';
                    $cad .= '</select>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Pre-pay**//
            $cad .= '<div class="row row-2" id="row-prepay">';
                $cad .= '<div class="col-md-3" id="div-label-prepay">';
                    $cad .= '<label id="label-prepay">'.trans('calculator.pre-pay').'</label>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-9">';
                    $cad .= '<select class="select-style show-tick show-menu-arrow" id="select-prepaid">';
                        $cad .= '<option value="def" default>'.trans('storages.choose_option').'</option>';
                        $cad .= '<option value="Yes">'.trans('calculator.yes').'</option>';
                        $cad .= '<option value="No">'.trans('calculator.no').'</option>';
                    $cad .= '</select>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Credit card**//
            $cad .= '<div class="row row-1" id="row-credit-card">';
                $cad .= '<div class="col-md-3" id="div-label-credit">';
                    $cad .= '<label id="label-credit-card">'.trans('calculator.credit_card').'</label>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-9">';
                    $cad .= '<select class="select-style show-tick show-menu-arrow" id="select-credit-card">';
                        $cad .= '<option value="def" default>'.trans('storages.choose_option').'</option>';
                        $cad .= '<option value="Yes">'.trans('calculator.yes').'</option>';
                        $cad .= '<option value="No">'.trans('calculator.no').'</option>';
                    $cad .= '</select>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Extra items**//
            $cad .= '<div class="row row-1" id="row-credit-card">';
                $cad .= '<div class="col-md-3" id="div-label-credit">';
                    $cad .= '<label id="label-credit-card">'.trans('calculator.extra_items').'</label>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-9">';
                    $cad .= '<a href="javascript:void(0);" dir="1" class="btn_extraItems btn btn-primary">'.trans('storages.choose_items').'</a>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Insurance**//
            $cad .= '<div class="row row-1">';
                $cad .= '<div class="col-md-6" id="div-ensurance-1">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.insurance').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span id="span-ensurance-rent">16 pln</span>';
                    $cad .= '</div>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-ensurance-2">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.insurance').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span id="span-ensurance-rent">16 pln</span>';
                    $cad .= '</div>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Subtotal**//
            $cad .= '<div class="row row-2">';
                $cad .= '<div class="col-md-6" id="div-subtotal-1">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.subtotal').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span id="span-subtotal-1"></span>';
                    $cad .= '</div>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-subtotal-2">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.subtotal').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span id="span-subtotal-2"></span>';
                    $cad .= '</div>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Vat**//
            $cad .= '<div class="row row-1">';
                $cad .= '<div class="col-md-6" id="div-vat-1">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.vat').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span>23%</span>';
                    $cad .= '</div>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-vat-2">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.vat').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span>23%</span>';
                    $cad .= '</div>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Total**//
            $cad .= '<div class="row row-2">';
                $cad .= '<div class="col-md-6" id="div-total-1">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.total').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span class="" id="span-total-rent-1"></span>';
                    $cad .= '</div>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-total-2">';
                    $cad .= '<div class="col-md-4">';
                        $cad .= '<span>'.trans('calculator.total').'</span>';
                    $cad .= '</div>';
                    $cad .= '<div class="col-md-8">';
                        $cad .= '<span class="" id="span-total-rent-2"></span>';
                    $cad .= '</div>';
                $cad .= '</div>';
            $cad .= '</div>';
            //**Choose storage**//
            $cad .= '<div class="row row-btn">';
                $cad .= '<div class="col-md-6" id="div-btn-rent-1">';
                    $cad .= '<a href="#" id="btn_rent_1" role="button" data-toggle="modal" class="btn btn-success btn-large">'.trans('storages.add_storage').'</a>';
                $cad .= '</div>';
                $cad .= '<div class="col-md-6" id="div-btn-rent-2">';
                    $cad .= '<a href="#" id="btn_rent_2" role="button" data-toggle="modal" class="btn btn-success btn-large">'.trans('storages.add_storage').'</a>';
                $cad .= '</div>';
            $cad .= '</div>';

            $cad .= '<div style="visibility: hidden;">';
                $cad .= '<span class="" name="span_id_storage_1" id="span_id_storage_1">'.$storage1->id.'</span>';
                $cad .= '<span class="" name="span_id_storage_2" id="span_id_storage_2">'.$storage2->id.'</span>';
                $cad .= '<span class="" name="span_id_user" id="span_id_user">'.$user_id.'</span>';
            $cad .= '</div>';
        $cad .= '</div>';

        if ($data['mobile'] == 1) {
          $extraitems = ExtraItems::all();
          return ['success' => 'true','storages' => [$storage1,$storage2],'extra-items' => $extraitems];
        }else{
          return $cad;
        }

    }
    /**
     * Función para validar si el usuario esta completo.
     * @return [type] [description]
     */
    public function validateUser() {
        $user = Auth::user()->validate;
        //dd($user);

        if ($user == 0) {
            return "1";
        }else {
            return "2";
        }
    }

    /**
     * Función para completar los datos del usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function completeUser(Request $request)
    {
        $idUser = Auth::user()->id;
        $client = User::find($idUser);
        //$adress = clientAddress::find('user_id', '=', $idUser);

        //dd($adress);

        $client->name = $request->get('name');
        $client->lastName = $request->get('lastName');
        $client->birthday = $request->get('birthday');
        $client->peselNumber = $request->get('peselNumber');
        $client->idNumber = $request->get('idNumber');
        $client->phone = $request->get('phone');
        $client->validate = '1';
        $res = $client->save();

        if ($res) {
            $column = 'user_id';
            $adress = clientAddress::where($column,'=', $idUser)->first();
            $adress->street = $request->get('street');
            $adress->number = $request->get('number');
            $adress->apartmentNumber = $request->get('apartmentNumber');
            $adress->postCode = $request->get('postCode');
            $adress->city = $request->get('city');
            $adress->country_id = $request->get('country');
            $adress->save();
        }

        $data = [];
        \Mail::send('emails.plantilla', $data, function($message){
            $message->from('soporte@novacloud.mx')
                    ->to('aguilas.2007@hotmail.com')
                    ->subject('Probando mailgun en laravel 5.1');
        });

        return Redirect::route('sizeCalculator');
    }

    
}
