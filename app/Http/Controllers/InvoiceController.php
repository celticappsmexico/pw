<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Fecha;
use App\DetOrders;
use App\CataTerm;
use App\CataPrepay;
use App\ConfigSystem;
use Storage;
use Illuminate\Http\Response;
use App\CataPaymentTypes;
use App\Orders;
use App\User;
use App\Countries;
use App\Billing;
use App\BillingDetail;
use App\Storages;
use App\PaymentData;
use App\btRecBilling;
use App\History;
use Illuminate\Support\Facades\Log;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Carbon\Carbon;
use App\Library\MoneyString;
use App\ReciboSequence;
use App\InvoiceSequence;
use App\Library\BraintreeHelper;
use App\CardUser;
use App\ParagonSequence;
use App\Library\BillingHelper;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        $orderId = $data['orderId'];
        $paymentType = $data['paymentType'];
        isset($data['reference']) ? $ref = $data['reference'] : $ref = '';

        $flagPaid = $data->get('flag_paid');
        
        //$this->createInvoice($orderId,$paymentType);
        $res = $this->createInvoiceV2($orderId,$paymentType,$ref,$flagPaid,$data['token_card'],$data['customer']);
        //dd($res);
        return view('successPay',['response' => $res]);
    }
    
    public function createInvoiceV2($orderId,$paymentType,$ref,$flagPaid, $tokenCard = false, $customer = false)
    {

    	$pt = CataPaymentTypes::find($paymentType);
    	$pt = $pt->name;
    	
    	$items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
    	$order = Orders::where('id','=',$orderId)->first();
    	$userId = $order->user_id;
    	
    	//DATOS QUE MUEsTRA EN PANTALLA DE SUCCESS
    	$amountCurrent = "";
    	$amountPrepay = "";
    	
    	$transactionIDCurrent = "";
    	$transactionIDPrepay = "";
    	
    	$returnCodeCurrent = "";
    	$returnCodePrepay = "";
    	
    	$errorCurrent = "";
    	$errorPrepay = "";
    	
    	$statusCurrent = "";
    	$cardTypeCurrent = "";
    	
    	$statusPrepay = "";
    	$cardTypePrepay = "";
    	//FIN DE DATOS DE PANTALLA DE SUCCESS
    	
    	//return [$newInvoiceNumberDb];
    	
    	//return ['items' => $items];
    	$idStorage = 0;
    	
    	foreach ($items as $key => $item) {
    		if($item->product_type == 'box'){
    			 
    			$idStorage = $item->product_id;
    	
    			$fecha = $item->rent_starts;
    	
    	
    			$st = Storages::find($idStorage);
    	
    			if ($st == null) {
    				return ['success' => false, 'msj' => trans('storages.incomplete_data')];
    			}
    			
    			$prepayMonths = $item->paymentData[0]->prepay;
    			$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
    			
    			//$desc2 = ($prepayMonths->off / 100);--
    			$prepayMonths = $prepayMonths->months;
    	
    			$st->flag_rent = 1;
    			$st->user_id = $userId;
    			//
    			$st->rent_start = $fecha;
    			$st->st_meses_prepago = $prepayMonths;
    			//$st->price_per_month = $totalNextMonth;
    			$res = $st->save();
    		}
    	}
    	$paymentData = $st->StoragePaymentData->first();

    	$bill = BillingHelper::insertaCargos($userId,$idStorage,$paymentData->id,$flagPaid,$paymentType,$tokenCard,$customer,0);
    	

    	//Desactiva orden
    	$order->active = 0;
    	$order->save();
    	
    	Log::info('***************');
    	Log::info($bill);
    	Log::info('***************');
    	
    	$amountCurrent = $bill['amountCurrent'];
    	$amountPrepay = $bill['amountPrepay'];
    	 
    	$transactionIDCurrent = $bill['transactionIDCurrent'];
    	$transactionIDPrepay = $bill['transactionIDPrepay'];
    	 
    	$returnCodeCurrent = $bill['returnCodeCurrent'];
    	$returnCodePrepay = $bill['returnCodePrepay'];
    	 
    	$errorCurrent = $bill['errorCurrent'];
    	$errorPrepay = $bill['errorPrepay'];
    	 
    	$statusCurrent = $bill['statusCurrent'];
    	$cardTypeCurrent = $bill['cardTypeCurrent'];
    	 
    	$statusPrepay = $bill['statusPrepay'];
    	$cardTypePrepay = $bill['cardTypePrepay'];
    	
    	
		    if($prepayMonths > 0){
		    	$prepay = 1;
		    }else{
		    	$prepay = 0;
		    }
		    	
		    if($flagPaid == 1){
		    	if($returnCodeCurrent == 200){
		    		$msgCurrent = "Payment Successful";
		    	    $msg = "Payment Successful";
		    	}if($returnCodeCurrent == ""){
            $msgCurrent = "Payment Successful";
          }
		    	else{
		    		$msgCurrent = "Payment Error";
		    	    $msg = "Payment Error. Invoice Saved";
		    	}
		   	}else{
		    	$returnCodeCurrent = 200;
		    	$msg = "Saved successful";
		    	$msgCurrent = "Saved successful";
		    }
		    	
		    if($returnCodeCurrent == 200 && $flagPaid == 1){
		    	$msgCurrent = "Payment Successful";
        }

        Log::info('ReturnCodeCurrent : '. $returnCodeCurrent);
		    	
		    return ['returnCode' 			=> 200,
		    	        'amountCurrent'		=> $amountCurrent,
		    	        'amountPrepay'		=> $amountPrepay,
		    	        'pt'				=> $pt,
		    	        'prepay'			=> $prepay,
		    	        'date'				=> Carbon::now()->format('Y-m-d'),
		    	        'msgCurrent'		=> $msgCurrent,
		    	        'returnCodeCurrent'	=> $returnCodeCurrent,
		    	        'returnCodePrepay'	=> $returnCodePrepay,
						'transactionIDCurrent' 	=> $transactionIDCurrent,
		    	        'transactionIDPrepay' 	=> $transactionIDPrepay,
		    	        'errorCurrent'		=> $errorCurrent,
		    	        'errorPrepay'		=> $errorPrepay,
		    	        'flagPaid'			=> $flagPaid,
		    			'statusCurrent'		=> $statusCurrent,
		    	        'cardTypeCurrent'	=> $cardTypeCurrent,
		    			'statusPrepay'		=> $statusPrepay,
		    	        'cardTypePrepay'	=> $cardTypePrepay
					];
    	
    	
    	
    }

    public function createInvoice($orderId,$paymentType,$ref,$flagPaid, $tokenCard = false, $customer = false)
    {
    	
    	Log::info('OrderID: ' . $orderId );
    	Log::info('PaymentType: ' . $paymentType );
    	Log::info('ref: ' . $ref );
    	
        $pt = CataPaymentTypes::find($paymentType);
        $pt = $pt->name;
        
        $items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
        $order = Orders::where('id','=',$orderId)->first();
        $userId = $order->user_id;
        $dataUser = User::with(['UserAddress'])->find($userId);
        
        //
        $paragon = 0;       
        
        if($dataUser->userType_id == 1){
        	//FISCAL RECIPT
        	$paragon = 1;
        }
        
        $paragon = 0;
        
        Log::info('USerid : '. $dataUser->id);
        Log::info("Paragon: ". $paragon);
        
        //
        
        //return [$dataUser];
        // id user is a client, the name is the real name if not, name is a company name
        if($dataUser->userType_id == 1){
          isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
          isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
          $clientName = $userName.' '.$userLast;
        }else{
        //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
          isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
          $clientName = $userName;
        }
        
        //DATOS QUE MUEsTRA EN PANTALLA DE SUCCESS
        $amountCurrent = "";
        $amountPrepay = "";
        
        $transactionIDCurrent = "";
        $transactionIDPrepay = "";
        
        $returnCodeCurrent = "";
        $returnCodePrepay = "";
        
        $errorCurrent = "";
        $errorPrepay = "";
        
        $statusCurrent = "";
        $cardTypeCurrent = "";
        
        $statusPrepay = "";
        $cardTypePrepay = "";
        
        //FIN DE DATOS DE PANTALLA DE SUCCESS
        
        
        // NIP
        $clientNip = $dataUser->nipNumber;
        // Address
        if($dataUser->UserAddress[0]->street != '' ){
          $street = $dataUser->UserAddress[0]->street;
          $numExt = $dataUser->UserAddress[0]->number;
          $numInt = $dataUser->UserAddress[0]->apartmentNumber;
          $city = $dataUser->UserAddress[0]->city;
          $country = $dataUser->UserAddress[0]->country_id;
          $country = Countries::where('id','=',$country)->first();
          $country = $country->name;
          $cp = $dataUser->UserAddress[0]->postCode;
          
          if($numInt != "")
          	$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
          else
          	$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
          
           
        }else{
          $fullAddress = '';
        }        
        
        $subtotal = 0;
        
        //return [$newInvoiceNumberDb];

        //return ['items' => $items];
        $idStorage = 0;

        foreach ($items as $key => $item) {
          if($item->product_type == 'box'){
          	
            $idStorage = $item->product_id;
            
            $prepayMonths = $item->paymentData[0]->prepay;
            $prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
            
            //$desc2 = ($prepayMonths->off / 100);--
            $prepayMonths = $prepayMonths->months;
            
           
            $ins = $item->paymentData[0]->insurance;
           
            $fecha = $item->rent_starts;
            
            
            $st = Storages::find($idStorage);
            
            if ($st == null) {
                return ['success' => false, 'msj' => trans('storages.incomplete_data')];
            }

            $st->flag_rent = 1;
            $st->user_id = $userId;
            //
            $st->rent_start = $fecha;
            $st->st_meses_prepago = $prepayMonths;
            //$st->price_per_month = $totalNextMonth;
            $res = $st->save();
          }
        }
        
        //OBTIENE LA FECHA DE INICIO ... NUEVA LOGICA PARA CReAR TRANSACCIONES Y CARGOS SI ES 1er DIA DE MES
        $firstDayOfMonth = Carbon::parse($st->rent_start)->firstOfMonth()->format('Y-m-d');
        
        $flagPrimerDiaMes = 0;
        
        if($firstDayOfMonth == $st->rent_start && $prepayMonths > 0){
        	$flagPrimerDiaMes = 1;
        }

        $billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userId, 'storage_id' => $idStorage])->first();
        if(!is_null($billingUpdate)){
        	$saldo = $billingUpdate->saldo;
        	$billingUpdate->bi_flag_last = 0;
        	$billingUpdate->save();
        }
        else{
        	$saldo = 0;
        }
        
        
        
        //CALUCLO D SUBTOTAL VAT
        Log::info('***** Calculando totales');
        
        $paymentData = $st->StoragePaymentData->first();
        
        $ins = $paymentData->insurance;
        $vatTag = $paymentData->pd_vat + 1;
        $vat = $paymentData->pd_vat * 100;

        Log::info('Paymentdata ID: '.$paymentData->id);
        Log::info('ins: '.$ins);
        Log::info('vatTag: '.$vatTag);
        Log::info('vat: '.$vat);
        
        //$subtotal = round( $st->price_per_month / $vatTag , 2 );
        //$vatTotal = round( $st->price_per_month - $subtotal , 2 );
        
        $subtotal = 0;
        $vatTotal = 0;
        
        $box = $subtotal - $ins;
        $vatInsurance = round($ins * ($vatTag - 1), 2);
        //$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
        $vatTotalInsurance = 0;
        
        $insuranceBruto = round($ins + $vatInsurance, 2);
         
        $boxBruto = round( $box + $vatTotalInsurance,2);
        
        Log::info('Subtotal: '.$subtotal);
        Log::info('vat total: '.$vatTotal);
        Log::info('box: '. $box);
        Log::info('vatInsurance: '. $vatInsurance);
        Log::info('vatTotalInsurance: '. $vatTotalInsurance);
        Log::info('vat '. $vat );
        Log::info('***** ');
        //FIN DE CALCULO


        $reciboSequence = new ReciboSequence();
        $reciboSequence->save();
         
        $reciboFiscal = str_pad($reciboSequence->id,6,"0",STR_PAD_LEFT);
        $reciboFiscal = "W".$reciboFiscal;
        
        Log::info("Recibo Fiscal: ". $reciboFiscal);
        
        $status_impresion = "PENDIENTE PDF";
        
        //GENERAMOS EL CARGO
	    if($flagPrimerDiaMes == 0){
	    	
	    	Log::info('If.. Primer dia mes = 0');

	    	//Generamos el nombre del pdf
	    	
	    	//FISCAL CHANGES
	    	if($paragon == 0)
	    	{
	    		$invoiceSecuence = new InvoiceSequence();
	    		$invoiceSecuence->is_contador =  $reciboSequence->id;
	    		$invoiceSecuence->save();
	    	}
	    	else{
	    		$invoiceSecuence = new ParagonSequence();
	    		$invoiceSecuence->save();
	    	}
	    	
	    	$amountCurrent = $paymentData->pd_total;
	    	
	        $billing = new Billing();
	        $billing->user_id = $userId;
	        $billing->storage_id = $idStorage;
	        $billing->pay_month = date("Y-m-d");
	        //$billing->flag_payed = $flagPaid;
	        $billing->abono = 0;
	        $billing->cargo = $paymentData->pd_total;
	        $billing->saldo = ($paymentData->pd_total * -1) + $saldo;
	        
	        if($flagPaid == 1){
	        	$billing->payment_type_id = $paymentType;
	        
	        }else{
	        	$billing->payment_type_id = 0;
	        }
	        //$billing->pdf = $pdfName;
	        $billing->reference = '';
	        $billing->bi_flag_last	= 1;
	        $billing->bi_subtotal 			= $paymentData->pd_subtotal;
	        $billing->bi_porcentaje_vat 	= $vat;
	        $billing->bi_total_vat 			= $paymentData->pd_total_vat;
	        $billing->bi_total 				= $paymentData->pd_total;
	        $billing->bi_status_impresion 	= $status_impresion;
	        $billing->bi_recibo_fiscal 		= $reciboSequence->id;
	        $billing->payment_data_id		= $paymentData->id;
	        $billing->bi_number_invoice 	= $invoiceSecuence->id;
	        $billing->bi_year_invoice		= date("y");
	        $billing->bi_batch				= "0";
	        $billing->bi_paragon			= $paragon;
	        $billing->save();
	        
	      
	        
	        //GENERA BILLING DETAIL
	        //BOX
	        $billingDetail = new BillingDetail();
	        $billingDetail->billing_id			= $billing->id;
	        $billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$st->sqm ."m2";
	        $billingDetail->bd_codigo			= "box ".$st->alias;
	        $billingDetail->bd_numero			= "1";
	        $billingDetail->bd_valor_neto		= $paymentData->pd_box_price;
	        $billingDetail->bd_porcentaje_vat 	= $vat;
	        $billingDetail->bd_total_vat		= $paymentData->pd_box_vat;
	        $billingDetail->bd_total			= $paymentData->pd_box_total;
	        $billingDetail->bd_tipo_partida		= "BOX";
	        $billingDetail->save();
	         
	        //INSURANCE
	        if($ins > 0){
	        	Log::info('Registra billing detail de insurance, billing_id: '. $billing->id);
	        	
	        	$billingDetail = new BillingDetail();
	        	$billingDetail->billing_id			= $billing->id;
	        	$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
	        	$billingDetail->bd_codigo			= "ubezpieczenie";
	        	$billingDetail->bd_numero			= "1";
	        	$billingDetail->bd_valor_neto		= $paymentData->pd_insurance_price;
	        	$billingDetail->bd_porcentaje_vat 	= $vat;
	        	$billingDetail->bd_total_vat		= $paymentData->pd_insurance_vat;
	        	$billingDetail->bd_total			= $paymentData->pd_insurance_total;
	        	$billingDetail->bd_tipo_partida		= "INSURANCE";
	        	$billingDetail->save();
	        }
	    }
        //
        /*
        
        */
        //
        Log::info('Saldo : '.$saldo);
        
        //generamos el abono
        if($flagPaid == 1 && $flagPrimerDiaMes == 0){
        	
	        $billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userId, 'storage_id' => $idStorage])->first();
	        
	        if(!is_null($billingUpdate)){
	        	$saldo = $billingUpdate->saldo;
	        	$billingUpdate->bi_flag_last = 0;
	        	$billingUpdate->save();
	        }
	        else{
	        	$saldo = 0;
	        }
	        	
	       	if($customer != ""){
	        	//VA A GENERAR TRANSACCION BRAINTREE
	        	if($prepayMonths > 0 /*&& $cc == 2*/){ //NUEVO
	        		//SI HAY PREPAY NO SUSCRIBE.. PROGRAMA SUSCRIPCION
	        		
	        		$fechaProgramada = Carbon::now()->addMonths($prepayMonths)->startOfMonth();
	        		
	        		Log::info('fechaProgramada: '. $fechaProgramada . ', StorageID: '. $st->id);
	        		
	        		//$st->date_subscribe_braintree	= $fechaProgramada;
	        		$st->save();
	        		
	        		$cardUser = CardUser::where(['cu_token_card'	=> $tokenCard])->first();
	        		
	        		$paymentData->pd_suscription_canceled = 0;
	        		$paymentData->card_user_id 		= $cardUser->id;
	        		$paymentData->save();
	        	}else{
	        		//SUSCRIBE AHORA
	        		
	        		$cardUser = CardUser::where(['cu_token_card'	=> $tokenCard])->first();
	        		
	        		//$subs = BraintreeHelper::createSuscription($tokenCard,env('PLAN_ID'),$paymentData->pd_total_next_month);
	        		$fechaSuscripcion = Carbon::parse($st->rent_start)->startOfMonth()->addMonths(1)->toDateString();
	        		Log::info('Se programa la fecha de suscripcion: '. $fechaSuscripcion); 
	        		$paymentData->card_user_id 		= $cardUser->id;
	        		$paymentData->save();
	        		
	        		$st->date_subscribe_braintree	= $fechaSuscripcion;
	        		$st->save();
	        		/*
	        		$paymentData->pd_suscription_id = $subs['subscription_id'];
	        		$paymentData->pd_suscription_canceled = 0;
	        		*/
	        	}
        
	        	//GENERA TRANSACCION
	        	Log::info('Before transaction, userid: '. $userId);
	        	$trans = BraintreeHelper::createTransaction($tokenCard,$customer,$billing->cargo,$paymentData->order_id,false,$userId);
	        	
	        	if($trans['returnCode'] == 200){
		        	$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
		        	$billing->flag_payed = 1;
		        	$billing->save();
		        	
		        	$transactionIDCurrent = $trans['transaction_id'];
		        	$returnCodeCurrent = 200;
		        	
		        	$statusCurrent = $trans['status'];
		        	$cardTypeCurrent = $trans['cardType'];
		        	
	        	}
	        	else{
	        		$returnCodeCurrent = 100;
	        		$billing->flag_payed = 0; 
	        		$errorCurrent = $trans['msg'];
	        	}
        	}
        	 
        	//FIN DE BRAINTREE
        		
		        $billingA = new Billing();
		        $billingA->user_id 				= $userId;
		        $billingA->storage_id 			= $idStorage;
		        $billingA->pay_month 			= date("Y-m-d");
		        $billingA->flag_payed 			= 1;
		        $billingA->abono 				= $paymentData->pd_total;
		        $billingA->cargo 				= 0;
		        $billingA->saldo 				= $paymentData->pd_total + $saldo;
		        //$billingA->pdf 					= $pdfName;
		        $billingA->reference 			= $ref;
		        $billingA->bi_flag_last			= 1;
		        $billingA->payment_type_id 		= $paymentType;
		        $billingA->payment_data_id		= $paymentData->id;
		        $billingA->bi_paragon			= $paragon;
		        if(isset($trans['transaction_id'])){
		        	 
			        if($customer != ""){
			        	$billingA->bi_transaction_braintree_id	= $trans['transaction_id'];
			        	$billingA->reference	= $trans['transaction_id'];
			        }
		        }
		        $billingA->save();
        	
	       
	        //		        
        }
        
        if($flagPaid == 1 && $paymentType != 2){
        	$returnCodeCurrent = 200;
        	
        	$billing->flag_payed = 1;
        	$billing->save();
        }
        
        // generar el pdf
        //$data = $this->getData();
        $ss = Storage::disk('invoices')->makeDirectory('1');
        //dd($ss);
        
        $totalString = MoneyString::transformQtyCurrency($paymentData->pd_total);
         
        $totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
        
        //OBITIENE MES
        setlocale(LC_ALL, 'de_DE');
    	
    	$year = Carbon::now()->year;
    	
    	$storage = Storages::find($idStorage);
    	$storage->price_per_month = $paymentData->pd_total_next_month;
    	$storage->save();
    	
    	Log::info('Actualiza price per month: Storage_id: '. $idStorage . ", Precio : ". $paymentData->pd_total_next_month);

    	//dd.mm-dd.mm.yyyy
    	$startPeriod = $storage->rent_start;
    	
    	$startPeriod = Carbon::parse($storage->rent_start)->toDateString();
    	$arrStartPeriod = explode('-', $startPeriod);
    	
    	$endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->toDateString();
    	$arrEndPeriod = explode('-',$endPeriod);
    	
    	$period = $arrStartPeriod[2].".".$arrStartPeriod[1]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
    	
    	Log::info('Period: '.$period);
    	
        
        
        // desactivamos la orden
        $order->active = 0;
        $res = $order->save();
        
        //GENERAMOS EL CARGO DEL PREPAY
        if($prepayMonths > 0){
        	
        	Log::info('SE GENERA FACTURA DE PREPAY');
        	Log::info('PrepayMonths: ' . $prepayMonths);
        	
        	//$invoiceSecuence = new InvoiceSequence();
        	//$invoiceSecuence->save();
        	
        	if($paragon == 0)
        	{
        		$invoiceSecuence = new InvoiceSequence();
        		$invoiceSecuence->is_contador =  $reciboSequence->id;
        		$invoiceSecuence->save();
        	}
        	else{
        		$invoiceSecuence = new ParagonSequence();
        		$invoiceSecuence->save();
        	}
        	
        	//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
        	
        	$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
        	$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
        	
        	$startPeriod = Carbon::parse($storage->rent_start)->startOfMonth()->addMonths(1)->toDateString();
        	$arrStartPeriod = explode('-', $startPeriod);
        	 
        	$endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->addMonths($prepayMonths)->toDateString();
        	$arrEndPeriod = explode('-',$endPeriod);
        	 
        	$period = $arrStartPeriod[2].".".$arrStartPeriod[1]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
        	 
        	Log::info('Period prepay: '.$period);
        	
        	
        	$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userId, 'storage_id' => $idStorage])->first();
        	if(!is_null($billingUpdate)){
        		$saldo = $billingUpdate->saldo;
        		$billingUpdate->bi_flag_last = 0;
        		$billingUpdate->save();
        	}
        	else{
        		$saldo = 0;
        	}
        	
        	//GENERAMOS EL CARGO DE PREPAY
        	$amountPrepay = $paymentData->pd_total_prepay;
        	
        	$billing = new Billing();
        	$billing->user_id = $userId;
        	$billing->storage_id = $idStorage;
        	$billing->pay_month = date("Y-m-d");
        	$billing->flag_payed = $flagPaid;
        	$billing->abono = 0;
        	$billing->cargo = $paymentData->pd_total_prepay;
        	$billing->saldo = ($paymentData->pd_total_prepay * -1) + $saldo;
        	//$billing->pdf = $pdfName;
        	$billing->reference = '';
        	$billing->bi_flag_last	= 1;
        	$billing->bi_subtotal 			= $paymentData->pd_subtotal_prepay;
        	$billing->bi_porcentaje_vat 	= $vat;
        	$billing->bi_total_vat 			= $paymentData->pd_total_vat_prepay;
        	$billing->bi_total 				= $paymentData->pd_total_prepay;
        	$billing->bi_status_impresion 	= $status_impresion;
        	$billing->bi_recibo_fiscal 		= $reciboSequence->id;
        	$billing->bi_number_invoice 	= $invoiceSecuence->id;
        	$billing->bi_year_invoice		= date("y");
        	$billing->bi_flag_prepay		= "1";
        	$billing->payment_data_id		= $paymentData->id;
        	$billing->bi_paragon			= $paragon;
        	
        	if($flagPaid == 1){
        		$billing->payment_type_id = $paymentType;
        	
        	}else{
        		$billing->payment_type_id = 0;
        	}
        	
        	$billing->save();
        	
        	Log::info('Billing de prepay: '. $billing->id);
        	
        	
        	//GENERA BILLING DETAIL
        	//BOX
        	$billingDetail = new BillingDetail();
        	$billingDetail->billing_id			= $billing->id;
        	$billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$st->sqm ."m2";
        	$billingDetail->bd_codigo			= "box ".$st->alias;
        	$billingDetail->bd_numero			= "1";
        	$billingDetail->bd_valor_neto		= $paymentData->pd_box_price_prepay;
        	$billingDetail->bd_porcentaje_vat 	= $vat;
        	$billingDetail->bd_total_vat		= $paymentData->pd_box_vat_prepay;
        	$billingDetail->bd_total			= $paymentData->pd_box_total_prepay;
        	$billingDetail->bd_tipo_partida		= "BOX";
        	$billingDetail->save();
        	 
        	//INSURANCE
        	if($ins > 0){
        		Log::info('Registra billing detail de insurance, billing_id: '. $billing->id);
        		 
        		$billingDetail = new BillingDetail();
        		$billingDetail->billing_id			= $billing->id;
        		$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
        		$billingDetail->bd_codigo			= "ubezpieczenie";
        		$billingDetail->bd_numero			= "1";
        		$billingDetail->bd_valor_neto		= $paymentData->pd_insurance_price_prepay;
        		$billingDetail->bd_porcentaje_vat 	= $vat;
        		$billingDetail->bd_total_vat		= $paymentData->pd_insurance_vat_prepay;
        		$billingDetail->bd_total			= $paymentData->pd_insurance_total_prepay;
        		$billingDetail->bd_tipo_partida		= "INSURANCE";
        		$billingDetail->save();
        	}
        	
        	//IF FLAG PAID
        	if($flagPaid == 1){
        		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $userId, 'storage_id' => $idStorage])->first();
        		if(!is_null($billingUpdate)){
        			$saldo = $billingUpdate->saldo;
        			$billingUpdate->bi_flag_last = 0;
        			$billingUpdate->save();
        		}
        		else{
        			$saldo = 0;
        		}
        		
        		//VA A GENERAR TRANSACCION BRAINTREE PARA LE PREPAY
				if($customer != ""){
					
					$fechaProgramada = Carbon::parse($st->rent_start)->addMonths($prepayMonths)->addMonths(1)->startOfMonth();
					 
					Log::info('fechaProgramada: '. $fechaProgramada . ', StorageID: '. $st->id);
					 
					//$st->date_subscribe_braintree	= $fechaProgramada;
					$st->save();
					
        		//GENERA TRANSACCION
	        		$trans = BraintreeHelper::createTransaction($tokenCard,$customer,$billing->cargo,$paymentData->order_id,false,$userId);
	        		
	        		if($trans['returnCode'] == 200){
	        			Log::info('Transaccion correcta en braintree');
	        			$billing->flag_payed = 1;
	        			$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
	        			$billing->save();
	        			
	        			$returnCodePrepay = 200;
	        			
	        			if($flagPrimerDiaMes == 1){
	        				$returnCodeCurrent = 200;
	        			}
	        			
	        			$transactionIDPrepay = $trans['transaction_id'];
	        			$statusPrepay = $trans['status'];
	        			$cardTypePrepay = $trans['cardType'];
	        			
	        		}else{
	        			$msg = $trans['msg'];
	        			$result = $trans['result'];
	        			$billing->flag_payed = 0;
	        			
	        			$returnCodePrepay = 100;
	        			
	        			Log::info('Error al generar la transaccion en braintree');
	        			Log::info('Msg Error: '. $msg);
	        			Log::info('Result Error: '. $result);
	        			
	        			$errorPrepay  = $msg;
	        		}
	        		
				}
        		 
        		//FIN DE BRAINTREE
        		
        		
        		Log::info('El saldo antes de cargod e prepay es: '. $saldo);
        		
        		if(isset($trans['transaction_id'])){
	        		//GENERAMOS EL ABONO DE PREPAY
	        		$billing = new Billing();
	        		$billing->user_id = $userId;
	        		$billing->storage_id = $idStorage;
	        		$billing->pay_month = date("Y-m-d");
	        		$billing->flag_payed = 1;
	        		$billing->abono = $paymentData->pd_total_prepay;
	        		$billing->cargo = 0;
	        		$billing->saldo = $paymentData->pd_total_prepay + $saldo;
	        		//$billing->pdf = $pdfName;
	        		$billing->reference = '';
	        		$billing->bi_flag_last	= 1;
	        		$billing->bi_recibo_fiscal 		= $reciboSequence->id;
	        		$billing->bi_number_invoice 	= $invoiceSecuence->id;
	        		$billing->bi_year_invoice		= date("y");
	        		$billing->bi_flag_prepay		= "1";
	        		$billing->payment_data_id		= $paymentData->id;
	        		$billing->bi_paragon			= $paragon;
	        		if($customer != ""){
	        			
	        			if($trans['returnCode'] == 200){
	        				Log::info('Transaccion correcta en braintree');
	        				 
	        				$billing->reference	= $trans['transaction_id'];
	        				$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
	        			}else{
	        				$msg = $trans['msg'];
	        				$result = $trans['result'];
	        				 
	        				Log::info('Error al generar la transaccion en braintree');
	        				Log::info('Msg Error: '. $msg);
	        				Log::info('Result Error: '. $result);
	        			}
	        			
		        		
		        		
	        		}
	        		if($flagPaid == 1){
	        			$billing->payment_type_id = $paymentType;
	        		
	        		}else{
	        			$billing->payment_type_id = 0;
	        		}
	        		$billing->save();
        		}//END IF
        		
        		
        		if($paymentType != 2){
        			//TEST INSERTA ABONO
        			$billing = new Billing();
        			$billing->user_id = $userId;
        			$billing->storage_id = $idStorage;
        			$billing->pay_month = date("Y-m-d");
        			$billing->flag_payed = 1;
        			$billing->abono = $paymentData->pd_total_prepay;
        			$billing->cargo = 0;
        			$billing->saldo = $paymentData->pd_total_prepay + $saldo;
        			//$billing->pdf = $pdfName;
        			$billing->reference = '';
        			$billing->bi_flag_last	= 1;
        			$billing->bi_recibo_fiscal 		= $reciboSequence->id;
        			$billing->bi_number_invoice 	= $invoiceSecuence->id;
        			$billing->bi_year_invoice		= date("y");
        			$billing->bi_flag_prepay		= "1";
        			$billing->payment_data_id		= $paymentData->id;
        			$billing->bi_paragon			= $paragon;
        			
        			if($flagPaid == 1){
        				$billing->payment_type_id = $paymentType;
        				 
        			}else{
        				$billing->payment_type_id = 0;
        			}
        			$billing->save();
        		}
        		
        	}
        	
        }
        

        //return ['success' => true, 'pdf' => $pdfName];
        
        //return $pdfName;
        //return $billing;
        
        if($prepayMonths > 0){
        	$prepay = 1;
        }else{
        	$prepay = 0;
        }
        
        if($flagPaid == 1){
        	if($returnCodeCurrent == 200){
        		$msgCurrent = "Payment Successful";
        		$msg = "Payment Successful";
        	}
        	else{ 
        		$msgCurrent = "Payment Error";
        		$msg = "Payment Error. Invoice Saved";
        	}
        }
        else{
        	$returnCodeCurrent = 200;
        	$msg = "Saved successful";
        	$msgCurrent = "Saved successful";
        	
        }
        
        if($returnCodeCurrent == 200 && $flagPaid == 1){
        	$msgCurrent = "Payment Successful";
        }
        
        Log::info('flagPaid : ' . $flagPaid);
        Log::info('returnCodeCurrent: '. $returnCodeCurrent);
        Log::info('returnCodePrepay: ' . $returnCodePrepay) ;
        Log::info('paymentType: ' . $paymentType) ;
        
       
        
        return ['returnCode' 			=> 200, 
        			'msg'				=> $msg,
        			'amountCurrent'		=> $amountCurrent,
        			'amountPrepay'		=> $amountPrepay,
        			'pt'				=> $pt,
        			'prepay'			=> $prepay,
        			'date'				=> Carbon::now()->format('Y-m-d'),
        			
        			'msgCurrent'		=> $msgCurrent,
        			
        			'returnCodeCurrent'	=> $returnCodeCurrent, 
        			'returnCodePrepay'	=> $returnCodePrepay,
        		
        			'transactionIDCurrent' 	=> $transactionIDCurrent,
        			'transactionIDPrepay' 	=> $transactionIDPrepay,
        			
        			'errorCurrent'		=> $errorCurrent,
        			'errorPrepay'		=> $errorPrepay,
        			
        			'flagPaid'			=> $flagPaid,
        		
        			'statusCurrent'		=> $statusCurrent,
        			'cardTypeCurrent'	=> $cardTypeCurrent,

	        		'statusPrepay'		=> $statusPrepay,
	        		'cardTypePrepay'	=> $cardTypePrepay
        		
        ];
        

    }

    public function downloadInvoice($var){
      $fileName = $var;
      $filePath = Storage::disk('invoices')->get($var);
      $mimeType = "application/pdf";
      return (new Response($filePath, 200))->header('Content-Type', $mimeType)
      ->header('Content-Disposition', 'attachment; filename="'.$fileName.'"');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pay(Request $request){
    	
    /*	
      isset($request['reference']) ? $ref = $request['reference'] : $ref = '';

      $idBt = 0;
      $tokenPay = uniqid();
      $paymentType = $request['paymentType'];
      $orderId = $request['orderId'];
      $order = Orders::with('OrderDeatil')->where('id','=',$orderId)->first();
      $userId = $order->user_id;
      $stId = $order->OrderDeatil[0]->product_id;
      $dataUser = User::with(['UserAddress'])->where('id','=',$userId)->first();
      $dataPayment = PaymentData::with('paymentPrepay')
                                  ->where('user_id','=',$userId)
                                  ->where('order_id','=',$orderId)
                                  ->first();

      $result = \Braintree_Customer::create([
        'firstName' => $dataUser->name,
        'lastName' => $dataUser->lastName,
        'company' => $dataUser->companyName,
        'email' => $dataUser->email,
        'phone' => $dataUser->phone,
        'fax' => '',
        'website' => '',
        'paymentMethodNonce' => $request['payment_method_nonce']
      ]);

      //dd($result);
      $pmt = $result->customer->creditCards[0]->token;
      //dd($pmt);

      $res = $result->success;
      if ($res) {
        $idBt = $result->customer->id;
        $dataPayment->idBt = $idBt;
        $dataPayment->bt_id_plan = $request['suscriptionId'];
        $dataPayment->token_pay = $tokenPay;
        $res = $dataPayment->save();

        $startPlan = $order->OrderDeatil[0]->rent_starts;
        $utilFecha = new Fecha();

        // si no tiene meses de prepay hago la suscripcion
        if ($dataPayment->paymentPrepay[0]->months == 0) {
          $startPlan = $utilFecha->sumaMeses($startPlan,0);
          //$startPlan = $utilFecha->getPrimerFechaMes($startPlan);
          // se guarda la fecha en que se activa el plan
          $dataPayment->start_plan = $startPlan;
          $dataPayment->flag_plan_active = 1;
          $res = $this->btSuscription($userId,$stId,$tokenPay);

          //sacamos el id:
          $suscriptionId = $res->subscription->_attributes['id'];
          //return ['btSuscriptionData' => $res->subscription->_attributes['id']];
          //sacamos el id de la suscripcion
          $dataPayment->bt_id_suscription = $suscriptionId;
          $resBt = $dataPayment->save();

          if ($res->success != true) {
            return ['success' => false, 'details' => $res];
          }
        }else{
          // si no solo guardo los datos para mas adelante hacer la suscripcion
          //$meses = $dataPayment->paymentPrepay[0]->months + 1;
          $meses = $dataPayment->paymentPrepay[0]->months;
          $startPlan = $utilFecha->sumaMeses($startPlan,$meses);
          //$startPlan = $utilFecha->getPrimerFechaMes($startPlan);

          // Se guarda la fecha del dia que se activara el plan
          $dataPayment->start_plan = $startPlan;
          $dataPayment->flag_plan_active = 0;
        }

      }else{
        return ['success' => false, 'msj' => 'Bt client not created.'];
      }

      // nuevo paymentMethodNonce
      $result = \Braintree_PaymentMethodNonce::create($pmt);
      $nonce = $result->paymentMethodNonce->nonce;

      $result = \Braintree_Transaction::sale([
        'customerId' => $idBt,
        'amount' => $request['amount'],
        'paymentMethodNonce' => $nonce,
        'options' => [
          'submitForSettlement' => True
        ]
      ]);

      if ($result->success) {
        $dataPayment->active = 1;
        $res = $dataPayment->save();
      }else{
        return ['success' => false, 'details' => $result];
      }

      $res = $this->createInvoice($orderId,$paymentType,$ref);
      //dd($res);
      return view('successPay',['pdf' => $res]);
      
      */    	
    	
      //////V2
      
		$orderId = $request['orderId'];
    	$order = Orders::with('OrderDeatil')->where('id','=',$orderId)->first();
    	
    	$userId = $order->user_id;
    	$stId = $order->OrderDeatil[0]->product_id;
    	
    	$dataUser = User::with(['UserAddress'])->where('id','=',$userId)->first();
    	
    	$dataPayment = PaymentData::with('paymentPrepay')
    	->where('user_id','=',$userId)
    	->where('order_id','=',$orderId)
    	->first();
    	
      Log::info('Braintree');
      
      //$customer_id = $this->registerUserOnBrainTree($dataUser->name,$dataUser->lastName,$dataUser->email,$dataUser->phone,$dataUser->companyName);
      Log::info('customer_id: '.$customer_id);
      
      //$card_token = $this->getCardToken($customer_id,$input['cardNumber'],$input['cardExpiry'],$input['cardCVC']);
      //$card_token = $this->getCardToken($customer_id,'378282246310005','02/22','3888');
      
      Log::info('card_token' . $card_token );
      
      /// gateway will provide this plan id whenever you creat plans there
      $plan_id = env('PLAN_ID');
      
      $subscribed = true;
      
      //$transction_id = $this->createTransaction($card_token,$customer_id,$plan_id,$subscribed,$request->get('amount'),$orderId);

    }
    
   
    
    //// for subscription Braintree_WebhookNotification
    /*
    public function subscription(Request $request)
    {
    	Log::info('Payload');
    	Log::info($request->get('bt_payload'));
    	Log::info('******');
    	Log::info('bt_signature');
    	Log::info($request->get('bt_signature'));
    
    	try{
    		if(!is_null($request->get('bt_signature')) && !is_null($request->get('bt_payload'))) {
    
    			$webhookNotification = Braintree_WebhookNotification::parse(
    					$request->get('bt_signature'), $request->get('bt_payload')
    			);
    
    			// $message =
    			// "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
    			// . "Kind: " . $webhookNotification->kind . " | "
    			// . "Subscription: " . $webhookNotification->subscription->id . "\n";Log::info("msg " . Log::info("subscription " . json_encode($webhookNotification->subscription));
    			Log::info("transactions " . json_encode($webhookNotification->subscription->transactions));
    			Log::info("transactions_id " . json_encode($webhookNotification->subscription->transactions[0]->id));
    			Log::info("customer_id " . json_encode($webhookNotification->subscription->transactions[0]->customerDetails->id));
    			Log::info("amount " . json_encode($webhookNotification->subscription->transactions[0]->amount));
    		}
    		else{
    			Log::info('Sin informacion a procesar');
    		}
    	}
    	catch (\Exception $ex) {
    		Log::error("PaymentController::subscription() " . $ex->getMessage());
    	}
    }
    */
    ////////////////FIN BRAINTREE////////////

    public function getStorageInvoices(Request $data)
    {
      $storage = Storages::where('id','=',$data['idSt'])
                          ->where('flag_rent','=',1)->first();

      //return ['user_id' => $storage->user_id];

      $invoices = Billing::where('user_id','=',$storage->user_id)
                          ->where('storage_id','=',$data['idSt'])
                          ->where('cargo','!=',0)
                          ->get();

      return ['success' => true, 'invoices' => $invoices];
    }

    public function payInvoice(Request $data)
    {

      $invoice = Billing::where('id','=',$data['file'])->first();
      $invoice->flag_payed = 1;
      $res = $invoice->save();

      if ($res) {
        return ['success' => true];
      }else{
        return ['success' => false];
      }

    }

    public function markInvoiceAsPayed(Request $data){
      return ['success' => false];
    }


    public function btSuscription($userId,$stId,$tokenPay){

        // obtener suscription id
        $dataPay = PaymentData::where('user_id','=',$userId)
                                ->where('storage_id','=',$stId)
                                ->where('token_pay','=',$tokenPay)
                                ->first();

        $idBt = $dataPay->idBt;
        $suscriptionId = $dataPay->bt_id_plan;
        $customer = \Braintree_Customer::find($idBt);
        $pmt = $customer->creditCards[0]->token;

        // hacer la suscripcion

        $result = \Braintree_Subscription::create([
          'paymentMethodToken' => $pmt,
          'planId' => $suscriptionId,
        ]);

        return $result;

    }

    function saveBtBilling(Request $data){

      if($data->test != true){
        if(isset($data["bt_signature"]) && isset($data["bt_payload"])){
          $webhookNotification = \Braintree_WebhookNotification::parse($data["bt_signature"], $data["bt_payload"]);

          //sacamos el id de la suscripcion para poner el abono en el billing
          $susId = $webhookNotification->subscription->id;
          //$message ="[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] ". "Kind: " . $webhookNotification->kind . " | ";
        }
      }else{
          $webhookNotification = 'Test transaction';
          //sacamos el id de la suscripcion para poner el abono en el billing
          $susId = $data->susId;
      }

        //return ['test' => $data->test, 'hook' => $webhookNotification];
       
        //file_put_contents("/tmp/webhook.log", $message, FILE_APPEND);
        $rb = btRecBilling::create([
                'data' => $webhookNotification,
                'type' => 'pay'
        ]);

        // generar el pdf y guardarlo en la tabla de billing
        

        $dataPay = PaymentData::where('bt_id_suscription','=',$susId)
                              ->first();
        $userId = $dataPay->user_id;
        $idStorage = $dataPay->storage_id;
        $storage = Storages::where('id','=',$idStorage)
                          ->where('user_id','=',$userId)
                          ->where('flag_rent','=',1)->first();
        $granTotal = $storage->price_per_month;
        //hacemos el insert del billing
        //generamos el abono
        $billing = Billing::create([
                'user_id' => $userId,
                'storage_id' => $idStorage,
                'pay_month' => date("Y-m-d"),
                'flag_payed' => 1,
                'abono' => $granTotal,
                'cargo' => 0,
                'saldo' => 0,
                'pdf' => '',
                'reference' => ''
            ]);
        //$res = $this->createInvoiceRecurrentBilling(2,$dataPay->user_id,'xxxxx',$dataPay->storage_id);

        return $res;

    }

    function cancelSuscription(Request $data){
      if(
        isset($data["bt_signature"]) &&
        isset($data["bt_payload"])
      ) {
        $webhookNotification = \Braintree_WebhookNotification::parse(
          $data["bt_signature"], $data["bt_payload"]
        );

        $res = json_encode($webhookNotification);
        
        $rb = btRecBilling::create([
                'data' => $res,
                'type' => 'cancel'
        ]);
      }
    }

    function endStorageRent(Request $data){

      $fecha_hoy = date("Y-m-d");
      // verificamos cada una de las cajas que tengan fecha de term
      $storage = Storages::where(\DB::raw('DATE(`rent_end`)'),'=',$fecha_hoy)
                          ->where('flag_rent','=',1)->get();

      //return[$storage];
      // si la fecha de term es iagual a la fecha actual entonces la limpiamos
      
      foreach ($storage as $st) {
        if ($st->rent_end == $fecha_hoy) {
          // cancelamos la suscripcion a braintree si la tiene
          $dataPay = PaymentData::where('user_id','=',$st->user_id)
                                ->where('storage_id','=',$st->id)
                                ->first();

          $result = \Braintree_Subscription::cancel($dataPay->bt_id_suscription);
          // pasamos los datos a la tabla de historial
          $history = History::create([
                    'storage_id' => $st->id,
                    'user_id' => $st->user_id,
                    'rent_start' => $st->rent_start,
                    'rent_end' => $st->rent_end,
                    'price_per_month' => $st->price_per_month

            ]);

          // limpiamos el storage en la tabla para que este disponible para rentar
          $st->flag_rent = 0;
          $st->user_id = 0;
          $st->final_price = 0;
          $st->rent_start = '0000-00-00';
          $st->rent_end = '0000-00-00';
          $st->price_per_month = 0;
          $res = $st->save();

          if ($res) {
              //enviamos mail o notif de que un storage ha terminado su periodo
              //return ['desactiva' => $history->success];
          }
         

          
        }else{
          //return ['desactiva' => false,'fecha' => $fecha_hoy];
        }
        return ['rent_end' => $st->rent_end,'fecha_hoy' => $fecha_hoy];
      }

    }

    function testBtBilling(Request $data){
      

        $sampleNotification = \Braintree_WebhookTesting::sampleNotification(
            \Braintree_WebhookNotification::SUBSCRIPTION_CHARGED_SUCCESSFULLY,
            '7nbhn6'
        );

        $webhookNotification = \Braintree_WebhookNotification::parse(
            $sampleNotification['bt_signature'],
            $sampleNotification['bt_payload']
        );

        //$webhookNotification->subscription->id;

        $res = json_encode($webhookNotification);

        $rb = btRecBilling::create([
                'data' => $res,
                'type' => 'pay'
        ]);

    }

    public function createInvoiceRecurrentBilling($paymentType,$userId,$ref,$idStorage)
    {
        //return ['msj' => 'create incvoice controller'];
        $pt = CataPaymentTypes::where('id','=',$paymentType)->first();
        $pt = $pt->name;
        $dataUser = User::with(['UserAddress'])->where('id','=',$userId)->first();
        //return [$dataUser];
        // id user is a client, the name is the real name if not, name is a company name
        if($dataUser->userType_id == 1){
          isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
          isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
          $clientName = $userName.' '.$userLast;
        }else{
        //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
          isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
          $clientName = $userName;
        }
        // NIP
        $clientNip = $dataUser->nipNumber;
        // Address
        if($dataUser->UserAddress[0]->street != '' ){
          $street = $dataUser->UserAddress[0]->street;
          $numExt = $dataUser->UserAddress[0]->number;
          $numInt = $dataUser->UserAddress[0]->apartmentNumber;
          $city = $dataUser->UserAddress[0]->city;
          $country = $dataUser->UserAddress[0]->country_id;
          $country = Countries::where('id','=',$country)->first();
          $country = $country->name;
          $cp = $dataUser->UserAddress[0]->postCode;

          if($numInt != "")
    		$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt;
    	  else 
    		$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt;
    				
        }else{
          $fullAddress = '';
        }

        $subtotal = 0;
        $data = ConfigSystem::all();
        $ins = $data[0]->value;
        $vat = $data[1]->value;
        $vatDb = $data[1]->value;
        $vat = $vat / 100;

        $placeDb = $data[2]->value;
        $addressDb = $data[3]->value;
        $nipDb = $data[4]->value;
        $bankDb = $data[5]->value;
        $accountDb = $data[6]->value;
        $invoiceNumberDb = $data[7]->value;
        $seller = $data[8]->value;

        // increments invoice number:
        $invoiceNumberDb = (int)$invoiceNumberDb;
        //return [$invoiceNumberDb];
        $invoiceNumberDb = $invoiceNumberDb + 1;
        //return [$invoiceNumberDb];
        $newInvoiceNumberDb = sprintf( '%04d', $invoiceNumberDb );
        $data[7]->value = $newInvoiceNumberDb;
        $res = $data[7]->save();
        //return ['success' => true, 'invoice' => $newInvoiceNumberDb];

        //Generamos el nombre del pdf
        $pdfName = 'Invoice_'.$newInvoiceNumberDb.'.pdf';

        $storage = Storages::where('id','=',$idStorage)
                          ->where('flag_rent','=',1)->first();

        $subtotal = $storage->price_per_month;
        $vat = $subtotal * $vat;
        $subtotal =  $subtotal - $vat;
        $granTotal = $subtotal + $vat;
        //return ['Subtotal' => $subtotal,'vat' => $vat,'Total' => $granTotal];


        //generamos el abono
        $billing = Billing::create([
                'user_id' => $userId,
                'storage_id' => $idStorage,
                'pay_month' => date("Y-m-d"),
                'flag_payed' => 1,
                'abono' => $granTotal,
                'cargo' => 0,
                'saldo' => 0,
                'pdf' => $pdfName,
                'reference' => $ref
            ]);

        // generar el pdf
        //$data = $this->getData();
        $ss = Storage::disk('invoices')->makeDirectory('1');
        //dd($ss);
        $data =  [
          'insurance' => $ins,
          'vat' => $vat,
          'vatDb' => $vatDb,
          'place' => $placeDb,
          'address' => $addressDb,
          'nip' => $nipDb,
          'bank' => $bankDb,
          'account' => $accountDb,
          'invoiceNumber' => $invoiceNumberDb,
          'seller' => $seller,
          '2y' => date("y"),
          'currentDate' => date("Y-m-d"),
          'paymentType' => $pt,
          'clientName' => $clientName,
          'clientAddress' => $fullAddress,
          'clientNip' => $clientNip,
          'grandTotal' => $granTotal
        ];

        //$date = date('Y-m-d');
        //$invoice = "2222";
        $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //guarda
        $pdf->save(storage_path().'/app/invoices/'.$pdfName);
        //return ['success' => true, 'pdf' => $pdfName];
        return $pdfName;

    }

    function generateChargeBilling(Request $data){
      $utilFecha = new Fecha();
      //$fecha_hoy = date("Y-m-d");
      $fecha_hoy = date("2017-06-30");
      $lastDayOfMonth = $utilFecha->esUltimoDiaMes($fecha_hoy);

      if ($lastDayOfMonth == false) {
        return ['IsLastDay' => false];
      }
      //sacamos todos los storages que esten rentados
      $storage = Storages::with(['StoragePaymentData'=>function($q){
                                $q->where('flag_plan_active','=',1)
                                ->latest();
                              }])
                              ->where('flag_rent','=',1)->get();
      $count = count($storage);

      $dataSt = array();
      foreach ($storage as $st) {
        
        $paymentData = $st->StoragePaymentData->first();
        if ($paymentData->creditcard) {
          $pt = CataPaymentTypes::where('id','=',2)->first();
          $pt = $pt->name;
        }else{
          $pt = CataPaymentTypes::where('id','=',3)->first();
          $pt = $pt->name;
        }

        $dataUser = User::with(['UserAddress'])->where('id','=',$st->user_id)->first();
        //return [$dataUser];
        // id user is a client, the name is the real name if not, name is a company name
        if($dataUser->userType_id == 1){
          isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
          isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
          $clientName = $userName.' '.$userLast;
        }else{
        //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
          isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
          $clientName = $userName;
        }

        // NIP
        $clientNip = $dataUser->nipNumber;
        // Address
        if($dataUser->UserAddress[0]->street != '' ){
          $street = $dataUser->UserAddress[0]->street;
          $numExt = $dataUser->UserAddress[0]->number;
          $numInt = $dataUser->UserAddress[0]->apartmentNumber;
          $city = $dataUser->UserAddress[0]->city;
          $country = $dataUser->UserAddress[0]->country_id;
          $country = Countries::where('id','=',$country)->first();
          $country = $country->name;
          $cp = $dataUser->UserAddress[0]->postCode;

          $fullAddress = $street.' '.$numExt.' '.$numInt.', '.$city.' '.$country.', '.$cp;
        }else{
          $fullAddress = '';
        }
        

        if ($paymentData != null) {
          $activePay = $paymentData->id;

          $data = ConfigSystem::all();
          $ins = $data[0]->value;
          $vat = $data[1]->value;
          $vatDb = $data[1]->value;
          $vat = $vat / 100;

          $placeDb = $data[2]->value;
          $addressDb = $data[3]->value;
          $nipDb = $data[4]->value;
          $bankDb = $data[5]->value;
          $accountDb = $data[6]->value;
          $invoiceNumberDb = $data[7]->value;
          $seller = $data[8]->value;

          // increments invoice number:
          $invoiceNumberDb = (int)$invoiceNumberDb;
          //return [$invoiceNumberDb];
          $invoiceNumberDb = $invoiceNumberDb + 1;
          //return [$invoiceNumberDb];
          $newInvoiceNumberDb = sprintf( '%04d', $invoiceNumberDb );
          $data[7]->value = $newInvoiceNumberDb;
          $res = $data[7]->save();

          $pdfName = 'Invoice_'.$newInvoiceNumberDb.'.pdf';

          //crear el pdf
          $ss = Storage::disk('invoices')->makeDirectory('1');
          //dd($ss);
          $data =  [
            'insurance' => $ins,
            'vat' => $vat,
            'vatDb' => $vatDb,
            'place' => $placeDb,
            'address' => $addressDb,
            'nip' => $nipDb,
            'bank' => $bankDb,
            'account' => $accountDb,
            'invoiceNumber' => $invoiceNumberDb,
            'seller' => $seller,
            '2y' => date("y"),
            'currentDate' => date("Y-m-d"),
            'paymentType' => $pt,
            'clientName' => $clientName,
            'clientAddress' => $fullAddress,
            'clientNip' => $clientNip,
            'grandTotal' => $st->price_per_month
          ];

          $view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
          $pdf = \App::make('dompdf.wrapper');
          $pdf->loadHTML($view);
          $pdf->save(storage_path().'/app/invoices/'.$pdfName);


          $billing = Billing::create([
                'user_id' => $st->user_id,
                'storage_id' => $st->id,
                'pay_month' => date("Y-m-d"),
                'flag_payed' => 0,
                'abono' => 0,
                'cargo' => $st->price_per_month,
                'saldo' => ($st->price_per_month * -1),
                'pdf' => $pdfName,
                'reference' => '',
          ]);
          array_push($dataSt, array("idStorage" =>$st->id,'idDataPay' => $activePay));
        }
        
      }
      

      return['count' => $count, 'fecha' => $fecha_hoy, 'st' => $dataSt];
    }

    function activateBtPlan(Request $data){
      $fecha_hoy = date("Y-m-d");
      $pd = PaymentData::where(\DB::raw('DATE(`start_plan`)'),'=',$fecha_hoy)
                          ->where('idBt','!=','')
                          ->get();

      foreach ($pd as $p) {
        $p->flag_plan_active = 1;
        $res = $this->btSuscription($p->user_id,$p->storage_id,$p->token_pay);
        //sacamos el id:
        $suscriptionId = $res->subscription->_attributes['id'];
        $p->bt_id_suscription = $suscriptionId;
        $resBt = $p->save();
      }
          
      return ['st' => $pd];
    }

    function test(Request $data){
      $utilFecha = new Fecha();
      $res = $utilFecha->sumaMeses($data->fecha,$data->meses);
      return ['Fecha' => $res];
    }
    
    
    
    
    
    
    
    

// end class
}


