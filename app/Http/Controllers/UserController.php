<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\clientAddress;
use App\LegalRepresentative;
use Illuminate\Support\Facades\Session;
use App\Warehouse;
use Yajra\Datatables\Datatables;
use App\Billing;
use App\Library\Formato;
use App\Storages;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Levels;
use Illuminate\Support\Facades\Log;
use NumberToWords\NumberToWords;
use App\Library\MoneyString;
use Illuminate\Support\Facades\DB;
use App\Library\EstimatorHelper;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use App\Library\UtilNotifications;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use Braintree_SubscriptionSearch;
use Braintree_Gateway;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	
	
    public function index($id = false){
    	
//    	$estimator = EstimatorHelper::estimate(231, '2017-09-21', 32, 20, 1);

    	//$paymentMethod = \Braintree_PaymentMethod::find('97shv4');
    	
    	//dd($paymentMethod);
    	
    	//dd($estimator);
    	
    	$fecha = Carbon::parse('2018-01-30')->addMonths(1)->subDays(1)->toDateString();
    	
    	//dd($fecha);
    	
    	$warehouse = Warehouse::where('Active', '!=', '0')->get();
    	
    	/*
    	$clientToken = \Braintree_ClientToken::generate([
    			"customerId" => '693531203'
    	]);
    	
    	dd($clientToken);*/
    	
    	//$result = Braintree_Subscription::cancel('4bsjs6');
    	
    	//dd($result);
    	
    	$data = Levels::join('warehouse','warehouse_id','=','warehouse.id')
    				->select(['levels.id','warehouse.name AS warehousename','levels.name'])
    				->where([
    						'warehouse.Active' 	=> '1',
    						'levels.active'		=> '1'
    				])->get();
    	
    	return view('pw.users',['warehouses' => $data, 'id' => $id]);
    }
    
    public function indexDT(Request $request){
        //TODO IMPORTANTE. OPTIMIZAR EL CODIGO

        $gateway = new Braintree_Gateway([
            'environment' => env('ENVIRONMENT_BRAINTREE'),
            'merchantId' => env('MERCHANT_ID_BRAINTREE'),
            'publicKey' => env('PUBLIC_KEY_BRAINTREE'),
            'privateKey' => env('PRIVATE_KEY_BRAINTREE')
        ]);

        
        $platform = $request->get("platform");

        $filterPlatform = array();

        if($platform == "Web"){
            array_push($filterPlatform, "0");
        }elseif($platform == "App"){
            array_push($filterPlatform, "1","2");
        }else{
            array_push($filterPlatform, "0");
            array_push($filterPlatform, "1");
            array_push($filterPlatform, "2");
        }        
    	
    	if($request->get('chkTenant') == 'true' || $request->get('level') != "" && $request->get('chkNotPaid') == 'false'){
    		if($request->get('level') == "")
    		{
    			//BUSCA LOS TENANTS
    			$users = Storages::where('flag_rent',1)->lists('user_id');
    			
                $clients = User::join('cata_user_types','cata_user_types.id','=','userType_id')
                ->whereIn("movil_dispositive",$filterPlatform)
    			->where('users.id','<>','1')
    			->where('active','<>','0')
    			->where('username', 'LIKE', '%'.$request->get('username').'%')
    			->where('email', 'LIKE', '%'.$request->get('email').'%')
    			->where('users.name', 'LIKE', '%'.$request->get('name').'%')
    			->where('lastName', 'LIKE', '%'.$request->get('last').'%')
    			->where('companyName', 'LIKE', '%'.$request->get('company').'%')
    			->whereIn('users.id',$users)
    			->select('*','cata_user_types.name AS TipoUsuario','users.name AS NombreUsuario', 'users.id AS Userid', 'cata_user_types.id as idTipoUsuario')
    			->get();
    		}else{
    		//if($request->get('level') != ""){
    			//CON NIVEL SELECCIONADO 
    			$users = Storages::where('flag_rent','=',1)->whereIn('level_id',$request->get('level'))->lists('user_id');
    			
                $clients = User::join('cata_user_types','cata_user_types.id','=','userType_id')
                ->whereIn("movil_dispositive",$filterPlatform)
    			->where('users.id','<>','1')
    			->where('active','<>','0')
    			->where('username', 'LIKE', '%'.$request->get('username').'%')
    			->where('email', 'LIKE', '%'.$request->get('email').'%')
    			->where('users.name', 'LIKE', '%'.$request->get('name').'%')
    			->where('lastName', 'LIKE', '%'.$request->get('last').'%')
    			->where('companyName', 'LIKE', '%'.$request->get('company').'%')
    			->whereIn('users.id',$users)
    			->select('*','cata_user_types.name AS TipoUsuario','users.name AS NombreUsuario', 'users.id AS Userid', 'cata_user_types.id as idTipoUsuario')
    			->get();
    		}
    	}
    	else if($request->get('chkWithNotice') == 'true' && $request->get('chkNotPaid') == 'false'){
    		if($request->get('level') == ""){
    			//WITH NOTICE
    			$users = Storages::where('flag_rent',1)
    			->where('rent_end','<>','0000-00-00')
    			->orWhere('rent_end','>',Carbon::now())
    			->lists('user_id');
    			
                $clients = User::join('cata_user_types','cata_user_types.id','=','userType_id')
                ->whereIn("movil_dispositive",$filterPlatform)
    			->where('users.id','<>','1')
    			->where('active','<>','0')
    			->where('username', 'LIKE', '%'.$request->get('username').'%')
    			->where('email', 'LIKE', '%'.$request->get('email').'%')
    			->where('users.name', 'LIKE', '%'.$request->get('name').'%')
    			->where('lastName', 'LIKE', '%'.$request->get('last').'%')
    			->where('companyName', 'LIKE', '%'.$request->get('company').'%')
    			->whereIn('id',$users)
    			->select('*','cata_user_types.name AS TipoUsuario','users.name AS NombreUsuario', 'users.id AS Userid', 'cata_user_types.id as idTipoUsuario')
    			->get();
    		}
    		else{
    		//if($request->get('level') != ""){
    			
    			//WITH NOTICE DEL NIVEL SELECCIONADO
    			$users = Storages::where('flag_rent','=',1)
    					->whereIn('level_id',$request->get('level'))
    					->where('rent_end', '<>', '0000-00-00')
    					->Where('rent_end','>',Carbon::now())
    					->lists('user_id');
    				
                $clients = User::join('cata_user_types','cata_user_types.id','=','userType_id')
                            ->whereIn("movil_dispositive",$filterPlatform)
			    			->where('users.id','<>','1')
			    			->where('active','<>','0')
			    			->where('username', 'LIKE', '%'.$request->get('username').'%')
			    			->where('email', 'LIKE', '%'.$request->get('email').'%')
			    			->where('users.name', 'LIKE', '%'.$request->get('name').'%')
			    			->where('lastName', 'LIKE', '%'.$request->get('last').'%')
			    			->where('companyName', 'LIKE', '%'.$request->get('company').'%')
			    			->whereIn('users.id',$users)
			    			->select('*','cata_user_types.name AS TipoUsuario','users.name AS NombreUsuario', 'users.id AS Userid', 'cata_user_types.id as idTipoUsuario', 'cata_user_types.id as idTipoUsuario')
    						->get();
    		}
		}
    	else{
    		
	    	$clients = User::join('cata_user_types','cata_user_types.id','=','userType_id')
                        ->whereIn("movil_dispositive",$filterPlatform)
                        ->where('users.id','<>','1') ////////
    					->where('active','<>','0')
    					->where('username', 'LIKE', '%'.$request->get('username').'%')
				    	->where('email', 'LIKE', '%'.$request->get('email').'%')
				    	->where('users.name', 'LIKE', '%'.$request->get('name').'%')
				    	->where('lastName', 'LIKE', '%'.$request->get('last').'%')
                        ->where('companyName', 'LIKE', '%'.$request->get('company').'%')
                        //->where('customer_id','>','0')
				    	->select('*','cata_user_types.name AS TipoUsuario','users.name AS NombreUsuario', 'users.id AS Userid', 'cata_user_types.id as idTipoUsuario', 'cata_user_types.id as idTipoUsuario')
				    	->get();
    	}	    	
    	 
    	return Datatables::of($clients)
				->addColumn('actions',function($row){
			   		
			   		$return = '<a href="javascript:void(0);" id="'.$row->Userid.'" level="'.$row->Role_id.'" dir="'.$row->idTipoUsuario.'" class="btn btn-default editClient"><i class="fa fa-pencil" aria-hidden="true"></i> </a>
					        <a href="javascript:void(0);" data-id="'.$row->Userid.'" class="btn btn-warning btnCreditCard"><i class="fa fa-credit-card" aria-hidden="true"></i></a>
			   				<a href="javascript:void(0);" id="btn-client-'.$row->Userid.'" lang="'.$row->Userid.'" dir="'.$row->idTipoUsuario.'" class="btn btn-info btnClientBoxes"><i class="fa fa-inbox" aria-hidden="true"></i></a>
    	 					<a href="javascript:void(0);" id="'.$row->Userid.'" dir="'.$row->idTipoUsuario.'" class="btn btn-primary btnDelClient"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                             ';

                    if($row->movil_dispositive == 0){
                        $return .='<a href="'.route('users.invite_to_app',[$row->Userid]).'" 
                                        class="btn btn-success btn-invite-app" 
                                        title="Invite to APP"><i class="fa fa-mobile" aria-hidden="true"></i>
                                    </a>';
                    }
                             
                    return $return;
			   		
			   	})
			   	->addColumn('user_type',function($row){
			   		isset($row->UserType->name) ? $ut = $row->UserType->name : $ut = '';
			   		return $ut;
			   	})
			   	->addColumn('balance',function($row){
			   		$saldo = Billing::where(['user_id' => $row->Userid, 'bi_flag_last' => 1])->sum('saldo');
			   		
			   		return Formato::formatear('ZLOTY', $saldo);
                })    	
                ->addColumn('platform', function($row){
					if($row->movil_dispositive == "1"){
						return '<i class="fa fa-apple fa-2x" title="iOs User"></i>';
					}elseif($row->movil_dispositive == "2"){
						return '<i class="fa fa-android fa-2x" title="Android User"></i>';
					}else{
						return '<i class="fa fa-globe fa-2x" title="Web User"></i>';
					}
                })
                ->addColumn('braintree_balance', function($row) use ($gateway, $request){
                    
                    if($request->get('chkBTBalance') == 'false' ){
                        return '--';
                    }

                    $balance = 0;
                    
                    if(env('ENVIRONMENT') == "PROD"){
                        $suscriptions = PaymentData::where([
                            'user_id'           => $row->Userid,
                        ])->whereNotNull('pd_suscription_id')->get();

                        foreach($suscriptions as $sus){

                            $btSusc = $gateway->subscription()->find($sus->pd_suscription_id);

                            if(!is_null($btSusc)){
                                $balance += ( $btSusc->balance ) * -1;
                            }
                            
                        }
                    }
                    return Formato::formatear('ZLOTY', $balance);   
                })
    			->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {

    }
    
    public function createCreditCard($id)
    {
    	return view('users.create_credit_card_user',[
    			'user_id'	=> $id
    	]);
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {
        //
        isset($data['email']) ? $mail = $data['email'] : $mail = '';
        isset($data['Role_id']) ? $role = $data['Role_id'] : $role = '';
        isset($data['userType_id']) ? $userType = $data['userType_id'] : $userType = '';
        isset($data['phone']) ? $phone = $data['phone'] : $phone = '';
        isset($data['active']) ? $active = $data['active'] : $active = '';
        isset($data['companyName']) ? $company = $data['companyName'] : $company = '';
        isset($data['nipNumber']) ? $nip = $data['nipNumber'] : $nip = '';
        isset($data['regonNumber']) ? $regon = $data['regonNumber'] : $regon = '';
        isset($data['courtNumber']) ? $courtNumber = $data['courtNumber'] : $courtNumber = '';
        isset($data['courtPlace']) ? $courtPlace = $data['courtPlace'] : $courtPlace = '';
        isset($data['krsNumber']) ? $krsNumber = $data['krsNumber'] : $krsNumber = '';
        isset($data['Legal_person1']) ? $person1 = $data['Legal_person1'] : $person1 = '';
        isset($data['Legal_person2']) ? $person2 = $data['Legal_person2'] : $person2 = '';
        isset($data['peselNumber']) ? $pesel = $data['peselNumber'] : $pesel = '';
        isset($data['idNumber']) ? $idNumber = $data['idNumber'] : $idNumber = '';
        isset($data['name']) ? $name = $data['name'] : $name = '';
        isset($data['lastName']) ? $lastName = $data['lastName'] : $lastName = '';
        isset($data['birthday']) ? $birthday = $data['birthday'] : $birthday = '';
        isset($data['hdfut']) ? $ut = $data['hdfut'] : $ut = '9';
        isset($data['hdfr']) ? $ur = $data['hdfr'] : $ur = '9';
        isset($data['street']) ? $street = $data['street'] : $street = '';
        isset($data['number']) ? $number = $data['number'] : $number = '';
        isset($data['apartmentNumber']) ? $apartment = $data['apartmentNumber'] : $apartment = '';
        isset($data['postCode']) ? $postCode = $data['postCode'] : $postCode = '';
        isset($data['city']) ? $city = $data['city'] : $city = '';
        isset($data['country']) ? $country = $data['country'] : $country = '';
        isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;

        $success = User::create([
            'username' => $data['username'],
            'name' => $name,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'lastName'=> $lastName,
            'companyName'=> $company,
            'peselNumber'=> $pesel,
            'idNumber'=> $idNumber,
            'nipNumber'=> $nip,
            'regonNumber'=> $regon,
            'courtNumber'=> $courtNumber,
            'courtPlace'=> $courtPlace,
            'krsNumber'=> $krsNumber,
            'Legal_person1'=> $person1,
            'Legal_Person2'=> $person2,
            'Role_id'=> '3',
            'userType_id'=> '1',
            'phone'=> $phone,
            'birthday' => $birthday,
            'active' => '1',
            'validate' => '0',
            'remember_token' => str_random(10)
        ]);

        if ($success->id) {
          $address = clientAddress::create([
            'user_id' => $success->id,
            'street' => $street,
            'number' => $number,
            'apartmentNumber' => $apartment,
            'postCode' => $postCode,
            'city' => $city,
            'country' => $country
          ]);
        }


        if($mobile == 1){
          return ['success' => 'true','id' => $success->id];
        }else{
          return $success;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return ['method' => 'show'];
    }
    
    public function checkUsername(Request $request) {
    	Log::info('UserController@checkUsername');
    	$username = $request->get ( 'username' );
    	
    	Log::info('Username :'. $username);
    	
    	// return $idUsuario;
    	
    	$usuario = User::where([
    			'username'	=> $username
    	])->get();
    	
    	//Log::info('Usuario: '.$usuario);
    
    	$valid = false;
    
    	if (isset( $usuario )) {
    		$valid = true;
    	}
    	
    	Log::info('Return : '.$valid);
    	
    	return [
    			'valid' => $valid
    	];
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return ['method' => 'edit'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return ['method' => 'update'];
    }

    public function inviteToAPP($id)
    {
        Log::info("**********************************");
        Log::info("inviteToAPP");
        $userApp = "";

        $user = User::find($id);
            
        if($user->userType_id == 1){
            //PERSON
            $userApp = $user->peselNumber;
        }else{
            //ONE PERSON COMPANY Y COMPANY
            $userApp = $user->nipNumber;
        }

        Log::info("AppUser: ".$userApp);
        
        $password = uniqid();
        $passwordBcrypt = bcrypt($password);

        Log::info("Password: ". $password);
        
        $user = User::find($user->id);
        $user->username = $userApp;
        $user->password	 = $passwordBcrypt;
        $user->save();
            
        if($user->userType_id == 1){
            isset($user->name) ? $userName = $user->name : $userName = '';
            isset($user->lastName) ? $userLast = $user->lastName : $userLast = '';
            $clientName = $userName.' '.$userLast;
        }else{
            //  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
            isset($user->companyName) ? $userName = $user->companyName : $userName = '';
            $clientName = $userName;
        }

        if($userApp == ""){
            return [
                "returnCode"    => 401,
                "msg"           => "App Username empty"
            ];
        }

        $emailData = [
                    'name' 		=> $clientName,
                    'email' 	=> $user->email,
                    'user'		=> $userApp,
                    'pass'		=> $password
            ];
        
        
        //ENVIO DE CORREO
        if(env('ENVIRONMENT') == 'DEV'){

            $resp = Mail::send ( 'emails.email_welcome_tenant', $emailData, function ($message) use ($emailData) {
            
                $message->to ( 'oscar.sanchez@novacloud.mx' );
            
                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( 'Witaj w self storage Przechowamy Wszystko' );
            } );
        }else{
            
            $resp = Mail::send ( 'emails.email_welcome_tenant', $emailData, function ($message) use ($emailData) {
            
                $message->to ( $emailData['email'] );
                $message->bcc ( 'oscar.sanchez@novacloud.mx' );
            
                $message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
                $message->subject ( 'Witaj w self storage Przechowamy Wszystko' );
            } );
        }
        Log::info("**********************************");
        
        return [
            'returnCode'    => 200,
            'msg'           => 'Invitation Sent'
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return ['method' => 'destroy'];
    }
}
