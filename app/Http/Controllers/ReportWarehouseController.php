<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Warehouse;
use App\Storages;

class ReportWarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $storages = Storages::with(['level.warehouse'])->select('level_id', 'sqm')->distinct('sqm')->orderBy('sqm', 'desc')->get();
        // dd($storages[0]->level->warehouse);

        foreach ($storages as $storage) {
            foreach ($storage->level as $key => $level) {
                // dd($storage->level->warehouse);
                $ocupados = 0;
                $totalSqm = 0;
                $warehouse = $storage->level->warehouse_id;
                // dd($warehouse);
                $level_id = $storage->level->id;
                // dd($level_id);
                $sqm = $storage->sqm;
                // dd($storage->sqm);
                $totalSqm = Storages::where('level_id', $level_id)->where('sqm', $sqm)->count();
                // dd($totalSqm);
                $ocupados = Storages::where('level_id', $level_id)->where('flag_rent', 1)->where('sqm', $sqm)->count();
                // dd($ocupados);
                $numberBoxes = \DB::table('warehouse')
                    ->join('levels', 'warehouse.id', '=', 'levels.warehouse_id')
                    ->join('storages', 'levels.id', '=', 'storages.level_id')
                    ->where('warehouse.id', $warehouse)
                    ->where('levels.id', $level_id)
                    ->where('storages.sqm', $sqm)
                    ->select(\DB::raw('count(storages.id) as numberBoxes'))
                    ->first();
                // dd($numberBoxes);
                $numberBoxesRented = \DB::table('warehouse')
                    ->join('levels', 'warehouse.id', '=', 'levels.warehouse_id')
                    ->join('storages', 'levels.id', '=', 'storages.level_id')
                    ->where('warehouse.id', $warehouse)
                    ->where('levels.id', $level_id)
                    ->where('storages.sqm', $sqm)
                    ->where('storages.flag_rent', 1)
                    ->select(\DB::raw('count(storages.id) as numberBoxesRented'))
                    ->first();
                // dd($numberBoxesRented);
                $numberBoxesAvailable = \DB::table('warehouse')
                    ->join('levels', 'warehouse.id', '=', 'levels.warehouse_id')
                    ->join('storages', 'levels.id', '=', 'storages.level_id')
                    ->where('warehouse.id', $warehouse)
                    ->where('levels.id', $level_id)
                    ->where('storages.sqm', $sqm)
                    ->where('storages.flag_rent', 0)
                    ->select(\DB::raw('count(storages.id) as numberBoxesAvailable'))
                    ->first();
                // dd($numberBoxesAvailable);

                $storage->totalSqm=$totalSqm;
                $storage->ocupados=$ocupados;
                $storage->promedio=($ocupados/$totalSqm)*100;
                $storage->numberBoxes=$numberBoxes;
                $storage->numberBoxesRented=$numberBoxesRented;
                $storage->numberBoxesAvailable=$numberBoxesAvailable;
            }
        }
        // dd($storages);
        return view('pw.reports.warehouse_report', ['storages'=>$storages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
