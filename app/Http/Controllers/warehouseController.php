<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Warehouse;

class warehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pw.warehouse');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        //dd($data);
        $success = Warehouse::create([
            'name' => $data['name'],
            'country_id' => $data['country'],
            'address' => $data['address'],
            'Active'=> '1'
        ]);

        if ($success->id) {
        return['success' => true,
                'client' => $data
              ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $column = 'id';
        $data = Warehouse::with(['Country'])->where('id','=',$request['id'])->where('Active', '!=', '0')->first();
        if ($request->ajax()) {
            return['success' => true,
                'building' => $data
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data)
    {
        $column = 'id';
        $build = Warehouse::where($column,'=', $data['id'])->first();

        $build->name = $data['name'];
        $build->country_id = $data['country'];
        $build->address = $data['address'];
        $res =  $build->save();

        if ($data->ajax()) {
            return ['res' => $res];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $column = 'id';
        $data = Warehouse::where($column,'=', $request['id'])->first();

        $data->Active = 0;
        $res = $data->save();
        return ['res'=>$res];
    }

    public function getBuildingtList(){
        $buildings = Warehouse::with(['Country'])->where('Active', '!=', '0')->get();

        $cad = '';
        foreach ($buildings as $key => $build) {

            $cad .= '<tr>';
            $cad .= '<td>'.$build->name.'</td>';
            $cad .= '<td>'.$build->Country->name.'</td>';
            $cad .= '<td>'.$build->address.'</td>';
            $cad .= '<td><a href="javascript:void(0);" id="'.$build->id.'" class="btn btn-default btn-xs editBuild"><i class="fa fa-pencil" aria-hidden="true"></i> </a>';
            $cad .= '<a href="javascript:void(0);" id="'.$build->id.'" class="btn btn-primary btn-xs btnDelBuild"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
            $cad .= '</tr>';

        }

        return $cad;
    }
}
