<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CardUser;
use App\Http\Controllers\Controller;
use App\User;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Log;
use App\Library\BraintreeHelper;

class CardsUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function indexDT(Request $request)
    {
    	$cardsUser = CardUser::where([
    			'user_id'		=> $request->get('user_id'), 
    			'cu_eliminado'	=> false
    	])->get();
    	
    	return Datatables::of($cardsUser)
			    	->addColumn('imageUrl',function($row){
			    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
			    		return '<img src="'.$paymentMethod->imageUrl.'"/>';
			    	})
			    	->addColumn('expirationDate',function($row){
			    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
			    		return $paymentMethod->expirationDate;
			    	})
    				->addColumn('maskedNumber',function($row){
    					$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    					return $paymentMethod->maskedNumber;
    				})
    				->make(true);
    	
    }
    
    public function showCreditCardBraintree($user_id,$storage_id,$paymentid)
    {	
    	return view('users.credit_card_user_braintree_index',[
    			'user_id'	=> $user_id,
    			'storage_id'=> $storage_id,
    			'paymentid'	=> $paymentid
    	]);
    	 
    }
    
    public function showCreditCardBraintreeDT(Request $request)
    {
    	 $cardsUser = CardUser::where([
    			'user_id'		=> $request->get('user_id'), 
    			'cu_eliminado'	=> false
    	])->get();
    	
    	return Datatables::of($cardsUser)
    				->addColumn('select_card',function($row) use($request){
    					$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    					
    					
    					return '<button type="button" class="btn btn-primary btn-xs btnSelectCard"
    								data-user="'.$request->get('user_id').'"
    								data-token="'.$row->cu_token_card.'"
    								data-customer="'.$row->cu_customer_id.'"
    								data-storage="'.$request->get('storage_id').'"
    								data-paymentid="'.$request->get('paymentid').'"
    								data-mask="'.$paymentMethod->maskedNumber.'"		
    								>	
    								'.trans('cart.card_select').'	
    							</button>';
    				})
    				->addColumn('imageUrl',function($row){
    					$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    					return '<img src="'.$paymentMethod->imageUrl.'"/>';
    				})
    				->addColumn('expirationDate',function($row){
    					$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    					return $paymentMethod->expirationDate;
    				})
    				->addColumn('maskedNumber',function($row){
    					$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    					return $paymentMethod->maskedNumber;
    				})
    				->make(true);
    }
    
    
    public function showCreditCardBraintreeDeposits($user_id,$storage_id,$paymentid)
    {
    	return view('users.credit_card_user_braintree_deposits_index',[
    			'user_id'	=> $user_id,
    			'storage_id'=> $storage_id,
    			'paymentid'	=> $paymentid
    	]);
    
    }
    
    public function showCreditCardBraintreeDepositsDT(Request $request)
    {
    	
    	Log::info('showCreditCardBraintreeDepositsDT');
    	Log::info($request->get('user_id'));
    	
    	$cardsUser = CardUser::where([
    			'user_id'		=> $request->get('user_id'),
    			'cu_eliminado'	=> false
    	])->get();
    	 
    	return Datatables::of($cardsUser)
    	->addColumn('select_card',function($row) use($request){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    			
    			
    		return '<button type="button" class="btn btn-primary btn-xs btnSelectCardDeposit"
    								data-id="'.$row->id.'"
    								data-user="'.$request->get('user_id').'"
    								data-token="'.$row->cu_token_card.'"
    								data-customer="'.$row->cu_customer_id.'"
    								data-storage="'.$request->get('storage_id').'"
    								data-paymentid="'.$request->get('paymentid').'"
    								data-mask="'.$paymentMethod->maskedNumber.'"
    								>
    								'.trans('cart.card_select').'
    							</button>';
    	})
    	->addColumn('imageUrl',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return '<img src="'.$paymentMethod->imageUrl.'"/>';
    	})
    	->addColumn('expirationDate',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return $paymentMethod->expirationDate;
    	})
    	->addColumn('maskedNumber',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return $paymentMethod->maskedNumber;
    	})
    	->make(true);
    }

    public function showCreditCardBraintreeInvoices($user_id,$storage_id,$paymentid)
    {
    	return view('users.credit_card_user_braintree_invoices_index',[
    			'user_id'	=> $user_id,
    			'storage_id'=> $storage_id,
    			'paymentid'	=> $paymentid
    	]);
    
    }
    
    public function showCreditCardBraintreeInvoicesDT(Request $request)
    {
    	 
    	Log::info('showCreditCardBraintreeDepositsDT');
    	Log::info($request->get('user_id'));
    	 
    	$cardsUser = CardUser::where([
    			'user_id'		=> $request->get('user_id'),
    			'cu_eliminado'	=> false
    	])->get();
    
    	return Datatables::of($cardsUser)
    	->addColumn('select_card',function($row) use($request){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		 
    		 
    		return '<button type="button" class="btn btn-primary btn-xs btnSelectCardInvoice"
    								data-id="'.$row->id.'"
    								data-user="'.$request->get('user_id').'"
    								data-token="'.$row->cu_token_card.'"
    								data-customer="'.$row->cu_customer_id.'"
    								data-storage="'.$request->get('storage_id').'"
    								data-paymentid="'.$request->get('paymentid').'"
    								data-mask="'.$paymentMethod->maskedNumber.'"
    								>
    								'.trans('cart.card_select').'
    							</button>';
    	})
    	->addColumn('imageUrl',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return '<img src="'.$paymentMethod->imageUrl.'"/>';
    	})
    	->addColumn('expirationDate',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return $paymentMethod->expirationDate;
    	})
    	->addColumn('maskedNumber',function($row){
    		$paymentMethod = \Braintree_PaymentMethod::find($row->cu_token_card);
    		return $paymentMethod->maskedNumber;
    	})
    	->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
    	//$subscribed= false;
    	 
    	//if(isset($input['subscribed']))
    	//{
    	//    		$subscribed= true;
    		//  	}
    		
    	
    	Log::info('CardUserContoller@store');
    	$user = User::find($request->get('user_id'));
    		 
    	
    	//VALIDA SI YA TIENE COSTUMER ID O SI SE GENENRA UNO NUEVO
    	
    	$findCardUser = CardUser::where(['user_id' => $request->get('user_id')])->first();

    	if(!is_null($findCardUser)){
    		$customer_id = $findCardUser->cu_customer_id; 
    	}
    	else{
    		//$customer_id = $this->registerUserOnBrainTree($user->name,$user->lastName,$user->email);
    		$customer_id = BraintreeHelper::registerUserOnBrainTree($user->name  . " " . $user->companyName, $user->lastName, $user->email, '', $request->get('user_id'));
    	}
    	
    	//echo 'customer id - '.$customer_id;/// Create card token
    	Log::info('Customer ID: '.$customer_id);
    	Log::info('payment_method_nonce: '.$request->get('payment_method_nonce'));
    	
    	
    	if($request->get('payment_method_nonce') == ""){
    		return ['returnCode'	=> 100 , 'msg' => trans('messages.complete_form')];
    	}
    	
    	//$card_token = $this->getCardToken($customer_id,$request->get('payment_method_nonce'));
    	$data = BraintreeHelper::getCardToken($customer_id, $request->get('payment_method_nonce'),$request->get('user_id'));
    	
    	
    	if($data['returnCode'] == 200){
	    	$card_token = $data['token'];
	    	Log::info('CardToken: '.$card_token);
	    	//echo 'card_token - '.$card_token;
	    	
	    	/// gateway will provide this plan id whenever you creat plans there
	    	//$plan_id = 'jhrw';
	    	//$transction_id = $this->createTransaction($card_token,$customer_id,$plan_id,$subscribed);
	    	//dd($transction_id);
	    	
	    	$cardUser = new CardUser();
	    	$cardUser->user_id 			= $request->get('user_id');
	    	$cardUser->cu_token_card	= $card_token;
	    	$cardUser->cu_customer_id	= $customer_id;
	    	$cardUser->save();
    	}
    		
		if($request->get('ajax') == "1"){
			Log::info('PETICION POR AJAX');
			
			Log::info('END---CardUserContoller@store');
			return ['returnCode'	=> 200 , 'msg' => trans('messages.card_saved')];
		}else{
			Log::info('END---CardUserContoller@store');
			//return redirect()->route('users');
			return back();
		}
    }
    
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
