<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Billing;
use App\History;
use App\BillingDetail;
use Illuminate\Support\Facades\Log;
use App\ReciboSequence;
use Illuminate\Support\Facades\DB;
use App\Library\EstimatorHelper;
use App\Library\BraintreeHelper;
use App\Library\Formato;
use App\Storages;
use App\ConfigSystem;
use App\ExtraItems;
use App\CataPaymentTypes;
use App\Library\MoneyString;
use App\User;
use App\Countries;
use App\InvoiceSequence;
use App\ParagonSequence;
use DebugBar\Storage\StorageInterface;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\PaymentData;
use App\DetOrders;
use App\Orders;
use App\notificaciones;
use App\CataTerm;
use App\clientAddress;
use App\LegalRepresentative;
use App\CataPrepay;
use App\Http\Fecha;
use App\CardUser;
use App\App;
use App\Library\UtilNotificaciones;
use App\CataSecciones;
use App\Library\BillingHelper;
use App\Contract;

class APIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function getComprobantes(Request $request)
    {
    	/*
    	Log::info('APIController@getComprobantes');
    	
    	$json = json_decode(base64_decode($request->get('ws-payload')));
    	 
    	if(isset($json->idBilling)){
    		
    		Log::info('Se mando el id del billing: ' . $json->idBilling);
    		
    		$billing = Billing::with(['storages','user','billingDetail'])->find($json->idBilling);
    		
    		if($billing->bi_status_impresion == 'PENDIENTE'){
    			return [	"returnCode" 	=> 200,	"billing"	=> $billing];
    		}else{
    			return [	"returnCode" 	=> 300,	"msg"	=> "Has already been printed"];
    		}
    	}
    	else{
    		
    		Log::info('No se mando el id del billing');
    		
	    	$billing = Billing::with(['storages','user','billingDetail'])
	    				->where(['bi_status_impresion' => 'PENDIENTE'])
	    				->where('cargo','>','0')
	    				->first();
	    	
	    	if($billing){
		    	$billing->bi_status_impresion = 'EN PROCESO';
		    	$billing->save();
		    	
		    	$nextBilling = Billing::with(['storages','user'])
						    	->where(['bi_status_impresion' => 'PENDIENTE'])
						    	->where('cargo','>','0')
						    	->first();
	    	}
	    	
	    	if($billing){
	    		return [	"returnCode" 	=> 200,	"billing"	=> $billing, "nextBilling"	=> $nextBilling];
	    	}else{
	    		return [	"returnCode" 	=> 100,	"msg"	=> "Nothing pending"];
    		}
    	}
    	*/
    	
    	$billing = Billing::with(['storages','user','billingDetail'])
    		->where(['bi_status_impresion' => 'PENDIENTE PDF'])
    		->where('cargo','>','0')
    		->first();
    	
    	
    	if($billing){
    		Log::info('BillingID: '.$billing->id);
    		return [	"returnCode" 	=> 200,	"billing"	=> $billing ];
    	}else{
    		Log::info('No hay billing pendiente');
    		return [	"returnCode" 	=> 100,	"msg"	=> "Nothing pending"];
    	}
    	
    }
    

    public function updateComprobantes(Request $request)
    {
    	$json = json_decode(base64_decode($request->get('ws-payload')));
    
    	Log::info('APIController@updateComprobantes');
    	
    
    	$contador = 0;
    
    	foreach ($json as $item){
    		
    		$billing = Billing::find($item);
    		$billing->bi_status_impresion = 'TERMINADO';
    		$billing->save();
    		
    		$contador++;
    	}
    
    	return array('actualizados' => $contador );
    }
    
	public function updateComprobantesErroneos(Request $request)
    {
    	$json = json_decode(base64_decode($request->get('ws-payload')));
    
    	Log::info('APIController@updateComprobantesErroneos');
    	
    
    	$contador = 0;
    
    	foreach ($json as $item){
    		
    		$billing = Billing::find($item);
    		$billing->bi_status_impresion = 'ERROR';
    		$billing->save();
    		
    		$contador++;
    	}
    
    	return array('actualizados' => $contador );
    }
    
    public function updateSequence(Request $request)
    {
    	$json = json_decode(base64_decode($request->get('ws-payload')));
    	
    	$reciboSequence = ReciboSequence::truncate();
    	
    	Log::info('APIController@updateSequence');
    	
    	$secuencia = $json->lastPrintOut + 1;
    	
    	Log:info('Nueva secuencia : '. $secuencia);
    	
    	DB::statement("ALTER TABLE recibos_sequence AUTO_INCREMENT = " . $secuencia );
    	
    	Log::info('Lastprintout: '.$json->lastPrintOut);
    	
    	return [	"returnCode" 	=> 200,	"msg"	=> "Secuencia Actualizada"];
    	 
    }
    
	//public function updateBilling(Request $request)
	public function updateBilling($billing_id)
    {
		$recibo_fiscal = -1;
    	
    	Log::info('APIController@updateBilling');
    	
		//$json = json_decode(base64_decode($request->get('ws-payload')));
		
		//$billing_id = $json->billing_id;
		//$recibo_fiscal = $json->recibo_fiscal + 1;
		
		if(!isset($json->recibo_fiscal)){
			return ['returnCode'	=> 100 , 'msg' 	=> 'Recibo fiscal no recibido'];
		}
		
		Log::info('Billing_id: '.$billing_id);
		Log::info('ReciboFiscal: '.$recibo_fiscal);
		
		$billing = Billing::find($billing_id);
		$billing->bi_recibo_fiscal = $recibo_fiscal;
		$billing->save();
		
		$rentStartBilling = $billing->pay_month;
		
		$flagPaid = $billing->flag_payed; 
		
		//NUMERO QUE SE COLOCA EN EL PDF
		$reciboFiscal = str_pad($recibo_fiscal,6,"0",STR_PAD_LEFT);
		$reciboFiscal = "W".$reciboFiscal;
		
		Log::info($json->recibo_fiscal);
		
		//if($json->recibo_fiscal == -1){
		if($recibo_fiscal == -1){
			$reciboFiscal = "-1";
			
			Log:info('Recibo fiscal -1');
			
		}
		
		$paragon = $billing->bi_paragon;
		
		$encabezado = "";
		if($paragon == 1)
			$encabezado = "PAR";
		else
			$encabezado = "PW";
			
		Log::info("Recibo Fiscal: ". $reciboFiscal);
		
		$dataUser = User::with(['UserAddress'])->find($billing->user_id);
		
		// id user is a client, the name is the real name if not, name is a company name
		if($dataUser->userType_id == 1){
			isset($dataUser->name) ? $userName = $dataUser->name : $userName = '';
			isset($dataUser->lastName) ? $userLast = $dataUser->lastName : $userLast = '';
			$clientName = $userName.' '.$userLast;
		}else{
			//  if( $dataUser->companyName != '' ) { $userName = $dataUser->companyName; }else{ $userName = ''; }
			isset($dataUser->companyName) ? $userName = $dataUser->companyName : $userName = '';
			$clientName = $userName;
		}
		
		Log::info('ClientName: '.$clientName);
		
		//NIP
		$clientNip = $dataUser->nipNumber;
			
		// Address
		if($dataUser->UserAddress[0]->street != '' ){
				
			$street = $dataUser->UserAddress[0]->street;
			$numExt = $dataUser->UserAddress[0]->number;
			$numInt = $dataUser->UserAddress[0]->apartmentNumber;
			$city = $dataUser->UserAddress[0]->city;
			$country = $dataUser->UserAddress[0]->country_id;
			$country = Countries::find($country);
			$country = $country->name;
			$cp = $dataUser->UserAddress[0]->postCode;
				
			if($numInt != "")
				$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt.'/'.$numInt . ', '.$country;
			else
				$fullAddress = $cp.' '.$city. ', '.$street.' '.$numExt. ', '.$country;
				
		}else{
			$fullAddress = '';
		}
		
		//GENERA PDF
		//VALIDA SI ES POR BATCH O ESTIMATOR
		$st = Storages::find($billing->storage_id);
		
		$paymentData = $st->StoragePaymentData->first();
		
		//PAYMENT DATA INFO
		if ($paymentData->creditcard) {
			$pt = CataPaymentTypes::find(2);
			$pt = $pt->cpt_lbl_factura;
		}else{
			$pt = CataPaymentTypes::find(3);
			$pt = $pt->cpt_lbl_factura;
		}
		
		//DATA SYSTEM CONFIG
		$data = ConfigSystem::all();
		//$ins = $data[0]->value;
			
		$ins = $paymentData->insurance;
		$vatTag = $paymentData->pd_vat + 1;
		$vat = $paymentData->pd_vat * 100;
		
		//			
		$activePay = $paymentData->id;
		
		$vatDb = $data[1]->value;
			
		$data = ConfigSystem::all();
		//		
		$placeDb = $data[2]->value;
		$addressDb = $data[3]->value;
		$nipDb = $data[4]->value;
		$bankDb = $data[5]->value;
		$accountDb = $data[6]->value;
		$seller = $data[8]->value;
		//END DATA SYSTEM CONFIG
		

		//OBITIENE MES
		setlocale(LC_ALL, 'de_DE');
		$mes = "";
		
		switch(Carbon::now()->format('m'))
		{
			case '01':
				$mes = 'styczeń';
				break;
			case '02':
				$mes = 'luty';
				break;
			case '03':
				$mes = 'marzec';
				break;
			case '04':
				$mes = 'kwiecień';
				break;
			case '05':
				$mes = 'maj';
				break;
			case '06':
				$mes = 'czerwiec';
				break;
			case '07':
				$mes = 'lipiec';
				break;
			case '08':
				$mes = 'sierpień';
				break;
			case '09':
				$mes = 'wrzesień';
				break;
			case '10':
				$mes = 'październik';
				break;
			case '11':
				$mes = 'listopad';
				break;
			case '12':
				$mes = 'grudzień';
				break;
					
		}
			
		$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
		
		$year = Carbon::now()->year;
		
		if($billing->bi_batch == 1){
			//BATCH
	
			if ($paymentData != null) {
			
				//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
				if($paragon == 1){
					$pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
					$fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
				}else{
					$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
					$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
				}
						 
				Log::info('PDF NAME: '.$pdfName);
				
				//ACTUALIZA NOMBRE DEL PDF
				$billing->pdf  = $pdfName;
				$billing->save();
			
				//crear el pdf
				$ss = Storage::disk('invoices')->makeDirectory('1');
						 
				$totalString = MoneyString::transformQtyCurrency($st->price_per_month);
						 
				$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');

				Log::info('Total String: '.$totalString);
						 
				//CALUCLO D SUBTOTAL VAT
				$subtotal = round( $st->price_per_month / $vatTag , 2 );
				$vatTotal = round( $st->price_per_month - $subtotal , 2 );
						 
				$box = $subtotal - $ins;
				$vatInsurance = round($ins * ($vatTag - 1), 2);
				$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
				$insuranceBruto = round($ins + $vatInsurance, 2);
					 
				$boxBruto = round( $box + $vatTotalInsurance,2);
						 
				Log::info('***** Calculando totales');
				Log::info('Subtotal: '.$subtotal);
				Log::info('vat total: '.$vatTotal);
				Log::info('box: '. $box);
				Log::info('vatInsurance: '. $vatInsurance);
				Log::info('vatTotalInsurance: '. $vatTotalInsurance);
				Log::info('vat '. $vat );
				Log::info('***** ');
				//FIN DE CALCULO
						 
				Log::info("RFISCAL: ".$reciboFiscal);
					$data =  [
							'insurance' => $ins,
							'place' => $placeDb,
							'address' => $addressDb,
							'nip' 				=> $nipDb,
							'bank' 				=> $bankDb,
							'account' 			=> $accountDb,
							'invoiceNumber' 	=> $billing->bi_number_invoice,
							'seller' 			=> $seller,
							'2y' 				=> date("y"),
							'currentDate' 		=> date("Y-m-d"),
							'paymentType' 		=> $pt,
							'clientName' 		=> $clientName,
							'clientAddress' 	=> $fullAddress,
							'clientNip' 		=> $clientNip,
							'clientPesel'		=> $dataUser->peselNumber,
							'clientType'		=> $dataUser->userType_id,
							'grandTotal' 		=> $st->price_per_month,
							'grandTotalString'  => $totalString,
							'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
							'dataUser'			=> $dataUser,
							'mes'				=> $mes,
							'year'				=> $year,
							'storage'			=> $st ,
							'vat'				=> $vat ,
							'box'				=> $box,
							'vatTotalInsurance' => $vatTotalInsurance,
							'subtotal'			=> $subtotal ,
			    	
							'vatTotal'			=> $vatTotal,
							'vatInsurance'		=> $vatInsurance ,
			    	
							'boxBruto'			=> $boxBruto,
							'insuranceBruto'	=> $insuranceBruto,
			   	
							'accountant_number'	=> $dataUser->accountant_number,
							'reciboFiscal'		=> $reciboFiscal,
							'flag_payed'		=> $flagPaid,
							'paragon'			=> $paragon,
							'encabezado'		=> $encabezado,
							'container'			=> $st->st_es_contenedor
					];
						 
			
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
						 
					$pdf = \App::make('dompdf.wrapper');
					$pdf->loadHTML($view);
					$pdf->save(storage_path().'/app/invoices/'.$pdfName);
					
					//GENERA COPIA
					Log::info('Inicia generacion de copia ');
					
					if($paragon == 1){
						$pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					}else{
						$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					}
					//	$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					
						
					$data =  [
							'insurance' => $ins,
							'place' => $placeDb,
							'address' => $addressDb,
							'nip' 				=> $nipDb,
							'bank' 				=> $bankDb,
							'account' 			=> $accountDb,
							'invoiceNumber' 	=> $billing->bi_number_invoice,
							'seller' 			=> $seller,
							'2y' 				=> date("y"),
							'currentDate' 		=> date("Y-m-d"),
							'paymentType' 		=> $pt,
							'clientName' 		=> $clientName,
							'clientAddress' 	=> $fullAddress,
							'clientNip' 		=> $clientNip,
							'clientPesel'		=> $dataUser->peselNumber,
							'clientType'		=> $dataUser->userType_id,
							'grandTotal' 		=> $st->price_per_month,
							'grandTotalString'  => $totalString,
							'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
							'dataUser'			=> $dataUser,
							'mes'				=> $mes,
							'year'				=> $year,
							'storage'			=> $st ,
							'vat'				=> $vat ,
							'box'				=> $box,
							'vatTotalInsurance' => $vatTotalInsurance,
							'subtotal'			=> $subtotal ,
			    	
							'vatTotal'			=> $vatTotal,
							'vatInsurance'		=> $vatInsurance ,
			    	
							'boxBruto'			=> $boxBruto,
							'insuranceBruto'	=> $insuranceBruto,
			   	
							'accountant_number'	=> $dataUser->accountant_number,
							'reciboFiscal'		=> $reciboFiscal,
							'flag_payed'		=> $flagPaid,
							'copy'				=> 1,
							'paragon'			=> $paragon,
							'encabezado'		=> $encabezado,
							'container'			=> $st->st_es_contenedor
					];
						
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
					$pdf = \App::make('dompdf.wrapper');
					$pdf->loadHTML($view);
						
					$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
					
					$billing->bi_status_impresion = 'TERMINADO';
					$billing->save();
						 
						
			}
			//FIN DE BATCH
		}
		else{
			//ESTIMATOR
			
			if($billing->bi_flag_prepay == 0){
				
					$paymentData = PaymentData::find($billing->payment_data_id);
					
					$orderId = $paymentData->order_id;
					$paymentType = $paymentData->payment_type_id;
					
					Log::info('OrderID: ' . $orderId );
					
					$items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$orderId)->get();
					$order = Orders::where('id','=',$orderId)->first();
					$userId = $order->user_id;
					//return [$dataUser];
					// id user is a client, the name is the real name if not, name is a company name
					
					// NIP
					$clientNip = $dataUser->nipNumber;
					
					$subtotal = 0;
					
					//return ['items' => $items];
					$idStorage = 0;
					
					foreach ($items as $key => $item) {
						if($item->product_type == 'box'){
							Log::info('IF');
							 
							$idStorage = $item->product_id;
							$desc3 = 0;
							$nextMonth = 0;
					
							Log::info('idStorage: '.$idStorage);
					
							$term = $item->paymentData[0]->term;
							$term = CataTerm::where('id','=',$term)->first();
							$desc1 = ($term->off / 100);
							$term = $term->months;
							//return [$term,$desc1];
							
							$prepayMonths = $item->paymentData[0]->prepay;
							$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
							$desc2 = ($prepayMonths->off / 100);
							$prepayMonths = $prepayMonths->months;
					
							Log::info('desc2: '.$desc2);
							Log::info('prepayMonths: '.$prepayMonths);
					
							//return [$desc2,$prepayMonths];
					
							$cc = $item->paymentData[0]->creditcard;
							if ($cc == 1) {
								$ext = 1;
							}else{
								$ext = ($item->paymentData[0]->ext /100 ) + 1;
							}
					
							//return [$ext];
					
							$ins = $item->paymentData[0]->insurance;
					
							Log::info('Ins. '.$ins);
					
							//return [$ins];
					
							$fecha = $item->rent_starts;
							$price = $item->price;
							$utilFecha = new Fecha();
							$lastDay = $utilFecha->getUltimoDiaMes($fecha);
							$lastDay = $utilFecha->getDia($lastDay);
							//return [$lastDay];
							$currentDay = $utilFecha->getDia($fecha);
							//return [$lastDay,$currentDay];
							$daysCurrent = $lastDay-$currentDay;
							$pricePerDay = $price / $lastDay;
							//return [$fecha,$lastDay,$daysCurrent,$pricePerDay];
							$totalCurrentMonth = ($daysCurrent + 1) * $pricePerDay;
							//return [$daysCurrent,$price,$pricePerDay,$totalCurrentMonth];
					
							//calculo del current insurance
							$insurencePerDay =  $ins / $lastDay;
							$currentInsurence = $insurencePerDay * ($daysCurrent + 1);
					
							Log::info('insurencePerDay: '.$insurencePerDay);
							Log::info('currentInsurence: '.$currentInsurence);
					
							//caclculo primer descuento:
							$descTermCurrent = $totalCurrentMonth - ($totalCurrentMonth * $desc1);
							$descTermNext = $price - ($price * $desc1);
							//return [$descTermCurrent,$descTermNext];
					
							Log::info('descTermCurrent: '.$descTermCurrent);
							Log::info('descTermNext: '.$descTermNext);
					
							//calculo segundo desc
							$descPrepayCurrent = $descTermCurrent - ($descTermCurrent * $desc2);
							$descPrepayNext = $descTermNext - ($descTermNext * $desc2);
							//return [$descPrepayCurrent,$descPrepayNext];
					
							//calculo tercer desc
							$descCCCurrent = $descPrepayCurrent * $ext;
							$descCCNext = $descPrepayNext * $ext;
							//return [$descCCCurrent,$descCCNext];
							Log::info('$descCCCurrent: '  . $descCCCurrent);
							Log::info('$descCCNext: '. $descCCNext);
								
							//Mes extra antes de impuestos
							//$deposit = $descCCCurrent + $descCCNext;
							$deposit = $descCCNext;
							Log::info('Deposit1: '.$deposit);
							//return [$deposit];
					
							//sumamos el seguro
							//$deposit = $deposit + $ins + $currentInsurence;
							//Log::info('Deposit2: '.$deposit);
							//return [$deposit];
					
							// calculamos los mese de prepago
							$prepayTotal = ($descCCNext * $prepayMonths) + ($ins * $prepayMonths);
							Log::info('PrepayTotal'. $prepayTotal);
							//return [$prepayTotal];
					
							// calculamos el subtotal
							//QUITAMOS EL DEPOSIT
							$total = $prepayTotal; //+ $deposit;
							Log::info('PrepayTotal'. $total);
							//return [$total];
					
							//sacamos el precio total del next
							$subtotalNextMonth = $descCCNext + $ins;
							$vatNextMonth = $subtotalNextMonth * $vat;
							$totalNextMonth = $subtotalNextMonth + $vatNextMonth;
							//return [$totalNextMonth];
					
							$subtotal += $total;
				
					
						}else{
							//Entonces es un item
							$price = $item->price;
							$qt = $item->quantity;
							$art = $item->articleData[0]->nombre;
							$total = $price * $qt;
					
							//return ['Art' => $art,'Price' => $price, 'Quantity' => $qt,'Total' => $total];
							$subtotal += $total;
						}
					}
					
					// El subtotal y el vat se calcula junto con los demas articulos.
					$vat = $subtotal * $vat;
					$granTotal = $subtotal + $vat;
					//return ['Subtotal' => $subtotal,'vat' => $vat,'Total' => $granTotal];
					
					Log::info('***** Info recibida...');
					Log::info('Subtotal: '. $subtotal);
					Log::info('Vat: '.$vat);
					Log::info('granTotal: '. $granTotal);
					
					//Generamos el nombre del pdf
					if($paragon == 1){
						$pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
						$fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
					}else{
						$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
						$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
					}
					
					//CALUCLO D SUBTOTAL VAT
					Log::info('***** Calculando totales');
					
					$paymentData = $st->StoragePaymentData->first();
					
					$ins = $paymentData->insurance;
					$vatTag = $paymentData->pd_vat + 1;
					$vat = $paymentData->pd_vat * 100;
					
					Log::info('Paymentdata ID: '.$paymentData->id);
					Log::info('ins: '.$ins);
					Log::info('vatTag: '.$vatTag);
					Log::info('vat: '.$vat);
					
					
					//$subtotal = round( $st->price_per_month / $vatTag , 2 );
					//$vatTotal = round( $st->price_per_month - $subtotal , 2 );
					
					$subtotal = 0;
					$vatTotal = 0;
					
					 
					$box = $subtotal - $ins;
					$vatInsurance = round($ins * ($vatTag - 1), 2);
					//$vatTotalInsurance = round($vatTotal - $vatInsurance,2);
					$vatTotalInsurance = 0;
					
					$insuranceBruto = round($ins + $vatInsurance, 2);
					 
					$boxBruto = round( $box + $vatTotalInsurance,2);
					
					Log::info('Subtotal: '.$subtotal);
					Log::info('vat total: '.$vatTotal);
					Log::info('box: '. $box);
					Log::info('vatInsurance: '. $vatInsurance);
					Log::info('vatTotalInsurance: '. $vatTotalInsurance);
					Log::info('vat '. $vat );
					Log::info('***** ');
					//FIN DE CALCULO
					
					$billing->pdf = $pdfName;
					$billing->bi_status_impresion = "TERMINADO";
					$billing->save();
					
								
					// generar el pdf
					//$data = $this->getData();
					$ss = Storage::disk('invoices')->makeDirectory('1');
					//dd($ss);
					
					$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total);
					 
					$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
					 			
					
					//dd.mm-dd.mm.yyyy
					//$startPeriod = $rentStartBilling;
					
					//RENT START
					$startPeriod = $st->rent_start;

					$startPeriod = Carbon::parse($startPeriod)->toDateString();
					Log::info('Start period invoicee: '. $startPeriod);
					
					$arrStartPeriod = explode('-', $startPeriod);
					 
					$endPeriod = Carbon::parse($startPeriod)->lastOfMonth()->toDateString();
					Log::info('End period invoice: '. $endPeriod);
					$arrEndPeriod = explode('-',$endPeriod);
					 
					$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
					 
					Log::info('Period: '.$period);
					$year = $arrEndPeriod[0];
					
					switch($arrEndPeriod[1])
					{
						case '01':
							$mes = 'styczeń';
							break;
						case '02':
							$mes = 'luty';
							break;
						case '03':
							$mes = 'marzec';
							break;
						case '04':
							$mes = 'kwiecień';
							break;
						case '05':
							$mes = 'maj';
							break;
						case '06':
							$mes = 'czerwiec';
							break;
						case '07':
							$mes = 'lipiec';
							break;
						case '08':
							$mes = 'sierpień';
							break;
						case '09':
							$mes = 'wrzesień';
							break;
						case '10':
							$mes = 'październik';
							break;
						case '11':
							$mes = 'listopad';
							break;
						case '12':
							$mes = 'grudzień';
							break;
								
					}
						
					$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
					//	
					
					$data =  [
							'insurance' => $paymentData->pd_insurance_price,
							'vat' => $vat,
							'vatDb' => $vatDb,
							'place' => $placeDb,
							'address' => $addressDb,
							'nip' => $nipDb,
							'bank' => $bankDb,
							'account' => $accountDb,
							'invoiceNumber' => $billing->bi_number_invoice,
							'seller' => $seller,
							'2y' => date("y"),
							'currentDate' => "2019-01-07",
							'clientName' => $clientName,
							'clientAddress' => $fullAddress,
							'clientNip' 		=> $clientNip,
							'clientPesel'		=> $dataUser->peselNumber,
							'clientType'		=> $dataUser->userType_id,
							'grandTotal' 		=> $paymentData->pd_total,
							'grandTotalString'	=> $totalString,
							'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
							'mes'				=> $mes,
							'year'				=> $year,
							'storage'			=> $st    ,
							'paymentType' 		=> $pt,	
							'dataUser'			=> $dataUser,
							'vat'				=> $vat ,
					
							'box'				=> $paymentData->pd_box_price,
							'vatTotalInsurance' => $paymentData->pd_box_vat,
							'subtotal'			=> $paymentData->pd_subtotal,
							 
							'vatTotal'			=> $paymentData->pd_total_vat,
							'vatInsurance'		=> $paymentData->pd_insurance_vat,
							 
							'boxBruto'			=> $paymentData->pd_box_total,
							'insuranceBruto'	=> $paymentData->pd_insurance_total,
							 
							'accountant_number'	=> $dataUser->accountant_number,
							'reciboFiscal'		=> $reciboFiscal,
					
							'period'			=> $period,
							'flag_payed'		=> $flagPaid,
							'paragon'			=> $paragon,
							'encabezado'		=> $encabezado,
							'container'			=> $st->st_es_contenedor
					];
					
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
					$pdf = \App::make('dompdf.wrapper');
					$pdf->loadHTML($view);
					
					$pdf->save(storage_path().'/app/invoices/'.$pdfName);
					
					
					//GENERA COPIA
					Log::info('Inicia generacion de copia ');
					
					if($paragon == 1){
						$pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					}else{
						$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					}
					//$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					
					
					Log::info('$billing->user_id: '.$billing->user_id );
					Log::info('$dataUser->userType_id: '. $dataUser->userType_id);
					
					$data =  [
							'insurance' => $paymentData->pd_insurance_price,
							'vat' => $vat,
							'vatDb' => $vatDb,
							'place' => $placeDb,
							'address' => $addressDb,
							'nip' => $nipDb,
							'bank' => $bankDb,
							'account' => $accountDb,
							'invoiceNumber' => $billing->bi_number_invoice,
							'seller' => $seller,
							'2y' => date("y"),
							'currentDate' => "2019-01-07",
							'clientName' => $clientName,
							'clientAddress' => $fullAddress,
							'clientNip' 		=> $clientNip,
							'clientPesel'		=> $dataUser->peselNumber,
							'clientType'		=> $dataUser->userType_id,
							'grandTotal' 		=> $paymentData->pd_total,
							'grandTotalString'	=> $totalString,
							'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
							'mes'				=> $mes,
							'year'				=> $year,
							'storage'			=> $st   ,
							'paymentType' 		=> $pt,
							'dataUser'			=> $dataUser,
							'vat'				=> $vat ,
					
							'box'				=> $paymentData->pd_box_price,
							'vatTotalInsurance' => $paymentData->pd_box_vat,
							'subtotal'			=> $paymentData->pd_subtotal,
							 
							'vatTotal'			=> $paymentData->pd_total_vat,
							'vatInsurance'		=> $paymentData->pd_insurance_vat,
							 
							'boxBruto'			=> $paymentData->pd_box_total,
							'insuranceBruto'	=> $paymentData->pd_insurance_total,
							 
							'accountant_number'	=> $dataUser->accountant_number,
							'reciboFiscal'		=> $reciboFiscal,
					
							'period'			=> $period,
							'flag_payed'		=> $flagPaid,
							'copy'				=> 1,
							'paragon'			=> $paragon,
							'encabezado'		=> $encabezado,
							'container'			=> $st->st_es_contenedor
					];
					
					$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
					$pdf = \App::make('dompdf.wrapper');
					$pdf->loadHTML($view);
					
					$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
					
					//Log::info('Fin de genera copia');
					
					//FIN DE GENERA COPIA
					//return ['success' => true, 'pdf' => $pdfName];
					
					//return $pdfName;

			}else{
				///ES UNA FACTURA DE PREPAY
				
				$paymentData = $st->StoragePaymentData->first();
				
				////////////////
				
				$orderId = $paymentData->order_id;
				$paymentType = $paymentData->payment_type_id;
				
					
				//$pdfName = 'Invoice_'.$invoiceSecuence->id.'.pdf';
				if($paragon == 1){
					$pdfName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
					$fakturaName = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
				}else{
					$pdfName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'.pdf';
					$fakturaName = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number;
				}
					
				Log::info('PDF NAME: '.$pdfName);
				
				//ACTUALIZA NOMBRE DEL PDF
				$billing->pdf  = $pdfName;
				$billing->bi_status_impresion = "TERMINADO";
				$billing->save();
					
				//crear el pdf
				$ss = Storage::disk('invoices')->makeDirectory('1');
				//dd($ss);
				
				$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
					
				$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
				
				$prepayMonths = $paymentData->prepay;
				Log::info('PrepayMonths: ' . $prepayMonths);
				
				$prepayMonths = CataPrepay::where('id','=',$prepayMonths)->first();
				$desc2 = ($prepayMonths->off / 100);
				$prepayMonths = $prepayMonths->months;
				
				//FIX
				$prepayMonths = $st->st_meses_prepago;
					
				$vat = $paymentData->pd_vat * 100;
				
				Log::info('SE GENERA FACTURA DE PREPAY');
				Log::info('PrepayMonths: ' . $prepayMonths);
					
				//$invoiceSecuence = new InvoiceSequence();
				//$invoiceSecuence->save();
					
				//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
					
				//$totalString = MoneyString::transformQtyCurrency($paymentData->pd_total_prepay);
				//$totalString = mb_convert_encoding($totalString, 'HTML-ENTITIES', 'UTF-8');
				
				//APLICA CUANDO ES DESDE ESTIMATOR
				$rentStartBilling = $st->rent_start;
				Log::info('$rentStartBilling: '. $rentStartBilling);
				
				$startPeriod = Carbon::parse($rentStartBilling)->startOfMonth()->addMonths(1)->toDateString();
				Log::info('$startPeriod: '. $startPeriod);
				
				$startMonth = Carbon::parse($st->rent_start)->startOfMonth()->format('Y-m-d');
				Log::info('$startMonth: '. $startMonth);
				
				if($startMonth == $st->rent_start){
					//OJO ES PRIMERO DE MES
					$startPeriod = Carbon::parse($st->rent_start)->startOfMonth()->toDateString();
				}
				
				//SI ES POR ADD PREPAID MONTHS
				if($billing->bi_batch == 2){
					$startPeriod = Carbon::now()->startOfMonth()->addMonths(1)->toDateString();
				}
				
				Log::info('StartPeriod: '. $startPeriod);
				
				$arrStartPeriod = explode('-', $startPeriod);

				$endPeriod = Carbon::parse($startPeriod)->addMonths( $prepayMonths - 1)->toDateString();
				$endPeriod = Carbon::parse($endPeriod)->endOfMonth()->toDateString();
				
				Log::info('endPeriod: '.$endPeriod);
				
				$arrEndPeriod = explode('-',$endPeriod);
					
				$period = $arrStartPeriod[2].".".$arrStartPeriod[1].".".$arrStartPeriod[0]."-".$arrEndPeriod[2].".".$arrEndPeriod[1].".".$arrEndPeriod[0];
				
				$year = $arrEndPeriod[0];
				
				//OJO::: CALCULA EL MES EN QUE ACABA EL PREPAYMENT
				$mes = "";
				
				switch($arrEndPeriod[1])
				{
					case '01':
						$mes = 'styczeń';
						break;
					case '02':
						$mes = 'luty';
						break;
					case '03':
						$mes = 'marzec';
						break;
					case '04':
						$mes = 'kwiecień';
						break;
					case '05':
						$mes = 'maj';
						break;
					case '06':
						$mes = 'czerwiec';
						break;
					case '07':
						$mes = 'lipiec';
						break;
					case '08':
						$mes = 'sierpień';
						break;
					case '09':
						$mes = 'wrzesień';
						break;
					case '10':
						$mes = 'październik';
						break;
					case '11':
						$mes = 'listopad';
						break;
					case '12':
						$mes = 'grudzień';
						break;
							
				}
					
				$mes = mb_convert_encoding($mes, 'HTML-ENTITIES', 'UTF-8');
				//
				
					
				Log::info('Period prepay: '.$period);
				
				//$boxNeto = round($paymentData->pd_box_price_prepay * $prepayMonths,2);
				
				//$suma1 = round($boxNeto + $paymentData->pd_insurance_price_prepay,2);
				//$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
				
				//CALCULA EL VALOR NETO DE LA CAJA
				
				//Log::info('prepayMonths');
				//Log::info($prepayMonths);
				
				 
				
				$cenaNetto = $paymentData->pd_box_price_prepay / $prepayMonths;
				$suma1 = round($paymentData->pd_box_price_prepay + $paymentData->pd_insurance_price_prepay,2);
				$suma2 = round($paymentData->pd_box_vat_prepay + $paymentData->pd_insurance_vat_prepay,2);
				
				$data =  [
						'insurance_neto'	=> $paymentData->insurance,
						'insurance' => $paymentData->pd_insurance_price_prepay,
						'vat' => $vat,
						'vatDb' => $vatDb,
						'place' => $placeDb,
						'address' => $addressDb,
						'nip' => $nipDb,
						'bank' => $bankDb,
						'account' => $accountDb,
						'invoiceNumber' => $billing->bi_number_invoice,
						'seller' => $seller,
						'2y' => date("y"),
						'currentDate' => "2019-01-07",
						'paymentType' => $pt,
						'clientName' => $clientName,
						'clientAddress' => $fullAddress,
						'clientNip' 		=> $clientNip,
						'clientPesel'		=> $dataUser->peselNumber,
						'clientType'		=> $dataUser->userType_id,
						'grandTotal' 		=> $paymentData->pd_total_prepay,
						'grandTotalString'	=> $totalString,
						'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
						'mes'				=> $mes,
						'year'				=> $year,
						'storage'			=> $st    ,
							
						'dataUser'			=> $dataUser,
						'vat'				=> $vat ,
						'cenaNetto'			=> $cenaNetto,
						'box'				=> $paymentData->pd_box_price_prepay,
						'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
						'subtotal'			=> $paymentData->pd_subtotal_prepay,
							
						'vatTotal'			=> $paymentData->pd_total_vat_prepay,
						'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
							
						'boxBruto'			=> $paymentData->pd_box_total_prepay,
						'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
							
						'accountant_number'	=> $dataUser->accountant_number,
						'reciboFiscal'		=> $reciboFiscal,
							
						'period'			=> $period,
						'flag_payed'		=> $flagPaid,
						'mesesPrepago'		=> $prepayMonths,
						'suma1'				=> $suma1,
						'suma2'				=> $suma2,
						'paragon'			=> $paragon,
						'encabezado'		=> $encabezado,
						'container'			=> $st->st_es_contenedor
				];
					
				$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
				$pdf = \App::make('dompdf.wrapper');
				$pdf->loadHTML($view);

				
				$pdf->save(storage_path().'/app/invoices/'.$pdfName);
				
				Log::info('PDF NAME: '.$pdfName);
				Log::info('Finaliza pdf de prepay');
				

				//GENERA COPIA
				Log::info('Inicia generacion de copia ');
					
				if($paragon == 1){
					$pdfNameCopy = date("y")."-PAR-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
				}else{
					$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
				}
				//$pdfNameCopy = date("y")."-PW-".$billing->bi_number_invoice.'_'.$dataUser->accountant_number.'_COPY.pdf';
					
				$data =  [
						'insurance_neto'	=> $paymentData->insurance,
						'insurance' => $paymentData->pd_insurance_price_prepay,
						'vat' => $vat,
						'vatDb' => $vatDb,
						'place' => $placeDb,
						'address' => $addressDb,
						'nip' => $nipDb,
						'bank' => $bankDb,
						'account' => $accountDb,
						'invoiceNumber' => $billing->bi_number_invoice,
						'seller' => $seller,
						'2y' => date("y"),
						'currentDate' => "2019-01-07",
						'paymentType' => $pt,
						'clientName' => $clientName,
						'clientAddress' => $fullAddress,
						'clientNip' 		=> $clientNip,
						'clientPesel'		=> $dataUser->peselNumber,
						'clientType'		=> $dataUser->userType_id,
						'grandTotal' 		=> $paymentData->pd_total_prepay,
						'grandTotalString'	=> $totalString,
						'date'				=> Carbon::now()->addDays(7)->format('Y-m-d'),
						'mes'				=> $mes,
						'year'				=> $year,
						'storage'			=> $st    ,
							
						'dataUser'			=> $dataUser,
						'vat'				=> $vat ,
						'cenaNetto'			=> $cenaNetto,
						'box'				=> $paymentData->pd_box_price_prepay,
						'vatTotalInsurance' => $paymentData->pd_box_vat_prepay,
						'subtotal'			=> $paymentData->pd_subtotal_prepay,
							
						'vatTotal'			=> $paymentData->pd_total_vat_prepay,
						'vatInsurance'		=> $paymentData->pd_insurance_vat_prepay,
							
						'boxBruto'			=> $paymentData->pd_box_total_prepay,
						'insuranceBruto'	=> $paymentData->pd_insurance_total_prepay,
							
						'accountant_number'	=> $dataUser->accountant_number,
						'reciboFiscal'		=> $reciboFiscal,
							
						'period'			=> $period,
						'flag_payed'		=> $flagPaid,
						'mesesPrepago'		=> $prepayMonths , 
						'copy'				=> 1,
						
						'suma1'				=> $suma1,
						'suma2'				=> $suma2,
						'paragon'			=> $paragon,
						'encabezado'		=> $encabezado,
						'container'			=> $st->st_es_contenedor
				];
					
				$view =  \View::make('factura', compact('data', 'date', 'invoice'))->render();
				$pdf = \App::make('dompdf.wrapper');
				$pdf->loadHTML($view);
					
				$pdf->save(storage_path().'/app/invoices/'.$pdfNameCopy);
				
			}
			//FIN DE ESTIMATOR
		}
		
		Log::info('Correo del cliente al que se debe enviar: '. $dataUser->email);
			
		$email2 = $dataUser->email2;
			
		if($email2 == "")
			$email2 = $dataUser->email;

		if($paragon == 1){
			$tituloCorreo = 'Paragon'; 
		}else{
			$tituloCorreo = 'Faktura';
		}
		
		if(env('ENVIRONMENT') == 'DEV'){
			$emailData = [
					'nombreDestinatario' 	=> $clientName,
					/* DEV */
					'email' 				=> 'oscarbar17@gmail.com',
					'email2' 				=> 'oscarbar17@gmail.com',
					'tituloCorreo'			=> $tituloCorreo,
					'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
					'nombrePDF'				=> $pdfName,
					'period'				=> $mes,
					'fakturaName'			=> $fakturaName,
					'paragon'				=> $paragon
			];

			//ENVIO DE CORREO
			$resp = Mail::send ( 'emails.new_billing_email', $emailData, function ($message) use ($emailData, $tituloCorreo) {
				$message->to ( $emailData['email'] );
				$message->cc ( $emailData['email2'] );

				//DEV
				$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
				$message->bcc ( ['myss.latam@gmail.com'] );
				//
					
				$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
				$message->subject ( $tituloCorreo. ' - ' . $emailData['fakturaName'] );
				$message->attach ( $emailData ['pathPDF'], array (
						'as' => $emailData ['nombrePDF'],
						'mime' => 'application/pdf'
				) );
			} );
		}
		else{
			//PROD
			$emailData = [
					'nombreDestinatario' 	=> $clientName,
					/* PROD */
					 
					'email' 				=> $dataUser->email,
					'email2' 				=> $email2,
					
					/* DEV */ 
					/*
					'email' 				=> 'oscar.sanchez@novacloud.mx',
					'email2' 				=> 'abraham.hernandez@gmail.com',
					*/
					'tituloCorreo'			=> $tituloCorreo,
					'pathPDF'				=> storage_path().'/app/invoices/'.$pdfName,
					'nombrePDF'				=> $pdfName,
					'period'				=> $mes,
					'fakturaName'			=> $fakturaName,
					'paragon'				=> $paragon
			];
			

			//ENVIO DE CORREO
			$resp = Mail::send ( 'emails.new_billing_email', $emailData, function ($message) use ($emailData) {
				$message->to ( $emailData['email'] );
				$message->cc ( $emailData['email2'] );
			
				//PROD
				$message->replyTo ( 'biuro@przechowamy-wszystko.pl' );
				$message->bcc ( ['abraham.hernandez@gmail.com','biuro@przechowamy-wszystko.pl','jagiello@jsg-cs.com.pl'] );
				//
					
				$message->from ( 'no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
				$message->subject ( 'Faktura - ' . $emailData['fakturaName'] );
				$message->attach ( $emailData ['pathPDF'], array (
						'as' => $emailData ['nombrePDF'],
						'mime' => 'application/pdf'
				) );
			} );
		}
		
		
    	return ['returnCode'	=> 200 , 'msg' 	=> 'Billing update'];
    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function wsCalculator(Request $request){
    	
    	$json = json_decode($request->get('payload'));
    	
    	$storage1 = Storages::find($json->id_box1);
    	//$storage2 = Storages::find($json->id_box2);
    	
    	Log::info('BOX 1: '. $json->id_box1);
    	//Log::info('BOX 2: '. $json->id_box2);
    	
    	$data1 = EstimatorHelper::estimate($storage1->id, $json->dateStart, 
    								$json->insurance, $json->extraPercentage, 
    								$json->prepay, $json->term , "1", $json->creditCard);
    	/*
    	
    	$data2 = EstimatorHelper::estimate($storage2->id, $json->dateStart, 
    								$json->insurance, $json->extraPercentage, 
    								$json->prepay, $json->term, "1" , $json->creditCard);
    	*/
    	return [
    			'returnCode'	=> 200,
    			'dataBox1' 		=> $data1
    	];
    	 
    }
    
    /*function wsGetStorages(Request $request)
    {
    	$sqm =  $request->get('total');
    	$posible1 = Storages::where('flag_rent','!=',1)->where('sqm', '>=', $sqm)->where('sqm', '<', ($sqm*1.20))->where('price', '>', '0')->orderBy('sqm', 'asc')->get();
    	$num1 = count($posible1);
    	if ($num1 == 0) {
    		return ['success' => false];
    	}
    	
    	$posible2 = Storages::where('flag_rent','!=',1)->where('sqm', '>=', ($sqm*1.20))->where('sqm', '<', ($sqm*1.40))->where('price', '>', '0')->orderBy('sqm', 'asc')->get();
    	$num2 = count($posible2);
    	if ($num2 == 0) {
    		$posible2 = $posible1;
    	}
    	$storage1 = '';
    	$storage2 = '';
    	
    	//return [$sqm];
    	
    	$storages = Storages::where('level_id', '=', $request->get('level'))->where('active', '!=', '0')->get();
    	
    	$settings = ConfigSystem::all();
    	$extraitems = ExtraItems::all();
    	
    	$vat = $settings[1]->value;
    	$insurance = $settings[0]->value;
    	
    	
    	//dd($posible2);
    	
    	
    	return['success' => true,
    			'storage1' => $storage1,
    			'storage2' => $storage2,
    			'storages' => $storages,
    			'pos1' => $posible1,
    			'pos2' => $posible2,
    			'extra-items' => $extraitems,
    			'vat' => $vat,
    			'insurance' => $insurance
    	];
    }*/

    public function wsGetStorages(Request $request){
		$storages = Storages::where('level_id', '=', $request->get('level'))->where('active', '!=', '0')->get();

		return['success' => true,'storages' => $storages];
    }

    function wsUpdateClientInformation(Request $request) {
    	$client = User::where('id', $request->get('id_client'))->first();
    	
    	Log::info('wsUpdateClientInformation');
    	//Log::info($request->all());
    	
    	Log::info('accountant_number: '. $request->get('accountant_number'));
    	Log::info('accountant_code: '. $request->get('accountant_code'));
    	
    	if ($client == null) {
    		return ['returnCode' => 602, 'msg' => 'Client not found'];
		}
		
		Log::info('User type: '. $request->get('userType_id'));

    	if ($request->get('userType_id') != null) {
			$client->userType_id = $request->get('userType_id');
			$client->save();
    	}
    	$client_address = clientAddress::where('user_id', $request->get('id_client'))->first();
    	if ($client_address == null) {
    		$client_address = new clientAddress();
    		$client_address->user_id = $request->get('id_client');
    	}

    	if ($client->userType_id == 1) {
    		$client->username = $request->get('username');
	    	$client->name = $request->get('name');
	    	$client->lastName = $request->get('lastName');
	    	$client->birthday = $request->get('birthday');

	    	$client_address->street = $request->get('street');
	    	$client_address->number = $request->get('number');
	    	$client_address->apartmentNumber = $request->get('apartmentNumber');
	    	$client_address->postCode = $request->get('postCode');
	    	$client_address->city = $request->get('city');
	    	$client_address->country_id = $request->get('country_id');

	    	$client->peselNumber = $request->get('peselNumber');
	    	$client->idNumber = $request->get('idNumber');
	    	$client->phone = $request->get('phone');
	    	$client->phone2 = $request->get('phone2');
	    	$client->email = $request->get('email');
	    	$client->email2 = $request->get('email2');
	    	//$client->accountant_number = $request->get('accountant_number');
	    	//$client->accountant_code = $request->get('accountant_code');

	    	$client_address->save();
	    	$client->us_complete_profile	= 1;
	    	$client->save();
    		
    	}elseif ($client->userType_id == 3) {
			Log::info('***************************************');
			Log::info('User type: Company');
			Log::info('ID Cliente: '.$request->get('id_client'));
    		
    		$legal_representative_1 = LegalRepresentative::where('user_id', $request->get('id_client'))->orderBy('id', 'asc')->first();
    		
    		
    		if(is_null($legal_representative_1)){
				Log::info('Crea legal representative 1');
    			// SI NO EXISTE LO CREA
    			$legal_representative_1 = new LegalRepresentative();
    			$legal_representative_1->user_id = $request->get('id_client');
			}
			Log::info('Name: '. $request->get('name_person_1'));
			Log::info('Lastname:' . $request->get('lastName_person_1'));

    		$legal_representative_1->name = $request->get('name_person_1');
    		$legal_representative_1->lastName = $request->get('lastName_person_1');
    		$legal_representative_1->function = $request->get('function_person_1');
    		$legal_representative_1->phone = $request->get('phone_person_1');
    		$legal_representative_1->email = $request->get('email_person_1');
    		$legal_representative_1->save();
			
			Log::info("Updating... ". $request->get('companyName'));

    		$client->username = $request->get('username');
	    	$client->companyName = $request->get('companyName');

	    	$client_address->street = $request->get('street');
	    	$client_address->number = $request->get('number');
	    	$client_address->apartmentNumber = $request->get('apartmentNumber');
	    	$client_address->postCode = $request->get('postCode');
	    	$client_address->city = $request->get('city');
	    	$client_address->country_id = $request->get('country_id');

			$client->courtNumber = $request->get('courtNumber');
			$client->courtPlace = $request->get('courtPlace');
			$client->krsNumber = $request->get('krsNumber');
			$client->nipNumber = $request->get('nipNumber');
			$client->regonNumber = $request->get('regonNumber');
			
			$legal_representative_2 = LegalRepresentative::where([
					'user_id' => $request->get('id_client')
			])->where('id','<>',$legal_representative_1->id)
			->orderBy('id', 'desc')->first();
			
			
			if($request->get('name_person_2') != ""){
				if(is_null($legal_representative_2) && $request->get('name_person_2') != ""){
					$legal_representative_2 = new LegalRepresentative();
					$legal_representative_2->user_id = $request->get('id_client');

					Log::info('Crea legal representative 2');
				}
				
				Log::info('Name: '. $request->get('name_person_2'));
				Log::info('Lastname:' . $request->get('lastName_person_2'));

				$legal_representative_2->name = $request->get('name_person_2');
				$legal_representative_2->lastName = $request->get('lastName_person_2');
				$legal_representative_2->function = $request->get('function_person_2');
				$legal_representative_2->phone = $request->get('phone_person_2');
				$legal_representative_2->email = $request->get('email_person_2');
				$legal_representative_2->save();
			}
			
			
			Log::info('Phone 1: ' . $request->get('phone'));
			Log::info('Phone 2: ' . $request->get('phone2'));

	    	$client->phone = $request->get('phone');
	    	$client->phone2 = $request->get('phone2');
	    	$client->email = $request->get('email');
	    	$client->email2 = $request->get('email2');
	    	$client->notes = $request->get('notes');
	    	//$client->accountant_number = $request->get('accountant_number');
	    	//$client->accountant_code = $request->get('accountant_code');

	    	
	    	
	    	$client->us_complete_profile	= 1;
	    	
	    	$client_address->save();
    		$client->save();
    	}elseif ($client->userType_id == 2) {
			$client->name = $request->get('name');
			
			Log::info('***************************************');
			Log::info('User type: One Person Company');
			Log::info('ID Cliente: '.$request->get('id_client'));

	    	$client->lastName = $request->get('lastName');
	    	$client->companyName = $request->get('companyName');
	    	$client->birthday = $request->get('birthday');

	    	$client_address->street = $request->get('street');
	    	$client_address->number = $request->get('number');
	    	$client_address->apartmentNumber = $request->get('apartmentNumber');
	    	$client_address->postCode = $request->get('postCode');
	    	$client_address->city = $request->get('city');
	    	$client_address->country_id = $request->get('country_id');

	    	$client->nipNumber = $request->get('nipNumber');
			$client->regonNumber = $request->get('regonNumber');

			Log::info('Phone: '.$request->get('phone'));
			Log::info('Phone2: '.$request->get('phone2'));

	    	$client->phone = $request->get('phone');
	    	$client->phone2 = $request->get('phone2');
	    	$client->email = $request->get('email');
	    	$client->email2 = $request->get('email2');
	    	//$client->accountant_number = $request->get('accountant_number');
	    	//$client->accountant_code = $request->get('accountant_code');
	    	$client->us_complete_profile	= 1;

	    	$client_address->save();
    		$client->save();
    	}

		return ['returnCode' => 200, 'msg' => 'Client Saved'];
    	
    }
    
    function wsClientToken(Request $request){
    	$json = json_decode($request->get('payload'));
    	
    	$user = User::where('id', $json->id_user)->select('customer_id')->first();
    	
    	if(is_null($user->customer_id)){
    		return ['returnCode'	=> '400',
    				'msg'			=> trans('ws.user_not_registered_braintree') ];
    	}else{
    		$clientToken = \Braintree_ClientToken::generate([
    				"customerId" => $user->customer_id
    		]);
    		 
    		return ['returnCode' => 200, 
    				'clientToken'	=> $clientToken ];
    	}
    }

    /*function wsListBoxes (Request $request) {
    	$json = json_decode($request->get('payload'));
    	
    	$current_storages = Storages::where([
    					'user_id'		=> $json->id_user
    				])->get();
    	
    	//[{"id":"","alias":"","balance":"","prepay_month":"","date_start":"","date_end":"","flag_notice":"","flag_subcripcion":"","flag_active":"","posible_notice":"","month_disscount":"","price_disscount":"","recurrent_billing":""}]
		                
    	
    	return ['returnCode'	=> 200, 
    			'current_boxes' => $current_storages,
    			'history_boxes'	=> $history_storages
    	];
    }*/

    function wsListBoxes(Request $request){
    	$json = json_decode($request->get('payload'));
    	
    	$saldo = Billing::where(['user_id' => $json->id_user, 'bi_flag_last' => 1])->sum('saldo');

        $st = Storages::with('StoragePaymentData','StoragePaymentData.paymentTerm')
        			->with(['StorageBilling' => function($a) use ($json) {
                        $a->where(['user_id' => $json->id_user]);
                    }])->where([
                    		'user_id'	=> 	$json->id_user,
                    		'flag_rent'	=> 1
                    ])
                    ->select('*',
                    	DB::raw('(SELECT COALESCE(SUM(saldo),0) FROM billing WHERE user_id = '.$json->id_user.' AND bi_flag_last = 1 AND storage_id = storages.id ) as balance'),
                    	DB::raw('(SELECT COALESCE(de_monto_deposito,0) FROM depositos 
            					WHERE user_id = '.$json->id_user.' AND storage_id = storages.id ) as deposito'),
                    	DB::raw('(SELECT de_regresado FROM depositos WHERE user_id = '.$json->id_user.' AND storage_id = storages.id ) as deposito_regresado'),
                    	DB::raw('(SELECT de_pagado FROM depositos WHERE user_id = '.$json->id_user.' AND storage_id = storages.id ) as deposito_pagado'),
                    	DB::raw('(SELECT id FROM depositos WHERE user_id = '.$json->id_user.' AND storage_id = storages.id ) as id_deposito'),
                    	DB::raw('(SELECT updated_at FROM depositos WHERE user_id = '.$json->id_user.' AND storage_id = storages.id ) as fecha_devolucion_deposito'),
                    	DB::raw(' CASE WHEN rent_end >= rent_start THEN "1" ELSE "0" END AS "giveNotice"'),
                    	DB::raw('(SELECT LAST_DAY ( DATE_ADD( now(), INTERVAL months month ) ) 
                    					FROM cata_term  
                    			WHERE id = (SELECT term FROM payment_data pd WHERE storages.payment_data_id = pd.id)
                    			) as "FechaGiveNotice"')
                   	)->get();
		
        $storages = Storages::where([
        						'user_id'	=> $json->id_user,
        						'flag_rent'	=> 1
        					])->lists('id');

        $billingStorages = Billing::where([
        						'user_id'	=> $json->id_user
        						])->whereNotIn('storage_id',$storages)
        						->with('storages')
        						->select('*',
				                    	DB::raw('(SELECT COALESCE(SUM(saldo),0) FROM billing b WHERE user_id = '.$json->id_user.' AND bi_flag_last = 1 AND b.storage_id = billing.storage_id ) as balance'),
        								
        								DB::raw('(SELECT de_regresado FROM depositos de WHERE user_id = '.$json->id_user.' AND de.storage_id = billing.storage_id ) as deposito_regresado'),
        								DB::raw('(SELECT de_pagado FROM depositos de WHERE user_id = '.$json->id_user.' AND de.storage_id = billing.storage_id ) as deposito_pagado'),
        								DB::raw('(SELECT id FROM depositos de WHERE user_id = '.$json->id_user.' AND de.storage_id = billing.storage_id ) as id_deposito'),
        								DB::raw('(SELECT updated_at FROM depositos de WHERE user_id = '.$json->id_user.' AND de.storage_id = billing.storage_id ) as fecha_devolucion_deposito')
				                   	)
        						->groupBy('storage_id')
        						->get();

        if ($st->count() < 1 && $billingStorages->count() < 1) {
            return [
            		'returnCode' => 601, 'msj' => trans('storages.nothing_to_show')
            ];
        }

        return ['returnCode' => 200, 
        		'current_boxes' => $st , 
        		'history_boxes' => $billingStorages, 
        		'saldo_formateado' => Formato::formatear('ZLOTY', $saldo),
        ];
    }
    
    function wsGetSizes(Request $request){
    	$storages = Storages::distinct()->select('sqm','st_es_contenedor')
    				->where([
    						'active'	 	=> '1',
							'flag_rent'	 	=> '0',
							'st_mostrar_app'=> '1'		
    				])->orderBy('sqm','ASC')->get(); 
    	
    	return ['returnCode'	=> 200, 
    			'boxes' 		=> $storages
    	];
    }
    
    function wsCheckDate(Request $request){
    	$size = $request->get('size');
    	$date = $request->get('date');
    	$contenedor = $request->get('contenedor');
    	
    	Log::info('Size: '. $size);
    	Log::info('Date: '. $date);
    	
    	$boxes = [];

    	//{ "id1" : "", "size1" : "","price1" : "","alias1" : ""},{ "id2" : "", "size2" : "","price2" : "","alias2" : ""}
    	$box1 = Storages::where([
    				'flag_rent'			=> '0' ,
    				'active'			=> '1',
					'st_es_contenedor'	=> $contenedor,
					'st_mostrar_app'	=> '1'
    			])->where('sqm','>=',$size)
    			->select('id AS id1','sqm as sqm1','price as price1','alias as alias1', 'level_id as level1')
    			->orderBy('sqm','ASC')
    			->first();

    	if (is_null($box1)) {
    		
    		Log::info('1st if');
    		
    		return ['returnCode' => 601, 'msj' => 'There are no boxes available with that size, please choose another size.'];
    	}

    	/*
    	$sizeBox1 = $box1->sqm1;
    	
    	$box2 = Storages::where([
		    			'flag_rent'		=> '0' ,
		    			'active'		=> '1'
		    	])->where('sqm','>',$sizeBox1)
		    	->select('id AS id2','sqm AS sqm2','price as price2','alias as alias2', 'level_id as level2')
		    	->orderBy('sqm','ASC')
		    	->first();

		if (is_null($box2)) {
			Log::info('2nd if');
			
    		return ['returnCode' => 601, 'msj' => 'There are no boxes available with that size, please choose another size.'];
    	}
    	*/
    	array_push($boxes,$box1);
    	//array_push($boxes,$box2);
    	
    	return ['returnCode'	=> 200,
    			'boxes' 		=> $boxes
    	];
    }
    
    function wsCataEstimator(Request $request){
    	$data = [];
    	$termnotice = [];
    	$prepay = [];
    	$extraPercentage = [];
    	$insurance = [];
    	
    	$cataTerm = CataTerm::select('id','months','off')->get();
    	array_push($termnotice, $cataTerm);

    	$data['termnotice'] = $termnotice;
    	
    	$cataPrepay = CataPrepay::select('id','months','off')->get();
    	array_push($prepay, $cataPrepay);
    	
    	$data['prepay'] = $prepay;
    	
    	//EXTRA
    	array_push($extraPercentage, "20");
    	$data['extraPercentage'] = $extraPercentage;
    	
    	//INSURANCES
    	$insurance16['id'] = 16; 
    	$insurance16['label'] = 100;
    	
		array_push($insurance, $insurance16);
		/*
		$insurance32['id'] = 32;
		$insurance32['label'] = 200;
		array_push($insurance, $insurance32);
    	
		$insurance48['id'] = 48;
		$insurance48['label'] = 300;
		array_push($insurance, $insurance48);
		
		$insurance64['id'] = 64;
		$insurance64['label'] = 400;
		array_push($insurance, $insurance64);
		
		$insurance80['id'] = 80;
		$insurance80['label'] = 500;
		array_push($insurance, $insurance80);
		*/

		//$insurance0['id'] = 0;
		//$insurance0['label'] = trans('calculator.insurance_0');
		//array_push($insurance, $insurance0);
		
		$data['insurance'] = $insurance;
    	
    	return ['returnCode'	=> 200,
    			'data'			=> $data
    	];
    }
    
    function wsCheckPerfil(Request $request){
    	//$usuario = User::with(['UserAddress','UserLegal'])->find($request->get('id_user'));
    	Log::info('APIController@wsCheckPerfil');
    	
    	$json = json_decode($request->get('payload'));
    	
		Log::info('IDUSER: '.$json->id_user);

    	
    	$usuario = User::with(['UserAddress', 'UserLegal', 'UserAddress.country'])->find($json->id_user);

		Log::info('Usuario: '. $usuario);

    	$saldo = Billing::where(['user_id' => $json->id_user, 'bi_flag_last' => 1])->sum('saldo');
    	
    	if($usuario->us_complete_profile == 0)
    	{
    		return ['returnCode'	=> 100,
    				'user'			=> $usuario,
    				'balance'		=> $saldo,
    				'msg'			=> 'Incomplete Profile'
    		];
    	}else{
    		return ['returnCode'	=> 200,
    				'user'			=> $usuario,
    				'balance'		=> $saldo
    		];
    	}
    	
    }

    function wsLogin(Request $request) {
    	$json = json_decode($request->get('payload'));
		$email = $json->email;
		$pwd = $json->password;
		
		Log::info('Email: '. $email);
		Log::info('Pwd: '. $pwd);
		
		$user = User::where('email', '=', $email)->orWhere('username',$email)->first();

		if($user == null){
			Log::info('Usuario null');
			return ['returnCode' => 601, 'msg' => 'Usuario no Registrado'];
		}else{
			if(\Hash::check($pwd, $user->password)){
				$user->movil_dispositive = $json->movil_dispositive;
				$user->save();
				
				Log::info('Login success');
				
				return ['returnCode' => 200, 'id_user' => $user->id, 'nombre_app' => $user->appName, 'email' => $user->email, 'telefono' => $user->phone, 'facebook_id' => $user->fb_id,'avatar_url' => $user->avatar, 'customer_id' => $user->customer_id];
			}else{
				Log::info('usuario no registrado');
				
				return ['returnCode' => 601, 'msg' => 'Usuario no Registrado'];
			}
		}
    }

    function wsIdFb(Request $request) {
		Log::info("APIController@wsIdFb");
		
		Log::info($request->get('payload'));
		$json = json_decode($request->get('payload'));
		//Log::info($json);
		
		$id_fb = $json->id_fb;
    	$url_avatar = $json->url_avatar;
    	$user = User::where('fb_id', '=', $id_fb)->first();

    	if ($user == null) {
			Log::info("Usuario no registrado");
    		return ['returnCode' => 601, 'msg' => 'Usuario no Registrado'];
    	}else {
			
			Log::info();

    		$user->avatar = $url_avatar;
    		$user->save();

    		return ['returnCode' => 200, 'id_user' => $user->id, 'nombre_app' => $user->appName, 'email' => $user->email, 'telefono' => $user->phone, 'facebook_id' => $user->fb_id, 'avatar_url' => $user->avatar, 'customer_id' => $user->customer_id];
    	}
    }

    function wsGiveNotice(Request $request) {
		$json = json_decode($request->get('payload'));
		
		Log::info("APIController@wsGiveNotice");
		//Log::info($request);

    	$id_user = $json->id_user;
    	$id_box = $json->id_box;
    	
    	Log::info('APIController@wsGiveNotice');
    	Log::info('id_user: '. $id_user);
    	Log::info('id_box: '. $id_user);
    	
    	$storage = Storages::where('id', '=', $id_box)->where('user_id', '=', $id_user)->first();
		$date = Carbon::now()->toDateString();

		$pd = PaymentData::find($storage->payment_data_id);
		$term = CataTerm::find($pd->term);

		$date = Carbon::now()->addMonths($term->months)->endOfMonth()->toDateString();

		Log::info("Fecha de termino: " .$date);
		
	/*
		(SELECT LAST_DAY ( DATE_ADD( now(), INTERVAL months month ) ) 
                    					FROM cata_term  
                    			WHERE id = (SELECT term FROM payment_data pd WHERE storages.payment_data_id = pd.id)
								) as "FechaGiveNotice"
		*/
    	
    	//ENVIA NOTIFICACION AL ADMIN WEB
    	$notificaciones = new notificaciones();
    	$notificaciones->user_id 	= $id_user;
    	$notificaciones->storage_id = $id_box;
    	$notificaciones->notf_mensaje	= 'Give notice';
    	$notificaciones->notf_admin		= '1';
    	$notificaciones->notf_type		= 'GIVE NOTICE';
    	$notificaciones->save();

    	if ($storage == null) {
    		return ['returnCode' => 601 , 'msg'	=> 'No existe el storage'];
    	}else {
    		$storage->rent_end = $date;
    		$storage->save();

    		return ['returnCode' => 200];
    	}
    }
    
    function wsSaveCard(Request $request){

		Log::info("APIController@wsSaveCard");
    	
    	$idUser = $request->get('id_user');
    	
    	$user = User::where('id', $idUser)->select('customer_id', 'name', 'lastName', 'email', 'phone')->first();

    	if (is_null($user->customer_id)) {
			Log::info("User not found: ". $user->id);
    		return ['returnCode' => 100, 'msg' => 'User not found'];
    	}
    	 
    	//VALIDA SI YA TIENE COSTUMER ID O SI SE GENERA UNO NUEVO
    	$findCardUser = CardUser::where(['user_id' => $idUser])->first();
    	
    	if(!is_null($user->customer_id)){
    		$customer_id = $user->customer_id;
    	}else{
    		$customer_id = BraintreeHelper::registerUserOnBrainTree($user->name . " " . $user->companyName,$user->lastName,$user->email,$user->phone,$idUser);
    	}
    	 
    	if($request->get('payment_method_nonce') == ""){
    		return ['returnCode'	=> 100 , 'msg' => trans('ws.invalid_card')];
    	}
    	 
    	$data = BraintreeHelper::getCardToken($customer_id,$request->get('payment_method_nonce'),$idUser);
    	
    	if($data['returnCode'] != "200"){
    		return ['returnCode'	=> 100 , 'msg' => trans('ws.error_card_saved')];
    		
    	}else{
    		$cardUser = new CardUser();
    		$cardUser->user_id 			= $request->get('id_user');
    		$cardUser->cu_token_card	= $data['token'];
    		$cardUser->cu_customer_id	= $customer_id;
    		$cardUser->save();
    		
    		return ['returnCode'	=> 200 , 'msg' => trans('ws.card_saved')];
    	}
    }
    
    public function wsPaySuscribe(Request $request){

    	Log::info('*******************************************');
    	Log::info('***********wsPaySuscribe*******************');
    	Log::info('*******************************************');
    	 
    	$msgRetorno = "";
    	//$json = json_decode($request->get('payload'));
    	 
    	$ppm = $request->get('pricePerMonth');
    	 
    	//$tokenCard = $request->get('token_tarjeta');
    	 
    	$idUser = $request->get('user_id');
    	 
    	$customer = $request->get('customer');
    	$paymentNonce = $request->get('payment_method_nonce');
    	 
    	$storage = Storages::find($request->get('storage_id'));
    	
    	if($storage->flag_rent == 1 && $storage->user_id != $idUser){
    	
    		Log::info('Caja Ocupada');
    		Log::info('StorageID: '. $storage->id);
    		Log::info('StorageUserID: '. $storage->user_id);
    		Log::info('UserID: '. $idUser);
    	
    		return ['returnCode'	=> 610, 'msg'	=> 'Storage not available'];
    	}
    	 
		$paymentData = PaymentData::find($storage->payment_data_id);
		
		$order = Orders::find($paymentData->order_id);
		$order->active= 0;
		$order->save();
    	 
    	//Log::info('APP: PaymentDataID: '. $paymentData->id);
    	 
    	$prepayMonths = CataPrepay::find($request->get('prepay'));
    	$prepayMonths = $prepayMonths->months;
    	
    	//VARIABLE QUE NOS SERVIRA PARA IDENTIFICAR TOKEN CARD
    	$tokenCard = "";
    	
    	$usuario = User::find($idUser);
    	
    	$customer = $usuario->customer_id;
    	
		$bill = BillingHelper::insertaCargos($idUser,$storage->id,$paymentData->id,1,2,$paymentNonce,$customer,1);
		
    	
    	$amountCurrent = $bill['amountCurrent'];
    	$amountPrepay = $bill['amountPrepay'];
    	
    	$transactionIDCurrent = $bill['transactionIDCurrent'];
    	$transactionIDPrepay = $bill['transactionIDPrepay'];
    	
    	$returnCodeCurrent = $bill['returnCodeCurrent'];
    	$returnCodePrepay = $bill['returnCodePrepay'];
    	
    	$errorCurrent = $bill['errorCurrent'];
    	$errorPrepay = $bill['errorPrepay'];
    	
    	$statusCurrent = $bill['statusCurrent'];
    	$cardTypeCurrent = $bill['cardTypeCurrent'];
    	
    	$statusPrepay = $bill['statusPrepay'];
    	$cardTypePrepay = $bill['cardTypePrepay'];
    	   
    	return [
    			'returnCode' => 200  , 'msg' => 'Success' ,
    			'amountCurrent' => $amountCurrent,
    			'amountPrepay' => $amountPrepay,
    			 
    			'transactionIDCurrent' => $transactionIDCurrent,
    			'transactionIDPrepay' => $transactionIDPrepay,
    			 
    			'returnCodeCurrent' => $returnCodeCurrent,
    			'returnCodePrepay' => $returnCodePrepay,
    			 
    			'errorCurrent' => $errorCurrent,
    			'errorPrepay' => $errorPrepay,
    			 
    			'statusCurrent' => $statusCurrent,
    			'cardTypeCurrent' => $cardTypeCurrent,
    			 
    			'statusPrepay' => $statusPrepay,
    			'cardTypePrepay' => $cardTypePrepay
    	];
    	 
    }
    
    
    public function wsPaySuscribeOld(Request $request){
    	//Log::info($request->all());
    	
    	Log::info('*******************************************');
    	Log::info('***********wsPaySuscribe*******************');
    	Log::info('*******************************************');
    	
    	$msgRetorno = "";
    	//$json = json_decode($request->get('payload'));
    	
    	$ppm = $request->get('pricePerMonth');
    	
    	//$tokenCard = $request->get('token_tarjeta');
    	
    	$idUser = $request->get('user_id');
    	
    	$customer = $request->get('customer');
    	$paymentNonce = $request->get('payment_method_nonce');
    	
    	//$tokenCard = $json->tokenTarjeta;
    	//Log::info('Customer: '. $customer);
    	//Log::info('PaymentNonce: '. $paymentNonce);
    	//Log::info('TokenCard: '.$tokenCard);
    	
    	//$cardUser = CardUser::where(['cu_token_card' => $tokenCard ])->first();

    	//if(is_null($tokenCard)){
    		//return ['returnCode' => 100  , 'msg'	=> 'Invalid Card'];
    	//}
    	
    	//$customer = $cardUser->cu_customer_id;
    	
    	$storage = Storages::find($request->get('storage_id'));
    	
    	//Log::info('Storage');
    	//Log::info($storage->all());
    	
    	if($storage->flag_rent == 1 && $storage->user_id != $idUser){
    		
    		Log::info('Caja Ocupada');
    		Log::info('StorageID: '. $storage->id);
    		Log::info('StorageUserID: '. $storage->user_id);
    		Log::info('UserID: '. $idUser);
    		
    		return ['returnCode'	=> 610, 'msg'	=> 'Storage not available'];
    	}
    	
    	$paymentData = PaymentData::find($storage->payment_data_id);
    	
    	//Log::info('APP: PaymentDataID: '. $paymentData->id);
    	
    	$prepayMonths = CataPrepay::find($request->get('prepay'));
    	$prepayMonths = $prepayMonths->months; 

    	//VARIABLE QUE NOS SERVIRA PARA IDENTIFICAR TOKEN CARD
    	$tokenCard = "";   	

    	$usuario = User::find($idUser);
    	 
    	$customer = $usuario->customer_id;
    	//Log::info('Card User: '.$cardUser);
    	
    	$paragon = 0;
    	
    	if($usuario->userType_id == 1){
    		//GENERA FISCAL RECIPT
    		$paragon = 1;
    	}
    	
		//DEFINE VARIABLES DE RETORNO
		$amountCurrent = "";
		$amountPrepay = "";
		
		$transactionIDCurrent = "";
		$transactionIDPrepay = "";
		
		$returnCodeCurrent = "";
		$returnCodePrepay = "";
		
		$errorCurrent = "";
		$errorPrepay = "";
		
		$statusCurrent = "";
		$cardTypeCurrent = "";
		
		$statusPrepay = "";
		$cardTypePrepay = "";
		 
		if($customer == ""){
			return ['returnCode'	=> 600 , 'Invalid customer'];
		}

		//ACTUALIZACION...PRIMERO QUE NADA HACE EL PAGO
		//VA A GENERAR TRANSACCION BRAINTREE

		//GENERA TRANSACCION
		//Log::info('Generara transaccion en braintree');
			
		Log::info('******');
		Log::info('******');
		Log::info('---------------------------------');
		$trans = BraintreeHelper::createTransaction($paymentNonce,$customer,round($request->get('pd_total'),2),$paymentData->order_id,1,$idUser);
			
		//Log::info('trans');
		//Log::info($trans);
				 
		if($trans['returnCode'] == 200){
			
			Log::info('Transaction Current month success');
			Log::info('Amount: '.round($request->get('pd_total'),2));
				
			//Log::info('Transaccion correcta en braintree');
			//Log::info('Continua en el WS');
				
			$cardUser = CardUser::where(['cu_customer_id' => $customer, 'cu_token_card' => $trans['paymentMethodToken']])->first();
				
			if(is_null($cardUser)){
				//SI NO EXISTE LA TARJETA LA REGISTRA
				//Log::info('Registra CardUser en transaccion');
				$cardUser = new CardUser();
				$cardUser->cu_customer_id 	= $customer;
				$cardUser->cu_token_card 	= $trans['paymentMethodToken'];
				$cardUser->user_id			= $request->get('user_id');
				$cardUser->save();
					 
				$tokenCard = $trans['paymentMethodToken'];
					
				Log::info('Register Card User: '. $tokenCard);
					
			}else{
				$tokenCard = $cardUser->cu_token_card;
				
				//Log::info('Already Registered Card: '. $tokenCard);
			}

			//SET VARIABLES RETORNO
			/***AQUI VOY****/
			/***AQUI VOY****/
			/***AQUI VOY****/
			
			$amountCurrent = round($request->get('pd_total'),2);
			$transactionIDCurrent = $trans['transaction_id'];
			$returnCodeCurrent = 200;
			$errorCurrent = "";
			$statusCurrent = $trans['status'];
			$cardTypeCurrent = $trans['cardType'];
			
			Log::info('---------------------------------');
		}else{
			$msg = $trans['msg'];
			$result = $trans['result'];

			Log::info('Transaction Current month ERROR');
			Log::info('Msg Error: '. $msg);
			Log::info('Result Error: '. $result);

			//$billing->flag_payed = 0;
	
			$returnCodeCurrent = 400;
			$errorCurrent = $trans['msg'];
			$statusCurrent = "";
			$cardTypeCurrent = "";
				
			Log::info('---------------------------------');
				
			return ['returnCodeCurrent'	=> 100	, 'errorCurrent'	=> $errorCurrent];
		}
		
    	///////////////////////////////
    	//ACTUALIZA DATOS EN PAYMENT DATA
    	$paymentData->pd_box_price	= $request->get('pd_box_price');
    	$paymentData->pd_box_vat	= $request->get('pd_box_vat');
    	$paymentData->pd_box_total	= $request->get('$json->pd_box_total');
    	
    	$paymentData->pd_insurance_price	= $request->get('pd_insurance_price');
    	$paymentData->pd_insurance_vat	= $request->get('pd_insurance_vat');
    	$paymentData->pd_insurance_total	= $request->get('pd_insurance_total');
    	
    	$paymentData->pd_subtotal	= $request->get('pd_subtotal');
    	$paymentData->pd_total_vat	= $request->get('pd_total_vat');
    	$paymentData->pd_total		= $request->get('pd_total');
    	
    	$paymentData->pd_total_next_month = $request->get('pd_total_next_month');
    	
		//NUEVOS CAMPOS DE PAYMENT DATA PREPAY
		
		Log::info(" pd_box_price_prepay : " . $request->get('pd_box_price_prepay'));
		Log::info(" pd_box_total_prepay : " . $request->get('pd_box_total_prepay'));		

    	$paymentData->pd_box_price_prepay		= $request->get('pd_box_price_prepay');
    	$paymentData->pd_box_vat_prepay			= $request->get('pd_box_vat_prepay');
    	$paymentData->pd_box_total_prepay		= $request->get('pd_box_total_prepay');
    	$paymentData->pd_insurance_price_prepay = $request->get('pd_insurance_price_prepay');
    	$paymentData->pd_insurance_vat_prepay	= $request->get('pd_insurance_vat_prepay');
    	$paymentData->pd_insurance_total_prepay	= $request->get('pd_insurance_total_prepay');
    	$paymentData->pd_subtotal_prepay		= $request->get('pd_subtotal_prepay');
    	$paymentData->pd_total_vat_prepay		= $request->get('pd_total_vat_prepay');
    	$paymentData->pd_total_prepay			= $request->get('pd_total_prepay');
    	$paymentData->save();
    	
    	
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	/////////////////////////METODOS DE INVOICE CONTROLLER PARA GENERACION DE BILLING///////////////////////////
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	
    	$ins = $paymentData->insurance;
    	
    	//Generamos el nombre del pdf
    	//$invoiceSecuence = new InvoiceSequence();
    	//$invoiceSecuence->save();
    	
    	//FISCAL CHANGES
    	if($paragon == 0)
    	{
    		$invoiceSecuence = new InvoiceSequence();
    		$invoiceSecuence->is_contador =  $reciboSequence->id;
    		$invoiceSecuence->save();
    	}
    	else{
    		$invoiceSecuence = new ParagonSequence();
    		$invoiceSecuence->save();
    	}
    	
    	//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
    	
    	$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $idUser, 'storage_id' => $storage->id])->first();
    	if(!is_null($billingUpdate)){
    		$saldo = $billingUpdate->saldo;
    		$billingUpdate->bi_flag_last = 0;
    		$billingUpdate->save();
    	}
    	else{
    		$saldo = 0;
    	}
    	
    	$vat = $paymentData->pd_vat * 100;
    	
    	$status_impresion = "PENDIENTE PDF";
    	
    	$amountCurrent = $paymentData->pd_total;
    	
    	//GENERAMOS EL CARGO
    	$billing = new Billing();
    	$billing->user_id = $idUser;
    	$billing->storage_id = $storage->id;
    	$billing->pay_month = date("Y-m-d");
    	$billing->flag_payed = 1;
    	$billing->abono = 0;
    	$billing->cargo = $request->get('pd_total');
    	$billing->saldo = ($request->get('pd_total') * -1) + $saldo;
    	$billing->payment_type_id = 2;
    	
    	//$billing->pdf = $pdfName;
    	$billing->reference = '';
    	$billing->bi_flag_last	= 1;
    	$billing->bi_subtotal 			= $request->get('pd_subtotal');
    	$billing->bi_porcentaje_vat 	= $vat;
    	$billing->bi_total_vat 			= $request->get('pd_total_vat');
    	$billing->bi_total 				= $request->get('pd_total');
    	$billing->bi_status_impresion 	= $status_impresion;
    	$billing->bi_recibo_fiscal 		= "";
    	$billing->payment_data_id		= $paymentData->id;
    	$billing->bi_number_invoice 	= $invoiceSecuence->id;
    	$billing->bi_year_invoice		= date("y");
    	$billing->bi_batch				= "0";
    	$billing->bi_contenido_app		= $request->get('contenido_app');
    	 
    	$billing->save();
    	
    	//GENERA BILLING DETAIL
    	//BOX
    	$billingDetail = new BillingDetail();
    	$billingDetail->billing_id			= $billing->id;
    	$billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$storage->sqm ."m2";
    	$billingDetail->bd_codigo			= "box ".$storage->alias;
    	$billingDetail->bd_numero			= "1";
    	$billingDetail->bd_valor_neto		= $request->get('pd_box_price');
    	$billingDetail->bd_porcentaje_vat 	= $vat;
    	$billingDetail->bd_total_vat		= $request->get('pd_box_vat');
    	$billingDetail->bd_total			= $request->get('pd_box_total');
    	$billingDetail->bd_tipo_partida		= "BOX";
    	$billingDetail->save();
    	 
    	//INSURANCE
    	if($ins > 0){
    		 
    		$billingDetail = new BillingDetail();
    		$billingDetail->billing_id			= $billing->id;
    		$billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
    		$billingDetail->bd_codigo			= "ubezpieczenie";
    		$billingDetail->bd_numero			= "1";
    		$billingDetail->bd_valor_neto		= $request->get('pd_insurance_price');
    		$billingDetail->bd_porcentaje_vat 	= $vat;
    		$billingDetail->bd_total_vat		= $request->get('pd_insurance_vat');
    		$billingDetail->bd_total			= $request->get('pd_insurance_total');
    		$billingDetail->bd_tipo_partida		= "INSURANCE";
    		$billingDetail->save();
    	}
    
    	//
    	//Log::info('Saldo : '.$saldo);
    	
    	//generamos el abono
    	 
    	$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $idUser, 'storage_id' => $storage->id])->first();
    	
    	if(!is_null($billingUpdate)){
    		
    		$saldo = $billingUpdate->saldo;
    		$billingUpdate->bi_flag_last = 0;
    		$billingUpdate->save();
    	}
    	else{
    		$saldo = 0;
    	}
    	

    	
    	//VA A GENERRAR TRANSACCION BRAINTREE
    	if($prepayMonths > 0){
	    	//SI HAY PREPAY NO SUSCRIBE.. PROGRAMA SUSCRIPCION
	    	//		Log::info('Hay Prepay de ('.$prepayMonths.') months.. No suscribe');
    			 
    		$fechaProgramada = Carbon::now()->addMonths($prepayMonths+1)->startOfMonth()->addDays(3);
    		//$storage->date_subscribe_braintree	= $fechaProgramada;
    		$storage->st_meses_prepago	= $prepayMonths;
    		$storage->save();
    				 
    		$paymentData->pd_suscription_canceled = 0;
    	}else{
    			
	    	//SOLO PAGA Y ASIGNA LA SUSCRIPCIÓN PARA EL SIGUIENTE MES
	    	$fechaProgramada = Carbon::now()->addMonth()->startOfMonth()->addDays(3);
			$storage->date_subscribe_braintree	= $fechaProgramada;
			$storage->save();
				 
			$paymentData->pd_suscription_canceled = 0;
    	}
	    		
    	///
	    if($trans['returnCode'] == 200){

	    	$billing->flag_payed = 1;
	    	$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
	    	$billing->save();
	    		
	    	$transactionIDCurrent = $trans['transaction_id'];
	    	$returnCodeCurrent = 200;
	    	$statusCurrent = $trans['status'];
	    	$cardTypeCurrent = $trans['cardType'];
	    			
	    }else{
	    	$msg = $trans['msg'];
	    	$result = $trans['result'];
	    			
	    	Log::info('Error al generar la transaccion en braintree');
	    	Log::info('Msg Error: '. $msg);
	    	Log::info('Result Error: '. $result);
	    			
	    	$billing->flag_payed = 0;
	    			
	    	$returnCodeCurrent = 100;
	    	$errorCurrent = $trans['msg'];
	    }
    	//FIN DE BRAINTREE
    	
    	$billingA = new Billing();
    	$billingA->user_id 				= $idUser;
    	$billingA->storage_id 			= $storage->id;
    	$billingA->pay_month 			= date("Y-m-d");
    	$billingA->flag_payed 			= 1;
    	$billingA->abono 				= $request->get('pd_total');
    	$billingA->cargo 				= 0;
    	$billingA->saldo 				= $request->get('pd_total') + $saldo;
    	//$billingA->pdf 					= $pdfName;
    	$billingA->reference 			= "";
    	$billingA->bi_flag_last			= 1;
    	$billingA->payment_type_id 		= 2;
    	$billingA->payment_data_id		= $paymentData->id;
    	$billingA->bi_transaction_braintree_id	= $trans['transaction_id'];
    	$billingA->reference	= $trans['transaction_id'];
    	$billingA->save();
    	//
   		//ACTUALIZA PRICE PER MONTH
   		$storage->price_per_month = $request->get('pd_total_next_month');
   		$storage->save();
        	
   		// DESACTIVAMOS LA ORDEN
   		if($paymentData->order_id != 0){
	   		$order = Orders::find($paymentData->order_id);
	   		$order->active = 0;
	   		$res = $order->save();
   		}
    	
		   //GENERAMOS EL CARGO DEL PREPAY
		Log::info('Prepaymonths: '.$prepayMonths );
   		if($prepayMonths > 0){
   			    			
   			//FISCAL CHANGES
   			if($paragon == 0)
   			{
   				$invoiceSecuence = new InvoiceSequence();
   				$invoiceSecuence->is_contador =  $reciboSequence->id;
   				$invoiceSecuence->save();
   			}
    		else{
    			$invoiceSecuence = new ParagonSequence();
    			$invoiceSecuence->save();
    		}
    						 										 
    		//$pdfName = date("y")."-PW-".$invoiceSecuence->id.'_'.$dataUser->accountant_number.'.pdf';
    						 										 
    		$billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $idUser, 'storage_id' => $storage->id])->first();
    			
    		if(!is_null($billingUpdate)){
    			$saldo = $billingUpdate->saldo;
    			$billingUpdate->bi_flag_last = 0;
    			$billingUpdate->save();
    		}
    		else{
    			$saldo = 0;
    		}
    	 
    		$amountPrepay = $paymentData->pd_total_prepay;
    			
		    //GENERAMOS EL CARGO DE PREPAY
		    $billing = new Billing();
		    $billing->user_id = $idUser;
		    $billing->storage_id = $storage->id;
		    $billing->pay_month = date("Y-m-d");
		    $billing->flag_payed = 0;
		    $billing->abono = 0;
		    $billing->cargo = $request->get('pd_total_prepay');
		    $billing->saldo = ($request->get('pd_total_prepay') * -1) + $saldo;
		    //$billing->pdf = $pdfName;
		    $billing->reference = '';
		    $billing->bi_flag_last	= 1;
		    $billing->bi_subtotal 			= $request->get('pd_subtotal_prepay');
		    $billing->bi_porcentaje_vat 	= $vat;
		    $billing->bi_total_vat 			= $request->get('pd_total_vat_prepay');
		    $billing->bi_total 				= $request->get('pd_total_prepay');
		    $billing->bi_status_impresion 	= $status_impresion;
		    $billing->bi_recibo_fiscal 		= "";
		    $billing->bi_number_invoice 	= $invoiceSecuence->id;
		    $billing->bi_year_invoice		= date("y");
		    $billing->bi_flag_prepay		= "1";
		    $billing->payment_data_id		= $paymentData->id;
		    $billing->payment_type_id = 2;
		    $billing->save();
		    	 
		    //Log::info('Billing de prepay: '. $billing->id);
    	 
    	 
		    //GENERA BILLING DETAIL
		    //BOX
		    $billingDetail = new BillingDetail();
		    $billingDetail->billing_id			= $billing->id;
		    $billingDetail->bd_nombre			= "Wynajem powierzchni magazynowej ".$storage->sqm ."m2";
		    $billingDetail->bd_codigo			= "box ".$storage->alias;
		    $billingDetail->bd_numero			= "1";
		    $billingDetail->bd_valor_neto		= $request->get('pd_box_price_prepay');
		    $billingDetail->bd_porcentaje_vat 	= $vat;
		    $billingDetail->bd_total_vat		= $request->get('pd_box_vat_prepay');
		    $billingDetail->bd_total			= $request->get('pd_box_total_prepay');
		    $billingDetail->bd_tipo_partida		= "BOX";
		    $billingDetail->save();
		    	
		    //INSURANCE
		    if($ins > 0){
				///Log::info('Registra billing detail de insurance, billing_id: '. $billing->id);
			    	 
			    $billingDetail = new BillingDetail();
			    $billingDetail->billing_id			= $billing->id;
			    $billingDetail->bd_nombre			= "ubezpieczenie pomieszczenia magazynowego";
			    $billingDetail->bd_codigo			= "ubezpieczenie";
			    $billingDetail->bd_numero			= "1";
			    $billingDetail->bd_valor_neto		= $paymentData->pd_insurance_price_prepay;
			    $billingDetail->bd_porcentaje_vat 	= $vat;
			    $billingDetail->bd_total_vat		= $paymentData->pd_insurance_vat_prepay;
			    $billingDetail->bd_total			= $paymentData->pd_insurance_total_prepay;
			    $billingDetail->bd_tipo_partida		= "INSURANCE";
		    	$billingDetail->save();
		    }

		    $billingUpdate = Billing::where(['bi_flag_last' => 1, 'user_id' => $idUser, 'storage_id' => $storage->id])->first();
		    	
	    	if(!is_null($billingUpdate)){
	    		$saldo = $billingUpdate->saldo;
	    		$billingUpdate->bi_flag_last = 0;
	    		$billingUpdate->save();
	    	}
   			else{
   				$saldo = 0;
   			}
    	
	    	//VA A GENERAR TRANSACCION BRAINTREE PARA EL PREPAY
	    	$prepayExitoso = 0;
		    	
	    	if($customer != ""){
		    	//GENERA TRANSACCION
	    		//Log::info('Generara transaccion en braintree PREPAY -> Token Card: '.$tokenCard);
		    		
	    		Log::info('------');
	    		Log::info('------');
	    		Log::info('---------------------------------');
	    		$transP = BraintreeHelper::createTransaction('23342343s'/*$tokenCard*/,$customer, round($billing->cargo,2),$paymentData->order_id,0,$idUser);
		    		
		    		
	    		Log::info(json_encode($transP));
		    		
		    		
	    		if($transP['returnCode'] == 200){
		    			
	    			Log::info('Transaction Prepay success');
	    			//EXITOSO
	    			$billing->flag_payed = 1;
		    		$billing->bi_transaction_braintree_id	= $transP['transaction_id'];
		    		$billing->save();
		    		
		    		$returnCodePrepay = 200;
		    		$transactionIDPrepay = $transP['transaction_id'];
		    		$statusPrepay = $transP['status'];
		    		$cardTypePrepay = $transP['cardType'];
			    		
		    		$prepayExitoso = 1;
			    		
			    		
		    		Log::info('Transaction ID: '. $transP['transaction_id']);
		    		
	    		}
		    		else{
		    			Log::info('Transaction Prepay ERROR');
		    			$returnCodePrepay = 400;
		    			$errorPrepay = $transP['msg'];
		    			
		    			Log::info('ERROR Prepay: '. $errorPrepay);
		    		}
		    		
		    	}
    	 
		    	//FIN DE BRAINTREE
    	
    			//Log::info('El saldo antes de cargod e prepay es: '. $saldo);
    	
		    	//GENERAMOS EL ABONO DE PREPAY
		    	$billing = new Billing();
		    	$billing->user_id = $idUser;
		    	$billing->storage_id = $storage->id;
		    	$billing->pay_month = date("Y-m-d");
		    	$billing->flag_payed = 1;
		    	$billing->abono = $request->get('pd_total_prepay');
		    	$billing->cargo = 0;
		    	$billing->saldo = $paymentData->pd_total_prepay + $saldo;
		    	//$billing->pdf = $pdfName;
		        $billing->reference = '';
		    	$billing->bi_flag_last	= 1;
		    	$billing->bi_recibo_fiscal 		= "";
		    	$billing->bi_number_invoice 	= $invoiceSecuence->id;
		    	$billing->bi_year_invoice		= date("y");
		    	$billing->bi_flag_prepay		= "1";
		    	$billing->payment_data_id		= $paymentData->id;
		    	
		    	if($prepayExitoso == 1){
		    		$billing->reference	= $trans['transaction_id'];
		    		$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
		    	}
    			
    			$billing->payment_type_id = 2;
    	
    			$billing->save();	    	
    	}
    	  
    	$paymentData->save();
    	 
    	$msg = "";
    	if($msgRetorno == ""){
    		//NO HUBO ERRORES EN BRAINTREE
    		$msg = trans('cart.suscription_success');
    	}else{
    		$msg = $msgRetorno;
    	}

    	Log::info('END---');
    	
    	return ['returnCode' => 200  , 'msg' => $msg ,
    			'amountCurrent' => $amountCurrent,
    			'amountPrepay' => $amountPrepay,
    			
    			'transactionIDCurrent' => $transactionIDCurrent,
    			'transactionIDPrepay' => $transactionIDPrepay,
    			
    			'returnCodeCurrent' => $returnCodeCurrent,
    			'returnCodePrepay' => $returnCodePrepay,
    			
    			'errorCurrent' => $errorCurrent,
    			'errorPrepay' => $errorPrepay,
    			
    			'statusCurrent' => $statusCurrent,
    			'cardTypeCurrent' => $cardTypeCurrent,
    			
    			'statusPrepay' => $statusPrepay,
    			'cardTypePrepay' => $cardTypePrepay
    			
    			
    	];
    	
 	}
 	
 	function wsPay(Request $request){
 		
 		Log::info('APIController@wsPay');
 		
 		$idBilling = $request->get('id_billing');
 		//$tokenCard = $request->get('payment_method_nonce');
 		
 		$billing = Billing::find($idBilling);
 		
 		$idUser = $billing->user_id;
 		$user = User::find($idUser);
 		
 		$storage = Storages::find($billing->storage_id);
 		$paymentData = PaymentData::find($storage->payment_data_id);

 		$cardUser = CardUser::
 					where([
 						'cu_customer_id' => $user->customer_id, 
 						'user_id' => $idUser
 						])
 					->first();

 		Log::info('CardUser: '.$cardUser);

 		if(is_null($cardUser)){
 			//SI NO EXISTE LA TARJETA LA REGISTRA
 			Log::info('Registra CardUser');
 			$card_token = BraintreeHelper::getCardToken($user->customer_id, $request->get('payment_method_nonce'),$idUser);

 			$cardUser = new CardUser();
 			$cardUser->cu_customer_id 	= $user->customer_id;
 			$cardUser->cu_token_card 	= $card_token;
 			$cardUser->user_id			= $idUser;
 			$cardUser->save();
 				
 			$tokenCard = $card_token;
 		
 			$paymentData->card_user_id = $cardUser->id;
 		}

 		$tokenCard = $cardUser->cu_token_card;
 		
 		Log::info('PaymentdataId: '.$storage->payment_data_id);
 		
 		$prepay = $paymentData->prepay;
 		
 		Log::info('++++++');
 		Log::info('++++++');
 		$trans = BraintreeHelper::createTransaction($tokenCard,$user->customer_id,
 								$billing->bi_total,$paymentData->order_id, 0, $idUser);
 		
 		Log::info('Braintree Transaction Reponse: '.$trans['paymentMethodToken']);
 		
 		/*$cardUser = CardUser::
 					where([
 						'cu_customer_id' => $user->customer_id, 
 						'cu_token_card' => $trans['paymentMethodToken']
 						])
 					->first();*/
 		
 		/*if(is_null($cardUser)){
 			//SI NO EXISTE LA TARJETA LA REGISTRA
 			Log::info('Registra CardUser');
 			$cardUser = new CardUser();
 			$cardUser->cu_customer_id 	= $user->customer_id;
 			$cardUser->cu_token_card 	= $trans['paymentMethodToken'];
 			$cardUser->user_id			= $idUser;
 			$cardUser->save();
 				
 			$tokenCard = $subs['paymentMethodToken'];
 		
 			$paymentData->card_user_id = $cardUser->id;
 		}*/
 		
 		$billing->flag_payed = 1;
 		$billing->bi_transaction_braintree_id	= $trans['transaction_id'];
 		$billing->reference	= $trans['transaction_id'];
 		$billing->save();
 		 		
 		if($trans['returnCode'] == 200){
 			Log::info('TransactionID : '. $trans['transaction_id']);
 			
 			$paymentData->pd_transaction_id = $trans['transaction_id'];
 			$paymentData->save();

			//GET SALDO
    		$saldo = Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])->sum('saldo');

    		if(is_null($saldo)){
    			$saldo = 0;
    		}

    		//New row
			Billing::where(['user_id' => $billing->user_id, 'bi_flag_last' => 1, 'storage_id' => $billing->storage_id])
			->update(['bi_flag_last' => '0']);
			 
			$saldoNuevo = $saldo + $billing->cargo;
			Log::info('El cargo en el nuevo registro es de : '.$saldoNuevo);
			Log::info('Operacion aritmetica realizada : '.$saldo .'+'. $billing->cargo);	 
			
			$newBilling = new Billing();
			$newBilling->user_id 	= $billing->user_id;
			$newBilling->storage_id	= $billing->storage_id;
			$newBilling->pay_month	= Carbon::now()->format('Y-m-d');
			$newBilling->flag_payed = 1;
			$newBilling->abono		= $billing->cargo;
			$newBilling->cargo		= 0;
			$newBilling->saldo		= $saldoNuevo;
			$newBilling->pdf		= "";
			$newBilling->reference	= "";
			$newBilling->bi_flag_last= 1;
			$newBilling->payment_type_id 	= $request->get('payment_type_id');
			
			$newBilling->save();

 			$mensaje = "Your payment of the box #".$storage->alias." has been successfully";

			UtilNotificaciones::Enviar($mensaje, $idUser);
			$notificacion = new notificaciones();
            $notificacion->user_id = $idUser;
            $notificacion->storage_id = $paymentData->storage_id;
            $notificacion->notf_mensaje = $mensaje;
            $notificacion->save();	

 			return ['returnCode' => 200  , 'msg' => trans('ws.transaction_success') , ];
 		}else{
 			return ['returnCode' => 601  , 'msg' => $trans['msg'] , ];
 		}

 	}
 	
 	function wsSuscribe(Request $request){
 		
 		$idUser = $request->get('id_user');
 		$tokenTarjeta = $request->get('payment_method_nonce');
 		$idStorage = $request->get('id_box');
 		
 		$storage = Storages::find($idStorage);
 		
 		$paymentData = PaymentData::find($storage->payment_data_id);

 		
 		$user = User::find($idUser);	
 		
 			
 		$subs = BraintreeHelper::createSuscription($tokenTarjeta, env('PLAN_ID'), $paymentData->pd_total_next_month,1,$idUser);
 		
 		$cardUser = CardUser::where(['cu_customer_id' => $user->customer_id, 'cu_token_card' => $subs['paymentMethodToken']])->first();
 		 
 		if(is_null($cardUser)){
 			//SI NO EXISTE LA TARJETA LA REGISTRA
 			Log::info('Registra CardUser');
 			$cardUser = new CardUser();
 			$cardUser->cu_customer_id 	= $user->customer_id;
 			$cardUser->cu_token_card 	= $subs['paymentMethodToken'];
 			$cardUser->user_id			= $idUser;
 			$cardUser->save();
 		
 			$tokenCard = $subs['paymentMethodToken'];
 			
 			$paymentData->card_user_id = $cardUser->id;
 		}
 		
 		$paymentData->pd_suscription_id = $subs['subscription_id'];
 		$paymentData->pd_suscription_canceled = 0;
 			
 		$paymentData->save();
 			
 		return ['returnCode' => 200  , 'msg' => trans('ws.subscription_success') , 
 				'suscriptionID'	=> $subs['subscription_id']
 		];

 	}

 	function wsInvoices(Request $request) {

		Log::info($request);

		$json = json_decode($request->get('payload'));
		

    	$id_user = $json->id_user;
    	$id_box = $json->id_box;
    	$billings = Billing::with(['paymentType'])->where('user_id', '=', $id_user)->where('storage_id', '=', $id_box)->orderBy('id', 'desc')->get();

    	if ($billings == null) {
    		return ['returnCode' => 601];
    	}else {
    		return ['returnCode' => 200, 'invoices' => $billings];
    	}
    }

    function wsJoin(Request $request) {
    	$json = json_decode($request->get('payload'));
    	if ($json->nombre_app == null) {
    		$nombre_app = "";
    	}else {
    		$nombre_app = $json->nombre_app;	
    	}
    	
    	if ($json->name == null) {
    		$name = "";
    	}else {
    		$name = $json->name;	
    	}
    	
    	if ($json->lastName == null) {
    		$lastName = "";
    	}else {
    		$lastName = $json->lastName;	
    	}
    	
    	$email = $json->email;
    	$telefono = $json->telefono;
    	if ($json->password == null) {
    		$password = "";
    	}else {
    		$password = $json->password;	
    	}
    	
    	$id_fb = $json->id_fb;
    	$url_avatar = $json->url_avatar;

		Log::info("Registering user with email... ". $email);
		
    	$consultaUsuario = User::where('email', '=', $email)->first();

    	if ($consultaUsuario == null) {
    		$user = new User();
    		$user->username = "";
    		$user->name = $name;
    		$user->lastName = $lastName;
    		$user->companyName = "";
    		$user->peselNumber = "";
    		$user->idNumber = "";
    		$user->nipNumber = "";
    		$user->regonNumber = "";
    		$user->courtNumber = "";
    		$user->courtPlace = "";
    		$user->krsNumber = "";
    		$user->Role_id = 0;
    		$user->userType_id = 0;
    		$user->birthday = "0000-00-00";
    		$user->active = 1;
    		$user->validate = 0;
    		$user->oportunity_reminder = "0000-00-00";
    		$user->notes = "";
    		$user->avatar_tocken = "";
    		$user->phone2 = "";
    		$user->email2 = "";
    		$user->appName = $nombre_app;
    		$user->email = $email;
    		$user->phone = $telefono;
    		$user->password = bcrypt($password);
    		if ($id_fb == null) {
    			$id_fb = "";
    		}
    		$user->fb_id = $id_fb;
    		if ($url_avatar == null) {
    			$url_avatar = "";
    		}
    		$user->avatar = $url_avatar;
    		
    		/*$clientToken = \Braintree_ClientToken::generate([
    				"customerId" => $customer_id
    		]);*/

    		$user->movil_dispositive = $json->movil_dispositive;

    		$user->save();

    		$customer_id = BraintreeHelper::registerUserOnBrainTree($user->name . " " . $user->companyName,$user->lastName,$user->email,$user->phone,$user->id);
    		$user->customer_id = $customer_id;
    		$user->save();

    		return ['returnCode' => 200, 'id_user' => $user->id, 'nombre_app' => $user->appName, 'email' => $user->email, 'telefono' => $user->phone, 'facebook_id' => $user->fb_id, 'avatar_url' => $user->avatar, 'customer_id' => $user->customer_id, 'name' => $user->name, 'lastName' => $user->lastName, 'movil_dispositive' => $user->movil_dispositive];
    	} else {
    		return ['returnCode' => 602, 'msg' => 'Usuario ya Existente'];
    	}
    }

    function wsSaveOrder(Request $request) {
    	$json = json_decode($request->get('payload'));
    	$ppm = $json->pricePerMonth;
    	$idClient = $json->user_id;
    	$order = $json->id_order;
    	$itemType = $json->itemType;
    	$itemId = $json->itemId;
        $dateBox = $json->dateBox;
        $term = $json->term;
        $prepay = $json->prepay;
        $cc = $json->cc;
        $insurance = $json->insurance;
        $ext = $json->ext;
        $pd_box_price = $json->pd_box_price;
        $pd_box_vat = $json->pd_box_vat;
        $pd_box_total = $json->pd_box_total;
        $pd_insurance_price = $json->pd_insurance_price;
        $pd_insurance_vat = $json->pd_insurance_vat;
        $pd_insurance_total = $json->pd_insurance_total;
        $pd_subtotal = $json->pd_subtotal;
        $pd_total_vat = $json->pd_total_vat;
        $pd_total = $json->pd_total;
        $pd_total_next_month = $json->pd_total_next_month;
        $pd_box_price_prepay = $json->pd_box_price_prepay;
        $pd_box_vat_prepay = $json->pd_box_vat_prepay;
        $pd_box_total_prepay = $json->pd_box_total_prepay;
        $pd_insurance_price_prepay = $json->pd_insurance_price_prepay;
        $pd_insurance_vat_prepay = $json->pd_insurance_vat_prepay;
        $pd_insurance_total_prepay = $json->pd_insurance_total_prepay;
        $pd_subtotal_prepay = $json->pd_subtotal_prepay;
        $pd_total_vat_prepay = $json->pd_total_vat_prepay;
        $pd_total_prepay = $json->pd_total_prepay;
        $itemQuantity = $json->itemQuantity;
        

        if($order == 0 || is_null($order)){
            $newOrder = Orders::create([
				'user_id' => $idClient,
				'active' => 1
            ]);

            \Session::put('order', $newOrder->id);
            $order = $newOrder->id;
        }

        // if an item is a box:
		if ($itemType == 'box') {
			$price = Storages::where('id', '=', $itemId)->where('active', '!=', '0')->first();
			$price = $price->price;

			$dateBox = Carbon::parse($dateBox)->toDateString();

			// save the payment data
			$pd = new paymentData();
			$pd->pd_method 		= "APP";
			$pd->user_id 		= $idClient;
			$pd->storage_id		= $itemId;
			$pd->term			= $term;
			$pd->prepay			= $prepay;
			$pd->creditcard		= $cc;
			$pd->order_id		= $order;
			$pd->insurance		= $insurance;
			$pd->pd_vat			= env('VAT');
			$pd->ext			= $ext;
			$pd->user_transaction_id = $idClient;

			//NUEVOS CAMPOS DE PAYMENT DATA
			$pd->pd_box_price	= $pd_box_price;
			$pd->pd_box_vat		= $pd_box_vat;
			$pd->pd_box_total	= $pd_box_total;

			$pd->pd_insurance_price	= $pd_insurance_price;
			$pd->pd_insurance_vat	= $pd_insurance_vat;
			$pd->pd_insurance_total	= $pd_insurance_total;

			$pd->pd_subtotal	= $pd_subtotal;
			$pd->pd_total_vat	= $pd_total_vat;
			$pd->pd_total		= $pd_total;

			$pd->pd_total_next_month = $pd_total_next_month;

			//NUEVOS CAMPOS DE PAYMENT DATA PREPAY
			$pd->pd_box_price_prepay		= $pd_box_price_prepay;
			$pd->pd_box_vat_prepay			= $pd_box_vat_prepay;
			$pd->pd_box_total_prepay		= $pd_box_total_prepay;
			$pd->pd_insurance_price_prepay 	= $pd_insurance_price_prepay;
			$pd->pd_insurance_vat_prepay	= $pd_insurance_vat_prepay;
			$pd->pd_insurance_total_prepay	= $pd_insurance_total_prepay;
			$pd->pd_subtotal_prepay			= $pd_subtotal_prepay;
			$pd->pd_total_vat_prepay		= $pd_total_vat_prepay;
			$pd->pd_total_prepay			= $pd_total_prepay;
			$pd->save();

			//FIX... ASIGNA EL ID DEL PAYMENT DATA CREADO EN LA TABLA DE STORAGES
			

			if(isset($json->flagPaid)){
				Log::info('ASgina Storage al cliente: ' . $idClient);
				
				$storage = Storages::find($pd->storage_id);
				$storage->payment_data_id 	= $pd->id;
				$storage->flag_rent 		= 1;
				$storage->user_id 			= $idClient;
				$storage->rent_start 		= $dateBox;
				$storage->save();
				
				//ENVIA NOTIFICACION AL ADMIN WEB
				$notificaciones = new notificaciones();
				$notificaciones->user_id 	= $idClient;
				$notificaciones->storage_id = $pd->storage_id;
				$notificaciones->notf_mensaje	= 'Box rented';
				$notificaciones->notf_admin		= '1';
				$notificaciones->notf_type		= 'RENT BOX';
				$notificaciones->save();
					
			}
			else{
				$storage = Storages::find($pd->storage_id);
				$storage->payment_data_id = $pd->id;
				$storage->save();
			}
		}

          // if an item is a extra item:
          if ($itemType == 'item') {
            $price = ExtraItems::where('id', '=', $itemId)->first();
            $price = $price->price;
          }

          $addItem = DetOrders::create([
            'order_id' => $order,
            'product_type' => $itemType,
            'product_id' => $itemId,
            'rent_starts' => $dateBox,
            'quantity' => $itemQuantity,
            'price' => $price,
            'price_per_month' => $pd_total_next_month,
          ]);
          
          if($pd->creditcard == 2){
          	//CON TARJETA NO HAY DEPOSITO
	          $deposito = new Deposito();
	          $deposito->storage_id 		= $itemId;
	          $deposito->user_id			= $idClient;
	          $deposito->de_monto_deposito	= $pd_total_next_month;
	          $deposito->de_regresado 		= 0;
	          
	          $deposito->save();
          }

          return ['returnCode' => 200  , 'msg' => 'Order Saved'];
    }

    public function wsGetCountries(Request $request) {
    	$countries = Countries::all();

    	if (is_null($countries)) {
    		return ['returnCode' => 602];
    	}
    	return ['returnCode' => 200, 'countries' => $countries];
    }

    public function wsDeleteNotification(Request $request) {
    	$json = json_decode($request->get('payload'));
    	$notificacion = notificaciones::find($json->id_notificacion);
    	$notificacion->notf_eliminado = 1;
    	$notificacion->save();

    	if (is_null($notificacion)) {
    		return ['returnCode' => 602, 'msg' => 'Notification not found'];
    	}
    	return ['returnCode' => 200, 'msg' => 'Notification deleted successfully!'];
    }

    public function wsUserNotifications(Request $request) {
    	$json = json_decode($request->get('payload'));
    	$notificaciones = notificaciones::where('user_id', '=', $json->id_user)
    				->where([
    						'notf_eliminado' => 0,
    						'notf_admin'	=> 0
    				])->orderBy('created_at', 'desc')->get();

    	if (is_null($notificaciones)) {
    		return ['returnCode' => 602, 'msg' => 'This user has not notifications.'];
    	}
    	return ['returnCode' => 200, 'notifications' => $notificaciones];
    }

    public function wsDeleteAllNotifications(Request $request) {
    	$json = json_decode($request->get('payload'));
    	$notificaciones = notificaciones::where('user_id', '=', $json->id_user)->get();
    	foreach ($notificaciones as $key => $notificacion) {
    		$notificacion->notf_eliminado = 1;
    		$notificacion->save();
    	}

    	return ['returnCode' => 200, 'msg' => 'All the notifications have been read.'];
    }
    
    public function getArticulos(Request $request){
    	
    	$secciones = CataSecciones::with(['articulos'])->get();
    	//$totalSecciones = CataSecciones::count();
    	//$articulos = Articulos::all();
    
    	return ['success' => 'true', 'secciones' => $secciones];
	}
	
	

}
