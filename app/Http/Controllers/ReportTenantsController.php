<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\User;
use App\Warehouse;
use App\Levels;
use App\Storages;

class ReportTenantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tenants = User::
        leftJoin('storages', 'users.id', '=', 'storages.user_id')
            ->join('levels', 'storages.level_id', '=', 'levels.id')
            ->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
            ->select(
                'storages.alias as storageAlias',
                'levels.name as levelName',
                'warehouse.name as warehouseName',
                'users.name as userName',
                'users.lastName as userLN',
                'users.companyName as companyName',
                'storages.rent_start as rentStart',
                'storages.rent_end as rentEnd'
            )
            ->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
            ->get();
            // dd($tenants);

        return view('pw.reports.tenants.tenants_report', ['tenants'=>$tenants]);
    }

    public function generarExcel() {

        \Excel::create('Tenants Report CSV', function($excel) {

            $excel->sheet('History', function($sheet) {
                $tenants = User::
                leftJoin('storages', 'users.id', '=', 'storages.user_id')
                    ->join('levels', 'storages.level_id', '=', 'levels.id')
                    ->join('warehouse', 'levels.warehouse_id', '=', 'warehouse.id')
                    ->select(
                        'storages.alias as storageAlias',
                        'levels.name as levelName',
                        'warehouse.name as warehouseName',
                        'users.name as userName',
                        'users.lastName as userLN',
                        'users.companyName as companyName',
                        'storages.rent_start as rentStart',
                        'storages.rent_end as rentEnd'
                    )
                    ->orderByRaw(\DB::raw('CAST(`storageAlias` AS decimal)'))
                    ->get();

                $sheet->row(1, [
                    'Storage', 'Level', 'Warehouse', 'Name', 'Company Name', 'Rent Start', 'Rent End'
                ]);

                foreach($tenants as $index => $tenant) {
                    $sheet->row($index+2, [
                        $tenant->storageAlias, $tenant->levelName, $tenant->warehouseName,
                        $tenant->userName." ".$tenant->userLN, $tenant->companyName, $tenant->rentStart, $tenant->rentEnd
                    ]);
                }

            });
        })->export('xls');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
