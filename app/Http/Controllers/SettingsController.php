<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ConfigSystem;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pw.settings');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      $data = ConfigSystem::all();

      $vat = $data[1]->value;
      $insurance = $data[0]->value;
      $place = $data[2]->value;
      $address = $data[3]->value;
      $nip = $data[4]->value;
      $bank = $data[5]->value;
      $account = $data[6]->value;
      $invoiceNumber = $data[7]->value;
      $seller = $data[8]->value;


      return ['success' => true, 'vat' => $vat, 'insurance' => $insurance, 'place' => $place, 'address' => $address, 'nip' => $nip, 'bank' => $bank, 'account' => $account,'invoiceNumber' => $invoiceNumber,'seller' => $seller];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data)
    {
      $column = 'name';
      $vat = ConfigSystem::where($column,'=', 'vat')->first();
      $insurance = ConfigSystem::where($column,'=', 'insurance')->first();
      $place = ConfigSystem::where($column,'=', 'place')->first();
      $address = ConfigSystem::where($column,'=', 'address')->first();
      $nip = ConfigSystem::where($column,'=', 'nip')->first();
      $bank = ConfigSystem::where($column,'=', 'bank')->first();
      $account = ConfigSystem::where($column,'=', 'account')->first();
      $invoiceNumber = ConfigSystem::where($column,'=', 'invoice_number')->first();
      $seller = ConfigSystem::where($column,'=', 'seller')->first();

      $vat->value = $data['vat'];
      $insurance->value = $data['insurance'];
      $place->value = $data['expedicion'];
      $address->value = $data['address'];
      $nip->value = $data['nip'];
      $bank->value = $data['bank'];
      $account->value = $data['account'];
      $invoiceNumber->value = $data['invoiceNumber'];
      $seller->value = $data['seller'];
      $res = $vat->save();
      $res = $insurance->save();
      $res = $place->save();
      $res = $address->save();
      $res = $nip->save();
      $res = $bank->save();
      $res = $account->save();
      $res = $invoiceNumber->save();
      $res = $seller->save();

      return ['res' => $res];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
