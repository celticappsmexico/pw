<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('contact_us.contact_us');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $message = $request->get('name')." (". $request->get('email') . ") - ". $request->get('message');

        $emailData = [
            'email'                 => 'oscar.sanchez@novacloud.mx',
            'tituloCorreo'          => 'Contact PW',
            'mensaje'               => $message
        ];
            
        $resp = Mail::send ( 'emails.email_contact_us', $emailData, function ($message) use ($emailData) {
              
            $message->to ( $emailData['email'] );
        
              
            $message->from('no-reply@przechowamy-wszystko.pl' , 'Przechowamy Wszystko' );
            $message->subject('Contact Us - PW');
            
        });
        return [
            'returnCode'    =>  200,
            'msg'           => 'Message sent'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
