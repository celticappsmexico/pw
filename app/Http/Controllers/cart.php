<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Orders;
use App\DetOrders;
use Auth;
use Session;
use App\Storages;
use App\ExtraItems;
use App\PaymentData;
use App\Deposito;
use Carbon\Carbon;
use App\Http\Fecha;
use Illuminate\Support\Facades\Log;

class cart extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('pw.cart',['idOrder' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
        isset($data['pricePerMonth']) ? $ppm = $data['pricePerMonth'] : $ppm = 0;

        $dateBox = '0000-00-00';

        if ($mobile) {
          $idClient = $data['userId'];
          $order = $data['order'];
        }else{
          $idClient = Auth::user()->id;
          $order = Session::get('order');
        }

        if($order == 0 || is_null($order)){
            $newOrder = Orders::create([
              'user_id' => $idClient,
              'active' => 1
            ]);

            \Session::put('order', $newOrder->id);
            $order = $newOrder->id;
        }

          // if an item is a box:
          if ($data['itemType'] == 'box') {
            $price = Storages::where('id', '=', $data['itemId'])->where('active', '!=', '0')->first();
            $price = $price->price;
            
            $dateBox = Carbon::parse($data['dateBox'])->toDateString();
            
            // save the payment data
            /*
            $pd = PaymentData::create([
                        'user_id' => $idClient,
                        'storage_id' => $data['itemId'],
                        'term' => $data['term'],
                        'prepay' => $data['prepay'],
                        'creditcard' => $data['cc'],
                        'order_id' => $order,
                        'insurance' => $data['insurance'],
                        'ext' => $data['ext']
                    ]);           
            **/
            
            $pd = new paymentData();
            $pd->user_id 		= $idClient;
            $pd->storage_id		= $data['itemId'];
            $pd->term			= $data['term'];
            $pd->prepay			= $data['prepay'];
            $pd->creditcard		= $data['cc'];
            $pd->order_id		= $order;
            $pd->insurance		= $data['insurance'];
            $pd->pd_vat			= env('VAT');
            $pd->ext			= $data['ext'];
            
            //NUEVOS CAMPOS DE PAYMENT DATA

            $pd->pd_box_price	= $data['pd_box_price'];
            $pd->pd_box_vat		= $data['pd_box_vat'];
            $pd->pd_box_total	= $data['pd_box_total'];
            
            $pd->pd_insurance_price	= $data['pd_insurance_price'];
            $pd->pd_insurance_vat	= $data['pd_insurance_vat'];
            $pd->pd_insurance_total	= $data['pd_insurance_total'];
            
            $pd->pd_subtotal	= $data['pd_subtotal'];
            $pd->pd_total_vat	= $data['pd_total_vat'];
            $pd->pd_total		= $data['pd_total'];
            
            $pd->pd_total_next_month = $data['pd_total_next_month'];
            
            //NUEVOS CAMPOS DE PAYMENT DATA PREPAY
            $pd->pd_box_price_prepay		= $data['pd_box_price_prepay'];
            $pd->pd_box_vat_prepay			= $data['pd_box_vat_prepay'];
            $pd->pd_box_total_prepay		= $data['pd_box_total_prepay'];
            $pd->pd_insurance_price_prepay 	= $data['pd_insurance_price_prepay'];
            $pd->pd_insurance_vat_prepay	= $data['pd_insurance_vat_prepay'];
            $pd->pd_insurance_total_prepay	= $data['pd_insurance_total_prepay'];
            $pd->pd_subtotal_prepay			= $data['pd_subtotal_prepay'];
            $pd->pd_total_vat_prepay		= $data['pd_total_vat_prepay'];
            $pd->pd_total_prepay			= $data['pd_total_prepay'];

            $pd->user_transaction_id = "1";

            $pd->save();
            
            //FIX... ASIGNA EL ID DEL PAYMENT DATA CREADO EN LA TABLA DE STORAGES
            $storage = Storages::find($pd->storage_id);
            $storage->payment_data_id = $pd->id;
            $storage->save();

          }

          // if an item is a extra item:
          if ($data['itemType'] == 'item') {
            //$price = ExtraItems::where('id', '=', $data['itemId'])->where('active', '!=', '0')->get();
            $price = ExtraItems::where('id', '=', $data['itemId'])->first();
            $price = $price->price;
          }

          $addItem = DetOrders::create([
            'order_id' => $order,
            'product_type' => $data['itemType'],
            'product_id' => $data['itemId'],
            'rent_starts' => $dateBox,
            'quantity' => $data['itemQuantity'],
            'price' => $price,
            'price_per_month' => $data['pd_total_next_month'],
          ]);
          
          if($pd->creditcard == 2){
          	//CON TARJETA NO HAY DEPOSITO
	          $deposito = new Deposito();
	          $deposito->storage_id 		= $data['itemId'];
	          $deposito->user_id			= $idClient;
	          $deposito->de_monto_deposito	= $data['pd_total_next_month'];
	          $deposito->de_regresado 		= 0;
	          
	          $deposito->save();
          }
          if ($mobile) {
            return ['success' => 'true','order_id' => $order,'new_item_id' => $addItem->id];
          }else{
            return $addItem->id;
          }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $data){

      isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
      isset($data['idUser']) ? $idUser = $data['idUser'] : $idUser = 0;
      $idOrder = $data['order'];
      $client = $data['name'];

      //dd($client);
      if($mobile){
        $orders = Orders::with('OrderDeatil')
                              ->where('user_id','=',$data['idUser'])
                              ->where('Active', '!=', '0')
                              ->where(function($query) use ($idOrder){
                                  if ($idOrder != '') {
                                      $query->where(['id' => $idOrder]);
                                  }
                              })
                              ->orderBy('id','DESC')->get();

        return ['success' => true,'orders' => $orders];

      }else{
        $orders = Orders::with(['Client','OrderDeatil'])
                              ->whereHas('Client',function($query) use ($client){
                                  $query->where('name','LIKE','%'.$client.'%');
                              })
                              ->where('Active', '!=', '0')
                              ->where(function($query) use ($idOrder){
                                  if ($idOrder != '') {
                                      $query->where(['id' => $idOrder]);
                                  }
                              })
                              ->orderBy('id','DESC')->get();
        //dd(count($orders));
        //dd($orders[0]);
        $cad = '';
        foreach ($orders as $key => $ord) {
          $array = array();
          foreach ($ord->OrderDeatil as $detail) {
            $sum = 0;
            $sum = $sum + (float)$detail->price;
            $sum = $sum * $detail->quantity;
            array_push($array, $sum);
          }

          $total = 0;
          for ( $i = 0; $i < sizeof($array); $i++ ) {
            $total += $array[$i];
          }

          if ($ord->Client->name == '') {
            $showName = $ord->Client->email;
          }else{
            $showName = $ord->Client->name.' '.$ord->Client->lastName;
          }

          //$total = $total * 1.23;
          $cad .= '<tr>';
          $cad .= '<td>'.$ord->id.'</td>';
          $cad .= '<td>'.$ord->Client->name.' '.$ord->Client->lastName . ' - ' . $ord->Client->companyName .'</td>';
          $cad .= '<td>'.$ord->Client->email.'</td>';
          $cad .= '<td>'.$ord->created_at.'</td>';
          $cad .= '<td><a href="javascript:void(0);" id="'.$ord->id.'" class="btn btn-default btn-xs viewOrder"><i class="fa fa-eye" aria-hidden="true"></i> </a>';
          //$cad .= '<a href="javascript:void(0);" id="'.$ord->id.'" class="btn btn-info btn-xs payOrder"><i class="fa fa-money" aria-hidden="true"></i> </a>';
          $cad .= '<a href="javascript:void(0);" id="'.$ord->id.'" class="btn btn-primary btn-xs delOrder"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
          $cad .= '</tr>';
        }

        return $cad;
      }
    }

    public function showDetailOrder(Request $data)
    {
      isset($data['mobile']) ? $mobile = $data['mobile'] : $mobile = 0;
      //isset($data['idUser']) ? $idUser = $data['idUser'] : $idUser = 0;

      $items = DetOrders::with(['paymentData','articleData'])->where('order_id','=',$data['order_id'])->get();
      $order = Orders::where('id','=',$items[0]->order_id)->where('active','!=','0')->first();
      //return [$order];

      if ($mobile) {
        return ['success' => true, 'items' => $items];
      }

      $num = count($order);
      if($num == 0){
        return ['success' => false];
      }

      //dd($items[0]->paymentData);
      $cad = '';
      $prefix = '';

      foreach ($items as $key => $item) {
        $cad2 = '';

        if ($item->product_type == 'item') {
          $wrapItem = '';
          $endWrapItem = '';
          $art = ExtraItems::where('id','=',$item->product_id)->first();
          $prefix = $art->nombre;
        }else{
          $art = Storages::where('id','=',$item->product_id)->first();
          $wrapItem = 'class="boxWrap'.$item->id.' storagewrap"';
          $fechaClass = new Fecha();
          $dateStart = $fechaClass->fechaPw($item->rent_starts);
          //$endWrapItem = '</span>';
          $prefix = 'Storage - '.$art->alias;
          ($item->paymentData[0]->creditcard == 1) ? $ccText = trans('calculator.yes') : $ccText = trans('calculator.no');
          ($item->paymentData[0]->insurance >= 1) ? $insText = trans('calculator.yes') : $insText = trans('calculator.no');
          
          ($item->paymentData[0]->creditcard == 1) ? $cad2.="<input type='hidden' id='creditCardFlag' value='1' />" : $cad2.= "<input type='text' id='creditCardFlag' value='0' />";
          
          $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.term_notice').'</td><td>'.$item->paymentData[0]->paymentTerm[0]->months.'<input type="hidden" name="termdb" id="termdb" value="'.$item->paymentData[0]->paymentTerm[0]->id.'"></td></td>';
          $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.pre-pay').'</td><td><span id="months">'.$item->paymentData[0]->paymentPrepay[0]->months.'</span><input type="hidden" name="prepaydb" id="prepaydb" value="'.$item->paymentData[0]->paymentPrepay[0]->id.'"></td></td>';
          $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.credit_card').'</td><td>'.$ccText.'<input type="hidden" name="ccdb" id="ccdb" value="'.$item->paymentData[0]->creditcard.'"></td></td>';
          if ($item->paymentData[0]->creditcard != 1) {
            $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.credit_card_percent').'</td><td>'.$item->paymentData[0]->ext.' %<input type="hidden" name="ccpdb" id="ccpdb" value="'.$item->paymentData[0]->ext.'"></td></td>';
          }
          $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.rent_start').'</td><td><div class="input-group date" id="rentstartdb"><input style="width:120px!important;" type="text" name="rentstartdb" class="form-control rentstartdb" dir="'.$item->id.'" value="'.$dateStart.'"></div></td></td>';
          $cad2 .= '<tr '.$wrapItem.'><td colspan="2"></td><td>'.trans('calculator.insurance').'</td><td>'.$insText.'<input type="hidden" name="insdb" id="insdb" value="'.$item->paymentData[0]->insurance.'"></td></td>';
        }
        //$cad .= $wrapItem;
        $cad .= '<tr '.$wrapItem.'>';
        $cad .= '<td>'.$prefix.'</td>';
        $cad .= '<td id="originalPrice">'.$item->price.'</td>';
        $cad .= '<td>'.$item->quantity.'</td>';
        $cad .= '<td class="totalItem" id="totalItem'.$item->id.'" dir="'.$item->id.'">'.($item->quantity)*($item->price).'</td>';
        $cad .= '</tr>';
        
        $cad2 .= '
        		<tr>
        			<th colspan="4" style="text-align:center !important;">'.trans('calculator.current_month_invoice').'</th>
        		</tr>
        		<tr>
              		<th></th>
              		<th></th>
              		<th>'.trans('cart.subtotal').'</th>
              		<th>'.$item->paymentData[0]->pd_subtotal.'</th>
            	</tr>
            	<tr>
	              	<th></th>
	              	<th></th>
	              	<th>'. trans('cart.vat') .'</th>
	              <th>'.$item->paymentData[0]->pd_total_vat.'</th>
	            </tr>
	            <tr>
	              <th></th>
	              <th></th>
	              <th>'. trans('cart.total') .'</th>
	              <th>'.$item->paymentData[0]->pd_total.'</th>
	            </tr>
	            <tr '.$wrapItem.'>
	            	<td colspan="2"></td>
	            	<td>'.trans('calculator.next_month').'</td>
	            	<td>'.$item->paymentData[0]->pd_total_next_month.'</td>
	            </tr>';
        if($item->paymentData[0]->creditcard == 1){
        	//CREDIT CARD YES
		 $cad2.='   <tr>
		              <th></th>
		              <th></th>
		              <th>'. trans('calculator.deposit_month') .'</th>
		              <th>0</th>
		            </tr>';
        }else{
        	//CREDIT CARD NO
        	$cad2.='   <tr>
		              <th></th>
		              <th></th>
		              <th>'. trans('calculator.deposit_month') .'</th>
		              <th>'.$item->paymentData[0]->pd_total_next_month.'</th>
		            </tr>';
        }
        
	 $cad2.= '<tr>
	              <th></th>
	              <th></th>
	              <th>'. trans('calculator.prepayment') .'</th>
	              <th>'.$item->paymentData[0]->pd_total_prepay.'</th>
	            </tr>' ;
        
        $deposito = Deposito::where(['storage_id'	=> $item->paymentData[0]->storage_id,
        							 'user_id'		=> $item->paymentData[0]->user_id
        						])->first();
        
        if(is_null($deposito)){
        	$depositoPagado = 0;
        	$idDeposito = 0;
        }else{
        	$depositoPagado = $deposito->de_pagado;
        	$idDeposito = $deposito->id;
        }
        
        
        $cad2 .= '<tr style="display:none">
		              <td>
	        			<input type="hidden" value="'.$item->paymentData[0]->storage_id.'" id="idStorageOrderDetail" />
	        			<input type="hidden" value="'.$item->paymentData[0]->user_id.'" id="idClienteOrderDetail" />
	        			<input type="hidden" value="'.$item->paymentData[0]->pd_total_next_month.'" id="idTotalNextMonthOrderDetail" />

	        			<input type="hidden" value="'.$item->paymentData[0]->user_id.'" id="idClienteOrderDetail" />
	        			<input type="hidden" value="'.$item->paymentData[0]->id.'" id="paymentIdOrderDetail" />

	        			<input type=hidden" value="'.$item->paymentData[0]->creditcard.'" id="FlagCreditCard" />		
	        					
	        					
	        			<input type="hidden" value="'.$depositoPagado.'" id="depositoPagado" />
	        					
	        			<input type="hidden" value="'.$idDeposito.'" id="idDeposito" />
		              </td>
	        		</tr>'; 
        
        
        $cad .= $cad2;
        //$cad .= $endWrapItem;

      }

      return $cad;
    }


    public function getOrderData(Request $data)
    {
      $d = Orders::with('Client','OrderDeatil')->where('id','=',$data['order_id'])->first();

      //dd($d);

      $client = $d->Client->name.' '.$d->Client->lastName;

      return ['client' => $client,'idClient' => $d->Client->id];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $data)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $data)
    {
        $idOrder = $data['idOrder'];
        $order = Orders::where('id','=',$idOrder)->first();
        //return [$order];

        $order->active = 0;
        $res = $order->save();

        if($res){
          return ['success' => true];
        } else{
          return ['success' => false];
        }
    }

    public function closeOrder(Request $data)
    {
        \Session::put('order', 0);
        if ($data->ajax()) {
            return['success' => true];
        }
    }

    public function getNotificaciones(){
      $orders = Orders::with('Client')->where('Active', '!=', '0')->orderBy('id','DESC')->get();
      $count = count($orders);
      $notif = $orders;
      return ['success' => true, 'count' => $count, 'notif' => $notif];
    }

    public function assignOrder(Request $data){
      $order = Session::get('order');
      //dd($order);
      if ($order == 0 || is_null($order)) {
        return ['success' => false];
      }else{
        $orderEdit = Orders::where('id','=', $order)->first();
        $orderEdit->user_id = $data['user_id'];
        $res = $orderEdit->save();
        
        $paymentEdit = PaymentData::where('order_id','=', $order)->first();
        $num = count($paymentEdit);
        if($num > 0){
          
          //ACTUALIZA DEPOSITO
          $deposito = Deposito::where([
          		'storage_id' 	=> $paymentEdit->storage_id,
          		'user_id'		=> $paymentEdit->user_id])->first();
          
          if(!is_null($deposito)){
	          $deposito->user_id 	= $data['user_id'];
	          $deposito->save();
          }
          
          //ACTUALIZA PATYMENTDATA
          $paymentEdit->user_id = $data['user_id'];
          $res = $paymentEdit->save();
          
        }

        if ($data['redirect'] == true) {
          # code...
        }else{
          \Session::put('order', 0);
          return ['success' => true, 'order' => $order];
        }

      }
    }
}
