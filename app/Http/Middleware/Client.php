<?php

namespace App\Http\Middleware;

use Closure;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      switch ($this->auth->user()->K_Role) {
        case '1':
          return redirect()->to('administrator');
          break;

        case '2':
          return redirect()->to('manager');
          break;

        case '3':
          //return redirect()->to('client');
          break;

        default:
          //return redirect()->to('client');
          break;
      }

      return $next($request);
    }
}
