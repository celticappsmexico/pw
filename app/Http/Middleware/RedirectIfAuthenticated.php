<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            //return redirect('/home');
            switch ($this->auth->user()->Role_id) {
              case '1':
                return redirect()->to('administrator');
                break;

              case '2':
                return redirect()->to('manager');
                break;

              case '3':
                return redirect()->to('client');
                break;

              default:
                return redirect()->to('login');
                break;
            }
        }

        return $next($request);
    }
}
