<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'user*',
        'ws*',
    	'users.validate.username',
    	'api.pw.obtiene_comprobantes',
    	'obtiene_comprobantes',
    	'api.pw.actualiza_comprobantes',
    	'actualiza_comprobantes',
    	'api.pw.actualiza_comprobantes_erroneos',
    	'actualiza_comprobantes_erroneos',
    	'api.pw.actualiza_secuencia',
    	'actualiza_secuencia',
		'recurrent_billing',
		'braintree_log.webhook',
    	'api.pw.update_billing',
		'update_billing',
		'auth/login',
		'wsResetPassword',
		'password/postReset',
		'password/reset'
    ];
}
