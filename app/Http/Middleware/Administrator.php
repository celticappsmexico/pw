<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;

use Closure;
use Session;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
      switch ($this->auth->user()->Role_id) {
        case '1':
          //return redirect()->to('administrator');
          break;

        case '2':
          return redirect()->to('manager');
          break;

        case '3':
          return redirect()->to('client');
          break;

        default:
          return redirect()->to('home');
          break;
      }

      return $next($request);
    }
}
