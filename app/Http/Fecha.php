<?php

	namespace App\Http;

	/**
	 * Clase de utilería para manipular fechas
	 */


	class Fecha {

		/**
		 * Checa si la fecha es un ultimo
		 * dia del mes.
		 * @param $fecha YYYY-MM-DD
		 * @return BOOLEAN
		 */

		function getIDFechaUnico(){

			$time = time();
			$date = date('Y-m-d H:i:s',$time);
			$date = str_replace(":","",$date);
			$date = str_replace("-","",$date);
			$date = str_replace(" ","",$date);
			return $date;
		}

		function esUltimoDiaMes($fecha) {

			$resp = false;

			$arrFecha = explode("-",$fecha);

			$anio = (int)$arrFecha[0];
			$mes = (int)$arrFecha[1];
			$dia = (int)$arrFecha[2];

			$sigDia = $dia + 1;
			$resp = checkdate($mes,$sigDia,$anio);

			if($resp == 1){
				$resp = 0;
			}else{
				$resp = 1;
			}

			return $resp;
		}

		/**
		 * Devuelve Ultimo Dia del Mes
		 * Recibe Fecha Dentro del mes
		 * @param $fecha YYYY-MM-DD
		 * @return unknown_type
		 */
		function getUltimoDiaMes($fecha) {

			//return $fecha;
			$arrFecha = explode("-",$fecha);
			$anio = $arrFecha[0];
			$mes = $arrFecha[1];
			$dia = $arrFecha[2];
			$diaTmp = $dia;

			do{
				$dia = $diaTmp;
				$diaTmp = (int)$dia +1;
				$valido = checkdate($mes,$diaTmp,$anio);
			}while($valido);

			return $anio.'-'.$mes.'-'.$dia;
		}

		function getUltimoDiaMes2($fecha) {

			return $fecha;
			$arrFecha = explode("-",$fecha);
			$anio = $arrFecha[0];
			$mes = $arrFecha[1];
			$dia = $arrFecha[2];
			$diaTmp = $dia;

			do{
				$dia = $diaTmp;
				$diaTmp = (int)$dia +1;
				$valido = checkdate($mes,$diaTmp,$anio);
			}while($valido);

			return $anio.'-'.$mes.'-'.$dia;
		}
		/**
		 * Devuelve Ultima Fecha (YYYY-MM-DD) del Mes
		 * Recibe Fecha Dentro del mes
		 * @param $fecha YYYY-MM-DD
		 * @return (YYYY-MM-DD)
		 */
		function getUltimaFechaMes($fecha) {

			$arrFecha = explode("-",$fecha);
			$anio = $arrFecha[0];
			$mes = $arrFecha[1];
			$dia = $arrFecha[2];
			$diaTmp = $dia;

			do{
				$dia = $diaTmp;
				$diaTmp = (int)$dia +1;
				$valido = checkdate($mes,$diaTmp,$anio);
			}while($valido);

			$f = $anio."-".$this->get2Digitos($mes)."-".$this->get2Digitos($dia);

			return $f;
		}

		function getPrimerFechaMes($fecha) {

			$arrFecha = explode("-",$fecha);
			$anio = $arrFecha[0];
			$mes = $arrFecha[1];
			$dia = $arrFecha[2];
			$diaTmp = $dia;
			$f = $anio."-".$this->get2Digitos($mes)."-01";
			return $f;
		}

		/**
		 * Completa 2 digitos
		 * @param $num
		 * @return $numDos
		 */
		function get2Digitos($num) {
			$numDos = $num;

			if(strlen($num)==1){
				$numDos = "0".$num;
			}

			return $numDos;
		}

		/**
		 * Dias entre 2 fechas
		 * fecha1 < fecha2
		 * @param $fecha1, $fecha2
		 * @return $dias
		 */
		function getDiferenciaFechas($beginDate,$endDate) {

			 //explode the date by "-" and storing to array
   			$date_parts1=explode("-", $beginDate);
   			$date_parts2=explode("-", $endDate);
   			//gregoriantojd() Converts a Gregorian date to Julian Day Count
   			$start_date= gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
   			$end_date= gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);

   			return $end_date - $start_date;

		}

		function getDaysBetween($d1,$d2){
			$date1 = new \DateTime($d1);
			$date2 = new \DateTime($d2);

			$diff = $date2->diff($date1)->format("%a");
			return $diff;
		}

		/**
		 * Extrae el dia de una fecha en formato YYYY-MM-DD
		 * @param $fecha
		 * @return DD
		 */
		function getDia($fecha) {
			$arr = explode("-",$fecha);
			return $arr[2];
		}

			/**
		 * Extrae el mes de una fecha en formato YYYY-MM-DD
		 * @param $fecha
		 * @return MM
		 */
		function getMes($fecha) {
			$arr = explode("-",$fecha);
			return $arr[1];
		}

		/**
		 * Extrae el año de una fecha en formato YYYY-MM-DD
		 * @param $fecha
		 * @return YYYY
		 */
		function getAnio($fecha) {
			$arr = explode("-",$fecha);
			return $arr[0];
		}

		/**
		 * Regresa dia de hoy formato YYYY-MM-DD
		 * @param NULL
		 * @return fecha YYYY-MM-DD
		 */
		function getDiaHoy() {
			date_default_timezone_set("America/Mexico_City");
			$date = getdate();
			$ret = $date['year']."-".$this->get2Digitos($date["mon"])."-".$this->get2Digitos($date["mday"]);
			return $ret;
		}

		/**
		 * Regresa formato HH:MM:SS / H24
		 */
		function getHoraHoy(){
			date_default_timezone_set("America/Mexico_City");
			$date = getdate();
			$ret = $this->get2Digitos($date['hours']).":".$this->get2Digitos($date["minutes"]).":".$this->get2Digitos($date["seconds"]);
			return $ret;
		}

		/**
		 * Regresa fecha de inicio de la quincena
		 * La fecha de entrada tiene que estar dentro del rango del a quincena
		 * @param $fecha YYYY-MM-DD
		 * @return $fechaI (YYYY-MM-DD)
		 */
		function getFechaInicioQuincena($fecha) {

			$fechaInicio = "";
			$diaI = "";
			$dia = $this->getDia($fecha);

			if((int)$dia <=15){
				$diaI = 1;
			}else{
				$diaI = 16;
			}
			$fechaInicio = $this->getAnio($fecha)."-"
							.$this->get2Digitos($this->getMes($fecha))
							."-".$this->get2Digitos($diaI);
			return $fechaInicio;
		}

			/**
		 * Regresa fecha de final de la quincena
		 * La fecha de entrada tiene que estar dentro del rango del a quincena
		 * @param $fecha YYYY-MM-DD
		 * @return $fechaF (YYYY-MM-DD)
		 */
		function getFechaFinalQuincena($fecha) {

			$fechaFinal = "";

			$diaF = "";
			$dia = $this->getDia($fecha);

			if((int)$dia <=15){
				$diaF = 15;
			}else{
				$diaF = $this->getUltimoDiaMes($fecha);
			}
			$fechaFinal = $this->getAnio($fecha)."-"
							.$this->get2Digitos($this->getMes($fecha))
							."-".$this->get2Digitos($diaF);
			return $fechaFinal;
		}


			/**
		 * Años entre 2 fechas
		 * @param $fechaI
		 * @param $fechaF
		 * @return anios
		 */
		function getYearsBetween($fechaI,$fechaF){

			$anioI = $this->getAnio($fechaI);
			$anioF = $this->getAnio($fechaF);

			return $anioF - $anioI;
		}

		/**
		 * Regresar true si f1 es mayor o IGUAL a f2
		 * @param $f1
		 * @param $f2
		 * @return unknown_type
		 */
		function mayorIgualA($f1,$f2){

			//echo "<br/>".$f1." es mayor o igual a ".$f2;

			$y1 = $this->getAnio($f1);
			$m1 = $this->getMes($f1);
			$d1  = $this->getDia($f1);

			$y2 = $this->getAnio($f2);
			$m2 = $this->getMes($f2);
			$d2  = $this->getDia($f2);

			$unixTime1 = mktime(0,0,0,$m1,$d1,$y1);
			$unixTime2 = mktime(0,0,0,$m2,$d2,$y2);

			if($unixTime1 >= $unixTime2){
				return true;
			}else{
				return false;
			}

		}
			function mayorA($f1,$f2){

			//echo "<br/>".$f1." es mayor o igual a ".$f2;

			$y1 = $this->getAnio($f1);
			$m1 = $this->getMes($f1);
			$d1  = $this->getDia($f1);

			$y2 = $this->getAnio($f2);
			$m2 = $this->getMes($f2);
			$d2  = $this->getDia($f2);

			$unixTime1 = mktime(0,0,0,$m1,$d1,$y1);
			$unixTime2 = mktime(0,0,0,$m2,$d2,$y2);

			if($unixTime1 > $unixTime2){
				return true;
			}else{
				return false;
			}

		}

		/**
		 * Meses entre f1 y f2
		 * Cond: from < to
		 * @param $f1
		 * @param $f2
		 * @return unknown_type
		 */
		function getMonthsBetween($from,$to) {

			$fLoop = $to;
			$yLoop = $this->getAnio($fLoop);
			$mLoop = $this->getMes($fLoop);
			$dLoop = $this->getDia($fLoop);

			$meses = 0;
			$continue = true;

			do{
				// resta un mes
				if ($mLoop == 1){
					// cambio de año
					$mLoop = 12;
					$yLoop = $yLoop - 1;
				}else{
					$mLoop = $mLoop -1;
				}
				// ------------
				$fTest = $this->makeDate($yLoop,$mLoop,$dLoop);

				if($this->mayorIgualA($fTest,$from)){
					$meses++;
				}else{
					$continue = false;
				}

			}while($continue);

			return $meses;
		}


		/**
		 * Regresa fecha formateada yyyy-mm-dd
		 * @param $y
		 * @param $m
		 * @param $d
		 * @return unknown_type
		 */
		function makeDate($y,$m,$d) {

			return $y.'-'.$this->get2Digitos($m).'-'.$this->get2Digitos($d);

		}

		/**
		 * Suma un dia a la fecha
		 * @param $fecha (YYY-MM-DD)
		 * @return $fecha (YYY-MM-DD)
		 */
		function addDay($fecha) {

			$newDate = "";

			$y = $this->getAnio($fecha);
			$m = $this->getMes($fecha);
			$d = $this->getDia($fecha);

			$d = (int)$d + 1;

			if(checkdate($m,$d,$y)){
				// valido
				$newDate = $this->makeDate($y,$m,$d);
			}else{
				if($m == 12){
					$newDate = $this->makeDate($y+1,1,1);
				}else{
					$newDate = $this->makeDate($y,$m+1,1);
				}
			}
			return $newDate;
		}

			function sumaDias($fecha,$num_dias) {

				$newdate = strtotime ( '+'.$num_dias.' day' , strtotime ( $fecha ) ) ;
				$newdate = date ( 'Y-m-j' , $newdate );

			return $newdate;
            }
		/**
		 * Suma un dia a la fecha
		 * @param $fecha (YYY-MM-DD)
		 * @return $fecha (YYY-MM-DD)
		 */
		function minusDay($fecha) {

			$newDate = "";

			$y = $this->getAnio($fecha);
			$m = $this->getMes($fecha);
			$d = $this->getDia($fecha);

			$d = (int)$d - 1;

			if(checkdate($m,$d,$y)){
				// valido
				$newDate = $this->makeDate($y,$m,$d);
			}else{
				if($m == 12){
					$newDate = $this->getUltimaFechaMes($this->makeDate($y-1,1,1));
				}else{
					$newDate = $this->getUltimaFechaMes($this->makeDate($y,$m-1,$d));
				}
			}
			return $newDate;
		}

		/**
		 * Recibe 12/21/2013 3:28:25 AM' regresa 2013-12-10 04:18:22
		 */
		function toStdDatetimeFromENG($str){

			$arrTmp = explode(" ",$str);
			$fecha = $arrTmp[0];
			$hora = $arrTmp[1];
			$ampm = $arrTmp[2];

			$arrFecha = explode("/",$fecha);
			$mes = $arrFecha[0];
			$dia = $arrFecha[1];
			$anio = $arrFecha[2];

			$arrHoras = explode(":",$hora);
			$hr = $arrHoras[0];
			$min = $arrHoras[1];
			$seg = $arrHoras[2];

			if($ampm == "PM"){
				$hr = $hr + 12;
			}

			return $anio."-".$this->get2Digitos($mes)."-".$this->get2Digitos($dia).
						" ".$this->get2Digitos($hr).":".$this->get2Digitos($min).":".
						$this->get2Digitos($seg);

		}

		function fechaGregorian($fecha)
		{
					$y = $this->getAnio($fecha);
					$m = $this->getMes($fecha);
					$d = $this->getDia($fecha);
					$jd=cal_to_jd(CAL_GREGORIAN,$m,$d,$y);
					return $jd;
		}
		function numsDomingo ($fecha1,$fecha2)		{

					$domingos = 0;
					$f1 = $fecha1;
					while ($this->mayorA($f1,$fecha2)==false )
					{
						$fgreg = $this->fechaGregorian($f1);
						$dia_semana = jddayofweek($fgreg,0);
						//echo 'dia:'.$dia;
						if ($dia_semana == '0')
						{
							$domingos = $domingos + 1;
					    }
					    $f1 = $this->addDay($f1);
					    //echo 'f1:'.$f1;
					    //echo '<br>';
					}
                    return $domingos;
				}

		function parseFechaCSD($fecha){

			// May 7 16:01:29 2017 GMT
			//echo "   *$fecha*  ";
			$fecha = preg_replace("/ +/","#",$fecha);
			//echo "   *$fecha*  ";
			$ret = "";
			$arrDatos = explode("#",$fecha);
			$mes = $arrDatos[0];
			$dia = $arrDatos[1];
			$anio = $arrDatos[3];

			//echo " anio $anio  mes $mes dia $dia  ";

			switch ($mes){

				case "Jan":
					$mes = "01";
				break;
				case "Feb":
					$mes = "02";
				break;
				case "Mar":
					$mes = "03";
				break;
				case "Apr":
					$mes = "04";
				break;
				case "May":
					$mes = "05";
				break;
				case "Jun":
					$mes = "06";
				break;
				case "Jul":
					$mes = "07";
				break;
				case "Aug":
					$mes = "08";
				break;
				case "Sep":
					$mes = "09";
				break;
				case "Oct":
					$mes = "10";
				break;
				case "Nov":
					$mes = "11";
				break;
				case "Dec":
					$mes = "12";
				break;
			}
			$ret = $anio."-".$mes."-".$this->get2Digitos($dia);
			return $ret;
		}

		function mapMes_Diminutivo($mes){

			$arrMeses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dec");
			return $arrMeses[$mes-1];

		}

		function fechaTipoMysql($fecha) {
			$fecha = str_replace('/', '-', $fecha);
			$time = strtotime($fecha);
			$newformat = date('Y-m-d',$time);
			return $newformat;
			/*return $fecha;*/
		}

		function fechaPw($fecha) {
			//$fecha = str_replace('-', '/', $fecha);
			$time = strtotime($fecha);
			//$newformat = date('Y-m-d',$time);
			$newformat = date('d/m/Y',$time);
			return $newformat;
			/*return $fecha;*/
		}

		// suma meses a una fecha
		function sumaMeses($fecha,$meses) {
			$meses = $meses * 30;
			$fecha = new \DateTime($fecha);
			$intervalo = new \DateInterval('P'.$meses.'D');
			$fecha->add($intervalo);
			$nuevafecha = $fecha->format('Y-m-d');

			//$nuevafecha = strtotime ( '+'.$meses.' month' , strtotime ( $fecha ) ) ;
			//$nuevafecha = date ( 'Y-m-d' , $nuevafecha );

			return $nuevafecha;
		}

	}

?>
