<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {

  //API Rest
  Route::resource('user', 'UserController',['only' => ['index','store', 'update', 'destroy', 'show']]);


  //Ruta para pruebas de funciones
  Route::any('wsTest',['as'=>'wsTest', 'uses'=>'InvoiceController@test']);

  //Rutas para moviles:
  Route::get('getToken',['as'=>'getToken', 'uses'=>'pwController@getToken']);
  Route::post('wsLogin',['as'=>'wsLogin', 'uses'=>'pwController@getLogin']);
    //Registration
    Route::post('wsRegistration', ['as' => 'wsRegistration', 'uses' => 'pwController@store']);
    //get 2 storages
    Route::post('wsStorages', ['as' => 'wsStorages','uses'=>'CalculatorController@show']);
    //get data box
    Route::any('wsGetDataBox', ['as' => 'wsGetDataBox','uses'=>'StoragesController@edit']);
    //map whit id box
    Route::get('wsMap', ['as' => 'wsMap','uses'=>'StoragesController@map']);
    //Route::any('wsGetStorages', ['as' => 'wsGetStorages','uses'=>'StoragesController@getStorages']);
    Route::any('wsGetUserStorages',['as'=>'wsGetUserStorages', 'uses'=>'StoragesController@getUserStorages']);
    Route::any('wsGetStoragesSizes',['as'=>'wsGetStoragesSizes', 'uses'=>'StoragesController@getStoragesSizes']);
    // send mail:
    Route::any('wsSendMail', ['as' => 'wsSendMail','uses'=>'MailController@send']);
    // articulos para la calculadora:
    
    Route::any('wsSendTermNotifAuto', ['as' => 'wsSendTermNotifAuto','uses'=>'StoragesController@setTermNoticeAutomatic']);
    Route::any('wsSendTermNotif', ['as' => 'wsSendTermNotif','uses'=>'StoragesController@setTermNotice']);
    //Genera factura
    Route::any('wsGenInvoice', ['as' => 'wsGenInvoice','uses'=>'StoragesController@genInvoice']);
    //cart
    Route::post('wsAddtocart', ['as'=>'wsAddtocart','uses'=>'cart@create']);
    Route::post('wsOrders', ['as'=>'wsOrders','uses'=>'cart@show']);
    Route::post('wsOrderdetail', ['as'=>'wsOrderdetail','uses'=>'cart@showDetailOrder']);
      Route::any('wsDeleteOrder', ['as'=>'wsDeleteOrder','uses'=>'cart@destroy']);

    // cata countries
    Route::any('wsCataCountries', ['as'=>'wsCataCountries','uses'=>'pwController@cata_countries']);
    //profile
    Route::any('wsProfile',['as'=>'wsProfile', 'uses'=>'pwController@showWs']);
    Route::any('wsUpdateAvatar',['as'=>'wsUpdateAvatar', 'uses'=>'pwController@update_avatar']);
    Route::any('wsEditClient',['as'=>'wsEditClient', 'uses'=>'pwController@update']);
    //catalogos
    Route::any('wsCataTerm',['as'=>'wsCataTerm', 'uses'=>'StoragesController@cataterm']);
    Route::any('wsCataPrepay',['as'=>'wsCataPrepay', 'uses'=>'StoragesController@cataprepay']);
    Route::any('wsGetSettings', ['as'=>'wsGetSettings','uses'=>'SettingsController@show']);


    //NUEVOS WS... APP
    Route::post ( 'wsCalculator', [
    		'uses' => 'APIController@wsCalculator',
    		'as' => 'wsCalculator'
    ] );
    
    Route::post ( 'wsGetStorages', [
    		'uses' => 'APIController@wsGetStorages',
    		'as' => 'wsGetStorages'
    ] );
    
    Route::post ( 'wsClientToken', [
    		'uses' => 'APIController@wsClientToken',
    		'as' => 'wsClientToken'
    ] );
    
    Route::post ( 'wsListBoxes', [
    		'uses' => 'APIController@wsListBoxes',
    		'as' => 'wsListBoxes'
    ] );
    
    Route::post ( 'wsGetSizes', [
    		'uses' => 'APIController@wsGetSizes',
    		'as' => 'wsGetSizes'
    ] );
    
    Route::post ( 'wsCheckDate', [
    		'uses' => 'APIController@wsCheckDate',
    		'as' => 'wsCheckDate'
    ] );
    
    Route::post ( 'wsCataEstimator', [
    		'uses' => 'APIController@wsCataEstimator',
    		'as' => 'wsCataEstimator'
    ] );
    
    Route::post ( 'wsCheckPerfil', [
    		'uses' => 'APIController@wsCheckPerfil',
    		'as' => 'wsCheckPerfil'
    ] );
    
    Route::post ( 'wsSaveCard', [
    		'uses' => 'APIController@wsSaveCard',
    		'as' => 'wsSaveCard'
    ] );
    
    Route::post ( 'wsPaySuscribe', [
    		'uses' => 'APIController@wsPaySuscribe',
    		'as' => 'wsPaySuscribe'
    ] );
    
    Route::post ( 'wsPay', [
    		'uses' => 'APIController@wsPay',
    		'as' => 'wsPay'
    ] );
    
    Route::any('wsArticles', [
    		'uses'=>'APIController@getArticulos',
    		'as' => 'wsArticles'
    ]);
    
    Route::post ( 'wsSuscribe', [
    		'uses' => 'APIController@wsSuscribe',
    		'as' => 'wsSuscribe'
    ] );
    
    //WS UPDATE CLIENT
    Route::any('wsUpdateClientInformation',[
    		'uses' => 'APIController@wsUpdateClientInformation',
    		'as' => 'wsUpdateClientInformation'
    ]);

    //WS Login
    Route::any('wsLogin',[
        'uses' => 'APIController@wsLogin',
        'as' => 'wsLogin'
    ]);

    //WS Give Notice
    Route::any('wsGiveNotice',[
        'uses' => 'APIController@wsGiveNotice',
        'as' => 'wsGiveNotice'
    ]);    

    //WS Invoices
    Route::any('wsInvoices',[
        'uses' => 'APIController@wsInvoices',
        'as' => 'wsInvoices'
    ]); 

    //WS Join
    Route::any('wsJoin',[
        'uses' => 'APIController@wsJoin',
        'as' => 'wsJoin'
    ]); 

    //WS IdFb
    Route::any('wsIdFb',[
        'uses' => 'APIController@wsIdFb',
        'as' => 'wsIdFb'
    ]); 

    //WS SaveOrder
    Route::any('wsSaveOrder',[
        'uses' => 'APIController@wsSaveOrder',
        'as' => 'wsSaveOrder'
    ]);

    //WS Reset Password
    Route::get('wsResetPassword', [
      'as' => 'wsResetPassword', 
      'uses' => 'Auth\PasswordController@getEmail'
    ]);
    Route::post('wsResetPassword', [
      'as' => 'wsResetPasswordPost', 
      'uses' => 'Auth\PasswordController@postEmail'
    ]);

    //WS Get Countries
    Route::get('wsGetCountries', [
      'as' => 'wsGetCountries', 
      'uses' => 'APIController@wsGetCountries'
    ]);

    //WS Delete Notification
    Route::post('wsDeleteNotification', [
      'as' => 'wsDeleteNotification', 
      'uses' => 'APIController@wsDeleteNotification'
    ]);

    //WS User Notifications
    Route::post('wsUserNotifications', [
      'as' => 'wsUserNotifications', 
      'uses' => 'APIController@wsUserNotifications'
    ]);

    //WS Delete All Notifications
    Route::post('wsDeleteAllNotifications', [
      'as' => 'wsDeleteAllNotifications', 
      'uses' => 'APIController@wsDeleteAllNotifications'
    ]);
    
    
    //FIN DE NUEVOS WS.. APP


  // Authentication routes...
  Route::get('auth/login', 'Auth\AuthController@getLogin');
  Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
  //Route::post('auth_login', ['as' =>'login', 'uses' => 'Auth\AuthController@postLogin']);
  Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

  // Registration routes...
  Route::get('auth/register', 'Auth\AuthController@getRegister');
  Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

  // Password reset link request routes...
  Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
  Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

  // Password reset routes...
  Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
  Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);
  Route::get('password/response', ['as' => 'password/response', 'uses' =>  'PasswordController@response']);

/* ********************************************************************************************************************* */

  Route::get('home', ['as' => 'home','uses'=>'HomeController@index']);
  Route::get('home/user', ['as' => 'home/user','uses'=>'HomeController@index']);
  Route::get('sizeCalculator', ['as' => 'sizeCalculator','uses'=>'SizeCalculatorController@index']);
  Route::post('searchStorage', ['as' => 'searchStorage','uses'=>'SizeCalculatorController@getStorage']);
  Route::get('validateUser', ['as' => 'validateUser','uses'=>'SizeCalculatorController@validateUser']);
  Route::get('/', 'HomeController@index');
  Route::get('/factura', 'HomeController@fac');
  Route::any('sendPay',['as'=>'sendPay', 'uses'=>'InvoiceController@pay']);
  Route::post('wsGetStorageInvoices',['as'=>'wsGetStorageInvoices', 'uses'=>'InvoiceController@getStorageInvoices']);
  Route::post('wsPayInvoices',['as'=>'wsPayInvoices', 'uses'=>'InvoiceController@payInvoice']);

  //Complete user...
  Route::get('homeCompleteUser', ['as' => 'homeCompleteUser','uses'=>'SizeCalculatorController@inicioComplete']);
  Route::post('completeUser',['as'=>'completeUser','uses'=>'SizeCalculatorController@completeUser']);

  // Rutas del idioma
  Route::get('lang/{lang}', function ($lang) {
      session(['lang' => $lang]);
      return \Redirect::back();
  })->where([
      'lang' => 'en|pl'
  ]);

  // Authentication routes...
  Route::get('auth/login', 'Auth\AuthController@getLogin');
  Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
  //Route::post('auth_login', ['as' =>'login', 'uses' => 'Auth\AuthController@postLogin']);
  Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

  // Registration routes...
  Route::get('auth/register', 'Auth\AuthController@getRegister');
  Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);


  // Password reset link request routes...
  Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
  Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

  // Password reset routes...
  /*
  Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
  Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);
  */
  // Mails
  Route::any('template', ['as' => 'template', 'uses' =>  'MailController@show']);

  // profile
  Route::any('profile',['as'=>'profile', 'uses'=>'pwController@show']);
  Route::any('updateAvatar',['as'=>'updateAvatar', 'uses'=>'pwController@update_avatar']);

  // assign storage
  Route::any('assignStorage',['as'=>'assignStorage', 'uses'=>'StoragesController@assignStorage']);
  Route::any('unAssignStorage',['as'=>'unAssignStorage', 'uses'=>'StoragesController@unAssignStorage']);
  Route::any('getUserStorages',['as'=>'getUserStorages', 'uses'=>'StoragesController@getUserStorages']);

  // routes by middleware
  Route::group(['middleware'=>['auth','Administrator'], 'prefix'=>'administrator'], function(){
    Route::get('map', ['as'=>'storagesmap','uses'=>'pwController@index']);

    Route::get ( 'users/{idUser?}', [
    		'uses' => 'UserController@index',
    		'as' => 'users'
    ] );

    Route::get ( 'users.create.credit_card/{id}', [
    		'uses' => 'UserController@createCreditCard',
    		'as' => 'users.create.credit_card'
    ] );


    Route::any ( 'cards_user.index.data', [
    		'uses' => 'CardsUserController@indexDT',
    		'as' => 'cards_user.index.data'
    ] );

    Route::post ( 'cards_user.store', [
    		'uses' => 'CardsUserController@store',
    		'as' => 'cards_user.store'
    ] );


    /////-DASHBOARD STORAGES
    Route::any ( 'storages_expires.index/{thisMonth}', [
            'uses' => 'StoragesController@indexExpires',
            'as' => 'storages_expires.index'
    ] );

    Route::any ( 'storages_expires.index.data', [
            'uses' => 'StoragesController@indexExpiresDT',
            'as' => 'storages_expires.index.data'
    ] );
    //-

    Route::get ( 'depositos.update.regresar_deposito/{id}', [
    		'uses' => 'DepositosController@updateRegresarDeposito',
    		'as' => 'depositos.update.regresar_deposito'
    ] );

    Route::get ( 'depositos.create/{storage_id}/{client_id}', [
    		'uses' => 'DepositosController@create',
    		'as' => 'depositos.create'
    ] );

    Route::post ( 'depositos.store', [
    		'uses' => 'DepositosController@store',
    		'as' => 'depositos.store'
    ] );
    
    Route::get ( 'depositos.pagar.create/{id}', [
    		'uses' => 'DepositosController@createPago',
    		'as' => 'depositos.pagar.create'
    ] );
    
    Route::post ( 'depositos.pagar.store', [
    		'uses' => 'DepositosController@storePago',
    		'as' => 'depositos.pagar.store'
    ] );
    
    
    //PREPAY
    Route::get ( 'storages.prepay.create/{storage_id}', [
    		'uses' => 'StoragesController@createPrepay',
    		'as' => 'storages.prepay.create'
    ] );
    
    Route::post ( 'storages.prepay.update', [
    		'uses' => 'StoragesController@updatePrepay',
    		'as' => 'storages.prepay.update'
    ] );
    
    Route::get ( 'storages.recurrent_billing_edit/{id}', [
    		'uses' => 'StoragesController@editRecurrentBilling',
    		'as' => 'storages.recurrent_billing_edit'
    ] );
    Route::post ( 'storages.update_recurrent_billing', [
    		'uses' => 'StoragesController@updateRecurrentBilling',
    		'as' => 'storages.update_recurrent_billing'
    ] );
    
    //PAYMENTS BRAINTREE
    Route::get ( 'create_transaction/{creditCardToken}/{customerId}/{planId}/{subscribed}/{storage}/{paymentid}/{transaction?}', [
    		'uses' => 'PaymentController@createTransaction',
    		'as' => 'create_transaction'
    ] );
    
    Route::get ( 'cancel_subscription/{suscription_id}', [
    		'uses' => 'PaymentController@cancelSubscription',
    		'as' => 'cancel_subscription'
    ] );

    Route::post('newClient',['as'=>'newClient', 'uses'=>'pwController@store']);
    Route::post('updateUser',['as'=>'updateUser', 'uses'=>'pwController@update']);
    Route::any('findClient',['as'=>'findClient', 'uses'=>'pwController@findUser']);
    Route::any('oportunities',['as'=>'oportunities', 'uses'=>'pwController@oportunitiesList']);
    Route::any('listOportunities',['as'=>'listOportunities', 'uses'=>'pwController@getOpotunitiesList']);
    Route::any('tenants',['as'=>'tenants', 'uses'=>'pwController@tenantsList']);
    Route::any('listTenants',['as'=>'listTenants', 'uses'=>'pwController@getTenantsList']);


    Route::get('warehouse',['as'=>'warehouse', 'uses'=>'warehouseController@index']);
    Route::post('newBuilding',['as'=>'newBuilding', 'uses'=>'warehouseController@create']);
    Route::post('updateBuilding',['as'=>'updateBuilding', 'uses'=>'warehouseController@update']);

    Route::get('levels/{id}',['as'=>'levels', 'uses'=>'LevelsController@index']);
    Route::post('newLevel',['as'=>'newLevel', 'uses'=>'LevelsController@create']);
    Route::post('updateLevel',['as'=>'updateLevel', 'uses'=>'LevelsController@update']);

    Route::get('storages/{id}',['as'=>'storages', 'uses'=>'StoragesController@index']);
    Route::post('newStorage',['as'=>'newStorage', 'uses'=>'StoragesController@create']);
    Route::post('updateStorage',['as'=>'updateStorage', 'uses'=>'StoragesController@update']);

    Route::any('calculator',['as'=>'calculator', 'uses'=>'CalculatorController@index']);
    
    Route::any ( 'calculator.calculate', [
    		'uses' => 'CalculatorController@calculate',
    		'as' => 'calculator.calculate'
    ] );
    
    Route::any('getLevels',['as'=>'getLevels', 'uses'=>'CalculatorController@getLevels']);
    Route::any('findStorage',['as'=>'findStorage', 'uses'=>'CalculatorController@show']);
    Route::any('findOneStorage',['as'=>'findOneStorage', 'uses'=>'CalculatorController@findOneStorage']);

    // system settings
    Route::any('systemSettings', ['as'=>'systemSettings','uses'=>'SettingsController@index']);
    Route::any('getSettings', ['as'=>'getSettings','uses'=>'SettingsController@show']);
    Route::any('updateSettings', ['as'=>'updateSettings','uses'=>'SettingsController@update']);
    
    Route::get ( 'app_users', [
            'uses' => 'ReportController@indexAppUsers',
            'as' => 'app_users.index'
    ] );
    Route::any ( 'app_users.index.data', [
        'uses' => 'ReportController@indexAppUsersDT',
        'as' => 'app_users.index.data'
    ] );
    Route::get ( 'users.invite_to_app/{id}', [
            'uses' => 'UserController@inviteToAPP',
            'as' => 'users.invite_to_app'
    ] );
    Route::get ( 'app_users.storages.index/{idUser}', [
        'uses' => 'ReportController@indexAppUsersStorages',
        'as' => 'app_users.storages.index'
    ] );
    Route::any ( 'app_users.storages.index.data', [
        'uses' => 'ReportController@indexAppUsersStoragesDT',
        'as' => 'app_users.storages.index.data'
    ] );
    
    
  }); //FIN DE ADMINISTRATOR

  // dataTables:
  Route::post('checkUsername',['as'=>'checkUsername', 'uses'=>'UserController@checkUsername']);
  Route::post('checkEmail',['as'=>'checkEmail', 'uses'=>'UserController@checkEmail']);


  Route::any ( 'users.data', [
  		'uses' => 'UserController@indexDT',
  		'as' => 'users.data'
  ] );
  
  Route::get ( 'cards_users.credit_card_braintree/{user_id}/{storage}/{paymentid}', [
  		'uses' => 'CardsUserController@showCreditCardBraintree',
  		'as' => 'cards_users.credit_card_braintree'
  ] );
  
  Route::any ( 'cards_users.credit_card_braintree.data', [
  		'uses' => 'CardsUserController@showCreditCardBraintreeDT',
  		'as' => 'cards_users.credit_card_braintree.data'
  ] );
  
  Route::get ( 'cards_users.credit_card_braintree.deposits/{user_id}/{storage}/{paymentid}', [
  		'uses' => 'CardsUserController@showCreditCardBraintreeDeposits',
  		'as' => 'cards_users.credit_card_braintree.deposits'
  ] );
  
  Route::any ( 'cards_users.credit_card_braintree.deposits.data', [
  		'uses' => 'CardsUserController@showCreditCardBraintreeDepositsDT',
  		'as' => 'cards_users.credit_card_braintree.deposits.data'
  ] );
  
  Route::get ( 'cards_users.credit_card_braintree.invoices/{user_id}/{storage}/{paymentid}', [
  		'uses' => 'CardsUserController@showCreditCardBraintreeInvoices',
  		'as' => 'cards_users.credit_card_braintree.invoices'
  ] );
  
  Route::any ( 'cards_users.credit_card_braintree.invoices.data', [
  		'uses' => 'CardsUserController@showCreditCardBraintreeInvoicesDT',
  		'as' => 'cards_users.credit_card_braintree.invoices.data'
  ] );

  Route::post ( 'users.validate.username', [
  		'uses' => 'UserController@checkUsername',
  		'as' => 'users.validate.username'
  ] );

  Route::get ( 'billing_index/{storage_id}/{cliente_id}', [
  		'uses' => 'BillingController@index',
  		'as' => 'billing_index'
  ] );

  Route::get ( 'billing_index.data', [
  		'uses' => 'BillingController@indexDT',
  		'as' => 'billing_index.data'
  ] );

    Route::get ( 'billing_current_month.index', [
        'uses' => 'BillingController@indexCurrentMonth',
        'as' => 'billing_current_month.index'
    ] );

    Route::get ( 'billing_current_month.index.data', [
        'uses' => 'BillingController@indexCurrentMonthDT',
        'as' => 'billing_current_month.index.data'
    ] );


  Route::get ( 'billing_edit.pay/{id}', [
  		'uses' => 'BillingController@editPay',
  		'as' => 'billing_edit.pay'
  ] );
  
  Route::get ( 'billing.edit_invoice/{id}', [
  		'uses' => 'BillingController@editInvoice',
  		'as' => 'billing.edit_invoice'
  ] );
  
  Route::post ( 'billing.update_invoice', [
  		'uses' => 'BillingController@updateInvoice',
  		'as' => 'billing.update_invoice'
  ] );

  Route::post ( 'billing_update.pay', [
  		'uses' => 'BillingController@updatePay',
  		'as' => 'billing_update.pay'
  ] );
  
  Route::get ( 'billing.cancel_paragon/{id}', [
  		'uses' => 'BillingController@cancelParagon',
  		'as' => 'billing.cancel_paragon'
  ] );
  

  Route::any ( 'billing.download_pdf/{id}', [
  		'uses' => 'BillingController@downloadPDF',
  		'as' => 'billing.download_pdf'
  ] );

  Route::any ( 'contract.current_contract', [
        'uses' => 'ContractController@currentContract',
        'as' => 'contract.current_contract'
    ] );
  
  Route::any ( 'billing.download_pdf_corregido/{id}', [
  		'uses' => 'BillingController@downloadPDFCorrected',
  		'as' => 'billing.download_pdf_corregido'
  ] );
  
  Route::any ( 'billing.download_pdf_copy/{id}', [
  		'uses' => 'BillingController@downloadPDFCopy',
  		'as' => 'billing.download_pdf_copy'
  ] );

  Route::get ( 'usuario_saldo_storage/{storage_id}/{cliente_id}', [
  		'uses' => 'BillingController@indexSaldo',
  		'as' => 'usuario_saldo_storage'
  ] );
  
  Route::get ( 'update.cc.paymentdata/{id}/{cc}', [
  		'uses' => 'StoragesController@updateCreditCardPaymentData',
  		'as' => 'update.cc.paymentdata'
  ] );
  
  Route::any ( 'notification.create', [
  		'uses' => 'NotificationsController@create',
  		'as' => 'notification.create'
  ] );
  
  Route::post ( 'notification.store', [
  		'uses' => 'NotificationsController@store',
  		'as' => 'notification.store'
  ] );
  
  Route::post('getNotificacionesAdmin', [
  		'uses'=>'NotificationsController@getNotificacionesAdmin',
  		'as'=>'getNotificacionesAdmin'
  ]);

    Route::get('notificaciones.destroy_admin/{id}', [
        'uses'=>'NotificationsController@destroyNotifAdmin',
        'as'=>'notificaciones.destroy_admin'
    ]);
  

  Route::any ( 'correspondence.create', [
      'uses' => 'CorrespondenceController@create',
      'as' => 'correspondence.create'
  ] );
  
  Route::post ( 'correspondence.store', [
      'uses' => 'CorrespondenceController@store',
      'as' => 'correspondence.store'
  ] );

    Route::any ( 'contracts.create', [
        'uses' => 'ContractController@create',
        'as' => 'contracts.create'
    ]);

    Route::any ( 'contracts.store', [
        'uses' => 'ContractController@store',
        'as' => 'contracts.store'
    ]);


  Route::post('deleteUser', ['as'=>'deleteUser','uses'=>'pwController@destroy']);
  Route::post('getDataUser', ['as'=>'getDataUser','uses'=>'pwController@edit']);

  Route::post('getBuildingList', ['as'=>'getBuildingList','uses'=>'warehouseController@getBuildingtList']);
  Route::post('deleteBuilding', ['as'=>'deleteBuilding','uses'=>'warehouseController@destroy']);
  Route::post('getDataBuilding', ['as'=>'getDataBuilding','uses'=>'warehouseController@edit']);

  Route::post('getLevelsList', ['as'=>'getLevelsList','uses'=>'LevelsController@getLevelsList']);
  Route::post('deleteLevel', ['as'=>'deleteLevel','uses'=>'LevelsController@destroy']);
  Route::post('getDataLevel', ['as'=>'getDataLevel','uses'=>'LevelsController@edit']);

  Route::post('getStoragesList', ['as'=>'getStoragesList','uses'=>'StoragesController@getStoragesList']);
  Route::post('deleteStorage', ['as'=>'deleteStorage','uses'=>'StoragesController@destroy']);
  Route::post('getDataStorage', ['as'=>'getDataStorage','uses'=>'StoragesController@edit']);

  // cart:
  Route::post('addtocart', ['as'=>'addtocart','uses'=>'cart@create']);
  Route::post('closeOrder', ['as'=>'closeOrder','uses'=>'cart@closeOrder']);
  Route::post('getNotificaciones', ['as'=>'getNotificaciones','uses'=>'cart@getNotificaciones']);
  Route::post('closeAssignClose', ['as'=>'closeAssignClose','uses'=>'cart@assignOrder']);
  Route::get('cart/{id}', ['as'=>'cart','uses'=>'cart@index']);
  Route::post('getOrdersList', ['as'=>'getOrdersList','uses'=>'cart@show']);
  Route::post('showDetailOrder', ['as'=>'showDetailOrder','uses'=>'cart@showDetailOrder']);
  Route::post('getOrderData', ['as'=>'getOrderData','uses'=>'cart@getOrderData']);

  // pay:
  Route::post('pay', ['as'=>'pay','uses'=>'InvoiceController@create']);

  Route::get('downloadInvoice/{var}', ['as'=>'downloadInvoice','uses'=>'InvoiceController@downloadInvoice']);
  Route::any('wsJsonBilling',['as'=>'wsJsonBilling', 'uses'=>'InvoiceController@saveBtBilling']);

  // Activamos planes
  Route::any('wsActivateBtPlans',['as'=>'wsActivateBtPlans', 'uses'=>'InvoiceController@activateBtPlan']);
  // Desactivamos los storages que terminan su renta
  Route::any('wsEndStorageRent',['as'=>'wsEndStorageRent', 'uses'=>'InvoiceController@endStorageRent']);
  // generamos el cargo en el billing
  Route::any('wsChargeBilling',['as'=>'wsChargeBilling', 'uses'=>'InvoiceController@generateChargeBilling']);
  // cancelar una suscripcion
  Route::any('wsJsonBillingCancel',['as'=>'wsJsonBillingCancel', 'uses'=>'InvoiceController@cancelSuscription']);
  //test cargos
  Route::any('wsTestBilling',['as'=>'wsTestBilling', 'uses'=>'InvoiceController@testBtBilling']);

  //REPORTES

  Route::any('getHistory',['as'=>'getHistory', 'uses'=>'HistoryController@show']);
  Route::any('wsGetHistoryGraph',['as'=>'wsGetHistoryGraph', 'uses'=>'Reports@show']);
  Route::any('wsGetXlsHistory',['as'=>'wsGetXlsHistory', 'uses'=>'ReportsController@index']);
  Route::any('history',['as'=>'history', 'uses'=>'HistoryController@index']);

  //Billing Reports
  Route::any('reports',['as'=>'reports', 'uses'=>'ReportController@index']);
  Route::any('billing_reports',['as'=>'billing_reports', 'uses'=>'ReportController@billingReports']);
  Route::any('filterYear/{anio}/{levels}',['as'=>'filterYear', 'uses'=>'ReportController@filterYear']);
  Route::any('filterYearClient/{year}/{month}/{levels}',['as'=>'filterClient', 'uses'=>'ReportController@filterYearClient']);
  Route::any('getBillingClientDetails/{id}/{year}/{month}',['as'=>'getBillingClientDetails', 'uses'=>'ReportController@getBillingClientDetails']);

  Route::get ( 'bad_payers', [
  		'uses' => 'ReportController@indexBadPayers',
  		'as' => 'bad_payers'
  ] );

  Route::any ( 'bad_payers.data', [
  		'uses' => 'ReportController@indexBadPayersDT',
  		'as' => 'bad_payers.data'
  ] );

  Route::get('bad_payers.data.csv',[
  		'uses'	=>	'ReportController@indexBadPayersDTCSV',
  		'as'	=>	'bad_payers.data.csv'
  ]);

  Route::get ( 'invoices_report/{paragon}', [
  		'uses' => 'ReportController@indexInvoices',
  		'as' => 'invoices_report'
  ] );

  Route::any ( 'invoices_report.data', [
  		'uses' => 'ReportController@indexInvoicesDT',
  		'as' => 'invoices_report.data'
  ] );
  Route::get('invoices_report.data.csv/{fechaInicio}/{fechaFin}/{client}/{numberInvoices}/{from}/{to}/{paragon}',[
  		'uses'	=>	'ReportController@indexInvoicesDTCSV',
  		'as'	=>	'invoices_report.data.csv'
  ]);
  Route::get('invoices_report.data.zip/{fechaInicio}/{fechaFin}/{client}/{numberInvoices}/{from}/{to}/{paragon}',[
  		'uses'	=>	'ReportController@indexInvoicesDTZIP',
  		'as'	=>	'invoices_report.data.zip'
  ]);
  Route::get('invoices_report.data.zip_copies/{fechaInicio}/{fechaFin}/{client}/{numberInvoices}/{from}/{to}/{paragon}',[
  		'uses'	=>	'ReportController@indexInvoicesDTZIPCopies',
  		'as'	=>	'invoices_report.data.zip_copies'
  ]);
  Route::get('invoices_report.data.txt/{fechaInicio}/{fechaFin}/{client}/{numberInvoices}/{from}/{to}/{paragon}',[
  		'uses'	=>	'ReportController@indexInvoicesTXT',
  		'as'	=>	'invoices_report.data.txt'
  ]);
  
  Route::get ( 'jpk_fa', [
        'uses' => 'ReportController@indexJPKFA',
        'as' => 'jpk_fa'
  ] );
  Route::get ( 'jpk_fa.data', [
      'uses' => 'ReportController@indexJPKFADT',
      'as' => 'jpk_fa.data'
  ] );
  Route::get ( 'jpk_fa.generar/{fechaInicio}/{fechaFin}', [
      'uses' => 'ReportController@generarJPKFA_File',
      'as' => 'jpk_fa.generar'
  ] );

    #PAYMENTS_REPORT 
    Route::get ( 'report_payments', [
        'uses' => 'ReportController@indexPayments',
        'as' => 'report.payments_index'
    ]);

    Route::any ( 'report.payments_index.data', [
        'uses' => 'ReportController@indexPaymentsDT',
        'as' => 'report.payments_index.data'
    ]);
    
    Route::any ( 'report.payments_index.data.csv/{fechaInicio}/{fechaFin}/{paymentType}', [
        'uses' => 'ReportController@indexPaymentsDTCSV',
        'as' => 'report.payments_index.data.csv'
    ]);
    #END_PAYMENTS_REPORT

    #WEEKLY_REPORT 
    Route::get ( 'weekly_report', [
        'uses' => 'ReportController@indexWeekly',
        'as' => 'report.weekly_report'
    ]);

    Route::any ( 'report.weekly_report_index.data', [
        'uses' => 'ReportController@indexWeeklyDT',
        'as' => 'report.weekly_report_index.data'
    ]);
    
    Route::any ( 'report.weekly_report_index.data.csv/{fechaInicio}/{fechaFin}/{paymentType}', [
        'uses' => 'ReportController@indexWeeklyDTCSV',
        'as' => 'report.weekly_report_index.data.csv'
    ]);
    #END_WEEKLY_REPORT
    


  //Warehouses Reports
  Route::any('warehouseReport',['as'=>'warehouseReport', 'uses'=>'ReportWarehouseController@index']);

  //Tenants Repots
  Route::any('tenantsReport',['as'=>'tenantsReport', 'uses'=>'ReportTenantsController@index']);
  Route::any('tenantsReport/export',['as'=>'tenantsReport/export', 'uses'=>'ReportTenantsController@generarExcel']);

  //Boxes Repots
  Route::any('boxesReport',['as'=>'boxesReport', 'uses'=>'ReportBoxesController@index']);
  Route::any('boxesReportDays/{days}',['as'=>'boxesReportDays', 'uses'=>'ReportBoxesController@showBoxes']);
  Route::any('boxesReportDays/export/{days}',['as'=>'boxesReportDays/export', 'uses'=>'ReportBoxesController@generarExcel']);

  //Deposits Report
  Route::any('depositsReport',['as'=>'depositsReport', 'uses'=>'DepositsReportController@index']);

  //History occupancy rate
  Route::any('historyOccupancyRate',['as'=>'historyOccupancyRate', 'uses'=>'HistoryOccupancyRateController@index']);
  Route::any('filterHistoryOccupancyRate/{year}/{sqm}/{warehouse}',['as'=>'filterHistoryOccupancyRate', 'uses'=>'HistoryOccupancyRateController@filterHistoryOccupancyRate']);

  //Insurance Report
  Route::any('insuranceReport',['as'=>'insuranceReport', 'uses'=>'InsuranceReportController@index']);
  Route::any('insuranceReportFilter/{anio}/{mes}/{dia}',['as'=>'insuranceReportFilter', 'uses'=>'InsuranceReportController@dateFilter']);
  
  Route::any('correctedInvoicesReport',[
  		'uses' => 'ReportController@indexCorrectedInvoices',
  		'as' => 'correctedInvoicesReport'
  ]);
  
  Route::any('correctedInvoicesReport.data',[
  		'uses' => 'ReportController@indexCorrectedInvoicesDT',
  		'as' => 'correctedInvoicesReport.data'
  ]);
  
  Route::any('paragonCancelledReport',[
  		'uses' => 'ReportController@indexParagonCancelledReport',
  		'as' => 'paragonCancelledReport'
  ]);
  
  Route::any('paragonCancelledReport.data',[
  		'uses' => 'ReportController@indexParagonCancelledReportDT',
  		'as' => 'paragonCancelledReport.data'
  ]);
  //REPORTE DE INSURANCEDETAILS

  Route::get('correctedInvoicesReport.data.csv/{from}/{to}/{year}',[
      'uses'	=>	'ReportController@indexCorrectedInvoicesDTCSV',
  		'as'	=>	'correctedInvoicesReport.data.csv'
  ]);
  
//reporte de Insurance Report o insuranceDetails
 Route::get('insuranceReportDetails.data.csv/{fechaInicio}/{fechaFin}',[
      'uses'  =>  'ReportController@indexInsuranceReportDetailsCSV',
      'as'  =>  'insuranceReportDetails.data.csv'
  ]);

 //reporte de prepayment
  Route::get('prepayment.data.csv/{storage}',[
      'uses'  =>  'ReportController@indexPrepaymentReportCSV',
      'as'  =>  'prepayment.data.csv'
  ]);


  Route::get('correctedInvoicesReport.data.csv/{fromInvoice}/{toInvoice}/{yearInvoice}',[
      'uses'  =>  'ReportController@indexCorrectedInvoicesDTCSV',
      'as'  =>  'correctedInvoicesReport.data.csv'
  ]);


  Route::any('prepaymentReport',[
  		'uses' => 'ReportController@indexPrepayment',
  		'as' => 'prepaymentReport'
  ]);
  
  Route::any('prepaymentReport.data',[
  		'uses' => 'ReportController@indexPrepaymentDT',
  		'as' => 'prepaymentReport.data'
  ]);
  
  Route::get('prepaymentReport.data.csv',[
  		'uses'	=>	'ReportController@indexPrepaymentDTCSV',
  		'as'	=>	'prepaymentReport.data.csv'
  ]);
  
  //REPORTE DE BRAINTREE
  Route::any('braintreeReport',[
  		'uses' => 'ReportController@indexBraintree',
  		'as' => 'braintreeReport'
  ]);
  
  Route::any('braintreeReport.data',[
  		'uses' => 'ReportController@indexBraintreeDT',
  		'as' => 'braintreeReport.data'
  ]);
  
  //REPORTE DE INSURANCE
  Route::any('insuranceDetails',[
  		'uses' => 'ReportController@indexInsurance',
  		'as' => 'insuranceDetails'
  ]);
  
  Route::any('insuranceDetails.data',[
  		'uses' => 'ReportController@indexInsuranceDT',
  		'as' => 'insuranceDetails.data'
  ]);
  
  Route::any('insuranceDetails.data.xls',[
  		'uses' => 'ReportController@indexInsuranceDTXLS',
  		'as' => 'insuranceDetails.data.xls'
  ]);
  
  //WS IM;PRESION DE COMPROBANTES
  Route::any('obtiene_comprobantes',[
  		'uses' => 'APIController@getComprobantes',
  		'as' => 'api.pw.obtiene_comprobantes'
  ]);
  
  Route::any('actualiza_comprobantes',[
  		'uses' => 'APIController@updateComprobantes',
  		'as' => 'api.pw.actualiza_comprobantes'
  ]);
  
  Route::any('actualiza_comprobantes_erroneos',[
  		'uses' => 'APIController@updateComprobantesErroneos',
  		'as' => 'api.pw.actualiza_comprobantes_erroneos'
  ]);
  
  Route::any('actualiza_secuencia',[
  		'uses' => 'APIController@updateSequence',
  		'as' => 'api.pw.actualiza_secuencia'
  ]);
  
  Route::any('update_billing',[
  		'uses' => 'APIController@updateBilling',
  		'as' => 'api.pw.update_billing'
  ]);
});
	
	Route::post('recurrent_billing',[
			'uses' => 'PaymentController@recurrentBilling',
			'as' => 'recurrent_billing'
    ]);
    Route::post('braintree_log.webhook',[
            'uses' => 'BraintreeLogController@store',
            'as' => 'braintree_log.webhook'
    ]);
	/*
	Route::post('error_recurrent_billing',[
			'uses' => 'PaymentController@errorRecurrentBilling',
			'as' => 'error_recurrent_billing'
	]);
*/

Route::get ( 'billing.pay.retry_charge/{suscription_id}/{amount}/{billing_id?}', [
    'uses' => 'BillingController@payRetryCharge',
    'as' => 'billing.pay.retry_charge'
]);

Route::get ( 'contact_us', [
      'uses' => 'ContactController@index',
      'as' => 'contact_us'
]);

Route::post ( 'contact.store', [
      'uses' => 'ContactController@store',
      'as' => 'contact.store'
]);

Route::get ( 'confirm_corrected_invoice/{id}', [
    'uses' => 'BillingController@confirmCorrectInvoice',
    'as' => 'confirm_corrected_invoice'
]);