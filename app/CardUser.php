<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardUser extends Model
{
    //
	protected $table = 'cards_user';
	
	protected $fillable = [
			'user_id',
    		'cu_token_card',
    		'cu_customer_id',
			'cu_eliminado'
	];
	
	public function user() {
		return $this->belongsTo(User::class);
	}
}
