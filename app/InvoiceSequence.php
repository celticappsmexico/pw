<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceSequence extends Model
{
    //
	//Definicion de tabla
	protected $table = "invoice_sequence";
	
	//Definicion de campos
	protected $fillable = ["id","is_contador"];
	
	//Definicion de exclusion campos en respuesta JSON
	protected $hidden = [];
	
	//Definicion de realciones
}
