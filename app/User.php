<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username',
                            'avatar',
                            'email',
                            'password',
                            'name',
                            'lastName',
                            'companyName',
                            'peselNumber',
                            'idNumber',
                            'nipNumber',
                            'regonNumber',
                            'courtNumber',
                            'courtPlace',
                            'krsNumber',
                            'Role_id',
                            'userType_id',
                            'birthday',
                            'phone',
                            'remember_token',
                            'active',
                            'validate',
                            'flag_oportunity',
                            'oportunity_reminder',
                            'notes',
                            'fb_id',
                            'avatar_tocken',
                            'phone2',
                            'email2',
    						            'accountant_number',
    						            'accountant_code',
    						            'us_complete_profile',
    						            'appName',
                            'customer_id',
                            'movil_dispositive'
                          ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /*public function role(){
      return $this->belongsTo(Role::class,'Role_id');
    }*/

    public function RoleDesc(){
      return $this->belongsTo(Role::class,'Role_id');
    }

    public function UserType(){
      return $this->belongsTo(UserType::class,'userType_id');
    }

    public function UserAddress(){
      return $this->hasMany(clientAddress::class,'user_id');
    }

    public function UserLegal(){
      return $this->hasMany(LegalRepresentative::class,'user_id');
    }
    
    public function storages(){
    	return $this->hasMany(Storages::class,'user_id','id')
    			->where('active','1');
    }
}
