<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Enviroment
    |--------------------------------------------------------------------------
    |
    | Please provide the enviroment you would like to use for braintree.
    | This can be either 'sandbox' or 'production'.
    |
    */

	//'environment' => 'sandbox',
	'environment' 	=> env('ENVIRONMENT_BRAINTREE'),

	/*
    |--------------------------------------------------------------------------
    | Merchant ID
    |--------------------------------------------------------------------------
    |
    | Please provide your Merchant ID.
    |
    */
	//'merchantId' => '6z789r7xvc6ymcb4',
	'merchantId' 	=> env('MERCHANT_ID_BRAINTREE'),

	/*
    |--------------------------------------------------------------------------
    | Public Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Public Key.
    |
    */
	//'publicKey' => '5sqp927tjn3tyn45',
	'publicKey' => env('PUBLIC_KEY_BRAINTREE'),

	/*
    |--------------------------------------------------------------------------
    | Private Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Private Key.
    |
    */
	//'privateKey' => '018a75eee6265596e5e4c6a35abe3273',
	'privateKey' => env('PRIVATE_KEY_BRAINTREE'),

	/*
    |--------------------------------------------------------------------------
    | Client Side Encryption Key
    |--------------------------------------------------------------------------
    |
    | Please provide your CSE Key.
    |
    */
	'clientSideEncryptionKey' => 'MIIBCgKCAQEAyGPleIDC/X+D8+QcYHhyUXM+S+3JAZkdlZFKf2STnT9qZBUUb/cLORvVkYN1b3zScZXPY/QTvxYKQtXsL8+xBHvq+1uCvFzf1gp+d/3Kq/U5TpvoCQqGVx69PrmiCtkEgdq9Yf7PH5Gf17T3yxhFUMR+vKBG0K4AR7OFxDXEKBEMqSJUbr+eoaKoVy8N45Zj+U7fp1id8wk/vTXDXlpo5EHqMpEFgCBR6i5KTYHhrEPzhBk03Xu3u87+Dhv26a9jJn0+eOcAH+Jk+9648DX/bhdVxStPcJGDPVjW8ZLAGWmlV1R6DwhQGgHWJ8GmrKCC5+4JxhlIu9H8DDmuMJt5MQIDAQAB',

];
