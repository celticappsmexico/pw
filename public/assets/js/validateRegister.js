$('#forRegister').validate({
        rules: {
            username: {
                minlength: 3,
                required: true
            },
            name: {
                minlength: 3,
                maxlength: 45,
                required: true
            },
            email: {
                email: true,
                required: true
            },
            password: {
              required: true
            },
            password_confirmation: {
              required: true,
              equalTo: "#password"
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
