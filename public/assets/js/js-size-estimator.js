$(document).ready(function(){
    $('.Living.room').attr('class', 'Living.room active');
    $('.span_Living.room_total').attr('id', 'row_calculo_activo');
    $('#div_Living\\ room').show();
    $('#btn_search').addClass('disabled');
});

var totalSecciones = $('#totalSecciones').val();

/**
 * Función para pintar los textos con sus totales, dependiendo la sección que clickee el usuario.
 * @param  {[type]} spanTotal [Recibe el nombre de la sección clickeada].
 */
function pintarSpanTotal(spanTotal){
    var cadena = spanTotal,
    patron = / /g,
    nuevoValor    = ".",
    nuevaCadena = cadena.replace(patron, nuevoValor);

    var cadena2 = spanTotal,
    patron2 = / /g,
    nuevoValor2    = "\\ ",
    nuevaCadena2 = cadena2.replace(patron2, nuevoValor2);

    $('#row_calculo_activo').attr('id', 'row_calculo');
    $('.tab-pane').hide();
    $('#div_'+nuevaCadena2).show();
    $('.span_'+nuevaCadena+'_total').attr('id', 'row_calculo_activo');
}

var i = 0;
var total = 0.00;
/**
 * Función que suma el valor del objeto clickeado al total general y al total de su respectiva sección.
 */
$(document).on("click", "#sum", function(){
    $(this).parents('div').find('#link_limpiar').show();
    i = $(this).parent('div').find('#input-cantidad').val();
    totalSeccion = parseFloat($('.span_'+$(this).parent('div').find('#input-cantidad').attr('class')).text());

    if (i < 100) {
        i++;
        $(this).parent('div').find('#input-cantidad').val(i);
        num = $(this).parent('div').find('#input-cantidad').attr('dir');

        total += parseFloat(num);
        totalSeccion += parseFloat(num);

        $('#span_total').text(total.toFixed(2));
        $('.span_'+$(this).parent('div').find('#input-cantidad').attr('class')).text(totalSeccion.toFixed(2));
    }

    i = 0;
});

/**
 * Función que resta el valor del objeto clickeado al total general y al total de su respectiva sección.
 */
$(document).on("click", "#res", function(){
    i = $(this).parent('div').find('#input-cantidad').val();
    totalSeccion = parseFloat($('.span_'+$(this).parent('div').find('#input-cantidad').attr('class')).text());

    if (i > 0) {
        i--;
        $(this).parent('div').find('#input-cantidad').val(i);
        num = $(this).parent('div').find('#input-cantidad').attr('dir');

        total -= parseFloat(num);
        totalSeccion -= parseFloat(num);
    }

    if (total < 0.00) {
        total = 0.00;
    }

    if (totalSeccion == 0.00) {
        $(this).parents('div').find('#link_limpiar').hide();
    }

    $('#span_total').text(total.toFixed(2));
    $('.span_'+$(this).parent('div').find('#input-cantidad').attr('class')).text(totalSeccion.toFixed(2));

    i = 0;
});

/**
 * Función que limpia todas las cantidades de los objetos, resta el total de su respectiva sección al total general y
 * coloca en ceros el total de la sección.
 */
$(document).on("click", "#link_limpiar", function(){
    seccion = $(this).attr('class');
    valorSeccion = parseFloat($('.span_'+seccion).text());
    total -= valorSeccion;

    if (total < 0.00) {
        total = 0.00;
    }

    $('#span_total').text(total.toFixed(2));
    $('.span_'+seccion).text('0.00');
    $(this).parents('form')[0].reset();
    $(this).hide();
});

/**
 * Función para verificar si el campo total esta en cero, si lo esta, se le agrega la clase disabled al boton rentar storage,
 * si no, se le remueve la clase.
 * @type {[type]}
 */
$(document).on('DOMSubtreeModified', "#span_total", function(){
    valorCampoTotal = parseFloat($('#span_total').text());
    if (valorCampoTotal == 0) {
        $('#btn_search').addClass('disabled');
    }else {
        $('#btn_search').removeClass('disabled');
    }
});
