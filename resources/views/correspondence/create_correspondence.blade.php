<link rel="stylesheet" href="{{asset('assets/css/bootstrap3-wysihtml5.min.css')}}">

<form action="{{route('correspondence.store')}}" method="post" id="frmCorrespondence" enctype="multipart/form-data">
{{ csrf_field() }}
	<div class="modal-header">
		<h4 class="modal-title" id="">Create Collective Correspondence</h4>
	</div>
	<div class="modal-body">
	    <div class="row" >
	    	<div class="col-md-12">
	      		<div class="form-group">
	      			<label>Subject</label>
	      			<input type="text" name="subject" class="form-control">
	      		</div>

			</div>

	      	<div class="col-md-12">
	      		<div class="form-group">
	      			<label>Message</label>
	      			<textarea name="message" class="form-control textarea" rows="5"></textarea>
	      		</div>

			</div>
			
			<div class="col-md-4">
	      		<div class="form-group">
	      			<label>Only Admin</label><br/>
	      			<input type="radio" name="only_admin" class="chkOportunity" checked />
	      		</div>
			</div>
			<div class="col-md-4">
	      		<div class="form-group">
	      			<label>Active Users (Rent active)</label><br/>
	      			<input type="radio" name="only_admin" class="chkOportunity" checked />
	      		</div>
			</div>
			<div class="col-md-4">
	      		<div class="form-group">
	      			<label>All Users</label><br/>
	      			<input type="radio" name="only_admin" class="chkOportunity" checked />
	      		</div>
			</div>
			
		<!-- 
			<div class="col-md-12">
				<input type="checkbox" name="selectAllUsers" value="allUsers" disabled="disabled" checked >
				<label>All users </label>
			</div>
		
			<div class="col-md-6">
				<input type="file" name="attachFile" />
			</div>
		-->
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script src="{{asset('assets/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
<script>


$('#frmCorrespondence').validate({
	      rules: {
	          message: {
	        	  minlength: 5,	             
	              required: true
	          },
	          subject: {
	        	  minlength: 5,	             
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {

		      	//console.log($("#frmPayBilling").attr('action'));

	        	$("#frmCorrespondence").find('button').prop('disabled',false);
	        	
	             $.ajax({
	               url: $("#frmCorrespondence").attr('action'),
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               //console.info(data);
	               //alert(JSON.stringify(data));
	               var message = "";
	               if (data.returnCode == 200) {
		               
		                toastr["success"](data.msg);
		                $('#modal-generico-small').modal('hide');
	               }else{
	                 	toastr["error"](data.msg);
	                 	message = data.msg;
	               }
	
	             })
	             .fail(function() {
	               //console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               //console.log("complete");
	             });
	      return false; // required to block normal submit since you used ajax
	     }
	  });
</script>
