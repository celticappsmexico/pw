
		<div id="div-table-cards">
			<div class="col-md-12">
				<button type="button" class="btn btn-primary pull-right" id="btn-add-new-card">
					{{ trans('client.add_credit_card_user') }}
				</button>
			</div>
			
			<div class="col-md-12">
				<table class="table table-hover" id="cards-user-invoices-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>{{ trans('cart.card_type') }}</th>
							<th>{{ trans('cart.card_number') }}</th>
							<th>{{ trans('cart.card_expiration') }}</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
			
			<div class="col-md-12" id="div-add-new-card-invoices" style="display:none">
				<form id="credit-card-form-invoice" method="post" action="{{ route('cards_user.store') }}" role="from" enctype="multipart/form-data" >
					{{ csrf_field() }}
					
					<input type="hidden" name="user_id" value="{{$user_id}}" />
					<input type="hidden" name="ajax" value="1" />
					
					<div class="wrapBraintree">
						<div class="row">
							<div id="bt-dropin2-new"></div>
							
							<button type="submit" id="btn-submit-credit-card" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('client.send_lbl') }} </button>
		
						</div>
						<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
						<script>
								// We generated a client token for you so you can test out this code
								// immediately. In a production-ready integration, you will need to
								// generate a client token on your server (see section below).
								<?php
		
									$token = $clientToken = \Braintree_ClientToken::generate();
		
								?>
								clientToken = "<?php echo $token; ?>";
								
								braintree.setup(clientToken, "dropin", {
									container: "bt-dropin2-new"
								});
						</script>
		
		
		
					</div>
				</form>
			</div>
		</div>
	
<script>	
var oTableCardsUserB = $('#cards-user-invoices-table').DataTable({
	"bInfo" : false,
	processing: true,
	serverSide: true,
	bFilter : true,
	"order": [[ 0, 'desc' ]],
	ajax: {
		url : "{!! route('cards_users.credit_card_braintree.invoices.data') !!}",
		data: function (d) {
			d.user_id = '{{$user_id}}';
			d.storage_id = '{{$storage_id}}';
			d.paymentid = '{{$paymentid}}';
		}
	},
	columns: [
		{ data: 'id', name: 'id'},
		{ data: 'imageUrl', name: 'imageUrl'},
		{ data: 'maskedNumber', name: 'maskedNumber'},
		{ data: 'expirationDate', name: 'expirationDate'},
		{ data: 'select_card', name: 'select_card'},
	],
	language : {
		lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
		zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
		info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
		infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
		infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
		search:         "{{ trans('client.dt_find_lbl') }}:",
		paginate: {
			first:      "{{ trans('client.dt_first_lbl') }}",
			last:       "{{ trans('client.dt_last_lbl') }}",
			next:       "{{ trans('client.dt_next_lbl') }}",
			previous:   "{{ trans('client.dt_prev_lbl') }}"
		},
		loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
		processing:     "{{ trans('client.dt_processing_lbl') }}",
	}
});

$("#btn-add-new-card").click(function(event){
	$("#div-add-new-card-invoices").show();
	$("#cards-user-invoices-table").hide();
});


$('#credit-card-form-invoice').validate({
    rules: {
    	'credit-card-number': {
			required : true
        	}
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
	      	//console.log($("#frmPayBilling").attr('action'));

	      	//$("#btnSaveBilling").hide();            	   	
      		$("#credit-card-form-invoice").find('button').prop('disabled',true);
      	
           $.ajax({
             url: $("#credit-card-form-invoice").attr('action'),
             type: 'POST',
             data: $(form).serialize(),
           })
           .done(function(data) {
             //console.info(data);
             if (data.returnCode == 100) {
	         	toastr["error"](data.msg);
	         	$("#credit-card-form-invoice").find('button').prop('disabled',false);
             }else{
                 alert("ELSE");
               toastr["success"](data.msg);
               oTableCardsUserB.draw();
             }

           })
           .fail(function() {
             //console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             //console.log("complete");
           });

    	return false; // required to block normal submit since you used ajax
   }
});
</script>