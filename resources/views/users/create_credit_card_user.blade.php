
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">{{ trans('client.credit_cards_user') }}</h4>
	</div>

	<div class="modal-body" >

		<div id="div-table-cards">
			<div class="col-md-12">
				<button type="button" class="btn btn-primary pull-right" id="btn-add-new-card">
					{{ trans('client.add_credit_card_user') }}
				</button>
	
				<br/>
			</div>
			
			<div class="col-md-12">
				<table class="table table-hover" id="cards-user-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>{{ trans('cart.card_type') }}</th>
							<th>{{ trans('cart.card_number') }}</th>
							<th>{{ trans('cart.card_expiration') }}</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="col-md-12" id="div-add-new-card" style="display:none">
			<form id="credit-card-form" method="post" action="{{ route('cards_user.store') }}" role="from" enctype="multipart/form-data" >
				{{ csrf_field() }}
				
				<input type="hidden" name="user_id" value="{{$user_id}}" />
				
				<div class="wrapBraintree">
					<div class="row">
						<div id="bt-dropin2-new"></div>
						
						<button type="submit" id="btn-submit-credit-card" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('client.send_lbl') }} </button>
	
					</div>
					<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
					<script>
							// We generated a client token for you so you can test out this code
							// immediately. In a production-ready integration, you will need to
							// generate a client token on your server (see section below).
							<?php
	
								$token = $clientToken = \Braintree_ClientToken::generate();
	
							?>
							clientToken = "<?php echo $token; ?>";
							
							braintree.setup(clientToken, "dropin", {
								container: "bt-dropin2-new"
							});
					</script>
	
	
	
				</div>
			</form>
		</div>
	</div>
	
<script>	
var oTableCardsUser = $('#cards-user-table').DataTable({
	"bInfo" : false,
	processing: true,
	serverSide: true,
	bFilter : true,
	"order": [[ 0, 'desc' ]],
	ajax: {
		url : "{!! route('cards_user.index.data') !!}",
		data: function (d) {
			d.user_id = '{{$user_id}}';
		}
	},
	columns: [
		{ data: 'id', name: 'id'},
		{ data: 'imageUrl', name: 'imageUrl'},
		{ data: 'maskedNumber', name: 'maskedNumber'},
		{ data: 'expirationDate', name: 'expirationDate'},
	],
	language : {
		lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
		zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
		info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
		infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
		infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
		search:         "{{ trans('client.dt_find_lbl') }}:",
		paginate: {
			first:      "{{ trans('client.dt_first_lbl') }}",
			last:       "{{ trans('client.dt_last_lbl') }}",
			next:       "{{ trans('client.dt_next_lbl') }}",
			previous:   "{{ trans('client.dt_prev_lbl') }}"
		},
		loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
		processing:     "{{ trans('client.dt_processing_lbl') }}",
	}
});

$("#btn-add-new-card").click(function(event){
	event.preventDefault();

	$("#div-table-cards").hide();
	$("#div-add-new-card").fadeIn(500);
	
});
</script>