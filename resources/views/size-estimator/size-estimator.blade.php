@extends('app')

@section('content')
<div class="row">
  <div class="storagesMap">
    <svg id="mysvg"></svg>
    <a href="javascript:void(0);" class="btn btn-primary hideMap"><i class="fa fa-angle-up" aria-hidden="true"></i> {{ trans('storages.hide_storagesMap') }}</a>
  </div>
    <!-- Menu secciones -->
    <div class="contentCalc">
      <div class="col-sm-2">
          <div class="panel panel-default">
              <div class="panel-body">
                  <div class="col-md-12">
                    <ul class="nav nav-pills nav-stacked">
                        @foreach ($secciones as $key => $seccion)
                          <li class="{!! $seccion->nombre !!}" data-toggle="pill" onclick="pintarSpanTotal('{!! $seccion->nombre !!}');"><a href="#">{!! $seccion->nombre !!}</a>
                          </li>
                        @endforeach
                        <input type="hidden" id="totalSecciones" name="name" value="{!! $totalSecciones !!}">
                    </ul>
                  </div>
              </div>
          </div>
      </div>
      <!-- Fin menu secciones -->

      <!-- Div articulos -->
      <form class="" action="" method="post">
          @foreach ($secciones as $key => $seccion)
              <div class="tab-pane" id="div_{!! $seccion->nombre !!}" style="display: none">
                  <div class="col-sm-7" id="">
                      <div class="panel panel-default">
                          <div class="panel-heading clearfix">
                              <div class="col-md-9">
                                  <span id="span_titulo_seccion"> {!! $seccion->nombre !!}</span>
                              </div>
                              <div class="col-md-3">
                                  <a href="javascript:void(0);" id="link_limpiar" class="{!! $seccion->id !!}" style="display:none">{{ trans('calculator.empty_room') }}<img src="{{asset('assets/images/size-estimator/limpiar.png')}}" alt="" id="icono_limpiar"/></a>
                              </div>
                          </div>
                          <div class="panel-body">
                              @foreach ($seccion->articulos as $key => $articulo)
                                  <div class="col-md-6">
                                      <div class="col-md-7">
                                          <div class="row">
                                              <img src="{{asset('assets/images/size-estimator/'.$articulo->img_path)}}" />
                                              <span id="spans_nombre_objetos">{!! $articulo->nombre !!}</span>
                                          </div>
                                      </div>
                                      <div class="col-md-5">
                                          <div class="row" id="botones_mas_menos">
                                              <a href="javascript:void(0);" id="res" class="link_min">
                                                  <img src="{{asset('assets/images/size-estimator/ico_min.png')}}" id="ico_min"/>
                                              </a>
                                              <input type="text" class="{!! $articulo->id_seccion !!}" id="input-cantidad" value="0" dir="{!! $articulo->sqm !!}" disabled>
                                              <a href="javascript:void(0);" id="sum" class="link_max">
                                                  <img src="{{asset('assets/images/size-estimator/ico_plus.png')}}" id="ico_min"/>
                                              </a>
                                          </div>
                                      </div>
                                  </div>
                              @endforeach
                          </div>
                      </div>
                  </div>
              </div>
          @endforeach
      </form>

      <!-- Fin div articulos -->

      <!-- Div calculos -->
      <div class="col-sm-3">
          <div class="panel panel-default">
              <div class="panel-body">
                  @foreach ($secciones as $key => $seccion)
                      <div class="span_{!! $seccion->nombre !!}_total" id="row_calculo">
                          <div class="col-md-8">
                              <span>{!! $seccion->nombre !!}</span>
                          </div>
                          <div class="col-md-4">
                              <span class="span_{!! $seccion->id !!}">0.00</span>
                              <span>m2</span>
                          </div>
                      </div>
                  @endforeach

                  <input type="hidden" name="name" value="" id="hiddenTotalSecciones">
                  <div id="row_calculo" class="div_total_calculo">
                      <div class="col-md-8">
                          <span id="" class="spans_calculo">{{ trans('calculator.total_row_calculo') }}</span>
                      </div>
                      <div class="col-md-4">
                          <span name="total"  id="span_total" class="spans_calculo_decimal">0.00</span>
                          <span>m2</span>
                      </div>
                  </div>
                  <br>

                  @if ($calculator)
                    <div class="row wrapperBtnSearchStorageCalc">
                        <div class="col-md-12">
                            <label for="cboWarehouse">Building:</label>
                            <select class="form-control cboWarehouse" name="cboWarehouse">
                              <option selected>{!! trans('calculator.select_option') !!}</option>
                              @foreach ($buildings as $w)
                                <option value="{{ $w->id }}">{{ $w->name }}</option>
                              @endforeach
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label for="cboLevel">Levels:</label>
                            <select class="form-control cboLevel" name="cboLevel" disabled>
                              <option selected>{!! trans('calculator.select_option') !!}</option>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <a data-toggle="modal" id="btn_calcFindStorage" class="btn btn-default btn-lg btn-block">Find storage</a>
                        </div>
                    </div>
                  @else
                    <div class="row wrapperBtnSearchStorage">
                        <div class="col-md-12">
                            <a data-toggle="modal" href="#rent_storage" id="btn_search" class="btn btn-default btn-lg btn-block">{{ trans('calculator.search_storage') }}</a>
                        </div>
                    </div>
                  @endif

              </div>
          </div>
          @if ($calculator)
            <div class="panel panel-default">
              <div class="panel-heading"><h5>{{ trans('calculator.header_iKnowSize') }}</h5></div>
              <div class="panel-body">
                <div class="col-sm-8">
                  <!-- <input type="number" class="" name=""  min="0.1" step="any" value="0.1"> -->
                  <select class="form-control sizeOfStorage" name="sizeOfStorage">
                  </select>
                </div>

                <div class="col-sm-4">
                  <a href="javascript:void(0);" class="btn btn-primary calcBtnNext" name="button">{{ trans('calculator.next_btn') }}</a>

                </div>
              </div>
            </div>
          @endif

      </div>
    </div>
    <!-- Fin div calculos -->
    @if ($calculator)
      <div class="storagesPlans">
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4 dataTables_wrapper">
            <table class="table-striped table-bordered tblCalcStorages">
              <tr>
                <td>
                </td>
                <td>
                  <img src="{{ asset('assets/images/size-estimator/storage-box.png') }}" alt="" />
                </td>
                <td>
                  <img src="{{ asset('assets/images/size-estimator/storage-box.png') }}" alt="" />
                </td>
              </tr>
              <tr>
                <td>
                  {{ trans('calculator.select_storage')}}
                </td>
                <td>
                  <select class="form-control pos1 updateEstimator" name="pos1" id="pos1">

                  </select>
                </td>
                <td>
                  <select class="form-control pos2 updateEstimator" name="pos2" id="pos2">

                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  {{ trans('calculator.map')}}
                </td>
                <td>
                  <a href="javascript:void(0);" id="seeOnMap1" class="seeOnMap btn btn-default btn-xs"> {{ trans('calculator.see_on_map')}} </a>
                </td>
                <td>
                  <a href="javascript:void(0);" id="seeOnMap2" class="seeOnMap btn btn-default btn-xs"> {{ trans('calculator.see_on_map')}} </a>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.sqm') }}</td>
                <td id="sqm1_fixed"></td>
                <td id="sqm2_fixed"></td>
              </tr>
              <tr>
                <td>{{ trans('calculator.price') }}</td>
                <td id="price1_fixed"></td>
                <td id="price2_fixed"></td>
              </tr>
              <tr>
                <td>{{ trans('calculator.term_notice') }}</td>
                <td colspan="2">
                  <select class="form-control cboTermNotice updateEstimator" name="cboTermNotice">
                    <option value="0" selected>{{ trans('calculator.select_option') }}</option>
                    @foreach ($cataterm as $term)
                      <option value="{{$term->id}}">{{$term->months}} -- {{$term->off}}%</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.pre-pay') }}</td>
                <td colspan="2">
                  <select class="form-control cboPrePay updateEstimator" name="cboPrePay">
                    <option value="0" selected>{{ trans('calculator.select_option') }}</option>
                    @foreach ($cataprepay as $pre)
                      <option value="{{$pre->id}}">{{$pre->months}} -- {{$pre->off}}%</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <!--
              <tr>
                <td>{{ trans('calculator.prepay-amount') }}</td>
                <td id="ppa1"></td>
                <td id="ppa2"></td>
              </tr>
              -->
              <tr>
                <td>{{ trans('calculator.credit_card') }}</td>
                <td colspan="2">
                  <select class="form-control cboCc updateEstimator" name="cboCc">
                    <option selected>{{ trans('calculator.select_option') }}</option>
                    <option value="1">{{ trans('calculator.yes') }}</option>
                    <option value="2">{{ trans('calculator.no') }}</option>
                  </select>
                </td>

              </tr>
              <tr>
                <td>{{ trans('calculator.credit_card_percent') }}</td>
                <td colspan="2">
                  <input type="number" name="ccp" id="ccp" class="form-control" value="">
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.rent_start') }}</td>
                <td colspan="2">
                  <div class='input-group date' id='rentStart'>
                    <input type="text" name="resntStart" class="form-control rentStart updateEstimator" value="">
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </td>
              </tr>
              <!-- 
              <tr>
                <td>{{ trans('calculator.extra_items') }}</td>
                <td colspan="2">
                  <a href="javascript:void(0);" dir="1" class="extraItems btn btn-primary"> {{ trans('calculator.extra_items') }} </a>
                </td>
              </tr>
               -->
              <!-- 
              <tr>
                <td>{{ trans('calculator.current_month') }}</td>
                <td id="currentMonth1"></td>
                <td id="currentMonth2"></td>
              </tr>
               -->
               <tr>
                <td>{{ trans('calculator.insurance') }}</td>
                <td colspan="2">
                  <select class="form-control cboInsurance updateEstimator" name="cboCc">
                    <option value="" selected>{{ trans('calculator.select_option') }}</option>
                    <option value="0">{{ trans('calculator.no') }}</option>
                    <option value="16">{{ trans('calculator.insurance_16') }} </option>
                    <!--
                    <option value="32">{{ trans('calculator.insurance_32') }} </option>
                    <option value="48">{{ trans('calculator.insurance_48') }} </option>
                    <option value="64">{{ trans('calculator.insurance_64') }} </option>
                    <option value="80">{{ trans('calculator.insurance_80') }} </option>
                    -->
                  </select>
                </td>
              </tr>
              
              <tr>
              	<td colspan="3" style="text-align:center">
              		<a href="javascript:void(0);" class="btnCalculate btn btn-primary"> {{ trans('calculator.calculate') }} </a>
              	</td>
              </tr>
              <tr>
              	<td colspan="3" style="text-align:center">
              		{{ trans('calculator.current_month_invoice')}}
              	</td>
              </tr>
              <tr>
                <td>{{ trans('calculator.subtotal') }}</td>
                <td id="sub1_fixed"></td>
                <td id="sub2_fixed"></td>
              </tr>
              <tr>
                <td>{{ trans('calculator.vat') }} (+ 23%)</td>
                <td id="vat1_fixed"></td>
                <td id="vat2_fixed"></td>
              </tr>
              <tr class="totalesCalc">
                <td>{{ trans('calculator.total') }}</td>
                <td class="totalCalc" id="total1_fixed"></td>
                <td class="totalCalc" id="total2_fixed"></td>
              </tr>
              <tr class="totalNextMonth">
                <td>{{ trans('calculator.next_month') }}</td>
                <td class="totalCalc" id="nextMonth1_fixed"></td>
                <td class="totalCalc" id="nextMonth2_fixed"></td>
              </tr>
              
              <tr>
                <td>{{ trans('calculator.deposit_month') }}</td>
                <td id="depositMonth1_fixed"></td>
                <td id="depositMonth2_fixed"></td>
              </tr>
              
              <tr>
                <td>{{ trans('calculator.prepayment') }}</td>
                <td id="prepayment1_fixed"></td>
                <td id="prepayment2_fixed"></td>
              </tr>
              
              <tr>
                <td></td>
                <td><a href="javascript:void(0);" dir="1" class="rent1 btn btn-primary"> {{ trans('calculator.add_item') }} </a></td>
                <td><a href="javascript:void(0);" dir="2" class="rent2 btn btn-primary"> {{ trans('calculator.add_item') }} </a></td>
              </tr>
              <tr>
                <td></td>
                <td colspan="2" style="text-align: center;"><button type="button" class="btn btn-primary closeOrderInProgress" name="button">{{ trans('calculator.terminate_order') }}</button></td>
              </tr>
            </table>
          </div>
          <div class="col-md-4">
          </div>
        </div>
      </div>
      
      <!-- VALORES ESCONDIDOS QUE REGGRESA EL HELPER -->
      
      <input type="hidden" id="flag_calculate" value="0" />
      
      <input type="hidden" id="pd_box_price_1" />
      <input type="hidden" id="pd_box_vat_1" />
      <input type="hidden" id="pd_box_total_1" />
      <input type="hidden" id="pd_insurance_price_1" />
      <input type="hidden" id="pd_insurance_vat_1" />
      <input type="hidden" id="pd_insurance_total_1" />
      <input type="hidden" id="pd_subtotal_1" />
      <input type="hidden" id="pd_total_vat_1" />
      <input type="hidden" id="pd_total_1" />
      <input type="hidden" id="pd_total_next_month_1" />
      
      
      
      <input type="hidden" id="pd_box_price_2" />
      <input type="hidden" id="pd_box_vat_2" />
      <input type="hidden" id="pd_box_total_2" />
      <input type="hidden" id="pd_insurance_price_2" />
      <input type="hidden" id="pd_insurance_vat_2" />
      <input type="hidden" id="pd_insurance_total_2" />
      <input type="hidden" id="pd_subtotal_2" />
      <input type="hidden" id="pd_total_vat_2" />
      <input type="hidden" id="pd_total_2" />
      <input type="hidden" id="pd_total_next_month_2" />
      
      
      <!-- PREPAY -->
      <input type="hidden" id="pd_box_price_prepay_1" />
      <input type="hidden" id="pd_box_vat_prepay_1" />
      <input type="hidden" id="pd_box_total_prepay_1" />
      <input type="hidden" id="pd_insurance_price_prepay_1" />
      <input type="hidden" id="pd_insurance_vat_prepay_1" />
      <input type="hidden" id="pd_insurance_total_prepay_1" />
      <input type="hidden" id="pd_subtotal_prepay_1" />
      <input type="hidden" id="pd_total_vat_prepay_1" />
      <input type="hidden" id="pd_total_prepay_1" />
      
      <input type="hidden" id="pd_box_price_prepay_2" />
      <input type="hidden" id="pd_box_vat_prepay_2" />
      <input type="hidden" id="pd_box_total_prepay_2" />
      <input type="hidden" id="pd_insurance_price_prepay_2" />
      <input type="hidden" id="pd_insurance_vat_prepay_2" />
      <input type="hidden" id="pd_insurance_total_prepay_2" />
      <input type="hidden" id="pd_subtotal_prepay_2" />
      <input type="hidden" id="pd_total_vat_prepay_2" />
      <input type="hidden" id="pd_total_prepay_2" />
      
      
	
    @endif

    <!-- JS Size Estimator -->
    <script type="text/javascript" src="{{ asset('assets/js/js-size-estimator.js' )}}"></script>

    <script>

    jQuery(document).ready(function($) {
        var creditCard = "";
        $(document).on('change', '.cboCc', function(e) {
            //console.log(this.options[e.target.selectedIndex].text);
            creditCard = this.options[e.target.selectedIndex].text;
            if (creditCard == '-------- SELECT --------') {
                $("#ccp").val('');
            }
            //alert(creditCard);
        });

        $("#ccp").mouseover(function(){
            //alert(creditCard);
            if (creditCard == 'Yes' || creditCard == 'No') {
            }else {
                $(this).val('');
            }
        });

        $("#ccp").mouseleave(function(){
            if (creditCard == 'Yes' || creditCard == 'No') {
            }else {
                $(this).val('');
            }
        });

    	$(".rent1, .rent2, .closeOrderInProgress").attr({ disabled: 'disabled' });

    	var date = new Date();
    	date.setMonth(date.getMonth(), 1);

    	var today = new Date();
    	var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+2, 0);

    	    
        $('#rentStart').datetimepicker({
          format: 'Y-MM-DD',
          allowInputToggle: true,
          minDate: date,
          maxDate: lastDayOfMonth
        }).on('dp.change',function(e){
            //
            var valor_ccp = $("#ccp").val();  
            
            $(".updateEstimator").trigger("change");

            $("#ccp").val(valor_ccp); 
        });;

        {{ $order = Session::get('order') }}
        @if ($order > 0 )
          $('#order_modal').modal('show');
        @endif

        $(document).on('click', '.newOrderModal', function(event) {
          $.ajax({
            url: '{{ route('closeOrder') }}',
            type: 'POST',
            data: {_token: token}
          })
          .done(function(data) {
            if(data != 0) {
              toastr['success']("{{ trans('calculator.order_close') }}");
              $('#order_modal').modal('hide');
            }else{
              toastr['error']("{{ trans('calculator.error') }}");
            }
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        });

        function sendOrder(flag_pay){
          $('#assingOrder').modal('hide');
          showLoader();

          idUser = $('#assingOrder #id_client').val();

          if (idUser == '') {
            toastr['error']("{{ trans('calculator.user_no_valid') }}");
            setTimeout(function(){
              hideLoader();
            },3000);
            return false;
          }

          cad = '_token='+token+'&user_id='+idUser;

          $.ajax({
            url: '{{ route('closeAssignClose') }}',
            type: 'POST',
            data: cad
          })
          .done(function(data) {
            console.log(data);
            if (data.success) {
              toastr['success']("{{ trans('calculator.close_order_assign') }}");
              $('#btnAssignClose').attr({ disabled: 'disabled' });
              $('#btnAssignPay').attr({ disabled: 'disabled' });

              if(flag_pay){
                setTimeout(function(){
                  window.location.href = "/cart/"+data.order;
                },2000)
              }else{
                setTimeout(function(){
                  location.reload();
                },2000)
              }

            }else{
              console.info('no hay orden!');
              toastr['error']("{{ trans('calculator.no_orders') }}");
              setTimeout(function(){
                hideLoader();
              },3000);
            }
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });
        }


        function checkBox(v){
          term = parseInt($('.cboTermNotice').val());
          prePay = parseInt($('.cboPrePay').val());
          cc = parseInt($('.cboCc').val());
          rentStart = parseInt($('.rentStart').val());

          //alert(term+' ++ '+prePay+' ++ '+cc+' ++ '+rentStart)

          if (term == '' || term <= 0 || isNaN(term) ) {
            $('.cboTermNotice').addClass('has-error');
            toastr["error"]("{{ trans('calculator.choose_term')}}");
            return false;
          }else{
            $('.cboTermNotice').removeClass('has-error');
          }

          if (prePay == '' || prePay <= 0  || isNaN(prePay) ) {
            $('.cboPrePay').addClass('has-error');
            toastr["error"]("{{ trans('calculator.choose_prePay')}}");
            return false;
          }else{
            $('.cboPrePay').removeClass('has-error');
          }

          if (cc == '' || cc <= 0  || isNaN(cc) ) {
            $('.cboCc').addClass('has-error');
            toastr["error"]("{{ trans('calculator.choose_cc')}}");
            return false;
          }else{
            $('.cboCc').removeClass('has-error');
          }

          if (rentStart == '' || cc <= 0  || isNaN(rentStart) ) {
            $('.rentStart').addClass('has-error');
            toastr["error"]("{{ trans('calculator.choose_rentStart')}}");
            return false;
          }else{
            $('.rentStart').removeClass('has-error');
          }

          $('#modalBraintree #price').val(parseFloat($('#total'+v).text()));
          $('#modalBraintree #term_notice').val($('.cboTermNotice').val());
          $('#modalBraintree #prePay').val($('.cboPrePay').val());
          $('#modalBraintree #cc').val($('.cboCc').val());
          $('#modalBraintree #rent_start').val($('.rentStart').val());
          $('#modalBraintree #id_storage').val($('#seeOnMap'+v).attr('dir'));

          $('#modalBraintree .alertClientPayment').hide();
          $('#modalBraintree .braintreeWrapper').hide();
          $('#modalBraintree #sendPaymentCash').hide();
          $('#modalBraintree #sendPay').hide();
          $('#modalBraintree #nextPayment').show();
          $('#modalBraintree #g_client').show();

          return true;
        }

        /*$( ".rent1" ).click(function() {
            if(checkBox(1)){
                $('#modalBraintree').modal('show');
            }

        });*/

        $( ".rent1" ).click(function() {
          if(checkBox(1)){
            $(this).attr('disabled', 'true');
            $(".btnCalculate").attr('disabled', 'true');
            $( ".rent2" ).attr('disabled', 'true');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            price = parseFloat($('#sub1').text());
            itemId = $('#seeOnMap1').attr('dir');
            term = $('.cboTermNotice').val();
            prepay = $('.cboPrePay').val();
            cc = $('.cboCc').val();
            fecha = $('.rentStart').val();
            today = moment(fecha, "DD/MM/YYYY");
            dateBox = today.toDate();
            insurance = $('.cboInsurance').val();
            ext = $('#ccp').val();
            pricePerMonth = parseFloat($('#nextMonth1').text());

            cad = '_token='+token+'&itemId='+itemId+'&itemType=box&itemQuantity=1&price='+price+'&term='+term+'&prepay='+prepay+'&cc='+cc+'&dateBox='+fecha+'&insurance='+insurance+'&pricePerMonth='+pricePerMonth+'&ext='+ext;            

			cad+= '&pd_box_price='+$("#pd_box_price_1").val()+'&pd_box_vat='+$("#pd_box_vat_1").val()+'&pd_box_total='+$("#pd_box_total_1").val();
			cad+= '&pd_insurance_price='+$("#pd_insurance_price_1").val()+'&pd_insurance_vat='+$("#pd_insurance_vat_1").val()+'&pd_insurance_total='+$("#pd_insurance_total_1").val();
			cad+= '&pd_subtotal='+$("#pd_subtotal_1").val()+'&pd_total_vat='+$("#pd_total_vat_1").val()+'&pd_total='+$("#pd_total_1").val(); 
            cad+= '&pd_total_next_month='+$("#pd_total_next_month_1").val();

            cad+= '&pd_box_price_prepay='+$("#pd_box_price_prepay_1").val()+'&pd_box_vat_prepay='+$("#pd_box_vat_prepay_1").val()+'&pd_box_total_prepay='+$("#pd_box_total_prepay_1").val();
			cad+= '&pd_insurance_price_prepay='+$("#pd_insurance_price_prepay_1").val()+'&pd_insurance_vat_prepay='+$("#pd_insurance_vat_prepay_1").val()+'&pd_insurance_total_prepay='+$("#pd_insurance_total_prepay_1").val();
			cad+= '&pd_subtotal_prepay='+$("#pd_subtotal_prepay_1").val()+'&pd_total_vat_prepay='+$("#pd_total_vat_prepay_1").val()+'&pd_total_prepay='+$("#pd_total_prepay_1").val();
                
            $.ajax({
              url: '{{ route('addtocart') }}',
              type: 'POST',
              data: cad
            })
            .done(function(data) {
              if(data != 0) {
                toastr['success']("{{ trans('calculator.item_add_succes') }}");
                $('select.updateEstimator option:not(:selected)').attr('disabled', true);

                //HABILITA BOTON DE CERRAR ORDEN
                $(".closeOrderInProgress").attr("disabled",false);

                $(".btnCalculate").attr("disabled",'disabled');
                
              }else{
                toastr['error']("{{ trans('calculator.error') }}");
                $(this).removeAttr('disabled');
              }
              console.log(data);
            })
            .fail(function() {
              console.log("error");
              $(this).removeAttr('disabled');
            })
            .always(function() {
              console.log("complete");
            });
          }
        });


        $( ".rent2" ).click(function() {
          if(checkBox(2)){
            $(this).attr('disabled', 'true');
            $( ".rent1" ).attr('disabled', 'true');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            price = parseFloat($('#sub2').text());
            itemId = $('#seeOnMap2').attr('dir');
            term = $('.cboTermNotice').val();
            prepay = $('.cboPrePay').val();
            cc = $('.cboCc').val();
            fecha = $('.rentStart').val();
            today = moment(fecha, "DD/MM/YYYY");
            dateBox = today.toDate();
            insurance = $('.cboInsurance').val();
            ext = $('#ccp').val();
            pricePerMonth = parseFloat($('#nextMonth2').text());

            cad = '_token='+token+'&itemId='+itemId+'&itemType=box&itemQuantity=1&price='+price+'&term='+term+'&prepay='+prepay+'&cc='+cc+'&dateBox='+fecha+'&insurance='+insurance+'&pricePerMonth='+pricePerMonth+'&ext='+ext;

			cad+= '&pd_box_price='+$("#pd_box_price_2").val()+'&pd_box_vat='+$("#pd_box_vat_2").val()+'&pd_box_total='+$("#pd_box_total_2").val();
			cad+= '&pd_insurance_price='+$("#pd_insurance_price_2").val()+'&pd_insurance_vat='+$("#pd_insurance_vat_2").val()+'&pd_insurance_total='+$("#pd_insurance_total_2").val();
			cad+= '&pd_subtotal='+$("#pd_subtotal_2").val()+'&pd_total_vat='+$("#pd_total_vat_2").val()+'&pd_total='+$("#pd_total_2").val(); 
			cad+= '&pd_total_next_month='+$("#pd_total_next_month_2").val();

            cad+= '&pd_box_price_prepay='+$("#pd_box_price_prepay_2").val()+'&pd_box_vat_prepay='+$("#pd_box_vat_prepay_2").val()+'&pd_box_total_prepay='+$("#pd_box_total_prepay_2").val();
			cad+= '&pd_insurance_price_prepay='+$("#pd_insurance_price_prepay_2").val()+'&pd_insurance_vat_prepay='+$("#pd_insurance_vat_prepay_2").val()+'&pd_insurance_total_prepay='+$("#pd_insurance_total_prepay_2").val();
			cad+= '&pd_subtotal_prepay='+$("#pd_subtotal_prepay_2").val()+'&pd_total_vat_prepay='+$("#pd_total_vat_prepay_2").val()+'&pd_total_prepay='+$("#pd_total_prepay_2").val();
			
            $.ajax({
              url: '{{ route('addtocart') }}',
              type: 'POST',
              data: cad
            })
            .done(function(data) {
              if(data != 0) {
                toastr['success']("{{ trans('calculator.item_add_succes') }}");
                $('select.updateEstimator option:not(:selected)').attr('disabled', true);

            	//HABILITA BOTON DE CERRAR ORDEN
                $(".closeOrderInProgress").attr("disabled",false);
                $(".btnCalculate").attr("disabled",'disabled');
                
              }else{
                toastr['error']("{{ trans('calculator.error') }}");
                $(this).removeAttr('disabled');
              }
              console.log(data);
            })
            .fail(function() {
              console.log("error");
              $(this).removeAttr('disabled');
            })
            .always(function() {
              console.log("complete");
            });
          }
        });

        $( "#find_client_assign" ).autocomplete({
          source: function( request, response ) {
            $.ajax( {
              url: "{{ route('findClient') }}",
              //dataType: "jsonp",
              data: {
                term: request.term,
                _token: token
              },
              success: function( data ) {
                $('#find_client_list_assign').empty();
                var jsonOptions = JSON.parse(data);
                jsonOptions.forEach(function(item) {
                  $('#find_client_list_assign').append('<option class="client_option_list" id="'+item.id+'" value="'+item.name + ' ' + item.lastName + ' ' + item.companyName  +' - '+item.email+' - '+item.username+'">');
                });
              }
            });
          },
          minLength: 2,
        });

        $(".client_option_list").bind('input', function () {
          alert($(this).attr('id'));
        });

        $(document).on('click', '.alertClientPaymentClose', function() {
          $('#modalBraintree #clientName').empty();
          $('#modalBraintree #id_client').val('');
          $('#modalBraintree #find_client').empty();
          $('#modalBraintree #find_client').val('');
          $('#modalBraintree #find_client_list').empty();
          $('#modalBraintree .alertClientPayment').slideUp(200);
          $('#modalBraintree #g_client').slideDown(200);
        });

        $("#find_client_assign").on('input', function () {
          var val = this.value;
          if($('#find_client_list_assign option').filter(function(){
            if(this.value == val){
        	     id = $(this).attr('id');
               //alert(id);
               $('#assingOrder #id_client').val(id);
               $('#assingOrder #find_client').blur();
               $('#find_client_assign').hide();
               $('.assignName').empty();
               $('.assignName').append(this.value+'<button type="button" class="close delAssignName" aria-hidden="true">&times;</button>');
               $('.assignName').show();
               return false;
             }else{
               $('#modalBraintree #id_client').val('');
             }
          }).length) {
            //send ajax request
            //alert(this.value);
          }
          $('.find_client_assign').removeClass('ui-autocomplete-loading');
        });

        $(document).on('click', '.delAssignName', function() {
          $('#assingOrder #id_client').val('');
          $('.assignName').hide();
          $('#find_client_assign').val('');
          $('#find_client_assign').show();
        });

        $('#nextPayment').click(function() {
          val = $('#id_client').val();
          alert(val);
          if (val == '') {
            toastr['error']("{{ trans('calculator.user_not_valid') }}")
            return false;
          }
        });

        $('.btnNewClient').click(function() {
          $('#modalBraintree').modal('hide');
          $('#assingOrder').modal('hide');
        });

        /*$('.sendNewClientBtn').click(function() {
          $('#modalBraintree').modal('show');
        });*/

        $('#nextPayment').click(function() {
          $(this).hide();
          $('#modalBraintree .braintreeWrapper').slideDown(200);
          $('#modalBraintree #sendPaymentCash').show();
          $('#modalBraintree #sendPay').show();
          $('.alertClientPaymentClose').hide();
        });

        $(document).on('click','.extraItems',function(){
            $('#extra_items2').modal('show');
        });

        //$('#modalBraintree').modal('show');

        $(document).on('click', '#btn_rent_1', function(){
            $('#modalBraintree .braintreeWrapper').slideDown(200);
        });
        $(document).on('click', '#btn_rent_2', function(){
            $('#modalBraintree .braintreeWrapper').slideDown(200);
        });

        $(document).on('click', '.addExtraItem', function() {


            price = parseFloat($('#span_price_extra').text());
            itemId = $(this).attr('dir');
            quantityItem = $(this).parents('tr').find('#cantidad').val();

            //alert(quantityItem);
            //return false;

            if (quantityItem < 1) {
              toastr['error']("{{ trans('calculator.item_more_than') }}");
            }else{
              cad = '_token='+token+'&itemId='+itemId+'&itemType=item&itemQuantity='+quantityItem+'&price='+price;
              
              $.ajax({
                url: '{{ route('addtocart') }}',
                type: 'POST',
                data: cad
              })
              .done(function(data) {
                if(data != 0) {
                  toastr['success']("{{ trans('calculator.item_add_succes') }}");
                }else{
                  toastr['error']("{{ trans('calculator.error') }}");
                }
                console.log(data);
              })
              .fail(function() {
                console.log("error");
              })
              .always(function() {
                console.log("complete");
              });
            }

        });

        $(document).on('click', '.closeOrderInProgress', function(event) {
          $('#assingOrder #id_client').val('');
          $('#assingOrder').modal('show');
          $('.assignName').hide();
        });

        $(document).on('click', '.assignOrderNoClosed', function(event) {
          $('#assingOrder').modal('show');
          $('.assignName').hide();
        });

        $(document).on('click', '#btnAssignClose', function() {

          sendOrder(false);

        });

        $(document).on('click', '#btnAssignPay', function() {

          sendOrder(true);

        });
        
        $(document).on('click', '.btnCalculate', function(event) {
			event.preventDefault();
			
			var idStorage = $("#pos1").val();
			var date_start = $(".rentStart").val();
    		var insurance = $(".cboInsurance").val();
    		var extra_percentage = $("#ccp").val();
    		var prepay = $(".cboPrePay").val();
    		var term = $(".cboTermNotice").val();
    		var idStorage2 = $("#pos2").val();
    		var cc = $('.cboCc').val();  //CC YES:1 ; NO: 2


    		if(date_start == ""){
    			toastr['error']("{{ trans('calculator.choose_rentStart') }}");
    			return false;
        	}

        	if(insurance == ""){
        		toastr['error']("{{ trans('calculator.choose_insurance') }}");
        		return false;
        	}

        	if(prepay == ""){
        		toastr['error']("{{ trans('calculator.choose_prePay') }}");
				return false;
            }

            if(term == ""){
            	toastr['error']("{{ trans('calculator.choose_term') }}");
				return false;
            }

    		showLoader();
			$.post( '{!! route('calculator.calculate') !!}', { 
					'_token'			: '{{ csrf_token() }}',
					'idStorage'			: idStorage, 
					'date_start'		: date_start, 
					'insurance'			: insurance,
					'extra_percentage'	: extra_percentage, 
					'prepay'			: prepay,  
					'term'				: term,
					'cc'				: cc
				}, function( data ) {
					var json = JSON.stringify(data.estimator);
					json = JSON.parse(json);

					$("#sub1_fixed").text(json['currentMonthSubtotal']);
					$("#vat1_fixed").text(json['currentMonthVAT']);
					$("#total1_fixed").text(json['currentMonthTotal']);

					$("#nextMonth1_fixed").text(json['nextMonthTotal']);

					//INPUTS
					$("#pd_box_price_1").val(json['currentMonthBoxPrice']);
					$("#pd_box_vat_1").val(json['currentMonthBoxVAT']);
					$("#pd_box_total_1").val(json['currentMonthBoxTotal']);
					$("#pd_insurance_price_1").val(json['currentMonthInsurancePrice']);
					$("#pd_insurance_vat_1").val(json['currentMonthInsuranceVAT']);
					$("#pd_insurance_total_1").val(json['currentMonthInsuranceTotal']);
					$("#pd_subtotal_1").val(json['currentMonthSubtotal']);
					$("#pd_total_vat_1").val(json['currentMonthVAT']);
					$("#pd_total_1").val(json['currentMonthTotal']);
					$("#pd_total_next_month_1").val(json['nextMonthTotal']);

					$("#depositMonth1_fixed").text(json['depositTotal']);

					$("#pd_box_price_prepay_1").val(json['boxSubtotalPrepay']);
					$("#pd_box_vat_prepay_1").val(json['boxVATPrepay']);
					$("#pd_box_total_prepay_1").val(json['boxTotalPrepay']);
					$("#pd_insurance_price_prepay_1").val(json['insuranceSubtotalPrepay']);
					$("#pd_insurance_vat_prepay_1").val(json['insuranceVATPrepay']);
					$("#pd_insurance_total_prepay_1").val(json['insuranceTotalPrepay']);
					$("#pd_subtotal_prepay_1").val(json['subtotalPrepay']);
					$("#pd_total_vat_prepay_1").val(json['vatPrepay']);
					$("#pd_total_prepay_1").val(json['totalPrepay']);

					$("#prepayment1_fixed").text(json['totalPrepay']);
					
			});

			$.post( '{!! route('calculator.calculate') !!}', { 
				'_token'			: '{{ csrf_token() }}',
				'idStorage'			: idStorage2, 
				'date_start'		: date_start, 
				'insurance'			: insurance,
				'extra_percentage'	: extra_percentage, 
				'prepay'			: prepay,  
				'term'				: term,
				'cc'				: cc
			}, function( data ) {
				var json = JSON.stringify(data.estimator);
				json = JSON.parse(json);
				
				$("#sub2_fixed").text(json['currentMonthSubtotal']);
				$("#vat2_fixed").text(json['currentMonthVAT']);
				$("#total2_fixed").text(json['currentMonthTotal']);

				$("#nextMonth2_fixed").text(json['nextMonthTotal']);


				//INPUTS
				$("#pd_box_price_2").val(json['currentMonthBoxPrice']);
				$("#pd_box_vat_2").val(json['currentMonthBoxVAT']);
				$("#pd_box_total_2").val(json['currentMonthBoxTotal']);
				$("#pd_insurance_price_2").val(json['currentMonthInsurancePrice']);
				$("#pd_insurance_vat_2").val(json['currentMonthInsuranceVAT']);
				$("#pd_insurance_total_2").val(json['currentMonthInsuranceTotal']);
				$("#pd_subtotal_2").val(json['currentMonthSubtotal']);
				$("#pd_total_vat_2").val(json['currentMonthVAT']);
				$("#pd_total_2").val(json['currentMonthTotal']);

				$("#pd_total_next_month_2").val(json['nextMonthTotal']);

				$("#depositMonth2_fixed").text(json['depositTotal']);

				$("#pd_box_price_prepay_2").val(json['boxSubtotalPrepay']);
				$("#pd_box_vat_prepay_2").val(json['boxVATPrepay']);
				$("#pd_box_total_prepay_2").val(json['boxTotalPrepay']);
				$("#pd_insurance_price_prepay_2").val(json['insuranceSubtotalPrepay']);
				$("#pd_insurance_vat_prepay_2").val(json['insuranceVATPrepay']);
				$("#pd_insurance_total_prepay_2").val(json['insuranceTotalPrepay']);
				$("#pd_subtotal_prepay_2").val(json['subtotalPrepay']);
				$("#pd_total_vat_prepay_2").val(json['vatPrepay']);
				$("#pd_total_prepay_2").val(json['totalPrepay']);

				$("#prepayment2_fixed").text(json['totalPrepay']);
			});
			setTimeout(function(){
	              hideLoader();
	            },1000);	 

			$(".rent1, .rent2").attr('disabled',false);


			//CALCULO REALIZADO
			$("#flag_calculate").val(1);
			
          });

                
        
        $(document).on('change', '.updateEstimator', function(event) {
            if($("#flag_calculate").val() == 1){
                
        		$(".rent1, .rent2").attr('disabled','disabled');
	        	$(".btnCalculate").text("{{ trans('calculator.re_calculate') }}");

            }
        });

      });
    </script>
@endsection
