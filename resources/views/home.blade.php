@extends('app')
@section('content')
<style media="screen">
    .chart-legend li span{
        display: inline-block;
        width: 12px;
        height: 12px;
        margin-right: 5px;
    }

    #total-billed-month:hover{
        text-decoration:underline;
        cursor:pointer;
    }

</style>
    @if (Auth::user()->Role_id == 1)
        <div class="container">
            {{--
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ trans('home.welcome_lbl') }}</div>
                        <div class="panel-body">
                            <p>{{ trans('home.msj_lbl') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            --}}

            <!-- REPORTES -->
            <div class="row">

                <!-- <div class="col-md-6">
                    <div class="card">
                        <div class="card-head">
                            <header>
                            {{ trans('home.history') }}
                            </header>
                            <div class="tools">
                                <a href="{{ route('wsGetXlsHistory') }}" target="_blank" class="btn btn-icon-toggle btn-refresh"><i class="md md-file-download"></i></a>
                            </div> -->
                        <!-- </div> end .card-head -->
                        <!-- <div class="card-body no-padding height-6">
                            <div class="stick-bottom-left-right force-padding">
                                <div id="flot-storages" class="flot height-5" data-title="{{ trans('home.rent_history') }}" data-color="#0aa89e"></div>
                            </div> -->
                        <!--</div> end .card-body
                    </div> end .card
                </div> end .col -->

                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">

                                <strong class="text-xl"><?=$actual?>%</strong><br/>
                                <span class="opacity-50">{{ trans('home.occupacy_rate') }}</span>
                                <table style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Warehouse</th>
                                            <th>Level</th>
                                            <th>O. Rate</th>
                                        </tr>
                                    </thead>
                                    @foreach($warehouses as $warehouse)
                                        @foreach($warehouse->Levels as $level)
                                            <tr>
                                                <td>{{ $warehouse->name }}</td>
                                                <td>{{ $level->name }}</td>
                                                <td>{{ number_format($level->Promedio, 2) }}%</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </table>

                                <div class="stick-bottom-left-right">
                                    <div class="progress progress-hairline no-margin">
                                        <div class="progress-bar progress-bar-info" style="width:<?=round($actual)?>%"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->

                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-xl">{{ $boxesRented }}</strong><br/>
                                        <span class="opacity-50">{{ trans('home.boxes_rented') }}</span>
                                    </div>
                                    <div class="col-md-6">
                                        @if($porcentajeBoxesRented >= 0)
                                            <strong class="text-xl" style="color: green;">+ {{ number_format($porcentajeBoxesRented, 1) }}%</strong><br/>
                                        @else
                                            <strong class="text-xl" style="color: red;">{{ number_format($porcentajeBoxesRented, 1) }}%</strong><br/>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-xl">{{ $boxesRentedPastMonth }}</strong><br/>
                                        <span class="opacity-50">{{ trans('home.previous_month') }}</span>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                                <div class="stick-bottom-left-right">
                                    <div class="progress progress-hairline no-margin">
                                        <div class="progress-bar progress-bar-info" style="width:<?=round($porcentajeBoxesRented)?>%"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->

                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-xl" id="total-billed-month">{{ number_format($totalBilledMonth, 1) }} pln</strong><br/>
                                        <span class="opacity-50">{{ trans('home.billed') }}</span>
                                    </div>
                                    <div class="col-md-6">
                                        @if($porcentajeBilled >= 0)
                                            <strong class="text-xl" style="color: green;">+ {{ number_format($porcentajeBilled, 1) }}%</strong><br/>
                                        @else
                                            <strong class="text-xl" style="color: red;"> {{ number_format($porcentajeBilled, 1) }}%</strong><br/>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <strong class="text-xl">{{ number_format($totalBilledPastMonth, 1) }} pln</strong><br/>
                                        <span class="opacity-50">{{ trans('home.previous_month') }}</span>
                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                </div>
                                <div class="stick-bottom-left-right">
                                    <div class="progress progress-hairline no-margin">
                                        <div class="progress-bar progress-bar-info" style="width:<?=round($porcentajeBilled)?>%"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->

                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-xl">{{ number_format($paidInvoices, 1) }}%</strong><br/>
                                        <span class="opacity-50">{{ trans('home.paid_invoices') }}</span>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-lg" style="font-weight: bold;">{{ trans('home.total_billing') }}:</strong><br/>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="text-lg">{{ number_format($totalBilledMonth, 1) }} pln</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong class="text-lg" style="font-weight: bold;">{{ trans('home.total_paid') }}:</strong><br/>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="text-lg">{{ number_format($totalPayedMonth, 1) }} pln</span>
                                    </div>
                                </div>
                                <div class="stick-bottom-left-right">
                                    <div class="progress progress-hairline no-margin">
                                        <div class="progress-bar progress-bar-info" style="width:<?=round($porcentajeBilled)?>%"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->

                <!-- Start Graph 1 -->
                <div class="row" style="margin-top: 20px;">
                    <canvas id="line-chart-boxes" width="800" height="250"></canvas>
                </div>
                <!-- End Graph 1 -->
                
                <!-- Start Graph 1.1 -->
                <div class="row" style="margin-top: 20px;">
                    <canvas id="line-chart-sqm" width="800" height="250"></canvas>
                </div>
                <!-- End Graph 1.1 -->

                <!-- Start Graph 2 -->
                <div class="row" style="margin-top: 20px; margin-bottom: 2%;">
                    <canvas id="line-chart-income-report" width="800" height="250"></canvas>
                </div>
                <!-- End Graph 2 -->

                <!-- Start row 5 -->
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <strong class="text-xl">{{ number_format($sqm_rented_year, 0) }} sqm</strong><br/>
                                            <span class="opacity-50">{{ trans('home.sqm_rented') }}</span>
                                        </div>
                                        <div class="col-md-6">
                                            @if($sqm_gained >= 0)
                                                <strong class="text-xl" style="color: green;">{{ number_format($sqm_gained, 0) }} sqm</strong><br/>
                                            @else
                                                <strong class="text-xl" style="color: red;">{{ number_format($sqm_gained, 0) }} sqm</strong><br/>
                                            @endif
                                            <span class="opacity-50">{{ trans('home.sqm_gained') }}</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 23.5%;">
                                        <div class="col-md-8">
                                            <strong class="text-xl">{{ number_format($sqm_lost, 0) }}</strong><br/>
                                            <span class="opacity-50">{{ trans('home.sqm_lost') }}</span>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->

                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin" style="height: 202px;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <strong class="text-xl">{{ number_format($sqm_space, 0) }} sqm</strong><br/>
                                            <span class="opacity-50">{{ trans('home.of_space') }}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <strong class="text-xl">{{ number_format($sqm_available, 0) }} sqm</strong><br/>
                                            @if($porcentaje_sqm_available >= 0)
                                                <strong class="text-lg" style="color: green;">{{ number_format($porcentaje_sqm_available, 1) }} %</strong><br/>
                                            @else
                                                <strong class="text-lg" style="color: red;">{{ number_format($porcentaje_sqm_available, 1) }} %</strong><br/>
                                            @endif
                                            <span class="opacity-50">{{ trans('home.available') }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="margin-top: 9%;">
                                            <strong class="text-xl">{{ number_format($sqm_rented, 0) }} sqm</strong><br/>
                                            @if($porcentaje_sqm_rented >= 0)
                                                <strong class="text-lg" style="color: green;">{{ number_format($porcentaje_sqm_rented, 1) }} %</strong><br/>
                                            @else
                                                <strong class="text-lg" style="color: red;">{{ number_format($porcentaje_sqm_rented, 1) }} %</strong><br/>
                                            @endif
                                            <span class="opacity-50">{{ trans('home.rented') }}</span>
                                        </div>
                                        <div class="col-md-6">

                                        </div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->

                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin" style="height: 202px;">
                                    <div class="row">
                                        <div class="col-md-6" style="margin-left: 5%;">
                                            <strong class="text-xl">{{ number_format($total_tenants, 0) }}</strong><br/>
                                            <span class="opacity-50">{{ trans('home.tenants') }}</span>
                                        </div>
                                        <div class="col-md-6">

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 8%;">
                                        <div class="col-md-6">
                                            <canvas id="line-chart-total-tenants" width="100" height="100"></canvas>
                                        </div>
                                        <div class="col-md-6" style="margin-left: -7%; margin-top: 6%;">
                                            <div id="js-legend" class="chart-legend"></div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->

                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin" style="height: 202px;">
                                    <div class="row">
                                    <div class="col-md-12" style="margin-left: 5%;">
                                            <strong class="text-xl">
                                                <a href="javascript:void();" id="boxes-expires-this-month" style="color:red; font-size:2em">
                                                    {{ number_format($boxesExpiresThisMonth, 0) }}
                                                </a>
                                            </strong>
                                                <br/>
                                            <span class="opacity-50">BOXES EXPIRES AT END THIS MONTH</span>
                                        </div>
                                        
                                    </div>
                                    <div class="row" style="margin-top:15%;">
                                        <div class="col-md-12" style="margin-left: 5%;">
                                            <strong class="text-xl">
                                                <a href="javascript:void();" id="boxes-expires-next-month">
                                                    {{ number_format($boxesExpiresNextMonth, 0) }}
                                                </a>
                                            </strong><br/>
                                            <span class="opacity-50">BOXES EXPIRES NEXT MONTH</span>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->
                </div>
                <!-- End row 5 -->

                <!-- Start row 6 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body no-padding">
                                    <div class="alert alert-callout alert-info no-margin">
                                        <div class="row">
                                            <table class="table table-hover no-footer dataTable" id="tableSqmHome">
                                                <thead>
                                                    <tr>
                                                        <th>{{ trans('home.sqm_size') }}</th>
                                                        <th>#</th>
                                                        <th># {{ trans('home.rented') }}</th>
                                                        <th># {{ trans('home.available') }}</th>
                                                        <th>{{ trans('home.occupacy_rate') }} %</th>
                                                        <th>$ {{ trans('home.billing') }}</th>
                                                        <th>{{ trans('home.billing') }} %</th>
                                                        <th>{{ trans('home.size') }} %</th>
                                                    </tr>
                                                </thead>
                                                @foreach($sqms as $sqm)
                                                    <tr>
                                                        <td>{{ $sqm->sqm }}</td>
                                                        <td>{{ $sqm->countSqm }}</td>
                                                        <td>{{ $sqm->countSqmRented }}</td>
                                                        <td>{{ $sqm->countSqmAvailable }}</td>
                                                        <td>{{ number_format($sqm->occupancyRateSqm, 0) }} %</td>
                                                        <td>{{ number_format($sqm->totalBillingSqm, 1) }} pln</td>
                                                        <td>{{ number_format($sqm->porcentajeBilling, 0) }} %</td>
                                                        <td>{{ number_format($sqm->porcentajeSize, 0) }} %</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div><!--end .card-body -->
                            </div><!--end .card -->
                        </div><!--end .col -->
                    </div>
                <!-- End row 6 -->

            <div class="row">
            </div>
        </div>

        <script src="{{ asset('assets/js/Chart.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#tableSqmHome').DataTable( {
                    "pagingType": "full_numbers",
                    "order": [ 0, 'asc' ]
                } );
            } );
            new Chart(document.getElementById("line-chart-boxes"), {
                type: 'line',  data: {
                    labels: [
						'{{$month1}}', '{{$month2}}', '{{$month3}}', '{{$month4}}', '{{$month5}}', '{{$month6}}', '{{$month7}}', '{{$month8}}', '{{$month9}}', '{{$month10}}', '{{$month11}}', '{{$month12}}'
                    ],
                    datasets: [
                    {
                        data: [{{$rented_boxes}}],
                        label: '{{ trans('home.current_boxes_rented') }}',
                        borderColor: "gray",
                        fill: false
                    },
                    {
                        data: [{{$new_boxes}}],
                        label: '{{ trans('home.new_boxes') }}',
                        borderColor: "green",
                        fill: false
                    }
                ]},
                options: {
                    title: {
                        display: true,
                        text: '{{ trans('home.boxes') }}'
                    },
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '{{$traMonth}}'
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '#',
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index'
                    }
                }
            });

            new Chart(document.getElementById("line-chart-sqm"), {
                type: 'line',  data: {
                    labels: [
						'{{$month1}}', '{{$month2}}', '{{$month3}}', '{{$month4}}', '{{$month5}}', '{{$month6}}', '{{$month7}}', '{{$month8}}', '{{$month9}}', '{{$month10}}', '{{$month11}}', '{{$month12}}'
                    ],
                    datasets: [
                    {
                        data: [{{$rented_sqm}}],
                        label: 'Sqm Boxes Rented',
                        borderColor: "gray",
                        fill: false
                    },
                    {
                        data: [{{$new_boxes_sqm}}],
                        label: 'Sqm New Boxes',
                        borderColor: "green",
                        fill: false
                    }
                ]},
                options: {
                    title: {
                        display: true,
                        text: 'Sqm'
                    },
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '{{$traMonth}}'
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '#',
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index'
                    }
                }
            });

            new Chart(document.getElementById("line-chart-income-report"), {
                type: 'line',  data: {
                    labels: [
                        '{{$month1}}', '{{$month2}}', '{{$month3}}', '{{$month4}}', '{{$month5}}', '{{$month6}}', '{{$month7}}', '{{$month8}}', '{{$month9}}', '{{$month10}}', '{{$month11}}', '{{$month12}}'
                    ],
                    datasets: [{
                    	data: [{{$netos_box}}],
                        label: 'Box neto',
                        borderColor: "gray",
                        fill: false
                    },
                    {
                        data: [{{$netos_insurance}}],
                        label: 'Insurance neto',
                        borderColor: "green",
                        fill: false
                    }
                ]},
                options: {
                    title: {
                        display: true,
                        text: '{{ trans('home.income_report') }}'
                    },
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '{{$traMonth}}'
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '#',
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index'
                    }
                }
            });

            var pieChart = new Chart(document.getElementById("line-chart-total-tenants"), {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            {{$tenants_grouped[0]->conteo}},
                            {{$tenants_grouped[1]->conteo}},
                            {{$tenants_grouped[2]->conteo}},
                        ],
                        backgroundColor: [
                            "gray",
                            "green",
                            "red",
                        ],
                        borderColor: "black",
                    }],
                    labels: [
                        "{{$tenants_grouped[0]->name}}",
                        "{{$tenants_grouped[1]->name}}",
                        "{{$tenants_grouped[2]->name}}",
                    ]
                },
                options: {
                    tooltips: {
                        bodyFontSize: 8,
                    },
                    responsive: false,
                    legend: {
                        display: false,
                		position: 'top',
                		reverse: false,
                        fullWidth: false,
                    }
                },
            });
            document.getElementById('js-legend').innerHTML = pieChart.generateLegend();
        </script>

        <script>

            (function (namespace, $) {
                "use strict";

                var pwDashboard = function () {
                    // Create reference to this instance
                    var o = this;
                    // Initialize app when document is ready
                    $(document).ready(function () {
                        o.initialize();
                    });

                };
                var p = pwDashboard.prototype;

                // =========================================================================
                // MEMBERS
                // =========================================================================

                p.rickshawSeries = [[], []];
                p.rickshawGraph = null;
                p.rickshawRandomData = null;
                p.rickshawTimer = null;

                // =========================================================================
                // INIT
                // =========================================================================

                p.initialize = function () {
                    //this._initSparklines();
                    //this._initFlotVisitors();
                    //this._initRickshaw();
                    //this._initKnob();
                    this._initFlotRegistration();
                };

                // =========================================================================
                // FLOT
                // =========================================================================

                p._initFlotRegistration = function () {
                    var o = this;
                    var chart = $("#flot-storages");

                    // Elements check
                    if (!$.isFunction($.fn.plot) || chart.length === 0) {
                        return;
                    }

                    // Chart data
                    var data = [
                        {
                            label: 'Storages',
                            data: [
                            [moment().subtract(0, 'month').valueOf(), <?=$currentRent;?>],
                                <?php
                                    foreach ($historyGraph as $h) {
                                        echo '[moment("'.$h->date.'"),'.$h->storages_rent.'],';
                                    }
                                ?>

                            ],
                            last: true
                        }
                    ];

                    // Chart options
                    var labelColor = chart.css('color');
                    var options = {
                        colors: chart.data('color').split(','),
                        series: {
                            shadowSize: 0,
                            lines: {
                                show: true,
                                lineWidth: 2
                            },
                            points: {
                                show: true,
                                radius: 3,
                                lineWidth: 2
                            }
                        },
                        legend: {
                            show: false
                        },
                        xaxis: {
                            show:false,
                            mode: "time",
                            timeformat: "%b",
                            color: 'rgba(0, 0, 0, 0)',
                            font: {color: labelColor},
                            ticks: 6,
                            alignTicksWithAxis: 2
                        },
                        yaxis: {
                            font: {color: labelColor}
                        },
                        grid: {
                            borderWidth: 0,
                            color: labelColor,
                            hoverable: true
                        }
                    };
                    chart.width('100%');

                    // Create chart
                    var plot = $.plot(chart, data, options);

                    // Hover function
                    var tip, previousPoint = null;
                    chart.bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint !== item.dataIndex) {
                                previousPoint = item.dataIndex;

                                var x = item.datapoint[0];
                                var y = item.datapoint[1];
                                var tipLabel = '<strong>' + $(this).data('title') + '</strong>';
                                var tipContent = y + " " + item.series.label.toLowerCase() + " on " + moment(x).format('MMMM-YYYY');

                                if (tip !== undefined) {
                                    $(tip).popover('destroy');
                                }
                                tip = $('<div></div>').appendTo('body').css({left: item.pageX, top: item.pageY - 5, position: 'absolute'});
                                tip.popover({html: true, title: tipLabel, content: tipContent, placement: 'top'}).popover('show');
                            }
                        }
                        else {
                            if (tip !== undefined) {
                                $(tip).popover('destroy');
                            }
                            previousPoint = null;
                        }
                    });
                };

                // =========================================================================
                namespace.pwDashboard = new pwDashboard;
            }(this.materialadmin, jQuery)); // pass in (namespace, jQuery):

        </script>
    @elseif (Auth::user()->Role_id == 3)
        <div class="containter">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div class="col-md-6">
                            <a href="javascript:void();">
                                <img src="{{asset('assets/img/storage-icon.png')}}" alt="" />
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('sizeCalculator')}}">
                                <img src="{{asset('assets/img/calculator-icon.png')}}" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

<script>
$("#total-billed-month").click(function(e){
    $("#modal-generico").modal("show");
    $("#modal-generico-content").load('{{route("billing_current_month.index")}}');
});

$("#boxes-expires-this-month").click(function(e){
    $("#modal-generico").modal("show");
    $("#modal-generico-content").load('{{route("storages_expires.index",[1])}}');
});

$("#boxes-expires-next-month").click(function(e){
    $("#modal-generico").modal("show");
    $("#modal-generico-content").load('{{route("storages_expires.index",[0])}}');
});
</script>

@endsection



