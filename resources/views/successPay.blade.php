@extends('app') @section('content')
<style>
@if($response['returnCodeCurrent'] == 200 || $response['returnCodeCurrent'] == "")
.panel-success > .panel-heading {
  background-color:#3C763D;
  border-color:#3C763D;
  color:#fff;
}
@else
.panel-success > .panel-heading {
  background-color:#cc0000;
  border-color:#cc0000;
  color:#fff;
}	
@endif

@if($response['returnCodeCurrent'] == 200)
	.tabla-resumen tr{
		border-bottom:1px dashed #3C763D;
	}	
@else
	.tabla-resumen tr{
		border-bottom:1px dashed #cc0000;
	}
@endif
.tabla-resumen th {
	text-align:center;
	font-size: 14px
}
.tabla-resumen td {
	padding: 5px;
	font-size: 13px
}
.tabla-resumen td:first-child{
	text-align:right;
	font-weight: normal;
}
.tabla-resumen td:last-child{
	text-align:left;
	font-weight: bold;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">

			<div class="panel panel-success" style="text-align: center;">
				<div class="panel-heading">
					<h1>
						@if($response['returnCodeCurrent'] == 200)
							<i class="fa fa-check-circle-o" aria-hidden="true"></i> 
						@elseif($response['returnCodeCurrent'] == "")
							<i class="fa fa-check-circle-o" aria-hidden="true"></i> 
						@else
							<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
						@endif
							{{$response['msgCurrent']}}
						
					</h1>
				</div>
				<div class="panel-body" style="@if($response['flagPaid'] == 0 ) display:none @endif")>
					<table style="margin: 0 auto; width:100%; font-size:13px" class="">
						<tr>
							<td>Payment Method: <b>{{$response['pt']}}</b></td>
							<td>Date: <b>{{$response['date']}}</b></td>
						</tr>
					</table>					
					 
					<table style="margin: 0 auto; margin-top:10px;" class="tabla-resumen">
						@if($response['amountCurrent'] != "")
							<tr>
								<th colspan="2">CURRENT MONTH</th>
							</tr>
							<tr>
								<td>Amount:</td>
								<td>{{$response['amountCurrent']}}</td>
							</tr>
						@endif

						@if($response['transactionIDCurrent'] != "")
						<tr>
							<td>Transaction ID:</td>
							<td>{{$response['transactionIDCurrent']}}</td>
						</tr>
						@endif
						
						@if($response['statusCurrent'] != "")
						<tr>
							<td>Status</td>
							<td>{{$response['statusCurrent']}}</td>
						</tr>
						@endif
						
						@if($response['cardTypeCurrent'] != "")
						<tr>
							<td>Card Type</td>
							<td>{{$response['cardTypeCurrent']}}</td>
						</tr>						
						@endif
						
						@if($response['errorCurrent'] != "")
							<tr>
								<td>Error:</td>
								<td>{{$response['errorCurrent']}}</td>
							</tr>
						@endif
						
					</table>
					<hr/>
					@if($response['prepay'] == 1)
					<table style="margin: 0 auto; margin-top:10px;" class="tabla-resumen">
						<tr>
							<th colspan="2">PREPAY</th>
						</tr>
						<tr>
							<td>Amount:</td>
							<td>{{$response['amountPrepay']}}</td>
						</tr>

						@if($response['transactionIDPrepay'] != "")
						<tr>
							<td>Transaction ID:</td>
							<td>{{$response['transactionIDPrepay']}}</td>
						</tr>
						@endif
						
						@if($response['statusPrepay'] != "")
						<tr>
							<td>Status</td>
							<td>{{$response['statusPrepay']}}</td>
						</tr>
						@endif
						
						@if($response['cardTypePrepay'] != "")
						<tr>
							<td>Card Type</td>
							<td>{{$response['cardTypePrepay']}}</td>
						</tr>						
						@endif
						
						@if($response['errorPrepay'] != "")
							<tr>
								<td>Error:</td>
								<td>{{$response['errorPrepay']}}</td>
							</tr>
						@endif
					</table>
					@endif
					<!-- 
                            	<a href="javascript:void(0);" class="downInvoice">{{ trans('cart.download_invoice') }}</a>
                             -->

					<!-- 
                            <h3>
                           		<a href="javascript:void(0);" class="printInvoice">{{ trans('cart.print_invoice') }}</a>
                            </h3>
                             -->

				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">


        </script>

@endsection
