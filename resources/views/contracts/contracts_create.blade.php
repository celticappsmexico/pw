<form action="{{route('contracts.store')}}" method="post" id="frmContract" enctype="multipart/form-data">
{{ csrf_field() }}
	<div class="modal-header">
		<h4 class="modal-title" id="">New Contract (APP)</h4>
	</div>
	<div class="modal-body">
	    <div class="row" >
			<div class="col-md-12">
				<br>
				<a href="{{route('contract.current_contract')}}" type="button" class="btn btn-success pull-right">
					Download Current Contract
				</a>
			</div>
			<div class="col-md-12">
	      		<div class="form-group">
	      			<label>Select File (PDF)*</label>
	      			<input type="file" name="contract_file" accept=".pdf" class="form-control">
	      		</div>

			</div>
			
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script>
    $('#frmContract').validate({
	      rules: {		
	          pdf: {    
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {

		      	//console.log($("#frmPayBilling").attr('action'));

	        	$("#frmContract").find('button').prop('disabled',false);
	        	
				
				var form = document.getElementById("frmContract"); 
				var formdata = new FormData(form); // high importance!


	             $.ajax({
	               url: $("#frmContract").attr('action'),
	               type: 'POST',
	               data: formdata,
				   async: true,
					dataType: "JSON", 
					contentType: false, // high importance!
					processData: false, // high importance!
	             })
	             .done(function(data) {
	               //console.info(data);
	               //alert(JSON.stringify(data));
	               var message = "";
	               if (data.returnCode == 200) {
		               
		                toastr["success"](data.msg);
		                $('#modal-generico').modal('hide');

	               }else{
	                 	toastr["error"](data.msg);
	                 	message = data.msg;
	               }
	
	             })
	             .fail(function() {
	               //console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               //console.log("complete");
	             });
	      return false; // required to block normal submit since you used ajax
	     }
	  });
</script>
