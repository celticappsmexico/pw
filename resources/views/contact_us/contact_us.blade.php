<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Przechowamy-Wszystko</title>
    	<link rel="shortcut icon" href="http://www.przechowamy-wszystko.pl/wp-content/themes/przechowamy-wszystko/images/fav.ico" type="image/x-icon" />

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/bootstrap.css?1422792965') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/materialadmin.css?1425466319') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/font-awesome.min.css?1422529194') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/material-design-iconic-font.min.css?1421434286') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/libs/morris/morris.core.css?1420463396') }}" />
	  	<link rel="stylesheet" href="{{ asset('assets/css/jquery.qtip.css') }}" media="screen" title="no title" charset="utf-8">
	  	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/toastr.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/reports.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/transformicons.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/jquery.tree-multiselect.min.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
		
		<!-- BOOTSTRAP TOGGLE-->
		<!--  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" /> -->
		
		 
		<!-- END BOOTSTRAP -->
		
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="{{ asset('assets/js/libs/utils/html5shiv.js?1403934957') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/libs/utils/respond.min.js?1403934956') }}"></script>
		<![endif]-->

        <!-- CSS Size Estimator -->
        <link rel="stylesheet" href="{{asset('assets/css/css-size-estimator.css')}}" media="screen" title="no title">
    <style media="screen">
      /*
      estilos que hay que mover a otra hoja de estilos
      */
      body{
        font-family: Arial;
        font-size: 10px
      }

      .text12{
        font-size: 12px;
      }
      .text10{
        font-size: 10px;
      }
      .text9{
        font-size: 9px;
        letter-spacing: -1px;
      }
      .text8{
        font-size: 8px;
        letter-spacing: -1px;
      }

      .navbar-brand img{
        height: 30px!important;
        top: -10px!important;
        margin-top: -5px;
      }

      .report-title{
      	margin-top:0;
      }
      
      	@if(env('ENVIRONMENT') == 'DEV')
      		#header{
	      		background:#008080 !important;
	      	}
	      	
	      	#menubar .menubar-scroll-panel{
	      		background:#B22222
	      	}
      	
		@endif   


		.form-control:focus{
			background-color:rgba(255,255,255,0.5);
		}   
    </style>

    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.qtip.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/snap.svg-min.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/toastr.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/datetimepicker.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

		<script type="text/javascript">

		</script>

    	<script>
			@include('partials.js.pw')
		</script>
		<script>
			@include('partials.js.validator_messages')
		</script>
		<script>
			@include('partials.js.sizeEstimator')
		</script>

	</head>
	<body class="menubar-hoverable header-fixed ">

		<audio id="soundNotif"><source src="{{asset('assets/sound/notif.mp3')}}" type="audio/mpeg"></audio>

		<div class="pageLoader">
			<div class="wraploader">
				<img src="{{ asset('assets/images/loader.gif') }}" alt="" />
			</div>
		</div>
	  	<div class="superWrap">
	    @include('partials.layout.navbar_pw')
	  	@include('partials.layout.errors')
			<div class="sticky-container">
			  <ul class="sticky">
			    <li><a href="{{ url('lang', ['en']) }}"><img src="{{ asset('assets/images/uk.png') }}" /> <p>En</p></a></li>
			    <li><a href="{{ url('lang', ['pl']) }}"><img src="{{ asset('assets/images/pl.png') }}" /> <p>Pl</p></a></li>
			  </ul>
			</div>

			<!-- BEGIN BASE-->
			<div id="base">

				<!-- BEGIN OFFCANVAS LEFT -->
				<div class="offcanvas">
				</div><!--end .offcanvas-->
				<!-- END OFFCANVAS LEFT -->

				<!-- BEGIN CONTENT-->
				<div id="content">
					<section>
						<div class="section-body">
							<form action="{{route('contact.store')}}" method="POST" id="frm-contact">
								{{csrf_field()}}
								<div class="row">
									<div class="col-md-8">
										<h3>Contact-Us</h3>
									</div>

									<div class="col-md-8">
							      		<div class="form-group">
							      			<label>Name</label>
							      			<input type="text" name="name" class="form-control" autocomplete="off" autofocus>
							      		</div>
									</div>

									<div class="col-md-8">
							      		<div class="form-group">
							      			<label>Email</label>
							      			<input type="email" name="email" class="form-control" autocomplete="off">
							      		</div>
									</div>

									<div class="col-md-8">
							      		<div class="form-group">
							      			<label>Message</label>
							      			<textarea rows="5" class="form-control" style="resize:none" name="message"></textarea>
							      		</div>
									</div>

									<div class="col-md-8">
							      		<div class="form-group">
							      			<button class="btn btn-primary" type="submit">
												SEND
							      			</button>
							      		</div>
							      	</div>

								</div>
							</form>
						</div>
					</section>
				</div><!--end #content-->
				<!-- END CONTENT -->
			</div><!--end #base-->
		</div>
		<!-- END BASE -->

		@include('partials.modals.payment_modal')
		@include('partials.modals.rent_storage')
		@include('partials.modals.extra_items_modal')
		@include('partials.modals.new_client')
		@include('partials.modals.delete_client')
    	@include('partials.modals.edit_client')
		@include('partials.modals.delete_building')
	    @include('partials.modals.new_building')
	    @include('partials.modals.edit_building')
	    @include('partials.modals.new_level')
	    @include('partials.modals.delete_level')
	    @include('partials.modals.edit_level')
	    @include('partials.modals.new_storage')
	    @include('partials.modals.delete_storage')
		@include('partials.modals.edit_storage')
		@include('partials.modals.new_order')
	    @include('partials.modals.extra_items_bk_modal')
		@include('partials.modals.assign_order_modal')
		@include('partials.modals.order_detail')
		@include('partials.modals.get_client_boxes')
		@include('partials.modals.add_storage_modal')
		@include('partials.modals.storage_invoices')

		<div class="modal fade" id="modal-generico" tabindex="-1" role="dialog"
				data-keyboard="false" data-backdrop="static"
					aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content" id="modal-generico-content">
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-generico-small" tabindex="-1" role="dialog"
				data-keyboard="false" data-backdrop="static"
					aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" id="modal-generico-small-content">
				</div>
			</div>
		</div>


		<!-- BEGIN JAVASCRIPT -->

		<script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/spin.js/spin.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/moment/moment.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.time.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.resize.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.orderBars.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.pie.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/curvedLines.js') }}"></script>
		<script src="{{ asset('assets/js/libs/jquery-knob/jquery.knob.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/sparkline/jquery.sparkline.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/d3/d3.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/d3/d3.v3.js') }}"></script>
		<script src="{{ asset('assets/js/libs/rickshaw/rickshaw.min.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/App.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppNavigation.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppOffcanvas.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppCard.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppForm.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppNavSearch.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppVendor.js') }}"></script>
		<script src="{{ asset('assets/js/core/dashboard.js') }}"></script>
		<script src="{{ asset('assets/js/Chart.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		
		<!-- bootstrap toggle -->
		<!-- 
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> 
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		 --> 
		<!--  -->
		
		<script>


			$(".headerbar-right, .pageLoader").hide();

			$("input").attr("autocomplete","off");

		$('#frm-contact').validate({
	  	 	ignore: ':hidden',
	      	rules: {
	          name: {
	              minlength: 3,
	              required: true
	          },
	          email: {
	          		email: true,
	              	required: true,
		      },
	          message: {
	          	minlength: 10,
	            required: true
	          }
          },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {

	      	
	      
	             $.ajax({
	               url: url,
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
		            toastr["success"](data.msg);	              
	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });
		</script>

		<!-- END JAVASCRIPT -->
					
	</body>
</html>
