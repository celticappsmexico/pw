<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <style media="screen">
        	 
        	
            *{
              margin: 0;
              padding:0;
              font-size: 10px;
              font-family: DejaVu Sans;
            }

            .tblWrap{
              margin: 25px;
              /*width: 800px;*/
            }

            .firm{
              border-collapse: seperate;
              border-spacing: 50px!important;
            }

            .faktura-head{
              border-collapse: seperate;
              border-spacing: 3px!important;
            }
            .faktura-head td{
              text-align: center!important;
              margin: 0px;
              padding: 5px;
            }

            .dataWrap{
              border: none;
            }

            .dataWrap td{
              border: solid 1px #000;
            }

            .dataWrap td table td{
              border: none;
            }

            .dataWrap td{
              width: 50%;
            }

            .dataWrap td table{
              width: 100%;
            }

            .dataWrap td table .data{
              width: 15%;
            }


            .dataWrap td table .data2{
              font-weight: bold;
            }

            .items{
              border-collapse: collapse;
              border-spacing: 0px!important;
            }

            #row-1 {
                border: 1px solid black;
                text-align: center;
                /*padding-top: 80px;*/
                font-size: 11px;
            }
            .col-md-3 {
                margin-left: 3px;
                width: 313.3px;
            }
            #row-2 {
                border: 1px solid black;
                margin: 2px;
                width: 42.56%;
            }
            .faktura {
                border: 1px solid black;
                margin-top: 2px;
                background-color: #ccc;
                text-align: center;
            }
            .row.warszawa {
                border: 1px solid black;
                margin-top: 2px;
                text-align: center;
                height: 36px;
            }
            #span-2-warszawa {
                font-size: 10px;
            }
            .warszawa-fecha {
                margin-bottom: -5px;
            }
            .row.num-fact, .row.sprzedazy {
                border: 1px solid black;
                margin-top: 2px;
                height: 30.5px;
                text-align: center;
            }
            .kopia {
                border: 0px #FFF;
                text-align: center;
            }
            #div-row-3 {
                border: 1px solid black;
                margin-left: 17px;
                text-align: left;
                width: 69.4%;
                margin-top: 2px;
            }
            .col-md-3.forma, .col-md-3.termin {
                width: 220px;
                margin-left: -25px !important;
            }
            .col-md-3.bank {
                width: 140px;
            }
            .col-md-3.konto {
                width: 360px;
            }
            table {
              width: 100%;
                margin-top: 4px;
                margin-left: 2px;
            }
            th {
                border: 1px solid black;
                text-align: center;
                background-color: #ccc;
                padding:5px;
            }
            td {
                border: 1px solid black;
                text-align: left !important;
				padding:5px !important;
            }
        </style>
    </head>
    <body>
      <table class="tblWrap">
        <tr>
          <td style="border: none">
            <table class="faktura-head">
              <tr>
                <td rowspan="3" id="row-1">
                	<img src="http://pw.novacloud.link/assets/images/logo.png" style="width:160px"/>
                </td>
                <td rowspan="2" class="faktura">
                  <b>
                  	Contract
                 </b>
                </td>
                <td class="num-fact">
                  <b></b>
                </td>
              </tr>
              <tr>
                <td class="kopia">
                	<b>ORYGINAŁ/KOPIA</b>
                </td>
              </tr>
              <tr>
                <td class="warszawa-fecha">
                  <span id="span-1-warszawa"></span><br>
                  <span id="span-2-warszawa">data i miejsce wystawienia dokumentu</span>
                </td>
                <td>
                  <span id="span-1-warszawa"></span><br>
                  <span id="span-2-warszawa">Okres sprzedaży</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="dataWrap">
              <tr>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Sprzedawca:
                      </td>
                      <td class="data2">
                        
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        
                      </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        5272703362
	                      </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Nabywca:
                      </td>
                      <td class="data2">
                       
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        
                      </td>
                    </tr>
                    <tr>
	                    <td class="data">
	                        NIP:
	                    </td>
	                    <td class="data2">
	                        
	                    </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table>
              <tr>
                <td>
                  Forma płatności: <span style="font-weight:bold"></span>
                  Termin płatności: <span style="font-weight:bold"></span>
                  Bank: <span style="font-weight:bold"></span>
                  Konto: <span style="font-weight:bold"></span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa</th>
                    <th>Kod</th>
                    <th style="padding:4px">llość</th>
                    <th style="padding:6px">Jm</th>
                     
                    <th class="no-paragon">Cena netto</th>
                    
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    
                    <th class="no-paragon">Kwota VAT</th>
                   	
                    <th>Wartość brutto</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>
                    
                    </td>
                    <td>
                        box
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        szt
                    </td>
                    
                    <td class="no-paragon" style="text-align:right !important">
                        
                    </td>
                    
                    <td style="text-align:right !important">
                        
                    </td>
                    <td style="text-align:right !important">
                        
                    </td>
                    
                    <td  class="no-paragon" style="text-align:right !important">
                        
                    </td>
                    
                    <td style="text-align:right !important">
                        
                    </td>
                </tr>
	                
	                <tr>
	                    <td style="border: 0px"></td>
	                    <td class="no-paragon" style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    
		                <td style="border: 0px"></td>
		                <td style="border: 0px"></td>
	                    
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">
                        
                        </td>
	                    
                        <td style="text-align:right !important">
                            
                        </td>
	                    
                        <td style="text-align:right !important">X</td>
		                
	                    <td style="text-align:right !important">
                            
                        </td>
	                </tr>
	              
            </table>
          </td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="firm">
              <tr>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do odebrania dokumentu
                </td>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do wystawienia dokumentu
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </body>
</html>
