<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <style media="screen">
        	 
        	
            *{
              margin: 0;
              padding:0;
              font-size: 9.5px;
              font-family: DejaVu Sans;
            }

            .tblWrap{
              margin: 25px;
              /*width: 800px;*/
            }

            .firm{
              border-collapse: seperate;
              border-spacing: 50px!important;
            }

            .faktura-head{
              border-collapse: seperate;
              border-spacing: 3px!important;
            }
            .faktura-head td{
              text-align: center!important;
              margin: 0px;
              padding: 5px;
            }

            .dataWrap{
              border: none;
            }

            .dataWrap td{
              border: solid 1px #000;
            }

            .dataWrap td table td{
              border: none;
            }

            .dataWrap td{
              width: 50%;
            }

            .dataWrap td table{
              width: 100%;
            }

            .dataWrap td table .data{
              width: 15%;
            }


            .dataWrap td table .data2{
              font-weight: bold;
            }

            .items{
              border-collapse: collapse;
              border-spacing: 0px!important;
            }

            #row-1 {
                border: 1px solid black;
                text-align: center;
                /*padding-top: 80px;*/
                font-size: 11px;
            }
            .col-md-3 {
                margin-left: 3px;
                width: 313.3px;
            }
            #row-2 {
                border: 1px solid black;
                margin: 2px;
                width: 42.56%;
            }
            .faktura {
                border: 1px solid black;
                margin-top: 2px;
                background-color: #ccc;
                text-align: center;
            }
            .row.warszawa {
                border: 1px solid black;
                margin-top: 2px;
                text-align: center;
                height: 36px;
            }
            #span-2-warszawa {
                font-size: 10px;
            }
            .warszawa-fecha {
                margin-bottom: -5px;
            }
            .row.num-fact, .row.sprzedazy {
                border: 1px solid black;
                margin-top: 2px;
                height: 30.5px;
                text-align: center;
            }
            .kopia {
                border: 0px #FFF;
                text-align: center;
            }
            #div-row-3 {
                border: 1px solid black;
                margin-left: 17px;
                text-align: left;
                width: 69.4%;
                margin-top: 2px;
            }
            .col-md-3.forma, .col-md-3.termin {
                width: 220px;
                margin-left: -25px !important;
            }
            .col-md-3.bank {
                width: 140px;
            }
            .col-md-3.konto {
                width: 360px;
            }
            table {
              width: 100%;
                margin-top: 4px;
                margin-left: 2px;
            }
            th {
                border: 1px solid black;
                text-align: center;
                background-color: #ccc;
                padding:3px;
            }
            td {
                border: 1px solid black;
                text-align: left !important;
				padding:2px !important;
            }
        </style>
    </head>
    <body>
      <table class="tblWrap">
        <tr>
          <td style="border: none">
            <table class="faktura-head">
              <tr>
                <td rowspan="2" id="row-1">
                	<img src="http://pw.novacloud.link/assets/images/logo.png" style="width:160px"/>
                </td>
                <td rowspan="2" class="faktura">
                  <b>Faktura korygująca</b>
                </td>
                <td class="num-fact">
                  <b>{{$billing->bi_year_invoice_corrected}}-KPW-{{ $billing->bi_number_invoice_corrected }}</b>
                </td>
              </tr>
              <tr>
                <td class="kopia">
                @if(isset($data['copy']))
                	<b>KOPIA</b>
                @else
                	<b>ORYGINAŁ</b>
                @endif
                  
                </td>
              </tr>
              <tr>
                <td colspan="2" class="warszawa-fecha">
                	<table style="border:none !important">
						<tr style="border:none !important">
							<td colspan="5" style="border:none !important">
								{{ $originalBilling->bi_year_invoice }}-PW-{{ $originalBilling->bi_number_invoice }} , {{ $originalBilling->pay_month }}</span><br><span id="span-2-warszawa">numer, data dokumentu korygowanego</span>
							</td>
							<td colspan="5" style="border:none !important">
								<span id="span-1-warszawa">{{ App\Library\MonthToWords::convertMonthToWords($billing->pay_month) }} {{ Carbon\Carbon::parse($billing->pay_month)->year }}</span><br>
				                  		<span id="span-2-warszawa">okres sprzedaży</span>
							</td>
						</tr>
					</table>
                  	
                </td>
                <td>
                  <span id="span-1-warszawa">{{ $billing->pay_month }} {{ env('DATA_PLACE') }}</span><br>
                  <span id="span-2-warszawa">data i miejsce wystawienia dokumentu</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="dataWrap">
              <tr>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Sprzedawca:
                      </td>
                      <td class="data2">
                        {{ env('DATA_SELLER') }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ env('DATA_ADDRESS') }}
                      </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        5272703362
	                      </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Nabywca:
                      </td>
                      <td class="data2">
                        {{ $clientName }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ $fullAddress }}
                      </td>
                    </tr>
                    <tr>
                    	@if($user->userType_id == 1)
                    		<td class="data">
		                        Pesel:
		                    </td>
		                    <td class="data2">
		                        {{ $user->peselNumber }}
		                    </td>
		                  @else
	                      <td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        {{ $user->nipNumber }}
	                      </td>
	                      @endif
                    </tr>
                  </table>
                </td>
              </tr>
              
            </table>
          </td>
        </tr>
        
         <tr>
          <td style="border: none">
            <table>
              <tr>
                <td>
                  Forma płatności: <span style="font-weight:bold"></span>
                  Termin płatności: <span style="font-weight:bold">{{ Carbon\Carbon::now()->addDays(7)->format('Y-m-d') }}</span>
                  Bank: <span style="font-weight:bold">{{ env('DATA_BANK') }}</span>
                  Konto: <span style="font-weight:bold">{{ env('DATA_ACCOUNT') }}</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align: center !important">POZYCJE KORYGOWANE</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>                    
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość </th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                
                
                <tr>
                    <td>
                        1
                    </td>
                    <td>Wynajem powierzchni magazynowej {{$originalBilling->storages->sqm}} m2 </td>
                    <td>
                        m-c
                    </td>
                    <td>
                        {{$originalBillingDetail->bd_numero}}
                    </td>
                    <td style="text-align:right !important">
                      {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_valor_neto)}}
                    </td>
                      <td style="text-align:right !important">
                      {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_valor_neto)}}
                    </td>
                      <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('PERCENT', $originalBillingDetail->bd_porcentaje_vat)}}
                      </td>
                    <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_total_vat)}}
                      </td>
                    <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_total)}}
                          <?php 
                            $subtotal = $originalBillingDetail->bd_valor_neto;
                            $impuestos = $originalBillingDetail->bd_total_vat;
                            $total = $originalBillingDetail->bd_total;
                          ?>
                      </td>
                  </tr>
                  <tr>
                      <td style="border: 0px"></td>
                      <td style="border: 0px"></td>
                      <td style="border: 0px"></td>
                      <td style="border: 0px"></td>
                      <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
                      <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_valor_neto)}}
                      </td>
                      <td style="text-align:right !important">X</td>
                      <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_total_vat)}}
                      </td>
                      <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $originalBillingDetail->bd_total)}}
                      </td>
                  </tr>
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align: center !important">KOREKTY CENY: </td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość</th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>Wynajem powierzchni magazynowej {{$originalBilling->storages->sqm}} m2 </td>
                    <td>
                        m-c
                    </td>
                    <td>
                        {{$billingDetail->bd_numero}}
                    </td>
                    <td style="text-align:right !important">
                    {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_valor_neto)}}
                  </td>
                    <td style="text-align:right !important">
                    {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_valor_neto)}}
                  </td>
                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('PERCENT', $billingDetail->bd_porcentaje_vat)}}
                    </td>
                  <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total_vat)}}
                    </td>
                  <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total)}}
                        <?php
                            $subtotal = $subtotal + $billingDetail->bd_valor_neto;
                            $impuestos = $impuestos + $billingDetail->bd_total_vat;
                            $total = $total + $billingDetail->bd_total;
                        ?>
                    </td>
                </tr>

                <tr>
                    <td style="border: 0px"></td>
                    <td style="border: 0px"></td>
                    <td style="border: 0px"></td>
                    <td style="border: 0px"></td>
                    <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
                    
                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY',$billingDetail->bd_valor_neto)}}
                    </td>
                    <td style="text-align:right !important">X</td>
                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total_vat)}}
                    </td>
                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total)}}
                    </td>
                </tr>    
                
             </table>
        	</td>
        </tr>
        
        <tr>
        	<td style="border: none; text-align:center !important;">POZYCJE PO KOREKCIE:</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość</th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                
                    <tr>
                        <td>
                            1
                        </td>
                        <td>Wynajem powierzchni magazynowej {{$originalBilling->storages->sqm}} m2 </td>
                        <td>
                            m-c
                        </td>
                        <td>
                            {{$originalBillingDetail->bd_numero}}
                        </td>
                        <td style="text-align:right !important">
                          <?php 
                                echo App\Library\Formato::formatear("MONEY",$subtotal); 
                              ?>
                        </td>
                        <td style="text-align:right !important">
                          <?php 
                                echo App\Library\Formato::formatear("MONEY",$subtotal); 
                              ?>
                        </td>
                        <td style="text-align:right !important">
                            {{App\Library\Formato::formatear('PERCENT', $originalBillingDetail->bd_porcentaje_vat)}}
                        </td>
	                      <td style="text-align:right !important">
                          <?php 
                              echo App\Library\Formato::formatear("MONEY",$impuestos); 
                            ?>
                        </td>
	                      <td style="text-align:right !important">
                            <?php 
                              echo App\Library\Formato::formatear("MONEY",$total); 
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: 0px"></td>
                        <td style="border: 0px"></td>
                        <td style="border: 0px"></td>
                        <td style="border: 0px"></td>
                        <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
                        
                        <td style="text-align:right !important">
                          <?php 
                            echo App\Library\Formato::formatear("MONEY",$subtotal); 
                          ?>
                        </td>
                        <td style="text-align:right !important">X</td>
                        <td style="text-align:right !important">
                          <?php 
                            echo App\Library\Formato::formatear("MONEY",$impuestos); 
                          ?>
                        </td>
                        <td style="text-align:right !important">
                          <?php 
                            echo App\Library\Formato::formatear("MONEY",$total); 
                          ?>
                        </td>
                    </tr>
                    
               
                  <tr style="border: none !important">
                    <td colspan="9" style="border: none !important"></td>
                  </tr>
                	<tr>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
	                   	<td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_valor_neto)}}
                      </td>
	                    <td style="text-align:right !important">X</td>
	                    <td style="text-align:right !important">
                          {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total_vat)}}
                        </td>
	                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total)}}
                      </td>
	                </tr>
	                <tr>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">W tym</td>
	                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_valor_neto)}}
                      </td>
	                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('PERCENT', $originalBillingDetail->bd_porcentaje_vat)}}
                      </td>
	                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total_vat)}}
                        </td>
	                    <td style="text-align:right !important">
                        {{App\Library\Formato::formatear('MONEY', $billingDetail->bd_total)}}
                      </td>
	                </tr>
	        		
                
                <!-- END RAZEM FINAL  -->
             </table>
        	</td>
        </tr>
        

        <tr>
          <td style="border: none">
            <table>
              <tr>
                <td colspan="2" style="padding: 15px;">
                  Przyczyna korekty : <b>{{$billing->bi_razon_correccion}}</b>
                </td>
              </tr>
              <tr style="border:1px solid black !important">
                <td style="padding: 15px; border:none">
                  Razem do zapłaty: <b><span style="font-size:12px">
                    {{App\Library\Formato::formatear("MONEY",$billing->bi_total)}}
                  </span></b>
                </td>
                <td style="padding: 15px; border:none">
                	w tym zwiekszenie podatku VAT : <b> {{App\Library\Formato::formatear("MONEY",$billing->bi_total_vat)}} </b>
                	<br/>
					      Pozosta o do zapłaty : <b> 
                  <?php 
                      echo App\Library\Formato::formatear("MONEY",$total); 
                    ?>
                </b> 
                </td>
              </tr>
              <tr>
                <td colspan="2" style="padding: 15px;">
                  <b>Słownie:</b> {{App\Library\MoneyString::transformQtyCurrency($billing->bi_total)}}
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="firm">
              <tr>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do odebrania dokumentu
                </td>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do wystawienia dokumentu
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table  class="items" style="width:40%">
              <tr>
                <td style="border:none;text-align:right">
                 @if(isset($data['copy']))
                  <table>
                  	<tr>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">WN</th>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">Kwota</th>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">MA</th>
                  	</tr>
                  	<tr style="border:none !important">
                  		<td style="vertical-align: top; ">201-{{$data['accountant_number']}}</td>
                  		<td>
                  			{{ App\Library\Formato::formatear('MONEY',$data['grandTotal']) }}
                  			<br/>
                  			@if(isset($data['boxNeto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['boxNeto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
	                    	@endif
                  			<br/>
                  			@if($data['insurance'] > 0)
                  				@if(isset($data['insurance_neto']))
	                				{{App\Library\Formato::formatear('MONEY',$data['insurance_neto'])}}
		                		@else
		                			{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}
		                		@endif
                  			@endif
                  			<br/>
                  			{{App\Library\Formato::formatear('MONEY',$data['vatTotal'])}}
                  		</td>
                  		<td>
                  			<br/>
                  			<br/>
                  			700-102010
                  			@if($data['insurance'] > 0)
	                  			<br/>
	                  			700-102020
                  			@endif
                  			<br/>
                  			221-102300
                  		</td>
                  	</tr>
                  </table>
                 @else
                 	<!-- <img src="http://pw.novacloud.link/assets/images/krd.png" alt=""> -->
                 @endif
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </body>
</html>