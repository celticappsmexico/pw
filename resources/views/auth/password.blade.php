@extends('app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('client.reset_title') }}</div>
                 <div class="panel-body">
                    {!! Form::open(['route' => 'password/email', 'class' => 'form']) !!}
                        <div class="form-group">
                            <label>{{ trans('client.email_lbl') }}</label>
                            {!! Form::email('email', old('email'), ['class'=> 'form-control']) !!}
                        </div>
                        {!! Form::submit(trans('client.sendResetPass_lbl') ,['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection