@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('client.signup_lbl') }}</div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'auth/register', 'class' => 'form', 'name' => 'forRegister', 'id' => 'forRegister']) !!}

                      <div class="form-group">
                          <label>{{ trans('client.username_lbl') }}</label>
                          {!! Form::input('text', 'username', '', ['class'=> 'form-control','id'=>'username']) !!}
                      </div>

                        <div class="form-group">
                            <label>{{ trans('client.name_lbl') }}</label>
                            {!! Form::input('text', 'name', '', ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.last_lbl') }}</label>
                            {!! Form::input('text', 'lastName', '', ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.birthday_lbl') }}</label>
                            {!! Form::input('text', 'birthday', '', ['class'=> 'form-control datepicker']) !!}
                        </div>

                        <fieldset>
                          <legend>{{ trans('client.address_lbl') }}</legend>

                          <div class="form-group">
                              <label>{{ trans('client.street_lbl') }}</label>
                              {!! Form::input('text', 'street', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.number_lbl') }}</label>
                              {!! Form::input('text', 'number', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.apartment_lbl') }}</label>
                              {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.postCode_lbl') }}</label>
                              {!! Form::input('text', 'postCode', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.city_lbl') }}</label>
                              {!! Form::input('text', 'city', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.country_lbl') }}</label>
                              {!! Form::input('text', 'country', '', ['class'=> 'form-control']) !!}
                          </div>

                        </fieldset>

                        <div class="form-group">
                            <label>{{ trans('client.pesel_lbl') }}</label>
                            {!! Form::text('peselNumber', '', ['class'=> 'form-control', 'id' => 'peselNumber']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.id_lbl') }}</label>
                            {!! Form::text('idNumber', '', ['class'=> 'form-control', 'id' => 'idNumber']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.phone_lbl') }}</label>
                            {!! Form::text('phone', '', ['class'=> 'form-control', 'id' => 'phone']) !!}
                        </div>

                        {!! Form::hidden('hdfr', '', ['class'=> 'form-control', 'value' => '3']) !!}
                        {!! Form::hidden('hdfut', '', ['class'=> 'form-control', 'value' => '1']) !!}

                        <div class="form-group">
                            <label>{{ trans('client.email_lbl') }}</label>
                            {!! Form::email('email', '', ['class'=> 'form-control', 'id' => 'email']) !!}
                        </div>
                        <div class="form-group">
                            <label>{{ trans('client.password_lbl') }}</label>
                            {!! Form::password('password', ['class'=> 'form-control', 'id' => 'password']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.passConfirm_lbl') }}</label>
                            {!! Form::password('password_confirmation', ['class'=> 'form-control', 'id' => 'repassword']) !!}
                        </div>

                        <div>
                            {!! Form::submit(trans('client.send_lbl'),['class' => 'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script>

        $('.datepicker').datetimepicker({
          format: 'Y-MM-DD',
          allowInputToggle: true,
          //minDate: moment(),
        });
</script>