@extends('app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('auth.loginHead') }}</div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'auth/login', 'class' => 'form']) !!}
                            <div class="form-group">
                                <label>{{ trans('auth.usernameTag') }}</label>
                                {!! Form::text('username', '', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>{{ trans('auth.passwordTag') }}</label>
                                {!! Form::password('password', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="checkbox">
                                <label><input name="remember" type="checkbox">{{ trans('auth.rememberTag') }}</label>
                            </div>
                            <div>
                                {!! Form::submit(trans('auth.loginBtnTag'),['class' => 'btn btn-primary']) !!}
                                <a href="{{ url('password/email') }}">{{ trans('auth.forgotTag') }}</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
