@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('client.signup_lbl') }}</div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'completeUser', 'class' => 'form', 'name' => 'forRegister', 'id' => 'forRegister']) !!}
                        <div class="form-group">
                            <label>{{ trans('client.name_lbl') }}</label>
                            {!! Form::input('text', 'name', '', ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.last_lbl') }}</label>
                            {!! Form::input('text', 'lastName', '', ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group" style="border:2px solid blue">
                            <label>{{ trans('client.birthday_lbl') }}</label>
                            {!! Form::input('text', 'birthday', '', ['class'=> 'form-control datepicker']) !!}
                        </div>

                        <fieldset>
                          <legend>{{ trans('client.address_lbl') }}</legend>

                          <div class="form-group">
                              <label>{{ trans('client.street_lbl') }}</label>
                              {!! Form::input('text', 'street', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.number_lbl') }}</label>
                              {!! Form::input('text', 'number', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.apartment_lbl') }}</label>
                              {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.postCode_lbl') }}</label>
                              {!! Form::input('text', 'postCode', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.city_lbl') }}</label>
                              {!! Form::input('text', 'city', '', ['class'=> 'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <label>{{ trans('client.country_lbl') }}</label>
                              {!! Form::input('text', 'country', '', ['class'=> 'form-control']) !!}
                          </div>

                        </fieldset>

                        <div class="form-group">
                            <label>{{ trans('client.pesel_lbl') }}</label>
                            {!! Form::text('peselNumber', '', ['class'=> 'form-control', 'id' => 'peselNumber']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.id_lbl') }}</label>
                            {!! Form::text('idNumber', '', ['class'=> 'form-control', 'id' => 'idNumber']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.phone_lbl') }}</label>
                            {!! Form::text('phone', '', ['class'=> 'form-control', 'id' => 'phone']) !!}
                        </div>

                        <div>
                            {!! Form::submit(trans('client.send_lbl'),['class' => 'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>

$('.datepicker').datetimepicker({
    format: 'D/M/Y',
    allowInputToggle: true,
    //minDate: moment(),
  });

</script>
@endpush