@extends('app')
@section('content')
<style>
.alert-info , .alert-warning {
    margin-top: 100px !important;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>Success</h4>
                <ul>
                    Password reset successfully
                </ul>
            </div>
            
        </div>
    </div>
</div>
@endsection

