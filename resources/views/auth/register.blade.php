@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('client.signup_lbl') }}</div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'auth/register', 'class' => 'form', 'name' => 'forRegister', 'id' => 'forRegister']) !!}
                        <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label>{{ trans('client.username_lbl') }}</label>
                          {!! Form::input('text', 'username', '', ['class'=> 'form-control','id'=>'username']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.email_lbl') }}</label>
                            {!! Form::email('email', '', ['class'=> 'form-control', 'id' => 'email']) !!}
                        </div>

                        {!! Form::hidden('hdfr', '', ['class'=> 'form-control', 'value' => '3']) !!}
                        {!! Form::hidden('hdfut', '', ['class'=> 'form-control', 'value' => '1']) !!}

                        <div class="form-group">
                            <label>{{ trans('client.password_lbl') }}</label>
                            {!! Form::password('password', ['class'=> 'form-control', 'id' => 'password']) !!}
                        </div>

                        <div class="form-group">
                            <label>{{ trans('client.passConfirm_lbl') }}</label>
                            {!! Form::password('password_confirmation', ['class'=> 'form-control', 'id' => 'repassword']) !!}
                        </div>

                        <div>
                            {!! Form::submit(trans('client.send_lbl'),['class' => 'btn btn-primary']) !!}
                            
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
