<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <style media="screen">
        	 
        	
            *{
              margin: 0;
              padding:0;
              font-size: 9.5px;
              font-family: DejaVu Sans;
            }

            .tblWrap{
              margin: 25px;
              /*width: 800px;*/
            }

            .firm{
              border-collapse: seperate;
              border-spacing: 50px!important;
            }

            .faktura-head{
              border-collapse: seperate;
              border-spacing: 3px!important;
            }
            .faktura-head td{
              text-align: center!important;
              margin: 0px;
              padding: 5px;
            }

            .dataWrap{
              border: none;
            }

            .dataWrap td{
              border: solid 1px #000;
            }

            .dataWrap td table td{
              border: none;
            }

            .dataWrap td{
              width: 50%;
            }

            .dataWrap td table{
              width: 100%;
            }

            .dataWrap td table .data{
              width: 15%;
            }


            .dataWrap td table .data2{
              font-weight: bold;
            }

            .items{
              border-collapse: collapse;
              border-spacing: 0px!important;
            }

            #row-1 {
                border: 1px solid black;
                text-align: center;
                /*padding-top: 80px;*/
                font-size: 11px;
            }
            .col-md-3 {
                margin-left: 3px;
                width: 313.3px;
            }
            #row-2 {
                border: 1px solid black;
                margin: 2px;
                width: 42.56%;
            }
            .faktura {
                border: 1px solid black;
                margin-top: 2px;
                background-color: #ccc;
                text-align: center;
            }
            .row.warszawa {
                border: 1px solid black;
                margin-top: 2px;
                text-align: center;
                height: 36px;
            }
            #span-2-warszawa {
                font-size: 10px;
            }
            .warszawa-fecha {
                margin-bottom: -5px;
            }
            .row.num-fact, .row.sprzedazy {
                border: 1px solid black;
                margin-top: 2px;
                height: 30.5px;
                text-align: center;
            }
            .kopia {
                border: 0px #FFF;
                text-align: center;
            }
            #div-row-3 {
                border: 1px solid black;
                margin-left: 17px;
                text-align: left;
                width: 69.4%;
                margin-top: 2px;
            }
            .col-md-3.forma, .col-md-3.termin {
                width: 220px;
                margin-left: -25px !important;
            }
            .col-md-3.bank {
                width: 140px;
            }
            .col-md-3.konto {
                width: 360px;
            }
            table {
              width: 100%;
                margin-top: 4px;
                margin-left: 2px;
            }
            th {
                border: 1px solid black;
                text-align: center;
                background-color: #ccc;
                padding:3px;
            }
            td {
                border: 1px solid black;
                text-align: left !important;
				padding:2px !important;
            }
        </style>
    </head>
    <body>
      <table class="tblWrap">
        <tr>
          <td style="border: none">
            <table class="faktura-head">
              <tr>
                <td rowspan="2" id="row-1">
                	<img src="http://pw.novacloud.link/assets/images/logo.png" style="width:160px"/>
                </td>
                <td rowspan="2" class="faktura">
                  <b>Faktura korygująca</b>
                </td>
                <td class="num-fact">
                  <b>{{ $data['2y'] }}-KPW-{{ $data['invoiceCorrected'] }}</b>
                </td>
              </tr>
              <tr>
                <td class="kopia">
                @if(isset($data['copy']))
                	<b>KOPIA</b>
                @else
                	<b>ORYGINAŁ</b>
                @endif
                  
                </td>
              </tr>
              <tr>
                <td colspan="2" class="warszawa-fecha">
                	<table style="border:none !important">
						<tr style="border:none !important">
							<td colspan="5" style="border:none !important">
								{{ $data['2y'] }}-PW-{{ $data['invoiceNumber'] }} , {{ $data['fechaOriginal'] }}</span><br><span id="span-2-warszawa">numer, data dokumentu korygowanego</span>
							</td>
							<td colspan="5" style="border:none !important">
								<span id="span-1-warszawa">{{ $data['mes'] }} {{ $data['year'] }}</span><br>
				                  		<span id="span-2-warszawa">okres sprzedaży</span>
							</td>
						</tr>
					</table>
                  	
                </td>
                <td>
                  <span id="span-1-warszawa">{{ $data['currentDate'] }}  {{ $data['place'] }}</span><br>
                  <span id="span-2-warszawa">data i miejsce wystawienia dokumentu</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="dataWrap">
              <tr>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Sprzedawca:
                      </td>
                      <td class="data2">
                        {{ $data['seller'] }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ $data['address'] }}
                      </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        5272703362
	                      </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Nabywca:
                      </td>
                      <td class="data2">
                        {{ $data['clientName'] }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ $data['clientAddress'] }}
                      </td>
                    </tr>
                    <tr>
                    	@if($data['clientType'] == 1)
                    		<td class="data">
		                        Pesel:
		                    </td>
		                    <td class="data2">
		                        {{ $data['clientPesel'] }}
		                    </td>
		                  @else
	                      <td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        {{ $data['clientNip'] }}
	                      </td>
	                      @endif
                    </tr>
                  </table>
                </td>
              </tr>
              
              <!-- <TR> -->
              <!--  
              <tr>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Forma pÅ‚atnoÅ›ci:
                      </td>
                      <td class="data2">
                        <span style="font-weight:bold">{{ $data['paymentType'] }}</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Termin pÅ‚atnoÅ›ci:
                      </td>
                      <td class="data2">
                        <span style="font-weight:bold">{{ $data['date'] }}</span>
                      </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        Bank:
	                    </td>
	                    <td class="data2">
	                        <span style="font-weight:bold">{{ $data['bank'] }}</span>
	                    </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        Konto:
	                    </td>
	                    <td class="data2">
	                        <span style="font-weight:bold">{{ $data['account'] }}</span>
	                    </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Odbiorca:
                      </td>
                      <td class="data2">
                        PENDING
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        PENDING
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              -->
              <!-- </TR> -->
            </table>
          </td>
        </tr>
        
         <tr>
          <td style="border: none">
            <table>
              <tr>
                <td>
                  Forma płatności: : <span style="font-weight:bold">{{ $data['paymentType'] }}</span>
                  Termin płatności: <span style="font-weight:bold">{{ $data['date'] }}</span>
                  Bank: <span style="font-weight:bold">{{ $data['bank'] }}</span>
                  Konto: <span style="font-weight:bold">{{ $data['account'] }}</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align: center !important">POZYCJE KORYGOWANE</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>                    
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość </th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                @if(isset($data['box']))
	                <tr>
	                    <td>1</td>
	                    <td>Wynajem powierzchni magazynowej {{$data['storage']->sqm}} m2 
	                    
	                    @if(isset($data['period']))
	                    	za okres {{$data['period']}} r.
	                    @endif
	                    
	                    box {{$data['storage']->alias}}
	                    </td>
	                    <td>
	                    	@if(isset($data['mesesPrepago']))
	                    		m-c
	                    	@else
	                    		szt
	                    	@endif
	                    	
	                    </td>
	                    <td>
	                    	@if(isset($data['mesesPrepago']))
	                    		{{$data['mesesPrepago']}}
	                    	@else
	                    			1
	                    	@endif
	                    </td>
	                    <td style="text-align:right !important">
	                    	@if(isset($data['cenaNetto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['cenaNetto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
	                    	@endif
	                    </td>
	                    <td style="text-align:right !important">
	                    	@if(isset($data['boxNeto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['boxNeto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
	                    	@endif
	                    	
	                    </td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY', $data['vatTotalInsurance'])}}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY', $data['boxBruto'])}}</td>
	                </tr>
	                
                	@if($data['insurance'] > 0)
                		<tr>
                			<td>2</td>
	                		<td>
	                			ubezpieczenie pomieszczenia magazynowego
	                			@if(isset($data['period']))
				                   	za okres {{$data['period']}} r.
				                @endif
				                
				                ubezpieczenie
	                		</td>
	                		<td>szt</td>
	                		<td>
		                		@if(isset($data['mesesPrepago']))
		                    		{{$data['mesesPrepago']}}
		                    	@else
		                    			1
		                    	@endif
	                		</td>
	                		@if(isset($data['insurance_neto']))
	                			<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance_neto'])}}</td>
	                		@else
	                			<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}</td>
	                		@endif
		                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}</td>
		                    <td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
		                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['vatInsurance'])}}</td>
		                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insuranceBruto'])}}</td>
	                	</tr>
	                @endif
	                
	                <tr>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
	                    @if(isset($data['suma1']))
	                    	<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['suma1'])}}</td>
	                    @else
	                    	<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['subtotal'])}}</td>
	                    @endif
	                    <td style="text-align:right !important">X</td>
	                    @if(isset($data['suma2']))
	                    	<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['suma2'])}}</td>
	                    @else
	                    	<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['vatTotal'])}}</td>
	                    @endif
	                    <td style="text-align:right !important">{{ App\Library\Formato::formatear('MONEY',$data['grandTotal']) }} </td>
	                </tr>
	                
                @endif
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align: center !important">KOREKTY CENY: </td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość</th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                <tr>
                	<td>1</td>
                	 <td>
                	 	Wynajem powierzchni magazynowej {{$data['storage']->sqm}} m2 
	                    
	                    @if(isset($data['period']))
	                    	za okres {{$data['period']}} r.
	                    @endif
	                    
	                    box {{$data['storage']->alias}}
	                </td>
                	<td>
                    	@if(isset($data['mesesPrepago']))
                    		m-c
                    	@else
                    		szt
                    	@endif
                    	
                    </td>
                	<td>{{$data['llosc_correccion']}}</td>
                	<td style="text-align:right !important">
                		@if($data['correction_price'] == 1)
	                    	{{App\Library\Formato::formatear('MONEY', $data['cenna_netto_correccion'])}}
	                    @else
                			@if(isset($data['cenaNetto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['cenaNetto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
	                    	@endif
	                    @endif
                	</td>
                	<td style="text-align:right !important">{{$data['wartosc_netto_correccion']}}</td>
                	<td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
                	<td style="text-align:right !important">{{$data['kwota_vat_correccion']}}</td>
                	<td style="text-align:right !important">{{$data['wartosc_brutto_correccion']}}</td>
                </tr>
                @if($data['insurance'] > 0)
                <tr>
                	<td>2</td>
                	<td>
                		ubezpieczenie pomieszczenia magazynowego
                		@if(isset($data['period']))
			            	za okres {{$data['period']}} r.
			            @endif
			                
			        	ubezpieczenie
                	</td>
                	<td>szt</td>
                	<td>{{$data['llosc_correccion_ins']}}</td>
                	@if($data['correction_price'] == 1)
                		<td style="text-align:right !important">
	                   		{{App\Library\Formato::formatear('MONEY', $data['cenna_netto_correccion_ins'])}}
	                   	</td>
	                @else
	                	@if(isset($data['insurance_neto']))
	                		<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance_neto'])}}</td>
	                	@else
	                		<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}</td>
	                	@endif
	                @endif
		              
                	<td style="text-align:right !important">{{$data['wartosc_netto_correccion_ins']}}</td>
                	<td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
                	<td style="text-align:right !important">{{$data['kwota_vat_correccion_ins']}}</td>
                	<td style="text-align:right !important">{{$data['wartosc_brutto_correccion_ins']}}</td>
                </tr>
                @endif
                
                 <tr>
		        	<td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
			        
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_netto_correccion'])}}</td>
			        <td style="text-align:right !important">X</td>
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_kwota_vat_correccion'])}}</td>
			       	<td style="text-align:right !important">{{ App\Library\Formato::formatear('MONEY',$data['razem_wartosc_brutto_correccion']) }} </td>
				</tr>
                
             </table>
        	</td>
        </tr>
        
        <tr>
        	<td style="border: none; text-align:center !important;">POZYCJE PO KOREKCIE:</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                    <th style="padding:4px">Lp.</th>
                    <th>Nazwa, kod towaru, PKWiU</th>
                    <th style="padding:6px">Jm</th>
                    <th style="padding:4px">llość</th>
                    <th>Cena netto</th>
                    <th>Wartość netto</th>
                    <th>Stawka VAT</th>
                    <th>Kwota VAT</th>
                    <th>Wartość brutto</th>
                </tr>
                <tr>
                	<td>1</td>
                	 <td>
                	 	Wynajem powierzchni magazynowej {{$data['storage']->sqm}} m2 
	                    
	                    @if(isset($data['period']))
	                    	za okres {{$data['period']}} r.
	                    @endif
	                    
	                    box {{$data['storage']->alias}}
	                </td>
                	<td>
                    	@if(isset($data['mesesPrepago']))
                    		m-c
                    	@else
                    		szt
                    	@endif
                    	
                    </td>
                	<td>{{$data['llosc_corregido']}}</td>
                	<td style="text-align:right !important">
                		@if($data['correction_price'] == 1)
	                    	{{App\Library\Formato::formatear('MONEY', $data['cenna_netto_corregido'])}}
	                    @else
	                		@if(isset($data['cenaNetto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['cenaNetto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
                    		@endif
                    	@endif
                	</td>
                	<td style="text-align:right !important">{{$data['wartosc_netto_corregido']}}</td>
                	<td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
                	<td style="text-align:right !important">{{$data['kwota_vat_corregido']}}</td>
                	<td style="text-align:right !important">{{$data['wartosc_brutto_corregido']}}</td>
                </tr>
                @if($data['insurance'] > 0)
                <tr>
                	<td>2</td>
                	<td>
                		ubezpieczenie pomieszczenia magazynowego
                		@if(isset($data['period']))
			            	za okres {{$data['period']}} r.
			            @endif
			                
			        	ubezpieczenie
                	</td>
                	<td>szt</td>
                	<td>{{$data['llosc_corregido_ins']}}</td>
                	@if($data['correction_price'] == 1)
                		<td style="text-align:right !important">
	                  		{{App\Library\Formato::formatear('MONEY', $data['cenna_netto_corregido_ins'])}}
	                  	</td>
	                @else
	                	@if(isset($data['insurance_neto']))
	                		<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance_neto'])}}</td>
	                	@else
	                		<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}</td>
	                	@endif
	                @endif
                	<td style="text-align:right !important">{{$data['wartosc_netto_corregido_ins']}}</td>
                	<td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT', $data['vat'])}}</td>
                	<td style="text-align:right !important">{{$data['kwota_vat_corregido_ins']}}</td>
                	<td style="text-align:right !important">{{$data['wartosc_brutto_corregido_ins']}}</td>
                </tr>
                @endif
                
                <tr>
		        	<td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_netto_corregido'])}}</td>
			        <td style="text-align:right !important">X</td>
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_kwota_vat_corregido'])}}</td>
			        <td style="text-align:right !important">{{ App\Library\Formato::formatear('MONEY',$data['razem_wartosc_brutto_corregido']) }} </td>
				</tr>
				
				
				<!-- RAZEM FINAL -->
				
				<!-- 
				 <tr>
		        	<td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="border: 0px"></td>
			        <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
			        
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_netto_correccion'])}}</td>
			        <td style="text-align:right !important">X</td>
			        <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_kwota_vat_correccion'])}}</td>
			       	<td style="text-align:right !important">{{ App\Library\Formato::formatear('MONEY',$data['razem_wartosc_brutto_correccion']) }} </td>
				</tr>
				 -->
				
				<tr style="border: none !important">
					<td colspan="9" style="border: none !important"></td>
				</tr>
                	<tr>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">RAZEM</td>
	                   	<td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_netto_correccion'])}}</td>
	                    <td style="text-align:right !important">X</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_kwota_vat_correccion'])}}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_brutto_correccion'])}} </td>
	                </tr>
	                <tr>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="border: 0px"></td>
	                    <td style="background-color: #ccc; text-align:center; font-weight:bold">W tym</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_netto_correccion'])}}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('PERCENT',$data['vat']) }}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_kwota_vat_correccion'])}}</td>
	                    <td style="text-align:right !important">{{App\Library\Formato::formatear('MONEY',$data['razem_wartosc_brutto_correccion'])}} </td>
	                </tr>
	        		
                
                <!-- END RAZEM FINAL  -->
             </table>
        	</td>
        </tr>
        

        <tr>
          <td style="border: none">
            <table>
              <tr>
                <td colspan="2" style="padding: 15px;">
                  Przyczyna korekty : <b>{{$data['reason_correction']}}</b>
                </td>
              </tr>
              <tr style="border:1px solid black !important">
                <td style="padding: 15px; border:none">
                  Razem do zwrotu: <b><span style="font-size:12px">{{ App\Library\Formato::formatear('ZLOTY',($data['razem_wartosc_brutto_correccion'] * (-1))) }} </span></b>
                </td>
                <td style="padding: 15px; border:none">
                	w tym zmniejszenie podatku VAT : <b> {{ App\Library\Formato::formatear('ZLOTY',($data['razem_kwota_vat_correccion'] * (-1))) }} </b>
                	<br/>
					Pozosta o do zwrotu : <b> {{ App\Library\Formato::formatear('ZLOTY',($data['razem_wartosc_brutto_correccion'] * (-1))) }} </b> 
                </td>
              </tr>
              <tr>
                <td colspan="2" style="padding: 15px;">
                  <b>Słownie:</b> {{ $data['grandTotalString'] }}
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="firm">
              <tr>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do odebrania dokumentu
                </td>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do wystawienia dokumentu
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table  class="items" style="width:40%">
              <tr>
                <td style="border:none;text-align:right">
                 @if(isset($data['copy']))
                  <table>
                  	<tr>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">WN</th>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">Kwota</th>
                  		<th style="background-color: #ccc; text-align:center; font-weight:bold">MA</th>
                  	</tr>
                  	<tr style="border:none !important">
                  		<td style="vertical-align: top; ">201-{{$data['accountant_number']}}</td>
                  		<td>
                  			{{ App\Library\Formato::formatear('MONEY',$data['grandTotal']) }}
                  			<br/>
                  			@if(isset($data['boxNeto']))
	                    		{{App\Library\Formato::formatear('MONEY', $data['boxNeto'])}}
	                    	@else
	                    		{{App\Library\Formato::formatear('MONEY', $data['box'])}}
	                    	@endif
                  			<br/>
                  			@if($data['insurance'] > 0)
                  				@if(isset($data['insurance_neto']))
	                				{{App\Library\Formato::formatear('MONEY',$data['insurance_neto'])}}
		                		@else
		                			{{App\Library\Formato::formatear('MONEY',$data['insurance'])}}
		                		@endif
                  			@endif
                  			<br/>
                  			{{App\Library\Formato::formatear('MONEY',$data['vatTotal'])}}
                  		</td>
                  		<td>
                  			<br/>
                  			<br/>
                  			700-102010
                  			@if($data['insurance'] > 0)
	                  			<br/>
	                  			700-102020
                  			@endif
                  			<br/>
                  			221-102300
                  		</td>
                  	</tr>
                  </table>
                 @else
                 	<!-- <img src="http://pw.novacloud.link/assets/images/krd.png" alt=""> -->
                 @endif
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </body>
</html>