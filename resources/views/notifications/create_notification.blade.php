<form action="{{route('notification.store')}}" method="post" id="frmNotification">
{{ csrf_field() }}
	<div class="modal-header">
		<h4 class="modal-title" id="">Create Notification</h4>
	</div>
	<div class="modal-body">
	    <div class="row" >
	      	<div class="col-md-12">
	      		<div class="form-group">
	      			<label>Message</label>
	      			<textarea name="message" class="form-control"></textarea>
	      		</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script>


$('#frmNotification').validate({
	      rules: {
	          message: {
	        	  minlength: 5,	             
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {

		      	//console.log($("#frmPayBilling").attr('action'));

	        	$("#frmNotification").find('button').prop('disabled',true);
	        	
	             $.ajax({
	               url: $("#frmNotification").attr('action'),
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               //console.info(data);
	               //alert(JSON.stringify(data));
	               var message = "";
	               if (data.returnCode == 200) {
		               
		                toastr["success"](data.msg);
		                $('#modal-generico-small').modal('hide');
	               }else{
	                 	toastr["error"](data.msg);
	                 	message = data.msg;
	               }
	
	             })
	             .fail(function() {
	               //console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               //console.log("complete");
	             });
	
	
	      return false; // required to block normal submit since you used ajax
	     }
	  });
</script>