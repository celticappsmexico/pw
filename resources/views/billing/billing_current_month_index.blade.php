<div class="modal-header">
	<h4 class="modal-title" id=""> Detail </h4>
</div>
<div class="modal-body">
      <div class="row" >
      	<div class="col-md-12">      		
			<table class="table table-hover" id="billing-current-table">
				<thead>
					<tr>
						<th>{{Lang::get('client.id_lbl')}}</th>
                        <th>Tenant</th>
						<th>Storage</th>
						<th>NET</th>
						<th>% VAT</th>
						<th>Total VAT</th>
						<th>TOTAL</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
        {{ trans('client.close_lbl') }}
    </button>
</div>
<script>
      		
var oTableBilling = $('#billing-current-table').DataTable({
	"bInfo" : false,
	processing: true,
	serverSide: true,
	bFilter : true,
    paging: false,
	"order": [[ 0, 'desc' ]],
	ajax: {
		url : "{!! route('billing_current_month.index.data') !!}",
		data: function (d) {
			
		}
	},
	columns: [
		{ data: 'id', name: 'id'},
        { data: 'tenant', name: 'tenant'},
		{ data: 'storage', name: 'storage'},
        { data: 'bd_valor_neto', name: 'bd_valor_neto'},
        { data: 'bd_porcentaje_vat', name: 'bd_porcentaje_vat'},
        { data: 'bd_total_vat', name: 'bd_total_vat'},
        { data: 'bd_total', name: 'bd_total'},
	],
	language : {
		lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
		zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
		info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
		infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
		infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
		search:         "{{ trans('client.dt_find_lbl') }}:",
		paginate: {
			first:      "{{ trans('client.dt_first_lbl') }}",
			last:       "{{ trans('client.dt_last_lbl') }}",
			next:       "{{ trans('client.dt_next_lbl') }}",
			previous:   "{{ trans('client.dt_prev_lbl') }}"
		},
		loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
		processing:     "{{ trans('client.dt_processing_lbl') }}",
	}
});




</script>