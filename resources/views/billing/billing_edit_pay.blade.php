<style>

</style>

<form action="{{route('billing_update.pay')}}" method="post" id="frmPayBilling">
{{ csrf_field() }}
<input type="hidden" name="id" value="{{$id}}" />
	<div class="modal-header">
		<h4 class="modal-title" id="">{{ trans('client.pay_now') }} </h4>
	</div>
	<div class="modal-body">
	      <div class="row" >
	      	@if($billing->id_billing_correccion)
	      		<div class="col-md-12">
		      		<table class="table table-hover dataTable no-footer">
		      			<tr>
		      				<th colspan="2" style="text-align:center !important">PAYMENT SUMMARY</th>
		      			</tr>
		      			<tr>
		      				<td>Debit:</td>
		      				<td><b>{{App\Library\Formato::formatear('MONEY', $billing->cargo)}}</b></td>
		      			</tr>
		      			<tr>
		      				<td>Payment:</td>
		      				<td><b>{{App\Library\Formato::formatear('MONEY', $billingCorreccion->abono)}}</b></td>
		      			</tr>
		      			<tr style="color:red">
		      				<td>Outstanding:</td>
		      				<td><b>{{ $pay = $billing->cargo - $billingCorreccion->abono}}</b></td>
		      			</tr>
		      			
		      		</table>
		      	</div>
	      	@endif
	      	<div class="col-md-12">
	      		<div class="form-group">
	      			<label>{{Lang::get('settings.payment_type')}}</label>
	      			<select name="payment_type_id" id="payment_type_id" class="form-control">
	      				{{\App\Library\Combo::render($paymentsType,'','id','name',false)}}
	      			</select>
	      		</div>
			</div>
			
			<div class="col-md-12">
				<div class="wrapBraintree" id="braintree-cards-pay"></div>
			</div>
			
			<br/>
			
			<div class="col-md-12" id="div-credit-card" style="display:none">
				<input type="hidden" name="idCardUser" id="idCardUserInvoice" />
				<div class="form-group">
					<label>{{ trans('cart.credit_card') }}</label>
						 <input type="text" name="mask_card_user" id="mask-card-user" readonly class="form-control" />
				</div>
			</div>
			
			<div class="col-md-12">
	      		<div class="form-group">
	      			<label>{{Lang::get('cart.reference')}}</label>
	      			<input type="text" name="reference" class="form-control" autocomplete="off" />
	      		</div>
			</div>
		</div>
		
		
		
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" id="btnSaveBilling" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script>

@if($paymentData->creditcard == 2)
	$("#payment_type_id").children().each(function() {
        if ($(this).val() == "2") {
            $(this).prop('disabled',true);
        }
    });

	//SELECCIONA TRANSFER POR DEFAULT
	$("#payment_type_id").val("3");

@else
	$("#payment_type_id").children().each(function() {
        if ($(this).val() != "2") {
            $(this).prop('disabled',true);
        }
    });

	$("#payment_type_id").val("2");

	var route = "{{ route('cards_users.credit_card_braintree.invoices',['*user*','*storage*','*paymentid*']) }}";
	route = route.replace('*user*','{{$billing->user_id}}');
	route = route.replace('*storage*','{{$billing->storage_id}}');
	route = route.replace('*paymentid*','{{$billing->payment_data_id}}');
	
    $('#braintree-cards-pay').load(route);

    $('#div-credit-card').show();
@endif


$(document).on('click', '.btnSelectCardInvoice', function(event) {
	event.preventDefault();
	
	$(this).attr("disabled","disabled");

	$("#idCardUserInvoice").val( $(this).data('id') );
	$("#mask-card-user").val( $(this).data('mask') );
		
});


$('#frmPayBilling').validate({
	      rules: {
	    	  payment_type_id: {
	              required: true
	          },
	          reference: {
	        	  minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          mask_card_user:{
		          required: true
		      }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {

		      	//console.log($("#frmPayBilling").attr('action'));

		      	$("#btnSaveBilling").hide();            	   	
	        	$("#frmPayBilling").find('button').prop('disabled',true);
	        	
	             $.ajax({
	               url: $("#frmPayBilling").attr('action'),
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               //console.info(data);
	               //alert(JSON.stringify(data));
	               var message = "";
	               if (data.returnCode == 200) {
		               
		                toastr["success"]("{{ trans('messages.pay_saved') }}");
		                //$('#modal-generico-small').modal('hide');
		                oTableBilling.draw();
		                cargaSaldo();
		                message = "{{ trans('messages.pay_saved') }}";
	               }else{
	                 	toastr["error"](data.msg);
	                 	message = data.msg;
	               }

	               var line_transaction = ""; 
	               
	               if(data.transaction_id != null){
	               		line_transaction = '<tr>'+
													'<td>Transaction ID:</td>'+
													'<td>'+ data.transaction_id +'</td>'+
												'</tr>'+
												'<tr>'+
					    							'<td>Status</td>'+
					    							'<td>'+ data.status +'</td>'+
					    						'</tr>'+
					    						
					    						'<tr>'+
					    							'<td>Card Type</td>'+
					    							'<td>'+ data.cardType +'</td>'+
					    						'</tr>'+
					    						
				    							'<tr>'+
				    								'<td>Error:</td>'+
				    								'<td>'+ data.error +'</td>'+
				    							'</tr>';
		               }
	               

	                //$('#modal-generico-small').modal('show');
	               $('#modal-generico-small-content').html(
			                '<div class="panel panel-success" style="text-align: center;">'+
	        					'<div class="panel-heading">'+
	    							'<h1>'+
	    								'<i class="fa fa-check-circle-o" aria-hidden="true"></i> '+ message +
	    							'</h1>'+
	    					'</div>'+
	    					'<div class="panel-body">'+
	    						'<table style="margin: 0 auto; width:100%; font-size:13px" class="">'+
	    							'<tr>'+
	    								'<td>Payment Method: <b>'+ data.payment_method +'</b></td>'+
		    							'<td>Date: <b>'+ data.payment_date +'</b></td>'+
	    							'</tr>'+
	    						'</table>'+					
	    					 
	    						'<table style="margin: 0 auto; margin-top:10px;" class="tabla-resumen">'+
	    							'<tr>'+
	    								'<th colspan="2">CURRENT TRANSACTION</th>'+
	    							'</tr>'+
	    							'<tr>'+
	    								'<td>Amount:</td>'+
	    								'<td>'+ data.amount +'</td>'+
	    							'</tr>'+
		    						line_transaction +
		    						
		    					'</table>'+
		    				'</div>'+
		    			'</div>'+
		    			'<div class="panel-footer">'+
		    				'<button type="button" class="btn btn-primary" data-dismiss="modal">'+	
		    					'Close'+
	    					'</button>'+
		    			'</div>'
			    			);
	
	             })
	             .fail(function() {
	               //console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               //console.log("complete");
	             });
	
	
	      return false; // required to block normal submit since you used ajax
	     }
	  });
</script>