<div class="modal-header">
	<h4 class="modal-title" id="">{{ trans('storages.down_invoice') }} </h4>
</div>
<div class="modal-body">
      <div class="row" >
      	<div class="col-md-12" id="div-saldo">
      	</div>
		@if($braintreeBalance > 0 && $braintreeBalance < 0)
		<div class="col-md-12">
			<h3 class="text-center"> 
				Braintree Balance <strong>{{$braintreeBalance}}</strong>
				<a href="{{route('billing.pay.retry_charge',[$suscription_id, $braintreeBalance])}}" type="button" 
					class="btn btn-primary btn-retry-charge">
					Retry Charge
				</a>
			</h3>
		@endif

		</div>
      	<div class="col-md-12">      		
			<table class="table table-hover" id="billing-table">
				<thead>
					<tr>
						<th>{{Lang::get('client.id_lbl')}}</th>
						<th>{{Lang::get('client.invoice_number')}}</th>
						<th>{{Lang::get('client.pay_month')}}</th>
						<th>{{Lang::get('client.payment')}}</th>
						<th>{{Lang::get('client.charge')}}</th>
						<th>{{Lang::get('client.balance')}}</th>
						<th>{{Lang::get('client.reference')}}</th>
						<th>{{Lang::get('client.pdf_label')}}</th>
						<th>{{Lang::get('client.pay_now')}}</th>
						<th>{{Lang::get('settings.payment_type')}}</th>
						<th>{{Lang::get('client.fix_invoice')}}</th>
						<th>PDF Corrected</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
</div>
<script>

function cargaSaldo(){
	var route="{{route('usuario_saldo_storage',['*storage*','*cliente*'])}}";
	route = route.replace('*storage*','{{$storage_id}}');
	route = route.replace('*cliente*','{{$client_id}}');

	$.get(route, function(response, status){
		 //REGRESA SALDO
		 if(Number(response.saldo) >= 0){
			 $("#div-saldo").html(
					'<div class="alert alert-success">'+	
      					'<h2 class="text-right">{{Lang::get("client.balance")}}: '+response.saldo_formateado +'</h2>'+
      				'</div>' );
		 }else{
			 $("#div-saldo").html(
				'<div class="alert alert-danger">'+	
					'<h2 class="text-right">{{Lang::get("client.balance")}}: '+response.saldo_formateado +'</h2>'+
   				'</div>');
		 }
		 
    });

}

cargaSaldo();
      		
var oTableBilling = $('#billing-table').DataTable({
	"bInfo" : false,
	processing: true,
	serverSide: true,
	bFilter : true,
	"order": [[ 0, 'desc' ]],
	ajax: {
		url : "{!! route('billing_index.data') !!}",
		data: function (d) {
			d.storage_id = '{{$storage_id}}';
			d.client_id  = '{{$client_id}}';
		}
	},
	columns: [
		{ data: 'id', name: 'id'},
		{ data: 'invoice_number', name: 'invoice_number'},
		{ data: 'pay_month', name: 'pay_month'},
		{ data: 'abono', name: 'abono'},
		{ data: 'cargo', name: 'cargo'},
		{ data: 'saldo', name: 'saldo'},
		{ data: 'reference', name: 'reference'},
		{ data: 'download_pdf', name: 'download_pdf', searchable:false, orderable: false},
		{ data: 'pay_now', name: 'pay_now'},
		{ data: 'payment_type', name: 'payment_type'},
		{ data: 'fix', name: 'fix', searchable:false, orderable: false},
		{ data: 'download_pdf_corrected', name: 'download_pdf_corrected', searchable:false, orderable: false},
		/*{ data: 'print', name: 'print', searchable:false, orderable: false},*/
	],
	language : {
		lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
		zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
		info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
		infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
		infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
		search:         "{{ trans('client.dt_find_lbl') }}:",
		paginate: {
			first:      "{{ trans('client.dt_first_lbl') }}",
			last:       "{{ trans('client.dt_last_lbl') }}",
			next:       "{{ trans('client.dt_next_lbl') }}",
			previous:   "{{ trans('client.dt_prev_lbl') }}"
		},
		loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
		processing:     "{{ trans('client.dt_processing_lbl') }}",
	}
});




</script>