<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id=""> Correct invoice </h4>
</div>
<div class="modal-body">
	<div class="row" >
			<input type="hidden" id="billing-id" value="{{$billing->id}}" />
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#tab-quantity">Quantity Correction</a></li>
						  <li><a data-toggle="tab" href="#tab-price">Correction of Price</a></li>
						</ul>
						
						<div class="tab-content">
						  <div id="tab-quantity" class="tab-pane fade in active">
						    <div class="row">
						    	<div class="col-md-12">
						    		<h4>Before Correction</h4>
						    	</div>
						    	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
											<tr>
												<td>
													<strong>{{$det->bd_codigo}}</strong>
												</td>
												<td>
													@if($billing->bi_flag_prepay == 0)
														{{$det->bd_numero}}
														<input type="hidden" id="before-correction-llosc-{{$det->bd_tipo_partida}}" value="{{$det->bd_numero}}" />
													@else
														{{$prepayMonths}}
														<input type="hidden" id="before-correction-llosc-{{$det->bd_tipo_partida}}" value="{{$prepayMonths}}" />
													@endif
												</td>
												<td>
													
													@if($billing->bi_flag_prepay == 0)
														@if($det->bd_tipo_partida == 'BOX')
															@if(is_null($pd->pd_box_price))
																{{$det->bd_valor_neto}}																
															@else
																{{$pd->pd_box_price}}
															@endif
														@else
															@if(is_null($pd->pd_insurance_price))
																{{$det->bd_valor_neto}}																
															@else
																{{$pd->pd_insurance_price}}
															@endif
															
														@endif
													@else
														
														@if($det->bd_tipo_partida == 'BOX')
															{{$pd->pd_box_price_prepay / $prepayMonths}}
														@else
															@if($billing->bi_flag_prepay == 0){
																{{$pd->pd_insurance_price}}
															@else
																{{$pd->insurance}}
															@endif
														@endif
													@endif
												</td>
												<td>
													{{$det->bd_valor_neto}}
													<input type="hidden" id="before-wartosc-netto-{{$det->bd_tipo_partida}}" value="{{$det->bd_valor_neto}}" />
												</td>
												<td>
													{{$det->bd_total_vat}}
													<input type="hidden" id="before-kwota-vat-{{$det->bd_tipo_partida}}" value="{{$det->bd_total_vat}}" />
												</td>
												<td>
													{{$det->bd_total}}
													<input type="hidden" id="before-wartosc-brutto-{{$det->bd_tipo_partida}}" value="{{$det->bd_total}}"/>
												</td>
													
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<hr/>
								</div>
								
						      	<div class="col-md-12">
						      		<h4>Correction</h4>
						      	</div>
						      	
						      	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
										
											<tr>
												<td><strong>{{$det->bd_codigo}}</strong></td>
												<td>
													@if($billing->bi_flag_prepay == 0)
														<input type="number" value="-1" class="form-control quantity-correction" 
																style="background-color:yellow;" max="0" min="-1"
																id="quantity-correction-{{$det->bd_tipo_partida}}"
																data-tipo="{{$det->bd_tipo_partida}}"  />
													@else
														<input type="number" value="-{{$prepayMonths}}" class="form-control quantity-correction"
															style="background-color:yellow;" max="0" min="-{{$prepayMonths}}"
															id="quantity-correction-{{$det->bd_tipo_partida}}" 
															data-tipo="{{$det->bd_tipo_partida}}" />
													@endif
												</td>
												<td>
													<p style="display:none">
													<!--  SE OCULTA PARA QUE NO SE MUESTRE LA VARIABLE -->
													@if($billing->bi_flag_prepay == 0)
														@if($det->bd_tipo_partida == 'BOX')
															@if(is_null($pd->pd_box_price))
																{{ $cenna_netto = $det->bd_valor_neto }}
															@else
																{{ $cenna_netto = $pd->pd_box_price }}
															@endif
														@else
															@if(is_null($pd->pd_insurance_price))
																{{ $cenna_netto = $det->bd_valor_neto }}																
															@else
																{{ $cenna_netto = $pd->pd_insurance_price }}
															@endif
														@endif
													@else
														@if($det->bd_tipo_partida == 'BOX')
															{{ $cenna_netto = $pd->pd_box_price_prepay/$prepayMonths }}
														@else
															@if($billing->bi_flag_prepay == 0)
																{{ $cenna_netto = $pd->pd_insurance_price/$prepayMonths }}
															@else
																{{ $cenna_netto = $pd->insurance}}/$prepayMonths }}
															@endif
														@endif
													@endif
													
													<input type="hidden" id="vat-{{$det->bd_tipo_partida}}" value="{{$pd->pd_vat}}"/>
													</p>
													<input type="text" value="{{$cenna_netto}}" id="correction-cenna-netto-{{$det->bd_tipo_partida}}" class="form-control" readonly />
												</td>
												<td>
													<input type="text" value="{{$det->bd_valor_neto}}" id="correction-warstoc-netto-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
												<td><input type="text" value="{{$det->bd_total_vat}}" id="correction-kwota-vat-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
												<td><input type="text" value="{{$det->bd_total}}" id="correction-wartosc-brutto-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<hr/>
								</div>
								
								<div class="col-md-12">
						      		<h4>After Correction</h4>
						      	</div>
						      	
						      	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
											<tr>
												<td><h5>{{$det->bd_codigo}}</h5></td>
												<td>
													<h5 id="after-correction-llosc-{{$det->bd_tipo_partida}}"></h5>
												</td>
												<td>
													<p style="display:none">
														<!--  SE OCULTA PARA QUE NO SE MUESTRE LA VARIABLE -->
														@if($billing->bi_flag_prepay == 0)
															
															<!--  -->
															<!--  -->
															@if($det->bd_tipo_partida == 'BOX')
																@if(!is_null($pd->pd_box_price))
																	{{ $cenna_netto = $det->bd_valor_neto }}
																@else
																	{{ $cenna_netto = $pd->pd_box_price }}
																@endif
															@else
																@if(!is_null($pd->pd_insurance_price))
																	{{ $cenna_netto = $det->bd_valor_neto }}																
																@else
																	{{ $cenna_netto = $pd->pd_insurance_price }}
																@endif
															@endif
															
															<!--  -->
															<!--  -->
															
														@else
															@if($det->bd_tipo_partida == 'BOX')
																{{ $cenna_netto = $pd->pd_box_price_prepay/$prepayMonths }}
															@else
																@if($billing->bi_flag_prepay == 0)
																	{{ $cenna_netto = $pd->pd_insurance_price/$prepayMonths }}
																@else
																	{{ $cenna_netto = $pd->insurance}}/$prepayMonths }}
																@endif
															@endif
														@endif
													</p>
													<h5>{{$cenna_netto}}</h5>
												</td>
												<td>		
													<h5 id="after-correction-warstoc-netto-{{$det->bd_tipo_partida}}"></h5>
												</td>
												<td>
													<h5 id="after-correction-kwota-vat-{{$det->bd_tipo_partida}}"></h5>
												</td>
												<td>
													<h5 id="after-correction-wartosc-brutto-{{$det->bd_tipo_partida}}"></h5>
												</td>
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<label>Reason for correction</label>
									<input type="text" id="reason-correction-quantity" placeholder="Type here..." class="form-control"/>
								</div>
								
								<div class="col-md-4 center-block">
									<br/>
									<button class="btn btn-primary" id="btn-correct-invoice-quantity">
										Correct Invoice
									</button>
								</div>
						    </div>
						  </div>
						  
						  
						  <!-- *************************** -->
						  <!-- *************************** -->
						  <!-- ***CORRECCION DE PRECIO**** -->
						  <!-- *************************** -->
						  <!-- *************************** -->
						  
						  <div id="tab-price" class="tab-pane fade">
						     <div class="row">
						    	<div class="col-md-12">
						    		<h4>Before Correction</h4>
						    	</div>
						    	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
											<tr>
												<td>
													<strong>{{$det->bd_codigo}}</strong>
												</td>
												<td>
													@if($billing->bi_flag_prepay == 0)
														{{$det->bd_numero}}
														<input type="hidden" id="before-correction-price-llosc-{{$det->bd_tipo_partida}}" value="{{$det->bd_numero}}" />
													@else
														{{$prepayMonths}}
														<input type="hidden" id="before-correction-price-llosc-{{$det->bd_tipo_partida}}" value="{{$prepayMonths}}" />
													@endif
												</td>
												<td>
													@if($billing->bi_flag_prepay == 0)
													
														@if($det->bd_tipo_partida == 'BOX')
																														
															@if(!is_null($pd->pd_box_price))
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$det->bd_valor_neto}}" />
																{{$det->bd_valor_neto}}																
															@else
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->pd_box_price}}" />
																{{$pd->pd_box_price}}
															@endif
															
														@else
															
															
															@if(is_null($pd->pd_insurance_price))
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->insurance}}" />
																{{$pd->insurance}}																
															@else
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->pd_insurance_price}}" />
																{{$pd->pd_insurance_price}}
															@endif
															
															
														@endif
														
													@else
														@if($det->bd_tipo_partida == 'BOX')
															{{$pd->pd_box_price_prepay / $prepayMonths}}
															<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->pd_box_price_prepay / $prepayMonths}}" />
														@else
															@if($billing->bi_flag_prepay == 0){
																{{$pd->pd_insurance_price}}
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->pd_insurance_price}}" />
															@else
																{{$pd->insurance}}
																<input type="hidden" id="before-price-cenna-netto-{{$det->bd_tipo_partida}}" value="{{$pd->insurance}}" />
															@endif
														@endif
													@endif
												</td>
												<td>
													{{$det->bd_valor_neto}}
													<input type="hidden" id="before-price-wartosc-netto-{{$det->bd_tipo_partida}}" value="{{$det->bd_valor_neto}}" />
												</td>
												<td>
													{{$det->bd_total_vat}}
													<input type="hidden" id="before-price-kwota-vat-{{$det->bd_tipo_partida}}" value="{{$det->bd_total_vat}}" />
												</td>
												<td>
													{{$det->bd_total}}
													<input type="hidden" id="before-price-wartosc-brutto-{{$det->bd_tipo_partida}}" value="{{$det->bd_total}}"/>
												</td>
													
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<hr/>
								</div>
								
						      	<div class="col-md-12">
						      		<h4>Correction</h4>
						      	</div>
						      	
						      	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
											<tr>
												<td><strong>{{$det->bd_codigo}}</strong></td>
												<td>
													@if($billing->bi_flag_prepay == 0)
														<input type="number" value="{{$det->bd_numero}}" class="form-control"  
																id="quantity-correction-price-{{$det->bd_tipo_partida}}" readonly
																data-tipo="{{$det->bd_tipo_partida}}"  />
													@else
														<input type="number" value="{{$prepayMonths}}" class="form-control"
															id="quantity-correction-price-{{$det->bd_tipo_partida}}"  readonly
															data-tipo="{{$det->bd_tipo_partida}}" />
													@endif
												</td>
												<td>
													<p style="display:none">
													<!--  SE OCULTA PARA QUE NO SE MUESTRE LA VARIABLE -->
													@if($billing->bi_flag_prepay == 0)
														@if($det->bd_tipo_partida == 'BOX')
															
															@if(!is_null($pd->pd_box_price))
																{{ $cenna_netto = $det->bd_valor_neto }}
															@else
																{{ $cenna_netto = $pd->pd_box_price }}
															@endif

														@else
															
															@if(is_null($pd->pd_insurance_price))
																
																{{ $cenna_netto = $pd->insurance }}																
															@else
																
																{{ $cenna_netto = $pd->pd_insurance_price }}
															@endif
															
														@endif
														
														 
													@else
														@if($det->bd_tipo_partida == 'BOX')
															{{ $cenna_netto = $pd->pd_box_price_prepay/$prepayMonths }}
														@else
															@if($billing->bi_flag_prepay == 0)
																{{ $cenna_netto = $pd->pd_insurance_price/$prepayMonths }}
															@else
																{{ $cenna_netto = $pd->insurance}}/$prepayMonths }}
															@endif
														@endif
													@endif
													
													<input type="hidden" id="vat-price-{{$det->bd_tipo_partida}}" value="{{$pd->pd_vat}}"/>
													</p>
													<input type="number" value="-{{$cenna_netto}}" id="correction-price-cenna-netto-{{$det->bd_tipo_partida}}" class="form-control price-correction" 
														style="background-color:yellow;"  min="-{{$cenna_netto}}" max="0" data-tipo="{{$det->bd_tipo_partida}}" />
												</td>
												<td><input type="text" value="{{$det->bd_valor_neto}}" id="correction-price-warstoc-netto-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
												<td><input type="text" value="{{$det->bd_total_vat}}" id="correction-price-kwota-vat-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
												<td><input type="text" value="{{$det->bd_total}}" id="correction-price-wartosc-brutto-{{$det->bd_tipo_partida}}" class="form-control" readonly /></td>
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<hr/>
								</div>
								
								<div class="col-md-12">
						      		<h4>After Correction</h4>
						      	</div>
						      	
						      	<div class="col-md-12">
									<table class="table table-hover dataTable no-footer">
										<tr>
											<th></th>
											<th>Ilosc</th>
											<th>Cenna netto</th>
											<th>Warstoc netto</th>
											<th>Kwota Vat</th>
											<th>Wartosc brutto</th>
										</tr>
										@foreach($billing->billingDetail as $det)
											<tr>
												<td><h5>{{$det->bd_codigo}}</h5></td>
												<td>
													
													
													@if($billing->bi_flag_prepay == 0)
														<h5 id="after-correction-price-llosc-{{$det->bd_tipo_partida}}">{{$det->bd_numero}}</h5>
													@else
														<h5 id="after-correction-price-llosc-{{$det->bd_tipo_partida}}">{{$prepayMonths}}</h5>													
													@endif
													
													
												</td>
												<td>
													<p style="display:none">
														<!--  SE OCULTA PARA QUE NO SE MUESTRE LA VARIABLE -->
														@if($billing->bi_flag_prepay == 0)
															 
															@if(is_null($pd->pd_box_price))
																{{ $cenna_netto = $det->bd_valor_neto }}
															@else
																{{ $cenna_netto = $pd->pd_box_price }}
															@endif
															
														@else
															@if($det->bd_tipo_partida == 'BOX')
																{{ $cenna_netto = $pd->pd_box_price_prepay/$prepayMonths }}
																
															@else
																@if($billing->bi_flag_prepay == 0)
																	{{ $cenna_netto = $pd->pd_insurance_price/$prepayMonths }}
																@else
																	{{ $cenna_netto = $pd->insurance}}/$prepayMonths }}
																@endif
															@endif
														@endif
													</p>
													<h5 id="after-correction-price-cenna-netto-{{$det->bd_tipo_partida}}" ></h5>
												</td>
												<td>		
													<h5 id="after-correction-price-warstoc-netto-{{$det->bd_tipo_partida}}"></h5>
												</td>
												<td>
													<h5 id="after-correction-price-kwota-vat-{{$det->bd_tipo_partida}}"></h5>
												</td>
												<td>
													<h5 id="after-correction-price-wartosc-brutto-{{$det->bd_tipo_partida}}"></h5>
												</td>
											</tr>
										@endforeach
									</table>
								</div>
								
								<div class="col-md-12">
									<label>Reason for correction</label>
									<input type="text" id="reason-correction-price" placeholder="Type here..." class="form-control"/>
								</div>
								
								<div class="col-md-4 center-block">
									<br/>
									<button class="btn btn-primary" id="btn-correct-invoice-price">
										Correct Invoice
									</button>
								</div>
						    </div>
						    
						  </div>
						</div>
					</div> 
					
	      		</div>
      		</div>
    </div>
</div>


<script>
	

	$(".quantity-correction").on('keyup change', function (event){

		var tipo = $(this).data('tipo');
		var minimo = $(this).attr("min");
		var maximo = $(this).attr("max");
		var valor = $(this).val();

		if( Number(valor) < Number(minimo) || Number(valor) > Number(maximo) ){
			toastr['error']("Invalid data");
			return false;
		}
		
		var cenna_netto = $("#correction-cenna-netto-" + tipo).val();
		//var wartosc_netto = cenna_netto * valor ;
		var wartosc_netto = Number($("#correction-warstoc-netto-" + tipo).val()) * -1;
		var vat = $("#vat-" + tipo ).val();
		//var kwota_vat = vat * wartosc_netto;
		var kwota_vat = Number($("#correction-kwota-vat-" + tipo ).val()) * -1;

		//alert("vat: "+ vat );
		//alert("wartosc_netto: "+ wartosc_netto );
		
		//var wartosc_brutto = wartosc_netto + kwota_vat;
		var wartosc_brutto = Number($("#correction-wartosc-brutto-" + tipo ).val()) * -1;
		
		$("#correction-warstoc-netto-" + tipo).val(wartosc_netto.toFixed(2));
		$("#correction-kwota-vat-" + tipo ).val(kwota_vat.toFixed(2));
		$("#correction-wartosc-brutto-" + tipo ).val(wartosc_brutto.toFixed(2));

		//AFTER CORRECTION
		var llosc = $("#before-correction-llosc-" + tipo).val();
		var before_wartosc_netto = $("#before-wartosc-netto-" + tipo).val();
		var before_kwota_vat = $("#before-kwota-vat-" + tipo).val();
		var before_wartosc_brutto = $("#before-wartosc-brutto-" + tipo).val();
		
		var llosc_fixed = Number(llosc) + Number(valor);

		var warstoc_netto_fixed = Number(before_wartosc_netto) + Number(wartosc_netto);	
		var kwota_vat_fixed = Number(before_kwota_vat) + Number(kwota_vat);	
		var wartosc_brutto_fixed = Number(before_wartosc_brutto) + Number(wartosc_brutto);
		
		
		if(kwota_vat_fixed.toFixed(2) == "-0.00")
			kwota_vat_fixed = 0;

		if(wartosc_brutto_fixed.toFixed(2) == "-0.00")
			wartosc_brutto_fixed = 0;
		
		$("#after-correction-llosc-" + tipo).text(llosc_fixed);
		$("#after-correction-warstoc-netto-" + tipo).text(warstoc_netto_fixed.toFixed(2));
		$("#after-correction-kwota-vat-" + tipo).text(kwota_vat_fixed.toFixed(2));
		$("#after-correction-wartosc-brutto-" + tipo).text(wartosc_brutto_fixed.toFixed(2));		
	});

	$(".quantity-correction").trigger("change");

	$(".price-correction").on('keyup change', function (event){
		
		var tipo = $(this).data('tipo');
		var minimo = $(this).attr("min");
		var maximo = $(this).attr("max");
		var valor = $(this).val();
		/*
		if( Number(valor) < Number(minimo) || Number(valor) > Number(maximo) ){
			toastr['error']("Invalid data");
			$("#btn-correct-invoice-price").attr("disabled","disabled");
			return false;
		}*/

		$("#btn-correct-invoice-price").removeAttr("disabled");
		
		//var cenna_netto = $("#correction-price-cenna-netto-" + tipo).val();

		var cantidad = $("#quantity-correction-price-" + tipo ).val();
		var cenna_netto = valor;
		
		var wartosc_netto = cenna_netto * cantidad ;		
		var vat = $("#vat-price-" + tipo ).val();
		var kwota_vat = vat * wartosc_netto;
		var wartosc_brutto = wartosc_netto + kwota_vat;

		//alert(wartosc_netto);

		$("#correction-price-warstoc-netto-" + tipo).val(wartosc_netto.toFixed(2));
		$("#correction-price-kwota-vat-" + tipo ).val(kwota_vat.toFixed(2));
		$("#correction-price-wartosc-brutto-" + tipo ).val(wartosc_brutto.toFixed(2));

		//AFTER CORRECTION
		var llosc = $("#before-correction-price-llosc-" + tipo).val();
		var before_cenna_netto = $("#before-price-cenna-netto-" + tipo ).val();
		var before_wartosc_netto = $("#before-price-wartosc-netto-" + tipo).val();
		var before_kwota_vat = $("#before-price-kwota-vat-" + tipo).val();
		var before_wartosc_brutto = $("#before-price-wartosc-brutto-" + tipo).val();
		/*
		alert("llosc: " + llosc);
		alert("before_wartosc_netto: " + before_wartosc_netto);
		alert("before_kwota_vat: " + before_kwota_vat);
		alert("before_wartosc_brutto: " + before_wartosc_brutto);
		*/
		
		var llosc_fixed = llosc;
		
		var cenna_netto_fixed = Number(cenna_netto) + Number(before_cenna_netto);

		var warstoc_netto_fixed = Number(before_wartosc_netto) + Number(wartosc_netto);	
		var kwota_vat_fixed = Number(before_kwota_vat) + Number(kwota_vat);	
		var wartosc_brutto_fixed = Number(before_wartosc_brutto) + Number(wartosc_brutto);

		if(kwota_vat_fixed.toFixed(2) == "-0.00")
			kwota_vat_fixed = 0;

		if(wartosc_brutto_fixed.toFixed(2) == "-0.00")
			wartosc_brutto_fixed = 0;
		
		$("#after-correction-price-llosc-" + tipo).text(llosc_fixed);
		$("#after-correction-price-cenna-netto-" + tipo).text(cenna_netto_fixed.toFixed(2));
		$("#after-correction-price-warstoc-netto-" + tipo).text(warstoc_netto_fixed.toFixed(2));
		$("#after-correction-price-kwota-vat-" + tipo).text(kwota_vat_fixed.toFixed(2));
		$("#after-correction-price-wartosc-brutto-" + tipo).text(wartosc_brutto_fixed.toFixed(2));
	});

	$(".price-correction").trigger("change");
	
</script>

