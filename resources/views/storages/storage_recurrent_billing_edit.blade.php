<style>

</style>

<form action="{{route('storages.update_recurrent_billing')}}" method="post" id="frmUpdRecurrentBilling">
{{ csrf_field() }}
<input type="hidden" name="id" value="{{$storage->id}}" />

	<div class="modal-header">
		<h4 class="modal-title" id="">Edit recurrent billing </h4>
	</div>
	<div class="modal-body">
	      <div class="row" >

	      
			<div class="col-md-6 col-md-offset-3">
	      		<div class="form-group">
	      			<label>New Recurrent Billing Amount</label>
	      			<input type="number" name="price_per_month" class="form-control" autocomplete="off" min="0" />
	      		</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" id="btnSaveBilling" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script>
$('#frmUpdRecurrentBilling').validate({
    rules: {
    	price_per_month: {
            required: true
        },
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {

      	$("#frmUpdRecurrentBilling").find('button').prop('disabled',true);
      	
           $.ajax({
             	url: $("#frmUpdRecurrentBilling").attr('action'),
             	type: 'POST',
             	data: $(form).serialize(),
           })
           .done(function(data) {
             if (data.returnCode == 200) {
				toastr["success"](data.msg);
				$("#modal-generico-small").modal("hide");
				getListStoragesClient('{{$storage->user_id}}');
             }else{
               	toastr["error"](data.msg);
               	message = data.msg;
             }
           })
           .fail(function() {
             //console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             //console.log("complete");
           });


    return false; // required to block normal submit since you used ajax
   }
});

</script>