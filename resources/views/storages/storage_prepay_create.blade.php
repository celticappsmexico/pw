
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">{{ trans('storages.prepaid_months') }}</h4>
	</div>

	<div class="modal-body" >
			<form id="add-prepay-form" method="post" action="{{ route('storages.prepay.update') }}" role="from" enctype="multipart/form-data" >
				{{ csrf_field() }}
				<input type="hidden" name="storage_id" value="{{$storage_id}}" />
				
				<div class="row">
					<div class="col-md-12">
						<label>{{trans('storages.prepaid_months')}}</label>
						<input type="number" name="st_meses_prepago" min="1" class="form-control" />
					</div>

                    <div class="col-md-12">
						<label>Mark as Paid</label><br>
						<input type="checkbox" name="mark_paid" />
					</div>

					
					<div class="col-md-12">
						<br/>
						<button type="submit" id="btn-submit-prepay" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('client.send_lbl') }} </button>
					</div>
				</div>
			</form>
	</div>
	
<script>	
$('#add-prepay-form').validate({
    rules: {
        st_meses_prepago: {
            required: true,
            max: 61,
            min: 1,
            integer : true
        }
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
        $("#btn-submit-prepay").hide();
        
           $.ajax({
             url: $("#add-prepay-form").attr("action"),
             type: 'POST',
             data: $(form).serialize(),
           })
           .done(function(data) {
        	 showLoader();
        	 console.info('show loader');
             //console.info(data);
             if (data.success != false) {
              toastr["success"]("{{ trans('messages.prepay_saved') }}");
              $('#modal-generico-small').modal('hide');
              getListStoragesClient('{{$storage->user_id}}');
             }else{
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             }

             setTimeout(function(){
                 hideLoader();
               },1000);

           })
           .fail(function() {
             console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             console.log("complete");
           });
           
		

    return false; // required to block normal submit since you used ajax
   }
});


</script>