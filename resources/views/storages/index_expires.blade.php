<div class="modal-header">
	<h4 class="modal-title" id=""> List of Boxes </h4>
</div>
<div class="modal-body">
      <div class="row" >
		
		</div>
      	<div class="col-md-12">      		
			<table class="table table-hover" id="storages-table">
				<thead>
					<tr>
						<th>Tenant</th>
						<th>Box</th>
						<th>Date Start</th>
						<th>Date End</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
</div>
<script>
      		
var oTable = $('#storages-table').DataTable({
	"bInfo" : false,
	processing: true,
	serverSide: true,
	bFilter : true,
	"order": [[ 0, 'desc' ]],
	ajax: {
		url : "{!! route('storages_expires.index.data') !!}",
		data: function (d) {
			d.rentStart = '{{$rentStart}}';
			d.rentEnd  = '{{$rentEnd}}';
		}
	},
	columns: [
		{ data: 'tenant', name: 'tenant'},
		{ data: 'alias', name: 'alias'},
		{ data: 'rent_start', name: 'rent_start'},
		{ data: 'rent_end', name: 'rent_end'},
	],
	language : {
		lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
		zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
		info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
		infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
		infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
		search:         "{{ trans('client.dt_find_lbl') }}:",
		paginate: {
			first:      "{{ trans('client.dt_first_lbl') }}",
			last:       "{{ trans('client.dt_last_lbl') }}",
			next:       "{{ trans('client.dt_next_lbl') }}",
			previous:   "{{ trans('client.dt_prev_lbl') }}"
		},
		loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
		processing:     "{{ trans('client.dt_processing_lbl') }}",
	}
});




</script>