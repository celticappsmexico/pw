<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Przechowamy-Wszystko</title>
    	<link rel="shortcut icon" href="http://www.przechowamy-wszystko.pl/wp-content/themes/przechowamy-wszystko/images/fav.ico" type="image/x-icon" />

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/bootstrap.css?1422792965') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/materialadmin.css?1425466319') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/font-awesome.min.css?1422529194') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/material-design-iconic-font.min.css?1421434286') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/libs/morris/morris.core.css?1420463396') }}" />
	  	<link rel="stylesheet" href="{{ asset('assets/css/jquery.qtip.css') }}" media="screen" title="no title" charset="utf-8">
	  	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/toastr.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/reports.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/transformicons.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="{{ asset('assets/css/jquery.tree-multiselect.min.css') }}" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
		
		<!-- BOOTSTRAP TOGGLE-->
		<!--  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" /> -->
		
		 
		<!-- END BOOTSTRAP -->
		
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="{{ asset('assets/js/libs/utils/html5shiv.js?1403934957') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/libs/utils/respond.min.js?1403934956') }}"></script>
		<![endif]-->

        <!-- CSS Size Estimator -->
        <link rel="stylesheet" href="{{asset('assets/css/css-size-estimator.css')}}" media="screen" title="no title">
    <style media="screen">
      /*
      estilos que hay que mover a otra hoja de estilos
      */
      body{
        font-family: Arial;
        font-size: 10px
      }

      .text12{
        font-size: 12px;
      }
      .text10{
        font-size: 10px;
      }
      .text9{
        font-size: 9px;
        letter-spacing: -1px;
      }
      .text8{
        font-size: 8px;
        letter-spacing: -1px;
      }

      .navbar-brand img{
        height: 30px!important;
        top: -10px!important;
        margin-top: -5px;
      }

      .report-title{
      	margin-top:0;
      }
      
      	@if(env('ENVIRONMENT') == 'DEV')
      		#header{
	      		background:#008080 !important;
	      	}
	      	
	      	#menubar .menubar-scroll-panel{
	      		background:#B22222
	      	}
      	
		@endif      
    </style>

    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.qtip.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/snap.svg-min.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/toastr.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/datetimepicker.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

		<script type="text/javascript">

		  function showLoader(){
		    $('.pageLoader').show();
		    $('.superWrap').addClass('blurred');
		  }

		  function hideLoader(){
		    $('.pageLoader').hide();
		    $('.superWrap').removeClass('blurred');
		  }

		  function blurredContentOn(){
		    $('.superWrap').addClass('blurred');
		  }

		  function blurredContentOff(){
		    $('.superWrap').removeClass('blurred');
		  }

		  function getRandomColor() {
			  var letters = '0123456789ABCDEF';
			  var color = '#';
			  for (var i = 0; i < 6; i++ ) {
				  color += letters[Math.floor(Math.random() * 16)];
			  }
			  return color;
	  	}



			var token = $('meta[name="csrf-token"]').attr('content');
			var checkUsernameUrl = "{{ route('checkUsername') }}";
			var checkEmailUrl = "{{ route('checkEmail') }}";
			var deleteUserUrl = "{{ route('deleteUser') }}";
			var getDataClient = "{{ route('getDataUser') }}";
			var updateClient = "{{ route('updateUser') }}";
		</script>

    	<script>
			@include('partials.js.pw')
		</script>
		<script>
			@include('partials.js.validator_messages')
		</script>
		<script>
			@include('partials.js.sizeEstimator')
		</script>

	</head>
	<body class="menubar-hoverable header-fixed ">

		<audio id="soundNotif"><source src="{{asset('assets/sound/notif.mp3')}}" type="audio/mpeg"></audio>

		<div class="pageLoader">
			<div class="wraploader">
				<img src="{{ asset('assets/images/loader.gif') }}" alt="" />
			</div>
		</div>
	  	<div class="superWrap">
	    @include('partials.layout.navbar_pw')
	  	@include('partials.layout.errors')
			<div class="sticky-container">
			  <ul class="sticky">
			    <li><a href="{{ url('lang', ['en']) }}"><img src="{{ asset('assets/images/uk.png') }}" /> <p>En</p></a></li>
			    <li><a href="{{ url('lang', ['pl']) }}"><img src="{{ asset('assets/images/pl.png') }}" /> <p>Pl</p></a></li>
			  </ul>
			</div>

			<!-- BEGIN BASE-->
			<div id="base">

				<!-- BEGIN OFFCANVAS LEFT -->
				<div class="offcanvas">
				</div><!--end .offcanvas-->
				<!-- END OFFCANVAS LEFT -->

				<!-- BEGIN CONTENT-->
				<div id="content">
					<section>
						<div class="section-body">
	            @yield('content')
						</div>
					</section>
				</div><!--end #content-->
				<!-- END CONTENT -->

				@if (Auth::guest())

				@elseif (Auth::user()->Role_id == 1)
					@include('partials.layout.sidebar_pw')
				@elseif (Auth::user()->Role_id == 3)

				@endif
			</div><!--end #base-->
		</div>
		<!-- END BASE -->

		@include('partials.modals.payment_modal')
		@include('partials.modals.rent_storage')
		@include('partials.modals.extra_items_modal')
		@include('partials.modals.new_client')
		@include('partials.modals.delete_client')
    	@include('partials.modals.edit_client')
		@include('partials.modals.delete_building')
	    @include('partials.modals.new_building')
	    @include('partials.modals.edit_building')
	    @include('partials.modals.new_level')
	    @include('partials.modals.delete_level')
	    @include('partials.modals.edit_level')
	    @include('partials.modals.new_storage')
	    @include('partials.modals.delete_storage')
		@include('partials.modals.edit_storage')
		@include('partials.modals.new_order')
	    @include('partials.modals.extra_items_bk_modal')
		@include('partials.modals.assign_order_modal')
		@include('partials.modals.order_detail')
		@include('partials.modals.get_client_boxes')
		@include('partials.modals.add_storage_modal')
		@include('partials.modals.storage_invoices')

		<div class="modal fade" id="modal-generico" tabindex="-1" role="dialog"
				data-keyboard="false" data-backdrop="static"
					aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content" id="modal-generico-content">
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-generico-small" tabindex="-1" role="dialog"
				data-keyboard="false" data-backdrop="static"
					aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" id="modal-generico-small-content">
				</div>
			</div>
		</div>


		<!-- BEGIN JAVASCRIPT -->

		<script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/spin.js/spin.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/moment/moment.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.time.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.resize.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.orderBars.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/jquery.flot.pie.js') }}"></script>
		<script src="{{ asset('assets/js/libs/flot/curvedLines.js') }}"></script>
		<script src="{{ asset('assets/js/libs/jquery-knob/jquery.knob.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/sparkline/jquery.sparkline.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/d3/d3.min.js') }}"></script>
		<script src="{{ asset('assets/js/libs/d3/d3.v3.js') }}"></script>
		<script src="{{ asset('assets/js/libs/rickshaw/rickshaw.min.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/App.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppNavigation.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppOffcanvas.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppCard.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppForm.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppNavSearch.js') }}"></script>
		<script src="{{ asset('assets/js/core/source/AppVendor.js') }}"></script>
		<script src="{{ asset('assets/js/core/dashboard.js') }}"></script>
		<script src="{{ asset('assets/js/Chart.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
		
		<!-- bootstrap toggle -->
		<!-- 
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> 
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		 --> 
		<!--  -->
		
		<script>

		function getListStoragesClient(idClient){

			console.log(idClient);

		    $('.listStorages').empty();

		    cad = 'idClient='+idClient+'&_token='+token;

		    showLoader();
		    $('#modalClientBoxes').modal('show');
		    //$('#modalAddBox').modal('show');
		    //$('#modalClientBoxes .modal-body').append(token);
		    $('.modalAddBoxFrm1')[0].reset();
		    $('.modalAddBoxFrm2').hide();
		    $('.modalAddBoxFrm1').show();

		    $.ajax({
		      url: '{{ route('getUserStorages') }}',
		      type: 'POST',
		      data: cad,
		    })
		    .done(function(data) {
		      console.log("success");

		      if (data.success == true ) {
		      	//alert(data.saldo);
		      	//alert(data.saldo_formateado);

		      	if(Number(data.saldo) >= 0){
				 $('.listStorages').append(
						'<div class="alert alert-success" style="padding:0px">'+
	      					'<h2 class="text-right">{{Lang::get("client.balance")}}: '+data.saldo_formateado +'</h2>'+
		      				'</div>' );
				 }else{
					 $('.listStorages').append(
						'<div class="alert alert-danger" style="padding:0px">'+
							'<h2 class="text-right">{{Lang::get("client.balance")}}: '+data.saldo_formateado +'</h2>'+
		   				'</div>');
				 }

		        i = 0;
		        $.each(data.storages, function(index,val) {

		        var balance = val.balance + " pln";
		        
		        var meses_prepago = val.st_meses_prepago;

		        var linea_deposito = "";
		        var linea_regreso_deposito = "";
				var linea_meses_prepago = "";


				
		        if(val.deposito != null){
		        	linea_deposito = '<a class="text-lg text-medium"><b>{{Lang::get('client.deposit')}}: '+ val.deposito + " pln" +'</b></a>';

		        	if(val.deposito_pagado == 0){
						//DEPOSITO AUN NO PAGADO.. OPCION DE PAGAR
						
						var btnDepositStatus;
						 
						if(val.storage_payment_data[0].creditcard == 1){
							btnDepositStatus = 'disabled="disabled"';
						}else{
							btnDepositStatus = "";
						}
						
						
		        		linea_regreso_deposito =
    						'<button type="button" class="btn btn-warning btn-xs btnPagarDeposito" data-id="'+val.id_deposito+'" dir="'+val.user_id+'" '+btnDepositStatus+' > '+
    							
					          	'<i class="fa fa-money" ></i> {{ trans('cart.pay_deposit') }} '+
					         '</button>' ;
			        }
		        	else{
			        	if(val.deposito_regresado == 0){
			        		linea_regreso_deposito =
			        						'<button type="button" class="btn btn-warning btn-xs btnRegresarDeposito" data-id="'+val.id_deposito+'" dir="'+val.user_id+'"> '+
									          	'<i class="fa fa-undo" ></i> {{ trans('storages.return_deposit') }} '+
									         '</button>' ;
			        	}
			        	else{
			        		linea_regreso_deposito =
			        						'<button type="button" class="btn btn-warning btn-xs" disabled="disabled"> '+
									          	'<i class="fa fa-calendar"></i> {{ trans('storages.return_deposit_date') }} : '+ val.fecha_devolucion_deposito +
									         '</button>' ;
			        	}
		        	}
		        }
		        else{
		        	linea_regreso_deposito =
		        						'<button type="button" class="btn btn-warning btn-xs btnAddDeposito" data-id="'+val.id+'" dir="'+val.user_id+'"> '+
								          	'<i class="fa fa-plus"></i> {{ trans('storages.add_deposit') }} '+
								         '</button>' ;
		        }

		        
		        if(meses_prepago < 2 && val.storage_payment_data[0].pd_suscription_id == null){
			        linea_meses_prepago = '<button type="button" class="btn btn-primary btn-xs addPrepay" id="'+val.id+'" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
								          	'<i class="fa fa-asterisk" aria-hidden="true"></i> {{ trans('storages.add_prepaid_months') }} '+
									      '</button>';
			       }

			    var linea_braintree = "";
			    var card_braintree = val.storage_payment_data[0].pd_suscription_id;
			    var pd_suscription_canceled = val.storage_payment_data[0].pd_suscription_canceled;

				var btnDownloadContract = '<button style="display:none" type="button" class="btn btn-xs bg-red" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.storage_payment_data[0].id+'" >'+
		          						'<i class="fa fa-file-pdf-o"></i> Download Contract'+
			          				'</button>';

			    if(card_braintree == null || pd_suscription_canceled != 0){
				    
				    linea_braintree = '<button type="button" class="btn btn-xs btn-success btnAddBraintree" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.storage_payment_data[0].id+'" >'+
		          						'{{ trans('cart.suscribe_braintree') }}'+
			          				'</button>';
			    }
			    else{
			    	linea_braintree = '<button type="button" class="btn btn-xs btn-danger btnCancelBraintree" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.storage_payment_data[0].id+'" data-suscription="'+val.storage_payment_data[0].pd_suscription_id+'">'+
			    						'{{ trans('cart.cancel_braintree') }}'+
			          					'</button>';
			    }

			    var linea_credit_card	= '';
			    
			    if(val.storage_payment_data[0].creditcard == 1){
				    
				    linea_credit_card = '<button type="button" class="btn btn-xs btn-info btnChangeCC" data-cc="2" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.storage_payment_data[0].id+'" data-suscription="'+val.storage_payment_data[0].pd_suscription_id+'">'+
											'Credit Card: YES'+
						  					'</button>';
				   }
			    else{
			    	linea_credit_card = '<button type="button" class="btn btn-xs btn-danger btnChangeCC" data-cc="1" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.storage_payment_data[0].id+'" data-suscription="'+val.storage_payment_data[0].pd_suscription_id+'">'+
											'Credit Card: NO'+
  									'</button>';
				   }

			    var linea_suscription_braintree = "";
			    var suscription_id = val.storage_payment_data[0].pd_suscription_id;
			    var date_suscription = val.date_subscribe_braintree	;
			     
			    if(suscription_id == null){
			    	suscription_id = "--";
			    }

			    if(date_suscription == null || date_suscription == "0000-00-00"){
				    date_suscription = "--";
				}

			    linea_suscription_braintree = '<div class="col-md-4"><b>Suscription ID: </b>&nbsp;'+ suscription_id +' <br/> <b>Suscription Date:</b> '+ date_suscription +'</div>';
			    
		        $('.listStorages').append('<div class="col-xs-12 col-lg-12 hbox-xs">'+
		          '<div class="hbox-column width-2">'+
		          '<img class="img-circle img-responsive pull-left" src="{{ asset('assets/images/size-estimator/storage-box.png') }}" alt="">'+
		          '</div><div class="hbox-column v-top"><div class="clearfix"><div class="col-lg-12 margin-bottom-lg">'+
		          '<a class="text-lg text-medium">{{Lang::get('storages.box_number')}}: '+  val.alias+'</a><br/>'+
		          '<a class="text-lg text-medium"><b>{{Lang::get('client.balance')}}: '+ balance +'</b></a><br/>'+
				  '<a class="text-lg text-medium"><b>{{Lang::get('storages.prepaid_months')}}: '+ meses_prepago +'</b></a><br/>'+
				  
				  
		          linea_deposito +

		          '</div></div><div class="clearfix opacity-75"><div class="col-md-4"><span class="glyphicon glyphicon-calendar text-sm"></span>&nbsp;'+val.rent_start+' // '+val.rent_end+' </div><div class="col-md-4"><b>{{Lang::get('storages.recurrent_billing')}}</b><span class="glyphicon glyphicon-usd text-sm"></span>&nbsp;'+val.price_per_month+' </div>'+linea_suscription_braintree+'</div><div class="clearfix"><div class="col-lg-12"></div>'+
		          '<div class="col-lg-12">'+
			          '<button class="btn btn-danger btn-xs btnSendTermNotif" id="'+val.id+'" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
			          	'<i class="fa fa-mail-forward"></i> {{ trans('storages.send_term') }} ' +
			          '</button>'+

			          '<button type="button" class="btn btn-info btn-xs btnDownloadInvoice" id="'+val.id+'" dir="'+val.user_id+'"> '+
			          	'<i class="fa fa-cloud-download"></i> {{ trans('storages.down_invoice') }} '+
			          '</button>'+

			          '<button type="button" class="btn btn-danger btn-xs detachStorage" id="'+val.id+'" dir="'+val.user_id+'">'+
			          	'<i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('storages.detach') }} '+
			          '</button>'+

			          linea_regreso_deposito +
			          
			          linea_meses_prepago +

						
			          linea_braintree +
			          
					  '<br/><br/>'+

					  '<button class="btn btn-success btn-xs btnAutomaticDate '+val.id+'" id="'+val.id+'" style="display:none;" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
						  '<i class="fa fa-calendar"></i> {{ trans('storages.automatic_date') }} ' +
			          '</button>'+
					  '<button class="btn btn-danger btn-xs btnSetDate '+val.id+'" id="'+val.id+'" style="display:none;">'+
			          	'<i class="fa fa-calendar"></i> {{ trans('storages.set_date') }} ' +
			          '</button>'+

					    '<div class="input-group dateSelect '+val.id+'" style="width: 100px; border-style: solid; border-width:1px; display:none;">'+
					      '<span class="input-group-addon" style="border-right-style:solid; border-width:1px; border-right-color:black;">'+
					        '<i class="glyphicon glyphicon-calendar" style="margin-left:8px;"></i>'+
					      '</span>'+
					        '<input class="date-own form-control selectDate '+val.id+'" id="inptuAño" style="width: 80px; text-align: center;" type="text">'+
					    '</div>'+
						'<br/>'+
						'<button class="btn btn-success btn-xs btnSendTerm '+val.id+'" id="'+val.id+'" style="display:none;" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
	  					  '<i class="fa fa-mail-forward"></i> {{ trans('storages.send_term') }} ' +
	  		          	'</button>'+

	  		          linea_credit_card +

	  		        '<button class="btn btn-info btn-xs btnEditRecurrentBilling '+val.id+'" id="'+val.id+'" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
					  '<i class="fa fa-usd"></i> Edit Recurrent Billing ' +
		          	'</button>'+

					btnDownloadContract +

			          '<br/><br/>'+
			          '<div class="alert alert-warning" style="padding:5px"> {{Lang::get('storages.term_of_notice')}} '+
			          		val.storage_payment_data[0].payment_term[0].months +
			          ' &nbsp;{{Lang::get('billing_reports.months')}}</div>'+

					  
						
		          '</div></div><div class="stick-top-right small-padding"><i class="fa fa-dot-circle-o fa-fw text-success" data-toggle="tooltip" data-placement="left" data-original-title="Active"></i></div></div><!--end .hbox-column --></div><br/>'
					
		          );


		          i++;

		          //<i class="fa fa-money"></i>
		        });

		        $('.listStorages').append('<hr/>');

		        $.each(data.billingStorages, function(index,val) {

					var linea_regreso_deposito = "";
		        	
			        	if(val.deposito_pagado == 0){
			        		linea_regreso_deposito = "";
				        }
			        	else{
				        	if(val.deposito_regresado == 0){
				        		linea_regreso_deposito =
				        						'<button type="button" class="btn btn-warning btn-xs btnRegresarDeposito" data-id="'+val.id_deposito+'" dir="'+val.user_id+'"> '+
										          	'<i class="fa fa-undo" ></i> {{ trans('storages.return_deposit') }} '+
										         '</button>' ;
				        	}
				        	else{
				        		linea_regreso_deposito =
				        						'<button type="button" class="btn btn-warning btn-xs" disabled="disabled"> '+
										          	'<i class="fa fa-calendar"></i> {{ trans('storages.return_deposit_date') }} : '+ val.fecha_devolucion_deposito +
										         '</button>' ;
				        	}
			        	}

		        	var balance = val.balance + " pln";

		        	var linea_credit_card_billing	= "";

					console.log(val);
		        	
					if(val.creditCard == "1"){
						    
						linea_credit_card_billing = 
							'<button type="button" class="btn btn-xs btn-info btnChangeCC" data-cc="2" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.payment_data_id+'" >'+
								'Credit Card: YES'+
							'</button>';
						   }
					    else{
					    	linea_credit_card_billing = 
						    			'<button type="button" class="btn btn-xs btn-danger btnChangeCC" data-cc="1" data-id="'+val.id+'" data-user="'+val.user_id+'" data-paymentid="'+val.payment_data_id+'" >'+
											'Credit Card: NO'+
		  								'</button>';
						   }
		        	

		        	$('.listStorages').append('<div class="col-xs-12 col-lg-12 alert alert-warning" > '+
		        								'<div class="hbox-column width-2">'+
		        									'<img class="img-circle img-responsive pull-left" src="{{ asset('assets/images/size-estimator/storage-box.png') }}">'+
		        								'</div>'+

		        								'<div class="hbox-column v-top">'+
		        									'<div class="clearfix">'+
		        										'<div class="col-lg-12 margin-bottom-lg">'+
		        											'<a class="text-lg text-medium">{{Lang::get('storages.box_number')}}: '+  val.storages.alias +'</a><br/>'+
		        											'<a class="text-lg text-medium"><b>{{Lang::get('client.balance')}}: '+  balance+'</b></a>'+
		        											'</div>'+
		        									'</div>'+
		        									'<div class="clearfix opacity-75">'+
										          		'<div class="col-lg-6">'+
											          		'<button type="button" class="btn btn-info btn-xs btnDownloadInvoice" id="'+val.storage_id+'" dir="'+val.user_id+'"> '+
													          	'<i class="fa fa-cloud-download"></i> {{ trans('storages.down_invoice') }} '+
													        '</button>'+
													        linea_regreso_deposito +
													        linea_credit_card_billing + 
											          	'</div>'+
											          	'<div class="stick-top-right small-padding">'+
											          		'<i class="fa fa-dot-circle-o fa-fw text-success" data-toggle="tooltip" data-placement="left" data-original-title="Active"></i></div></div></div></div><br/>');
		        });

		      }else{
		        $('.listStorages').append('<h3>'+data.msj+'</h3>');
		      }

		    })
		    .fail(function() {
		      console.log("error");
		    })
		    .always(function() {
		      console.log("complete");
		    });


		    hideLoader();
		  };


		  $(document).on("click",".destroy-notif-user-rent", function(event){
			event.preventDefault();
			var route = $(this).attr("href");

			$.get(route, function(response, status){
				showLoader();
				if(response.returnCode == 200){			 
					toastr['success'](response.msg);

					var routeUsers = "{{ route('users',['*user*']) }}";
					routeUsers = routeUsers.replace('*user*',response.user_id);
					
					setTimeout(function(){
						hideLoader();
						location.href = routeUsers;
					},1500);
					
				}
			});
			

		  });
		</script>

		<!-- END JAVASCRIPT -->
					
	</body>
</html>
