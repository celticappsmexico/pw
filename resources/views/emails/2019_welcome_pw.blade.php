@extends('emails.layouts.layout_email_gral2')

@section('title')
    Gratulacje!
@stop


@section('content')
<br>
<hr/>
<br/>Właśnie zostałeś właścicielem swojego własnego boxu w Przechowamy Wszystko.
<br/>
<div style="text-align:justify !important">
Poniżej znajdziesz kilka istotnych informacji, które sprawią, że korzystanie z magazynu będzie jeszcze łatwiejsze.<br/>
Przechowamy Wszystko znajduje się na terenie Celtic Park Piaseczno przy ulicy Kineskopowej 1 w Piasecznie. Celtic znajduje się zaraz przy Fashion House – na rondzie nie skręcaj w prawo tylko pojedź prosto kolejne 500 m. Po prawej stronie zobaczysz baner z informacją, żeby skręcić w prawo za 100 metrów w celu dojechania do Celtic Park Piaseczno. To tutaj!<br/>
Po dojechaniu do szlabanu udaj się do budki ochrony i legitymując się dokumentem ze zdjęciem odbierz swoją spersonalizowaną kartę magnetyczną, która będzie tam na Ciebie czekać. Karta umożliwi Ci wstęp do hali, w której znajduje się Twój box. Ochroniarz wyjaśni Ci, jak dojechać do hali F.<br/>
Żeby wejśc do magazynu, musisz przyłożyć kartę do szarego czytnika, który znajduje się przy drzwiach. Światło zapala się po prawej stronie od wejścia. Pamiętaj, proszę, żeby je zgasić wychodząc.<br/>
Zobaczysz wózki paletowe, paleciaki oraz schodki, które są do Twojej dyspozycji. <br/>
Duża bramę otwiera się od wewnątrz – znajdź skrzynkę z trzema przyciskami.<br/>
Box zamykasz na właśną kłódkę, bo jesteś jedyną osobą, która ma dostęp do swoich rzeczy, pamiętaj więc, żeby ją ze sobą zabrać.<br/>
W godzinach pracy biura możesz kupić kłódkę, kartony, nożyki czy folię stretch. <br/>
<br/>
Hala, w której znajdują się magazynu posiada dwa wejścia i trzy piętra. Sprawdź w aplikacji, jaki jest numer Twojego boxu i zobacz, gdzie znajduje się Twoja komórka:
</div>
<br/><br/>
<table class="table-bordered">
	<tr>
		<th>Numery boxów</th>
		<th>piętro</th>
		<th>Wejście</th>
	</tr>
	<tr>
		<td>1-299</td>
		<td>Parter</td>
		<td>Wejście 1</td>
	</tr>
	<tr>
		<td>300-329</td>
		<td>Parter</td>
		<td>Wejście 1</td>
	</tr>
	<tr>
		<td>330-399</td>
		<td>Parter</td>
		<td>Wejście 2</td>
	</tr>
	<tr>
		<td>400-499</td>
		<td>Piętro I</td>
		<td>Wejście 1</td>
	</tr>
	<tr>
		<td>500-599</td>
		<td>Piętro II</td>
		<td>Wejście 1</td>
	</tr>
</table>
<hr/>


<br/>
Monitoring znajduje się zarówno na terenie całego magazynu jak i na zewnątrz obiektu.<br/>
Istotne dokumenty takie, jak:<br/>
<ul style="text-align:left !important">
<li>warunki umowy zaakceptowanej elektroniczne,</li>
<li>regulamin obiektu,</li>
<li>ogólne warunki ubezpieczenia,</li>
<li>wzór wypowiedznia umowy najmu</li>
</ul>
znajdziesz na naszej stronie internetowej w zakładce „dokumenty do pobrania”.<br/><br/>

Jeśli nadal masz pytania sprawdź, jakie odpowiedzi przygotowaliśmy dla Ciebie w strefie FAQ na naszej stronie internetowej.


@stop
