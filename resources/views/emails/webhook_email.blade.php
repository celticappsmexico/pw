@extends('emails.layouts.layout_email_gral')

@section('nombreDestinatario')
	{{$nombreDestinatario}}
@stop

@section('tituloCorreo')
	Webhook detectado
@stop


@section('content')
		<table width="100%" cellpadding="20" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#ffffff" class="contentblock">
				<h3>{{$request}}</h3>
				
			</td>
		</tr>
	</table>
@stop