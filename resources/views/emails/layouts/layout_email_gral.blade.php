<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>

body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

a:hover {
	text-decoration: underline !important;
}

td.promocell p {
	color: #e1d8c1;
	font-size: 16px;
	line-height: 26px;
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 0;
	padding-bottom: 14px;
	font-weight: normal;
}

td.contentblock h4 {
	color: #444444 !important;
	font-size: 16px;
	line-height: 24px;
	margin-top: 0;
	margin-bottom: 10px;
	padding-top: 0;
	padding-bottom: 0;
	font-weight: normal;
}

td.contentblock h4 a {
	color: #444444;
	text-decoration: none;
}

td.contentblock p {
	color: #888888;
	font-size: 13px;
	line-height: 19px;
	margin-top: 0;
	margin-bottom: 12px;
	padding-top: 0;
	padding-bottom: 0;
	font-weight: normal;
}

td.contentblock p a {
	color: #3ca7dd;
	text-decoration: none;
}
</style>
</head>
<body bgcolor="#e4e4e4" topmargin="0" leftmargin="0" marginheight="0"
	marginwidth="0"
	style="-webkit-font-smoothing: antialiased; width: 100% !important; background: #e4e4e4; -webkit-text-size-adjust: none;">

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="100%">
				<table width="600" cellpadding="0" cellspacing="0" border="0"
					align="center" class="table">
					<tr>
						<td width="600" class="cell">

							<!-- 
							<table width="600" cellpadding="0" cellspacing="0" border="0"
								class="table">
								<tr>
									<td align="right" width="350"
										style="color: #868686; font-size: 14px; text-shadow: 0 1px 0 #ffffff;"
										valign="top" bgcolor="#e4e4e4"><br>
									<br>
									<br>
									<br> <strong style="color:#313b45"><span>Witam: </span><span>@yield('nombreDestinatario')</span></strong>
									</td>
								</tr>
							</table>
							 -->
							<br/>
							
							<table width="100%" cellpadding="25" cellspacing="0" border="0"
								class="promotable">
								<tr>
									<td bgcolor="#313b45" width="600" class="promocell">
										<p>
											<strong>@yield('tituloCorreo')</strong>
										</p>
									</td>
								</tr>
							</table> <br>

							<table width="100%" cellpadding="25" cellspacing="0" border="0">
								<tr>
									<td width="100%" bgcolor="#ffffff" cellpadding="25"
										style="border-left: 2px solid #313b45">
										@yield('content')
										
									</td>
								</tr>
							</table> <br>

							<table width="100%" cellpadding="10" cellspacing="0" border="0"
								bgcolor="#f2f2f2">
								<tr>
									<td style="border-left: 2px solid #313b45"><br>
										<table width="600" cellpadding="0" cellspacing="0" border="0"
											align="center" class="table">
											<tr>
												<td width="600" nowrap bgcolor="#f2f2f2" class="cell">

													<table width="600" cellpadding="0" cellspacing="0"
														border="0" class="table">
														<tr>
															<td width="380" valign="top" class="footershow">

																<img src="http://pw.novacloud.link/assets/images/logo.png"/>

															</td>

														</tr>
													</table>
												</td>
											</tr>
										</table> <br></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
</body>
</html>
