@extends('emails.layouts.layout_email_gral')

@section('nombreDestinatario')
	{{$nombreDestinatario}}
@stop

@section('tituloCorreo')
	{{$tituloCorreo}}
@stop


@section('content')
		<table width="100%" cellpadding="20" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#ffffff" class="contentblock" style="text-align: justify !important">
				
				 				 
				<p>
					<h3>Drogi Najemco,</h3>
				</p>
				
				<p>
					W załączniku znajduje się @if($paragon == 1) paragon @else faktura @endif za wynajem pomieszczenia magazynowego. Jeśli masz ustawione płatności automatyczne, po prostu zachowaj ten dokument, a cała magia zrobi się sama. 
				</p>

				<p> 
					Jeśli jednak cały czas płacisz w tradycyjny sposób, czyli za pomocą przelewów, bądź kartą czy gotówką na miejscu, pragniemy poinformować Cię o wprowadzanych przez nas zmianach. w nawiązaniu do informacji, która od kilku miesięcy widnieje w mailach dotyczącej zmiany rodzaju płatności, informujemy, że <label style="text-decoration: underline;">ostatecznym terminem przejścia na system płatności automatycznych jest koniec czerwca 2018 roku.</label> 
				</p>

				<p> 
					Przypominając, płatności automatyczne (ang. direct debit or direct withdrawal) polegają na automatycznie ściągnych płatnościach każdomiesięcznie (bądź za inny wybrany okres, np. kwartalny) z karty płatniczej klienta. Karta może być zarówno debetowa, jak i kredytowa.
				</p>

				<p>
					Najemca podaje nam numer swojej karty, który my wprowadzamy do systemu i dzięki temu nigdy więcej nie musi pamiętać o płatnościach. Paragony są, jak do tej pory, wysyłane na adres mailowy. Opłata pobierana jest 6-ego każdego miesiąca. Nie musisz nic ustawiać w banku, to my wysyłamy do niego informację o włączeniu nowego sposobu płatności. 
				</p>
				
				<p>
					Zapewniamy wszystkie kwestie bezpieczeństwa, które muszą, ale też mogą być spełnione. Nie jesteśmy w posiadaniu Twoich danych, korzystamy z zewnętrznej platformy płatniczej, która przechowuje je w bezpieczny sposób. Po tym, jak wpiszemy Twój numer karty do systemu, nigdy więcej nie możemy go zobaczyć. 
				</p>
				
				<p>
					<b>
					Nie wysyłaj numeru karty elektronicznie – to nie jest bezpieczny sposób!
					Skontaktuj się w nami telefonicznie bądź wpadnij na kawę, w celu ustawienia nowego sposobu płatności.
					</b>
				</p>
				
				<p>
					Ciepło pozdrawiamy,
					zespół Przechowamy Wszystko.
				</p>
				
				<p> 
					Wiadomość została wygenerowana automatycznie, prosimy na nią nie odpowiadać. W razie potrzeby prosimy o kontakt pod adresem mailowym <a href="mailto:biuro@przechowamy-wszystko.pl">biuro@przechowamy-wszystko.pl</a> 
				</p>	
			</td>
		</tr>
	</table>
@stop
