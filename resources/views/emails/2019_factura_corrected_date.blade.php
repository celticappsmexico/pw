@extends('emails.layouts.layout_email_gral2')

@section('title')
    
@stop


@section('content')
<style>
    p{
        text-align:justify !important;
    }

    .btn-confirm{

        color: #ffffff;
        background-color: #DA1212;
        border-color: #DA1212;
        font-weight: bold;
        text-align: center;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 4.5px 14px;
        font-size: 15px;
        line-height: 1.846153846;
        border-radius: 2px;
        margin:0 auto;
        margin-left:40%;
        text-decoration:none;
    }

</style>
<div style="text-align:justify !important">
        <p>
            <h3>Szanowni Państwo,</h3>
        </p>

        <p>
            W załączeniu przesyłamy fakturę korygująca do wystawionego w bieżącym miesiącu dokumentu. 
        </p>

        <p>
            Treść korygowana dotyczy daty i miejsca wystawienia dokumentu. 
        </p>

        <p>
            Prosimy o potwierdzenie otrzymania wiadomości na adres e-mail: biuro@przechowamy-wszystko.pl
        </p>

        <p>
            Z poważaniem, <br>
            Zespół Przechowamy Wszystko 
        </p>
       
       <p>
            @if(env('ENVIRONMENT') == "DEV")
                <a href="http://pw.des:8080/confirm_corrected_invoice/{{$id_codificado}}" class="btn-confirm">
                    Kliknij aby potwierdzić
                </a>
            @else
                <a href="http://app.przechowamy-wszystko.pl/confirm_corrected_invoice/{{$id_codificado}}" class="btn-confirm">
                    Kliknij aby potwierdzić
                </a>
            @endif
        </p>
	</div>
@endsection