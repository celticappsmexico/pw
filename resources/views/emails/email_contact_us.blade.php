@extends('emails.layouts.layout_email_gral')

@section('nombreDestinatario')
	Contacto PW
@stop

@section('tituloCorreo')
	{{$tituloCorreo}}
@stop


@section('content')
		<table width="100%" cellpadding="20" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#ffffff" class="contentblock">
				
				
				<p>  {!! $mensaje !!} </p>


			</td>
		</tr>
	</table>
@stop
