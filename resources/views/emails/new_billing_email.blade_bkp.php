@extends('emails.layouts.layout_email_gral')

@section('nombreDestinatario')
	{{$nombreDestinatario}}
@stop

@section('tituloCorreo')
	{{$tituloCorreo}}
@stop


@section('content')
		<table width="100%" cellpadding="20" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#ffffff" class="contentblock">
				
				<!-- 
				<p> 
				Wysyłamy fakturę za okres {{$period}}, dołącz dokument pdf do faktury
				</p>
				 -->
				<p>
					<h3>Szanowni Państwo,</h3>
				</p>
				
				<p>
					w załączniku znajduje się faktura za wynajem pomieszczenia magazynowego we {{$period}}.  
					Uprzejmie prosimy o wpisanie numeru faktury w tytule przelewu oraz przelanie dokładnej kwoty znajdującej się na dokumencie. W innym wypadku Państwa płatność może nie zostać poprawnie zaksięgowana. 
				</p>

				<p>
					Wiadomość została wygenerowana automatycznie, prosimy na nią nie odpowiadać. W razie potrzeby prosimy o kontakt pod adresem mailowym <a href="mailto:biuro@przechowamy-wszystko.pl">biuro@przechowamy-wszystko.pl</a>
				</p>

				<p>
					Z góry dziękujemy za terminową płatność.
				</p>
				<p> 
					Ciepło pozdrawiamy,
				</p>
				<p>
					zespół Przechowamy Wszystko.
				</p> 
							
			</td>
		</tr>
	</table>
@stop