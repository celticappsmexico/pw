<?php
  // obtenemos la url para las imagenes
  $path = env('IMAGES_MAIL_PATH');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PW Registration E-Mail</title>
<link href="styles.css" media="all" rel="stylesheet" type="text/css" />
</head>

<body itemscope itemtype="http://schema.org/EmailMessage">

<table style="margin:0px;padding: 0px; background: #333333;border: 0px; font-family:'Droid Sans', sans-serif;" width="100%" border="0">
  <tr>
    <td align="center">
      <table width="600px" style="margin:20px 0px;padding: 0px; border-collapse: collapse;background: #FFF;overflow: hidden" border="0">
        <tr>
          <td style="background-image: url(<?php echo $path; ?>mailtemplate_r2_c2.png); height: 45px!important;width:600px;margin:0px;padding:0px;color:#FFF;font-size:9px;padding: 0px 0px 0px 15px; line-height: 10px;">
            <table>
              <tr>
                <td style="width:18px; height: 18px;"><img src="<?php echo $path; ?>mailtemplate_calendar_icon.png"></td>
                <td style="color:#FFF;font-size:9px;">{{ $fecha }}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table style="margin: 20px 20px;">
              <tr>
                <td style="text-align: left"><img src="<?php echo $path; ?>mailtemplate_r4_c3.png"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table style="margin:0px 15px;">
              <tr>
                <td style="background:#FFF;padding:12px">
                  <img src="<?php echo $path; ?>mailtemplate_r6_c4.png">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table style="margin:20px;background:#FFF;padding:12px">
              <tr>
                <td style="width:146px;height: 161px;">
                  <img src="<?php echo $path; ?>mailtemplate_r8_c4.png">
                </td>
                <td style="color: #333333; font-size: 16px;">
                   Estimado <b>Mauricio</b>:<br><br>
                   Se han generado <b>15</b> solicitudes nuevas.
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr style="">
          <td style="text-align: center;"><img src="<?php echo $path; ?>mailtemplate_r10_c6.png"></td>
        </tr>
        <tr>
          <td style="background-image: url(<?php echo $path; ?>mailtemplate_r12_c2.png);height: 35px; width: 600px;text-align:center;color:#FFF;font-size:9px;">POWERED BY: NOVACLOUD</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html>
