@extends('emails.layouts.layout_email_gral2')

@section('title')
    Welcome to APP PW
@stop


@section('content')
<style>
p{
    text-align:justify !important;
}
</style>

<hr>
  	
<b>Szanowni Państwo,</b> 

<p>jesteście Państwo wyjątkowymi odbiorcami, którzy jako pierwsi na świecie mają możliwość korzystać z tak zaawansowanego systemu ułatwiającego korzystanie z magazynu samoobsługowego. Pragniemy poinformować, że w celu ulepszenia jakości usług w Przechowamy Wszystko stworzyliśmy aplikację umożliwiająca kontrolę swoich boxów, szybkie i łatwe wynajmowanie kolejnych powierzchni bądź rezygnowanie z obecnych oraz nadzorowanie płatności. Dzięki zainstalowaniu aplikacji będziecie również na bieżąco ze wszystkimi informacjami takimi jak zmiany, czasowy brak dostępu do magazynu czy promocje.</p>
<p>W celu rozpoczęcia korzystania z aplikacji konieczne jest znalezienie jej w iTunes App Store bądź Play Store pod niezaskakującą nazwą „przechowamy wszystko” i zainstalowanie jej na swoim urządzeniu.</p> 
<p>Poniżej znajdziecie Państwo login i hasło niezbędne do zalogowania się:</p>

<strong>{{ $user }}</strong><br/>
<strong>{{ $pass }}</strong>
<br/>
<p>Mamy nadzieję, że aplikacja nie tylko spełni Państwa oczekiwania, ale i zaskoczy swoimi możliwościami oraz nowatorskością. 
Jeśli będzie to potrzebne, chętnie pomożemy w jej instalacji oraz zalogowaniu się.
</p>
<br/>
Pozdrawiamy ciepło,
<br/>
zespół Przechowamy Wszystko.
<br/>
<table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">
    <tr align="center">
        <th colspan="2">Get it on:</th>
    </tr>
    <tr>
        <td bgcolor="" align="center" style="border-radius:4px;" width="200" height="40">
            <div class="contentEditableContainer contentTextEditable">
                <div class="contentEditable" align='center' >
                    <a href="https://play.google.com/store/apps/details?id=mx.novacloud.pwstorage" 
                        target="_blank"
                        style="cursor: pointer;">
                        <img src="http://pw.novacloud.link/assets/images/playstore2.png" style="width: 200px; height: 80px;"></a>
                    <br/>
                </div>
            </div>
        </td>
        <td bgcolor="" align="center" style="border-radius:4px;" width="200" height="40">
            <div class="contentEditableContainer contentTextEditable">
                <div class="contentEditable" align='center' >
                    <a href="https://itunes.apple.com/us/app/pw-self-storage/id1223022421?l=es&ls=1&mt=8" 
                    target="_blank"
                    style="cursor: pointer;"><img 
                        src="http://pw.novacloud.link/assets/images/appstore2.png" style="width: 205px; height: 80px;"></a>
                </div>
            </div>
        </td>
    </tr>
</table>
@endsection