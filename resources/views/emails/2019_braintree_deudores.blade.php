@extends('emails.layouts.layout_email_gral2')

@section('title')
    Braintree Report {{\Carbon\Carbon::now()->format('Y-m-d')}}
@stop


@section('content')
<style>
    p{
        text-align:justify !important;
    }
    table.report {
        border:1px solid black;
        width:100%;
    }
    table.report th {
        font-size:11px;
        background-color:#DA1212;
        color:white;
    }
    table.report td{
        font-size:10px;
        padding:5px;
    }
    table.report tr{
        border:1px dashed #ccc;
    }
    h4{
        color:black;
        font-size:13px;
    }
</style>
<div style="text-align:justify !important">
    <br>
    <h4>Tenants without successfull payment for April - Braintree Subscriptions</h4>
    <br>
    <table class="report">
        <tr>
            <th>Tenant</th>
            <th>Storage</th>
            <th>Amount</th>
        </tr>
        
        @foreach($storages as $row)  
            <tr>
                <td>
                    {{$row->StorageOwner->name}} {{$row->StorageOwner->lastName}} - {{$row->StorageOwner->companyName}}
                </td>

                <td>
                    {{$row->alias}}
                </td>

                <td>
                    {{App\Library\Formato::formatear("ZLOTY",$row->price_per_month)}}
                </td>
            </tr>
        @endforeach
    </table>
    
</div>
@endsection