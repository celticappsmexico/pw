@extends('emails.layouts.layout_email_gral2')

@section('title')
    Faktura
@stop


@section('content')
<style>
    p{
        text-align:justify !important;
    }

</style>
<div style="text-align:justify !important">
        <p>
            <h3>Drogi Najemco,</h3>
        </p>
        
        <p>
            Uwaga!
        </p>

        <p> 
            Jako pierwsi na świecie wprowadzamy możliwość kontrolowania swojego boxu dzięki specjalnie w tym celu stworzonej <b>aplikacji</b>. <br> 
            Zachęcamy do ściągnięcia <b>PW self storage</b>, która sprawnie działa na systemach Apple oraz Android. Wiadomość z loginem oraz hasłem została wysłana na ten sam adres mailowy, na który dotarła ta wiadomość.  <br>
            Aplikacja jest wyjątkowo intuicyjna i bezproblematyczna w użyciu.
        </p>

        <p> 
            Z poziomu aplikacji można:            
        </p>

        <p>
            <ul>
                <li>
                    Zaktualizować swoje dane, takie jak adres, płatności czy @ do wysyłki faktur,
                </li>
                <li>
                    Zmienić rozmiar wynajmowanego boxu,
                </li>
                <li>
                    Wynająć kolejny magazyn,
                </li>
                <li>
                    Złożyć wypowiedzenie,
                </li>
                <li>
                    Pobrać wszystkie dotychczas wystawione faktury,
                </li>
                <li>
                    Być na bieżąco ze sprawami związanymi z magazynem (prace remontowe, brak dostępu do magazynu etc).
                </li>
            </ul>            
        </p>
        
        <p>
            Zachęcamy do ściągnięcia aplikacji i poczucia się administratorami swojej przestrzeni siedząc w ulubionych kapciach na wygodnej, domowej kanapie (mamy nadzieję, że akurat tej nie zostawiliście w boxie).
        </p>
        
        <p>
            Z góry dziękujemy za terminową płatność.<br/>
            Ciepło pozdrawiamy, zespół Przechowamy Wszystko.
        </p>
        
        <p>
            Wiadomość została wygenerowana automatycznie, prosimy na nią nie odpowiadać. W razie potrzeby prosimy o kontakt pod adresem mailowym <a href="mailto:biuro@przechowamy-wszystko.pl">biuro@przechowamy-wszystko.pl</a>
        </p>
	</div>
@endsection