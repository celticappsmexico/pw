@extends('emails.layouts.layout_email_gral')

@section('nombreDestinatario')
	{{$nombreDestinatario}}
@stop

@section('tituloCorreo')
	{{$tituloCorreo}}
@stop


@section('content')
		<table width="100%" cellpadding="20" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#ffffff" class="contentblock">
				
				 				 
				<p>
					<h3>Szanowni Państwo,</h3>
				</p>
				
		

				<p>  {!! $mensaje !!} </p>



				

				<p>
					Ciepło pozdrawiamy,<br/> 
					zespół Przechowamy Wszystko. 
				</p>
				
				<p>
					Wiadomość została wygenerowana automatycznie, prosimy na nią nie odpowiadać. W razie potrzeby prosimy o kontakt pod adresem mailowym <a href="mailto:biuro@przechowamy-wszystko.pl">biuro@przechowamy-wszystko.pl</a> 
				</p>
							
			</td>
		</tr>
	</table>
@stop
