@extends('emails.layouts.layout_email_gral2')

@section('title')
    Dzień dobry ,
@stop


@section('content')
<br>
<h4 style="text-align:center">PROŚBA O RESET HASŁA DLA PRZECHOWAMY WSZYSTKO</h4><hr/><br/>
<span style="color:#697b7c"><span style="font-size:14px; line-height:14px; text-align:justify">
Poprosiłeś o zresetowanie Twoich szczegółów logowania w <strong>Przechowamy Wszystko.</strong>
<br/>
Zauważ proszę, że to zmieni Twoje obecne hasło.
<br/>
Aby potwierdzić tą czynność, proszę użyć następującego linku: 	
</span></span></span></div>
<br/>
<a href="{{ url('password/reset/'.$token) }}" target="_blank">{{ url('password/reset/'.$token) }}</a>

<br/><br/><hr/><br/>
Możesz obejrzeć swoje wynajęte boxy i pobrać fakturę oraz zmienić wielkość boxu, wynająć kolejne pomieszczenie bądź złożyć wypowiedzenie aktualnego magazynu. 
<br/><br/><hr/>
<strong>Pozdrawiamy,</strong><br/>
<strong>Zespół Przechowamy Wszystko</strong>

<br/><br/>
Pomoc<br/>
W przypadku pytań, prosimy o kontakt <br/>
+48 668 11 66 22<br/>
biuro@przechowamy-wszystko.pl<br/>
ul. Kineskopowa 1<br/>
05-500 Piaseczno<br/>
pon-pt: 09:00 - 16:00<br/>


                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table>
@stop
