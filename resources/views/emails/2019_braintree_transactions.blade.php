@extends('emails.layouts.layout_email_gral2')

@section('title')
    Braintree Report {{\Carbon\Carbon::now()->format('Y-m-d')}}
@stop


@section('content')
<style>
    p{
        text-align:justify !important;
    }
    table.report {
        border:1px solid black;
        width:100%;
    }
    table.report th {
        font-size:11px;
        background-color:#DA1212;
        color:white;
    }
    table.report td{
        font-size:10px;
        padding:10px;
    }
    table.report tr{
        border:1px dashed #ccc;
    }
    h4{
        color:black;
        font-size:13px;
    }
</style>
<div style="text-align:justify !important">
    <br>
    <h4>Unsuccessfull Transactions</h4>
    <br>
    <table class="report">
        <tr>
            <th>Tenant</th>
            <th>Storage</th>
            <th>Subscription ID</th>
            <th>Transaction ID</th>
            <th>Error</th>
            <th>Amount</th>
            <th>Date</th>
        </tr>
        
        @foreach($erroneas as $row)  
            <tr>
                <td>
                    {{$row->user->name}} {{$row->user->lastName}} - {{$row->user->companyName}}
                </td>

                <td>
                    {{$row->storage->alias}}
                </td>

                <td>
                    {{$row->bl_suscription_id}}
                </td>

                <td>
                    {{$row->bl_transaction_id}}
                </td>

                <td>
                    {{$row->bl_msg}}
                </td>

                <td>
                    {{App\Library\Formato::formatear("ZLOTY",$row->bl_amount)}}
                </td>

                <td>
                    {{\Carbon\Carbon::parse($row->created_at)->format('d-m')}}
                </td>
            </tr>
        @endforeach
    </table>
    
    <br>
    <h4>Successfull Transactions</h4>
    <br>
    <table class="report">
        <tr>
            <th>Tenant</th>
            <th>Storage</th>
            <th>Subscription ID</th>
            <th>Transaction ID</th>
            <th>Amount</th>
            <th>Date</th>
        </tr>
        
        @foreach($exitosas as $row)  
            <tr>
                <td>
                    {{$row->user->name}} {{$row->user->lastName}} - {{$row->user->companyName}}
                </td>

                <td>
                    {{$row->storage->alias}}
                </td>

                <td>
                    {{$row->bl_suscription_id}}
                </td>

                <td>
                    {{$row->bl_transaction_id}}
                </td>

                <td>
                    {{App\Library\Formato::formatear("ZLOTY",$row->bl_amount)}}
                </td>

                <td>
                    {{\Carbon\Carbon::parse($row->created_at)->format('d-m')}}
                </td>
            </tr>
        @endforeach
    </table>
    
</div>
@endsection