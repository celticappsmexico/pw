<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id=""> Storages Tenant </h4>
</div>
<div class="modal-body">
	<div class="row" >
			<div class="col-md-12">
				<div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover" id="app-users-storages-table">
                            <thead>
                                <tr>
                                    <th>Alias</th>
                                    <th>Start Date</th>
                                    <th>Finish Date</th>
                                    <th>Prepayment</th>
                                    <th>User Transaction</th>
                                    <th>Credit Card</th>
                                    <th>Rent Method</th>
                                    <th>Rent Date</th>
                                    <th>Balance</th>
                                    <th>Braintree Suscription</th>
                                </tr>
                            </thead>
                        </table>
                            
                    </div>
	      		</div>
      		</div>
    </div>
</div>


<script>
	var oTableUsersStorages = $('#app-users-storages-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 0, "asc" ]] ,
		ajax: {
			url : "{!! route('app_users.storages.index.data') !!}",
			data: function (d) {
                d.user_id = '{{$id}}';
			}
		},
		columns: [
            { data: 'alias', name: 'alias'},
			{ data: 'rent_start', name: 'rent_start'},
			{ data: 'finish_date', name: 'finish_date'},
			{ data: 'prepayment', name: 'prepayment'},
			{ data: 'user_transaction', name: 'user_transaction'},
            { data: 'credit_card', name: 'credit_card'},
            { data: 'rent_method', name: 'rent_method'},
            { data: 'rent_date', name: 'rent_date'},
            { data: 'balance', name: 'balance'},
            { data: 'braintree_suscription', name: 'braintree_suscription'},
			
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});
</script>

