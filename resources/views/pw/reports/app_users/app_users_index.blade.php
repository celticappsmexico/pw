@extends('app')
@section('content')
  <div class="row">
  	<div class="col-md-4 col-md-offset-4">
  		<h2>App Users</h2>
  	</div>
  	
	<div class="col-md-12">
		<table class="table table-hover" id="app-users-table">
			<thead>
				<tr>
                    <th>ID</th>
					<th>{{Lang::get('client.accountant_number')}}</th>
                    <th>Email</th>
					<th>{{Lang::get('cart.client')}}</th>
					<th>Platform</th>
					<th>Registration Date</th>
                    <th>Current Storages</th>
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTableUsers = $('#app-users-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 0, "asc" ]] ,
		ajax: {
			url : "{!! route('app_users.index.data') !!}",
			data: function (d) {
			}
		},
		columns: [
            { data: 'id', name: 'id'},
            { data: 'accountant_number', name: 'accountant_number'},
            { data: 'email', name: 'email'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'platform', name: 'platform'},
			{ data: 'registration_date', name: 'registration_date'},
            { data: 'storages_rented', name: 'storages_rented'},
			
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

    $(document).on("click", ".btn-storages-user", function(event){
        event.preventDefault();

        $("#modal-generico").modal("show");
		$("#modal-generico-content").load( $(this).attr('href') );		 	
    });

  	
  </script>
@endsection
	
	