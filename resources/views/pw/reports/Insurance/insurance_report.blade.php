@extends('app')
@section('content')
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-1">
            <div class="input-group año" style="width: 100px; border-style: solid; border-width:1px;">
                <span class="input-group-addon" style="border-right-style:solid; border-width:1px; border-right-color:black;">
                <i class="glyphicon glyphicon-calendar" style="margin-left:8px;"></i>
                </span>
                <input class="date-own form-control" style="width: 80px; text-align: center;" type="text" id="inputDate" name="inputYear" value="DATE">
            </div>
        </div>
        <div class="col-md-11">
            <button type="button" id="btnFiltrar" class="btn btn-secondary" style="margin-left:20px; border-style:solid; border-width:1px; border-color:black; font-size:15.5px;">
                {{ trans('billing_reports.search') }}
            </button>
        </div>
    </div>
    <div class="row">
        <table class="table table-hover no-footer dataTable" id="tableInsuranceReport">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Company Name</th>
                    <th>Box Number</th>
                    <th>Date</th>
                    <th># Fact</th>
                    <th>Subtotal</th>
                    <th>Total</th>
                    <!--<th>{{ trans('history_occupancy_rate.warehouse') }}</th>-->
                </tr>
            </thead>
            @foreach($insurances as $insurance)
                @if(count($insurance->billing) > 0)
                    <tr>
                        <td>
                            {{ $insurance->billing->user->name }}
                        </td>
                        <td>
                            {{ $insurance->billing->user->lastName }}
                        </td>
                        <td>
                            {{ $insurance->billing->user->companyName }}
                        </td>
                        <td>
                            {{ $insurance->billing->storages->alias }}
                        </td>
                        <td>
                            {{ $insurance->billing->pay_month }}
                        </td>
                        <td>
                            {{ $insurance->bd_numero }}
                        </td>
                        <td>
                            {{ $insurance->bd_valor_neto }}
                        </td>
                        <td>
                            {{ $insurance->bd_total }}
                        </td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableInsuranceReport').DataTable( {
                "pagingType": "full_numbers",
                "order": [ 0, 'desc' ]
            } );
        } );

        $(function() {
            $('#inputDate').datepicker({
                minViewMode: 3,
                format: 'yyyy/mm/dd',
            });
        });

        $( "#btnFiltrar" ).click(function() {
            var date = $('#inputDate').val();
            var fecha = date.split("/");
            if (date != "DATE") {
                route = "{{ route('insuranceReportFilter', ['*anio*', '*mes*', '*dia*']) }}";

                route = route.replace('*anio*', fecha[0]);
                route = route.replace('*mes*', fecha[1]);
                route = route.replace('*dia*', fecha[2]);

                window.location.href = route;
            }else {
                console.log("Selecciona una fecha");
            }
        });
    </script>
@endsection