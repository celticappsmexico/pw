@extends('app')
@section('content')
  <div class="row">
  	<div class="col-md-4 col-md-offset-4">
  		<h2>{{Lang::get('reports.bad_payers')}}</h2>
  	</div>
  	<div class="col-md-4 col-md-offset-8">
  		<button class="btn btn-success pull-right" id="descargaCSV">
  			{{Lang::get('reports.export_csv')}}
  		</button>
  	</div>
	<div class="col-md-12">
		<table class="table table-hover" id="bad-payers-table">
			<thead>
				<tr>
					<th>{{Lang::get('client.accountant_number')}}</th>
					<th>{{Lang::get('cart.client')}}</th>
					<th>{{Lang::get('reports.date_document')}}</th>
					<th>{{Lang::get('reports.payment_date')}}</th>
					<th>{{Lang::get('reports.charge')}}</th>			
					<th>{{Lang::get('reports.days_late')}}</th>
					<th>{{Lang::get('client.pdf_label')}}</th>
					<th></th>		
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTableBadPayers = $('#bad-payers-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 1, "desc" ]] ,
		ajax: {
			url : "{!! route('bad_payers.data') !!}",
			data: function (d) {
			}
		},
		columns: [
			{ data: 'accountant_number', name: 'accountant_number'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'fecha_documento', name: 'fecha_documento'},
			{ data: 'fecha_pago', name: 'fecha_pago', searchable:false},
			{ data: 'cargo', name: 'cargo', searchable:false},
			{ data: 'dias_retraso', name: 'dias_retraso' , searchable:false},
			{ data: 'download_pdf', name: 'download_pdf', orderable:false, searchable:false},
			{ data: 'ver_cliente', name: 'ver_cliente', orderable:false, searchable:false},
			
			/*
			{ data: 'accountant_number', name: 'accountant_number'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'fecha_documento', name: 'fecha_documento'},
			{ data: 'fecha_pago', name: 'fecha_pago'},
			{ data: 'cargo', name: 'cargo'},
			{ data: 'dias_retraso', name: 'dias_retraso'},
			{ data: 'download_pdf', name: 'download_pdf', bSearchable:false, bSortable:false},
			{ data: 'ver_cliente', name: 'ver_cliente', bSearchable:false, bSortable:false},
			*/
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('#descargaCSV').click(function(){

		var route = "{{route('bad_payers.data.csv')}}";

		location.href= route;

		
	});
  </script>
@endsection
	
	