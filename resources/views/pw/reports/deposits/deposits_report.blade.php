@extends('app')
@section('content')
    <div class="row">
        <table class="table table-hover no-footer dataTable" id="tableDepositsReport">
            <thead>
                <tr>
                    <th>{{ trans('deposits_report.storage') }}</th>
                    <th>{{ trans('deposits_report.name') }}</th>
                    <th>{{ trans('deposits_report.company_name') }}</th>
                    <th>{{ trans('deposits_report.paid') }}</th>
                    <th>{{ trans('deposits_report.deposit_amount') }}</th>
                    <th>{{ trans('deposits_report.payment_type') }}</th>
                    <th>{{ trans('deposits_report.notes') }}</th>
                    <th>{{ trans('deposits_report.deposit_date') }}</th>
                </tr>
            </thead>
            @foreach($deposits as $deposit)
                    <tr>
                        <td>{{$deposit->storage->alias}}</td>
                        <td>{{$deposit->user->name}} {{$deposit->user->lastName}}</td>
                        <td>{{$deposit->user->companyName}}</td>
                        <td>
                        	@if($deposit->de_pagado == 1)
                        		{{trans('deposits_report.yes')}}
                        	@else
                        		{{trans('deposits_report.no')}}
                        	@endif
                        </td>
                        <td>{{number_format($deposit->de_monto_deposito, 2)}}</td>
                        <td>
                        	@if(!is_null($deposit->PaymentType))
                        		{{$deposit->PaymentType->name}}
                        	@endif
                        </td>
                        <td>{{$deposit->de_notas}}</td>
                        <td>{{$deposit->created_at}}</td>
                    </tr>
            @endforeach
        </table>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableDepositsReport').DataTable( {
                "pagingType": "full_numbers",
                "ordering": false
            } );
        } );
    </script>
@endsection
