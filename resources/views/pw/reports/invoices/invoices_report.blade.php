@extends('app')
@section('content')

<style>
.red {color:red !important}
</style>
  <div class="card">
    <div class="card-head style-default">
    	@if($paragon == 0)
      	<header>{{Lang::get('reports.invoices')}}</header>
      	@else
      	<header>Paragons</header>
      	@endif
    </div>
    <div class="card-body">
		  <div class="row">
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.start_date')}}</label>
		  		<input type="text" name="fechaInicio" id="fechaInicio" value="{{$start}}" class="form-control datepicker"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.end_date')}}</label>
		  		<input type="text" name="fechaFin" id="fechaFin" value="{{$end}}" class="form-control datepicker"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.number_invoices')}}</label>
		  		<input type="text" name="number_invoices" id="number_invoices" placeholder="Example: 101,102,103" class="form-control"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>{{trans('reports.client')}}</label>
		  		<input type="text" name="client" id="client_text" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-1">
		  		<label>{{trans('reports.from_invoice')}}</label>
		  		<input type="text" name="from_invoice" id="from_invoice" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-1">
		  		<label>{{trans('reports.to_invoice')}}</label>
		  		<input type="text" name="to_invoice" id="to_invoice" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-invoices">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  	<div class="col-md-6 col-md-offset-6">
		  		<button class="btn btn-danger pull-right" id="descargaZIPCopies">
		  			{{Lang::get('reports.export_zip_copies')}}
		  		</button>
		  		
		  		<button class="btn btn-info pull-right" id="descargaTXT">
		  			{{Lang::get('reports.export_txt')}}
		  		</button>
		  		
		  		<button class="btn btn-danger pull-right" id="descargaZIP">
		  			{{Lang::get('reports.export_zip')}}
		  		</button>
		  		
		  		<button class="btn btn-success pull-right" id="descargaCSV">
		  			{{Lang::get('reports.export_csv')}}
		  		</button>
		  	</div>
		</div>
	</div>
  </div>
		
  <div class="row">
	<div class="col-md-12">
		<table class="table table-hover" id="invoices-table">
			<thead>
				<tr>
					<!-- <th>{{trans('client.id_lbl')}}</th> -->
					<th>{{trans('reports.year_invoice')}}</th>
					<th>{{trans('reports.number_invoice')}}</th>
					<th>{{trans('client.accountant_number')}}</th>
					<th>{{trans('cart.client')}}</th>
					<th>{{trans('client.storage')}}</th>
					<th>{{trans('reports.date_document')}}</th>
					<th>{{trans('reports.payment_date')}}</th>
					<th>{{trans('reports.days_late')}}</th>
					<th>{{trans('reports.paid')}}</th>
					<th>{{trans('reports.subtotal')}}</th>
					<th>{{trans('reports.vat')}}</th>
					<th>{{trans('reports.total')}}</th>
					<th>{{trans('cart.client')}}</th>
					<th>{{trans('client.pdf_label')}}</th>
					<th>{{trans('client.pdf_copy_label')}}</th>		
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTableInvoices = $('#invoices-table').DataTable({
		"bInfo" : false,
		"order" : [[ 1, 'desc' ]],
		processing: true,
		serverSide: true,
		bFilter : false,
		ajax: {
			url : "{!! route('invoices_report.data') !!}",
			data: function (d) {
				d.fechaInicio 		= $("#fechaInicio").val();
				d.fechaFin			= $("#fechaFin").val();
				d.client			= $("#client_text").val();
				d.number_invoices	= $("#number_invoices").val();
				d.from_invoice		= $("#from_invoice").val();
				d.to_invoice		= $("#to_invoice").val();
				d.paragon			= "{{$paragon}}";
			}
		},
		columns: [
			/*{ data: 'idBilling', name: 'idBilling'},*/
			{ data: 'bi_year_invoice', name: 'bi_year_invoice'},
			{ data: 'bi_number_invoice', name: 'bi_number_invoice'},
			{ data: 'accountant_number', name: 'accountant_number'},
			{ data: 'Full', name: 'Full'},
			{ data: 'storages.alias', name: 'storages.alias'},
			{ data: 'fecha_documento', name: 'fecha_documento'},
			{ data: 'fecha_pago', name: 'fecha_pago'},
			{ data: 'dias_retraso', name: 'dias_retraso'},
			{ data: 'pagado', name: 'pagado', searchable:false, orderable:false},
			{ data: 'bi_subtotal', name: 'bi_subtotal'},
			{ data: 'bi_total_vat', name: 'bi_total_vat'},
			{ data: 'bi_total', name: 'bi_total'},
			{ data: 'ver_cliente', name: 'ver_cliente'},
			{ data: 'download_pdf', name: 'download_pdf', searchable:false, orderable:false},
			{ data: 'download_pdf_copy', name: 'download_pdf_copy', searchable:false, orderable:false},
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('.datepicker').datetimepicker({
  	    format: 'Y-MM-DD',
  	    allowInputToggle: true,
  	    //minDate: moment(),
  	  });

	 $("#btn-invoices").click(function(event){
		 if( $("#from_invoice").val() != "" ){
			if( $("#to_invoice").val() == "" ){
				toastr["error"]("{{ trans('reports.selectToInvoice') }}");
				return false;
			}
			else{
				oTableInvoices.draw();
			}
		 }
		 oTableInvoices.draw();	 
		});

	$('#descargaCSV').click(function(){

		var route = "{{route('invoices_report.data.csv',['*fechaInicio*','*fechaFin*','*client*','*numberInvoices*','*fromInvoice*','*toInvoice*','*paragon*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		route = route.replace('*paragon*',"{{$paragon}}");
		
		var cliente = 0;
		var number_invoices = 0;
		var from_invoice = 0;
		var to_invoice = 0;
		
		if($("#client_text").val() != "")
			cliente = $("#client_text").val();
		
		if($("#number_invoices").val() != "")
			number_invoices = $("#number_invoices").val();
		
		if($("#from_invoice").val() != "")
			from_invoice = $("#from_invoice").val();
		
		if($("#to_invoice").val() != "")
			to_invoice = $("#to_invoice").val();
		
		route = route.replace('*client*',cliente);
		route = route.replace('*numberInvoices*',number_invoices);
		route = route.replace('*fromInvoice*',from_invoice);
		route = route.replace('*toInvoice*',to_invoice);

		location.href= route;

	});

	$('#descargaZIP').click(function(){

		var route = "{{route('invoices_report.data.zip',['*fechaInicio*','*fechaFin*','*client*','*numberInvoices*','*fromInvoice*','*toInvoice*','*paragon*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		route = route.replace('*paragon*',"{{$paragon}}");
		
		var cliente = 0;
		var number_invoices = 0;
		var from_invoice = 0;
		var to_invoice = 0;
		
		if($("#client_text").val() != "")
			cliente = $("#client_text").val();
		
		if($("#number_invoices").val() != "")
			number_invoices = $("#number_invoices").val();
		
		if($("#from_invoice").val() != "")
			from_invoice = $("#from_invoice").val();
		
		if($("#to_invoice").val() != "")
			to_invoice = $("#to_invoice").val();
		
		route = route.replace('*client*',cliente);
		route = route.replace('*numberInvoices*',number_invoices);
		route = route.replace('*fromInvoice*',from_invoice);
		route = route.replace('*toInvoice*',to_invoice);
		
		location.href= route;

	});

	$('#descargaZIPCopies').click(function(){

		var route = "{{route('invoices_report.data.zip_copies',['*fechaInicio*','*fechaFin*','*client*','*numberInvoices*','*fromInvoice*','*toInvoice*','*paragon*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		route = route.replace('*paragon*',"{{$paragon}}");
		
		var cliente = 0;
		var number_invoices = 0;
		var from_invoice = 0;
		var to_invoice = 0;
		
		if($("#client_text").val() != "")
			cliente = $("#client_text").val();
		
		if($("#number_invoices").val() != "")
			number_invoices = $("#number_invoices").val();
		
		if($("#from_invoice").val() != "")
			from_invoice = $("#from_invoice").val();
		
		if($("#to_invoice").val() != "")
			to_invoice = $("#to_invoice").val();
		
		route = route.replace('*client*',cliente);
		route = route.replace('*numberInvoices*',number_invoices);
		route = route.replace('*fromInvoice*',from_invoice);
		route = route.replace('*toInvoice*',to_invoice);
		
		location.href= route;

	});

	$('#descargaTXT').click(function(){

		var route = "{{route('invoices_report.data.txt',['*fechaInicio*','*fechaFin*','*client*','*numberInvoices*','*fromInvoice*','*toInvoice*','*paragon*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		route = route.replace('*paragon*',"{{$paragon}}");
		
		var cliente = 0;
		var number_invoices = 0;
		var from_invoice = 0;
		var to_invoice = 0;
		
		if($("#client_text").val() != "")
			cliente = $("#client_text").val();
		
		if($("#number_invoices").val() != "")
			number_invoices = $("#number_invoices").val();
		
		if($("#from_invoice").val() != "")
			from_invoice = $("#from_invoice").val();
		
		if($("#to_invoice").val() != "")
			to_invoice = $("#to_invoice").val();
		
		route = route.replace('*client*',cliente);
		route = route.replace('*numberInvoices*',number_invoices);
		route = route.replace('*fromInvoice*',from_invoice);
		route = route.replace('*toInvoice*',to_invoice);
		
		location.href= route;

	});

	$("#client_text").keyup(function(){
		oTableInvoices.draw();	
		});

  </script>
@endsection
	
	