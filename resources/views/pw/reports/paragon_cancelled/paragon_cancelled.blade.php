@extends('app')
@section('content')

<div class="card">
    <div class="card-head style-default">
      <header>Paragon Cancelled</header>
    </div>
    <div class="card-body">
		  <div class="row">
		  <!-- 
		  	<div class="col-md-1">
		  		<label>{{trans('reports.from_invoice')}}</label>
		  		<input type="text" name="from_invoice" id="from_invoice" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-1">
		  		<label>{{trans('reports.to_invoice')}}</label>
		  		<input type="text" name="to_invoice" id="to_invoice" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-invoices">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  </div>
		  -->
				  <div class="row">
				  	<!-- 
				  	<div class="col-md-4 col-md-offset-8">
				  		<button class="btn btn-success pull-right" id="descargaCSV">
				  			{{Lang::get('reports.export_xls')}}
				  		</button>
				  	</div>
				  	 -->
					<div class="col-md-12">
						<table class="table table-hover" id="paragon-cancelled-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Client</th>
									<th>Storage</th>
									<th>Number Paragon</th>
									<th>Year Invoice</th>
									<th>Debit</th>
									<th>Payment</th>
									<th>Correction Date</th>
									<th>PDF</th>
								</tr>
							</thead>
						</table>
							
					</div>
				  </div>			
		</div>
	</div>
	

  <script type="text/javascript">

  	var oTable = $('#paragon-cancelled-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 1, "desc" ]] ,
		ajax: {
			url : "{!! route('paragonCancelledReport.data') !!}",
			data: function (d) {
				d.fromInvoice 		= $("#from_invoice").val();
				d.toInvoice			= $("#to_invoice").val();
			}
		},
		columns: [
			{ data: 'id', name: 'id'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'storages.alias', name: 'storages.alias'},
			{ data: 'bi_number_invoice', name: 'bi_number_invoice'},
			{ data: 'bi_year_invoice', name: 'bi_year_invoice'},
			{ data: 'cargo', name: 'cargo'},
			{ data: 'abono_corregido', name: 'abono_corregido'},
			{ data: 'correction_date', name: 'correction_date'},
			{ data: 'pdf', name: 'pdf', searchable:false, orderable:false},
			
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('#descargaCSV').click(function(){

  		var from_invoice = 0;
		var to_invoice = 0;
		
		var route = "{{route('correctedInvoicesReport.data.csv',['*fromInvoice*','*toInvoice*','*yearInvoice*'])}}";

		if($("#from_invoice").val() != "")
			from_invoice = $("#from_invoice").val();
		
		if($("#to_invoice").val() != "")
			to_invoice = $("#to_invoice").val();

		route = route.replace('*fromInvoice*',from_invoice);
		route = route.replace('*toInvoice*',to_invoice);
		route = route.replace('*yearInvoice*',$("#year_invoice").val());
		
		location.href= route;


	});
  </script>
@endsection
	
	