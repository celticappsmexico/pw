@extends('app')
@section('content')
    <div class="row">
        <table class="table table-hover no-footer dataTable" id="tableWarehouseLevelStorage">
            <thead>
                <tr>
                    <th>{{ trans('history_occupancy_rate.warehouse') }}</th>
                    <th>{{ trans('history_occupancy_rate.level') }}</th>
                    <th>{{ trans('history_occupancy_rate.sqm') }}</th>
                    <th>{{ trans('history_occupancy_rate.numberBoxes') }}</th>
                    <th>{{ trans('history_occupancy_rate.numberBoxesRented') }}</th>
                    <th>{{ trans('history_occupancy_rate.numberBoxesAvailable') }}</th>
                    <th>{{ trans('history_occupancy_rate.occupancy_rate') }}</th>
                </tr>
            </thead>
            @foreach($storages as $storage)
                    <tr>
                        <td>{{$storage->level->warehouse->name}}</td>
                        <td>{{$storage->level->name}}</td>
                        <td>{{$storage->sqm}}</td>
                        <td>{{$storage->numberBoxes->numberBoxes}}</td>
                        <td>{{$storage->numberBoxesRented->numberBoxesRented}}</td>
                        <td>{{$storage->numberBoxesAvailable->numberBoxesAvailable}}</td>
                        <td id="tdORate">{{number_format($storage->promedio, 2)}}%</td>
                    </tr>
            @endforeach
        </table>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableWarehouseLevelStorage').DataTable( {
                "pagingType": "full_numbers",
                "order": [ 0, 'desc' ]
            } );
        } );
    </script>
@endsection
