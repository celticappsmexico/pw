@extends('app')
@section('content')
    <div class="row" style="margin-top: 6%;">
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>0]) }}"><img src="{{asset('assets/img/0days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>1]) }}"><img src="{{asset('assets/img/1day.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>3]) }}"><img src="{{asset('assets/img/3days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>6]) }}"><img src="{{asset('assets/img/6days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
    </div>

    <div class="row" style="margin-top: 6%;">
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>9]) }}"><img src="{{asset('assets/img/9days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>15]) }}"><img src="{{asset('assets/img/15days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('boxesReportDays', ['days'=>30]) }}"><img src="{{asset('assets/img/30days.png')}}" alt="" style="width: 80%;"/></a>
        </div>
        <div class="col-md-3">

        </div>
    </div>
@endsection
