@extends('app')
@section('content')
    <div class="row" style="margin-bottom: 1%;">
        <a class="btn btn-primary" href="{{ route('boxesReport') }}" role="button">Back</a>
        <a href="{{ route('boxesReportDays/export', ['days'=>$days]) }}" class="btn btn-success pull-right" role="button">Export CSV</a>
    </div>
    <div class="row">
        <table class="table table-hover no-footer dataTable" id="tableBoxesReport">
            <thead>
                <tr>
                    <th>{{ trans('tenants_report.storage') }}</th>
                    <th>{{ trans('tenants_report.level') }}</th>
                    <th>{{ trans('tenants_report.warehouse') }}</th>
                    <th>{{ trans('tenants_report.name') }}</th>
                    <th>{{ trans('tenants_report.company_name') }}</th>
                    <th>{{ trans('tenants_report.rent_start') }}</th>
                    <th>{{ trans('tenants_report.rent_end') }}</th>
                    <th>{{ trans('boxes.remaining_days') }}</th>
                </tr>
            </thead>
            @foreach($tenants as $tenant)
                    <tr>
                        <td>{{$tenant->storageAlias}}</td>
                        <td>{{$tenant->levelName}}</td>
                        <td>{{$tenant->warehouseName}}</td>
                        <td>{{$tenant->userName}} {{$tenant->userLN}}</td>
                        <td>{{$tenant->companyName}}</td>
                        <td>{{$tenant->rentStart}}</td>
                        <td>{{$tenant->rentEnd}}</td>
                        <td>{{$tenant->remaining_days}}</td>
                    </tr>
            @endforeach
        </table>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableBoxesReport').DataTable( {
                "pagingType": "full_numbers",
                "ordering": false
            } );
        } );
    </script>
@endsection
