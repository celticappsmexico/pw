@extends('app')
@section('content')

<style>
.red {color:red !important}
</style>
  <div class="card">
    <div class="card-head style-default">
    	<header>Payments</header>
      	
    </div>
    <div class="card-body">
		  <div class="row">
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.start_date')}}</label>
		  		<input type="text" name="fechaInicio" id="fechaInicio" value="{{$start}}" class="form-control datepicker"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.end_date')}}</label>
		  		<input type="text" name="fechaFin" id="fechaFin" value="{{$end}}" class="form-control datepicker"/>
		  	</div>
            <div class="col-md-3">
		  		<label>Payment Type</label>
                  <select id="payment_type" class="form-control">
                    <option value="">All</option>
                    <option value="1">Cash</option>
                    <option value="2">Credit Card - Braintree</option>
                    <option value="3">Transfer</option>
                    <option value="4">Credit Card - Terminal</option>  
                  </select>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-search">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  	<div class="col-md-3">
		  		<button class="btn btn-success pull-right" id="btn-export-csv">
		  			EXPORT TO CSV
		  		</button>
		  	</div>
            
		</div>
	</div>
  </div>
		
  <div class="row">
	<div class="col-md-12">
		<table class="table table-hover" id="payments-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tenant</th>
					<th>Storage / Box</th>
					<th>Payment Date</th>
					<th>Automatic Pay</th>
					<th>Payment Type</th>
					<th>Amount</th>
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTableBilling = $('#payments-table').DataTable({
		"bInfo" : false,
		"order" : [[ 1, 'desc' ]],
		processing: true,
		serverSide: true,
		bFilter : false,
		ajax: {
			url : "{!! route('report.payments_index.data') !!}",
			data: function (d) {
				d.fechaInicio 		= $("#fechaInicio").val();
				d.fechaFin			= $("#fechaFin").val();
                d.paymentTypeId     = $("#payment_type").val();
			}
		},
		columns: [
			{ data: 'id', name: 'id'},
            { data: 'tenant', name: 'tenant'},
            { data: 'storages.alias', name: 'storages.alias'},
            { data: 'pay_month', name: 'pay_month'},
            { data: 'automatic_pay', name: 'automatic_pay', searchable:false},
            { data: 'payment_type.name', name: 'payment_type.name'},
            { data: 'amount', name: 'amount'}, 
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('.datepicker').datetimepicker({
  	    format: 'Y-MM-DD',
  	    allowInputToggle: true,
  	    //minDate: moment(),
  	  });

	$('#btn-export-csv').click(function(){

		var route = "{{route('report.payments_index.data.csv',['*fechaInicio*','*fechaFin*','*paymentType*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		
		var payment_type ;
		if($("#payment_type").val() == ""){
			payment_type = 0
		}else{
			payment_type = $("#payment_type").val();
		}

        route = route.replace('*paymentType*',payment_type);
		
		location.href= route;

	});

    $("#btn-search").click(function(e){
        oTableBilling.draw();
    });

  </script>
@endsection
	
	