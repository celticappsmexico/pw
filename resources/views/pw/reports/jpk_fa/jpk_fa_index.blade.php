@extends('app')
@section('content')

<style>
.red {color:red !important}
</style>
  <div class="card">
    <div class="card-head style-default">
    	<header>JPK_FA</header>
      	
    </div>
    <div class="card-body">
		  <div class="row">
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.start_date')}}</label>
		  		<input type="text" name="fechaInicio" id="fechaInicio" value="{{$start}}" class="form-control datepicker"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>{{Lang::get('reports.end_date')}}</label>
		  		<input type="text" name="fechaFin" id="fechaFin" value="{{$end}}" class="form-control datepicker"/>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-invoices">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  	<div class="col-md-6 col-md-offset-6">
		  		<button class="btn btn-danger pull-right" id="btn-generate-jpk-fa">
		  			GENERATE JPK FA
		  		</button>
		  	</div>
		</div>
	</div>
  </div>
		
  <div class="row">
	<div class="col-md-12">
		<table class="table table-hover" id="invoices-table">
			<thead>
				<tr>
					<th>{{trans('reports.year_invoice')}}</th>
					<th>{{trans('reports.number_invoice')}}</th>
					<th>{{trans('client.accountant_number')}}</th>
					<th>{{trans('cart.client')}}</th>
					<th>{{trans('client.storage')}}</th>
					<th>{{trans('reports.date_document')}}</th>
					<th>{{trans('reports.subtotal')}}</th>
					<th>{{trans('reports.vat')}}</th>
					<th>{{trans('reports.total')}}</th>
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTableInvoices = $('#invoices-table').DataTable({
		"bInfo" : false,
		"order" : [[ 1, 'desc' ]],
		processing: true,
		serverSide: true,
		bFilter : false,
		ajax: {
			url : "{!! route('jpk_fa.data') !!}",
			data: function (d) {
				d.fechaInicio 		= $("#fechaInicio").val();
				d.fechaFin			= $("#fechaFin").val();
			}
		},
		columns: [
			{ data: 'bi_year_invoice', name: 'bi_year_invoice'},
			{ data: 'bi_number_invoice', name: 'bi_number_invoice'},
			{ data: 'user.accountant_number', name: 'user.accountant_number'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'storages.alias', name: 'storages.alias'},
			{ data: 'fecha_documento', name: 'fecha_documento'},
			{ data: 'bi_subtotal', name: 'bi_subtotal'},
			{ data: 'bi_total_vat', name: 'bi_total_vat'},
			{ data: 'bi_total', name: 'bi_total'},
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('.datepicker').datetimepicker({
  	    format: 'Y-MM-DD',
  	    allowInputToggle: true,
  	    //minDate: moment(),
  	  });

	$('#btn-generate-jpk-fa').click(function(){

		var route = "{{route('jpk_fa.generar',['*fechaInicio*','*fechaFin*'])}}";
		route = route.replace('*fechaInicio*',$("#fechaInicio").val());
		route = route.replace('*fechaFin*',$("#fechaFin").val());
		
		location.href= route;

	});

  </script>
@endsection
	
	