@extends('app')
@section('content')
<div class="card">
	<div class="card-head style-default">
      <header>Insurance Report</header>
    </div>
    <div class="card-body">
		  <div class="row">
		  	<div class="col-md-3">
		  		<label>Date Start</label>
		  		<input type="text" id="fechaInicio" value="{{$start}}"  class="form-control datepicker"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>Date end</label>
		  		<input type="text" id="fechaFin" value="{{$end}}" class="form-control datepicker"/>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-search">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  	<div class="col-md-6 col-md-offset-6">
		  		<button class="btn btn-success pull-right" id="descargaCSV">
		  			{{Lang::get('reports.export_csv')}}
		  		</button>
		  	</div>
		</div>
	</div>
 </div>
 

 
  <div class="row">
	<div class="col-md-12">
		<table class="table table-hover" id="insurance-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Client</th>
					<th>Storage</th>
					<th>Annual Cost</th>
					<th>Daily cost</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Days Period</th>
					<th>Total</th>
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTable = $('#insurance-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 2, "desc" ]] ,
		ajax: {
			url : "{!! route('insuranceDetails.data') !!}",
			data: function (d) {
				d.fechaInicio 		= $("#fechaInicio").val();
				d.fechaFin			= $("#fechaFin").val();
			}
		},
		columns: [
			{ data: 'DT_Row_Index', name: 'DT_Row_Index'},
			{ data: 'fullname', name: 'fullname'},
			{ data: 'alias', name: 'alias'},
			{ data: 'costo_anual', name: 'costo_anual'},
			{ data: 'costo_diario', name: 'costo_diario'},
			{ data: 'fecha_inicio', name: 'fecha_inicio'},
			{ data: 'fecha_fin', name: 'fecha_fin'},
			{ data: 'dias_periodo', name: 'dias_periodo'},
			{ data: 'total_insurance', name: 'total_insurance'},
			
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('#descargaCSV').click(function(){

		var route = "{{route('insuranceReportDetails.data.csv',['*fechaInicio*','*fechaFin*'])}}";
        
        route = route.replace('*fechaInicio*',$("#fechaInicio").val());
        route = route.replace('*fechaFin*',$("#fechaFin").val());
    	location.href= route;

	});


	$("#btn-search").click(function(event){
		oTable.draw();
		});

  	$('.datepicker').datetimepicker({
  	    format: 'Y-MM-DD',
  	    allowInputToggle: true,
  	    //minDate: moment(),
  	  });

  	$('#input-storage , #input-client').keyup(function(e){
  		oTable.draw();
	});
  </script>
@endsection
	
	