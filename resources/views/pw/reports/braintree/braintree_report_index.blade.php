@extends('app')
@section('content')
<div class="card">
	<div class="card-head style-default">
      <header>Braintree Transaction</header>
    </div>
    <div class="card-body">
		  <div class="row">
		  	<div class="col-md-3">
		  		<label>Storage</label>
		  		<input type="text" id="input-storage" value="" class="form-control"/>
		  	</div>
		  	<div class="col-md-3">
		  		<label>Client</label>
		  		<input type="text" id="input-client" value="" class="form-control"/>
		  	</div>
		  	
		  	<div class="col-md-3">
		  		<br/>
		  		<button class="btn btn-info" id="btn-invoices">
		  			{{Lang::get('billing_reports.search')}}
		  		</button>
		  	</div>
		  	<div class="col-md-6 col-md-offset-6">
		  		<button class="btn btn-success pull-right" id="descargaCSV">
		  			{{Lang::get('reports.export_csv')}}
		  		</button>
		  	</div>
		</div>
	</div>
 </div>
 
  <div class="row">
	<div class="col-md-12">
		<table class="table table-hover" id="prepayments-table">
			<thead>
				<tr>
					<th>Client</th>
					<th>Type</th>
					<th>Date</th>
					<th style="width: 40%">Result</th>
				</tr>
			</thead>
		</table>
			
	</div>
  </div>

  <script type="text/javascript">
  	var oTable = $('#prepayments-table').DataTable({
		"bInfo" : false,
		processing: true,
		serverSide: true,
		bFilter : true,
		"order": [[ 2, "desc" ]] ,
		ajax: {
			url : "{!! route('braintreeReport.data') !!}",
			data: function (d) {
				
			}
		},
		columns: [
			{ data: 'fullname', name: 'fullname'},
			{ data: 'tb_tipo', name: 'tb_tipo'},
			{ data: 'created_at', name: 'created_at'},
			{ data: 'tb_respuesta', name: 'tb_respuesta'},
			
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

  	$('#descargaCSV').click(function(){

		var route = "{{route('correctedInvoicesReport.data.csv')}}";

		location.href= route;

		
	});

  	$('#input-storage , #input-client').keyup(function(e){
  		oTable.draw();
	});
  </script>
@endsection
	
	