@extends('app')
@section('content')
  {{-- dd($udata->avatar) --}}

  <script>
    $(document).ready(function() {
      $(document).on('click','.sendAvatar', function() {
        $('#frmUpdateAvatar .clientAvatar img').remove();

        var form = $('#frmUpdateAvatar')[0];
        var formData = new FormData(form);

        $.ajax({
          url: '{{ route('updateAvatar') }}',
          type: 'POST',
          dataType : 'json',
          async: false,
          data: formData,
          contentType: false,
          processData: false,
        })
        .done(function(data) {
          //var jsonData = data.responseJSON
          console.log(data.success);
          if(data.success == 'true'){
            $('#frmUpdateAvatar .clientAvatar img').remove();
            $('.avatar_top img').remove();
            //$('#frmUpdateAvatar .clientAvatar').attr('src',data.path_img);
            $('#frmUpdateAvatar .clientAvatar').append('<img style="width: 140px; height: 140px;" class="img-circle border-white border-xl img-responsive auto-width " src="'+data.path_img+'" alt=""><br>');
            
            $('.avatar_top').append('<img style="width: 40px; height: 40px;" class="img-circle border-white border-xl img-responsive auto-width " src="'+data.path_img+'" alt="">');
            
            toastr['success']('{{ trans('client.done') }}');

            $("#frmUpdateAvatar")[0].reset();

          }else{
            toastr['error']('{{ trans('client.somethingWasWrong_lbl') }}');
          }


        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });


      });
    });
  </script>

  <h1>{{ trans('client.profile') }}</h1>
  <div class="col-sm-6">
    <div class="card">
		  <div class="card-body">
        <form action="" name="updateAvatar" id="frmUpdateAvatar" class="frmUpdateAvatar form" autocomplete=”off”>
          <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
          <input type="hidden" name="id" id="id" class="id" value="{{ $udata->id }}">
          <input type="hidden" name="profileEdit" id="profileEdit" class="profileEdit" value="1">

          <div class="row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-6 clientAvatar">
              <img style="width: 140px; height: 140px;" class="img-circle border-white border-xl img-responsive auto-width " src="{{ asset( $udata->avatar) }}" alt=""><br>
            </div>
          </div>

          <input type="file" name="fileAvatar" id="fileAvatar" class="form-control username">

          <div class="form-footer">
            <a href="javascript:void(0);" class="btn btn-primary sendAvatar" name="button">{{ trans('client.btn_update') }}</a>
          </div>

        </form>
      </div>
    </div>
  </div>


  <div class="col-sm-6">
    <div class="card">
		  <div class="card-body">

        @if ($udata->userType_id == 1)
        <form action="" name="frmUpdateClientPerson" id="frmUpdateClientPerson" class="frmUpdateClient form" autocomplete=”off”>

          <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
          <input type="hidden" name="id" id="id" class="id" value="{{ $udata->id }}">
          <input type="hidden" name="profileEdit" id="profileEdit" class="profileEdit" value="1">


          <div class="form-group" id="g_username">
              <label>{{ trans('client.username_lbl') }}</label>
              {!! Form::input('text', 'username', $udata->username , ['class'=> 'form-control username', 'id' => 'usernameClientPerson', 'readonly' => 'true']) !!}
          </div>

            <div class="form-group" id="g_name">
                <label>{{ trans('form.label.name') }}</label>
                {!! Form::input('text', 'name', $udata->name, ['class'=> 'form-control name', 'id' => 'name']) !!}
            </div>

            <div class="form-group" id="g_lastName">
                <label>{{ trans('client.last_lbl') }}</label>
                {!! Form::input('text', 'lastName', $udata->lastName, ['class'=> 'form-control lastName', 'id' => 'lastName']) !!}
            </div>

            <div class="form-group" id="g_birthday">
                <label>{{ trans('client.birthday_lbl') }}</label>
                {!! Form::input('text', 'birthday', $udata->birthday, ['class'=> 'form-control birthday datepicker', 'id' => 'birthday']) !!}
            </div>

            <fieldset>
              <legend>{{ trans('client.address_lbl') }}</legend>

              <div class="form-group" id="g_street">
                  <label>{{ trans('client.street_lbl') }}</label>
                  {!! Form::input('text', 'street', $udata->userAddress[0]->street, ['class'=> 'form-control street', 'id' => 'street']) !!}
              </div>

              <div class="form-group" id="g_number">
                  <label>{{ trans('client.number_lbl') }}</label>
                  {!! Form::input('text', 'number', $udata->userAddress[0]->number, ['class'=> 'form-control number', 'id' => 'number']) !!}
              </div>

              <div class="form-group" id="g_apartmentNumber">
                  <label>{{ trans('client.apartment_lbl') }}</label>
                  {!! Form::input('text', 'apartmentNumber', $udata->userAddress[0]->apartmentNumber, ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
              </div>

              <div class="form-group" id="g_postCode">
                  <label>{{ trans('client.postCode_lbl') }}</label>
                  {!! Form::input('text', 'postCode', $udata->userAddress[0]->postCode, ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
              </div>

              <div class="form-group" id="g_city">
                  <label>{{ trans('client.city_lbl') }}</label>
                  {!! Form::input('text', 'city', $udata->userAddress[0]->city, ['class'=> 'form-control city', 'id' => 'city']) !!}
              </div>

              <div class="form-group" id="g_country">
                <label for="">{{ trans('client.country_lbl') }}</label>
                <select id="country" name="country" id="country" class="form-control country">
                  @foreach ($countries as $country)
                  <?php $val = ($country->id ==  $udata->userAddress[0]->country_id) ? 'selected' : ''; ?>
                  <option value="{{$country->id}}" <?php echo $val; ?>>{{$country->name}}</option>
                  @endforeach
                </select>
              </div>

            </fieldset>

            <div class="form-group" id="g_peselNumber">
                <label>{{ trans('client.pesel_lbl') }}</label>
                {!! Form::input('text', 'peselNumber', $udata->peselNumber, ['class'=> 'form-control peselNumber', 'id' => 'peselNumber']) !!}
            </div>

            <div class="form-group" id="g_idNumber">
                <label>{{ trans('client.id_lbl') }}</label>
                {!! Form::input('text', 'idNumber', $udata->idNumber, ['class'=> 'form-control idNumber', 'id' => 'idNumber']) !!}
            </div>

            <div class="form-group" id="g_phone">
                <label>{{ trans('client.phone_lbl') }}</label>
                {!! Form::input('text', 'phone', $udata->phone, ['class'=> 'form-control phone', 'id' => 'phone']) !!}
            </div>

            <div class="form-group" id="g_email">
                <label>{{ trans('client.email_lbl') }}</label>
                {!! Form::input('email', 'email', $udata->email, ['class'=> 'form-control email', 'id' => 'emailClientPerson', 'autocomplete' => 'false', 'readonly' => 'true']) !!}
            </div>

            <div class="form-footer">
              <button type="submit" class="btn btn-primary" name="button">{{ trans('client.btn_update') }}</button>
            </div>

          </form>

          @endif
          @if ($udata->userType_id == 2)

          <!-- form new client person -->

          <form action="" name="frmUpdateClientOnePerson" id="frmUpdateClientOnePerson" class="frmUpdateClient" autocomplete=”off”>

            <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
            <input type="hidden" name="id" id="id" class="id" value="{{ $udata->id }}">
            <input type="hidden" name="profileEdit" id="profileEdit" class="profileEdit" value="1">

            <div class="form-group" id="g_username">
                <label>{{ trans('client.username_lbl') }}</label>
                {!! Form::input('text', 'username', $udata->username, ['class'=> 'form-control username', 'id' => 'usernameClientOnePerson', 'readonly' => 'true']) !!}
            </div>

              <div class="form-group" id="g_name">
                  <label>{{ trans('form.label.name') }}</label>
                  {!! Form::input('text', 'name', $udata->name, ['class'=> 'form-control name', 'id' => 'name']) !!}
              </div>

              <div class="form-group" id="g_lastName">
                  <label>{{ trans('client.last_lbl') }}</label>
                  {!! Form::input('text', 'lastName', $udata->lastName, ['class'=> 'form-control lastName', 'id' => 'lastName']) !!}
              </div>

              <div class="form-group" id="g_companyName">
                  <label>{{ trans('client.company_lbl') }}</label>
                  {!! Form::input('text', 'companyName', $udata->companyName, ['class'=> 'form-control companyName', 'id' => 'companyName']) !!}
              </div>

              <div class="form-group" id="g_birthday">
                  <label>{{ trans('client.birthday_lbl') }}</label>
                  {!! Form::input('text', 'birthday', $udata->birthday, ['class'=> 'form-control birthday datepicker', 'id' => 'birthday']) !!}
              </div>

              <fieldset>
                <legend>{{ trans('client.address_lbl') }}</legend>

                <div class="form-group" id="g_street">
                    <label>{{ trans('client.street_lbl') }}</label>
                    {!! Form::input('text', 'street', $udata->userAddress[0]->street, ['class'=> 'form-control street', 'id' => 'street']) !!}
                </div>

                <div class="form-group" id="g_number">
                    <label>{{ trans('client.number_lbl') }}</label>
                    {!! Form::input('text', 'number', $udata->userAddress[0]->number, ['class'=> 'form-control number', 'id' => 'number']) !!}
                </div>

                <div class="form-group" id="g_apartmentNumber">
                    <label>{{ trans('client.apartment_lbl') }}</label>
                    {!! Form::input('text', 'apartmentNumber', $udata->userAddress[0]->apartmentNumber, ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
                </div>

                <div class="form-group" id="g_postCode">
                    <label>{{ trans('client.postCode_lbl') }}</label>
                    {!! Form::input('text', 'postCode', $udata->userAddress[0]->postCode, ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
                </div>

                <div class="form-group" id="g_city">
                    <label>{{ trans('client.city_lbl') }}</label>
                    {!! Form::input('text', 'city', $udata->userAddress[0]->city, ['class'=> 'form-control city', 'id' => 'city']) !!}
                </div>

               <div class="form-group" id="g_country">
                  <label for="">{{ trans('client.country_lbl') }}</label>
                  <select id="country" name="country" id="country" class="form-control country">
                    @foreach ($countries as $country)
                    <?php $val = ($country->id ==  $udata->userAddress[0]->country_id) ? 'selected' : ''; ?>
                    <option value="{{$country->id}}" <?php echo $val; ?>>{{$country->name}}</option>
                    @endforeach
                  </select>
                </div>

              </fieldset>


              <div class="form-group" id="g_nipNumber">
                  <label>{{ trans('client.nip_lbl') }}</label>
                  {!! Form::input('text', 'nipNumber', $udata->nipNumber, ['class'=> 'form-control nipNumber', 'id' => 'nipNumber']) !!}
              </div>

              <div class="form-group" id="g_regonNumber">
                  <label>{{ trans('client.regon_lbl') }}</label>
                  {!! Form::input('text', 'regonNumber', $udata->regonNumber, ['class'=> 'form-control regonNumber', 'id' => 'regonNumber']) !!}
              </div>

              <div class="form-group" id="g_phone">
                  <label>{{ trans('client.phone_lbl') }}</label>
                  {!! Form::input('text', 'phone', $udata->phone, ['class'=> 'form-control phone', 'id' => 'phone']) !!}
              </div>

              <div class="form-group" id="g_email">
                  <label>{{ trans('client.email_lbl') }}</label>
                  {!! Form::input('email', 'email', $udata->email, ['class'=> 'form-control email', 'id' => 'emailClientOnePerson', 'autocomplete' => 'false', 'readonly' => 'true']) !!}
              </div>

              <div class="form-footer">
                <button type="submit" class="btn btn-primary" name="button">{{ trans('client.btn_update') }}</button>
              </div>

            </form>

            @endif
            @if ($udata->userType_id == 3)

            <!-- form new client person -->

            <form action="" name="frmUpdateClientCompany" id="frmUpdateClientCompany" class="frmUpdateClient" >

              <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
              <input type="hidden" name="id" id="id" class="id" value="{{ $udata->id }}">
              <input type="hidden" name="profileEdit" id="profileEdit" class="profileEdit" value="1">

              <div class="form-group" id="g_username">
                  <label>{{ trans('client.username_lbl') }}</label>
                  {!! Form::input('text', 'username', $udata->username, ['class'=> 'form-control username', 'id' => 'usernameClientCompany', 'readonly' => 'true']) !!}
              </div>

                <div class="form-group" id="g_companyName">
                    <label>{{ trans('client.company_lbl') }}</label>
                    {!! Form::input('text', 'companyName', $udata->companyName, ['class'=> 'form-control companyName', 'id' => 'companyName']) !!}
                </div>

                <fieldset>
                  <legend>{{ trans('client.address_lbl') }}</legend>

                  <div class="form-group" id="g_street">
                      <label>{{ trans('client.street_lbl') }}</label>
                      {!! Form::input('text', 'street', $udata->userAddress[0]->street, ['class'=> 'form-control street', 'id' => 'street']) !!}
                  </div>

                  <div class="form-group" id="g_number">
                      <label>{{ trans('client.number_lbl') }}</label>
                      {!! Form::input('text', 'number', $udata->userAddress[0]->number, ['class'=> 'form-control number', 'id' => 'number']) !!}
                  </div>

                  <div class="form-group" id="g_apartmentNumber">
                      <label>{{ trans('client.apartment_lbl') }}</label>
                      {!! Form::input('text', 'apartmentNumber', $udata->userAddress[0]->apartmentNumber, ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
                  </div>

                  <div class="form-group" id="g_postCode">
                      <label>{{ trans('client.postCode_lbl') }}</label>
                      {!! Form::input('text', 'postCode', $udata->userAddress[0]->postCode, ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
                  </div>

                  <div class="form-group" id="g_city">
                      <label>{{ trans('client.city_lbl') }}</label>
                      {!! Form::input('text', 'city', $udata->userAddress[0]->city, ['class'=> 'form-control city', 'id' => 'city']) !!}
                  </div>

                  <div class="form-group" id="g_country">
                    <label for="">{{ trans('client.country_lbl') }}</label>
                    <select id="country" name="country" id="country" class="form-control country">
                      @foreach ($countries as $country)
                      <?php $val = ($country->id ==  $udata->userAddress[0]->country_id) ? 'selected' : ''; ?>
                      <option value="{{$country->id}}" <?php echo $val; ?>>{{$country->name}}</option>
                      @endforeach
                    </select>
                  </div>

                </fieldset>

                <div class="form-group" id="g_courtNumber">
                    <label>{{ trans('client.courtNumber_lbl') }}</label>
                    {!! Form::input('text', 'courtNumber',  $udata->courtNumber, ['class'=> 'form-control courtNumber', 'id' => 'courtNumber']) !!}
                </div>

                <div class="form-group" id="g_courtPlace">
                    <label>{{ trans('client.courtPlace_lbl') }}</label>
                    {!! Form::input('text', 'courtPlace', $udata->courtPlace, ['class'=> 'form-control courtPlace', 'id' => 'courtPlace']) !!}
                </div>

                <div class="form-group" id="g_krsNumber">
                    <label>{{ trans('client.krs_lbl') }}</label>
                    {!! Form::input('text', 'krsNumber', $udata->krsNumber, ['class'=> 'form-control krsNumber', 'id' => 'krsNumber']) !!}
                </div>

                <div class="form-group" id="g_nipNumber">
                    <label>{{ trans('client.nip_lbl') }}</label>
                    {!! Form::input('text', 'nipNumber', $udata->nipNumber, ['class'=> 'form-control nipNumber', 'id' => 'nipNumber']) !!}
                </div>

                <div class="form-group" id="g_regonNumber">
                    <label>{{ trans('client.regon_lbl') }}</label>
                    {!! Form::input('text', 'regonNumber', $udata->regonNumber, ['class'=> 'form-control regonNumber', 'id' => 'regonNumber']) !!}
                </div>

                <fieldset id="fieldLegalRep" style="background: rgb(228, 228, 228); padding: 10px;" disabled>
                  <legend style="background: rgb(255, 255, 255); padding: 5px; border: solid 1px rgb(209, 209, 209);">{{ trans('client.legal_title') }}</legend>
                  <h3>{{ trans('client.person1_lbl') }}</h3>

                  {!! Form::input('hidden', 'legalIdP1', $udata->UserLegal[0]->id, ['class'=> 'form-control legalIdP1', 'id' => 'legalIdP1']) !!}

                  <div class="form-group">
                      <label>{{ trans('client.name_lbl') }}</label>
                      {!! Form::input('text', 'legalNameP1',  $udata->UserLegal[0]->name, ['class'=> 'form-control legalNameP1', 'id' => 'legalNameP1']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.last_lbl') }}</label>
                      {!! Form::input('text', 'legalLastNameP1', $udata->UserLegal[0]->lastName, ['class'=> 'form-control legalLastNameP1', 'id' => 'legalLastNameP1']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.function_lbl') }}</label>
                      {!! Form::input('text', 'legalFunctionP1',  $udata->UserLegal[0]->function, ['class'=> 'form-control legalFunctionP1', 'id' => 'legalFunctionP1']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.phone_lbl') }}</label>
                      {!! Form::input('text', 'legalPhoneP1', $udata->UserLegal[0]->phone, ['class'=> 'form-control legalPhoneP1', 'id' => 'legalPhoneP1']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.email_lbl') }}</label>
                      {!! Form::input('text', 'legalEmailP1', $udata->UserLegal[0]->email, ['class'=> 'form-control legalEmailP1', 'id' => 'legalEmailP1']) !!}
                  </div>

                  <h3>{{ trans('client.person2_lbl') }}</h3>

                  {!! Form::input('hidden', 'legalIdP2', $udata->UserLegal[1]->id, ['class'=> 'form-control legalIdP2', 'id' => 'legalIdP2']) !!}

                  <div class="form-group">
                      <label>{{ trans('client.name_lbl') }}</label>
                      {!! Form::input('text', 'legalNameP2', $udata->UserLegal[1]->name, ['class'=> 'form-control legalNameP2', 'id' => 'legalNameP2']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.last_lbl') }}</label>
                      {!! Form::input('text', 'legalLastNameP2', $udata->UserLegal[1]->lastName, ['class'=> 'form-control legalLastNameP2', 'id' => 'legalLastNameP2']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.function_lbl') }}</label>
                      {!! Form::input('text', 'legalFunctionP2', $udata->UserLegal[1]->function, ['class'=> 'form-control legalFunctionP2', 'id' => 'legalFunctionP2']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.phone_lbl') }}</label>
                      {!! Form::input('text', 'legalPhoneP2', $udata->UserLegal[1]->phone, ['class'=> 'form-control legalPhoneP2', 'id' => 'legalPhoneP2']) !!}
                  </div>

                  <div class="form-group">
                      <label>{{ trans('client.email_lbl') }}</label>
                      {!! Form::input('text', 'legalEmailP2', $udata->UserLegal[1]->email, ['class'=> 'form-control legalEmailP2', 'id' => 'legalEmailP2']) !!}
                  </div>

                </fieldset>

                <div class="form-group" id="g_phone">
                    <label>{{ trans('client.phone_lbl') }}</label>
                    {!! Form::input('text', 'phone', $udata->phone, ['class'=> 'form-control phone', 'id' => 'phone']) !!}
                </div>

                <div class="form-group" id="g_email">
                    <label>{{ trans('client.email_lbl') }}</label>
                    {!! Form::input('email', 'email', $udata->email, ['class'=> 'form-control email', 'id' => 'emailClientCompany', 'autocomplete' => 'false', 'readonly' => 'true']) !!}
                </div>

                <div class="form-footer">
                  <button type="submit" class="btn btn-primary" name="button">{{ trans('client.btn_update') }}</button>
                </div>

              </form>
              @endif
      </div>
    </div>
  </div>
@endsection

<script>

        $('.datepicker').datetimepicker({
          format: 'Y-MM-DD',
          allowInputToggle: true,
          //minDate: moment(),
        });
</script>	