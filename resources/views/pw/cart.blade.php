@extends('app')
@section('content')

  <div class="row">
    <div class="col-md-9">
        <h2>{{ trans('cart.cart_title') }}</h2>
    </div>
    <div class="col-md-3" style="text-align: right">
    </div>
  </div>

  <div class="card">
    <div class="card-head style-default">
      <header>{{ trans('client.search_title') }}</header>
    </div>
    <div class="card-body">
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findOrder" id="findOrder" class="form-control" value="" required="required" placeholder="{{ trans('cart.order_number') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findName" id="findName" class="form-control" value="" required="required" placeholder="{{ trans('cart.client') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-info btnSearchOrder">{{ trans('client.search_lbl') }}</button>
        </div>
      </div>
    </div>
  </div>

  <table class="table table-hover" id="tblOrders">
	   <thead>
       <tr>
         <th>
           {{ trans('cart.order_number') }}
         </th>
         <th>
           {{ trans('cart.client') }}
         </th>
         <th>
           Email
         </th>
         <th>
           {{ trans('cart.date') }}
         </th>
         <th>
           {{ trans('cart.actions') }}
         </th>

       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>

   <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
   
  <script>
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready

     $('#rentstartdb').datetimepicker({
         format: 'Y-MM-DD',
         allowInputToggle: true,
         //minDate: moment(),
       });

    $(document).on('click', '.btnSearchOrder', function(event) {
      cargaGridOrders();
    });

    function cargaGridOrders(){
            //uid = 0;
            name = $('#findName').val();
            order = $('#findOrder').val();

            showLoader();
            cad = 'name='+name+'&order='+order+'&_token='+token;
            $('#tblOrders').dataTable().fnDestroy();
            $('#tblOrders tbody').empty();
            $.ajax({
                type: "POST",
                url: "{{ route('getOrdersList') }}",
                data: cad,
                success:function(data) {
                    $('#tblOrders tbody').append(data);
                    hideLoader();
                    $('#tblOrders').dataTable({
                        "language": {
                            "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                            "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                            "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                            "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                            "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                            "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                            "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                            "sInfoPostFix":    "",
                            "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                            "oPaginate": {
                                "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                                "sLast":     "{{ trans('client.dt_last_lbl') }}",
                                "sNext":     "{{ trans('client.dt_next_lbl') }}",
                                "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                            },
                            "oAria": {
                                "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                                "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                            }

                        }
                    });

                }
            });
        }

        cargaGridOrders();

      function viewOrderDetail(orderId){

          //alert(orderId);


          cad = 'order_id='+orderId+'&_token='+token;

          $.ajax({
            url: '{{ route('showDetailOrder') }}',
            type: 'POST',
            data: cad
          })
          .done(function(data) {
            //console.info(data);
            if(data.success == false){
              toastr['error']("{{ trans('cart.order_unavailable') }}")
              return false;
            }
            //return false;
            showLoader();
            $('#tblOrderDetail #orderDetailBody').empty();
            $('#modalOrderDetail .subtotal').empty();
            $('#modalOrderDetail .vat').empty();
            $('#modalOrderDetail .total').empty();

            setTimeout(function(){
              $('#modalOrderDetail .orderNo').empty();
              $('#modalOrderDetail .orderNo').append(orderId);
              $('#tblOrderDetail #orderDetailBody').append(data);
              totals = 0;
              $('#orderDetailBody .totalItem').each(function(index) {
                //alert($(this).text());
                dir = $(this).attr('dir');
                //console.info(dir);
                setTimeout(function(){
                  //calculatePriceOnDetail(dir);
                  hideLoader();
                },2000);
                totals = totals + parseFloat($(this).text());
              });

              /*grandTotal = totals * 1.23;
              vat = grandTotal - totals;

              $('#modalOrderDetail .subtotal').text(totals.toFixed(2));
              $('#modalOrderDetail .vat').text(vat.toFixed(2));
              $('#modalOrderDetail .total').text(grandTotal.toFixed(2));*/

              $('#modalOrderDetail').modal('show');

              //SI ES CREDIT CARD OCULTA PAY DEPOSIT
				if($("#FlagCreditCard").val() == "1"){
					$(".btnPayDepositModal").hide();
				}
				else{
					$(".btnPayDepositModal").show();
				}
              
              //hideLoader();
            },1000);

            

            //console.log("success");
          })
          .fail(function() {
            //console.log("error");
          })
          .always(function() {
            //console.log("complete");
          });

          setTimeout(function(){
              if($("#depositoPagado").val() == 1){
              	$(".btnPayDepositModal").hide();
              }  
            },2000);

          
          //
        }

        $(document).on('change paste keyup', '.rentstartdb', function() {
          item = $(this).attr('dir');
          //console.info(item);
          calculatePriceOnDetail(item)
        })

        function calculatePriceOnDetail(v){

          countBoxes = 0;
          $('#orderDetailBody .storagewrap').each(function(index){
            countBoxes++;
          });

          if (countBoxes > 0) {

            desc1 = 0;
            desc2 = 0;
            desc3 = 0;
            nextMonth = 0;
            prepayMonths = parseInt($('#orderDetailBody .boxWrap'+v+' #months').text());
            price = parseFloat($('#orderDetailBody .boxWrap'+v+' #originalPrice').text());

            fecha = $('#orderDetailBody .boxWrap'+v+' .rentstartdb').val();

            res = fecha.split('/');
            y = res[2];
            m = res[1];
            d = res[0];

            //calculate
            firstDay = new Date(y, m, 1);
            lastDay = new Date(y, m + 1, 0);
            daysOfMonth = new Date(y, m, 0).getDate();
            today = moment(fecha, "DD/MM/YYYY");
            today = today.toDate();
            pricePerDay = price / daysOfMonth;
            //console.warn(price+' - '+pricePerDay);

            //daysToPay = Math.round(Math.abs((today.getTime() - lastDay.getTime())/(oneDay)));
            daysToPay = daysOfMonth - d ;
            totalCurrentMonth = (daysToPay + 1) * pricePerDay;
            //console.error('total: '+daysOfMonth+', hoy: '+d+', '+daysToPay+' * '+pricePerDay+' = '+totalCurrentMonth);
            //$('.tblCalcStorages #currentMonth'+v).text(totalCurrentMonth.toFixed(2));

            current = totalCurrentMonth;
            //deposit = parseFloat($('.tblCalcStorages #price'+v).text());
            term = parseInt($('#orderDetailBody .boxWrap'+v+' #termdb').val());
            prepay = parseInt($('#orderDetailBody .boxWrap'+v+' #prepaydb').val());
            cc = parseInt($('#orderDetailBody .boxWrap'+v+' #ccdb').val());
            insurance = parseInt($('#orderDetailBody .boxWrap'+v+' #insdb').val());
            ccp = 1.2;
            ins = 0;
/*
            alert(insurance);
/
            if (insurance >= 1 ) {
              ins = parseFloat({{ $insDb }});
            }

            alert(ins);
*/

			ins = insurance;
			
            //calculo del current insurence
            insurancePerDay = ins/daysOfMonth;
            currentInsurence = insurancePerDay * (daysToPay + 1);

            //obtenemo el porcentaje extra
            if(cc == 1){
              ccp = 0;
            }else{
              cc = 2;
              ccp = $('#orderDetailBody .boxWrap'+v+' #ccpdb').val() / 100;
              ccp = ccp + 1;
            }

            if (price <= 0 || isNaN(price)){
              price = 1;
            }
            /*if (current <= 0 || isNaN(current)){
              current = 1;
            }*/
            if (term <= 0 || isNaN(term)){
              term = 1;
            }
            if (prepay <= 0 || isNaN(prepay)){
              prepay = 0;
            }
            /*if (cc <= 0 || isNaN(cc)){
              cc = 2;
            }*/

            (term == 2) ?  desc1 = 5 : desc1 = desc1;
            (term == 3) ?  desc1 = 10 : desc1 = desc1;
            (term == 4) ?  desc1 = 15 : desc1 = desc1;

            (prepay == 1) ?  desc2 = 0 : desc2 = desc2;
            (prepay == 2) ?  desc2 = 0 : desc2 = desc2;
            (prepay == 3) ?  desc2 = 5 : desc2 = desc2;
            (prepay == 4) ?  desc2 = 10 : desc2 = desc2;
            (prepay == 5) ?  desc2 = 15 : desc2 = desc2;

            desc1a = current - ((desc1/100) * current);
            desc1m = price - ((desc1/100) * price);
            //console.info('desc1: '+desc1a+' / '+desc1m);

            desc2a = desc1a - ((desc2/100) * desc1a);
            desc2m = desc1m - ((desc2/100) * desc1m);
            //console.info('desc2: '+desc2a+' / '+desc2m);

            if(cc == 1){
              desc3a = desc2a - ((desc2/100) * ccp);
              desc3m = desc2m - ((desc2/100) * ccp);
              deposit = desc3m * 1;
            }else{
              desc3a = desc2a * ccp;
              desc3m = desc2m * ccp;
              deposit = desc3m ;
            }
            //console.info('desc3: '+desc3a+' / '+desc3m);
            //console.warn('sub: '+desc3a+' + '+deposit);
            totalPrepay = (desc3m * prepayMonths) + (ins * prepayMonths);
            //console.warn(desc3m+' - '+prepayMonths+' - '+ins);
            
            //SE QUITA EL DEPOSITO
            //sub = desc3a + deposit + ins + totalPrepay + currentInsurence;
            //sub = desc3a + ins + totalPrepay + currentInsurence;
            sub = desc3a + totalPrepay + currentInsurence;

            console.info('sub ==> ' +  desc3a + ' + ' + totalPrepay + ' + ' + currentInsurence);
            
            //console.info(desc3a+' - '+totalPrepay+' - '+sub);
            insdb = parseFloat({{ $insDb }});
            //get total + insurance
            //total = sub * insdb;
            //vat = total - sub;

            //nextMonthSub = desc3m + ins;
            nextMonthSub = desc3m + insdb;
            nextMonth = nextMonthSub * ((parseFloat({{ $vatDb }})/100)+1);


            $('#orderDetailBody .boxWrap'+v+' #nextMonths').text(nextMonth.toFixed(2));

            $('#orderDetailBody .boxWrap'+v+' .totalItem').text(sub.toFixed(2));
            //$('#orderDetailBody .boxWrap'+v+' #totalItem'+v).text(sub.toFixed(2));

          }

          totals = 0;
          $('#orderDetailBody .totalItem').each(function(index) {
            //alert($(this).text());
            ti = parseFloat($(this).text());
            totals = totals + ti;
            //console.info(ti);
          });

          console.info(totals);

          $('#modalOrderDetail .subtotal').text(totals.toFixed(2));
          vat = totals * 0.23;
          $('#modalOrderDetail .vat').text(vat.toFixed(2));
          grandTotal = totals + vat;
          $('#modalOrderDetail .total').text(grandTotal.toFixed(2));

/*
          $("#subtotal_estimator").val(totals.toFixed(2));
          $("#vat_estimator").val(vat.toFixed(2));
          $("#total_estimator").val(grandTotal.toFixed(2));
  */       
        }

        $(document).on('click', '.viewOrder', function() {
          orderId = $(this).attr('id');
          viewOrderDetail(orderId);
        });


        @if ($idOrder > 0)
          orderId = <?php echo $idOrder; ?> ;
          viewOrderDetail(orderId);
        @else
          orderId = 0;
        @endif

        $(document).on('click', '.btnPayOrderModal', function() {
            if($("#creditCardFlag").val() == "1"){

                $("#cboPaymentType").children().each(function() {
                    if ($(this).val() != "2") {
                        $(this).prop('disabled',true);
                    }
                });

                $("#cboPaymentType").val("2");
            }else{

                $("#cboPaymentType").children().each(function() {
                    if ($(this).val() == "2") {
                        $(this).prop('disabled',true);
                    }
                });

                $("#cboPaymentType").val("1");
            }

            $('#modalPayment .wrapPaymentType').show();
            $('#modalPayment .wrapManualPay').hide();
            $('#modalPayment .wrapBraintree').hide();
            $('#modalPayment .wrapTransfer').hide();

            amountPayment = parseFloat($('#modalOrderDetail .total').text());

            $('.wrapManualPay').hide();
            $('.wrapPaymentType').show();
            $('#nextPay').show();
            $('#modalOrderDetail').modal('hide');
            $('#modalPayment #orderId').val(orderId);
            $('#modalPayment #amountPayment').val(amountPayment);
            $('#modalPayment #amount').val(amountPayment);

            showLoader();

            /*cad = '_token='+token+'&order_id='+orderId;
            $.ajax({
            url: '{{ route('getOrderData') }}',
            type: 'POST',
            data: cad
            })
            .done(function(data) {
            $('#modalBraintree2 #clientName').empty();
            $('#modalBraintree2 #clientName').append(data.client);
            $('#modalBraintree2 #id_client').val(data.idClient);
            $('#modalBraintree2 #id_order').val(orderId);
            $('#modalBraintree2 #_token').val(token);
            console.log("success");
            })
            .fail(function() {
            console.log("error");
            })
            .always(function() {
            console.log("complete");
            });*/

            hideLoader();

            //amount = parseFloat($('#modalOrderDetail .total').text());
            //$('#modalBraintree2 #price').val(amount.toFixed(2));

            var flag_paid = $(this).data('paid');

            $('#modalPayment #flag_paid').val(flag_paid);

            if(flag_paid == "0"){
                //SE GUARDA COMO NO PAGADO
                //	ENVIA A PAGO CON FlAG PAID 0
				/////alert($("#modalPayment form").attr("href"));
                $("#modalPayment form").trigger("submit");

            }else{
                //VA A VENTANA DE PAGO

                $('#modalPayment').modal('show');
            }

        });

        $(document).on('click', '.btnPayDepositModal', function(event) {
			event.preventDefault();
			
			$("#modal-generico-small").modal("show");
			
			var route = "{{ route('depositos.pagar.create',['*deposit_id*']) }}";
			route = route.replace("*deposit_id*", $("#idDeposito").val());

			$("#modal-generico-small-content").load(route);

        });

        
       
        $(document).on('click', '.delOrder', function() {
          showLoader();
          console.info('Del order!');
          idOrder = $(this).attr('id');
          cad = 'idOrder='+idOrder;
          setTimeout(function(){
            if (confirm('{{ trans('cart.sure_to_delete_order') }}')) {
              console.info('yes!');
              $.ajax({
                url: '{{ route('wsDeleteOrder') }}',
                type: 'POST',
                data: cad,
              })
              .done(function(data) {
                if (data.success) {
                  toastr["success"]('{{ trans('cart.done') }}');
                }else{
                  toastr["success"]('{{ trans('cart.wrong') }}');
                }
                hideLoader();
                getListStoragesClient(userId);
                console.log(data);
              })
              .fail(function() {
                console.log("error");
                  toastr["success"]('{{ trans('cart.wrong') }}');
              })
              .always(function() {
                console.log("complete");
              });
              cargaGridOrders();
              hideLoader();
            }else{
              cargaGridOrders();
              hideLoader();
              //getListStoragesClient(userId);
            };
          }, 500);
        })

      });

  </script>
@endsection
