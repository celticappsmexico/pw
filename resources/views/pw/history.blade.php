@extends('app')
@section('content')

  <div class="row">
    <div class="col-md-9">
        <h2>{{ trans('history.historypage_title') }}</h2>
    </div>
    <div class="col-md-3" style="text-align: right">
    </div>
  </div>

  <div class="card">
    <div class="card-head style-default">
      <header>{{ trans('client.search_title') }}</header>
    </div>
    <div class="card-body">
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findUsername" id="findUsername" class="form-control" value="" required="required" placeholder="{{ trans('client.username_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findName" id="findName" class="form-control" value="" required="required" placeholder="{{ trans('client.name_lbl') }}" title="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findCompany" id="findCompany" class="form-control" value="" required="required" placeholder="{{ trans('client.company_lbl') }}" title="">
        </div>
      </div>

       <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findAlias" id="findAlias" class="form-control" value="" required="required" placeholder="{{ trans('storages.alias') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findSqm" id="findSqm" class="form-control" value="" required="required" placeholder="{{ trans('storages.sqm') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findPrice" id="findPrice" class="form-control" value="" required="required" placeholder="{{ trans('storages.price') }}" title="">
        </div>
      </div>

      <div class="col-md-8" style="border: dashed 1px #ccc; padding: 10px;">
        <label>{{ trans('storages.date_range') }}</label>
        <div class="input-daterange input-group" id="demo-date-range">
          <div class="input-group-content">
            <input type="text" class="form-control" name="findStart" id="findStart" placeholder="{{ trans('storages.start') }}">
          </div>
          <span class="input-group-addon">{{ trans('storages.to') }}</span>
          <div class="input-group-content">
            <input type="text" class="form-control" name="findEnd" id="findEnd" placeholder="{{ trans('storages.end') }}">
            <div class="form-control-line"></div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-info btnSearchClients">{{ trans('client.search_lbl') }}</button>
        </div>
      </div>
    </div>
  </div>

  <table class="table table-hover" id="tblHistory">
	   <thead>
       <tr>
        <th>
           {{ trans('client.storage') }}
         </th>
         <th>
           {{ trans('client.sqm') }}
         </th>
         <th>
           {{ trans('client.price') }}
         </th>
         <th>
           {{ trans('client.username_lbl') }}
         </th>
         <th>
           {{ trans('client.name_lbl') }}
         </th>
         <th>
           {{ trans('client.company_lbl') }}
         </th>
         <th>
           {{ trans('client.rent_start') }}
         </th>
         <th>
           {{ trans('client.rent_end') }}
         </th>
         <th>
           User Modifies
         </th>
       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>

  <script>

  jQuery(document).ready(function($) {
        $('#findStart').datetimepicker({
          format: 'D/M/Y',
          allowInputToggle: true,
          //minDate: moment(),
        });

        $('#findEnd').datetimepicker({
          format: 'D/M/Y',
          allowInputToggle: true,
          //minDate: moment(),
        });
  });

  $(document).on('click', '.btnSearchClients', function(event) {
    cargaGridHistory();
  });

  function cargaGridHistory(){
          username = $('#findUsername').val();
          name = $('#findName').val();
          name_splitted = name.split(" ");
          name = name_splitted[0];
          if (name_splitted[1] != null){
            lastName = name_splitted[1];
          }else {
            lastName = '';
          }
          company = $('#findCompany').val();
          alias = $('#findAlias').val();
          sqm = $('#findSqm').val();
          price = $('#findPrice').val();
          rent_start = $('#findStart').val();
          rent_end = $('#findEnd').val();

          if (rent_start != '' && rent_end == '') {
            toastr['error']('{{ trans('storages.invalid_range') }}');
            return false;
          }

          if (rent_start == '' && rent_end != '') {
            toastr['error']('{{ trans('storages.invalid_range') }}');
            return false;
          }
          
          showLoader();
          
          cad = 'username='+username+'&name='+name+'&lastName='+lastName+'&company='+company+'&alias='+alias+'&sqm='+sqm+'&price='+price+'&rent_start='+rent_start+'&rent_end='+rent_end+'&_token='+token;
          $('#tblHistory').dataTable().fnDestroy();
          $('#tblHistory tbody').empty();
          console.info(cad);

          $.ajax({
              type: "POST",
              url: "{{ route('getHistory') }}",
              data: cad,
              success:function(data) {
                  $('#tblHistory tbody').append(data);
                  hideLoader();
                  $('#tblHistory').dataTable({
                      "language": {
                          "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                          "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                          "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                          "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                          "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                          "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                          "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                          "sInfoPostFix":    "",
                          "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                          "oPaginate": {
                              "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                              "sLast":     "{{ trans('client.dt_last_lbl') }}",
                              "sNext":     "{{ trans('client.dt_next_lbl') }}",
                              "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                          },
                          "oAria": {
                              "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                              "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                          }

                      }
                  });

                  //$('#load').slideUp(100);
              }
          });
      }

      cargaGridHistory();
  </script>
@endsection
