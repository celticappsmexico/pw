<div class="row" style="margin-bottom: 2%;">
    <canvas id="line-chart" width="800" height="250"></canvas>
</div>
<div class="row">
    <table class="table table-hover no-footer dataTable" id="tableBillingMonth">
        <thead>
            <tr>
                <th>{{ trans('billing_reports.month') }}</th>
                <th>{{ trans('billing_reports.billed') }}</th>
                <th>{{ trans('billing_reports.payed') }}</th>
                <th>{{ trans('billing_reports.balance') }}</th>
                <th>{{ trans('billing_reports.detail') }}</th>
            </tr>
        </thead>
        @foreach($meses as $mes)
            <tr>
                <td>{{ trans('billing_reports.'.$mes->monthName) }}</td>
                <td>{{ number_format($mes->sum_cargos, 2) }} pln</td>
                <td>{{ number_format($mes->sum_abonos, 2) }} pln</td>
                <td>{{ number_format($mes->balance, 2) }} pln</td>
                <td>
                    <button type="button" class="btn btn-info btn-lg clientDetail" id="{{ $mes->mes }}">{{ trans('billing_reports.detail') }}</button>
                </td>
            </tr>
        @endforeach
        <tr>
            <td>{{ trans('billing_reports.total') }}</td>
            <td>{{ $totalCharges }} pln</td>
            <td>{{ $totalAmounts }} pln</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>

<!-- JS  -->
<script src="{{ asset('assets/js/Chart.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script type="text/javascript">
    var month = 0;
    $(document).ready(function() {
        $('#tableBillingMonth').DataTable( {
            "pagingType": "full_numbers",
            "order": [ 4, 'desc' ],
            "bLengthChange": false,
            "iDisplayLength": 13,
            "bInfo": false,
            "bPaginate": false,
        } );
    } );
    $(document).on('click', '.clientDetail', function(){
        month = $(this).attr('id');
        year = $('#inputAño').val();
        levels = $('#select-sucursales').val();
        getClients(month, year, levels);
    });

    function getClients(month, year, levels){
      route = '{{ route('filterClient', ['*year*', '*month*', '*levels*']) }}';
      route = route.replace('*year*', year);
      route = route.replace('*month*', month);
      route = route.replace('*levels*', levels);
      window.location.href = route;
    }

    new Chart(document.getElementById("line-chart"), {
      type: 'line',  data: {
        labels: [
          '{{$January}}', '{{$February}}', '{{$March}}', '{{$April}}', '{{$May}}', '{{$June}}', '{{$July}}', '{{$August}}', '{{$September}}', '{{$October}}', '{{$November}}', '{{$December}}'
        ],
        datasets: [{
            data: [{{implode(',', $meses->lists('sum_cargos')->toArray())}}],
            label: '{{$traCharge}}',
            borderColor: getRandomColor(),
            fill: false
        },
        {
            data: [{{implode(',', $meses->lists('sum_abonos')->toArray())}}],
            label: '{{$traAmount}}',
            borderColor: getRandomColor(),
            fill: false
        }
      ]},
      options: {
        title: {
          display: true,
          text: '{{$traBillingReports}}'
        },
        scales: {
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: '{{$traMonth}}'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: '{{$traCharge}}, {{$traAmount}}',
            }
          }]
        },
        tooltips: {
          mode: 'index'
        }
      }
    });
</script>
