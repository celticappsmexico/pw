@extends('app')
@section('content')
  <div class="divDetailBilling">
      <div class="row">
          <button type="button" class="btn btn-info btn-lg btnBack" name="button" style="margin-left: 1%; margin-bottom: 0.5%;">Back</button>
      </div>
      <table class="table table-hover no-footer dataTable" id="tableBillingDetail">
          <thead>
              <tr>
                  <th>{{ trans('billing_reports.name') }}</th>
                  <th>{{ trans('billing_reports.billed') }}</th>
                  <th>{{ trans('billing_reports.payed') }}</th>
                  <th>{{ trans('billing_reports.balance') }}</th>
                  <th>{{ trans('billing_reports.detail') }}</th>
              </tr>
          </thead>
          @foreach($clients as $client)
              <tr>
                  <td>{{ $client->user->name }}</td>
                  <td>{{ $client->sumCargos }} pln</td>
                  <td>{{ $client->sumAbonos }} pln</td>
                  <td>{{ $client->balance }} pln</td>
                  <td>
                      <button type="button" class="btn btn-info btn-lg clientCompleteDetail" id="{{ $client->user_id }}" dirYear="{{ $year }}" dirMonth="{{ $month }}">{{ trans('billing_reports.detail') }}</button>
                  </td>
              </tr>
          @endforeach
          <tr>
              <td>{{ trans('billing_reports.total') }}</td>
              <td>{{ $totalCharges }} pln</td>
              <td>{{ $totalAmounts }} pln</td>
              <td></td>
              <td></td>
          </tr>
      </table>
  </div>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#tableBillingDetail').DataTable( {
              "pagingType": "full_numbers",
              "order": [ 4, 'desc' ],
          } );
      });
    $('.clientCompleteDetail').click(function(){
        // alert($(this).attr('dirMonth'));
        var idClient = $(this).attr('id');
        var year = $(this).attr('dirYear');
        var month = $(this).attr('dirMonth');
        getClientDetails(idClient, year, month);
    });

    $('.btnBack').click(function(){
        window.history.back();
    });

    function getClientDetails(id, year, month){
      route = '{{ route('getBillingClientDetails', ['*id*', '*year*', '*month*']) }}';
      route = route.replace('*id*', id);
      route = route.replace('*year*', year);
      route = route.replace('*month*', month);
    //   alert(route);
      window.location.href = route;
    }
  </script>
@endsection
