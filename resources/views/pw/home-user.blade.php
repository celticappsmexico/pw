@extends('app')

@section('content')

<div class="containter">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="col-md-6">
                    <a href="javascript:void();">
                        <img src="{{asset('assets/img/storage-icon.png')}}" alt="" />
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="{{route('sizeCalculator')}}">
                        <img src="{{asset('assets/img/calculator-icon.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
