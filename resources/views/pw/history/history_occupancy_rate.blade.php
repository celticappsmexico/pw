@extends('app')
@section('content')
<style media="screen">
    .toggle {
        display: block;
        position: absolute;
        width: 30%;
        background-color: rgba(255, 255, 255, 0.8);
        padding: 15px;
        -webkit-transition: ease-in 0.5s all;
        transition: ease-in 0.5s all;
        -webkit-transform: translateY(-200%);
        -ms-transform: translateY(-200%);
        transform: translateY(-200%);
        min-width: 320px;
        z-index:1000;
        opacity:0;
        filter: alpha(opacity=0); /* For IE8 and earlier */
    }
    .toggle--active {
        opacity: 1;
        margin-top: 10px;
        position: absolute;
        filter: alpha(opacity=100); /* For IE8 and earlier */
        -webkit-transition: ease-in 0.5s all;
        transition: ease-in 0.5s all;
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
        z-index:100;
    }
    .divHistoryContent {
        height: 800px;
        width: 800px;
    }
</style>

<div class="row">
  <div class="col-md-1">
    <button type="button" class="tcon tcon-menu--xcross" aria-label="toggle menu" id="btnFilterMenu">
      <span class="tcon-menu__lines" aria-hidden="true"></span>
      <span class="tcon-visuallyhidden">toggle menu</span>
    </button>
    <div class="toggle">
        <select id="select-sqm" multiple="multiple" style="display: none">
            @foreach ($warehouses as $warehouse)
                @foreach ($warehouse->sqms as $sqms)
                <option value="" data-section="{{$warehouse->warehouse}}">
                    {{ $sqms->sqm }}
                </option>
                @endforeach
            @endforeach
        </select>
    </div>
  </div>
  <div class="col-md-1">
    <div class="input-group año" style="width: 100px; border-style: solid; border-width:1px;">
      <span class="input-group-addon" style="border-right-style:solid; border-width:1px; border-right-color:black;">
        <i class="glyphicon glyphicon-calendar" style="margin-left:8px;"></i>
      </span>
        <input class="date-own form-control" style="width: 80px; text-align: center;" type="text" id="inputAño" name="inputYear" value="{{$year}}">
    </div>
  </div>
  <div class="col-md-10">
    <button type="button" id="btnFiltrar" class="btn btn-secondary" style="margin-left:20px; border-style:solid; border-width:1px; border-color:black; font-size:15.5px;">{{ trans('billing_reports.search') }}</button>
  </div>
</div>

<div class="divHistoryContent">

</div>

<!-- JS  -->
<!-- JS  -->
<script src="{{ asset('assets/js/jquery.tree-multiselect.min.js') }}"></script>
<script src="{{ asset('assets/js/transformicons.min.js') }}"></script>
<script type="text/javascript">

    // var sqm = '';

    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth();
    var d = d.getDay();

    $(function() {
        $('#inputAño').datepicker({
            minViewMode: 2,
            format: 'yyyy',
        });
    });

    $("#select-sqm").treeMultiselect({
        allowBatchSelection: false,
        startCollapsed: true,
    });

    // $('#select-sqm').change(function() {
    //     sqm = $('#select-sqm option:selected').text();
    //     sqm = $.trim(sqm);
    //     warehouse = $('#select-sqm option:selected').attr('data-section');
    //     warehouse = warehouse.replace(/\s+/g, '_');
    //     console.log(warehouse);
    // });

    // $(document).ready(function(){
    //     filtrar(sqm, warehouse);
    // });

    $('.tcon-menu--xcross').click(function (){
        var classBtn = $(this).attr('class');
        if (classBtn == 'tcon tcon-menu--xcross') {
            $(this).attr('class', 'tcon tcon-menu--xcross tcon-transform');
            $('.toggle').toggleClass('toggle--active');
        }else {
            $(this).attr('class', 'tcon tcon-menu--xcross');
            $('.toggle').attr('class', 'toggle');
        }
    });

    $( "#btnFiltrar" ).click(function() {

        var sqm = [];
        var warehouse = [];
        $("#select-sqm option:selected").each(function() {
            sqm.push($.trim($(this).text()));
            warehouse.push(($(this).attr('data-section')).replace(/\s+/g, '_'));
            // console.log(sqm);
        });
        // warehouse = $('#select-sqm option:selected').attr('data-section');
        // warehouse = warehouse.replace(/\s+/g, '_');

        // alert(sqm);
        if (sqm.length > 0) {
            filtrar(sqm, warehouse);
        }else {
            $('.divHistoryContent').html('');
            $('.tcon-menu--xcross').attr('class', 'tcon tcon-menu--xcross');
            $('.toggle').attr('class', 'toggle');
            toastr["error"]("{{ trans('history_occupancy_rate.errorSqm') }}");
        }
    });

    /**
    * Function to filter the history with a specific year, sqm and warehouse
    */
    function filtrar(sqm, warehouse){
        console.log(warehouse);
        console.log(sqm);

        $('.tcon-menu--xcross').attr('class', 'tcon tcon-menu--xcross');
        $('.toggle').attr('class', 'toggle');

        route = "{{ route('filterHistoryOccupancyRate', ['*year*', '*sqm*', '*warehouse*']) }}";

        year = $('#inputAño').val();
        sqm = sqm;
        warehouse = warehouse;

        route = route.replace('*year*', year);
        route = route.replace('*sqm*', sqm);
        route = route.replace('*warehouse*', warehouse);

        $('.divHistoryContent').html('');
        $('.divHistoryContent').load(route);
    }
</script>

@endsection
