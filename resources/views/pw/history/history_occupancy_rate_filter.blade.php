@foreach($records as $key => $record)
    <div class="row" style="margin-top: 20px;">
        <canvas id="line-chart-{{$key}}" width="800" height="250"></canvas>
    </div>
@endforeach

<script src="{{ asset('assets/js/Chart.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script type="text/javascript">
@foreach($records as $key => $record)
    new Chart(document.getElementById("line-chart-{{$key}}"), {
      type: 'line',  data: {
        labels: [
          '{{$January}}', '{{$February}}', '{{$March}}', '{{$April}}', '{{$May}}', '{{$June}}', '{{$July}}', '{{$August}}', '{{$September}}', '{{$October}}', '{{$November}}', '{{$December}}'
        ],
        datasets: [{
          data: [{{$record["occupancy_rate"]}}],
          label: '{{ trans('history_occupancy_rate.occupancy_rate') }}',
          borderColor: getRandomColor(),
          fill: false
        }
      ]},
      options: {
        title: {
          display: true,
          text: '{{ trans('history_occupancy_rate.history_occupancy_rate') }} - {{ trans('history_occupancy_rate.sqm') }}: {{$record["sqm"]}} - {{ trans('history_occupancy_rate.warehouse') }}: {{$record["warehouse"]}}'
        },
        scales: {
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: '{{$traMonth}}'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: '{{ trans('history_occupancy_rate.occupancy_rate') }}',
            }
          }]
        },
        tooltips: {
          mode: 'index'
        }
      }
    });
@endforeach

</script>
