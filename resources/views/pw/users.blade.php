@extends('app')
@section('content')
<style>
    .toggle2 {
        display: block;
        position: absolute;
        width: 30%;
        background-color: rgba(255, 255, 255, 0.8);
        padding: 15px;
        -webkit-transition: ease-in 0.5s all;
        transition: ease-in 0.5s all;
        -webkit-transform: translateY(-200%);
        -ms-transform: translateY(-200%);
        transform: translateY(-200%);
        min-width: 320px;
        z-index:1000;
        opacity:0;
        filter: alpha(opacity=0); /* For IE8 and earlier */
    }
    
    .toggle--active {
	  opacity: 1;
	  filter: alpha(opacity=100); /* For IE8 and earlier */
	  -webkit-transition: ease-in 0.5s all;
	  transition: ease-in 0.5s all;
	  -webkit-transform: translateY(0);
	  -ms-transform: translateY(0);
	  transform: translateY(0);
	  z-index:100;
	}
		
		.toggle-menu { margin-bottom: 25px; }
		
		.toggle-menu li {
		  width: 25%;
		  display: block;
		  margin: 10px auto;
		}
		
		
		
.panel-success > .panel-heading {
  background-color:#3C763D;
  border-color:#3C763D;
  color:#fff;
}

.tabla-resumen tr{
	border-bottom:1px dashed #3C763D;
}	
.tabla-resumen th {
	text-align:center;
	font-size: 14px
}
.tabla-resumen td {
	padding: 5px;
	font-size: 13px
}
.tabla-resumen td:first-child{
	text-align:right;
	font-weight: normal;
}
.tabla-resumen td:last-child{
	text-align:left;
	font-weight: bold;
}
		    
    
    
</style>

  <div class="row">
    <div class="col-md-3">
        <h2>{{ trans('client.clientpage_title') }}</h2>
    </div>
    
    <div class="col-md-9" style="text-align: right">
		<a href="{{route('contracts.create')}}" id="btnAppContracts" class="btn btn-success">
    		<i class="fa fa-mobile"></i> App Contracts
    	</a>
    	

		<a href="{{route('correspondence.create')}}" id="btnSendCollectiveCorrespondence" class="btn btn-warning">
    		<i class="fa fa-envelope"></i> COLLECTIVE CORRESPONDENCE
    	</a>
    	
    	<a href="{{route('notification.create')}}" id="btnSendNotifications" class="btn btn-info">
    		<i class="fa fa-mobile"></i> SEND NOTIFICATIONS 
    	</a>
      	<a href="javascript:void(0);" class="btn btn-primary btnNewClient">
      		<i class="fa fa-user-plus"></i> {{ trans('client.newUser_btn') }}</a>
    	</div>
   
  </div>

  <div class="card">
    <div class="card-head style-default">
      <header>{{ trans('client.search_title') }}</header>
    </div>
    <div class="card-body">
    	<div class="col-md-4 checkbox checkbox-styled tile-text" >
			<label style="font-size:18px">
				<input name="chkTenant" id="chkTenant" type="checkbox" />
                {{ trans('client.tenants') }}
			</label>
    	</div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findUsername" id="findUsername" class="form-control" value="" required="required" placeholder="{{ trans('client.username_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findMail" id="findMail" class="form-control" value="" required="required" placeholder="{{ trans('client.email_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findName" id="findName" class="form-control" value="" required="required" placeholder="{{ trans('client.name_lbl') }}" title="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findLastname" id="findLastname" class="form-control" value="" required="required" placeholder="{{ trans('client.last_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findCompany" id="findCompany" class="form-control" value="" required="required" placeholder="{{ trans('client.company_lbl') }}" title="">
        </div>
      </div>
      <!-- 
      <div class="col-md-4 checkbox checkbox-styled tile-text" >
			<label style="font-size:18px">
				<input name="chkNotPaid" id="chkNotPaid" type="checkbox" value="1" />
                {{ trans('client.not_paid') }}
			</label>
    	</div>
    	-->
    	<div class="col-md-4 checkbox checkbox-styled tile-text" >
			<label style="font-size:18px">
				<input name="chkWithNotice" id="chkWithNotice" type="checkbox" value="1" />
                {{ trans('client.with_notice') }}
			</label>
    	</div>
		<div class="col-md-4">
			<div class="form-group">
				<select id="platform-select" class="form-control">
					<option value="">--Select Platform --</option>
					<option value="Web">Web User</option>
					<option value="App">Android or iOs</option>
				</select>

			</div>
		</div>
      <div class="col-md-4">
	      	<button type="button" class="tcon tcon-menu--xcross" aria-label="toggle2 menu" id="btnFilterMenu">
		      <span class="tcon-menu__lines" aria-hidden="true"></span>
		      <span class="tcon-visuallyhidden">toggle menu</span>
		    </button>
		    <div class="toggle2">
		        <select id="select-sqm" multiple="multiple" style="display: none">	
					@foreach ($warehouses as $item) 
						<option value="{{$item->id}}" data-section="{{$item->warehousename}}">{{$item->name}}</option>
					@endforeach
			    </select>
		        
		    </div>
		  </div>
      </div>
	  <div class="col-md-4 checkbox checkbox-styled tile-text" >
			<label style="font-size:18px">
				<input name="chkBTBalance" id="chkBTBalance" type="checkbox" value="1" />
                Show Braintree Balance
			</label>
    	</div>
      <div class="col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-info btnSearchClients">{{ trans('client.search_lbl') }}</button>
        </div>
      </div>
    </div>
  </div>

  <table class="table table-hover" id="tblUSers">
	   <thead>
       <tr>
       		<th>ID</th>
         <th>
           {{ trans('client.name_lbl') }}
         </th>
         <th>
           {{ trans('client.last_lbl') }}
         </th>
         <th>
           {{ trans('client.type_lbl') }}
         </th> 	
         <th>
           {{ trans('client.company_lbl') }}
         </th>
         <th>
           {{ trans('client.email_lbl') }}
         </th>
		 <th>
		 	Platform
		 </th>
         <th>
           {{ trans('client.balance') }}
         </th>
		 <th>
		 	Braintree Balance
		 </th>
		 <th>
		 	Customer BT
		 </th>
         <th>
           {{ trans('client.actions_lbl') }}
         </th>
       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>
   
    
<script src="{{ asset('assets/js/jquery.tree-multiselect.min.js') }}"></script>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    
<script type="text/javascript">

  $(document).on('click', '.btnSearchClients', function(event) {
    //cargaGridUsers();
	    oTableUsers.draw();
  });
	  
  $('.tcon-menu--xcross').click(function (){
      var classBtn = $(this).attr('class');
      if (classBtn == 'tcon tcon-menu--xcross') {
          $(this).attr('class', 'tcon tcon-menu--xcross tcon-transform');
          $('.toggle2').toggleClass('toggle--active');
      }else {
          $(this).attr('class', 'tcon tcon-menu--xcross');
          $('.toggle2').attr('class', 'toggle2');
      }
  });

  $("#select-sqm").treeMultiselect({
      allowBatchSelection: false,
      startCollapsed: true,
  });

//Oculta seccion de selected
	$(".selected, .collapse-section").hide();

  

	var oTableUsers = $('#tblUSers').DataTable({
		"bInfo" : true,
		processing: true,
		serverSide: true,
		bFilter : true,
		ajax: {
			url : "{!! route('users.data') !!}",
			data: function (d) {
				d.username = $('#findUsername').val();
		        d.email = $('#findMail').val();
		        d.name = $('#findName').val();
		        d.last = $('#findLastname').val();
		        d.company = $('#findCompany').val();
		        d.chkTenant = $('#chkTenant').is(':checked');
		        d.chkNotPaid = $('#chkNotPaid').is(':checked');
		        d.chkWithNotice = $('#chkWithNotice').is(':checked');
		        d.level = $("#select-sqm").val();
				d.platform = $("#platform-select").val();
				d.chkBTBalance = $("#chkBTBalance").is(':checked');
			}
		},
		columns: [
			{ data: 'Userid', name: 'Userid'},
			{ data: 'NombreUsuario', name: 'NombreUsuario'},
			{ data: 'lastName', name: 'lastName'},
			{ data: 'TipoUsuario', name: 'TipoUsuario'},
			{ data: 'companyName', name: 'companyName'},
			{ data: 'email', name: 'email'},
			{ data: 'platform', name: 'platform'},
			{ data: 'balance', name: 'balance'},
			{ data: 'braintree_balance', name:'braintree_balance'},
			{ data: 'customer_id', name:'customer_id'},
			{ data: 'actions', name: 'actions', searchable:false, orderable:false },
						
		],
		language : {
			lengthMenu: "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
			zeroRecords: "{{ trans('client.dt_noResults_lbl') }}",
			info: "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
			infoEmpty: "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
			infoFiltered: "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
			search:         "{{ trans('client.dt_find_lbl') }}:",
			paginate: {
				first:      "{{ trans('client.dt_first_lbl') }}",
				last:       "{{ trans('client.dt_last_lbl') }}",
				next:       "{{ trans('client.dt_next_lbl') }}",
				previous:   "{{ trans('client.dt_prev_lbl') }}"
			},
			loadingRecords: "{{ trans('client.dt_loading_lbl') }}",
			processing:     "{{ trans('client.dt_processing_lbl') }}",
		}
	});

	

	$(document).on("click", ".btn-pay-now", function(event){
		event.preventDefault();

		$("#modal-generico-small").modal("show");
		$("#modal-generico-small-content").load( $(this).attr('href') );
	});


	$(document).ready(function() {
		@if($id != "")
			 getListStoragesClient("{{$id}}");

			$("#idClient").val("{{$id}}");
		 
		@endif

		/*
		function getListStoragesClient(idClient){
			
		    $('.listStorages').empty();
		
		    cad = 'idClient='+idClient+'&_token='+token;
		
		    showLoader();
		    $('#modalClientBoxes').modal('show');
		    //$('#modalAddBox').modal('show');
		    //$('#modalClientBoxes .modal-body').append(token);
		    $('.modalAddBoxFrm1')[0].reset();
		    $('.modalAddBoxFrm2').hide();
		    $('.modalAddBoxFrm1').show();
		
		    $.ajax({
		      url: '{{ route('getUserStorages') }}',
		      type: 'POST',
		      data: cad,
		    })
		    .done(function(data) {
		      console.log("success");
		
		      if (data.success == true ) {
		      	//alert(data.saldo);
		      	//alert(data.saldo_formateado);
		      	
		      	if(Number(data.saldo) >= 0){
				 $('.listStorages').append(
						'<div class="alert alert-success" style="padding:0px">'+	
	      					'<h2 class="text-right">{{Lang::get("client.balance")}}: '+data.saldo_formateado +'</h2>'+
		      				'</div>' );
				 }else{
					 $('.listStorages').append(
						'<div class="alert alert-danger" style="padding:0px">'+	
							'<h2 class="text-right">{{Lang::get("client.balance")}}: '+data.saldo_formateado +'</h2>'+
		   				'</div>');
				 }
		      
		        i = 0;
		        $.each(data.storages, function(index,val) {
		        	
		        var balance = val.balance + " pln";
		        
		        $('.listStorages').append('<div class="col-xs-12 col-lg-12 hbox-xs">'+
		          '<div class="hbox-column width-2">'+
		          '<img class="img-circle img-responsive pull-left" src="{{ asset('assets/images/size-estimator/storage-box.png') }}" alt="">'+
		          '</div><div class="hbox-column v-top"><div class="clearfix"><div class="col-lg-12 margin-bottom-lg">'+
		          '<a class="text-lg text-medium">{{Lang::get('storages.box_number')}}: '+  val.alias+'</a><br/>'+
		          '<a class="text-lg text-medium"><b>{{Lang::get('client.balance')}}: '+ balance +'</b></a>'+
		          '</div></div><div class="clearfix opacity-75"><div class="col-md-6"><span class="glyphicon glyphicon-calendar text-sm"></span>&nbsp;'+val.rent_start+' // '+val.rent_end+' </div><div class="col-md-6"><b>{{Lang::get('storages.recurrent_billing')}}</b><span class="glyphicon glyphicon-usd text-sm"></span>&nbsp;'+val.price_per_month+' </div></div><div class="clearfix"><div class="col-lg-12"></div>'+
		          '<div class="col-lg-12">'+
			          '<button class="btn btn-danger btn-xs btnSendTermNotif" id="'+val.id+'" dir="'+val.user_id+'" '+termNotifCheck(val.rent_end,val.rent_start)+'>'+
			          	'<i class="fa fa-mail-forward"></i> {{ trans('storages.send_term') }} ' +
			          '</button>'+
			          
			          '<button type="button" class="btn btn-info btn-xs btnDownloadInvoice" id="'+val.id+'" dir="'+val.user_id+'"> '+
			          	'<i class="fa fa-cloud-download"></i> {{ trans('storages.down_invoice') }} '+
			          '</button>'+
			          
			          '<button type="button" class="btn btn-danger btn-xs detachStorage" id="'+val.id+'" dir="'+val.user_id+'">'+
			          	'<i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('storages.detach') }} '+
			          '</button><br/>'+
			          '<br/>'+
			          '<div class="alert alert-warning" style="padding:5px"> {{Lang::get('storages.term_of_notice')}} '+
			          		val.storage_payment_data[0].payment_term[0].months +
			          ' &nbsp;{{Lang::get('billing_reports.months')}}</div>'+
		          
		          '</div></div><div class="stick-top-right small-padding"><i class="fa fa-dot-circle-o fa-fw text-success" data-toggle="tooltip" data-placement="left" data-original-title="Active"></i></div></div><!--end .hbox-column --></div><br/>');
		          
		
		          i++;
		
		          //<i class="fa fa-money"></i>
		        });
		        
		        $('.listStorages').append('<hr/>');
		        
		        $.each(data.billingStorages, function(index,val) {
		        	var balance = val.balance + " pln";
		        	
		        	$('.listStorages').append('<div class="col-xs-12 col-lg-12 alert alert-warning" > '+
		        								'<div class="hbox-column width-2">'+
		        									'<img class="img-circle img-responsive pull-left" src="{{ asset('assets/images/size-estimator/storage-box.png') }}">'+
		        								'</div>'+
		        								
		        								'<div class="hbox-column v-top">'+
		        									'<div class="clearfix">'+
		        										'<div class="col-lg-12 margin-bottom-lg">'+
		        											'<a class="text-lg text-medium">{{Lang::get('storages.box_number')}}: '+  val.storages.alias +'</a><br/>'+
		        											'<a class="text-lg text-medium"><b>{{Lang::get('client.balance')}}: '+  balance+'</b></a>'+
		        											'</div>'+
		        									'</div>'+
		        									'<div class="clearfix opacity-75">'+
										          		'<div class="col-lg-6">'+
											          		'<button type="button" class="btn btn-info btn-xs btnDownloadInvoice" id="'+val.storage_id+'" dir="'+val.user_id+'"> '+
													          	'<i class="fa fa-cloud-download"></i> {{ trans('storages.down_invoice') }} '+
													        '</button>'+
											          	'</div>'+
											          	'<div class="stick-top-right small-padding">'+
											          		'<i class="fa fa-dot-circle-o fa-fw text-success" data-toggle="tooltip" data-placement="left" data-original-title="Active"></i></div></div></div></div><br/>');
		        });
		        
		      }else{
		        $('.listStorages').append('<h3>'+data.msj+'</h3>');
		      }
		
		    })
		    .fail(function() {
		      console.log("error");
		    })
		    .always(function() {
		      console.log("complete");
		    });
		
		
		    hideLoader();
		  };
			*/
	});

	function termNotifCheck(term,rentStart){
	
	    if( (new Date(term).getTime() > new Date(rentStart).getTime())){
	      return 'disabled';
	    }else{
	      return '';
	    }
	
	  }
		
	

  function cargaGridUsers(){
	  /*
          username = $('#findUsername').val();
          email = $('#findMail').val();
          name = $('#findName').val();
          last = $('#findLastname').val();
          company = $('#findCompany').val();
          
          showLoader();
          cad = 'username='+username+'&email='+email+'&name='+name+'&last='+last+'&company='+company+'&_token='+token;
          $('#tblUSers').dataTable().fnDestroy();
          $('#tblUSers tbody').empty();
          $.ajax({
              type: "POST",
              url: "{{ route('users.data') }}",
              data: cad,
              success:function(data) {
                  $('#tblUSers tbody').append(data);
                  hideLoader();
                  $('#tblUSers').dataTable({
                      "language": {
                          "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                          "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                          "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                          "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                          "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                          "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                          "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                          "sInfoPostFix":    "",
                          "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                          "oPaginate": {
                              "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                              "sLast":     "{{ trans('client.dt_last_lbl') }}",
                              "sNext":     "{{ trans('client.dt_next_lbl') }}",
                              "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                          },
                          "oAria": {
                              "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                              "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                          }

                      }
                  });

                  //$('#load').slideUp(100);
              }
          });
          */
      }

  $(document).on("click", ".btnCreditCard", function(event){
  		event.preventDefault();

  		//$('.wrapBraintree').slideDown(200);
		//$('.wrapBraintree #_token').val(token);
		
  		//$("#modalCreditCard").modal("show");

  		//$("#modalCreditCard").modal("show");

		$('#modal-generico-small').modal('show');
	    
	    var client_id 	= $(this).data('id');

		var route = "{{ route('users.create.credit_card',['*user*']) }}";
		route = route.replace('*user*',client_id);
		
	    $('#modal-generico-small-content').load(route);

  });

  $(document).on('click', '.printInvoicePrepay', function(event) {
		event.preventDefault();
		var id = $(this).data('id');

	    //route = 'http://127.0.0.1/ws/index_ws.php?id=' + id;
	    route = 'http://10.48.22.24/ws/index_ws.php?id=' + id;
	    window.open(route);


	  });
	  
  $(document).on('click', '.btnAddBraintree', function(event) {

		$('#modal-generico-small').modal('show');
	    
	    var client_id 	= $(this).data('user');
	    var storage_id  = $(this).data('id');
	    var paymentid   = $(this).data('paymentid');

		var route = "{{ route('cards_users.credit_card_braintree',['*user*','*storage*','*paymentid*']) }}";
		route = route.replace('*user*',client_id);
		route = route.replace('*storage*',storage_id);
		route = route.replace('*paymentid*',paymentid);
		
	    $('#modal-generico-small-content').load(route);
	  
  });

  $(document).on('click', '.btnCancelBraintree', function(event) {

	  if(confirm('{{trans("cart.confirm_cancel_suscription")}}')){
		$('#modal-generico-small').modal('show');
	    
	    var subscription_id = $(this).data('suscription');
	    var user_id 		= $(this).data('user');
	    
		var route = "{{ route('cancel_subscription',['*subscription_id*']) }}";
		route = route.replace('*subscription_id*',subscription_id);
		
		
	 	$.get(route, function(response, status){
			if(response.returnCode == 200){			 
		  		toastr['success']("{{ trans('cart.suscription_canceled') }}");
		  		
		  		getListStoragesClient(user_id);
	
		  		$('#modal-generico-small').modal('hide');
	
		  		setTimeout(function(){
		            hideLoader();
		          },1000);
			}
	    });
	  }
	  
	});	
  
  
  
  $(document).on('click', '.btnSelectCard', function(event) {


	  $(this).attr("disabled","disabled");

		showLoader();

		var user_id = $(this).data('user');
	  	var token = $(this).data("token");
	  	var customer = $(this).data("customer");
	  	var storage_id = $(this).data("storage");
	  	var paymentid = $(this).data("paymentid");
	  	
	  	var route="{{route('create_transaction',['*creditCardToken*','*customerId*','*planId*','*subscribed*','*storage_id*','*paymentid*',0])}}";

	  	
	  	route = route.replace('*creditCardToken*',token);
		route = route.replace('*customerId*',customer);
		route = route.replace('*planId*','{{env("PLAN_ID")}}');
		route = route.replace('*subscribed*','1');
		route = route.replace('*storage_id*',storage_id);
		route = route.replace('*paymentid*',paymentid);
		

	  	$.get(route, function(response, status){
			 //REGRESA SALDO

			if(response.returnCode == "200"){ 
	  			toastr['success']("<b style='font-size:1.2em'>" + response.msg +"</b>");
			}
			else{
				toastr['error']("<b style='font-size:1.2em'>" + response.msg +"</b>");
			}
	  		
	  		getListStoragesClient(user_id);

	  		$('#modal-generico-small').modal('hide');

	  		setTimeout(function(){
	            hideLoader();
	          },1000);
	    });
  });

  $(document).on("click",".btn-edit-invoice", function(event){
		event.preventDefault();
		//modal-generico-small
		
		var route = $(this).attr('href');
		
		$("#modal-generico").modal("show");
		$("#modal-generico-content").load(route);
		
	});

  $(document).on("click",".btn-cancel-paragon", function(event){
		event.preventDefault();
		//modal-generico-small
		
		var user_id = $(this).data('user');
		
		if(confirm('Are you sure to cancel paragon?')){
			showLoader();
			
			var route = $(this).attr("href");
		  	
		  	$.get(route, function(response, status){
		  		
		  		toastr['success'](response.msg);
		  		
		  		getListStoragesClient(user_id);

		  		$('#modal-generico').modal('hide');

		  		setTimeout(function(){
		            hideLoader();
		          },1000);

		    });
		}
	});

  $(document).on("click",".btn-retry-charge", function(event){
		event.preventDefault();
		
		var route = $(this).attr("href");
		
		if(confirm('Are you sure to retry charge?')){
			showLoader();
			
		  	$.get(route, function(response, status){
		  		
		  		toastr['success']("<h3>"+ response.msg+ "</h3>" );
		  	
		  		$('#modal-generico').modal('hide');

		  		setTimeout(function(){
		            hideLoader();
		          },1000);
		    });
		}
	});

  $(document).on("click","#btn-correct-invoice-quantity", function(event){
		//
	  	var reason = $("#reason-correction-quantity").val();
		if(reason == ""){
			toastr['error']("Type reason correction");
			return false;
		}
		
		$(this).attr("disabled","disabled");
		
		var route = "{{route('billing.update_invoice')}}";

		//CORRECTION
		//WARTOSC NETTO
		var wartosc_netto_correccion = $("#correction-warstoc-netto-BOX").val();
		var wartosc_netto_correccion_ins = $("#correction-warstoc-netto-INSURANCE").val();

		if(isNaN(wartosc_netto_correccion_ins)){
			wartosc_netto_correccion_ins = 0;
		}		
		var razem_wartosc_netto_correccion = Number(wartosc_netto_correccion) + Number(wartosc_netto_correccion_ins);
		
		//KWOTA VAT
		var kwota_vat_correccion = $("#correction-kwota-vat-BOX").val();
		var kwota_vat_correccion_ins = $("#correction-kwota-vat-INSURANCE").val();
		if(isNaN(kwota_vat_correccion_ins)){
			kwota_vat_correccion_ins = 0;
		}
		var razem_kwota_vat_correccion	   = Number(kwota_vat_correccion) + Number(kwota_vat_correccion_ins);

		//WARTOSC BRUTTO
		var wartosc_brutto_correccion = $("#correction-wartosc-brutto-BOX").val();
		var wartosc_brutto_correccion_ins = $("#correction-wartosc-brutto-INSURANCE").val();
		if(isNaN(wartosc_brutto_correccion_ins)){
			wartosc_brutto_correccion_ins = 0;
		}
		var razem_wartosc_brutto_correccion= Number(wartosc_brutto_correccion) + Number(wartosc_brutto_correccion_ins);

		//END CORRECTION
		
		//AFTER CORRECTION
		//WARTOSC NETTO
		var wartosc_netto_corregido = $("#after-correction-warstoc-netto-BOX").text();
		var wartosc_netto_corregido_ins = $("#after-correction-warstoc-netto-INSURANCE").text();

		if(isNaN(wartosc_netto_corregido_ins)){
			wartosc_netto_corregido_ins = 0;
		}	
		var razem_wartosc_netto_corregido = Number(wartosc_netto_corregido) + Number(wartosc_netto_corregido_ins);
		
		//KWOTA VAT
		var kwota_vat_corregido = $("#after-correction-kwota-vat-BOX").text();
		var kwota_vat_corregido_ins = $("#after-correction-kwota-vat-INSURANCE").text();
		if(isNaN(kwota_vat_corregido_ins)){
			kwota_vat_corregido_ins = 0;
		}
		var razem_kwota_vat_corregido	   = Number(kwota_vat_corregido) + Number(kwota_vat_corregido_ins);

		//WARTOSC BRUTTO
		var wartosc_brutto_corregido = $("#after-correction-wartosc-brutto-BOX").text();
		var wartosc_brutto_corregido_ins = $("#after-correction-wartosc-brutto-INSURANCE").text();
		if(isNaN(wartosc_brutto_corregido_ins)){
			wartosc_brutto_corregido_ins = 0;
		}
		var razem_wartosc_brutto_corregido = Number(wartosc_brutto_corregido) + Number(wartosc_brutto_corregido_ins);
		
		
		//END AFTER CORRECTION
		
		var postData = {
				billing_id 		: $("#billing-id").val(),
				_token			: "{{csrf_token()}}",

				//CORRECCIONES               
				//BOX
              	llosc_correccion : $("#quantity-correction-BOX").val(),                
              	wartosc_netto_correccion: $("#correction-warstoc-netto-BOX").val() ,
              	kwota_vat_correccion: $("#correction-kwota-vat-BOX").val(),
             	wartosc_brutto_correccion : $("#correction-wartosc-brutto-BOX").val(),
				//INS
				llosc_correccion_ins : $("#quantity-correction-INSURANCE").val(),
				wartosc_netto_correccion_ins: $("#correction-warstoc-netto-INSURANCE").val(),
              	kwota_vat_correccion_ins: $("#correction-kwota-vat-INSURANCE").val(),
             	wartosc_brutto_correccion_ins : $("#correction-wartosc-brutto-INSURANCE").val(),
             	
				//DESPUES DE LA CORRECCION
				//BOX
              	llosc_corregido : $("#after-correction-llosc-BOX").text(),
              	wartosc_netto_corregido: $("#after-correction-warstoc-netto-BOX").text(),
              	kwota_vat_corregido: $("#after-correction-kwota-vat-BOX").text(),
             	wartosc_brutto_corregido : $("#after-correction-wartosc-brutto-BOX").text(),

             	//INS
             	llosc_corregido_ins : $("#after-correction-llosc-INSURANCE").text(),
             	wartosc_netto_corregido_ins: $("#after-correction-warstoc-netto-INSURANCE").text(),
              	kwota_vat_corregido_ins: $("#after-correction-kwota-vat-INSURANCE").text(),
             	wartosc_brutto_corregido_ins : $("#after-correction-wartosc-brutto-INSURANCE").text(),

             	//RAZEM
             	razem_wartosc_netto_correccion	: razem_wartosc_netto_correccion,
             	razem_kwota_vat_correccion		: razem_kwota_vat_correccion,
             	razem_wartosc_brutto_correccion : razem_wartosc_brutto_correccion,

             	razem_wartosc_netto_corregido	: razem_wartosc_netto_corregido,
             	razem_kwota_vat_corregido		: razem_kwota_vat_corregido,
             	razem_wartosc_brutto_corregido	: razem_wartosc_brutto_corregido,

             	correction_price 				: '0',

             	reason_correction				: reason
             	
			};
			
			$.post(route, postData, function(data){
				if(data.returnCode == 200){
					toastr['success'](data.msg);
					$("#modal-generico").modal("hide");
				}else{
					toastr['error'](data.msg);
					$("#modal-generico").modal("hide");
				}
				
		    });
			
		
	  });


	 //	//
	 $(document).on("click","#btn-correct-invoice-price", function(event){
		//
		var reason = $("#reason-correction-price").val();
		if(reason == ""){
			toastr['error']("Type reason correction");
			return false;
		}
			
		$(this).attr("disabled","disabled");
		
		var route = "{{route('billing.update_invoice')}}";

		//CORRECTION
		//WARTOSC NETTO
		var wartosc_netto_correccion = $("#correction-price-warstoc-netto-BOX").val();
		var wartosc_netto_correccion_ins = $("#correction-price-warstoc-netto-INSURANCE").val();

		if(isNaN(wartosc_netto_correccion_ins)){
			wartosc_netto_correccion_ins = 0;
		}		
		var razem_wartosc_netto_correccion = Number(wartosc_netto_correccion) + Number(wartosc_netto_correccion_ins);
		
		//KWOTA VAT
		var kwota_vat_correccion = $("#correction-price-kwota-vat-BOX").val();
		var kwota_vat_correccion_ins = $("#correction-price-kwota-vat-INSURANCE").val();
		if(isNaN(kwota_vat_correccion_ins)){
			kwota_vat_correccion_ins = 0;
		}
		var razem_kwota_vat_correccion	   = Number(kwota_vat_correccion) + Number(kwota_vat_correccion_ins);

		//WARTOSC BRUTTO
		var wartosc_brutto_correccion = $("#correction-price-wartosc-brutto-BOX").val();
		var wartosc_brutto_correccion_ins = $("#correction-price-wartosc-brutto-INSURANCE").val();
		if(isNaN(wartosc_brutto_correccion_ins)){
			wartosc_brutto_correccion_ins = 0;
		}
		var razem_wartosc_brutto_correccion= Number(wartosc_brutto_correccion) + Number(wartosc_brutto_correccion_ins);

		//END CORRECTION
		
		//AFTER CORRECTION
		//WARTOSC NETTO
		var wartosc_netto_corregido = $("#after-correction-price-warstoc-netto-BOX").text();
		var wartosc_netto_corregido_ins = $("#after-correction-price-warstoc-netto-INSURANCE").text();

		if(isNaN(wartosc_netto_corregido_ins)){
			wartosc_netto_corregido_ins = 0;
		}	
		var razem_wartosc_netto_corregido = Number(wartosc_netto_corregido) + Number(wartosc_netto_corregido_ins);
		
		//KWOTA VAT
		var kwota_vat_corregido = $("#after-correction-price-kwota-vat-BOX").text();
		var kwota_vat_corregido_ins = $("#after-correction-price-kwota-vat-INSURANCE").text();
		if(isNaN(kwota_vat_corregido_ins)){
			kwota_vat_corregido_ins = 0;
		}
		var razem_kwota_vat_corregido	   = Number(kwota_vat_corregido) + Number(kwota_vat_corregido_ins);

		//WARTOSC BRUTTO
		var wartosc_brutto_corregido = $("#after-correction-price-wartosc-brutto-BOX").text();
		var wartosc_brutto_corregido_ins = $("#after-correction-price-wartosc-brutto-INSURANCE").text();
		if(isNaN(wartosc_brutto_corregido_ins)){
			wartosc_brutto_corregido_ins = 0;
		}
		var razem_wartosc_brutto_corregido = Number(wartosc_brutto_corregido) + Number(wartosc_brutto_corregido_ins);
		
		
		//END AFTER CORRECTION
		
		//alert($("#after-correction-price-warstoc-netto-INSURANCE").text());
		
		var postData = {
				billing_id 		: $("#billing-id").val(),
				_token			: "{{csrf_token()}}",

				//CORRECCIONES               
				//BOX
              	llosc_correccion : $("#quantity-correction-price-BOX").val(),             
              	cenna_netto_correccion: $("#correction-price-warstoc-netto-BOX").val() ,   
              	wartosc_netto_correccion: $("#correction-price-warstoc-netto-BOX").val() ,
              	kwota_vat_correccion: $("#correction-price-kwota-vat-BOX").val(),
             	wartosc_brutto_correccion : $("#correction-price-wartosc-brutto-BOX").val(),
				//INS
				llosc_correccion_ins : $("#quantity-correction-price-INSURANCE").val(),
				cenna_netto_correccion_ins: $("#correction-price-warstoc-netto-INSURANCE").val() ,
				wartosc_netto_correccion_ins: $("#correction-price-warstoc-netto-INSURANCE").val(),
              	kwota_vat_correccion_ins: $("#correction-price-kwota-vat-INSURANCE").val(),
             	wartosc_brutto_correccion_ins : $("#correction-price-wartosc-brutto-INSURANCE").val(),
             	
				//DESPUES DE LA CORRECCION
				//BOX
              	llosc_corregido : $("#after-correction-price-llosc-BOX").text(),
              	cenna_netto_corregido : $("#after-correction-price-cenna-netto-BOX").text(),
              	wartosc_netto_corregido: $("#after-correction-price-warstoc-netto-BOX").text(),
              	kwota_vat_corregido: $("#after-correction-price-kwota-vat-BOX").text(),
             	wartosc_brutto_corregido : $("#after-correction-price-wartosc-brutto-BOX").text(),

             	//INS
             	llosc_corregido_ins : $("#after-correction-llosc-INSURANCE").text(),
             	cenna_netto_corregido_ins : $("#after-correction-price-cenna-netto-INSURANCE").text(),
             	wartosc_netto_corregido_ins: $("#after-correction-price-warstoc-netto-INSURANCE").text(),
              	kwota_vat_corregido_ins: $("#after-correction-price-kwota-vat-INSURANCE").text(),
             	wartosc_brutto_corregido_ins : $("#after-correction-price-wartosc-brutto-INSURANCE").text(),

             	//RAZEM
             	razem_wartosc_netto_correccion	: razem_wartosc_netto_correccion,
             	razem_kwota_vat_correccion		: razem_kwota_vat_correccion,
             	razem_wartosc_brutto_correccion : razem_wartosc_brutto_correccion,

             	razem_wartosc_netto_corregido	: razem_wartosc_netto_corregido,
             	razem_kwota_vat_corregido		: razem_kwota_vat_corregido,
             	razem_wartosc_brutto_corregido	: razem_wartosc_brutto_corregido,

             	correction_price 				: '1',

             	reason_correction				: reason
             	
             	
			};

		//alert(JSON.stringify(postData));
	
			$.post(route, postData, function(data){
				if(data.returnCode = 200){
					toastr['success'](data.msg);
					$("#modal-generico").modal("hide");
				}
				
		    });
		
	  });

	 $(document).on("click",".btnChangeCC", function(event){
		 //alert("HOLA");
		 event.preventDefault();
		 
		 var cc = $(this).data("cc");
		 var id = $(this).data("paymentid");
		 var user_id = $(this).data("user");
		 
		 var route = "{{route('update.cc.paymentdata',['*id*','*cc*'])}}";
			route = route.replace('*id*',id);
			route = route.replace('*cc*',cc);
			
			$.get(route, function(data, status){
				/*notif({	
					msg: data.msg,
					type: data.type,
					position: "center",
					opacity : 0.8
				});*/

				if(data.returnCode == 200){
					toastr['success'](data.msg);

					getListStoragesClient(user_id);
				}
				
				//oTable.draw();
			});
	 });

	 $(document).on("click",".chkOportunity", function(event){
		 if($(this).is(':checked')){
			 //$("#peselNumber, #idNumber").addClass("ignore");
			 $("#g_peselNumber, #g_idNumber, .fieldset-address").hide();
			 $(".fieldset-address input").val("");
			}
		 else{
			 //$("#peselNumber, #idNumber").removeClass("ignore");
			 $("#g_peselNumber, #g_idNumber, .fieldset-address").show();
			}
		});

	 $(document).on("click","#btnSendNotifications", function(event){
		 event.preventDefault();
		 
		 $("#modal-generico-small").modal("show");
		 $("#modal-generico-small-content").load( $(this).attr('href') );		 	
	 });
	

	 $(document).on("click","#btnSendCollectiveCorrespondence", function(event){
		 event.preventDefault();
		 
		 $("#modal-generico").modal("show");
		 $("#modal-generico-content").load( $(this).attr('href') );		 	
	 });
	 
	 $(document).on("click","#btnAppContracts", function(event){
		 event.preventDefault();
		 
		 $("#modal-generico").modal("show");
		 $("#modal-generico-content").load( $(this).attr('href') );		 	
	 });
	 
	 $(document).on("click",".btnEditRecurrentBilling", function(event){
		event.preventDefault();

		var id = $(this).attr('id');
		
		var route = "{{ route('storages.recurrent_billing_edit',['*id*']) }}";
		route = route.replace('*id*', id);
		
		$("#modal-generico-small").modal("show");
		$("#modal-generico-small-content").load( route );
				 	
	 });

	 $(document).on("click",".btn-invite-app", function(event){
		event.preventDefault();

		var route = $(this).attr("href");

		$.get(route, function(data, status){
			if(data.returnCode == 200){
				toastr['success'](data.msg);
			}else{
				toastr['error'](data.msg);
			}		
		});		
				 	
	 });

      //cargaGridUsers();
  </script>
@endsection
