@extends('app')
@section('content')

  <div class="storagesMap">
    <svg id="mysvg"></svg>
    <a href="javascript:void(0);" class="btn btn-primary hideMap"><i class="fa fa-angle-up" aria-hidden="true"></i> {{ trans('storages.hide_storagesMap') }}</a>
  </div>
  <a href="javascript:void(0);" class="showMap"><i class="fa fa-angle-down" aria-hidden="true"></i> {{ trans('storages.show_storagesMap') }}</a>
  <div class="row">
    <div class="col-md-9">
        <h2>{{ trans('storages.storages_title') }}</h2>
    </div>
    <div class="col-md-3" style="text-align: right">
      <a href="javascript:void(0);" class="btn btn-primary btnNewStorage"><i class="fa fa-user-plus"></i> {{ trans('storages.new_storage') }}</a>
    </div>
  </div>

  <div class="card">
    <div class="card-head style-default">
      <header>{{ trans('storages.search_title') }}</header>
    </div>
    <div class="card-body">
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findAlias" id="findAlias" class="form-control" value="" required="required" placeholder="{{ trans('storages.alias') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findSqm" id="findSqm" class="form-control" value="" required="required" placeholder="{{ trans('storages.sqm') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findPrice" id="findPrice" class="form-control" value="" required="required" placeholder="{{ trans('storages.price') }}" title="">
        </div>
      </div>

      <div class="col-md-8" style="border: dashed 1px #ccc; padding: 10px;">
        <label>{{ trans('storages.date_range') }}</label>
        <div class="input-daterange input-group" id="demo-date-range">
          <div class="input-group-content">
            <input type="text" class="form-control" name="findStart" id="findStart" placeholder="{{ trans('storages.start') }}">
          </div>
          <span class="input-group-addon">{{ trans('storages.to') }}</span>
          <div class="input-group-content">
            <input type="text" class="form-control" name="findEnd" id="findEnd" placeholder="{{ trans('storages.end') }}">
            <div class="form-control-line"></div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-info btnSearchStorages">{{ trans('storages.search_lbl') }}</button>
        </div>
      </div>
    </div>
  </div>

  <table class="table table-hover" id="tblStorages">
	   <thead>
       <tr>
         <th>
           {{ trans('storages.alias') }}
         </th>
         <th>
           {{ trans('storages.sqm') }}
         </th>
         <th>
           {{ trans('storages.price') }}
         </th>
         <th>
           {{ trans('storages.rent_by') }}
         </th>
         <th>
           {{ trans('client.company_lbl') }}
         </th>
         <th>
           {{ trans('storages.rent_start') }}
         </th>
         <th>
           {{ trans('storages.rent_end') }}
         </th>
         <th>
           {{ trans('storages.actions_lbl') }}
         </th>
       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>

  <script>
  jQuery(document).ready(function($) {
        $('#findStart').datetimepicker({
          format: 'D/M/Y',
          allowInputToggle: true,
          //minDate: moment(),
        });

        $('#findEnd').datetimepicker({
          format: 'D/M/Y',
          allowInputToggle: true,
          //minDate: moment(),
        });
  });

  $(document).on('click', '.btnSearchStorages', function(event) {
    cargaGridStorages();
  });

  function cargaGridStorages(){
          alias = $('#findAlias').val();
          sqm = $('#findSqm').val();
          price = $('#findPrice').val();
          dateStart = $('#findStart').val();
          dateEnd = $('#findEnd').val();

          if (dateStart != '' && dateEnd == '') {
            toastr['error']('{{ trans('storages.invalid_range') }}');
            return false;
          }

          if (dateStart == '' && dateEnd != '') {
            toastr['error']('{{ trans('storages.invalid_range') }}');
            return false;
          }

          //uid = 0;
          showLoader();
          cad = 'id='+ {{ $idLevel }} +'&_token='+token+'&alias='+alias+'&sqm='+sqm+'&price='+price+'&dateStart='+dateStart+'&dateEnd='+dateEnd;
          $('#tblStorages').dataTable().fnDestroy();
          $('#tblStorages tbody').empty();
          $.ajax({
              type: "POST",
              url: "{{ route('getStoragesList') }}",
              data: cad,
              success:function(data) {
                  $('#tblStorages tbody').append(data);
                  $('#modalNewStorage #idLevel').val({{ $idLevel }});
                  hideLoader();
                  $('#tblStorages').dataTable({
                      "language": {
                          "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                          "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                          "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                          "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                          "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                          "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                          "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                          "sInfoPostFix":    "",
                          "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                          "oPaginate": {
                              "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                              "sLast":     "{{ trans('client.dt_last_lbl') }}",
                              "sNext":     "{{ trans('client.dt_next_lbl') }}",
                              "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                          },
                          "oAria": {
                              "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                              "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                          }

                      }
                  });

                  //$('#load').slideUp(100);
              }
          });
      }

    cargaGridStorages();

    var s= Snap('#mysvg');
    rojo = 'rgba(255, 23, 20, .5)';
    verde = 'rgba(0, 255, 18, .5)';
    amarillo = 'rgba(250, 255, 0, .5)';
    claro = 'rgba(255, 255, 255, 1)';

    function drawStorage(s,t,tx,ty,ts,rx,ry,rw,rh,rc,cl,sqm,price,uid,companyName,rents,rente,comments,id){
      // ctx = canvas
      // t = text
      // tx = text pos x
      // ty = text pos y
      // ts = text size
      // rx = rectangle pos x
      // ry = rectangle pos y
      // rw = rectangle width
      // rh = rectangle heigth
      // rc = rectangle color

      var rect = s.rect(rx,ry,rw,rh);
      rect.attr({
        fill: colors(t,rents,rente),
        stroke: '#000',
        strokeWidth: 1,
        id: t,
        dir: '<b>sqm</b>: '+sqm+'<br><b>Price</b>: '+price+'<br><b>Client</b>: '+uid+'<br><b>Company Name</b>:'+companyName+'<br><b>Rent Start</b>: '+rents+'<br><b>Rent end</b>: '+rente+'<br><b>Comments</b>: '+comments+'</br></br><a href="javascript:void(0);" class="btn btn-info btn-xs editStorage" id="'+id+'"><i class="fa fa-pencil" aria-hidden="true"></i></a>',
      });

      //cl =  colors(0);
      
      var t1 = s.text(tx, ty, t);
      t1.attr({
        color: '#000',
        class: cl
      });

    }

    function colors(t,start,end){
      if (!end) {
        return '#fff'
      }
      var TodayDate = new Date();
      var y = TodayDate.getYear();
      //var d = TodayDate.getDay();
      var m = TodayDate.getMonth()+1;
      res = end.split('-');
      res = parseInt(res[1]);
      m = parseInt(m);

      if(t == '327' || t == '10-C' || t == '20-C' || t == '21-C' || t == '72-C' || t == '542' || t == '22-C' || t == '73-C'){
          return 'rgb(255, 0, 0)';
      }

      if (t == 'OFFICE' || t == '') {
        return '#fff';
      }

      if (start == '0000-00-00') {
        return '#fff';
      }

      if (start != '0000-00-00' && end == '0000-00-00') {
        return 'rgb(255, 0, 0)';
      }

      if( m == res){
	      return 'rgb(15, 184, 0)';
      }

      if (start != '0000-00-00' && end != '0000-00-00') {
        return 'rgb(181, 0, 255)';
      }

      return '#fff';
    }

    function drawWall(s,xStart,yStart,xEnd,yEnd,lineColor,lineWeigt){
        var line = s.line(xStart, yStart, xEnd,yEnd);

        line.attr({
          stroke: '#000',
          strokeWidth: lineWeigt
        });
    }

    idLevel = {{ $idLevel }};
    if (idLevel == 1) {
      //acotaciones level 1
      drawWall(s,62, 77, 419,77,'#FFF',2);
      drawWall(s,473, 77, 504,77,'#FFF',2);
      drawWall(s,504, 60, 504,77,'#FFF',2);
      drawWall(s,522, 77, 595,77,'#FFF',2);
      drawWall(s,649, 77, 1223,77,'#FFF',2);
      drawWall(s,1222, 78, 1222, 644,'#FFF',2);
      drawWall(s,712, 645, 1222, 643,'#FFF',2);
      drawWall(s,649, 645, 695, 645,'#FFF',2);
      drawWall(s,695, 644, 695, 665,'#FFF',2);
      drawWall(s,473, 644, 595, 644,'#FFF',2);
      drawWall(s,62, 644, 417, 644,'#FFF',2);
      drawWall(s,104, 78, 104, 643,'#FFF',2);

      drawStorage(s,'',0,0,'0',419,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
      drawStorage(s,'',0,0,'0',595,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
      drawStorage(s,'',0,0,'0',595,639,54,5,'rgba(255, 255, 255, 0.3)','text12');
      drawStorage(s,'',0,0,'0',417,639,56,5,'rgba(255, 255, 255, 0.3)','text12');
      // dibujamos la oficina:
      drawStorage(s,'OFFICE',235,130,'40',104,78,305,112,'rgba(111, 111, 111, 0.3)','text12');

    }

    if (idLevel == 2) {
    drawWall(s,62, 77, 193,77,'#FFF',2); //INICIAL ARRIBA IZQUIERDA 225 30 px de cada puerta
    drawWall(s,223, 77, 770,77,'#FFF',2);
    drawWall(s,800, 60, 800, 77,'#FFF',2);
    drawWall(s,800, 77, 1223,77,'#FFF',2);
    drawWall(s,800, 77, 1240,77,'#FFF',2);
    drawWall(s,1222, 78, 1222, 644,'#FFF',2);
    drawWall(s,1010, 645, 1240, 643,'#FFF',2);
    drawWall(s,980, 645, 62, 643,'#FFF',2);
    drawWall(s,980, 645, 980, 660,'#FFF',2);
    drawWall(s,104, 78, 104, 643,'#FFF',2);


      drawWall(s,1120,79, 1120, 159,'#FFF',1);
      drawWall(s,1150,79, 1150, 159,'#FFF',1);



      drawStorage(s,'',412,150,'40',105,79,120,110,'rgba(111, 111, 111, 2)','text12');
      drawStorage(s,'SERVICE',412,150,'40',410,79,60,130,'rgba(111, 111, 111, 2)','text12');
      drawStorage(s,'',412,150,'40',650,79,120,130,'rgba(111, 111, 111, 2)','text12');
      drawStorage(s,'WC',1040,150,'40',1020,79,60,130,'rgba(111, 111, 111, 2)','text12');
      drawStorage(s,'',1170,340,'40',380,249,590,394,'rgba(111, 111, 111, 2)','text12');
      drawStorage(s,'',1170,340,'40',970,606,50,25,'rgba(111, 111, 111, 2)','text12');


      //drawWall(s,1120, 246, 1150, 246,'#FFF',1);


      drawWall(s,1150, 100, 1223, 100,'#FFF',1);
      //Dibuja escaleras izquierda
      drawWall(s,1150, 110, 1185, 110,'#D8D',1);
      drawWall(s,1150, 120, 1185, 120,'#D8D',1);
      drawWall(s,1150, 130, 1185, 130,'#D8D',1);
      drawWall(s,1150, 140, 1185, 140,'#D8D',1);
      drawWall(s,1150, 150, 1185, 150,'#D8D',1);

      //Dibuja escaleras derecha
      drawWall(s,1185, 110, 1223, 110,'#D8D',1);
      drawWall(s,1185, 120, 1223, 120,'#D8D',1);
      drawWall(s,1185, 130, 1223, 130,'#D8D',1);
      drawWall(s,1185, 140, 1223, 140,'#D8D',1);
      drawWall(s,1185, 150, 1223, 150,'#D8D',1);
      drawWall(s,1185, 160, 1223, 160,'#D8D',1);
      drawWall(s,1185, 170, 1223, 170,'#D8D',1);
      drawWall(s,1185, 180, 1223, 180,'#D8D',1);
      drawWall(s,1185, 190, 1223, 190,'#D8D',1);
      drawWall(s,1185, 200, 1223, 200,'#D8D',1);
      drawWall(s,1185, 210, 1223, 210,'#D8D',1);


      // LiFT Ojo despues del 40, vienenn las coordenada siniciales y luego el tamaño
      drawStorage(s,'LIFT',1170,340,'40',1150,316,72,39,'rgba(111, 111, 111, 2)','text12');


      // dibujamos la oficina:
      //drawStorage(s,'',130,140,'40',104,78,120,112,claro,'text12');
    }

    if (idLevel == 3 || idLevel ==4) {
      drawWall(s,62, 77, 1297,77,'#FFF',2); //Contorno Superior
      drawWall(s,1222, 78, 1222, 644,'#FFF',2); //Contorno derecho
      drawWall(s,1297, 643, 62, 643,'#FFF',2); // Contorno inferior
      drawWall(s,104, 78, 104, 643,'#FFF',2); //Contorno izquierdo
      drawWall(s,900, 78, 900, 643,'#FFF',2); // Pared interior divisora
      //bodega lado derecho


      //drawWall(s,1150, 246, 1150, 100,'#FFF',1);
      //drawWall(s,1120, 246, 1150, 246,'#FFF',1);

      drawWall(s,1150, 100, 1223, 100,'#FFF',1);
      //Dibuja escaleras izquierda
      drawWall(s,1150, 110, 1185, 110,'#D8D',1);
      drawWall(s,1150, 120, 1185, 120,'#D8D',1);
      drawWall(s,1150, 130, 1185, 130,'#D8D',1);
      drawWall(s,1150, 140, 1185, 140,'#D8D',1);
      drawWall(s,1150, 150, 1185, 150,'#D8D',1);

      //Dibuja escaleras derecha
      drawWall(s,1185, 110, 1223, 110,'#D8D',1);
      drawWall(s,1185, 120, 1223, 120,'#D8D',1);
      drawWall(s,1185, 130, 1223, 130,'#D8D',1);
      drawWall(s,1185, 140, 1223, 140,'#D8D',1);
      drawWall(s,1185, 150, 1223, 150,'#D8D',1);
      drawWall(s,1185, 160, 1223, 160,'#D8D',1);
      drawWall(s,1185, 170, 1223, 170,'#D8D',1);
      drawWall(s,1185, 180, 1223, 180,'#D8D',1);
      drawWall(s,1185, 190, 1223, 190,'#D8D',1);
      drawWall(s,1185, 200, 1223, 200,'#D8D',1);
      drawWall(s,1185, 210, 1223, 210,'#D8D',1);
      drawWall(s,1185, 220, 1223, 220,'#D8D',1);
      //drawWall(s,1185, 230, 1223, 230,'#D8D',1);
      //drawWall(s,1185, 240, 1223, 240,'#D8D',1);

      //Lift Drawing

      /*drawWall(s,1150, 316, 1223, 316,'#D8D',2);
      drawWall(s,1150, 316, 1150, 355,'#FFF',2);
      drawWall(s,1150, 355, 1223, 355,'#D8D',2); */
      // Ojo despues del 40, vienenn las coordenada siniciales y luego el tamaño
      drawStorage(s,'LIFT',1170,340,'40',1150,316,72,39,'rgba(111, 111, 111, 2)','text12');


     // drawStorage(s,'LIFT',1200,320,'40',1150,316,1223,355,'rgba(111, 111, 111, 0.3)','text12');

        // dibujamos la oficina:
        //drawStorage(s,'',130,140,'40',104,78,120,112,claro,'text12');
      }

    //(s,t,tx,ty,ts,rx,ry,rw,rh,rc,cl)
    @foreach ($storages as $st)
    
    
      drawStorage(
          s,
          '{{$st->alias}}',
          {{$st->text_posx}},
          {{$st->text_posy}},
          '10',
          {{$st->rect_posx}},
          {{$st->rect_posy}},
          {{$st->rect_width}},
          {{$st->rect_height}},
          {{$st->rect_background}},
          '{{$st->class}}',
          '{{$st->sqm}}',
          '{{$st->price}}',
          '{{ isset($st->StorageOwner->name) ? $st->StorageOwner->name.' '.$st->StorageOwner->lastName : '' }}',
          '{{ isset($st->StorageOwner->companyName) ? $st->StorageOwner->companyName : ''}}',
          '{{$st->rent_start}}',
          '{{$st->rent_end}}',
          '{{$st->comments}}',
          {{$st->id}}
      );
    @endforeach

    

    //drawStorage(s,'1',122,203,'10',104,190,41,19,verde,'text10'); //1



    $('rect').qtip({
      content: {
          text: function(event, api) {
                  return $(this).attr('dir');
                },
          title: function(event, api) {
                  return '<h2>'+$(this).attr('id')+'</h2>';
                },
          button: true
      },
      hide: {
          event: false
      },
      position: {
        target: 'event'
      },
      style: {
          //classes: 'qtip-bootstrap'
          classes: 'qtip-shadow'
      },
      hide: {
        delay: 1000
      }
    });


  </script>
@endsection
