@extends('app')
@section('content')


<script>
  $(document).ready(function() {
    var s= Snap(1274,765);
    rojo = 'rgba(255, 23, 20, .5)';
    verde = 'rgba(0, 255, 18, .5)';
    amarillo = 'rgba(250, 255, 0, .5)';
    claro = 'rgba(255, 255, 255, .0)';

    function drawStorage(s,t,tx,ty,ts,rx,ry,rw,rh,rc,cl){
      // ctx = canvas
      // t = text
      // tx = text pos x
      // ty = text pos y
      // ts = text size
      // rx = rectangle pos x
      // ry = rectangle pos y
      // rw = rectangle width
      // rh = rectangle heigth
      // rc = rectangle color

      var t1 = s.text(tx, ty, t);
      t1.attr({
        color: '#000',
        class: cl
      });

      var rect = s.rect(rx,ry,rw,rh);
      rect.attr({
        fill: rc,
        stroke: '#000',
        strokeWidth: 1,
        id: t
      });

    }

    function drawWall(s,xStart,yStart,xEnd,yEnd,lineColor,lineWeigt){
        var line = s.line(xStart, yStart, xEnd,yEnd);

        line.attr({
          stroke: '#000',
          strokeWidth: lineWeigt
        });
    }

    //acotaciones
    drawWall(s,62, 77, 419,77,'#FFF',2);
    drawWall(s,473, 77, 504,77,'#FFF',2);
    drawWall(s,504, 60, 504,77,'#FFF',2);
    drawWall(s,522, 77, 595,77,'#FFF',2);
    drawWall(s,649, 77, 1223,77,'#FFF',2);
    drawWall(s,1222, 78, 1222, 644,'#FFF',2);
    drawWall(s,712, 645, 1222, 643,'#FFF',2);
    drawWall(s,649, 645, 695, 645,'#FFF',2);
    drawWall(s,695, 644, 695, 665,'#FFF',2);
    drawWall(s,473, 644, 595, 644,'#FFF',2);
    drawWall(s,62, 644, 417, 644,'#FFF',2);
    drawWall(s,104, 78, 104, 643,'#FFF',2);

    /*aqui yo adry le movi para buscar el cuadro vacio */
    drawStorage(s,'',0,0,'0',419,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
    drawStorage(s,'',0,0,'0',595,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
    drawStorage(s,'',0,0,'0',595,639,54,5,'rgba(255, 255, 255, 0.3)','text12');
    drawStorage(s,'',0,0,'0',417,639,56,5,'rgba(255, 255, 255, 0.3)','text12');

    // dibujamos la oficina:
    drawStorage(s,'OFFICE',235,130,'40',104,78,305,112,'rgba(111, 111, 111, 0.3)','text12');

    drawStorage(s,'1',122,203,'10',104,190,41,19,verde,'text10'); //1
    drawStorage(s,'2',122,222,'10',104,209,41,19,rojo,'text10'); //2
    drawStorage(s,'3',122,241,'10',104,228,41,19,amarillo,'text10'); //3
    drawStorage(s,'4',122,260,'10',104,247,41,19,claro,'text10'); //4
    drawStorage(s,'81',120,279,'10',104,266,41,19,claro,'text10'); //81
    drawStorage(s,'82',120,298,'10',104,285,41,19,claro,'text10'); //82
    drawStorage(s,'83',120,317,'10',104,304,41,19,claro,'text10'); //83
    drawStorage(s,'84',120,336,'10',104,323,41,19,claro,'text10'); //84
    drawStorage(s,'85',120,355,'10',104,342,41,19,claro,'text10'); //85
    //dibujo 87 y 88
    drawStorage(s,'87',109,374,'10',104,361,21,19,claro,'text10'); //87
    drawStorage(s,'86',130,374,'10',125,361,20,19,claro,'text10'); //86

    //dibujo 88 y 89
    drawStorage(s,'88',109,410,'10',104,398,21,19,claro,'text10'); //88
    drawStorage(s,'89',130,410,'10',125,398,20,19,claro,'text10'); //89
    //dibujo 90,91, 112-114
    drawStorage(s,'90',120,431,'10',104,417,41,19,claro,'text10'); //90
    drawStorage(s,'91',120,449,'10',104,436,41,19,claro,'text10'); //91
    drawStorage(s,'112',117,469,'10',104,455,41,19,claro,'text10'); //112
    drawStorage(s,'113',117,488,'10',104,474,41,19,claro,'text10'); //113
    drawStorage(s,'114',117,506,'10',104,493,41,19,claro,'text10'); //114

    drawStorage(s,'115',117,532,'10',104,512,41,38,claro,'text10'); //115
    drawStorage(s,'116',117,572,'10',104,550,41,38,claro,'text10'); //116
    drawStorage(s,'117',130,613,'10',104,588,70,44,claro,'text10'); //117

    drawStorage(s,'6',180,241,'10',174,227,19,20,claro,'text10'); //6
    drawStorage(s,'8',195,241,'9',193,227,10,20,claro,'text9'); //8
    drawStorage(s,'9',205,241,'9',203,227,10,20,claro,'text9'); //9
    drawStorage(s,'10',214,241,'9',213,227,10,20,claro,'text8'); //10
    drawStorage(s,'11',224,241,'9',223,227,10,20,claro,'text8'); //11
    drawStorage(s,'12',234,241,'9',233,227,10,20,claro,'text8'); //12
    drawStorage(s,'13',244,241,'9',243,227,10,20,claro,'text8'); //13
    drawStorage(s,'14',257,241,'9',253,227,20,20,claro,'text10'); //14

    drawStorage(s,'5',190,260,'10',174,247,39,19,claro,'text10'); //5
    drawStorage(s,'100',185,280,'10',174,266,39,19,claro,'text10'); //100
    drawStorage(s,'99',188,299,'10',174,285,39,19,claro,'text10'); //99
    drawStorage(s,'98',188,318,'10',174,304,39,19,claro,'text10'); //98
    drawStorage(s,'97',188,337,'10',174,323,39,19,claro,'text10'); //97
    drawStorage(s,'96',188,356,'10',174,342,39,19,claro,'text10'); //96
    drawStorage(s,'95',188,374,'10',174,361,39,19,claro,'text10'); //95
    drawStorage(s,'94',188,393,'10',174,380,39,19,claro,'text10'); //94
    drawStorage(s,'93',188,412,'10',174,399,39,19,claro,'text10'); //93

    drawStorage(s,'92',188,440,'10',174,418,39,38,claro,'text10'); //92
    drawStorage(s,'121',185,481,'10',174,456,39,38,claro,'text10'); //121
    drawStorage(s,'120',185,518,'10',174,494,39,38,claro,'text10'); //120
    drawStorage(s,'119',185,555,'10',174,532,39,38,claro,'text10'); //119
    drawStorage(s,'118',185,605,'10',174,570,39,62,claro,'text10'); //118

    drawStorage(s,'16',238,260,'10',213,247,60,19,claro,'text10'); //16
    drawStorage(s,'101',234,288,'10',213,266,60,38,claro,'text10'); //101
    drawStorage(s,'102',234,327,'10',213,304,60,38,claro,'text10'); //102
    drawStorage(s,'103',234,357,'10',213,342,60,38,claro,'text10'); //103
    drawStorage(s,'104',234,410,'10',213,380,60,74,claro,'text10'); //104
    drawStorage(s,'122',234,485,'10',213,454,60,78,claro,'text10'); //122
    drawStorage(s,'123',234,554,'10',213,532,60,38,claro,'text10'); //123
    drawStorage(s,'124',234,607,'10',213,570,60,74,claro,'text10'); //124
    drawStorage(s,'124a',275,632,'10',273,611,27,33,claro,'text10'); //124a

    drawStorage(s,'18',300,240,'9',300,227,9,20,claro,'text8'); //18
    drawStorage(s,'19',310,240,'9',309,227,10,20,claro,'text8'); //19
    drawStorage(s,'20',324,241,'9',319,227,20,20,claro,'text10'); //20
    drawStorage(s,'22',343,241,'9',339,227,20,20,claro,'text10'); //22
    drawStorage(s,'24',360,241,'9',359,227,9,20,claro,'text8'); //24
    drawStorage(s,'25',369,241,'9',368,227,10,20,claro,'text8'); //25
    drawStorage(s,'26',379,241,'9',378,227,9,20,claro,'text8'); //26
    drawStorage(s,'27',388,241,'9',387,227,10,20,claro,'text8'); //27
    drawStorage(s,'28',398,241,'9',397,227,9,20,claro,'text8'); //28
    drawStorage(s,'29',407,241,'9',406,227,10,20,claro,'text8'); //29

    drawStorage(s,'17',322,260,'10',300,247,59,19,claro,'text10'); //17
    drawStorage(s,'111',320,279,'10',300,266,59,19,claro,'text10'); //111
    drawStorage(s,'110',320,298,'10',300,285,59,19,claro,'text10'); //110
    drawStorage(s,'109',320,317,'10',300,304,59,19,claro,'text10'); //109
    drawStorage(s,'108',320,336,'10',300,323,59,19,claro,'text10'); //108
    drawStorage(s,'107',320,355,'10',300,342,59,19,claro,'text10'); //107
    drawStorage(s,'106',320,374,'10',300,361,59,19,claro,'text10'); //106
    drawStorage(s,'105',320,402,'10',300,380,59,38,claro,'text10'); //105
    drawStorage(s,'129',320,440,'10',300,418,59,38,claro,'text10'); //129
    drawStorage(s,'128',320,478,'10',300,456,59,38,claro,'text10'); //128
    drawStorage(s,'127',320,516,'10',300,494,59,38,claro,'text10'); //127
    drawStorage(s,'126',320,554,'10',300,532,59,38,claro,'text10'); //126
    drawStorage(s,'125',320,607,'10',300,570,59,62,claro,'text10'); //125

    drawStorage(s,'30',385,260,'10',359,247,57,19,claro,'text10'); //30
    drawStorage(s,'31',385,279,'10',359,266,57,19,claro,'text10'); //31
    drawStorage(s,'32',385,298,'10',359,285,57,19,claro,'text10'); //32
    drawStorage(s,'33',385,317,'10',359,304,57,19,claro,'text10'); //33
    drawStorage(s,'34',385,336,'10',359,323,57,19,claro,'text10'); //34
    drawStorage(s,'35',385,355,'10',359,342,57,19,claro,'text10'); //35
    drawStorage(s,'36',385,374,'10',359,361,57,19,claro,'text10'); //36
    drawStorage(s,'37',385,393,'10',359,380,57,19,claro,'text10'); //37
    drawStorage(s,'38',385,413,'10',359,399,57,19,claro,'text10'); //38

    drawStorage(s,'39',385,444,'10',359,418,57,38,claro,'text10'); //129
    drawStorage(s,'40',385,483,'10',359,456,57,38,claro,'text10'); //128
    drawStorage(s,'41',385,515,'10',359,494,57,38,claro,'text10'); //127
    drawStorage(s,'42',385,555,'10',359,532,57,38,claro,'text10'); //126
    drawStorage(s,'43',385,607,'10',359,570,57,74,claro,'text10'); //125

    drawStorage(s,'58',458,241,'9',454,227,20,20,claro,'text10'); //58
    drawStorage(s,'60',474,241,'9',474,227,10,20,claro,'text10'); //60
    drawStorage(s,'61',484,241,'9',484,227,10,20,claro,'text10'); //60
    drawStorage(s,'62',494,241,'9',494,227,10,20,claro,'text10'); //60
    drawStorage(s,'63',504,241,'9',504,227,10,20,claro,'text10'); //60
    drawStorage(s,'64',514,241,'9',514,227,10,20,claro,'text10'); //60
    drawStorage(s,'65',524,241,'9',524,227,10,20,claro,'text10'); //60
    drawStorage(s,'66',534,241,'9',534,227,10,20,claro,'text10'); //60
    drawStorage(s,'67',544,241,'9',544,227,10,20,claro,'text10'); //60
    drawStorage(s,'68',554,241,'9',554,227,10,20,claro,'text10'); //60
    drawStorage(s,'69',564,241,'9',564,227,10,20,claro,'text10'); //60

    drawStorage(s,'57',480,260,'10',454,247,60,19,claro,'text10'); //57
    drawStorage(s,'56',480,279,'10',454,266,60,19,claro,'text10'); //56
    drawStorage(s,'55',480,298,'10',454,285,60,19,claro,'text10'); //55
    drawStorage(s,'54',480,317,'10',454,304,60,19,claro,'text10'); //54
    drawStorage(s,'53',480,336,'10',454,323,60,19,claro,'text10'); //53
    drawStorage(s,'52',480,355,'10',454,342,60,19,claro,'text10'); //52
    drawStorage(s,'51',480,374,'10',454,361,60,19,claro,'text10'); //51
    drawStorage(s,'50',480,393,'10',454,380,60,19,claro,'text10'); //50
    drawStorage(s,'49',480,413,'10',454,399,60,19,claro,'text10'); //49

    drawStorage(s,'48',480,444,'10',454,418,60,38,claro,'text10'); //48
    drawStorage(s,'47',480,483,'10',454,456,60,38,claro,'text10'); //47
    drawStorage(s,'46',480,515,'10',454,494,60,38,claro,'text10'); //46
    drawStorage(s,'45',480,555,'10',454,532,60,38,claro,'text10'); //45
    drawStorage(s,'44',480,583,'10',454,570,60,19,claro,'text10'); //44
    drawStorage(s,'44a',487,623,'10',474,589,40,55,claro,'text10'); //44

    drawStorage(s,'130',537,260,'10',514,247,60,19,claro,'text10'); //130
    drawStorage(s,'131',537,279,'10',514,266,60,19,claro,'text10'); //56
    drawStorage(s,'132',537,298,'10',514,285,60,19,claro,'text10'); //55
    drawStorage(s,'133',537,317,'10',514,304,60,19,claro,'text10'); //54
    drawStorage(s,'134',537,336,'10',514,323,60,19,claro,'text10'); //53
    drawStorage(s,'135',537,355,'10',514,342,60,19,claro,'text10'); //52
    drawStorage(s,'136',537,374,'10',514,361,60,19,claro,'text10'); //51
    drawStorage(s,'137',537,393,'10',514,380,60,19,claro,'text10'); //50
    drawStorage(s,'138',537,413,'10',514,399,60,19,claro,'text10'); //49

    drawStorage(s,'139',537,444,'10',514,418,60,38,claro,'text10'); //48
    drawStorage(s,'140',537,483,'10',514,456,60,38,claro,'text10'); //47
    drawStorage(s,'141',537,515,'10',514,494,60,38,claro,'text10'); //46
    drawStorage(s,'142',537,555,'10',514,532,60,38,claro,'text10'); //45
    drawStorage(s,'143',537,604,'10',514,570,60,74,claro,'text10'); //44

    drawStorage(s,'152',602,241,'9',600,227,20,20,claro,'text10'); //152
    drawStorage(s,'153',622,241,'9',620,227,20,20,claro,'text10'); //153
    drawStorage(s,'154',642,241,'9',640,227,20,20,claro,'text10'); //154
    drawStorage(s,'155',662,241,'9',660,227,20,20,claro,'text10'); //155
    drawStorage(s,'156',682,241,'9',680,227,20,20,claro,'text10'); //156
    drawStorage(s,'157',702,241,'9',700,227,20,20,claro,'text10'); //157

    drawStorage(s,'151',633,286,'9',600,247,80,76,claro,'text10'); //151
    drawStorage(s,'150',633,335,'10',600,323,80,18,claro,'text10'); //150
    drawStorage(s,'149',633,354,'10',600,341,80,18,claro,'text10'); //149
    drawStorage(s,'148',633,371,'10',600,359,80,18,claro,'text10'); //148

    drawStorage(s,'147',633,398,'10',600,377,80,36,claro,'text10'); //147
    drawStorage(s,'146',633,434,'10',600,413,80,36,claro,'text10'); //146
    drawStorage(s,'145',633,470,'10',600,449,80,36,claro,'text10'); //145
    drawStorage(s,'144',633,520,'10',600,485,80,64,claro,'text10'); //144
    drawStorage(s,'144a',630,580,'10',600,549,80,55,claro,'text10'); //145a

    drawStorage(s,'9',697,260,'10',680,247,40,19,claro,'text10'); //9
    drawStorage(s,'10',694,279,'10',680,266,40,19,claro,'text10'); //10
    drawStorage(s,'11',694,298,'10',680,285,40,19,claro,'text10'); //11
    drawStorage(s,'12',694,316,'10',680,304,40,19,claro,'text10'); //12
    drawStorage(s,'13',694,335,'10',680,323,40,18,claro,'text10'); //13
    drawStorage(s,'14',694,354,'10',680,341,40,18,claro,'text10'); //14
    drawStorage(s,'15',694,371,'10',680,359,40,18,claro,'text10'); //15

    drawStorage(s,'16',694,398,'10',680,377,40,36,claro,'text10'); //16
    drawStorage(s,'17',694,434,'10',680,413,40,36,claro,'text10'); //17
    drawStorage(s,'18',694,470,'10',680,449,40,36,claro,'text10'); //18
    drawStorage(s,'19',694,520,'10',680,485,40,64,claro,'text10'); //19
    drawStorage(s,'20',694,580,'10',680,549,40,55,claro,'text10'); //20

    drawStorage(s,'76',543,119,'10',537,111,20,10,claro,'text9'); //76
    drawStorage(s,'77',543,129,'10',537,121,20,10,claro,'text9'); //77
    drawStorage(s,'74',543,139,'10',537,131,20,10,claro,'text9'); //74
    drawStorage(s,'75',543,149,'10',537,141,20,9,claro,'text9'); //75
    drawStorage(s,'72',543,158,'10',537,150,20,10,claro,'text9'); //72
    drawStorage(s,'73',543,168,'10',537,160,20,10,claro,'text9'); //73
    drawStorage(s,'70',543,178,'10',537,170,20,10,claro,'text9'); //70
    drawStorage(s,'71',543,188,'10',537,180,20,10,claro,'text9'); //71

    drawStorage(s,'78',580,150,'10',557,92,56,98,claro,'text10'); //78
    drawStorage(s,'79',635,150,'10',613,92,56,98,claro,'text10'); //79
    drawStorage(s,'80',690,150,'10',669,92,56,98,claro,'text10'); //80
    drawStorage(s,'1',750,150,'10',725,92,56,98,claro,'text10'); //1
    drawStorage(s,'2',805,150,'10',781,92,56,98,claro,'text10'); //2
    drawStorage(s,'3',860,150,'10',837,92,56,98,claro,'text10'); //3
    drawStorage(s,'4',925,150,'10',893,92,73,98,claro,'text10'); //4
    drawStorage(s,'5',990,150,'10',966,92,59,98,claro,'text10'); //5
    drawStorage(s,'6',1051,150,'10',1025,92,58,98,claro,'text10'); //6
    drawStorage(s,'7',1110,150,'10',1083,92,59,98,claro,'text10'); //7
    drawStorage(s,'8',1168,150,'10',1142,92,56,98,claro,'text10'); //8

    drawStorage(s,'36',750,260,'10',745,227,20,57,claro,'text10'); //36
    drawStorage(s,'35',769,260,'10',765,227,19,57,claro,'text10'); //35
    drawStorage(s,'34',788,260,'10',784,227,20,57,claro,'text10'); //34
    drawStorage(s,'33',807,260,'10',804,227,19,57,claro,'text10'); //33
    drawStorage(s,'32',826,260,'10',823,227,19,57,claro,'text10'); //32
    drawStorage(s,'31',846,260,'10',842,227,20,57,claro,'text10'); //31
    drawStorage(s,'30',867,260,'10',862,227,20,57,claro,'text10'); //30
    drawStorage(s,'29',886,260,'10',882,227,19,57,claro,'text10'); //29
    drawStorage(s,'28',905,260,'10',901,227,19,57,claro,'text10'); //28
    drawStorage(s,'27',925,260,'10',920,227,20,57,claro,'text10'); //27
    drawStorage(s,'26',943,260,'10',940,227,19,57,claro,'text10'); //26
    drawStorage(s,'25',963,260,'10',959,227,20,57,claro,'text10'); //25
    drawStorage(s,'24',1000,260,'10',979,227,56,57,claro,'text10'); //24
    drawStorage(s,'23',1060,260,'10',1035,227,56,57,claro,'text10'); //23
    drawStorage(s,'22',1115,260,'10',1091,227,56,57,claro,'text10'); //22
    drawStorage(s,'21',1178,260,'10',1147,227,74,57,claro,'text10'); //21

    drawStorage(s,'37',750,309,'10',745,284,20,38,claro,'text10'); //37
    drawStorage(s,'38',769,309,'10',765,284,19,38,claro,'text10'); //38
    drawStorage(s,'39',788,309,'10',784,284,20,38,claro,'text10'); //39
    drawStorage(s,'40',807,309,'10',804,284,20,38,claro,'text10'); //40
    drawStorage(s,'41',828,309,'10',823,284,20,38,claro,'text10'); //41
    drawStorage(s,'42',846,309,'10',842,284,20,38,claro,'text10'); //42
    drawStorage(s,'43',867,309,'10',862,284,20,38,claro,'text10'); //43
    drawStorage(s,'44',886,309,'10',882,284,20,38,claro,'text10'); //44
    drawStorage(s,'45',905,309,'10',901,284,20,38,claro,'text10'); //45
    drawStorage(s,'46',925,309,'10',920,284,20,38,claro,'text10'); //46
    drawStorage(s,'47',945,309,'10',940,284,20,38,claro,'text10'); //47
    drawStorage(s,'48',965,309,'10',959,284,20,38,claro,'text10'); //48
    drawStorage(s,'49',983,309,'10',979,284,20,38,claro,'text10'); //49
    drawStorage(s,'50',1002,309,'10',998,284,20,38,claro,'text10'); //50
    drawStorage(s,'51',1023,309,'10',1018,284,20,38,claro,'text10'); //51
    drawStorage(s,'52',1053,309,'10',1037,284,38,38,claro,'text10'); //52
    drawStorage(s,'53',1090,309,'10',1075,284,38,38,claro,'text10'); //53
    drawStorage(s,'54',1125,309,'10',1113,284,37,38,claro,'text10'); //54
    drawStorage(s,'55',1163,309,'10',1150,284,38,38,claro,'text10'); //55
    drawStorage(s,'56',1200,309,'10',1188,284,33,38,claro,'text10'); //56

    drawStorage(s,'65',770,388,'10',745,355,67,58,claro,'text10'); //65
    drawStorage(s,'64',860,388,'10',812,355,116,58,claro,'text10'); //64
    drawStorage(s,'63',950,388,'10',928,355,57,58,claro,'text10'); //63
    drawStorage(s,'62',1010,388,'10',985,355,52,58,claro,'text10'); //62
    drawStorage(s,'61',1050,388,'10',1037,355,36,58,claro,'text10'); //61
    drawStorage(s,'60',1085,388,'10',1073,355,36,58,claro,'text10'); //60
    drawStorage(s,'59',1123,388,'10',1109,355,36,58,claro,'text10'); //59
    drawStorage(s,'58',1158,388,'10',1145,355,36,58,claro,'text10'); //58
    drawStorage(s,'57',1194,388,'10',1181,355,36,58,claro,'text10'); //57

    drawStorage(s,'66',800,465,'10',745,413,126,93,claro,'text10'); //66
    drawStorage(s,'67',910,465,'10',871,413,87,93,claro,'text10'); //67
    drawStorage(s,'70',975,455,'10',958,413,47,67,claro,'text10'); //70
    drawStorage(s,'68',965,497,'10',958,480,23,26,claro,'text10'); //68
    drawStorage(s,'69',988,497,'10',981,480,24,26,claro,'text10'); //69

    drawStorage(s,'71',1019,440,'10',1005,413,42,44,claro,'text10'); //71
    drawStorage(s,'72',1070,440,'10',1047,413,62,44,claro,'text10'); //72
    drawStorage(s,'73',1124,440,'10',1109,413,41,44,claro,'text10'); //73
    drawStorage(s,'74',1173,440,'10',1150,413,61,44,claro,'text10'); //74

    drawStorage(s,'78',1055,505,'10',1040,487,45,30,claro,'text10'); //78
    drawStorage(s,'77',1103,505,'10',1085,487,45,30,claro,'text10'); //77
    drawStorage(s,'76',1150,505,'10',1130,487,47,30,claro,'text10'); //76
    drawStorage(s,'75',1188,492,'10',1177,457,34,60,claro,'text10'); //75
    drawStorage(s,'79',1055,537,'10',1040,517,45,30,claro,'text10'); //79
    drawStorage(s,'60',1103,537,'10',1085,517,45,30,claro,'text10'); //80
    drawStorage(s,'81',1150,537,'10',1130,517,47,30,claro,'text10'); //81
    drawStorage(s,'82',1188,549,'10',1177,517,34,59,claro,'text10'); //82

    drawStorage(s,'90',752,563,'10',745,540,26,36,claro,'text10'); //74
    drawStorage(s,'91',752,596,'10',745,576,26,32,claro,'text10'); //74
    drawStorage(s,'92',752,630,'10',745,608,26,35,claro,'text10'); //74
    drawStorage(s,'89',820,590,'10',771,540,109,91,claro,'text10'); //74
    drawStorage(s,'88',910,590,'10',880,540,72,91,claro,'text10'); //74
    drawStorage(s,'87',972,590,'10',952,540,53,91,claro,'text10'); //74
    drawStorage(s,'86',1018,610,'10',1005,576,36,55,claro,'text10'); //74
    drawStorage(s,'85',1063,610,'10',1040,576,55,55,claro,'text10'); //74
    drawStorage(s,'84',1118,610,'10',1095,576,55,55,claro,'text10'); //74
    drawStorage(s,'83',1173,610,'10',1150,576,54,55,claro,'text10'); //74

    //drawStorage(s,'74',1168,150,'10',,,,,claro,'text10'); //74










    $('rect').qtip({
      content: {
          text: 'Storage description HERE!',
          title: function(event, api) {
                  return $(this).attr('id');
                },
          button: true
      },
      hide: {
          event: false
      },
      position: {
        target: 'event'
      },
      style: {
          //classes: 'qtip-bootstrap'
          classes: 'qtip-shadow'
      },
      hide: {
        delay: 1000
      }
    });



  });
</script>
@endsection
