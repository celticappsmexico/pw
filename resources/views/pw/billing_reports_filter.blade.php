@extends('app')
@section('content')
<style media="screen">
  .toggle {
    display: block;
    position: absolute;
    width: 30%;
    background-color: rgba(255, 255, 255, 0.8);
    padding: 15px;
    -webkit-transition: ease-in 0.5s all;
    transition: ease-in 0.5s all;
    -webkit-transform: translateY(-200%);
    -ms-transform: translateY(-200%);
    transform: translateY(-200%);
    min-width: 320px;
    z-index:1000;
    opacity:0;
    filter: alpha(opacity=0); /* For IE8 and earlier */
  }


  .toggle--active {
    opacity: 1;
    margin-top: 10px;
    position: absolute;
    filter: alpha(opacity=100); /* For IE8 and earlier */
    -webkit-transition: ease-in 0.5s all;
    transition: ease-in 0.5s all;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
    z-index:100;
  }
</style>

    <div class="row">
      <div class="col-md-1">
        <button type="button" class="tcon tcon-menu--xcross" aria-label="toggle menu" id="btnFilterMenu">
          <span class="tcon-menu__lines" aria-hidden="true"></span>
          <span class="tcon-visuallyhidden">toggle menu</span>
        </button>
        <div class="toggle">
            <select id="select-sucursales" multiple="multiple" style="display: none">
                @foreach ($warehouses as $warehouse)
                    @foreach ($warehouse->Levels as $Level)
                        <option value="{{$Level->id}}" data-section="{{$warehouse->name}}">
                            {{ $Level->name }}
                        </option>
                    @endforeach
                @endforeach
            </select>
        </div>
      </div>
      <div class="col-md-1">
        <div class="input-group año" style="width: 100px; border-style: solid; border-width:1px;">
          <span class="input-group-addon" style="border-right-style:solid; border-width:1px; border-right-color:black;">
            <i class="glyphicon glyphicon-calendar" style="margin-left:8px;"></i>
          </span>
            <input class="date-own form-control" style="width: 80px; text-align: center;" type="text" id="inputAño" name="inputYear" value="{{$year}}">
        </div>
      </div>
      <div class="col-md-3">
        <button type="button" id="btnFiltrar" class="btn btn-secondary" style="margin-left:20px; border-style:solid; border-width:1px; border-color:black; font-size:15.5px;">{{ trans('billing_reports.search') }}</button>
      </div>
      <div class="col-md-3">
      	<h2 class="report-title">{{Lang::get('billing_reports.billing_reports')}}</h2>
      </div>
    </div>
  <div class="divBillingReports" style="margin-top: 5px;">

  </div>

  <!-- JS  -->
  <script src="{{ asset('assets/js/jquery.tree-multiselect.min.js') }}"></script>
  <script src="{{ asset('assets/js/transformicons.min.js') }}"></script>
  <script type="text/javascript">
    $("#select-sucursales").treeMultiselect();

    $('.tcon-menu--xcross').click(function (){
      var classBtn = $(this).attr('class');
      if (classBtn == 'tcon tcon-menu--xcross') {
        $(this).attr('class', 'tcon tcon-menu--xcross tcon-transform');
        $('.toggle').toggleClass('toggle--active');
      }else {
        $(this).attr('class', 'tcon tcon-menu--xcross');
        $('.toggle').attr('class', 'toggle');
      }
    });

    $(document).ready(function(){
        filtrar();
    });

    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth();
    var d = d.getDay();
    $(function() {
      $('#inputAño').datepicker({
        minViewMode: 2,
        format: 'yyyy',
      });
    });

    $( "#btnFiltrar" ).click(function() {
      filtrar();
    });

    /**
     * Function to filter the billings with a specific year
     */
    function filtrar(){
      $('.tcon-menu--xcross').attr('class', 'tcon tcon-menu--xcross');
      $('.toggle').attr('class', 'toggle');
      route = "{{ route('filterYear', ['*anio*', '*levels*']) }}";
      levels = $('#select-sucursales').val();
      // alert(levels);
      anio = $('#inputAño').val();
      route = route.replace('*anio*', anio);
      route = route.replace('*levels*', levels);
      $('.divBillingReports').html('');
      $('.divBillingReports').load(route);
    }
  </script>
@endsection
