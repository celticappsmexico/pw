<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Maps</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/snap.svg-min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" media="screen" title="no title" charset="utf-8">
    <style media="screen">
      /*
      estilos que hay que mover a otra hoja de estilos
      */
      body{
        font-family: Arial;
        font-size: 10px
      }

      .text12{
        font-size: 12px;
      }
      .text10{
        font-size: 10px;
      }
      .text9{
        font-size: 9px;
        letter-spacing: -1px;
      }
      .text8{
        font-size: 8px;
        letter-spacing: -1px;
      }

      .navbar-brand img{
        height: 30px!important;
        top: -10px!important;
        margin-top: -5px;
      }
    </style>
  </head>
  <body style="width:1366px!important;height:800px!important;">

    <div style="width:100%!important;height:800pximportant;">
      <svg id="mysvg" style="width:100%!important;height:800px!important;"></svg>
    </div>

  </body>
    <script>
    jQuery(document).ready(function($) {
      idLevel = <?php echo $_GET['idLevel']; ?>;
      idBox = <?php echo $_GET['idBox']; ?>;
      var s= Snap('#mysvg');

      function drawStorage(s,t,tx,ty,ts,rx,ry,rw,rh,rc,cl,id){
        var t1 = s.text(tx, ty, t);
        t1.attr({
          color: '#000',
          class: cl
        });

        var rect = s.rect(rx,ry,rw,rh);
        rect.attr({
          fill: rc,
          stroke: '#000',
          strokeWidth: 1,
          id: t,
          dir: id,
        });

      }

      function drawWall(s,xStart,yStart,xEnd,yEnd,lineColor,lineWeigt){
          var line = s.line(xStart, yStart, xEnd,yEnd);

          line.attr({
            stroke: '#000',
            strokeWidth: lineWeigt
          });
      }

      if (idLevel == 1) {
        //acotaciones level 1
        drawWall(s,62, 77, 419,77,'#FFF',2);
        drawWall(s,473, 77, 504,77,'#FFF',2);
        drawWall(s,504, 60, 504,77,'#FFF',2);
        drawWall(s,522, 77, 595,77,'#FFF',2);
        drawWall(s,649, 77, 1223,77,'#FFF',2);
        drawWall(s,1222, 78, 1222, 644,'#FFF',2);
        drawWall(s,712, 645, 1222, 643,'#FFF',2);
        drawWall(s,649, 645, 695, 645,'#FFF',2);
        drawWall(s,695, 644, 695, 665,'#FFF',2);
        drawWall(s,473, 644, 595, 644,'#FFF',2);
        drawWall(s,62, 644, 417, 644,'#FFF',2);
        drawWall(s,104, 78, 104, 643,'#FFF',2);

        drawStorage(s,'',0,0,'0',419,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
        drawStorage(s,'',0,0,'0',595,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
        drawStorage(s,'',0,0,'0',595,639,54,5,'rgba(255, 255, 255, 0.3)','text12');
        drawStorage(s,'',0,0,'0',417,639,56,5,'rgba(255, 255, 255, 0.3)','text12');
        // dibujamos la oficina:
        drawStorage(s,'OFFICE',235,130,'40',104,78,305,112,'rgba(111, 111, 111, 0.3)','text12');

      }

      if (idLevel == 2) {
          drawWall(s,62, 77, 193,77,'#FFF',2); //INICIAL ARRIBA IZQUIERDA 225 30 px de cada puerta
          drawWall(s,223, 77, 770,77,'#FFF',2);
          drawWall(s,800, 60, 800, 77,'#FFF',2);
          drawWall(s,800, 77, 1223,77,'#FFF',2);
          drawWall(s,800, 77, 1240,77,'#FFF',2);
          drawWall(s,1222, 78, 1222, 644,'#FFF',2);
          drawWall(s,1010, 645, 1240, 643,'#FFF',2);
          drawWall(s,980, 645, 62, 643,'#FFF',2);
          drawWall(s,980, 645, 980, 660,'#FFF',2);
          drawWall(s,104, 78, 104, 643,'#FFF',2);

          //bodega lado derecho

          drawWall(s,1000, 77, 1000, 187,'#FFF',2);
          drawWall(s,999, 187, 1100, 187,'#FFF',2);
          drawWall(s,1122, 187, 1222, 187,'#FFF',2);
          //RECTANGULO ABAJO
          drawWall(s,1122, 186, 1122, 211,'#FFF',2);
          drawWall(s,1123, 202, 1098, 202,'#FFF',2);
          drawWall(s,1122, 227, 1122, 247,'#FFF',2);
          drawWall(s,1122, 247, 1132, 257,'#FFF',2);
          drawWall(s,1132, 257, 1222, 257,'#FFF',2);

          drawWall(s,1000, 87, 1077, 87,'#FFF',2);
          drawWall(s,1147, 87, 1223, 87,'#FFF',2);
          //RECTANGULO INTERNO
          drawWall(s,1077, 82, 1147, 82,'#FFF',2);
          drawWall(s,1076, 81, 1076, 162,'#FFF',2);
          //drawWall(s,1076, 81, 1076, 162,'#FFF',2);

          //PRIMER CELDA
          drawWall(s,1076, 102, 1100, 102,'#FFF',2);drawWall(s,1125, 102, 1147, 102,'#FFF',2);
          drawWall(s,1130, 82, 1130, 102,'#FFF',2);// cuadro pequeño
          drawWall(s,1147, 81, 1147, 110,'#FFF',2);
          //SEGUNDA CELDA
          drawWall(s,1076, 122, 1126, 122,'#FFF',2);
          drawWall(s,1125, 122, 1125, 142,'#FFF',2);
          drawWall(s,1124, 142, 1140, 142,'#FFF',2);
          //TERCERA CELDA
          drawWall(s,1076, 142, 1100, 142,'#FFF',2);
          drawWall(s,1099, 144, 1118, 144,'#FFF',2);
          //CUARTA CELDA
          drawWall(s,1076, 162, 1126, 162,'#FFF',2);
          //EXTREMO DERECHO
          drawWall(s,1147, 135, 1147, 163,'#FFF',2);
          drawWall(s,1148, 135, 1170, 135,'#FFF',2);
          drawWall(s,1147, 150, 1223, 150,'#FFF',2);

          drawWall(s,1147, 182, 1147, 187,'#FFF',2);
          drawWall(s,1127, 182, 1148, 182,'#FFF',2);

          drawWall(s,1076, 182, 1076, 187,'#FFF',2);
          drawWall(s,1075, 182, 1096, 182,'#FFF',2);
      }

      function colorStorageBk(v){
        color = '';
        switch (v) {
          case 'rojo':
              color = 'rgba(255, 23, 20, .5)';
            break;
          case 'verde':
              color = 'rgba(0, 255, 18, .5)';
            break;
          case 'amarillo':
              color = 'rgba(250, 255, 0, .5)';
            break;
          case 'claro':
              color = 'rgba(255, 255, 255, .0)';
            break;
          default:
              color = 'rgba(255, 255, 255, .0)';
        }
        return color;
      }

      function colorStorage(idBox){
        val = idBox;
        $('rect[dir="'+val+'"]').removeAttr('fill');
        $('rect[dir="'+val+'"]').attr('fill','rgba(120, 255, 0, 0.8');
      }

      function loadStorages(idLevel){
        cad = 'level='+idLevel;
        $.ajax({
          url: '{{ route('wsGetStorages') }}',
          type: 'POST',
          data: cad
        })
        .done(function(data) {
          printStorages(data.storages);
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
      }

      function printStorages(data){
        $.each(data, function(i, item) {
          //console.log("probandoCODIGO");
          //console.info(data[i].id);
          drawStorage(s,data[i].alias,data[i].text_posx,data[i].text_posy,'10',data[i].rect_posx,data[i].rect_posy,data[i].rect_width,data[i].rect_height,colorStorageBk(data[i].rect_background),data[i].class,data[i].id);
          //return false;
        });
        colorStorage(idBox);
      }

      loadStorages(idLevel);


  });


</script>
</html>
