@extends('app')
@section('content')

  <div class="row">
    <div class="col-md-9">
        <h2>{{ trans('client.client-Oportunitiespage_title') }}</h2>
    </div>
    <div class="col-md-3" style="text-align: right">
      <a href="javascript:void(0);" class="btn btn-primary btnNewClient"><i class="fa fa-user-plus"></i> {{ trans('client.newUser_btn') }}</a>
    </div>
  </div>

  <div class="card">
    <div class="card-head style-default">
      <header>{{ trans('client.search_title') }}</header>
    </div>
    <div class="card-body">
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findUsername" id="findUsername" class="form-control" value="" required="required" placeholder="{{ trans('client.username_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findMail" id="findMail" class="form-control" value="" required="required" placeholder="{{ trans('client.email_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findName" id="findName" class="form-control" value="" required="required" placeholder="{{ trans('client.name_lbl') }}" title="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findLastname" id="findLastname" class="form-control" value="" required="required" placeholder="{{ trans('client.last_lbl') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="findCompany" id="findCompany" class="form-control" value="" required="required" placeholder="{{ trans('client.company_lbl') }}" title="">
        </div>
      </div>
     
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="startDate" id="startDate" class="form-control datepicker_user" required="required" placeholder="{{ trans('client.start_date') }}" title="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <input type="text" name="endDate" id="endDate" class="form-control datepicker_user" required="required" placeholder="{{ trans('client.end_date') }}" title="">
        </div>
      </div>
      
      <div class="col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-info btnSearchClients">{{ trans('client.search_lbl') }}</button>
        </div>
      </div>
    </div>
  </div>

  <table class="table table-hover" id="tblUSers">
	   <thead>
       <tr>
         <th>
           {{ trans('client.username_lbl') }}
         </th>
         <th>
           {{ trans('client.name_lbl') }}
         </th>
         <th>
           {{ trans('client.last_lbl') }}
         </th>
         <th>
         	{{ trans('client.date_reminder') }}
         </th>
         <th>
           {{ trans('client.type_lbl') }}
         </th>
         <th>
           {{ trans('client.role_lbl') }}
         </th>
         <th>
           {{ trans('client.company_lbl') }}
         </th>
         <th>
           {{ trans('client.actions_lbl') }}
         </th>
       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>

  <script>
  $(document).on('click', '.btnSearchClients', function(event) {
    cargaGridUsers();
  });


  function cargaGridUsers(){
          username = $('#findUsername').val();
          email = $('#findMail').val();
          name = $('#findName').val();
          last = $('#findLastname').val();
          company = $('#findCompany').val();
          startDate = $('#startDate').val();
          endDate = $('#endDate').val();
          
          //uid = 0;
          showLoader();
          cad = 'username='+username+'&email='+email+'&name='+name+'&last='+last+'&company='+company+'&start_date='+startDate+'&end_date='+endDate+'&_token='+token;
          $('#tblUSers').dataTable().fnDestroy();
          $('#tblUSers tbody').empty();
          $.ajax({
              type: "POST",
              url: "{{ route('listOportunities') }}",
              data: cad,
              success:function(data) {
                  $('#tblUSers tbody').append(data);
                  hideLoader();
                  $('#tblUSers').dataTable({
                      "language": {
                          "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                          "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                          "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                          "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                          "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                          "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                          "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                          "sInfoPostFix":    "",
                          "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                          "oPaginate": {
                              "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                              "sLast":     "{{ trans('client.dt_last_lbl') }}",
                              "sNext":     "{{ trans('client.dt_next_lbl') }}",
                              "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                          },
                          "oAria": {
                              "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                              "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                          }

                      }
                  });

                  //$('#load').slideUp(100);
              }
          });
      }

      cargaGridUsers();

      $('.datepicker_user').datetimepicker({
    	    format: 'YYYY-MM-DD',
    	    allowInputToggle: true,
    	    //minDate: moment(),
    	  });
  </script>
@endsection
