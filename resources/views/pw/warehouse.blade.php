@extends('app')
@section('content')

  <div class="row">
    <div class="col-md-9">
        <h2>{{ trans('warehouse.warehouse_title') }}</h2>
    </div>
    <div class="col-md-3" style="text-align: right">
      <a href="javascript:void(0);" class="btn btn-primary btnNewWarehouse"><i class="fa fa-user-plus"></i> {{ trans('warehouse.new_warehouse') }}</a>
    </div>
  </div>

  <table class="table table-hover" id="tblWarehouse">
	   <thead>
       <tr>
         <th>
           {{ trans('warehouse.name') }}
         </th>
         <th>
           {{ trans('warehouse.country') }}
         </th>
         <th>
           {{ trans('warehouse.full_address') }}
         </th>
         <th>
           {{ trans('warehouse.actions_lbl') }}
         </th>
       </tr>
     </thead>
     <tbody>
     </tbody>
   </table>

  <script>
  function cargaGridWarehouse(){
          //uid = 0;
          showLoader();
          cad = '_token='+token;
          $('#tblWarehouse').dataTable().fnDestroy();
          $('#tblWarehouse tbody').empty();
          $.ajax({
              type: "POST",
              url: "{{ route('getBuildingList') }}",
              data: cad,
              success:function(data) {
                  $('#tblWarehouse tbody').append(data);
                  hideLoader();
                  $('#tblWarehouse').dataTable({
                      "language": {
                          "sProcessing":     "{{ trans('client.dt_processing_lbl') }}",
                          "sLengthMenu":     "{{ trans('client.dt_show_lbl') }} _MENU_ {{ trans('client.dt_show_lbl') }}",
                          "sZeroRecords":    "{{ trans('client.dt_noResults_lbl') }}",
                          "sEmptyTable":     "{{ trans('client.dt_noResults_lbl') }}",
                          "sInfo":           "{{ trans('client.dt_show_lbl') }} _START_ {{ trans('client.dt_to_lbl') }} _END_ {{ trans('client.dt_of_lbl') }} _TOTAL_ ",
                          "sInfoEmpty":      "{{ trans('client.dt_show_lbl') }} 0 {{ trans('client.dt_to_lbl') }} 0 {{ trans('client.dt_of_lbl') }} 0",
                          "sInfoFiltered":   "({{ trans('client.dt_filtering_lbl') }} _MAX_ )",
                          "sInfoPostFix":    "",
                          "sSearch":         "{{ trans('client.dt_find_lbl') }}:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "{{ trans('client.dt_loading_lbl') }}",
                          "oPaginate": {
                              "sFirst":    "{{ trans('client.dt_first_lbl') }}",
                              "sLast":     "{{ trans('client.dt_last_lbl') }}",
                              "sNext":     "{{ trans('client.dt_next_lbl') }}",
                              "sPrevious": "{{ trans('client.dt_prev_lbl') }}"
                          },
                          "oAria": {
                              "sSortAscending":  ": {{ trans('client.dt_orderAsc_lbl') }}",
                              "sSortDescending": ": {{ trans('client.dt_orderDesc_lbl') }}"
                          }

                      }
                  });

                  //$('#load').slideUp(100);
              }
          });
      }

      cargaGridWarehouse();

  
  </script>
@endsection
