@extends('app')
@section('content')
<style>
	h4{ text-align:center; }
</style>
    <div class="row">
        <div class="col-md-2">
            <a href="{{ url('billing_reports') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>{{Lang::get('billing_reports.billing_reports')}}</h4>
        </div>
        <div class="col-md-2">
            <a href="{{ url('warehouseReport') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>{{Lang::get('reports.occupancy_rate_by_size')}}</h4>
        </div>
        <div class="col-md-2">
            <a href="{{ url('historyOccupancyRate') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>{{Lang::get('reports.occupancy_rate_history')}}</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('bad_payers')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>{{Lang::get('reports.bad_payers')}}</h4>
        </div>
    
        <div class="col-md-2">
            <a href="{{url('invoices_report',[0])}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>{{Lang::get('reports.invoices')}}</h4>
        </div>
        
        <div class="col-md-2">
            <a href="{{url('invoices_report',[1])}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Paragons</h4>
        </div>
        
        <div class="col-md-2">
            <a href="{{ route('tenantsReport') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
			<h4>Tenants</h4>
        </div>
        <div class="col-md-2">
            <a href="{{ route('boxesReport') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
			<h4>Boxes</h4>
        </div>
        <div class="col-md-2">
            <a href="{{ route('depositsReport') }}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Deposits</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('insuranceReport')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Insurance</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('correctedInvoicesReport')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Corrected Invoices</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('paragonCancelledReport')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Paragon Cancelled</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('prepaymentReport')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Prepayments</h4>
        </div>
        <!--
        <div class="col-md-2">
            <a href="{{url('braintreeReport')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Braintree Errors</h4>
        </div>
        -->
        <div class="col-md-2">
            <a href="{{url('insuranceDetails')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Insurance Report</h4>
        </div>
        <div class="col-md-2">
            <a href="{{url('jpk_fa')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>JPK FA</h4>
        </div>

        <div class="col-md-2">
            <a href="{{url('administrator/app_users')}}"><img src="{{asset('assets/images/app_pw.jpeg')}}" alt="" id="imgReport"/></a>
            <h4>App Users</h4>
        </div>

        <div class="col-md-2">
            <a href="{{url('report_payments')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Payments</h4>
        </div>

        <div class="col-md-2">
            <a href="{{url('weekly_report')}}"><img src="{{asset('assets/img/report-icon.png')}}" alt="" id="imgReport"/></a>
            <h4>Weekly Report</h4>
        </div>
    </div>
@endsection
