@extends('app')
@section('content')

<script type="text/javascript">

  $(document).ready(function() {

    function loadSettings(){
      showLoader();
      $.ajax({
          method: "GET",
          url: "{{ route('getSettings') }}",
        })
        .done(function(data) {

          //console.log(data);
          $('#dbVat').val(data.vat);
          $('#dbInsurance').val(data.insurance);
          $('#dbExpedicion').val(data.place);
          $('#dbAddress').val(data.address);
          $('#dbNip').val(data.nip);
          $('#dbBank').val(data.bank);
          $('#dbAccount').val(data.account);
          $('#dbInvoiceNumber').val(data.invoiceNumber);
          $('#dbSeller').val(data.seller);


          hideLoader();

        })
        .fail(function() {
          //alert( "error" );
        })
        .always(function() {
          //alert( "complete" );
        });
    }

    loadSettings();

    $(document).on('click', '.sendForm', function() {
      showLoader();
      cad = $("#updateSettings").serialize();
      //console.info(cad);

      $.ajax({
          method: "POST",
          url: "{{ route('updateSettings') }}",
          data: cad,
        })
        .done(function(data) {
          //console.info(data);
          if(data.res){
            toastr["success"]("{{ trans('settings.update_ok') }}");
          }
          hideLoader();

        })
        .fail(function() {
          //alert( "error" );
        })
        .always(function() {
          //alert( "complete" );
        });

    })

  });


</script>

<h1>{{ trans('settings.settings_title') }}</h1>
<div class="col-sm-6">
  <form action="" name="updateSettings" id="updateSettings" class="frmUpdateSettings form" autocomplete=”off”>
    <div class="card">
      <div class="card-head style-default">
  			<header>{{ trans('settings.system') }}</header>
  		</div>

      <div class="card-body">
          <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

          <div class="row">

            <div class="form-group" id="g_vat">
                <label>{{ trans('settings.vat') }}</label>
                {!! Form::input('number', 'vat','' , ['class'=> 'form-control vat', 'id' => 'dbVat']) !!}
            </div>

            <div class="form-group" id="g_insurance">
                <label>{{ trans('settings.insurance') }}</label>
                {!! Form::input('number', 'insurance','' , ['class'=> 'form-control insurance', 'id' => 'dbInsurance']) !!}
            </div>


          </div>

          <div class="form-footer">
            <a href="javascript:void(0);" class="btn btn-primary sendForm" name="button">{{ trans('settings.btn_update') }}</a>
          </div>
      </div>
    </div>

    <div class="card">

      <div class="card-head style-default">
  			<header>{{ trans('settings.invoice') }}</header>
  		</div>

      <div class="card-body">

        <div class="row">

          <div class="form-group" id="g_invoiceNumber">
              <label>{{ trans('settings.invoice_number') }}</label>
              {!! Form::input('text', 'invoiceNumber','' , ['class'=> 'form-control invoiceNumber', 'id' => 'dbInvoiceNumber']) !!}
          </div>

          <div class="form-group" id="g_seller">
              <label>{{ trans('settings.seller') }}</label>
              {!! Form::input('text', 'seller','' , ['class'=> 'form-control seller', 'id' => 'dbSeller']) !!}
          </div>

          <div class="form-group" id="g_place">
              <label>{{ trans('settings.expedicion') }}</label>
              {!! Form::input('text', 'expedicion','' , ['class'=> 'form-control expedicion', 'id' => 'dbExpedicion']) !!}
          </div>

          <div class="form-group" id="g_address">
              <label>{{ trans('settings.address') }}</label>
              {!! Form::input('text', 'address','' , ['class'=> 'form-control address', 'id' => 'dbAddress']) !!}
          </div>

          <div class="form-group" id="g_nip">
              <label>{{ trans('settings.nip') }}</label>
              {!! Form::input('text', 'nip','' , ['class'=> 'form-control nip', 'id' => 'dbNip']) !!}
          </div>

          <div class="form-group" id="g_bank">
              <label>{{ trans('settings.bank') }}</label>
              {!! Form::input('text', 'bank','' , ['class'=> 'form-control bank', 'id' => 'dbBank']) !!}
          </div>

          <div class="form-group" id="g_account">
              <label>{{ trans('settings.account') }}</label>
              {!! Form::input('text', 'account','' , ['class'=> 'form-control account', 'id' => 'dbAccount']) !!}
          </div>

        </div>


        <div class="form-footer">
          <a href="javascript:void(0);" class="btn btn-primary sendForm" name="button">{{ trans('settings.btn_update') }}</a>
        </div>
      </div>
    </div>
  </form>

</div>

@endsection
