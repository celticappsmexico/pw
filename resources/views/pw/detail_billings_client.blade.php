@extends('app')
@section('content')
  <div class="divDetailBillingsClient">
      <div class="row">
          <button type="button" class="btn btn-info btn-lg btnBack" name="button" style="margin-left: 1%; margin-bottom: 0.5%;">Back</button>
      </div>
      <table class="table table-hover no-footer dataTable" id="tableBillingClientDetail">
          <thead>
              <tr>
                  <th>{{ trans('billing_reports.name') }}</th>
                  <th>{{ trans('billing_reports.billed') }}</th>
                  <th>{{ trans('billing_reports.payed') }}</th>
                  <th>{{ trans('billing_reports.balance') }}</th>
                  <th>{{ trans('billing_reports.storage') }}</th>
                  <th>{{ trans('billing_reports.level') }}</th>
                  <th>{{ trans('billing_reports.invoice_date') }}</th>
              </tr>
          </thead>
          @foreach($billings as $billing)
              <tr>
                  <td>{{ $billing->user->name }}</td>
                  <td>{{ $billing->cargo }} pln</td>
                  <td>{{ $billing->abono }} pln</td>
                  <td>{{ $billing->balance }} pln</td>
                  <td>{{ $billing->storages->alias }}</td>
                  <td>{{ $billing->storages->level->name }}</td>
                  <td>{{ $billing->pay_month }}</td>
              </tr>
          @endforeach
          <tr>
              <td>{{ trans('billing_reports.total') }}</td>
              <td>{{ $totalCharges }} pln</td>
              <td>{{ $totalAmounts }} pln</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
      </table>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#tableBillingClientDetail').DataTable( {
          "pagingType": "full_numbers",
          "order": [ 6, 'desc' ],
      } );
    });
    $('.btnBack').click(function(){
        window.history.back();
    });
  </script>
@endsection
