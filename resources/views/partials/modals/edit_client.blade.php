<div class="modal fade" id="modalUpdateClient" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title" id="">{{ trans('client.updateClient_title') }} <span class="lblClientType"></span></h4>
      </div>
      <div class="modal-body">
          <div class="frmUpdateClientStep1">

            <div class="form-group">
              <label for="">{{ trans('client.accessLevel_lbl') }}</label>
              <select id="cboUserLevel" name="cboUserLevel" class="form-control">
                @foreach ($roles as $rol)
                <option value="{{$rol->id}}">{{$rol->name}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="">{{ trans('client.clientType_lbl') }}</label>
              <select id="cboClientType" name="cboClientType" class="form-control">
                @foreach ($userTypes as $ut)
                <option value="{{$ut->id}}">{{$ut->name}}</option>
                @endforeach
              </select>
            </div>

          </div>
          <div class="frmUpdateClientStep2">

            <!-- form new client person -->



            <form action="" name="frmUpdateClientPerson" id="frmUpdateClientPerson" class="frmUpdateClient" autocomplete=”off”>

              <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
              <input type="hidden" name="id" id="id" class="id">

              <div class="form-group" id="g_username">
                  <label>{{ trans('client.username_lbl') }}</label>
                  {!! Form::input('text', 'username', '', ['class'=> 'form-control username', 'id' => 'usernameClientPerson', 'readonly' => 'true']) !!}
              </div>

                <div class="form-group" id="g_name">
                    <label>{{ trans('form.label.name') }}</label>
                    {!! Form::input('text', 'name', '', ['class'=> 'form-control name', 'id' => 'name']) !!}
                </div>

                <div class="form-group" id="g_lastName">
                    <label>{{ trans('client.last_lbl') }}</label>
                    {!! Form::input('text', 'lastName', '', ['class'=> 'form-control lastName', 'id' => 'lastName']) !!}
                </div>

                <div class="form-group" id="g_birthday">
                    <label>{{ trans('client.birthday_lbl') }}</label>
                    {!! Form::input('text', 'birthday', '', ['class'=> 'form-control birthday datepicker', 'id' => 'birthday']) !!}
                </div>

                <fieldset>
                  <legend>{{ trans('client.address_lbl') }}</legend>

                  <div class="form-group" id="g_street">
                      <label>{{ trans('client.street_lbl') }}</label>
                      {!! Form::input('text', 'street', '', ['class'=> 'form-control street', 'id' => 'street']) !!}
                  </div>

                  <div class="form-group" id="g_number">
                      <label>{{ trans('client.number_lbl') }}</label>
                      {!! Form::input('text', 'number', '', ['class'=> 'form-control number', 'id' => 'number']) !!}
                  </div>

                  <div class="form-group" id="g_apartmentNumber">
                      <label>{{ trans('client.apartment_lbl') }}</label>
                      {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
                  </div>

                  <div class="form-group" id="g_postCode">
                      <label>{{ trans('client.postCode_lbl') }}</label>
                      {!! Form::input('text', 'postCode', '', ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
                  </div>

                  <div class="form-group" id="g_city">
                      <label>{{ trans('client.city_lbl') }}</label>
                      {!! Form::input('text', 'city', '', ['class'=> 'form-control city', 'id' => 'city']) !!}
                  </div>

                  <div class="form-group" id="g_country">
                    <label for="">{{ trans('client.country_lbl') }}</label>
                    <select id="country" name="country" id="country" class="form-control country">
                      @foreach ($countries as $country)
                      <option value="{{$country->id}}">{{$country->name}}</option>
                      @endforeach
                    </select>
                  </div>

                </fieldset>

                <div class="form-group" id="g_peselNumber">
                    <label>{{ trans('client.pesel_lbl') }}</label>
                    {!! Form::input('text', 'peselNumber', '', ['class'=> 'form-control peselNumber', 'id' => 'peselNumber']) !!}
                </div>

                <div class="form-group" id="g_idNumber">
                    <label>{{ trans('client.id_lbl') }}</label>
                    {!! Form::input('text', 'idNumber', '', ['class'=> 'form-control idNumber', 'id' => 'idNumber']) !!}
                </div>

                <div class="form-group" id="g_phone">
                    <label>{{ trans('client.phone_lbl') }}</label>
                    {!! Form::input('text', 'phone', '', ['class'=> 'form-control phone', 'id' => 'phone']) !!}
                </div>

                <div class="form-group" id="g_phone">
                    <label>{{ trans('client.phone_lbl') }} (2)</label>
                    {!! Form::input('text', 'phone2', '', ['class'=> 'form-control phone2', 'id' => 'phone2']) !!}
                </div>

                {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}

                <div class="form-group" id="g_email">
                    <label>{{ trans('client.email_lbl') }} (1)</label>
                    {!! Form::input('email', 'email', '', ['class'=> 'form-control email', 'id' => 'emailClientPerson', 'autocomplete' => 'false']) !!}
                </div>

                <div class="form-group" id="g_email">
                    <label>{{ trans('client.email_lbl') }} (2)</label>
                    {!! Form::input('email', 'email2', '', ['class'=> 'form-control email2', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                </div>

                <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                    <label>
                       <input name="chkOportunity" class="chkOportunity" type="checkbox" value="1" checked>
                      <span>{{ trans('client.oportunity') }}</span>
                  </label>
                </div>

                <div class="form-group" id="g_dateReminder">
                  <label>{{ trans('client.oportunity_reminder') }}</label>
                  {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                </div>

                <div class="form-group" id="g_notes">
                    <label>{{ trans('client.notes') }}</label>
                    {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                </div>
                
                <div class="form-group" id="g_notes">
                      <label>{{ trans('client.accountant_number') }}</label>
                      <input type="text" name="accountant_number" id="accountant_number" class="form-control" autocomplete="off" />
                </div>
                
                <div class="form-group" id="g_notes">
                      <label>{{ trans('client.accountant_code') }}</label>
                      <input type="text" name="accountant_code" id="accountant_code" class="form-control" autocomplete="off" />
                </div>

                <div class="form-footer">
                  <button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                  <button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
                </div>

              </form>

              <!-- form new client person -->

              <form action="" name="frmUpdateClientOnePerson" id="frmUpdateClientOnePerson" class="frmUpdateClient" autocomplete=”off”>

                {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}
                <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
                <input type="hidden" name="id" id="id" class="id">

                <div class="form-group" id="g_username">
                    <label>{{ trans('client.username_lbl') }}</label>
                    {!! Form::input('text', 'username', '', ['class'=> 'form-control username', 'id' => 'usernameClientOnePerson', 'readonly' => 'true']) !!}
                </div>

                  <div class="form-group" id="g_name">
                      <label>{{ trans('form.label.name') }}</label>
                      {!! Form::input('text', 'name', '', ['class'=> 'form-control name', 'id' => 'name']) !!}
                  </div>

                  <div class="form-group" id="g_lastName">
                      <label>{{ trans('client.last_lbl') }}</label>
                      {!! Form::input('text', 'lastName', '', ['class'=> 'form-control lastName', 'id' => 'lastName']) !!}
                  </div>

                  <div class="form-group" id="g_companyName">
                      <label>{{ trans('client.company_lbl') }}</label>
                      {!! Form::input('text', 'companyName', '', ['class'=> 'form-control companyName', 'id' => 'companyName']) !!}
                  </div>

                  <div class="form-group" id="g_birthday">
                      <label>{{ trans('client.birthday_lbl') }}</label>
                      {!! Form::input('text', 'birthday', '', ['class'=> 'form-control birthday datepicker', 'id' => 'birthday']) !!}
                  </div>

                  <fieldset>
                    <legend>{{ trans('client.address_lbl') }}</legend>

                    <div class="form-group" id="g_street">
                        <label>{{ trans('client.street_lbl') }}</label>
                        {!! Form::input('text', 'street', '', ['class'=> 'form-control street', 'id' => 'street']) !!}
                    </div>

                    <div class="form-group" id="g_number">
                        <label>{{ trans('client.number_lbl') }}</label>
                        {!! Form::input('text', 'number', '', ['class'=> 'form-control number', 'id' => 'number']) !!}
                    </div>

                    <div class="form-group" id="g_apartmentNumber">
                        <label>{{ trans('client.apartment_lbl') }}</label>
                        {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
                    </div>

                    <div class="form-group" id="g_postCode">
                        <label>{{ trans('client.postCode_lbl') }}</label>
                        {!! Form::input('text', 'postCode', '', ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
                    </div>

                    <div class="form-group" id="g_city">
                        <label>{{ trans('client.city_lbl') }}</label>
                        {!! Form::input('text', 'city', '', ['class'=> 'form-control city', 'id' => 'city']) !!}
                    </div>

                   <div class="form-group" id="g_country">
                      <label for="">{{ trans('client.country_lbl') }}</label>
                      <select id="country" name="country" id="country" class="form-control country">
                        @foreach ($countries as $country)
                        <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>

                  </fieldset>


                  <div class="form-group" id="g_nipNumber">
                      <label>{{ trans('client.nip_lbl') }}</label>
                      {!! Form::input('text', 'nipNumber', '', ['class'=> 'form-control nipNumber', 'id' => 'nipNumber']) !!}
                  </div>

                  <div class="form-group" id="g_regonNumber">
                      <label>{{ trans('client.regon_lbl') }}</label>
                      {!! Form::input('text', 'regonNumber', '', ['class'=> 'form-control regonNumber', 'id' => 'regonNumber']) !!}
                  </div>

                  <div class="form-group" id="g_phone">
                      <label>{{ trans('client.phone_lbl') }} (1)</label>
                      {!! Form::input('text', 'phone', '', ['class'=> 'form-control phone', 'id' => 'phone']) !!}
                  </div>

                  <div class="form-group" id="g_phone">
                      <label>{{ trans('client.phone_lbl') }} (2)</label>
                      {!! Form::input('text', 'phone2', '', ['class'=> 'form-control phone2', 'id' => 'phone2']) !!}
                  </div>

                  <div class="form-group" id="g_email">
                      <label>{{ trans('client.email_lbl') }} (1)</label>
                      {!! Form::input('email', 'email', '', ['class'=> 'form-control email', 'id' => 'emailClientOnePerson', 'autocomplete' => 'false']) !!}
                  </div>

                  <div class="form-group" id="g_email">
                      <label>{{ trans('client.email_lbl') }} (2)</label>
                      {!! Form::input('email', 'email2', '', ['class'=> 'form-control email2', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                  </div>

                  <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                      <label>
                         <input name="chkOportunity"  class="chkOportunity" type="checkbox" value="1" checked>
                        <span>{{ trans('client.oportunity') }}</span>
                    </label>
                  </div>

                  <div class="form-group" id="g_dateReminder">
                    <label>{{ trans('client.oportunity_reminder') }}</label>
                    {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                  </div>

                  <div class="form-group" id="g_notes">
                      <label>{{ trans('client.notes') }}</label>
                      {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                  </div>
                  
                  <div class="form-group" id="g_notes">
                      <label>{{ trans('client.accountant_number') }}</label>
                      <input type="text" name="accountant_number" id="accountant_number" class="form-control" autocomplete="off" />
                  </div>
                  
                  <div class="form-group" id="g_notes">
	                      <label>{{ trans('client.accountant_code') }}</label>
	                      <input type="text" name="accountant_code" id="accountant_code" class="form-control" autocomplete="off" />
	                </div>

                  <div class="form-footer">
                    <button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                    <button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
                  </div>

                </form>

                <!-- form new client person -->

                <form action="" name="frmUpdateClientCompany" id="frmUpdateClientCompany" class="frmUpdateClient" >

                  {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                  {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}
                  <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">
                  <input type="hidden" name="id" id="id" class="id">

                  <div class="form-group" id="g_username">
                      <label>{{ trans('client.username_lbl') }}</label>
                      {!! Form::input('text', 'username', '', ['class'=> 'form-control username', 'id' => 'usernameClientCompany', 'readonly' => 'true']) !!}
                  </div>

                    <div class="form-group" id="g_companyName">
                        <label>{{ trans('client.company_lbl') }}</label>
                        {!! Form::input('text', 'companyName', '', ['class'=> 'form-control companyName', 'id' => 'companyName']) !!}
                    </div>

                    <fieldset>
                      <legend>{{ trans('client.address_lbl') }}</legend>

                      <div class="form-group" id="g_street">
                          <label>{{ trans('client.street_lbl') }}</label>
                          {!! Form::input('text', 'street', '', ['class'=> 'form-control street', 'id' => 'street']) !!}
                      </div>

                      <div class="form-group" id="g_number">
                          <label>{{ trans('client.number_lbl') }}</label>
                          {!! Form::input('text', 'number', '', ['class'=> 'form-control number', 'id' => 'number']) !!}
                      </div>

                      <div class="form-group" id="g_apartmentNumber">
                          <label>{{ trans('client.apartment_lbl') }}</label>
                          {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control apartmentNumber', 'id' => 'apartmentNumber']) !!}
                      </div>

                      <div class="form-group" id="g_postCode">
                          <label>{{ trans('client.postCode_lbl') }}</label>
                          {!! Form::input('text', 'postCode', '', ['class'=> 'form-control postCode', 'id' => 'postCode']) !!}
                      </div>

                      <div class="form-group" id="g_city">
                          <label>{{ trans('client.city_lbl') }}</label>
                          {!! Form::input('text', 'city', '', ['class'=> 'form-control city', 'id' => 'city']) !!}
                      </div>

                      <div class="form-group" id="g_country">
                        <label for="">{{ trans('client.country_lbl') }}</label>
                        <select id="country" name="country" id="country" class="form-control country">
                          @foreach ($countries as $country)
                          <option value="{{$country->id}}">{{$country->name}}</option>
                          @endforeach
                        </select>
                      </div>

                    </fieldset>

                    <div class="form-group" id="g_courtNumber">
                        <label>{{ trans('client.courtNumber_lbl') }}</label>
                        {!! Form::input('text', 'courtNumber', '', ['class'=> 'form-control courtNumber', 'id' => 'courtNumber']) !!}
                    </div>

                    <div class="form-group" id="g_courtPlace">
                        <label>{{ trans('client.courtPlace_lbl') }}</label>
                        {!! Form::input('text', 'courtPlace', '', ['class'=> 'form-control courtPlace', 'id' => 'courtPlace']) !!}
                    </div>

                    <div class="form-group" id="g_krsNumber">
                        <label>{{ trans('client.krs_lbl') }}</label>
                        {!! Form::input('text', 'krsNumber', '', ['class'=> 'form-control krsNumber', 'id' => 'krsNumber']) !!}
                    </div>

                    <div class="form-group" id="g_nipNumber">
                        <label>{{ trans('client.nip_lbl') }}</label>
                        {!! Form::input('text', 'nipNumber', '', ['class'=> 'form-control nipNumber', 'id' => 'nipNumber']) !!}
                    </div>

                    <div class="form-group" id="g_regonNumber">
                        <label>{{ trans('client.regon_lbl') }}</label>
                        {!! Form::input('text', 'regonNumber', '', ['class'=> 'form-control regonNumber', 'id' => 'regonNumber']) !!}
                    </div>

                    <fieldset id="fieldLegalRep">
                      <legend>{{ trans('client.legal_title') }}</legend>
                      <h3>{{ trans('client.person1_lbl') }}</h3>

                      {!! Form::input('hidden', 'legalIdP1', '', ['class'=> 'form-control legalIdP1', 'id' => 'legalIdP1']) !!}

                      <div class="form-group">
                          <label>{{ trans('client.name_lbl') }}</label>
                          {!! Form::input('text', 'legalNameP1', '', ['class'=> 'form-control legalNameP1', 'id' => 'legalNameP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.last_lbl') }}</label>
                          {!! Form::input('text', 'legalLastNameP1', '', ['class'=> 'form-control legalLastNameP1', 'id' => 'legalLastNameP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.function_lbl') }}</label>
                          {!! Form::input('text', 'legalFunctionP1', '', ['class'=> 'form-control legalFunctionP1', 'id' => 'legalFunctionP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.phone_lbl') }}</label>
                          {!! Form::input('text', 'legalPhoneP1', '', ['class'=> 'form-control legalPhoneP1', 'id' => 'legalPhoneP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.email_lbl') }}</label>
                          {!! Form::input('text', 'legalEmailP1', '', ['class'=> 'form-control legalEmailP1', 'id' => 'legalEmailP1']) !!}
                      </div>

                      <h3>{{ trans('client.person2_lbl') }}</h3>

                      {!! Form::input('hidden', 'legalIdP2', '', ['class'=> 'form-control legalIdP2', 'id' => 'legalIdP2']) !!}

                      <div class="form-group">
                          <label>{{ trans('client.name_lbl') }}</label>
                          {!! Form::input('text', 'legalNameP2', '', ['class'=> 'form-control legalNameP2', 'id' => 'legalNameP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.last_lbl') }}</label>
                          {!! Form::input('text', 'legalLastNameP2', '', ['class'=> 'form-control legalLastNameP2', 'id' => 'legalLastNameP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.function_lbl') }}</label>
                          {!! Form::input('text', 'legalFunctionP2', '', ['class'=> 'form-control legalFunctionP2', 'id' => 'legalFunctionP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.phone_lbl') }}</label>
                          {!! Form::input('text', 'legalPhoneP2', '', ['class'=> 'form-control legalPhoneP2', 'id' => 'legalPhoneP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.email_lbl') }}</label>
                          {!! Form::input('text', 'legalEmailP2', '', ['class'=> 'form-control legalEmailP2', 'id' => 'legalEmailP2']) !!}
                      </div>

                    </fieldset>

                    <div class="form-group" id="g_phone">
                        <label>{{ trans('client.phone_lbl') }} (1)</label>
                        {!! Form::input('text', 'phone', '', ['class'=> 'form-control phone', 'id' => 'phone']) !!}
                    </div>

                    <div class="form-group" id="g_phone">
                        <label>{{ trans('client.phone_lbl') }} (2)</label>
                        {!! Form::input('text', 'phone2', '', ['class'=> 'form-control phone2', 'id' => 'phone2']) !!}
                    </div>

                    <div class="form-group" id="g_email">
                        <label>{{ trans('client.email_lbl') }}</label>
                        {!! Form::input('email', 'email', '', ['class'=> 'form-control email', 'id' => 'emailClientCompany', 'autocomplete' => 'false']) !!}
                    </div>

                    <div class="form-group" id="g_email">
                        <label>{{ trans('client.email_lbl') }} (2)</label>
                        {!! Form::input('email', 'email2', '', ['class'=> 'form-control email2', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                    </div>

                    <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                        <label>
                           <input name="chkOportunity" class="chkOportunity" type="checkbox" value="1" checked>
                          <span>{{ trans('client.oportunity') }}</span>
                      </label>
                    </div>

                    <div class="form-group" id="g_dateReminder">
                      <label>{{ trans('client.oportunity_reminder') }}</label>
                      {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                    </div>

                    <div class="form-group" id="g_notes">
                        <label>{{ trans('client.notes') }}</label>
                        {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                    </div>
                    
                    
	                  <div class="form-group" id="g_notes">
	                      <label>{{ trans('client.accountant_number') }}</label>
	                      <input type="text" name="accountant_number" id="accountant_number" class="form-control" autocomplete="off" />
	                  </div>
	                  
	                  <div class="form-group" id="g_notes">
	                      <label>{{ trans('client.accountant_code') }}</label>
	                      <input type="text" name="accountant_code" id="accountant_code" class="form-control" autocomplete="off" />
	                </div>

                    <div class="form-footer">
                      <button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                      <button type="submit" class="btn btn-primary" name="button">{{ trans('client.send_lbl') }}</button>
                    </div>

                  </form>

          </div>



      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default closeFrmUpdateClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
        <a href="javascript:void(0);" class="btn btn-primary nextFrmUpdateClient">{{ trans('client.next_lbl') }}</a>
      </div>

    </div>
  </div>
</div>


<script>

        $('.datepicker').datetimepicker({
          format: 'Y-MM-DD',
          allowInputToggle: true,
          //minDate: moment(),
        });
</script>