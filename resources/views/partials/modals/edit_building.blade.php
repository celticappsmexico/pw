<div class="modal fade" id="modalWarehouseUpdate">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="formUpdateBuilding" id="formUpdateBuilding">
				<div class="modal-header">
					<h4 class="modal-title">{{ trans('warehouse.titleUpdate') }}</h4>
				</div>
				<div class="modal-body">

					<input type="hidden" name="id" id="id">

					<input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

					<div class="form-group" id="g_name">
	                  	<label>{{ trans('warehouse.name') }}</label>
	                	{!! Form::input('text', 'name', '', ['class'=> 'form-control', 'id' => 'name']) !!}
	              	</div>

	              	<div class="form-group" id="g_country">
	                    <label for="">{{ trans('warehouse.country') }}</label>
	                    <select name="country" id="country" class="form-control">
	                      	@foreach ($countries as $country)
	                      		<option value="{{$country->id}}" selected>{{$country->name}}</option>
	                    	@endforeach
	                	</select>
	                </div>

	              	<div class="form-group" id="g_address">
	                  	<label>{{ trans('warehouse.full_address') }}</label>
	                	{!! Form::input('text', 'address', '', ['class'=> 'form-control', 'id' => 'address']) !!}
	              	</div>


					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('warehouse.close') }}</button>
					<button type="submit" class="btn btn-primary">{{ trans('warehouse.save') }}</button>
				</div>
			</form>
		</div>
	</div>
</div>