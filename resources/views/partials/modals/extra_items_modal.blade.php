<div class="modal fade" id="extra_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Extra items</h4>
            </div>
            <div class="modal-body" id="modal_rent">
                <!-- Cabecera -->
                <div class="col-md-12" style="text-align:center; font-size:14px; font-weight:bold; border:1px solid black; background-color:rgb(78, 77, 76); color:white">
                    <div class="col-md-4" style="border-right:1px solid black">
                        <span>Item</span>
                    </div>
                    <div class="col-md-2" style="border-right:1px solid black">
                        <span>Price</span>
                    </div>
                    <div class="col-md-4" style="border-right:1px solid black">
                        <span>Quantity</span>
                    </div>
                    <div class="col-md-2">
                        <span>Total</span>
                    </div>
                </div>
                <!-- Fin cabecera -->
                <!-- Extra items -->
                <div class="col-md-12" style="border:1px solid black; border-top:0px">
                    @foreach ($extraItems as $key => $extraItem)
                        <div class="row">
                            <div class="col-md-4" style="border-right:1px solid black; width:34%; height:53.33px">
                                <img src="{{asset('assets/images/extra-items/'.$extraItem->img_path)}}" id="img_extra_item"/>
                                <span id="spans_nombre_objetos">{!! $extraItem->nombre !!}</span>
                            </div>
                            <div class="col-md-2" style="border-right:1px solid black; width:15.9%; height:53.33px; font-size:12px; text-align:center; padding-top:2%">
                                <span id="span_price_extra">${!! $extraItem->price !!}</span>
                            </div>
                            <div class="col-md-4" style="border-right:1px solid black; width:32% ; height:53.33px; padding-top:1%">
                                <a href="javascript:void(0);" id="rest" class="{!! $extraItem->id !!}">
                                    <img src="{{asset('assets/images/size-estimator/ico_min.png')}}" id="ico_min"/>
                                </a>
                                <input type="text" class="" id="cantidad" value="0" dir="{!! $extraItem->price !!}" disabled>
                                <a href="javascript:void(0);" id="plus" class="{!! $extraItem->id !!}">
                                    <img src="{{asset('assets/images/size-estimator/ico_plus.png')}}" id="ico_min"/>
                                </a>
                            </div>
                            <div class="col-md-1" style="width:102px; height:53.3px; font-size:12px; text-align:center; padding-top:2%; font-weight:bold">
                                $<span id="total_item_{!! $extraItem->id !!}">0.0</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Fin extra items -->
                <!-- Totales -->
                <div class="col-md-12" style="border:1px solid black; margin-top:2%; font-size:12px">
                    <div class="col-md-10" style="border-right:1px solid black">
                        <span>Subtotal:</span>
                    </div>
                    <div class="col-md-2" style="text-align:center; padding-left:23px">
                        $<span style="font-weight:bold" id="subtotal_extra_items">0.0</span>
                    </div>
                </div>
                <div class="col-md-12" style="border:1px solid black; border-top:0px; font-size:12px">
                    <div class="col-md-10" style="border-right:1px solid black">
                        <span>VAT:</span>
                    </div>
                    <div class="col-md-2" style="text-align:center; padding-left:27.4px">
                        <span style="font-weight:bold; text-align:center">23%</span>
                    </div>
                </div>
                <div class="col-md-12" style="border:1px solid black; border-top:0px; font-size:12px">
                    <div class="col-md-10" style="border-right:1px solid black">
                        <span>Total:</span>
                    </div>
                    <div class="col-md-2" style="text-align:center; padding-left:23px">
                        $<span style="font-weight:bold; text-align:center" id="total_extra_items">0.0</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-top:5%">
                        <a data-toggle="modal" href="#" id="btn_proceed_pay" class="btn btn-default btn-lg btn-block">Proceed to pay</a>
                    </div>
                </div>
                <!-- Fin totales -->
            </div>
        </div>
    </div>
</div>

<!-- CSS -->
<style media="screen">
    #img_extra_item {
        width: 30%;
    }
    #btn_proceed_pay:hover {
        background-color: rgb(203, 16, 16);
        color: white;
    }
    #btn_proceed_pay {
        background-color: rgb(78, 77, 76);
        color: white;
    }
    #rest, #plus {
        background: #777474;
        width: 23px;
        height: 23px;
        display: inline-block;
    }
    #rest {
        margin-left: 40px;
    }
    #ico_min {
        margin-top: 3px;
        margin-left: 6px;
    }
    #rest:hover, #plus:hover {
        background-color: rgb(203, 16, 16);
    }
    #cantidad {
        border: 1px solid #FFF;
        outline: 0;
        width: 22px;
        text-align: center;
        margin-top: 3px;
        color: #777474;
        font-weight: 700;
        padding: 3px 2px;
        vertical-align: middle;
        background-color: transparent;
    }
</style>
<!-- Fin CSS -->
<!-- JS -->
<script type="text/javascript">
    var cant = 0;
    var totalItem = 0;
    var subtotalExtraItems = 0;
    var totalExtraItems = 0;
    //****** Condiciones boton mas ******//
    $(document).on('click', '#plus', function(){
        item = $(this).attr('class');
        cant = $(this).parent('div').find('#cantidad').val();
        subtotalExtraItems = parseFloat($('#subtotal_extra_items').text());
        totalExtraItems = parseFloat($('#total_extra_items').text());
        if (cant < 100) {
            cant++;
            $(this).parent('div').find('#cantidad').val(cant);
            itemPrice = $(this).parent('div').find('#cantidad').attr('dir');
            totalItem = parseFloat(itemPrice*cant);
            subtotalExtraItems += parseFloat(itemPrice);
            totalExtraItems = subtotalExtraItems+((subtotalExtraItems*23)/100);
            $(this).parents('div').find('#total_item_'+item).text(totalItem.toFixed(2));
            $('#subtotal_extra_items').text(subtotalExtraItems.toFixed(2));
            $('#total_extra_items').text(totalExtraItems.toFixed(2));
        }
    });
    //****** Condiciones boton menos ******//
    $(document).on('click', '#rest', function(){
        item = $(this).attr('class');
        cant = $(this).parent('div').find('#cantidad').val();
        subtotalExtraItems = parseFloat($('#subtotal_extra_items').text());
        totalExtraItems = parseFloat($('#total_extra_items').text());
        if (cant > 0) {
            cant--;
            $(this).parent('div').find('#cantidad').val(cant);
            itemPrice = $(this).parent('div').find('#cantidad').attr('dir');
            totalItem = parseFloat(itemPrice*cant);
            subtotalExtraItems -= parseFloat(itemPrice);
            totalExtraItems = subtotalExtraItems+((subtotalExtraItems*23)/100);
            $(this).parents('div').find('#total_item_'+item).text(totalItem.toFixed(2));
            $('#subtotal_extra_items').text(subtotalExtraItems.toFixed(2));
            $('#total_extra_items').text(totalExtraItems.toFixed(2));
        }
    });
</script>
<!-- Fin JS -->
