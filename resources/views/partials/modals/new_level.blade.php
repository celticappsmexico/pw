
<div class="modal fade" id="modalNewLevel">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="formNewLevel" id="formNewLevel">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{ trans('levels.new_levels_title') }}</h4>
				</div>
				<div class="modal-body">

					<input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

					<input type="hidden" name="idBuilding" id="idBuilding">
					
					<div class="form-group" id="g_name">
	                  	<label>{{ trans('levels.level_name') }}</label>
	                	{!! Form::input('text', 'name', '', ['class'=> 'form-control', 'id' => 'name']) !!}
	              	</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('levels.close') }}</button>
					<button type="submit" class="btn btn-primary">{{ trans('levels.send') }}</button>
				</div>
			</form>
		</div>
	</div>
</div>