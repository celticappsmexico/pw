<div class="modal fade" id="modalNewStorage">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="formNewStorage" id="formNewStorage">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{ trans('storages.new_storage') }}</h4>
				</div>
				<div class="modal-body">

					<input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

					<input type="hidden" name="idLevel" id="idLevel">
					
					<div class="form-group" id="g_alias">
	                  	<label>{{ trans('storages.alias') }}</label>
	                	{!! Form::input('text', 'alias', '', ['class'=> 'form-control', 'id' => 'alias']) !!}
	              	</div>

	              	<div class="form-group" id="g_sqm">
	                  	<label>{{ trans('storages.sqm') }}</label>
	                	{!! Form::input('text', 'sqm', '', ['class'=> 'form-control', 'id' => 'sqm']) !!}
	              	</div>

	              	<div class="form-group" id="g_price">
	                  	<label>{{ trans('storages.price') }}</label>
	                	{!! Form::input('text', 'price', '', ['class'=> 'form-control', 'id' => 'price']) !!}
	              	</div>

	           		<div class="form-group" id="g_comments">
	                  	<label>{{ trans('storages.comments') }}</label>
	                	{!! Form::input('textarea', 'comments', '', ['class'=> 'form-control', 'id' => 'comments']) !!}
	              	</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('storages.close') }}</button>
					<button type="submit" class="btn btn-primary">{{ trans('storages.send') }}</button>
				</div>
			</form>
		</div>
	</div>
</div>