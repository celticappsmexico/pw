<div class="modal fade" id="modalBraintree2" style="z-index: 9999!important;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('calculator.rent_box') }}</h4>
			</div>
			<form id="payment-form" method="post" action="{{ route('sendPay') }}">

				<div class="modal-body" style="padding:20px 20px;">

						<div class="alert alert-success alertClientPayment" style="margin: 0px;">
							<!-- <a href="#" class="close alertClientPaymentClose" aria-label="close">&times;</a> -->
							<span id="clientName"></span>
						</div>

						<div style="visibility: hidden;">
							<input type="text" name="id_client" id="id_client" value="">
							<input type="text" name="id_order" id="id_order" value="">
							<input type="text" name="_token" id="_token">
						</div>



            <div class="row">
           		<div id="bt-dropin2"></div>
            </div>

            <div class="row">
							<div class="form-group" id="">
                <label for="amount">{{ trans('calculator.amount') }}</label>
                <input id="price" name="amount" type="text" min="1" placeholder="Amount" value="" class="form-control" readonly />
            	</div>
						</div>



            <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
            <script>
                // We generated a client token for you so you can test out this code
                // immediately. In a production-ready integration, you will need to
                // generate a client token on your server (see section below).
								<?php

									$token = $clientToken = \Braintree_ClientToken::generate();

								?>
                clientToken = "<?php echo $token; ?>";

                braintree.setup(clientToken, "dropin", {
                  container: "bt-dropin2"
                });
            </script>


          </div>

					<div class="modal-footer">
							<button type="button" class="btn btn-info" id="sendPaymentCash"><i class="fa fa-money" aria-hidden="true"></i> Cash Payment</button>
							<button type="submit" class="btn btn-info" id="sendPay"><i class="fa fa-paper-plane-o"></i> Pay</button>
					</div>
      </form>
		</div>
	</div>
</div>
