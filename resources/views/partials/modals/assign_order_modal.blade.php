<div class="modal fade" id="assingOrder" data-backdrop="static" style="z-index: 9999!important;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('calculator.assign_order') }}</h4>
			</div>
				<div class="modal-body" style="padding:40px 40px;">

							<div class="col-md-10">
								<div class="form-group">
									<label for="">{{ trans('calculator.find_client') }}</label>
									<div class="alert alert-info assignName">

									</div>
									<input type="text" name="find_client_assign" id="find_client_assign" list="find_client_list_assign" class="find_client_assign form-control" value="">
									<datalist id="find_client_list_assign"></datalist>
								</div>
							</div>
							<div class="col-md-2">
								<a href="javascript:void(0);" class="btn btn-primary btnNewClient"><i class="fa fa-user-plus"></i></a>
							</div>

						<div style="visibility: hidden;">
							<input type="text" name="id_client" id="id_client" value="">
							<input type="text" name="_token" id="_token">
						</div>
					</div>

					<div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnAssignClose"><i class="fa fa-paper-plane-o"></i> {{ trans('calculator.close_current_order') }}</button>
						      <button type="button" class="btn btn-info" id="btnAssignPay"><i class="fa fa-paper-plane-o"></i> {{ trans('calculator.close_current_order_and_pay') }}</button>
					</div>
        </div>

		</div>
	</div>
</div>
