<link rel="stylesheet" href="{{asset('assets/css/rent-storage.css')}}" media="screen" title="no title">

<div class="modal fade" id="rent_storage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:-1.7%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ trans('storages.rent_storage') }}</h4>
            </div>
            <div class="modal-body" id="modal_rent">

            </div>
        </div>
    </div>
</div>
