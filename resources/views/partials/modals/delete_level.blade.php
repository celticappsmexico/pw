<div class="modal fade" id="modalDeleteLevel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id=""> {{ trans('levels.deleteTitle') }} </h4>
      </div>
      <div class="modal-body">
        <h3>{{ trans('levels.delete_q') }}</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('levels.close') }}</button>
        <button type="button" class="btn btn-primary btnConfirmDelLevel">{{ trans('levels.delete') }}</button>
      </div>
    </div>
  </div>
</div>
