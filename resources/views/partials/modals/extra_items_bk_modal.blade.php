<div class="modal fade"  id="extra_items2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px!important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ trans('extraitems.title') }}</h4>
            </div>
            <div class="modal-body" id="modal_rent" >

              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>{{ trans('extraitems.item') }}</th>
                    <th>{{ trans('extraitems.price') }}</th>
                    <th>{{ trans('extraitems.quantity') }}</th>
                    <th>{{ trans('extraitems.subtotal') }}</th>
                    <th>{{ trans('extraitems.vat') }}</th>
                    <th>{{ trans('extraitems.total') }}</th>
                    <th>{{ trans('extraitems.actions') }}</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($extraItems as $key => $extraItem)
                      <tr>
                        <td style="width:250px">
                          <img src="{{asset('assets/images/extra-items/'.$extraItem->img_path)}}" id="img_extra_item"/>
                          <span id="spans_nombre_objetos">{!! $extraItem->nombre !!}</span>
                        </td>
                        <td>
                          <span id="span_price_extra">${!! $extraItem->price !!}</span>
                        </td>
                        <td>
                          <a href="javascript:void(0);" id="rest" class="{!! $extraItem->id !!}">
                              <img src="{{asset('assets/images/size-estimator/ico_min.png')}}" id="ico_min"/>
                          </a>
                          <input type="text" class="" id="cantidad" value="0" dir="{!! $extraItem->price !!}" disabled>
                          <a href="javascript:void(0);" id="plus" class="{!! $extraItem->id !!}">
                              <img src="{{asset('assets/images/size-estimator/ico_plus.png')}}" id="ico_min"/>
                          </a>
                        </td>
                        <td>
                          $<span id="subtotal_item_{!! $extraItem->id !!}">0.0</span>
                        </td>
                        <td>
                          $<span id="vat_item_{!! $extraItem->id !!}">0.0</span>
                        </td>
                        <td>
                          $<span class="totalExtraItemsTd" id="total_item_{!! $extraItem->id !!}">0.0</span>
                        </td>
                        <td>
                          <button type="button" dir="{!! $extraItem->id !!}" class="btn btn-xs btn-info addExtraItem" name="button">{{ trans('extraitems.add_item') }}</button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th><b>{{ trans('extraitems.total') }}</b></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                          $<span id="total_extraitems">0.0</span>
                        </th>
                        <th></th>
                      </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<style media="screen">
    #img_extra_item {
        width: 20%;
    }
    #btn_proceed_pay:hover {
        background-color: rgb(203, 16, 16);
        color: white;
    }
    #btn_proceed_pay {
        background-color: rgb(78, 77, 76);
        color: white;
    }
    #rest, #plus {
        background: #777474;
        width: 23px;
        height: 23px;
        display: inline-block;
    }
    #rest {
        margin-left: 40px;
    }
    #ico_min {
        margin-top: 3px;
        margin-left: 6px;
    }
    #rest:hover, #plus:hover {
        background-color: rgb(203, 16, 16);
    }
    #cantidad {
        border: 1px solid #FFF;
        outline: 0;
        width: 22px;
        text-align: center;
        margin-top: 3px;
        color: #777474;
        font-weight: 700;
        padding: 3px 2px;
        vertical-align: middle;
        background-color: transparent;
    }
</style>

<script type="text/javascript">
    var cant = 0;
    var totalItem = 0;
    var subtotalExtraItems = 0;
    var totalExtraItems = 0;

    $(document).on('click', '#plus', function(){
        item = $(this).attr('class');
        cant = $(this).parent('td').find('#cantidad').val();
        subtotalExtraItems = parseFloat($('#subtotal_extra_items').text());
        totalExtraItems = parseFloat($('#total_extra_items').text());
        if (cant < 100) {
            cant++;
            $(this).parent('td').find('#cantidad').val(cant);
            itemPrice = $(this).parent('td').find('#cantidad').attr('dir');
            totalItem = parseFloat(itemPrice*cant);
            subtotalExtraItems += parseFloat(itemPrice);
            vat = (totalItem * 1.23) - totalItem;
            grandtotal =  totalItem * 1.23;
            totalExtraItems = subtotalExtraItems+((subtotalExtraItems*23)/100);
            $(this).parents('tr').find('#subtotal_item_'+item).text(totalItem.toFixed(2));
            $(this).parents('tr').find('#vat_item_'+item).text(vat.toFixed(2));
            $(this).parents('tr').find('#total_item_'+item).text(grandtotal.toFixed(2));
            //$('#subtotal_extra_items').text(subtotalExtraItems.toFixed(2));
            //$('#total_extra_items').text(totalExtraItems.toFixed(2));
            sumExtraItems();
        }
    });

    $(document).on('click', '#rest', function(){
        item = $(this).attr('class');
        cant = $(this).parent('td').find('#cantidad').val();
        subtotalExtraItems = parseFloat($('#subtotal_extra_items').text());
        totalExtraItems = parseFloat($('#total_extra_items').text());
        if (cant > 0) {
            cant--;
            $(this).parent('td').find('#cantidad').val(cant);
            itemPrice = $(this).parent('td').find('#cantidad').attr('dir');
            totalItem = parseFloat(itemPrice*cant);
            subtotalExtraItems -= parseFloat(itemPrice);
            vat = (totalItem * 1.23) - totalItem;
            totalExtraItems = subtotalExtraItems+((subtotalExtraItems*23)/100);
            $(this).parents('tr').find('#total_item_'+item).text(totalItem.toFixed(2));
            //$('#subtotal_extra_items').text(subtotalExtraItems.toFixed(2));
            //$('#total_extra_items').text(totalExtraItems.toFixed(2));
            sumExtraItems();
        }
    });

    function sumExtraItems(){
      val = 0;
      $('.totalExtraItemsTd').each(function(index) {
        val = val + parseFloat($(this).text());
        //val = parseFloat($(this).text());
        //console.log(val);
      });
      $('#total_extraitems').text(val);
    }

</script>
