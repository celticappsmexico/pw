<div class="modal fade" id="modalOrderDetail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">{{ trans('cart.order_modal_title') }} [<span class="orderNo"></span>]</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped" id="tblOrderDetail">
          <thead>
            <tr style="font-size:11.5px">
              <th>{{ trans('cart.item') }}</th>
              <th>{{ trans('cart.price') }}</th>
              <th>{{ trans('cart.quantity') }}</th>
              <th>{{ trans('cart.total') }}</th>
            </tr>
          </thead>
          <tbody id="orderDetailBody">

          </tbody>
          <!-- 
          <tfoot>
            <tr>
              <th></th>
              <th></th>
              <th>{{ trans('cart.subtotal') }}</th>
              <th class="subtotal"></th>
            </tr>
            <tr>
              <th></th>
              <th></th>
              <th>{{ trans('cart.vat') }}</th>
              <th class="vat"></th>
            </tr>
            <tr>
              <th></th>
              <th></th>
              <th>{{ trans('cart.total') }}</th>
              <th class="total"></th>
            </tr>
          </tfoot>
           -->
        </table>
        
        <input type="hidden" id="subtotal_estimator" />
        <input type="hidden" id="vat_estimator" />
        <input type="hidden" id="total_estimator" />
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('cart.close') }}</button>
        <button type="button" class="btn btn-success btnPayOrderModal" data-paid="0">
        	{{ trans('cart.go_to_save') }}
        </button>
        <button type="button" class="btn btn-primary btnPayOrderModal" data-paid="1">
        	{{ trans('cart.go_to_pay') }}
        </button>
        <button type="button" class="btn btn-info btnPayDepositModal">
        	{{ trans('cart.pay_deposit') }}
        </button>
      </div>
    </div>
  </div>
</div>


<script>

</script>