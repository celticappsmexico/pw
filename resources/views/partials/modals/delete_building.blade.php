<div class="modal fade" id="modalDeleteBuilding" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id=""> {{ trans('warehouse.delete') }} </h4>
      </div>
      <div class="modal-body">
        <h3>{{ trans('warehouse.delete_q') }}</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('warehouse.close') }}</button>
        <button type="button" class="btn btn-primary btnConfirmDelBuilding">{{ trans('warehouse.delete') }}</button>
      </div>
    </div>
  </div>
</div>