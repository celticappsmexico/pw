<div class="modal fade" id="modalNewClient" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title" id="">{{ trans('client.newClient_title') }} <span class="lblClientType"></span></h4>
      </div>
      <div class="modal-body">
          <div class="frmNewClientStep1">

            <div class="form-group">
              <label for="">{{ trans('client.accessLevel_lbl') }}</label>
              <select id="cboUserLevel" name="cboUserLevel" class="form-control">
                @foreach ($roles as $rol)
                <option value="{{$rol->id}}" selected>{{$rol->name}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="">{{ trans('client.clientType_lbl') }}</label>
              <select id="cboClientType" name="cboClientType" class="form-control">
                @foreach ($userTypes as $ut)
                <option value="{{$ut->id}}" selected>{{$ut->name}}</option>
                @endforeach
              </select>
            </div>

          </div>
          <div class="frmNewClientStep2">

            <!-- form new client person -->



            <form action="{{ route('newClient') }}" name="frmNewClientPerson" id="frmNewClientPerson" class="frmNewClient" autocomplete=”off”>

              <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

              <!-- <div class="form-group" id="g_username">
                  <label>{{ trans('client.username_lbl') }}</label>
                  {!! Form::input('text', 'username', '', ['class'=> 'form-control', 'id' => 'usernameClientPerson']) !!}
              </div> -->

                <div class="form-group" id="g_name">
                    <label>{{ trans('form.label.name') }}</label>
                    {!! Form::input('text', 'name', '', ['class'=> 'form-control', 'id' => 'name']) !!}
                </div>

                <div class="form-group" id="g_lastName">
                    <label>{{ trans('client.last_lbl') }}</label>
                    {!! Form::input('text', 'lastName', '', ['class'=> 'form-control', 'id' => 'lastName']) !!}
                </div>

                <div class="form-group" id="g_birthday">
                    <label>{{ trans('client.birthday_lbl') }}</label>
                    {!! Form::input('text', 'birthday', '', ['class'=> 'form-control datepicker', 'id' => 'birthday']) !!}
                </div>

                <fieldset class="fieldset-address">
                  <legend>{{ trans('client.address_lbl') }}</legend>

                  <div class="form-group" id="g_street">
                      <label>{{ trans('client.street_lbl') }}</label>
                      {!! Form::input('text', 'street', '', ['class'=> 'form-control', 'id' => 'street']) !!}
                  </div>

                  <div class="form-group" id="g_number">
                      <label>{{ trans('client.number_lbl') }}</label>
                      {!! Form::input('text', 'number', '', ['class'=> 'form-control', 'id' => 'number']) !!}
                  </div>

                  <div class="form-group" id="g_apartmentNumber">
                      <label>{{ trans('client.apartment_lbl') }}</label>
                      {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control', 'id' => 'apartmentNumber']) !!}
                  </div>

                  <div class="form-group" id="g_postCode">
                      <label>{{ trans('client.postCode_lbl') }}</label>
                      {!! Form::input('text', 'postCode', '', ['class'=> 'form-control', 'id' => 'postCode']) !!}
                  </div>

                  <div class="form-group" id="g_city">
                      <label>{{ trans('client.city_lbl') }}</label>
                      {!! Form::input('text', 'city', '', ['class'=> 'form-control', 'id' => 'city']) !!}
                  </div>

                  <div class="form-group" id="g_country">
                    <label for="">{{ trans('client.country_lbl') }}</label>
                    <select name="country" id="country" class="form-control">
                      @foreach ($countries as $country)
                        @if ($country->name == "Polska")
                          <option value="{{$country->id}}" selected>{{$country->name}}</option>
                        @else
                          <option value="{{$country->id}}">{{$country->name}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>

                </fieldset>

                <div class="form-group" id="g_peselNumber">
                    <label>{{ trans('client.pesel_lbl') }}</label>
                    {!! Form::input('text', 'peselNumber', '', ['class'=> 'form-control', 'id' => 'peselNumber']) !!}
                </div>

                <div class="form-group" id="g_idNumber">
                    <label>{{ trans('client.id_lbl') }}</label>
                    {!! Form::input('text', 'idNumber', '', ['class'=> 'form-control', 'id' => 'idNumber']) !!}
                </div>

                <div class="form-group" id="g_phone">
                    <label>{{ trans('client.phone_lbl') }} (1)</label>
                    {!! Form::input('text', 'phone', '', ['class'=> 'form-control', 'id' => 'phone']) !!}
                </div>

                <div class="form-group" id="g_phone">
                    <label>{{ trans('client.phone_lbl') }} (2)</label>
                    {!! Form::input('text', 'phone2', '', ['class'=> 'form-control', 'id' => 'phone2']) !!}
                </div>

                {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}

                <div class="form-group" id="g_email">
                    <label>{{ trans('client.email_lbl') }} (1)</label>
                    {!! Form::input('text', 'email', '', ['class'=> 'form-control', 'id' => 'emailClientPerson', 'autocomplete' => 'false']) !!}
                </div>

                <div class="form-group" id="g_email">
                    <label>{{ trans('client.email_lbl') }} (2)</label>
                    {!! Form::input('email', 'email2', '', ['class'=> 'form-control', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                </div>

                <div class="form-group g_password" id="g_password">
                    <label>{{ trans('client.password_lbl') }}</label>
                    {!! Form::input('password','password', '', ['class'=> 'form-control txtPwd', 'id' => 'password','autocomplete' => 'false']) !!}
                </div>

                <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                    <label>
                       <input name="chkOportunity" class="chkOportunity" type="checkbox" value="1" checked>
                      <span>{{ trans('client.oportunity') }}</span>
                  </label>
                </div>

                <div class="form-group" id="g_dateReminder">
                  <label>{{ trans('client.oportunity_reminder') }}</label>
                  {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                </div>

                <div class="form-group" id="g_notes">
                    <label>{{ trans('client.notes') }}</label>
                    {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                </div>

                <div class="form-footer">
                  <button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                  <button type="submit" class="btn btn-primary sendNewClientBtn" name="button">{{ trans('client.send_lbl') }}</button>
                </div>

              </form>

              <!-- form new client person -->

              <form action="{{ route('newClient') }}" name="frmNewClientOnePerson" id="frmNewClientOnePerson" class="frmNewClient" autocomplete=”off”>

                {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}
                <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

                <!-- <div class="form-group" id="g_username">
                    <label>{{ trans('client.username_lbl') }}</label>
                    {!! Form::input('text', 'username', '', ['class'=> 'form-control', 'id' => 'usernameClientOnePerson']) !!}
                </div> -->
 
                  <div class="form-group" id="g_name">
                      <label>{{ trans('form.label.name') }}</label>
                      {!! Form::input('text', 'name', '', ['class'=> 'form-control', 'id' => 'name']) !!}
                  </div>

                  <div class="form-group" id="g_lastName">
                      <label>{{ trans('client.last_lbl') }}</label>
                      {!! Form::input('text', 'lastName', '', ['class'=> 'form-control', 'id' => 'lastName']) !!}
                  </div>

                  <div class="form-group" id="g_companyName">
                      <label>{{ trans('client.company_lbl') }}</label>
                      {!! Form::input('text', 'companyName', '', ['class'=> 'form-control', 'id' => 'companyName']) !!}
                  </div>

                  <div class="form-group" id="g_birthday">
                      <label>{{ trans('client.birthday_lbl') }}</label>
                      {!! Form::input('text', 'birthday', '', ['class'=> 'form-control datepicker', 'id' => 'birthday']) !!}
                  </div>

                  <fieldset class="fieldset-address">
                    <legend>{{ trans('client.address_lbl') }}</legend>

                    <div class="form-group" id="g_street">
                        <label>{{ trans('client.street_lbl') }}</label>
                        {!! Form::input('text', 'street', '', ['class'=> 'form-control', 'id' => 'street']) !!}
                    </div>

                    <div class="form-group" id="g_number">
                        <label>{{ trans('client.number_lbl') }}</label>
                        {!! Form::input('text', 'number', '', ['class'=> 'form-control', 'id' => 'number']) !!}
                    </div>

                    <div class="form-group" id="g_apartmentNumber">
                        <label>{{ trans('client.apartment_lbl') }}</label>
                        {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control', 'id' => 'apartmentNumber']) !!}
                    </div>

                    <div class="form-group" id="g_postCode">
                        <label>{{ trans('client.postCode_lbl') }}</label>
                        {!! Form::input('text', 'postCode', '', ['class'=> 'form-control', 'id' => 'postCode']) !!}
                    </div>

                    <div class="form-group" id="g_city">
                        <label>{{ trans('client.city_lbl') }}</label>
                        {!! Form::input('text', 'city', '', ['class'=> 'form-control', 'id' => 'city']) !!}
                    </div>

                    <div class="form-group" id="g_country">
                      <label for="">{{ trans('client.country_lbl') }}</label>
                      <select id="cboClientType" name="country" id="country" class="form-control">
                        @foreach ($countries as $country)
                          @if ($country->name == "Polska")
                          <option value="{{$country->id}}" selected>{{$country->name}}</option>
                        @else
                          <option value="{{$country->id}}">{{$country->name}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>

                  </fieldset>


                  <div class="form-group" id="g_nipNumber">
                      <label>{{ trans('client.nip_lbl') }}</label>
                      {!! Form::input('text', 'nipNumber', '', ['class'=> 'form-control', 'id' => 'nipNumber']) !!}
                  </div>

                  <div class="form-group" id="g_regonNumber">
                      <label>{{ trans('client.regon_lbl') }}</label>
                      {!! Form::input('text', 'regonNumber', '', ['class'=> 'form-control', 'id' => 'regonNumber']) !!}
                  </div>

                  <div class="form-group" id="g_phone">
                      <label>{{ trans('client.phone_lbl') }} (1)</label>
                      {!! Form::input('text', 'phone', '', ['class'=> 'form-control', 'id' => 'phone']) !!}
                  </div>

                  <div class="form-group" id="g_phone">
                      <label>{{ trans('client.phone_lbl') }} (2)</label>
                      {!! Form::input('text', 'phone2', '', ['class'=> 'form-control', 'id' => 'phone2']) !!}
                  </div>

                  <div class="form-group" id="g_email">
                      <label>{{ trans('client.email_lbl') }} (1)</label>
                      {!! Form::input('email', 'email', '', ['class'=> 'form-control', 'id' => 'emailClientOnePerson', 'autocomplete' => 'false']) !!}
                  </div>

                  <div class="form-group" id="g_email">
                      <label>{{ trans('client.email_lbl') }} (2)</label>
                      {!! Form::input('email', 'email2', '', ['class'=> 'form-control', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                  </div>


                  <div class="form-group g_password" id="g_password">
                      <label>{{ trans('client.password_lbl') }}</label>
                      {!! Form::input('password','password', '', ['class'=> 'form-control txtPwd', 'id' => 'password','autocomplete' => 'false']) !!}
                  </div>

                  <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                      <label>
                         <input name="chkOportunity" class="chkOportunity" type="checkbox" value="1" checked>
                        <span>{{ trans('client.oportunity') }}</span>
                    </label>
                  </div>

                  <div class="form-group" id="g_dateReminder">
                    <label>{{ trans('client.oportunity_reminder') }}</label>
                    {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                  </div>

                  <div class="form-group" id="g_notes">
                      <label>{{ trans('client.notes') }}</label>
                      {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                  </div>

                  <div class="form-footer">
                    <button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                    <button type="submit" class="btn btn-primary sendNewClientBtn" name="button">{{ trans('client.send_lbl') }}</button>
                  </div>

                </form>

                <!-- form new client person -->

                <form action="{{ route('newClient') }}" name="frmNewClientCompany" id="frmNewClientCompany" class="frmNewClient" >

                  {!! Form::input('hidden', 'hdfr', '', ['class'=> 'form-control hdfr', 'id' => 'hdfr']) !!}
                  {!! Form::input('hidden', 'hdfut', '', ['class'=> 'form-control hdfut', 'id' => 'hdfut']) !!}
                  <input type="hidden" name="_token" id="tokenClientPerson" value="{{ csrf_token() }}">

                  <!-- <div class="form-group" id="g_username">
                      <label>{{ trans('client.username_lbl') }}</label>
                      {!! Form::input('text', 'username', '', ['class'=> 'form-control', 'id' => 'usernameClientCompany']) !!}
                  </div> -->

                    <div class="form-group" id="g_companyName">
                        <label>{{ trans('client.company_lbl') }}</label>
                        {!! Form::input('text', 'companyName', '', ['class'=> 'form-control', 'id' => 'companyName']) !!}
                    </div>

                    <fieldset class="fieldset-address">
                      <legend>{{ trans('client.address_lbl') }}</legend>

                      <div class="form-group" id="g_street">
                          <label>{{ trans('client.street_lbl') }}</label>
                          {!! Form::input('text', 'street', '', ['class'=> 'form-control', 'id' => 'street']) !!}
                      </div>

                      <div class="form-group" id="g_number">
                          <label>{{ trans('client.number_lbl') }}</label>
                          {!! Form::input('text', 'number', '', ['class'=> 'form-control', 'id' => 'number']) !!}
                      </div>

                      <div class="form-group" id="g_apartmentNumber">
                          <label>{{ trans('client.apartment_lbl') }}</label>
                          {!! Form::input('text', 'apartmentNumber', '', ['class'=> 'form-control', 'id' => 'apartmentNumber']) !!}
                      </div>

                      <div class="form-group" id="g_postCode">
                          <label>{{ trans('client.postCode_lbl') }}</label>
                          {!! Form::input('text', 'postCode', '', ['class'=> 'form-control', 'id' => 'postCode']) !!}
                      </div>

                      <div class="form-group" id="g_city">
                          <label>{{ trans('client.city_lbl') }}</label>
                          {!! Form::input('text', 'city', '', ['class'=> 'form-control', 'id' => 'city']) !!}
                      </div>

                      <div class="form-group" id="g_country">
                        <label for="">{{ trans('client.country_lbl') }}</label>
                        <select id="cboClientType" name="country" id="country" class="form-control">
                          @foreach ($countries as $country)
                            @if ($country->name == "Polska")
                              <option value="{{$country->id}}" selected>{{$country->name}}</option>
                            @else
                              <option value="{{$country->id}}">{{$country->name}}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>

                    </fieldset>

                    <div class="form-group" id="g_courtNumber">
                        <label>{{ trans('client.courtNumber_lbl') }}</label>
                        {!! Form::input('text', 'courtNumber', '', ['class'=> 'form-control', 'id' => 'courtNumber']) !!}
                    </div>

                    <div class="form-group" id="g_courtPlace">
                        <label>{{ trans('client.courtPlace_lbl') }}</label>
                        {!! Form::input('text', 'courtPlace', '', ['class'=> 'form-control', 'id' => 'courtPlace']) !!}
                    </div>

                    <div class="form-group" id="g_krsNumber">
                        <label>{{ trans('client.krs_lbl') }}</label>
                        {!! Form::input('text', 'krsNumber', '', ['class'=> 'form-control', 'id' => 'krsNumber']) !!}
                    </div>

                    <div class="form-group" id="g_nipNumber">
                        <label>{{ trans('client.nip_lbl') }}</label>
                        {!! Form::input('text', 'nipNumber', '', ['class'=> 'form-control', 'id' => 'nipNumber']) !!}
                    </div>

                    <div class="form-group" id="g_regonNumber">
                        <label>{{ trans('client.regon_lbl') }}</label>
                        {!! Form::input('text', 'regonNumber', '', ['class'=> 'form-control', 'id' => 'regonNumber']) !!}
                    </div>

                    <fieldset id="fieldLegalRep">
                      <legend>{{ trans('client.legal_title') }}</legend>
                      <h3>{{ trans('client.person1_lbl') }}</h3>

                      <div class="form-group">
                          <label>{{ trans('client.name_lbl') }}</label>
                          {!! Form::input('text', 'legalNameP1', '', ['class'=> 'form-control', 'id' => 'legalNameP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.last_lbl') }}</label>
                          {!! Form::input('text', 'legalLastNameP1', '', ['class'=> 'form-control', 'id' => 'legalLastNameP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.function_lbl') }}</label>
                          {!! Form::input('text', 'legalFunctionP1', '', ['class'=> 'form-control', 'id' => 'legalFunctionP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.phone_lbl') }}</label>
                          {!! Form::input('text', 'legalPhoneP1', '', ['class'=> 'form-control', 'id' => 'legalPhoneP1']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.email_lbl') }}</label>
                          {!! Form::input('text', 'legalEmailP1', '', ['class'=> 'form-control', 'id' => 'legalEmailP1']) !!}
                      </div>

                      <h3>{{ trans('client.person2_lbl') }}</h3>

                      <div class="form-group">
                          <label>{{ trans('client.name_lbl') }}</label>
                          {!! Form::input('text', 'legalNameP2', '', ['class'=> 'form-control', 'id' => 'legalNameP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.last_lbl') }}</label>
                          {!! Form::input('text', 'legalLastNameP2', '', ['class'=> 'form-control', 'id' => 'legalLastNameP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.function_lbl') }}</label>
                          {!! Form::input('text', 'legalFunctionP2', '', ['class'=> 'form-control', 'id' => 'legalFunctionP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.phone_lbl') }}</label>
                          {!! Form::input('text', 'legalPhoneP2', '', ['class'=> 'form-control', 'id' => 'legalPhoneP2']) !!}
                      </div>

                      <div class="form-group">
                          <label>{{ trans('client.email_lbl') }}</label>
                          {!! Form::input('text', 'legalEmailP2', '', ['class'=> 'form-control', 'id' => 'legalEmailP2']) !!}
                      </div>

                    </fieldset>

                    <div class="form-group" id="g_phone">
                        <label>{{ trans('client.phone_lbl') }} (1)</label>
                        {!! Form::input('text', 'phone', '', ['class'=> 'form-control', 'id' => 'phone']) !!}
                    </div>

                    <div class="form-group" id="g_phone">
                        <label>{{ trans('client.phone_lbl') }} (2)</label>
                        {!! Form::input('text', 'phone2', '', ['class'=> 'form-control', 'id' => 'phone2']) !!}
                    </div>

                    <div class="form-group" id="g_email">
                        <label>{{ trans('client.email_lbl') }} (1)</label>
                        {!! Form::input('email', 'email', '', ['class'=> 'form-control', 'id' => 'emailClientCompany', 'autocomplete' => 'false']) !!}
                    </div>
                    
                    <div class="form-group" id="g_email">
                        <label>{{ trans('client.email_lbl') }} (2)</label>
                        {!! Form::input('email', 'email2', '', ['class'=> 'form-control', 'id' => 'emailClientCompany2', 'autocomplete' => 'false']) !!}
                    </div>

                    <div class="form-group g_password" id="g_password">
                        <label>{{ trans('client.password_lbl') }}</label>
                        {!! Form::input('password','password', '', ['class'=> 'form-control txtPwd', 'id' => 'password','autocomplete' => 'false']) !!}
                    </div>

                    <div class="alert-warning checkbox checkbox-styled tile-text" style="padding: 10px;">
                        <label>
                           <input name="chkOportunity" class="chkOportunity" type="checkbox" value="1" checked>
                          <span>{{ trans('client.oportunity') }}</span>
                      </label>
                    </div>

                    <div class="form-group" id="g_dateReminder">
                      <label>{{ trans('client.oportunity_reminder') }}</label>
                      {!! Form::input('text', 'oportunity_reminder', '', ['class'=> 'form-control datepicker', 'id' => 'oportunity_reminder']) !!}
                    </div>

                    <div class="form-group" id="g_notes">
                        <label>{{ trans('client.notes') }}</label>
                        {!! Form::textarea('notes', '', ['class'=> 'form-control', 'id' => 'notes','autocomplete' => 'false', 'rows' => '4']) !!}
                    </div>

                    <div class="form-footer">
                      <button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
                      <button type="submit" class="btn btn-primary sendNewClientBtn" name="button">{{ trans('client.send_lbl') }}</button>
                    </div>

                  </form>

          </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
        <a href="javascript:void(0);" class="btn btn-primary nextFrmNewClient">{{ trans('client.next_lbl') }}</a>
      </div>

    </div>
  </div>
</div>


<script>

        $('.datepicker').datetimepicker({
          format: 'Y-MM-DD',
          allowInputToggle: true,
          //minDate: moment(),
        });


        $('.sendNewClientBtn').click(function(e){
            //$(".frmNewClient").submit();
            });
        
</script>