<div class="modal fade" id="modalDeleteStorage" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id=""> {{ trans('storages.deleteTitle') }} </h4>
      </div>
      <div class="modal-body">
        <h3>{{ trans('storages.delete_q') }}</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('storages.close') }}</button>
        <button type="button" class="btn btn-primary btnConfirmDelStorage">{{ trans('storages.delete') }}</button>
      </div>
    </div>
  </div>
</div>