
<div class="modal fade" id="storageInvoices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:-1.7%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">{{ trans('client.invoices') }}</h4>
            </div>
            <div class="modal-body" id="modal_rent">

                <table class="table table-hover" id="tblStorageInvoices">
                  <thead>
                    <tr>
                      <th>{{ trans('client.name_lbl') }}</th>
                      <th>{{ trans('client.actions_lbl') }}</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
