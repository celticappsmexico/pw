<div class="modal fade" id="modalBraintree" style="z-index: 9999!important;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:red !important" >{{ trans('calculator.rent_box') }}</h4>
			</div>
			<form id="payment-form" method="post" action="{{ route('sendPay') }}">

				<div class="modal-body" style="padding:40px 40px;">
					@if (Auth::guest())

		            @elseif (Auth::user()->Role_id == 1)
						<div class="alert alert-success alertClientPayment">
							<a href="#" class="close alertClientPaymentClose" aria-label="close">&times;</a>
							<span id="clientName"></span>
						</div>
		            @elseif (Auth::user()->Role_id == 3)

		            @endif

					<div class="row" id="g_client">
						@if (Auth::guest())

			            @elseif (Auth::user()->Role_id == 1)
							<div class="col-md-10">
								<div class="form-group">
									<label for="">{{ trans('calculator.find_client') }}</label>
									<input type="text" name="find_client" id="find_client" list="find_client_list" class="find_client form-control" value="">
									<datalist id="find_client_list"></datalist>
								</div>
							</div>
							<div class="col-md-2">
								<a href="javascript:void(0);" class="btn btn-primary btnNewClient"><i class="fa fa-user-plus"></i></a>
							</div>
			            @elseif (Auth::user()->Role_id == 3)

			            @endif

						<div style="visibility: hidden;">
							<input type="text" name="id_client" id="id_client" value="">
							<input type="text" name="term_notice" id="term_notice" value="">
							<input type="text" name="prePay" id="prePay" value="">
							<input type="text" name="cc" id="cc" value="">
							<input type="text" name="rent_start" id="rent_start" value="">
							<input type="text" name="id_storage" id="id_storage" value="">
							<input type="text" name="_token" id="_token">
						</div>
					</div>

              <div class="row braintreeWrapper">
                  <div class="bt-drop-in-wrapper">
                      <div id="bt-dropin"></div>
                  </div>
              </div>
              	<div class="row">
					<div class="form-group" id="">
                  		<label for="amount">{{ trans('calculator.amount') }}</label>
                  		<input id="price" name="amount" type="text" min="1" placeholder="Amount" value="" class="form-control" readonly />
              		</div>
				</div>

				<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
				<script>
					// We generated a client token for you so you can test out this code
					// immediately. In a production-ready integration, you will need to
					// generate a client token on your server (see section below).
					/**<?php
						//$token = $clientToken = \Braintree_ClientToken::generate();
					?>**/
		  			//clientToken = "<?php //echo $token; ?>";

					/**braintree.setup(clientToken, "dropin", {
						container: "bt-dropin"
					});**/
				</script>


          </div>

					<div class="modal-footer">
						@if (Auth::guest())

									@elseif (Auth::user()->Role_id == 1)
							<button type="button" class="btn btn-info" id="nextPayment"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Next Payment</button>
							<button type="button" class="btn btn-info" id="sendPaymentCash"><i class="fa fa-money" aria-hidden="true"></i> Cash Payment</button>
							<button type="submit" class="btn btn-info" id="sendPay"><i class="fa fa-paper-plane-o"></i> Pay</button>
									@elseif (Auth::user()->Role_id == 3)
							<button type="submit" class="btn btn-info" id="sendPayClient"><i class="fa fa-paper-plane-o"></i> Pay</button>
									@endif
					</div>
      </form>
		</div>
	</div>
</div>
