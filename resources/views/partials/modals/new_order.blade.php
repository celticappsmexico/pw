<div class="modal fade" id="order_modal" tabindex="-1" role="dialog" aria-labelledby="" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">{{ trans('calculator.new_order_title') }}</h4>
      </div>
      <div class="modal-body">
        <h2>{{ trans('calculator.order_exist') }}</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default assignOrderNoClosed" data-dismiss="modal">{{ trans('calculator.assign_order') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('calculator.close_order') }}</button>
        <button type="button" class="btn btn-primary newOrderModal">{{ trans('calculator.new_order') }}</button>
      </div>
    </div>
  </div>
</div>
