<div class="modal fade" id="modalAddBox" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">{{ trans('calculator.add_storage_to_client') }}</h4>
      </div>
      <div class="modal-body">

        <form class="modalAddBoxFrm1" name="modalAddBoxFrm1">

          <label for="cboWarehouse">Building:</label>
          <select class="form-control cboWarehouse" name="cboWarehouse">
            <option selected>{!! trans('calculator.select_option') !!}</option>
            @foreach ($buildings as $w)
              <option value="{{ $w->id }}">{{ $w->name }}</option>
            @endforeach
          </select>

          <label for="cboLevel">Levels:</label>
          <select class="form-control cboLevel" name="cboLevel" disabled>
            <option selected>{!! trans('calculator.select_option') !!}</option>
          </select>

          <div class="form-group">
          <label>{{ trans('client.search_box') }}</label>
          <input id="searchStorageTxt" class="form-control">
          </div>

        </form>

        <form class="modalAddBoxFrm2" name="modalAddBoxFrm2">

            <input type="hidden" name="storageId" id="storageId">
            <input type="hidden" name="idClient" id="idClient">

            <table class="table-striped table-bordered tblCalcStorages2">
              <tr>
                <td>
                </td>
                <td>
                  <img src="{{ asset('assets/images/size-estimator/storage-box.png') }}" alt="" />
                </td>
              </tr>
              <!-- <tr>
                <td>
                </td>
                <td>
                  <a href="javascript:void(0);" id="seeOnMap2" class="seeOnMap btn btn-default btn-xs"> {{ trans('calculator.see_on_map')}} </a>
                </td>
              </tr> -->
              <tr>
                <td>{{ trans('calculator.sqm') }}</td>
                <td id="sqm3"></td>
              </tr>
              <tr>
                <td>{{ trans('calculator.price') }}</td>
                <td>
                  <input type="text" name="price3" id="price3" class="form-control">
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.term_notice') }}</td>
                <td>
                  <select class="form-control cboTermNotice" name="cboTermNotice">
                    <option selected>{{ trans('calculator.select_option') }}</option>
                    @foreach ($cataterm as $term)
                      <option value="{{$term->id}}" selected>{{$term->months}}</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.pre-pay') }}</td>
                <td>
                  <select class="form-control cboPrePay" name="cboPrePay">
                    <option selected>{{ trans('calculator.select_option') }}</option>
                    @foreach ($cataprepay as $pre)
                      <option value="{{$pre->id}}" selected>{{$pre->months}}</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.credit_card') }}</td>
                <td>
                  <select class="form-control cboCc" name="cboCc">
                    <option value="" selected>{{ trans('calculator.select_option') }}</option>
                    <option value="1">{{ trans('calculator.yes') }}</option>
                    <option value="2">{{ trans('calculator.no') }}</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.credit_card_percent') }}</td>
                <td colspan="2">
                  <input type="number" name="ccp" id="ccp" class="form-control" value="20">
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.rent_start') }}</td>
                <td>
                  <div class='input-group date' id='rentStart3'>
                    <input type="text" name="resntStart" class="form-control rentStart" value="">
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <script>
                      jQuery(document).ready(function($) {
                        $('#rentStart3').datetimepicker({
                          format: 'DD-MM-Y',
                          allowInputToggle: true,
                          //minDate: moment(),
                        });
                      });
                    </script>
                  </div>
                </td>
              </tr>
              <tr>
                <td>{{ trans('calculator.insurance') }}</td>
                <td colspan="2">
                  <select class="form-control cboInsurance" name="cboCc">
                    <option value="" selected>{{ trans('calculator.select_option') }}</option>
                    <option value="0">{{ trans('calculator.no') }}</option>
                    <option value="16">{{ Lang::get('calculator.insurance_16') }} </option>
                    <option value="32">{{ Lang::get('calculator.insurance_32') }} </option>
                    <option value="48">{{ Lang::get('calculator.insurance_48') }} </option>
                    <option value="64">{{ Lang::get('calculator.insurance_64') }} </option>
                    <option value="80">{{ Lang::get('calculator.insurance_80') }} </option>
                  </select>
                </td>
              </tr>
              
              <tr>
              	<td></td>
              	<td colspan="2" style="text-align:center">
              		<a href="javascript:void(0);" class="btnCalculateSt btn btn-primary"> {{ trans('calculator.calculate') }} </a>
              	</td>
              </tr>
              
              <tr>
                <td>{{ trans('calculator.subtotal') }}</td>
                <td id="sub3"></td>
              </tr>
              <tr>
                <td>{{ trans('calculator.vat') }} (+23%)</td>
                <td id="vat3"></td>
              </tr>
              <tr class="totalesCalc">
                <td>{{ trans('calculator.total') }}</td>
                <td class="totalCalc" id="">
                  <input type="number" name="total3" id="total3" class="form-control">
                </td>
              </tr>
              
              <tr class="totalesCalc">
                <td>{{ trans('calculator.deposit_month') }}</td>
                <td class="totalCalc" id="">
                  <input type="number" name="deposit_month" id="deposit_month" class="form-control" value="0" min="0">
                </td>
              </tr>
              
              <!-- <tr>
                <td></td>
                <td><a href="javascript:void(0);" dir="1" class="rent1 btn btn-primary"> {{ trans('calculator.add_item') }} </a></td>
              </tr> -->
            </table>

		<!-- VALORES ESCONDIDOS QUE REGGRESA EL HELPER -->
		      <input type="hidden" id="pd_box_price" />
		      <input type="hidden" id="pd_box_vat" />
		      <input type="hidden" id="pd_box_total" />
		      <input type="hidden" id="pd_insurance_price" />
		      <input type="hidden" id="pd_insurance_vat" />
		      <input type="hidden" id="pd_insurance_total" />
		      <input type="hidden" id="pd_subtotal" />
		      <input type="hidden" id="pd_total_vat" />
		      <input type="hidden" id="pd_total" />
		      <input type="hidden" id="pd_total_next_month" />
		      
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btnAssignStorage">{{ trans('calculator.assign_box') }}</button>
        <!-- <button type="button" class="btn btn-info btnProceedPayAssign">{{ trans('calculator.proceed_to_pay') }}</button> -->
      </div>
    </div>
  </div>
</div>

      

<script>

$(".btnAssignStorage").attr("disabled","disabled");

$(document).on('click', '.btnCalculateSt', function(event) {
	event.preventDefault();

	var idStorage = $('.modalAddBoxFrm2 #storageId').val();
	var date_start = $(".rentStart").val();
	var term = $(".cboTermNotice").val();
	var insurance = $(".cboInsurance").val();
	var extra_percentage = $("#ccp").val();
	var prepay = $(".cboPrePay").val();
	var idStorage2 = $(".pos2").val();
	var credit_card = $(".cboCc").val();
	
	if(date_start == ""){
		toastr['error']("{{ trans('calculator.choose_rentStart') }}");
		return false;
	}

	if(insurance == ""){
		toastr['error']("{{ trans('calculator.choose_insurance') }}");
		return false;
	}

	if(prepay == ""){
		toastr['error']("{{ trans('calculator.choose_prePay') }}");
		return false;
    }

    if(credit_card == ""){
    	toastr['error']("{{ trans('calculator.choose_cc') }}");
		return false;
	}

    if(term == ""){
    	toastr['error']("{{ trans('calculator.choose_term') }}");
		return false;
    }
	

	showLoader();
	$.post( '{!! route('calculator.calculate') !!}', { 
			'_token'			: '{{ csrf_token() }}',
			'idStorage'			: idStorage, 
			'date_start'		: date_start, 
			'insurance'			: insurance,
			'extra_percentage'	: extra_percentage, 
			'prepay'			: prepay,
			'term'			: term,    
		}, function( data ) {
			var json = JSON.stringify(data.estimator);
			json = JSON.parse(json);

			//$("#price1_fixed").text(json['price']);
			//$("#sqm1_fixed").text(json['sqm']);
			
			$("#sub3").text(json['currentMonthSubtotal']);
			$("#vat3").text(json['currentMonthVAT']);
			$("#total3").val(json['currentMonthTotal']);

			$("#nextMonth1_fixed").text(json['nextMonthTotal']);

			//INPUTS
			$("#pd_box_price").val(json['currentMonthBoxPrice']);
			$("#pd_box_vat").val(json['currentMonthBoxVAT']);
			$("#pd_box_total").val(json['currentMonthBoxTotal']);
			$("#pd_insurance_price").val(json['currentMonthInsurancePrice']);
			$("#pd_insurance_vat").val(json['currentMonthInsuranceVAT']);
			$("#pd_insurance_total").val(json['currentMonthInsuranceTotal']);
			$("#pd_subtotal").val(json['currentMonthSubtotal']);
			$("#pd_total_vat").val(json['currentMonthVAT']);
			$("#pd_total").val(json['currentMonthTotal']);
			$("#pd_total_next_month").val(json['nextMonthTotal']);
			
	});

	setTimeout(function(){
          hideLoader();
        },1000);	 

	$(".btnAssignStorage").attr("disabled",false);
	
  });

	$('.cboCc').change(function() {
	    //alert('change!');
	    //calculatePrice(1);
	    //calculatePrice(2);
	    if($(this).val() == "1"){
	    	//YES
	    	$("#ccp").val(0);
	    }
	    else{
	    	//NO
	    	$("#ccp").val(20);
	    }
	  });
</script>

<style media="screen">

/* highlight results */
.ui-autocomplete span.hl_results {</button>
  background-color: #ffff66;
}

/* loading - the AJAX indicator */
.ui-autocomplete-loading {
  background: white url(' {{ asset('assets/images/loader_16x16.gif') }}') right center no-repeat;
}

/* scroll results */
.ui-autocomplete {
  max-height: 250px;
  max-width: 768px;
  overflow-y: auto;
  /* prevent horizontal scrollbar */
  overflow-x: hidden;
  /* add padding for vertical scrollbar */
  /*padding-right: 5px;*/
  margin: 0px!important;
  padding: 0px!important;
  background-color: rgb(252, 252, 252);
}

.ui-autocomplete li {
  font-size: 12px;
  padding: 5px 10px;
  border-bottom: solid 1px rgb(164, 164, 164);
  list-style-type: none;
}

.ui-autocomplete li:hover {
  background-color: rgb(250, 255, 145);
}

/* IE 6 doesn't support max-height
* we use height instead, but this forces the menu to always be this tall
*/
* html .ui-autocomplete {
  height: 250px;
}

.ui-helper-hidden-accessible{
  background: rgb(249, 249, 249);
  padding: 3px;
  visibility: hidden;
  display: none!important;
}

.modal { overflow: visible; }
.modal-body { overflow-y: visible !important; }
</style>
