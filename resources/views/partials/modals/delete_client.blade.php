<div class="modal fade" id="modalDeleteClient" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id=""> {{ trans('client.delete_lbl') }} </h4>
      </div>
      <div class="modal-body">
        <h3>{{ trans('client.delete_q_lbl') }}</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
        <button type="button" class="btn btn-primary btnConfirmDelClient">{{ trans('client.delete_lbl') }}</button>
      </div>
    </div>
  </div>
</div>
