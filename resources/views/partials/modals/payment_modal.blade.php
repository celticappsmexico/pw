<div class="modal fade" id="modalPayment" style="z-index: 9999!important;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('calculator.rent_box') }}</h4>
			</div>

			<div class="modal-body" style="padding:20px 20px;">

				<div class="wrapPaymentType">
					<select class="form-control cboPaymentType" name="cboPaymentType" id="cboPaymentType">
			          @foreach ($catapaymenttype as $pt)
			            <option value="{{ $pt->id }}">{{ $pt->name }}</option>
			          @endforeach
			        </select>
				</div>

				<div class="wrapTransfer">
					<form class="" action="{{ route('pay') }}" method="post" id="frmManualPay">
					
						<input type="hidden" name="_token" id="_token">
						<input type="hidden" name="orderId" id="orderId">
						<input type="hidden" name="paymentType" id="paymentType" value="1">
					
						<input type="hidden" name="flag_paid" id="flag_paid" />
						
						<div class="form-group" id="g_reference">
							<label>{{ trans('cart.reference') }}</label>
								 {!! Form::input('text', 'reference', '', ['class'=> 'form-control', 'id' => 'reference', 'required' => 'true']) !!}
						</div>
						
						<!--<input type="hidden" name="flag_paid" id="flag_paid" />-->

						<button type="submit" class="btn btn-info" id="sendPay1"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>
					</form>
					<!-- <a href="javascript:void(0)" class="btn btn-info btnPayCash"> {{ trans('cart.pay') }}</a>-->
				</div>


				<div class="wrapManualPay">
					<form class="" action="{{ route('pay') }}" method="post" id="frmManualPay">
						<input type="hidden" name="_token" id="_token">
						<input type="hidden" name="orderId" id="orderId">
						<input type="hidden" name="paymentType" id="paymentType" value="1"/>
						
						<input type="hidden" name="flag_paid" id="flag_paid" />
						
						<button type="submit" class="btn btn-info" id="sendPay2"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>
					</form>
					<!-- <a href="javascript:void(0)" class="btn btn-info btnPayCash"> {{ trans('cart.pay') }}</a>-->
				</div>

				<form id="payment-form" method="post" action="{{ route('pay') }}">
					{{ csrf_field() }}
					
					<input type="hidden" name="orderId" id="orderId">
					<input type="hidden" name="paymentType" id="paymentType" value="2">
					
					<input type="hidden" name="flag_paid" id="flag_paid" />
					
					<div class="wrapBraintree" id="braintree-cards">
					</div>
					
					<input type="hidden" name="customer" id="customer_card"  />
					<input type="hidden" name="token_card" id="token_card"  />
						
					<!-- 
					<div class="wrapBraintree">
						<div class="row">
							<div id="bt-dropin2"></div>

							<div class="alert alert-dismissible alert-warning">
								<h4>{{ trans('calculator.notice') }}</h4>
  							<p>{{ trans('calculator.notice_label') }}</p>
								</div>

							<div class="form-group" id="g_suscription">
			        			<label>{{ trans('cart.suscription_id') }}</label>
			             		{!! Form::input('text', 'suscriptionId', '', ['class'=> 'form-control', 'id' => 'suscriptionId', 'required' => 'true']) !!}
			        		</div>

							<div class="form-group" id="g_amount">
			        			<label>{{ trans('cart.amount') }}</label>
			             		{!! Form::input('text', 'amount', '', ['class'=> 'form-control', 'id' => 'amount', 'readonly' => 'true']) !!}
			        		</div>


							<input type="hidden" name="orderId" id="orderId">
							<input type="hidden" name="paymentType" id="paymentType">
							<input type="hidden" name="_token" id="_token">

							<button type="submit" class="btn btn-info" id="sendPay3"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>

						</div>
						<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
						<script>
								// We generated a client token for you so you can test out this code
								// immediately. In a production-ready integration, you will need to
								// generate a client token on your server (see section below).
								<?php

									$token = $clientToken = \Braintree_ClientToken::generate();

								?>
								clientToken = "<?php echo $token; ?>";

								braintree.setup(clientToken, "dropin", {
									container: "bt-dropin2"
								});
						</script>
					</div>
					 -->
				</form>

				<div class="payment data" style="visibility: hidden;">
					<input type="text" name="amountPayment" id="amountPayment">
					<!-- <input type="text" name="orderId" id="orderId"> -->
				</div>



      </div>

			<div class="modal-footer">
					<button type="submit" class="btn btn-info" id="nextPay"></i>{{ trans('cart.next') }}</button>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		
		$('.wrapBraintree').hide();
		
		/*
		$("#size").children().each(function() {
	        if ($(this).val() == 'medium') {
	            $(this).prop('disabled',true);
	        }
    	});
		*/

		$(document).on('click', '#nextPay', function() {
			
			type = $('.cboPaymentType').val();

			$(this).slideUp(200);
			$('.wrapPaymentType').slideUp(200);

			if (type == 1) {
				paymentType = $('#modalPayment .cboPaymentType').val();
				orderId = $('#modalPayment #orderId').val();
				$('#frmManualPay #orderId').val(orderId);
				$('.wrapManualPay #_token').val(token);
				$('.wrapManualPay').slideDown(200);
			}

			if (type == 2) {
				$('.wrapBraintree').slideDown(200);
				/*
				$('.wrapBraintree #_token').val(token);
				$('.wrapBraintree #paymentType').val(2);
				*/

				orderId = $('#modalPayment #orderId').val();
				
				//NUEVA FUNCIONALIDAD								

				var storage_id = $("#idStorageOrderDetail").val();
				var client_id = $("#idClienteOrderDetail").val();
				var paymentId = $("#paymentIdOrderDetail").val();

				var route = "{{ route('cards_users.credit_card_braintree',['*user*','*storage*','*paymentid*']) }}";
				route = route.replace('*user*',client_id);
				route = route.replace('*storage*',storage_id);
				route = route.replace('*paymentid*',paymentId);
				
				
			    $('#braintree-cards').load(route);

				
			}

			if (type == 3) {
				$('.wrapTransfer').slideDown(200);
				$('.wrapTransfer #_token').val(token);
				$('.wrapTransfer #paymentType').val(3);
			}

			if (type == 4) {
				$('.wrapTransfer').slideDown(200);
				$('.wrapTransfer #_token').val(token);
				$('.wrapTransfer #paymentType').val(4);
			}			

		});

		function sendPay(){
			showLoader();
			orderId = $('#modalPayment #orderId').val();
			paymentType = $('#modalPayment .cboPaymentType').val();
			
			cad = '_token='+token+'&orderId='+orderId+'&paymentType='+paymentType;
			route = '{{ route('downloadInvoice',['*orderId*']) }}';
			route = route.replace('*orderId*',orderId);

			$('#modalPayment').modal('hide');

			$.ajax({
			      url: '{{ route('pay') }}',
			      type: 'POST',
			      data: cad,
			    })
	    	.done(function(data) {
				if (data.success) {
					toastr["success"]("{{ trans('messages.success_transaction') }}");
					window.open(route);
					//toastr['success']('Done!');
				}else{
					toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
					setTimeout(function(){
						hideLoader();
					},2000);
				}
				setTimeout(function(){
					//location.reload();
				},3000);

	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

		}

		$(document).on('click', '.wrapBraintree .btnSelectCard', function(event) {
			//event.preventDefault();
			
			$(this).attr("disabled","disabled");

			showLoader();

			
			var user_id = $(this).data('user');
		  	var token = $(this).data("token");
		  	var customer = $(this).data("customer");
		  	var storage_id = $(this).data("storage");
		  	var paymentid = $(this).data("paymentid");

		  	$("#customer_card").val(customer);
			$("#token_card").val(token);
		  	
			$("#payment-form").trigger("submit");
		  	/*
		  	{{--
		  	var route="{{route('create_transaction',['*creditCardToken*','*customerId*','*planId*','*subscribed*','*storage_id*','*paymentid*',1])}}";

		  	
		  	route = route.replace('*creditCardToken*',token);
			route = route.replace('*customerId*',customer);
			route = route.replace('*planId*','{{env("PLAN_ID")}}');
			route = route.replace('*subscribed*','1');
			route = route.replace('*storage_id*',storage_id);
			route = route.replace('*paymentid*',paymentid);
			
		  	$.get(route, function(response, status){
				 				 
				 if(response.returnCode == 200){
					 
			  		toastr['success']("{{ trans('cart.suscription_success') }}");
			  		
			  		$('#modal-generico-small').modal('hide');
			  		orderId = $('#modalPayment #orderId').val();
					paymentType = $('#modalPayment .cboPaymentType').val();
					
					//REALIZA EL PAGO
					$("#orderIdBraintree").val(orderId);
					$("#payment-form").trigger("submit");

				 }
		    });

		  	--}}
		    */
		});

		$(document).on('click', '.btnPayCash', function() {
			sendPay();
		})

	});


</script>
