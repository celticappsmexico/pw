jQuery.extend(jQuery.validator.messages, {
    required: "{{ trans('messages.required') }}",
    remote: "{{ trans('messages.remote') }}",
    email: "{{ trans('messages.email') }}",
    url: "{{ trans('messages.url') }}",
    date: "{{ trans('messages.date') }}",
    dateISO: "{{ trans('messages.dateISO') }}",
    number: "{{ trans('messages.number') }}",
    digits: "{{ trans('messages.digits') }}",
    creditcard: "{{ trans('messages.creditcard') }}",
    equalTo: "{{ trans('messages.equalTo') }}",
    accept: "{{ trans('messages.accept') }}",
    maxlength: jQuery.validator.format("{{ trans('messages.maxlength') }}"),
    minlength: jQuery.validator.format("{{ trans('messages.minlength') }}"),
    rangelength: jQuery.validator.format("{{ trans('messages.rangelength') }}"),
    range: jQuery.validator.format("{{ trans('messages.range') }}"),
    max: jQuery.validator.format("{{ trans('messages.max') }}"),
    min: jQuery.validator.format("{{ trans('messages.min') }}")
});