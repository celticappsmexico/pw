$(document).ready(function(){
    //**********Variables modal rent**********//
    var price1, price2, subtotal1, subtotal2, total1, total2, descNotice, descPrepay;
    var storage_clicked;

    //**********Cargar modal rent storage**********//
    $(document).on('click', '#btn_search', function(){
        total = parseFloat($('#span_total').text());
        cad = 'total='+total+'&_token='+token;
        $('#modal_rent').empty();
        $.ajax({
            url: '{{ route('searchStorage') }}',
            type: 'POST',
            data: cad
        })
        .done(function(data){
            $('#rent_storage').modal('show');
            $('#rent_storage .modal-body').append(data);
            $('.select-style').selectpicker();
            //$('#btn_rent_1').addClass('disabled');
            //$('#btn_rent_2').addClass('disabled');
            price1 = parseFloat($('#span-price-1').text());
            price2 = parseFloat($('#span-price-2').text());

            subtotal1 = price1+16;
            subtotal2 = price2+16;
            $('#span-subtotal-1').text(subtotal1.toFixed(2)+" pln");
            $('#span-subtotal-2').text(subtotal2.toFixed(2)+" pln");

            total1 = subtotal1+((subtotal1*23)/100);
            total2 = subtotal2+((subtotal2*23)/100);
            $('#span-total-rent-1').text(total1.toFixed(2)+" pln");
            $('#span-total-rent-2').text(total2.toFixed(2)+" pln");
        })
        .fail(function(){
            console.log("error");
        });

    });
    //**********Validar datos completos de usuario**********//
    $(document).on('click', '#btn_rent_1', function(){
        if ($('#select-notice').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_term')}}");
        }else if ($('#select-prepaid').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_prePay')}}");
        }else if ($('#select-credit-card').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_cc')}}");
        }else {
            /*$.ajax({
                url: '{{ route('validateUser') }}',
                type: 'GET'
            })
            .done(function(data){
                if (data == 1) {
                    window.location.href = "homeCompleteUser";
                }else{
                    alert("Usuario completo");
                }
            })
            .fail(function(){
                console.log("error");
            });*/
            storage_clicked = "1";
            $('#extra_items').modal('show');
        }
    });

    $(document).on('click', '#btn_rent_2', function(){
        if ($('#select-notice').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_term')}}");
        }else if ($('#select-prepaid').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_prePay')}}");
        }else if ($('#select-credit-card').val()=='def') {
            toastr["error"]("{{ trans('calculator.choose_cc')}}");
        }else {
            /*$.ajax({
                url: '{{ route('validateUser') }}',
                type: 'GET'
            })
            .done(function(data){
                if (data == 1) {
                    window.location.href = "homeCompleteUser";
                }else{
                    alert("Usuario completo");
                }
            })
            .fail(function(){
                console.log("error");
            });*/

            storage_clicked = "2";
            $('#extra_items').modal('show');
        }
    });

    $(document).on('click', '#btn_proceed_pay', function(){
        totalExtraItems = parseFloat($('#total_extra_items').text());
        totalStorage = parseFloat($('#span-total-rent-'+storage_clicked).text());
        fecha = new Date();
        dd = fecha.getDate();
        mm = fecha.getMonth()+1;
        yyyy = fecha.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }

        if(mm<10) {
            mm='0'+mm
        }

        fecha = mm+'/'+dd+'/'+yyyy;

        $('#modalBraintree').modal('show');
        $('#modalBraintree .braintreeWrapper').slideDown(200);
        $('#modalBraintree #id_client').val($('#span_id_user').text());
        $('#modalBraintree #price').val((totalExtraItems+totalStorage).toFixed(2));
        $('#modalBraintree #term_notice').val($('#select-notice').val());
        $('#modalBraintree #prePay').val($('#select-prepaid').val());
        $('#modalBraintree #cc').val($('#select-credit-card').val());
        $('#modalBraintree #rent_start').val(fecha);
        $('#modalBraintree #id_storage').val($('span_id_storage_'+storage_clicked).text());
        $('#modalBraintree #_token').val(token);
        $('#modalBraintree .alertClientPayment').hide();
        $('#modalBraintree .braintreeWrapper').hide();
        $('#modalBraintree #sendPaymentCash').hide();
        $('#modalBraintree #sendPay').hide();
        $('#modalBraintree #nextPayment').show();
        $('#modalBraintree #g_client').show();
        $('#extra_items').hide();
    });

    //**********Condiciones Rent Storage**********//
    $(document).on('click','.btn_extraItems',function(){
        $('#extra_items2').modal('show');
    });
    //**********Condiciones notice**********//
    $(document).on('change', '#select-notice', function(){
        if (this.value != 'def') {
            $('#select-notice').find('[value=def]').hide();
            $('#select-notice').selectpicker('refresh');
        }
        switch (this.value) {
            case "def":
                break;
            case "1":
                descNotice = 0;
                $('#select-notice').find('[value=def]').show();
                break;
            case "3":
                descNotice = 5;
                $('#select-prepaid').attr('disabled', false).selectpicker('refresh');
                break;
            case "6":
                descNotice = 10;
                $('#select-prepaid').attr('disabled', false).selectpicker('refresh');
                break;
            case "12":
                descNotice = 15;
                $('#select-prepaid').attr('disabled', false).selectpicker('refresh');
                break;
            default:
                descNotice = 0;
                $('#select-notice').find('[value=def]').show();
        }
        subtotal1 = price1+16;
        subtotal2 = price2+16;
        if (descNotice > 0) {
            subtotal1 -= (subtotal1*descNotice)/100;
            subtotal2 -= (subtotal2*descNotice)/100;
            if ($('#select-prepaid').val() == "Yes") {
                subtotal1 -= (subtotal1*descNotice)/100;
                subtotal2 -= (subtotal2*descNotice)/100;
            }
        }
        if ($('#select-credit-card').val() == "Yes") {
            subtotal1 -= (subtotal1*20)/100;
            subtotal2 -= (subtotal2*20)/100;
        }else if ($('#select-credit-card').val() == "No") {
            subtotal1 += (subtotal1*20)/100;
            subtotal2 += (subtotal2*20)/100;
        }
        $('#span-subtotal-1').text(subtotal1.toFixed(2)+" pln");
        $('#span-subtotal-2').text(subtotal2.toFixed(2)+" pln");
        total1 = subtotal1+((subtotal1*23)/100);
        total2 = subtotal2+((subtotal2*23)/100);
        $('#span-total-rent-1').text(total1.toFixed(2)+" pln");
        $('#span-total-rent-2').text(total2.toFixed(2)+" pln");
    });
    //**********Condiciones pre-pay**********//
    $(document).on('change', '#select-prepaid', function(){
        if (this.value != 'def') {
            $('#select-prepaid').find('[value=def]').hide();
            $('#select-prepaid').selectpicker('refresh');
        }
        notice = $('#select-notice').val();
        if (notice != "def") {
            switch (this.value) {
                case "Yes":
                    switch (notice) {
                        case "3":
                            descPrepay = 5;
                            break;
                        case "6":
                            descPrepay = 10;
                            break;
                        case "12":
                            descPrepay = 15;
                            break;
                        default:
                            descPrepay = 0;
                    }
                    break;
                case "No":
                    descPrepay = 0;
                    break;
                default:
                descPrepay = 0;
            }
            subtotal1 = price1+16;
            subtotal2 = price2+16;
            subtotal1 -= (subtotal1*descNotice)/100;
            subtotal2 -= (subtotal2*descNotice)/100;
            if (descPrepay > 0) {
                subtotal1 -= (subtotal1*descPrepay)/100;
                subtotal2 -= (subtotal2*descPrepay)/100;
            }
            if ($('#select-credit-card').val() == "Yes") {
                subtotal1 -= (subtotal1*20)/100;
                subtotal2 -= (subtotal2*20)/100;
            }else if ($('#select-credit-card').val() == "No") {
                subtotal1 += (subtotal1*20)/100;
                subtotal2 += (subtotal2*20)/100;
            }
            $('#span-subtotal-1').text(subtotal1.toFixed(2)+" pln");
            $('#span-subtotal-2').text(subtotal2.toFixed(2)+" pln");
            total1 = subtotal1+((subtotal1*23)/100);
            total2 = subtotal2+((subtotal2*23)/100);
            $('#span-total-rent-1').text(total1.toFixed(2)+" pln");
            $('#span-total-rent-2').text(total2.toFixed(2)+" pln");
        }
    });
    //**********Condiciones credit card **********//
    $(document).on('change', '#select-credit-card', function(){
        if (this.value != 'def') {
            $('#select-credit-card').find('[value=def]').remove();
            $('#select-credit-card').selectpicker('refresh');
        }
        switch (this.value) {
            case "def":
                subtotal1 = price1+16;
                subtotal2 = price2+16;
                if (descNotice > 0) {
                    subtotal1 -= (subtotal1*descNotice)/100;
                    subtotal2 -= (subtotal2*descNotice)/100;
                }
                if (descPrepay > 0) {
                    subtotal1 -= (subtotal1*descPrepay)/100;
                    subtotal2 -= (subtotal2*descPrepay)/100;
                }
                break;
            case "Yes":
                subtotal1 = price1+16;
                subtotal2 = price2+16;
                if (descNotice > 0) {
                    subtotal1 -= (subtotal1*descNotice)/100;
                    subtotal2 -= (subtotal2*descNotice)/100;
                }
                if (descPrepay > 0) {
                    subtotal1 -= (subtotal1*descPrepay)/100;
                    subtotal2 -= (subtotal2*descPrepay)/100;
                }
                subtotal1 -= (subtotal1*20)/100;
                subtotal2 -= (subtotal2*20)/100;
                break;
            case "No":
                subtotal1 = price1+16;
                subtotal2 = price2+16;
                if (descNotice > 0) {
                    subtotal1 -= (subtotal1*descNotice)/100;
                    subtotal2 -= (subtotal2*descNotice)/100;
                }
                if (descPrepay > 0) {
                    subtotal1 -= (subtotal1*descPrepay)/100;
                    subtotal2 -= (subtotal2*descPrepay)/100;
                }
                subtotal1 += (subtotal1*20)/100;
                subtotal2 += (subtotal2*20)/100;
                break;
            default:
                subtotal1 = price1+16;
                subtotal2 = price2+16;
                if (descNotice > 0) {
                    subtotal1 -= (subtotal1*descNotice)/100;
                    subtotal2 -= (subtotal2*descNotice)/100;
                }
                if (descPrepay > 0) {
                    subtotal1 -= (subtotal1*descPrepay)/100;
                    subtotal2 -= (subtotal2*descPrepay)/100;
                }
        }
        $('#span-subtotal-1').text(subtotal1.toFixed(2)+" pln");
        $('#span-subtotal-2').text(subtotal2.toFixed(2)+" pln");
        total1 = subtotal1+((subtotal1*23)/100);
        total2 = subtotal2+((subtotal2*23)/100);
        $('#span-total-rent-1').text(total1.toFixed(2)+" pln");
        $('#span-total-rent-2').text(total2.toFixed(2)+" pln");
    });

});
