$(document).ready(function() {

  /*
  $('.folder').click(function() {
    //$('.gui-folder').next().find(ul).show();
      return false;
  }).dblclick(function() {
      window.location = this.href;
      return false;
  });
  */

   $('.folder').hover(function(){
    //alert($(this).parent('li').find('ul:first').attr('class'));
    //$(this).next().find('ul').show();
    $(this).parent('li').find('ul:first').show();
   });


  $('.pageLoader').hide();

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "5000",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }


  /* **********************************************
  ** Validacion Registro
  ************************************************/
  $('#forRegister').validate({
      rules: {
          username: {
              minlength: 3,
              required: true,
              remote: {
                url: checkUsernameUrl,
                type: "post",
                data: {
                        username: function() {
                          return $( "#username" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          name: {
              minlength: 3,
              maxlength: 45,
              required: true
          },
          email: {
              email: true,
              required: true,
              remote: {
                url: checkEmailUrl,
                type: "post",
                data: {
                        email: function() {
                          return $( "#email" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          password: {
            required: true
          },
          password_confirmation: {
            required: true,
            equalTo: "#password"
          },
          street: {
            required: true
          },
          number: {
            required: true
          },
          postCode: {
            required: true
          },
          city: {
            required: true
          },
          country: {
            required: true
          },
          peselNumber: {
            required: true
          },
          idNumber: {
            required: true
          },
          phone: {
            required: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      }
  });


  /* **********************************************
  ** New client AUTH
  ************************************************/

  $('.btnNewClient').click(function() {
    resetModalNewClient();
    $('#modalNewClidnt').modal('show');
  });

  $('.closeFrmNewClient').click(function() {
    setTimeout(function(){
      resetModalNewClient();
    },500);
  });

  $('.nextFrmNewClient').click(function() {
    $('.frmNewClientStep1').hide(200);
    $('.frmNewClientStep2').show(200);
    $('.nextFrmNewClient').hide(200);
    $('.sendFrmNewClient').show(200);
  });

  function resetModalNewClient(){
    $('.lblClientType').empty();
    $('.frmNewClient')[0].reset();
    validator1 = $( "#frmNewClientOnePerson" ).validate();
    validator2 = $( "#frmNewClientPerson" ).validate();
    validator3 = $( "#frmNewClientCompany" ).validate();
    validator1.resetForm();
    validator2.resetForm();
    validator3.resetForm();
    $('.frmNewClientStep2').hide();
    $('.frmNewClientStep1').show();
    $('.nextFrmNewClient').show();
    $('.sendFrmNewClient').hide();
    $('#modalNewClient .modal-footer').show();
  }
  /* **********************************************
  ** New client DASHBOARD
  ************************************************/

  var form = '';
  var url = '';

  function formPerson(){
    $('.lblClientType').append('- Person');
    $('#frmNewClientPerson').show();
    $('#frmNewClientOnePerson').hide();
    $('#frmNewClientCompany').hide();
    $('#modalNewClient .modal-footer').hide(200);
    form = $('#frmNewClientPerson');
    url =form.attr('action');
  }

  function formOnePersonCompany(){
    //alert('One person company');
    $('.lblClientType').append('- One Person Company');
    $('#frmNewClientOnePerson').show();
    $('#frmNewClientPerson').hide();
    $('#frmNewClientCompany').hide();
    $('#modalNewClient .modal-footer').hide(200);
    form = $('#frmNewClientPerson');
    url =form.attr('action');
  }

  function formCompany(){
    $('.lblClientType').append('- Company');
    $('#frmNewClientOnePerson').hide();
    $('#frmNewClientPerson').hide();
    $('#frmNewClientCompany').show();
    $('#modalNewClient .modal-footer').hide(200);
    form = $('#frmNewClientPerson');
    url =form.attr('action');
  }

  $('.nextFrmNewClient').click(function() {
    ut = $('#cboClientType').val();
    ur = $('#cboUserLevel').val();
    $('.hdfr').val(ur);
    $('.hdfut').val(ut);
    console.log(ut+' // '+ur);
    switch (ut) {
      case '2':
        formOnePersonCompany();
        break;
      case '3':
        formCompany();
        break;
      default:
        formPerson();
    }

    formid = $('#modalNewClient form').attr('name');

  });

  $('#frmNewClientPerson').validate({
      rules: {
          username: {
              minlength: 3,
              required: true,
              remote: {
                url: checkUsernameUrl,
                type: "post",
                data: {
                        username: function() {
                          return $( "#usernameClientPerson" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          name: {
              minlength: 3,
              maxlength: 45,
              required: true
          },
          email: {
              email: true,
              required: true,
              remote: {
                url: checkEmailUrl,
                type: "post",
                data: {
                        email: function() {
                          return $( "#emailClientPerson" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          password: {
            required: true
          },
          street: {
              required: true
          },
          number: {
              required: true
          },
          postCode: {
              required: true
          },
          city: {
              required: true
          },
          country: {
              required: true
          },
          peselNumber: {
              required: true,
              number: true
          },
          idNumber: {
              required: true,
              number: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function (form) {
             $.ajax({
               url: url,
               type: 'POST',
               data: $(form).serialize(),
             })
             .done(function(data) {
               console.info(data);
               if (data != false) {
                toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
                $('#modalNewClient').modal('hide');
                cargaGridUsers();
               }else{
                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
               }

             })
             .fail(function() {
               console.log("error");
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             })
             .always(function() {
               console.log("complete");
             });


      return false; // required to block normal submit since you used ajax
     }
  });

  $('#frmNewClientOnePerson').validate({
      rules: {
          username: {
              minlength: 3,
              required: true,
              remote: {
                url: checkUsernameUrl,
                type: "post",
                data: {
                        username: function() {
                          return $( "#usernameClientOnePerson" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          name: {
              minlength: 3,
              maxlength: 45,
              required: true
          },
          email: {
              email: true,
              required: true,
              remote: {
                url: checkEmailUrl,
                type: "post",
                data: {
                        email: function() {
                          return $( "#emailClientOnePerson" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          password: {
            required: true
          },
          street: {
              required: true
          },
          number: {
              required: true
          },
          postCode: {
              required: true
          },
          city: {
              required: true
          },
          country: {
              required: true
          },
          peselNumber: {
              required: true,
              number: true
          },
          nipNumber: {
              required: true,
              number: true
          },
          regonNumber: {
              required: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function (form) {
             $.ajax({
               url: url,
               type: 'POST',
               data: $(form).serialize(),
             })
             .done(function(data) {
               console.info(data);
               if (data != false) {
                toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
                $('#modalNewClient').modal('hide');
                cargaGridUsers();
               }else{
                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
               }

             })
             .fail(function() {
               console.log("error");
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             })
             .always(function() {
               console.log("complete");
             });


      return false; // required to block normal submit since you used ajax
     }
  });

  $('#frmNewClientCompany').validate({
      rules: {
          username: {
              minlength: 3,
              required: true,
              remote: {
                url: checkUsernameUrl,
                type: "post",
                data: {
                        username: function() {
                          return $( "#usernameClientCompany" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          email: {
              email: true,
              required: true,
              remote: {
                url: checkEmailUrl,
                type: "post",
                data: {
                        email: function() {
                          return $( "#emailClientCompany" ).val();
                        },
                        _token: function(){
                          return token;
                        }
                      }
                    }
          },
          password: {
            required: true
          },
          street: {
              required: true
          },
          number: {
              required: true
          },
          postCode: {
              required: true
          },
          city: {
              required: true
          },
          country: {
              required: true
          },
          courtNumber: {
              required: true,
              number: true
          },
          courtPlace: {
              required: true,
          },
          krsNumber: {
              required: true,
              number: true
          },
          nipNumber: {
              required: true,
              number: true
          },
          regonNumber: {
              required: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function (form) {
             $.ajax({
               url: url,
               type: 'POST',
               data: $(form).serialize(),
             })
             .done(function(data) {
               console.info(data);
               if (data != false) {
                toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
                $('#modalNewClient').modal('hide');
                cargaGridUsers();
               }else{
                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
               }

             })
             .fail(function() {
               console.log("error");
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             })
             .always(function() {
               console.log("complete");
             });


      return false; // required to block normal submit since you used ajax
     }
  });

  /* **********************************************
  ** Delete client
  ************************************************/
  idClient = 0;
  $(document).on('click', '.btnDelClient', function() {
     idClient = $(this).attr('id');
    $('#modalDeleteClient').modal('show');
  });

  $(document).on('click', '.btnConfirmDelClient', function() {
    cad = 'id='+idClient+'&_token='+token;
    $.ajax({
      url: deleteUserUrl,
      type: 'POST',
      data: cad
    })
    .done(function(data) {
      if (data) {
        toastr["success"]("{{ trans('messages.clienDeleted_lbl') }}");
        $('#modalDeleteClient').modal('hide');
        cargaGridUsers();
      }else{
        toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


  /* **********************************************
  ** Edit client
  ************************************************/
idEditCliet = 0;
idRoleClient = 0;
idTypeClient = 0;

$(document).on('click', '.editClient', function() {
  showLoader();
  idEditCliet = $(this).attr('id');
  idRoleClient = $(this).attr('level');
  idTypeClient = $(this).attr('dir');
  $('#modalUpdateClient #cboUserLevel').val(idRoleClient);
  $('#modalUpdateClient #cboClientType').val(idTypeClient);
  $('#modalUpdateClient .id').val(idEditCliet);
  resetModalUpdateClient();
  //$('#modalUpdateClient').modal('show');
  cad = 'id='+idEditCliet+'&_token='+token;
  $.ajax({
    url: getDataClient,
    type: 'POST',
    data: cad
  })
  .done(function(data) {
    //console.log(data);
    //console.log(data.client.user_address[0].apartmentNumber);
    $('#modalUpdateClient .username').val(data.client.username);
    $('#modalUpdateClient .name').val(data.client.name);
    $('#modalUpdateClient .birthday').val(data.client.birthday);
    $('#modalUpdateClient .companyName').val(data.client.companyName);
    $('#modalUpdateClient .courtNumber').val(data.client.courtNumber);
    $('#modalUpdateClient .courtPlace').val(data.client.courtPlace);
    $('#modalUpdateClient .email').val(data.client.email);
    $('#modalUpdateClient .idNumber').val(data.client.idNumber);
    $('#modalUpdateClient .krsNumber').val(data.client.krsNumber);
    $('#modalUpdateClient .lastName').val(data.client.lastName);
    $('#modalUpdateClient .nipNumber').val(data.client.nipNumber);
    $('#modalUpdateClient .peselNumber').val(data.client.peselNumber);
    $('#modalUpdateClient .phone').val(data.client.phone);
    $('#modalUpdateClient .regonNumber').val(data.client.regonNumber);

    if (data.client.user_address.length > 0) {
      $('#modalUpdateClient .apartmentNumber').val(data.client.user_address[0].apartmentNumber);
      $('#modalUpdateClient .city').val(data.client.user_address[0].city);
      $('#modalUpdateClient .country').val(data.client.user_address[0].country_id);
      $('#modalUpdateClient .number').val(data.client.user_address[0].number);
      $('#modalUpdateClient .number').val(data.client.user_address[0].number);
      $('#modalUpdateClient .postCode').val(data.client.user_address[0].postCode);
      $('#modalUpdateClient .street').val(data.client.user_address[0].street);
    }

    if (data.client.user_legal.length > 0) {
      $('#modalUpdateClient .legalIdP1').val(data.client.user_legal[0].id);
      $('#modalUpdateClient .legalNameP1').val(data.client.user_legal[0].name);
      $('#modalUpdateClient .legalLastNameP1').val(data.client.user_legal[0].lastName);
      $('#modalUpdateClient .legalFunctionP1').val(data.client.user_legal[0].function);
      $('#modalUpdateClient .legalPhoneP1').val(data.client.user_legal[0].phone);
      $('#modalUpdateClient .legalEmailP1').val(data.client.user_legal[0].email);

      $('#modalUpdateClient .legalIdP2').val(data.client.user_legal[1].id);
      $('#modalUpdateClient .legalNameP2').val(data.client.user_legal[1].name);
      $('#modalUpdateClient .legalLastNameP2').val(data.client.user_legal[1].lastName);
      $('#modalUpdateClient .legalFunctionP2').val(data.client.user_legal[1].function);
      $('#modalUpdateClient .legalPhoneP2').val(data.client.user_legal[1].phone);
      $('#modalUpdateClient .legalEmailP2').val(data.client.user_legal[1].email);
    }

    //$('#modalUpdateClient .').val(data.client.user_address[0].);


    hideLoader();
    $('#modalUpdateClient').modal('show');
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

});


$('.closeFrmUpdateClient').click(function() {
  setTimeout(function(){
    resetModalUpdateClient();
  },500);
});

function resetModalUpdateClient(){
  $('#modalUpdateClient .lblClientType').empty();
  $('.frmUpdateClient')[0].reset();
  validator1 = $( "#frmUpdateClientOnePerson" ).validate();
  validator2 = $( "#frmUpdateClientPerson" ).validate();
  validator3 = $( "#frmUpdateClientCompany" ).validate();
  validator1.resetForm();
  validator2.resetForm();
  validator3.resetForm();
  $('.frmUpdateClientStep2').hide();
  $('.frmUpdateClientStep1').show();
  $('.nextFrmUpdateClient').show();
  $('.sendFrmUpdateClient').hide();
  $('#modalUpdateClient .modal-footer').show();
}

var form = '';
var url = '';

function formUpdatePerson(){
  $('#modalUpdateClient .lblClientType').append('- Person');
  $('#modalUpdateClient #frmUpdateClientPerson').show();
  $('#modalUpdateClient #frmUpdateClientOnePerson').hide();
  $('#modalUpdateClient #frmUpdateClientCompany').hide();
  $('#modalUpdateClient .modal-footer').hide(200);
  form = $('#frmUpdateClientPerson');
  url =form.attr('action');
}

function formUpdateOnePersonCompany(){
  //alert('One person company');
  $('#modalUpdateClient .lblClientType').append('- One Person Company');
  $('#modalUpdateClient #frmUpdateClientOnePerson').show();
  $('#modalUpdateClient #frmUpdateClientPerson').hide();
  $('#modalUpdateClient #frmUpdateClientCompany').hide();
  $('#modalUpdateClient .modal-footer').hide(200);
  form = $('#frmUpdateClientPerson');
  url =form.attr('action');
}

function formUpdateCompany(){
  $('#modalUpdateClient .lblClientType').append('- Company');
  $('#modalUpdateClient #frmUpdateClientOnePerson').hide();
  $('#modalUpdateClient #frmUpdateClientPerson').hide();
  $('#modalUpdateClient #frmUpdateClientCompany').show();
  $('#modalUpdateClient .modal-footer').hide(200);
  form = $('#frmUpdateClientPerson');
  url =form.attr('action');
}

$('.nextFrmUpdateClient').click(function() {
  $('#modalUpdateClient .frmUpdateClientStep1').hide(200);
  $('#modalUpdateClient .frmUpdateClientStep2').show(200);
  $('#modalUpdateClient .nextFrmUpdateClient').hide(200);
  $('#modalUpdateClient .sendFrmUpdateClient').show(200);
  ut = $('#modalUpdateClient #cboClientType').val();
  ur = $('#modalUpdateClient #cboUserLevel').val();
  $('.hdfr').val(ur);
  $('.hdfut').val(ut);
  console.log(ut+' // '+ur);
  switch (ut) {
    case '2':
      formUpdateOnePersonCompany();
      break;
    case '3':
      formUpdateCompany();
      break;
    default:
      formUpdatePerson();
  }

  formid = $('#modalUpdateClient form').attr('name');

});

$('#frmUpdateClientPerson').validate({
    rules: {
        name: {
            minlength: 3,
            maxlength: 45,
            required: true
        },
        street: {
            required: true
        },
        number: {
            required: true
        },
        postCode: {
            required: true
        },
        city: {
            required: true
        },
        country: {
            required: true
        },
        peselNumber: {
            required: true,
            number: true
        },
        idNumber: {
            required: true,
            number: true
        }
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
           $.ajax({
             url: updateClient,
             type: 'POST',
             data: $(form).serialize(),
           })
           .done(function(data) {
             console.info(data);
             if (data != false) {
              toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
              $('#modalUpdateClient').modal('hide');
             }else{
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             }

           })
           .fail(function() {
             console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             console.log("complete");
           });


    return false; // required to block normal submit since you used ajax
   }
});

$('#frmUpdateClientOnePerson').validate({
    rules: {
        name: {
            minlength: 3,
            maxlength: 45,
            required: true
        },
        password: {
          required: true
        },
        companyName: {
            required: true
        },
        street: {
            required: true
        },
        /*number: {
            required: true
        },*/
        postCode: {
            required: true
        },
        city: {
            required: true
        },
        country: {
            required: true
        },
        peselNumber: {
            required: true,
            number: true
        },
        nipNumber: {
            required: true,
            number: true
        },
        regonNumber: {
            required: true
        },
        phone: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
           $.ajax({
             url: updateClient,
             type: 'POST',
             data: $(form).serialize(),
           })
           .done(function(data) {
             console.info(data);
             if (data != false) {
              toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
              $('#modalUpdateClient').modal('hide');
             }else{
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             }

           })
           .fail(function() {
             console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             console.log("complete");
           });


    return false; // required to block normal submit since you used ajax
   }
});

$('#frmUpdateClientCompany').validate({
    rules: {
        street: {
            required: true
        },
        number: {
            required: true
        },
        postCode: {
            required: true
        },
        city: {
            required: true
        },
        country: {
            required: true
        },
        courtNumber: {
            required: true,
            number: true
        },
        courtPlace: {
            required: true,
        },
        krsNumber: {
            required: true,
            number: true
        },
        nipNumber: {
            required: true,
            number: true
        },
        regonNumber: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
           $.ajax({
             url: updateClient,
             type: 'POST',
             data: $(form).serialize(),
           })
           .done(function(data) {
             console.info(data);
             if (data != false) {
              toastr["success"]("{{ trans('messages.clientUpdated_lbl') }}");
              $('#modalUpdateClient').modal('hide');
             }else{
               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
             }

           })
           .fail(function() {
             console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             console.log("complete");
           });


    return false; // required to block normal submit since you used ajax
   }
});

/* ***********************************  WAREHOUSE  ****************************************************************/

$(document).on('click', '.btnNewWarehouse', function() {
  $('#modalWarehouse').modal('show');
  resetModalWarehouse();
});

/* **********************************************
  ** Validacion New Building
  ************************************************/
  $('#formNewBuilding').validate({
      rules: {
          name: {
              minlength: 3,
              maxlength: 45,
              required: true
          },
          country: {
            required: true
          },
          address: {
            minlength: 3,
            maxlength: 255,
            required: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function (form) {
             $.ajax({
               url: '{{ route('newBuilding') }}',
               type: 'POST',
               data: $(form).serialize(),
             })
             .done(function(data) {
               console.info(data);
               if (data != false) {
                toastr["success"]("{{ trans('warehouse.buildRegisterOk_lbl') }}");
                $('#modalWarehouse').modal('hide');
                cargaGridWarehouse();
               }else{
                 toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
               }

             })
             .fail(function() {
               console.log("error");
               toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
             })
             .always(function() {
               console.log("complete");
             });


      return false; // required to block normal submit since you used ajax
     }
  });

  /* **********************************************
  ** Delete Building
  ************************************************/
  idBuilding = 0;
  $(document).on('click', '.btnDelBuild', function() {
    idBuilding = $(this).attr('id');
    $('#modalDeleteBuilding').modal('show');
  });

  $(document).on('click', '.btnConfirmDelBuilding', function() {
    cad = 'id='+idBuilding+'&_token='+token;
    $.ajax({
      url: '{{ route('deleteBuilding') }}',
      type: 'POST',
      data: cad
    })
    .done(function(data) {
      if (data) {
        toastr["success"]("{{ trans('warehouse.buildDeleted_lbl') }}");
        $('#modalDeleteBuilding').modal('hide');
        cargaGridWarehouse();
      }else{
        toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  /* **********************************************
  ** Edit Building
  ************************************************/

function resetModalWarehouse(){
  $('#formNewBuilding')[0].reset();
}

idEditBuilding = 0;

$(document).on('click', '.editBuild', function() {
  showLoader();
  idEditBuilding = $(this).attr('id');
  $('#modalWarehouseUpdate #id').val(idEditBuilding);
  resetModalWarehouse();
  cad = 'id='+idEditBuilding+'&_token='+token;
  $.ajax({
    url: '{{ route('getDataBuilding') }}',
    type: 'POST',
    data: cad
  })
  .done(function(data) {
    //console.log(data);
    $('#formUpdateBuilding #name').val(data.building.name);
    $('#formUpdateBuilding #country').val(data.building.country_id);
    $('#formUpdateBuilding #address').val(data.building.address);
    hideLoader();
    $('#modalWarehouseUpdate').modal('show');
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

});

$('#formUpdateBuilding').validate({
      rules: {
          name: {
              minlength: 3,
              maxlength: 45,
              required: true
          },
          country: {
            required: true
          },
          address: {
            minlength: 3,
            maxlength: 255,
            required: true
          }
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function (form) {
             $.ajax({
               url: '{{ route('updateBuilding') }}',
               type: 'POST',
               data: $(form).serialize(),
             })
             .done(function(data) {
               console.info(data);
               if (data != false) {
                toastr["success"]("{{ trans('warehouse.buildRegisterOk_lbl') }}");
                $('#modalWarehouseUpdate').modal('hide');
                cargaGridWarehouse();
               }else{
                 toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
               }

             })
             .fail(function() {
               console.log("error");
               toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
             })
             .always(function() {
               console.log("complete");
             });


      return false; // required to block normal submit since you used ajax
     }
  });

console.info('PW is ready!');

});
