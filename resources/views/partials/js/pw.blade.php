$(document).ready(function() {

	var vatTags = '{{env("VAT")}}';
	vatTags = Number(vatTags) + 1;

	  // NOTIFICACIONES ********************************************************

	  var num = 0;
	  var globalInsurance = 0;
	  var globalVat = 0;

	  if (localStorage.numNotifications) {
			num = localStorage.numNotifications;
		}else{
			localStorage.setItem('numNotifications', num);
		}

	  function contadorSolicitudes(){

	    cad = '_token='+token;

			$.ajax({
				type: "post",
				dataType: "json",
				url: '{{ route('getNotificaciones') }}',
				data: cad,
				success: function( data ) {

					if(data.success == true)
					{
						if (data.count > num) {
							$('#soundNotif')[0].play();
							localStorage.setItem('numNotifications', data.count);
						}
						if(data.count < num){
							localStorage.setItem('numNotifications', data.count);
						}
						$('#badge-notif').empty();
						$('#badge-notif').text(data.count);

						num = data.count;

	          num2 = 0;
	          if(num > 0) {
	            $('.notifDropDown').empty();
	            $('.notifDropDown').append("<li class=\"dropdown-header\">{{ trans('nav.notification_title') }}</li>");
	            $.each(data.notif,function(index, not) {
	            	
	            	var icon = "{{ asset('assets/img/cart.jpg')}}";
	            
	              $('.notifDropDown').append(
											'<li>'+
												'<a class="alert alert-callout alert-info orderItem" ' +
													' dir="'+not.id+'" ' +
													' href=\"{{ route('cart','') }}/'+not.id+' \">' +
													' <img class="pull-right img-circle dropdown-avatar" ' +
																' src="'+icon+'" alt="" /> '+
																'<strong>'+not.client.name+'</strong><br/>'+
																'<small>{{ trans('nav.newOrderToGo') }}</small></a></li>');
	              if(num2 >= 3){
	                return false
	              }else{
	                num2++;
	              }
	            });
	            $('.notifDropDown').append("<li class=\"dropdown-header\">{{ trans('nav.options') }}</li>");
	            $('.notifDropDown').append("<li><a href=\"{{ route('cart',0) }}\">{{ trans('nav.view_orders') }}<span class=\"pull-right\"><i class=\"fa fa-arrow-right\"></i></span></a></li>");
	          }else{
	            $('.notifDropDown').append("<li class=\"dropdown-header\">{{ trans('nav.no_notifications') }}</li>");
	          }
					}
				}
			});
			//return boo;
	    //$('#soundNotif')[0].play();
		}
		
		function contadorNotificacionesAdmin(){

	    	cad = '_token='+token;
	    	

			$.ajax({
				type: "post",
				dataType: "json",
				url: '{{ route('getNotificacionesAdmin') }}',
				data: cad,
				success: function( data ) {

					if(data.success == true)
					{
						if (data.count > num) {
							$('#soundNotif')[0].play();
							localStorage.setItem('numNotifications', data.count);
						}
						if(data.count < num){
							localStorage.setItem('numNotifications', data.count);
						}
						$('#badge-admin').empty();
						$('#badge-admin').text(data.count);

						num = data.count;

	          num2 = 0;
	          
	          if(num > 0) {
	            $('.notifDropDownAdmin').empty();
	            $('.notifDropDownAdmin').append("<li class=\"dropdown-header\">{{ trans('nav.notification_title') }}</li>");
	            $.each(data.notif,function(index, not) {
	            	
	            	
	            	var fullname = not.user.name + ' ' + not.user.lastName + ' - ' + not.user.companyName;  
	            	
	            	var message = not.notf_type + ', BOX: ' + not.storage.alias; 
	            	
	            	if(not.notf_type == "RENT BOX"){
	            		icon = "{{ asset('assets/img/new-box-icon.png')}}";
	            	}
	            	else{
	            		icon = "{{ asset('assets/img/deleted-box-icon.png')}}";
	            	}
	            	
	              $('.notifDropDownAdmin').append(
										'<li>'+
											'<a class="alert alert-callout alert-info destroy-notif-user-rent" '+
											' dir="'+not.id+'" href=\"{{ route('notificaciones.destroy_admin','') }}/'+not.id+' \">'+
												' <img class="pull-right img-circle dropdown-avatar" '+
												' src="'+icon+'" alt="" />'+
													' <strong>'+fullname+'</strong>'+
													'<br/><small>'+message+'</small>'+
													'</a></li>');
	              
	              //alert(num2);
	              
	              if(num2 >= 3){
	                return false
	              }else{
	                num2++;
	              }
	            });
	            $('.notifDropDownAdmin').append("<li class=\"dropdown-header\">{{ trans('nav.options') }}</li>");
	            $('.notifDropDownAdmin').append("<li><a href=\"{{ route('cart',0) }}\">{{ trans('nav.view_orders') }}<span class=\"pull-right\"><i class=\"fa fa-arrow-right\"></i></span></a></li>");
	          }else{
	            $('.notifDropDownAdmin').append("<li class=\"dropdown-header\">{{ trans('nav.no_notifications') }}</li>");
	          }
					}
				}
			});
		}
		
	  @if (Auth::guest())
	  @else
	    @if (Auth::user()->Role_id == 1)
	      contadorSolicitudes();
	      contadorNotificacionesAdmin();
	    @endif
	  @endif

	  //setInterval(function(){ contadorSolicitudes(); }, 1000);
	  /*************************************************************************/


	  {{ isset($idLevel) ? $idLevel = $idLevel : $idLevel = 0}}

	   $('.folder').hover(function(){
	    $(this).parent('li').find('ul:first').show();
	   });


	  $('.pageLoader').hide();

	  toastr.options = {
	    "closeButton": true,
	    "debug": false,
	    "newestOnTop": false,
	    "progressBar": true,
	    "positionClass": "toast-top-right",
	    "preventDuplicates": false,
	    "onclick": null,
	    "showDuration": "5000",
	    "hideDuration": "1000",
	    "timeOut": "5000",
	    "extendedTimeOut": "5000",
	    "showEasing": "swing",
	    "hideEasing": "linear",
	    "showMethod": "fadeIn",
	    "hideMethod": "fadeOut"
	  }


	  /* **********************************************
	  ** Validacion Registro
	  ************************************************/
	  $('#forRegister').validate({
	      rules: {
	      		/*
	          username: {
	              minlength: 3,
	              required: true,
	              remote: {
	                url: checkUsernameUrl,
	                type: "post",
	                data: {
	                        username: function() {
	                          return $( "#username" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	          },
	          */

	          email: {
	              email: true,
	              required: true
	              /*,

	              remote: {
	                  url: checkEmailUrl,
	                  type: "post",
	                  data: {
	                      email: function() {
	                          return $( "#email" ).val();
	                      },
	                      _token: function(){
	                          return token;
	                      }
	                  }
	              }*/
	          },

	          password: {
	            required: true
	          },
	          password_confirmation: {
	            required: true,
	            equalTo: "#password"
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      }
	  });


	  /* **********************************************
	  ** New client AUTH
	  ************************************************/

	  $('.btnNewClient').click(function() {
	    resetModalNewClient();
	    pwd = genPwd(8);
	    console.info(pwd);
	    $('.txtPwd').val('');
	    $('.txtPwd').val(pwd);
	    $('.g_password').hide();

	    $('#modalNewClient').modal('show');
	  });

	  $('.closeFrmNewClient').click(function() {
	    setTimeout(function(){
	      resetModalNewClient();
	    },500);
	  });

	  $('.nextFrmNewClient').click(function() {
	    $('.frmNewClientStep1').hide(200);
	    $('.frmNewClientStep2').show(200);
	    $('.nextFrmNewClient').hide(200);
	    $('.sendFrmNewClient').show(200);
	  });

	  function resetModalNewClient(){
	    console.info('reset');
	    $('.lblClientType').empty();
	    $('.frmNewClient')[0].reset();
	    $('#frmNewClientPerson')[0].reset();
	    $('#frmNewClientOnePerson')[0].reset();
	    $('#frmNewClientCompany')[0].reset();
	    $('#frmNewClientPerson .chkOportunity').prop( "checked", false );
	    $('#frmNewClientOnePerson .chkOportunity').prop( "checked", false );
	    $('#frmNewClientCompany .chkOportunity').prop( "checked", false );
	    validator1 = $( "#frmNewClientOnePerson" ).validate();
	    validator2 = $( "#frmNewClientPerson" ).validate();
	    validator3 = $( "#frmNewClientCompany" ).validate();
	    validator1.resetForm();
	    validator2.resetForm();
	    validator3.resetForm();
	    $('.frmNewClientStep2').hide();
	    $('.frmNewClientStep1').show();
	    $('.nextFrmNewClient').show();
	    $('.sendFrmNewClient').hide();
	    $('#modalNewClient .modal-footer').show();
	  }
	  /* **********************************************
	  ** New client DASHBOARD
	  ************************************************/

	  var form = '';
	  var url = '';

	  function formPerson(){
	    $('.lblClientType').append('- Person');
	    $('#frmNewClientPerson').show();
	    $('#frmNewClientOnePerson').hide();
	    $('#frmNewClientCompany').hide();
	    $('#modalNewClient .modal-footer').hide(200);
	    form = $('#frmNewClientPerson');
	    url =form.attr('action');
	  }

	  function formOnePersonCompany(){
	    //alert('One person company');
	    $('.lblClientType').append('- One Person Company');
	    $('#frmNewClientOnePerson').show();
	    $('#frmNewClientPerson').hide();
	    $('#frmNewClientCompany').hide();
	    $('#modalNewClient .modal-footer').hide(200);
	    form = $('#frmNewClientPerson');
	    url =form.attr('action');
	  }

	  function formCompany(){
	    $('.lblClientType').append('- Company');
	    $('#frmNewClientOnePerson').hide();
	    $('#frmNewClientPerson').hide();
	    $('#frmNewClientCompany').show();
	    $('#modalNewClient .modal-footer').hide(200);
	    form = $('#frmNewClientPerson');
	    url =form.attr('action');
	  }

	  $('.nextFrmNewClient').click(function() {
	    ut = $('#cboClientType').val();
	    ur = $('#cboUserLevel').val();
	    $('.hdfr').val(ur);
	    $('.hdfut').val(ut);
	    console.log(ut+' // '+ur);
	    switch (ut) {
	      case '2':
	        formOnePersonCompany();
	        break;
	      case '3':
	        formCompany();
	        break;
	      default:
	        formPerson();
	    }

	    formid = $('#modalNewClient form').attr('name');

	  });

	  function genPwd(len){
	    var chars = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
	    var pwd = "";
	    for (i=0; i<len; i++) pwd += chars.charAt(Math.floor(Math.random()*chars.length));
	    return pwd;
	  }

	  $('#frmNewClientPerson').validate({
	  	 ignore: ':hidden',
	      rules: {
	          /*username: {
	              minlength: 3,
	              required: true,
	              remote: {
	                url: checkUsernameUrl,
	                type: "post",
	                data: {
	                        username: function() {
	                          return $( "#usernameClientPerson" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	          },*/
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          /*
	          email: {
	              email: true,
	              required: true,
	              remote: {
	                url: checkEmailUrl,
	                type: "post",
	                data: {
	                        email: function() {
	                          return $( "#emailClientPerson" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      },
	                dataType: 'json'
	          	}
	          },
	          */

	          email: {
	          		email: true,
	              	required: true,
	              	/*
			        remote: {
		                message: 'Ingresa otro email',
		                url: "{{route('users.validate.username')}}",
		                data: {
		                     username: function() {
	                          return $( "#emailClientPerson" ).val();
	                        },
		                },
			            type: 'POST'
			        }*/
		        },
	          password: {
	            required: true
	          },
	          street: {
	              required: true
	          },
	          number: {
	              required: true
	          },
	          postCode: {
	              required: true
	          },
	          city: {
	              required: true
	          },
	          country: {
	              required: true
	          },
	          peselNumber: {
	              required: true,
	              //number: true
	          },
	          idNumber: {
	              required: true,
	              //number: true
	          }
          },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	      
	      		$(".sendNewClientBtn").prop('disabled', true);
	      		
	             $.ajax({
	               url: url,
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               //console.info(data);

	               if (data.success == "false") {
	               		toastr["error"]("{{ trans('messages.clientRegisterError_lbl') }}");
	               }else{
		               toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
		                $('#modalNewClient').modal('hide');
		                //cargaGridUsers();
		                $(".btnSearchClients").trigger("click");
		                $(".sendNewClientBtn").prop('disabled', false);
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  $('#frmNewClientOnePerson').validate({
	      rules: {
	          /*username: {
	              minlength: 3,
	              required: true,
	              remote: {
	                url: checkUsernameUrl,
	                type: "post",
	                data: {
	                        username: function() {
	                          return $( "#usernameClientOnePerson" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	          },*/
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          email: {
	              email: true,
	              required: true
	              /*,
	              remote: {
	                url: checkEmailUrl,
	                type: "post",
	                data: {
	                        email: function() {
	                          return $( "#emailClientOnePerson" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	                    */
	          },
	          password: {
	            required: true
	          },
	          street: {
	              required: true
	          },
	          number: {
	              required: true
	          },
	          postCode: {
	              required: true
	          },
	          city: {
	              required: true
	          },
	          country: {
	              required: true
	          },
	          peselNumber: {
	              required: true,
	              //number: true
	          },
	          nipNumber: {
	              required: true,
	              //number: true
	          },
	          regonNumber: {
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	      	
	      	$(".sendNewClientBtn").prop('disabled', true);
	      	
	             $.ajax({
	               url: url,
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
	                $('#modalNewClient').modal('hide');
	                //cargaGridUsers();
	                $(".btnSearchClients").trigger("click");
	                
	                $(".sendNewClientBtn").prop('disabled', false);
	                
	               }else{
	                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  $('#frmNewClientCompany').validate({
	      rules: {
	          /*username: {
	              minlength: 3,
	              required: true,
	              remote: {
	                url: checkUsernameUrl,
	                type: "post",
	                data: {
	                        username: function() {
	                          return $( "#usernameClientCompany" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	          },*/
	          email: {
	              email: true,
	              required: true
	              /*,
	              remote: {
	                url: checkEmailUrl,
	                type: "post",
	                data: {
	                        email: function() {
	                          return $( "#emailClientCompany" ).val();
	                        },
	                        _token: function(){
	                          return token;
	                        }
	                      }
	                    }
	                    */
	          },
	          password: {
	            required: true
	          },
	          street: {
	              required: true
	          },
	          number: {
	              required: true
	          },
	          postCode: {
	              required: true
	          },
	          city: {
	              required: true
	          },
	          country: {
	              required: true
	          },
	          courtNumber: {
	              required: true,
	              //number: true
	          },
	          courtPlace: {
	              required: true,
	          },
	          krsNumber: {
	              required: true,
	              //number: true
	          },
	          nipNumber: {
	              required: true,
	              //number: true
	          },
	          regonNumber: {
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	      
	      	$(".sendNewClientBtn").prop('disabled', true);
	      	
	             $.ajax({
	               url: url,
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               //console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
	                $('#modalNewClient').modal('hide');
	                //cargaGridUsers();
	                $(".btnSearchClients").trigger("click");
	                
	                $(".sendNewClientBtn").prop('disabled', false);
	                
	               }else{
	                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  /* **********************************************
	  ** Delete client
	  ************************************************/
	  idClient = 0;
	  $(document).on('click', '.btnDelClient', function() {
	     idClient = $(this).attr('id');
	    $('#modalDeleteClient').modal('show');
	  });

	  $(document).on('click', '.btnConfirmDelClient', function() {
	    cad = 'id='+idClient+'&_token='+token;
	    $.ajax({
	      url: deleteUserUrl,
	      type: 'POST',
	      data: cad
	    })
	    .done(function(data) {
	      if (data) {
	        toastr["success"]("{{ trans('messages.clienDeleted_lbl') }}");
	        $('#modalDeleteClient').modal('hide');
	        cargaGridUsers();
	      }else{
	        toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });


	  /* **********************************************
	  ** Edit client
	  ************************************************/
	idEditCliet = 0;
	idRoleClient = 0;
	idTypeClient = 0;

	$(document).on('click', '.editClient', function() {
	  showLoader();
	  idEditCliet = $(this).attr('id');
	  idRoleClient = $(this).attr('level');
	  idTypeClient = $(this).attr('dir');
	  $('#modalUpdateClient #cboUserLevel').val(idRoleClient);
	  $('#modalUpdateClient #cboClientType').val(idTypeClient);
	  $('#modalUpdateClient #cboClientType').attr('disabled','disabled');
	  $('#modalUpdateClient #cboUserLevel').attr('disabled','disabled');
	  $('#modalUpdateClient .id').val(idEditCliet);
	  resetModalUpdateClient();
	  //$('#modalUpdateClient').modal('show');
	  cad = 'id='+idEditCliet+'&_token='+token;
	  $.ajax({
	    url: getDataClient,
	    type: 'POST',
	    data: cad
	  })
	  .done(function(data) {
	    //console.log(data);
	    //console.log(data.client.user_address[0].apartmentNumber);
	    $('#modalUpdateClient .username').val(data.client.username);
	    $('#modalUpdateClient .name').val(data.client.name);
	    $('#modalUpdateClient .birthday').val(data.client.birthday);
	    $('#modalUpdateClient .companyName').val(data.client.companyName);
	    $('#modalUpdateClient .courtNumber').val(data.client.courtNumber);
	    $('#modalUpdateClient .courtPlace').val(data.client.courtPlace);
	    $('#modalUpdateClient .email').val(data.client.email);
	    $('#modalUpdateClient .email2').val(data.client.email2);
	    $('#modalUpdateClient .idNumber').val(data.client.idNumber);
	    $('#modalUpdateClient .krsNumber').val(data.client.krsNumber);
	    $('#modalUpdateClient .lastName').val(data.client.lastName);
	    $('#modalUpdateClient .nipNumber').val(data.client.nipNumber);
	    $('#modalUpdateClient .peselNumber').val(data.client.peselNumber);
	    $('#modalUpdateClient .phone').val(data.client.phone);
	    $('#modalUpdateClient .phone2').val(data.client.phone2);
	    $('#modalUpdateClient .regonNumber').val(data.client.regonNumber);

	    if(data.client.flag_oportunity != 1){
	      $('#modalUpdateClient .chkOportunity').removeAttr('checked');
	    }else{
	      $('#modalUpdateClient .chkOportunity').prop('checked','true');
	    }

	    $('#modalUpdateClient #oportunity_reminder').val(data.client.oportunity_reminder);
	    $('#modalUpdateClient #notes').val(data.client.notes);
	    $('#modalUpdateClient #accountant_number').val(data.client.accountant_number);
	    $('#modalUpdateClient #accountant_code').val(data.client.accountant_code);


	    if (data.client.user_address.length > 0) {
	      $('#modalUpdateClient .apartmentNumber').val(data.client.user_address[0].apartmentNumber);
	      $('#modalUpdateClient .city').val(data.client.user_address[0].city);
	      $('#modalUpdateClient .country').val(data.client.user_address[0].country_id);
	      $('#modalUpdateClient .number').val(data.client.user_address[0].number);
	      $('#modalUpdateClient .number').val(data.client.user_address[0].number);
	      $('#modalUpdateClient .postCode').val(data.client.user_address[0].postCode);
	      $('#modalUpdateClient .street').val(data.client.user_address[0].street);
	    }

	    if (data.client.user_legal.length > 0) {
	      $('#modalUpdateClient .legalIdP1').val(data.client.user_legal[0].id);
	      $('#modalUpdateClient .legalNameP1').val(data.client.user_legal[0].name);
	      $('#modalUpdateClient .legalLastNameP1').val(data.client.user_legal[0].lastName);
	      $('#modalUpdateClient .legalFunctionP1').val(data.client.user_legal[0].function);
	      $('#modalUpdateClient .legalPhoneP1').val(data.client.user_legal[0].phone);
	      $('#modalUpdateClient .legalEmailP1').val(data.client.user_legal[0].email);

	      $('#modalUpdateClient .legalIdP2').val(data.client.user_legal[1].id);
	      $('#modalUpdateClient .legalNameP2').val(data.client.user_legal[1].name);
	      $('#modalUpdateClient .legalLastNameP2').val(data.client.user_legal[1].lastName);
	      $('#modalUpdateClient .legalFunctionP2').val(data.client.user_legal[1].function);
	      $('#modalUpdateClient .legalPhoneP2').val(data.client.user_legal[1].phone);
	      $('#modalUpdateClient .legalEmailP2').val(data.client.user_legal[1].email);
	    }

	    //$('#modalUpdateClient .').val(data.client.user_address[0].);


	    hideLoader();
	    $('#modalUpdateClient').modal('show');
	  })
	  .fail(function() {
	    console.log("error");
	  })
	  .always(function() {
	    console.log("complete");
	  });

	});


	$('.closeFrmUpdateClient').click(function() {
	  setTimeout(function(){
	    $('#modalUpdateClient #cboClientType').removeAttr('disabled');
	    $('#modalUpdateClient #cboUserLevel').removeAttr('disabled');
	    resetModalUpdateClient();
	  },500);
	});

	function resetModalUpdateClient(){
	  $('#modalUpdateClient .lblClientType').empty();
	  $('.frmUpdateClient')[0].reset();
	  validator1 = $( "#frmUpdateClientOnePerson" ).validate();
	  validator2 = $( "#frmUpdateClientPerson" ).validate();
	  validator3 = $( "#frmUpdateClientCompany" ).validate();
	  validator1.resetForm();
	  validator2.resetForm();
	  validator3.resetForm();
	  $('.frmUpdateClientStep2').hide();
	  $('.frmUpdateClientStep1').show();
	  $('.nextFrmUpdateClient').show();
	  $('.sendFrmUpdateClient').hide();
	  $('#modalUpdateClient .modal-footer').show();
	  $( "#frmUpdateClientOnePerson .chkOportunity" ).prop('checked','false');
	  $( "#frmUpdateClientPerson .chkOportunity" ).prop('checked','false');
	  $( "#frmUpdateClientCompany .chkOportunity" ).prop('checked','false');
	}

	var form = '';
	var url = '';

	function formUpdatePerson(){
	  $('#modalUpdateClient .lblClientType').append('- Person');
	  $('#modalUpdateClient #frmUpdateClientPerson').show();
	  $('#modalUpdateClient #frmUpdateClientOnePerson').hide();
	  $('#modalUpdateClient #frmUpdateClientCompany').hide();
	  $('#modalUpdateClient .modal-footer').hide(200);
	  form = $('#frmUpdateClientPerson');
	  url =form.attr('action');
	}

	function formUpdateOnePersonCompany(){
	  //alert('One person company');
	  $('#modalUpdateClient .lblClientType').append('- One Person Company');
	  $('#modalUpdateClient #frmUpdateClientOnePerson').show();
	  $('#modalUpdateClient #frmUpdateClientPerson').hide();
	  $('#modalUpdateClient #frmUpdateClientCompany').hide();
	  $('#modalUpdateClient .modal-footer').hide(200);
	  form = $('#frmUpdateClientPerson');
	  url =form.attr('action');
	}

	function formUpdateCompany(){
	  $('#modalUpdateClient .lblClientType').append('- Company');
	  $('#modalUpdateClient #frmUpdateClientOnePerson').hide();
	  $('#modalUpdateClient #frmUpdateClientPerson').hide();
	  $('#modalUpdateClient #frmUpdateClientCompany').show();
	  $('#modalUpdateClient .modal-footer').hide(200);
	  form = $('#frmUpdateClientPerson');
	  url =form.attr('action');
	}

	$('.nextFrmUpdateClient').click(function() {
	  $('#modalUpdateClient .frmUpdateClientStep1').hide(200);
	  $('#modalUpdateClient .frmUpdateClientStep2').show(200);
	  $('#modalUpdateClient .nextFrmUpdateClient').hide(200);
	  $('#modalUpdateClient .sendFrmUpdateClient').show(200);
	  ut = $('#modalUpdateClient #cboClientType').val();
	  ur = $('#modalUpdateClient #cboUserLevel').val();
	  $('.hdfr').val(ur);
	  $('.hdfut').val(ut);
	  console.log(ut+' // '+ur);
	  switch (ut) {
	    case '2':
	      formUpdateOnePersonCompany();
	      break;
	    case '3':
	      formUpdateCompany();
	      break;
	    default:
	      formUpdatePerson();
	  }

	  formid = $('#modalUpdateClient form').attr('name');

	});

	$('#frmUpdateClientPerson').validate({
	    rules: {
	        name: {
	            minlength: 3,
	            maxlength: 45,
	            required: true
	        },
	        street: {
	            required: true
	        },
	        number: {
	            required: true,
	            number: false
	        },
	        postCode: {
	            required: true
	        },
	        city: {
	            required: true
	        },
	        country: {
	            required: true
	        },
	        peselNumber: {
	            required: true,
	            //number: true
	        },
	        idNumber: {
	            required: true,
	            //number: true
	        }
	    },
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    submitHandler: function (form) {
	           $.ajax({
	             url: updateClient,
	             type: 'POST',
	             data: $(form).serialize(),
	           })
	           .done(function(data) {
	             //console.info(data);
	             if (data.success != false) {
	              toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
	              $('#modalUpdateClient').modal('hide');
	              //cargaGridUsers();
	             }else{
	               toastr["error"]("{{ trans('messages.clientRegisterErrorEmail_lbl') }}");
	             }

	           })
	           .fail(function() {
	             console.log("error");
	             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	           })
	           .always(function() {
	             console.log("complete");
	           });


	    return false; // required to block normal submit since you used ajax
	   }
	});

	$('#frmUpdateClientOnePerson').validate({
	    rules: {
	        name: {
	            minlength: 3,
	            maxlength: 45,
	            required: true
	        },
	        password: {
	          required: true
	        },
	        companyName: {
	            required: true
	        },
	        street: {
	            required: true
	        },
	        number: {
	            required: true,
	            number: false
	        },
	        postCode: {
	            required: true
	        },
	        city: {
	            required: true
	        },
	        country: {
	            required: true
	        },
	        peselNumber: {
	            required: true,
	            //number: true
	        },
	        nipNumber: {
	            required: true,
	            //number: true
	        },
	        regonNumber: {
	            required: true
	        },
	        phone: {
	            required: true
	        }
	    },
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    submitHandler: function (form) {
	           $.ajax({
	             url: updateClient,
	             type: 'POST',
	             data: $(form).serialize(),
	           })
	           .done(function(data) {
	             //console.info(data);
	             if (data.success != false) {
	              toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
	              $('#modalUpdateClient').modal('hide');
	              //cargaGridUsers();

	             }else{
	               toastr["error"]("{{ trans('messages.clientRegisterErrorEmail_lbl') }}");
	             }

	           })
	           .fail(function() {
	             console.log("error");
	             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	           })
	           .always(function() {
	             console.log("complete");
	           });


	    return false; // required to block normal submit since you used ajax
	   }
	});

	$('#frmUpdateClientCompany').validate({
	    rules: {
	        street: {
	            required: true
	        },
	        number: {
	            required: true,
	            number: false
	        },
	        postCode: {
	            required: true
	        },
	        city: {
	            required: true
	        },
	        country: {
	            required: true
	        },
	        courtNumber: {
	            required: true,
	            //number: true
	        },
	        courtPlace: {
	            required: true,
	        },
	        krsNumber: {
	            required: true,
	            //number: true
	        },
	        nipNumber: {
	            required: true,
	            //number: true
	        },
	        regonNumber: {
	            required: true
	        }
	    },
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    submitHandler: function (form) {
	           $.ajax({
	             url: updateClient,
	             type: 'POST',
	             data: $(form).serialize(),
	           })
	           .done(function(data) {
	             //console.info(data);
	             if (data.success != false) {
	              toastr["success"]("{{ trans('messages.clientUpdated_lbl') }}");
	              $('#modalUpdateClient').modal('hide');
	              cargaGridUsers();
	             }else{
	               toastr["error"]("{{ trans('messages.clientRegisterErrorEmail_lbl') }}");
	             }

	           })
	           .fail(function() {
	             console.log("error");
	             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
	           })
	           .always(function() {
	             console.log("complete");
	           });


	    return false; // required to block normal submit since you used ajax
	   }
	});

	/* ***********************************  WAREHOUSE  ****************************************************************/

	$(document).on('click', '.btnNewWarehouse', function() {
	  $('#modalWarehouse').modal('show');
	  resetModalWarehouse();
	});

	/* **********************************************
	  ** Validacion New Building
	  ************************************************/
	  $('#formNewBuilding').validate({
	      rules: {
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          country: {
	            required: true
	          },
	          address: {
	            minlength: 3,
	            maxlength: 255,
	            required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('newBuilding') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('warehouse.buildRegisterOk_lbl') }}");
	                $('#modalWarehouse').modal('hide');
	                cargaGridWarehouse();
	               }else{
	                 toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  /* **********************************************
	  ** Delete Building
	  ************************************************/
	  idBuilding = 0;
	  $(document).on('click', '.btnDelBuild', function() {
	    idBuilding = $(this).attr('id');
	    $('#modalDeleteBuilding').modal('show');
	  });

	  $(document).on('click', '.btnConfirmDelBuilding', function() {
	    cad = 'id='+idBuilding+'&_token='+token;
	    $.ajax({
	      url: '{{ route('deleteBuilding') }}',
	      type: 'POST',
	      data: cad
	    })
	    .done(function(data) {
	      if (data) {
	        toastr["success"]("{{ trans('warehouse.buildDeleted_lbl') }}");
	        $('#modalDeleteBuilding').modal('hide');
	        cargaGridWarehouse();
	      }else{
	        toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  /* **********************************************
	  ** Edit Building
	  ************************************************/

	function resetModalWarehouse(){
	  $('#formNewBuilding')[0].reset();
	}

	idEditBuilding = 0;

	$(document).on('click', '.editBuild', function() {
	  showLoader();
	  idEditBuilding = $(this).attr('id');
	  $('#modalWarehouseUpdate #id').val(idEditBuilding);
	  resetModalWarehouse();
	  cad = 'id='+idEditBuilding+'&_token='+token;
	  $.ajax({
	    url: '{{ route('getDataBuilding') }}',
	    type: 'POST',
	    data: cad
	  })
	  .done(function(data) {
	    //console.log(data);
	    $('#formUpdateBuilding #name').val(data.building.name);
	    $('#formUpdateBuilding #country').val(data.building.country_id);
	    $('#formUpdateBuilding #address').val(data.building.address);
	    hideLoader();
	    $('#modalWarehouseUpdate').modal('show');
	  })
	  .fail(function() {
	    console.log("error");
	  })
	  .always(function() {
	    console.log("complete");
	  });

	});

	$('#formUpdateBuilding').validate({
	      rules: {
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          country: {
	            required: true
	          },
	          address: {
	            minlength: 3,
	            maxlength: 255,
	            required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('updateBuilding') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('warehouse.buildRegisterOk_lbl') }}");
	                $('#modalWarehouseUpdate').modal('hide');
	                cargaGridWarehouse();
	               }else{
	                 toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('warehouse.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });


	  /* ***********************************  LEVELS  ****************************************************************/

	$(document).on('click', '.btnNewLevel', function() {
	  $('#modalNewLevel').modal('show');
	  resetModalLevels();
	});

	function resetModalLevels(){
	  $('#formNewLevel')[0].reset();
	  $('#idBuilding').val({{ $idLevel }});
	}

	/* **********************************************
	  ** Validacion New Level
	  ************************************************/
	  $('#formNewLevel').validate({
	      rules: {
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('newLevel') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('levels.levelRegisterOk_lbl') }}");
	                $('#modalNewLevel').modal('hide');
	                cargaGridLevels();
	               }else{
	                 toastr["error"]("{{ trans('levels.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('levels.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  /* **********************************************
	  ** Delete Level
	  ************************************************/
	  idLevel = 0;
	  $(document).on('click', '.btnDelLevel', function() {
	    idLevel = $(this).attr('id');
	    $('#modalDeleteLevel').modal('show');
	  });

	  $(document).on('click', '.btnConfirmDelLevel', function() {
	    cad = 'id='+idLevel+'&_token='+token;
	    $.ajax({
	      url: '{{ route('deleteLevel') }}',
	      type: 'POST',
	      data: cad
	    })
	    .done(function(data) {
	      if (data) {
	        toastr["success"]("{{ trans('levels.levelDeleted') }}");
	        $('#modalDeleteLevel').modal('hide');
	        cargaGridLevels();
	      }else{
	        toastr["error"]("{{ trans('levels.somethingWasWrong_lbl') }}");
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  /* **********************************************
	  ** Edit Building
	  ************************************************/

	idEditLevel = 0;

	$(document).on('click', '.editLevel', function() {
	  showLoader();
	  idEditLevel = $(this).attr('id');
	  $('#modalLevelUpdate #idLevel').val(idEditLevel);
	  resetModalLevels();
	  cad = 'id='+idEditLevel+'&_token='+token;
	  $.ajax({
	    url: '{{ route('getDataLevel') }}',
	    type: 'POST',
	    data: cad
	  })
	  .done(function(data) {
	    //console.log(data);
	    $('#formUpdateLevel #name').val(data.building.name);
	    hideLoader();
	    $('#modalLevelUpdate').modal('show');
	  })
	  .fail(function() {
	    console.log("error");
	  })
	  .always(function() {
	    console.log("complete");
	  });

	});

	$('#formUpdateLevel').validate({
	      rules: {
	          name: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('updateLevel') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('levels.levelRegisterOk_lbl') }}");
	                $('#modalLevelUpdate').modal('hide');
	                cargaGridLevels();
	               }else{
	                 toastr["error"]("{{ trans('levels.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('levels.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  /* ***********************************  STORAGES  ****************************************************************/

	$(document).on('click', '.btnNewStorage', function() {
	  $('#modalNewStorage').modal('show');
	  resetModalStorages();
	});

	function resetModalStorages(){
	  $('#formNewStorage')[0].reset();
	  $('#formNewStorage #idLevel').val({{ $idLevel }});
	}

	/* **********************************************
	  ** Validacion New storage
	  ************************************************/

	  $('#formNewStorage').validate({
	      rules: {
	          alias: {
	              minlength: 3,
	              maxlength: 45,
	              required: true
	          },
	          sqm: {
	              required: true,
	              number: true
	          },
	          price: {
	              required: true,
	              number: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('newStorage') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('storages.storagesRegisterOk_lbl') }}");
	                $('#modalNewLevel').modal('hide');
	                cargaGridStorages();
	               }else{
	                 toastr["error"]("{{ trans('storages.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('storages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });


	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  /* **********************************************
	  ** Delete Storage
	  ************************************************/

	  idStorage = 0;
	  $(document).on('click', '.delStorage', function() {
	    idStorage = $(this).attr('id');
	    $('#modalDeleteStorage').modal('show');
	  });

	  $(document).on('click', '.btnConfirmDelStorage', function() {
	    cad = 'id='+idStorage+'&_token='+token;
	    $.ajax({
	      url: '{{ route('deleteStorage') }}',
	      type: 'POST',
	      data: cad
	    })
	    .done(function(data) {
	      if (data) {
	        toastr["success"]("{{ trans('storages.levelDeleted') }}");
	        $('#modalDeleteStorage').modal('hide');
	        cargaGridStorages();
	      }else{
	        toastr["error"]("{{ trans('storages.somethingWasWrong_lbl') }}");
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  /* **********************************************
	  ** Edit Storage
	  ************************************************/

	idEditStorage = 0;

	$(document).on('click', '.editStorage', function() {
	  showLoader();
	  idEditStorage = $(this).attr('id');
	  $('#modalEditStorage #idLevel').val(idEditStorage);
	  resetModalStorages();
	  cad = 'id='+idEditStorage+'&_token='+token;
	  $.ajax({
	    url: '{{ route('getDataStorage') }}',
	    type: 'POST',
	    data: cad
	  })
	  .done(function(data) {
	    //console.log(data);
	    $('#formEditStorage #alias').val(data.storage.alias);
	    $('#formEditStorage #sqm').val(data.storage.sqm);
	    $('#formEditStorage #price').val(data.storage.price);
	    $('#formEditStorage #comments').val(data.storage.comments);
	    
	    if(data.storage.st_es_contenedor == 1){
	    	$('#formEditStorage #st_contenedor').attr("checked","checked");
	    }
	    else{
	    	$('#formEditStorage #st_contenedor').removeAttr("checked");
	    }

			if(data.storage.st_mostrar_app == 1){
	    	$('#formEditStorage #st_mostrar_app').attr("checked","checked");
	    }
	    else{
	    	$('#formEditStorage #st_mostrar_app').removeAttr("checked");
	    }
	    
	    
	    
	    hideLoader();
	    $('#modalEditStorage').modal('show');
	  })
	  .fail(function() {
	    console.log("error");
	  })
	  .always(function() {
	    console.log("complete");
	  });

	});

	$('#formEditStorage').validate({
	     rules: {
	          alias: {
	              minlength: 1,
	              maxlength: 45,
	              required: true
	          },
	          sqm: {
	              required: true,
	              number: true
	          },
	          price: {
	              required: true,
	              number: true
	          }
	      },
	      highlight: function(element) {
	          $(element).closest('.form-group').addClass('has-error');
	      },
	      unhighlight: function(element) {
	          $(element).closest('.form-group').removeClass('has-error');
	      },
	      errorElement: 'span',
	      errorClass: 'help-block',
	      errorPlacement: function(error, element) {
	          if(element.parent('.input-group').length) {
	              error.insertAfter(element.parent());
	          } else {
	              error.insertAfter(element);
	          }
	      },
	      submitHandler: function (form) {
	             $.ajax({
	               url: '{{ route('updateStorage') }}',
	               type: 'POST',
	               data: $(form).serialize(),
	             })
	             .done(function(data) {
	               console.info(data);
	               if (data != false) {
	                toastr["success"]("{{ trans('storages.storagesRegisterOk_lbl') }}");
	                $('#modalEditStorage').modal('hide');
	                cargaGridStorages();
	               }else{
	                 toastr["error"]("{{ trans('storages.somethingWasWrong_lbl') }}");
	               }

	             })
	             .fail(function() {
	               console.log("error");
	               toastr["error"]("{{ trans('storages.somethingWasWrong_lbl') }}");
	             })
	             .always(function() {
	               console.log("complete");
	             });

	      return false; // required to block normal submit since you used ajax
	     }
	  });

	  $('.cboWarehouse').change(function() {
	    buildingId = $(this).val();

	    if (buildingId > 0) {
	      //alert(buildingId);
	      cad = 'id='+buildingId+'&_token='+token;
	      $.ajax({
	        url: '{{ route('getLevels') }}',
	        type: 'POST',
	        data: cad,
	      })
	      .done(function(data) {
	        len = data.levels.length;
	        if(len > 0){
	          $('.cboLevel').empty();
	          $.each(data.levels, function(i, item) {
	            $('.cboLevel').append('<option value="'+data.levels[i].id+'">'+data.levels[i].name+'</option>');
	          })

	          lvlid = $('.cboLevel').val();
	          getStoragesSizes(lvlid);

	          $('.cboLevel').removeAttr('disabled');
	        }else{
	          $('.cboLevel').empty();
	          $('.cboLevel').append('<option selected>{{ trans('calculator.empty') }}</option>');
	        }

	      })
	      .fail(function() {
	        console.log("error");
	      })
	      .always(function() {
	        console.log("complete");
	      });

	    }

	  });


	  $('.cboLevel').change(function() {
	    lvlid = $(this).val();
	    getStoragesSizes(lvlid);
	  });

	  function getStoragesSizes(lvlid){
	    showLoader();
	    if (lvlid > 0) {
	      //alert(buildingId);
	      cad = 'lvlid='+lvlid+'&_token='+token;
	      $.ajax({
	        url: '{{ route('wsGetStoragesSizes') }}',
	        type: 'POST',
	        data: cad,
	      })
	      .done(function(data) {
	        //len = data.sizes.length;
	        //console.info(len);

	        if(data.success){
	          console.info('fill cbo sizes');
	          $('.sizeOfStorage').empty();
	          $.each(data.sizes, function(i, item) {
	            $('.sizeOfStorage').append('<option value="'+item.sqm+'">'+item.sqm+'</option>');
	          })

	          //$('.sizeOfStorage').removeAttr('disabled');
	        }else{
	          $('.sizeOfStorage').empty();
	          $('.sizeOfStorage').append('<option selected>{{ trans('calculator.empty') }}</option>');
	        }

	        hideLoader();

	      })
	      .fail(function() {
	        console.log("error");
	      })
	      .always(function() {
	        console.log("complete");
	      });

	    }
	  }

	  $('.storagesPlans').hide();

	  cad = '';

	  $(document).on('click', '#btn_calcFindStorage', function() {
	    build = $('.cboWarehouse').val();
	    level = $('.cboLevel').val();
	    sqm = parseFloat($('#span_total').text());

	    //console.info(sqm);

	    if (build <= 0 || isNaN(build)) {
	      toastr["error"]("{{ trans('calculator.select_building') }}");
	      return false;
	    }

	    if (level <= 0 || isNaN(level)) {
	      toastr["error"]("{{ trans('calculator.select_level') }}");
	      return false;
	    }

	    if (sqm <= 0 || isNaN(sqm)) {
	      toastr["error"]("{{ trans('calculator.sqm_error') }}");
	      return false;
	    }

	    cad = 'buid='+build+'&level='+level+'&sqm='+sqm+'&_token='+token;
	    findStorage(cad,level);

	  });

	  $(document).on('click', '.calcBtnNext', function() {
	    build = parseInt($('.cboWarehouse').val());
	    //alert(build);
	    level = parseInt($('.cboLevel').val());
	    sqm = parseFloat($('.sizeOfStorage').val());

	    if (build <= 0 || isNaN(build)) {
	      toastr["error"]("{{ trans('calculator.select_building') }}");
	      return false;
	    }

	    if (level <= 0 || isNaN(level)) {
	      toastr["error"]("{{ trans('calculator.select_level') }}");
	      return false;
	    }

	    if (sqm <= 0 || isNaN(sqm)) {
	      toastr["error"]("{{ trans('calculator.sqmClient_error') }}");
	      return false;
	    }

	    cad = 'buid='+build+'&level='+level+'&sqm='+sqm+'&_token='+token;
	    findStorage(cad,level);

	  });

	  function convertDate(date) {
	    var yyyy = date.getFullYear().toString();
	    var mm = (date.getMonth()+1).toString();
	    var dd  = date.getDate().toString();

	    var mmChars = mm.split('');
	    var ddChars = dd.split('');

	    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
	    //return (ddChars[1]?dd:"0"+ddChars[0])+'/'+(mmChars[1]?mm:"0"+mmChars[0])+'/'+yyyy;
	  }

	  function findStorage(cad,level){
	    showLoader();
	    $.ajax({
	      url: '{{ route('findStorage') }}',
	      type: 'POST',
	      data: cad
	    })
	    .done(function(data) {
	      if (data.success == false) {
	        toastr['error']("{{ trans('calculator.no_size_available')}}");
	        hideLoader();
	        return false;
	      }
	      //console.log(data.storage1.length);
	      if (data.storage1 === null){
	        toastr['error']("{{ trans('calculator.nothing_to_show') }}");
	      }else{
	        //alert(data.storage1.sqm);
	        $('.tblCalcStorages #sqm1').text(data.storage1.sqm);
	        $('.tblCalcStorages #sqm2').text(data.storage2.sqm);
	        $('.tblCalcStorages #price1').text(data.storage1.price);
	        $('.tblCalcStorages #price2').text(data.storage2.price);
	        $('.tblCalcStorages #price1').text(data.storage1.price);
	        $('.tblCalcStorages #depositMonth1').text(data.storage1.price);
	        $('.tblCalcStorages #depositMonth2').text(data.storage2.price);
	        $('.tblCalcStorages #seeOnMap1').attr('dir',data.storage1.id);
	        $('.tblCalcStorages #seeOnMap2').attr('dir',data.storage2.id);
	        $('.contentCalc').slideUp(200);
	        $('.storagesPlans').slideDown(200);

	        $('#pos1').empty();
	        $('#pos2').empty();

	        globalInsurance = data.insurance;
	        globalVat = data.vat;

	        $.each(data.pos1, function(i, item) {
	          $('#pos1').append('<option value="'+item.id+'" dir="'+item.price+'" lang="'+item.sqm+'">'+item.alias+' sqm: '+item.sqm+'</option>');
	        });

	        $.each(data.pos2, function(i, item) {
	          $('#pos2').append('<option value="'+item.id+'" dir="'+item.price+'" lang="'+item.sqm+'">'+item.alias+' sqm: '+item.sqm+'</option>');
	        });
	        
	        $("#pos1").trigger("change");
	        $("#pos2").trigger("change");

	        //calculate current month
	        /*oneDay = 24*60*60*1000;
	        date = new Date(), y = date.getFullYear(), m = date.getMonth();
	        firstDay = new Date(y, m, 1);
	        lastDay = new Date(y, m + 1, 0);
	        daysOfMonth = Math.round(Math.abs((firstDay.getTime() - lastDay.getTime())/(oneDay)));
	        */

	        //////////////////////////
	        today = new Date();

	        /*pricePerDay1 = data.storage1.price / daysOfMonth;
	        pricePerDay2 = data.storage2.price / daysOfMonth;
	        daysToPay = Math.round(Math.abs((today.getTime() - lastDay.getTime())/(oneDay)));
	        totalCurrentMonth1 = daysToPay * pricePerDay1;
	        totalCurrentMonth2 = daysToPay * pricePerDay2;
	        //$('.tblCalcStorages #currentMonth1').text(daysOfMonth+' // '+convertDate(today)+' // '+pricePerDay1.toFixed(2)+' // '+day
			sToPay+' // '+totalCurrentMonth.toFixed(2));
	        $('.tblCalcStorages #currentMonth1').text(totalCurrentMonth1.toFixed(2));
	        $('.tblCalcStorages #currentMonth2').text(totalCurrentMonth2.toFixed(2));*/
	        $('.tblCalcStorages .rentStart').val(convertDate(today));
	        //////////////////////////


	        //calculatePrice(1);
	        //calculatePrice(2);

	        drawStorageMap(data.storages,level);
	      }

	      hideLoader();
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  }

	  $('.tblCalcStorages .cboTermNotice').change(function() {
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $('.tblCalcStorages .cboPrePay').change(function() {
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });


	  $('.tblCalcStorages .cboCc').change(function() {
	    //alert('change!');
	    //calculatePrice(1);
	    //calculatePrice(2);
	    if($(this).val() == "1"){
	    	//YES
	    	$("#ccp").val(0);
	    }
	    else{
	    	//NO
	    	$("#ccp").val(20);
	    }
	  });

	  $('.tblCalcStorages .cboInsurance').change(function() {
	    //alert('change!');
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $('.tblCalcStorages .rentStart').blur(function(){
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $('.tblCalcStorages .rentStart').hover(function(){
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $('.tblCalcStorages #ccp').blur(function(){
	    if ($(this).val() == '') {
	      $(this).val('20');
	    }
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $('.tblCalcStorages #ccp').hover(function(){
	    if ($(this).val() == '') {
	      $(this).val('20');
	    }
	    //calculatePrice(1);
	    //calculatePrice(2);
	  });

	  $(document).on('change', '.pos1', function() {
	    idSt = $(this).val();
	    priceSt = $('option:selected', this).attr('dir');
	    sqm = $('option:selected', this).attr('lang');
	    
	    $("#sqm1_fixed").text(sqm)
	    $("#price1_fixed").text(priceSt);
	    
	    $('#seeOnMap1').removeAttr('dir');
	    $('#seeOnMap1').attr('dir',idSt);
	    $('.tblCalcStorages #price1').text(priceSt);
	    //calculatePrice(1);
	  });

	  $(document).on('change', '.pos2', function() {
	    idSt = $(this).val();
	    priceSt = $('option:selected', this).attr('dir');
	    sqm = $('option:selected', this).attr('lang');
	    
	    $("#sqm2_fixed").text(sqm);
	    $("#price2_fixed").text(priceSt);
	    
	    
	    $('#seeOnMap2').removeAttr('dir');
	    $('#seeOnMap2').attr('dir',idSt);
	    $('.tblCalcStorages #price2').text(priceSt);
	    //calculatePrice(2);
	  });

	  function calculatePrice(v){
	  	
	  	console.log("FN : calculatePrice");

	    //console.warn('Calculando!');
	    desc1 = 0;
	    desc2 = 0;
	    desc3 = 0;
	    nextMonth = 0;
	    prepayMonths = parseInt($('.tblCalcStorages .cboPrePay option:selected').text());
	    if (prepayMonths < 0 || isNaN(prepayMonths)) {
	      prepayMonths = 0;
	    }
	    //console.info(prepayMonths);
	    price = parseFloat($('.tblCalcStorages #price'+v).text());

	    fecha = $('.tblCalcStorages .rentStart').val();

	    /*
	    res = fecha.split('/');
	    y = res[2];
	    m = res[1];
	    d = res[0];
	    */

	    res = fecha.split('-');
	    y = res[0];
	    m = res[1];
	    d = res[2];


	    //calculate
	    //oneDay = 24*60*60*1000;
	    //date = new Date(), y = date.getFullYear(), m = date.getMonth() + 1, d = date.getUTCDate()-1;
	    firstDay = new Date(y, m, 1);
	    lastDay = new Date(y, m + 1, 0);
	    //daysOfMonth = Math.round(Math.abs((firstDay.getTime() - lastDay.getTime())/(oneDay)));
	    daysOfMonth = new Date(y, m, 0).getDate();
	    //console.info(y+'/'+m+' -> this month have: '+daysOfMonth+' days.');
	    today = moment(fecha, "DD/MM/YYYY");
	    today = today.toDate();
	    //console.info(today);
	    pricePerDay = price / daysOfMonth;
	    //console.warn(price+' - '+pricePerDay);

	    //daysToPay = Math.round(Math.abs((today.getTime() - lastDay.getTime())/(oneDay)));
	    daysToPay = daysOfMonth - d ;
	    totalCurrentMonth = (daysToPay + 1) * pricePerDay;
	    //console.error('total: '+daysOfMonth+', hoy: '+d+', '+daysToPay+' * '+pricePerDay+' = '+totalCurrentMonth);

	    $('.tblCalcStorages #currentMonth'+v).text(totalCurrentMonth.toFixed(2));

	    current = parseFloat($('.tblCalcStorages #currentMonth'+v).text());
	    deposit = parseFloat($('.tblCalcStorages #price'+v).text());
	    term = parseInt($('.cboTermNotice').val());
	    prepay = parseInt($('.cboPrePay').val());
	    cc = parseInt($('.cboCc').val());
	    insurance = parseInt($('.cboInsurance').val());
	    ccp = 1.2;
	    ins = 0;

		
	    if (insurance >= 1 ) {
	      //ins = parseFloat(globalInsurance);
	      ins = insurance;
	    }
	    
	    //alert(ins);

	    //insurence del mes en curso
	    insPerDay = ins/daysOfMonth;
	    currentInsurance = insPerDay  * (daysToPay + 1);
	    
	    //alert("insPerDay: " + insPerDay );
	    //alert("currentInsurance: " + currentInsurance );
	    
	    //console.info(currentInsurance);
	    
	    //deshabilita el campo del porcentaje
	    if(cc == 1){
	      $('#ccp').attr('disabled','true');
	      ccp = 0;
	    }else{
	      $('#ccp').removeAttr('disabled');
	      cc = 2;
	      ccp = $('#ccp').val() / 100;
	      ccp = ccp + 1;
	    }

	    if (price <= 0 || isNaN(price)){
	      price = 1;
	    }
	    if (current <= 0 || isNaN(current)){
	      current = 1;
	    }
	    if (term <= 0 || isNaN(term)){
	      term = 1;
	    }
	    if (prepay <= 0 || isNaN(prepay)){
	      prepay = 0;
	    }
	    /*if (cc <= 0 || isNaN(cc)){
	      cc = 2;
	    }*/

	    (term == 2) ?  desc1 = 5 : desc1 = desc1;
	    (term == 3) ?  desc1 = 10 : desc1 = desc1;
	    (term == 4) ?  desc1 = 15 : desc1 = desc1;

	    (prepay == 1) ?  desc2 = 0 : desc2 = desc2;
	    (prepay == 2) ?  desc2 = 0 : desc2 = desc2;
	    (prepay == 3) ?  desc2 = 5 : desc2 = desc2;
	    (prepay == 4) ?  desc2 = 10 : desc2 = desc2;
	    (prepay == 5) ?  desc2 = 15 : desc2 = desc2;

	    desc1a = current - ((desc1/100) * current);
	    desc1m = price - ((desc1/100) * price);
	    //console.info('desc1: '+desc1a+' / '+desc1m);

	    desc2a = desc1a - ((desc2/100) * desc1a);
	    desc2m = desc1m - ((desc2/100) * desc1m);
	    //console.info('desc2: '+desc2a+' / '+desc2m);

	    if(cc == 1){
	    	//console.info('IF');
	      desc3a = desc2a - ((desc2/100) * ccp);
	      desc3m = desc2m - ((desc2/100) * ccp);
	      
	      //console.info(desc2m + " - ((" + desc2 + "/100) * " + ccp +")");
	      
	      deposit = desc3m * 1;
	    }else{
	    	//console.info('ELSE');
	      desc3a = desc2a * ccp;
	      
	      //console.info(desc2m + "*" + ccp);
	      
	      desc3m = desc2m * ccp;
	      deposit = desc3m ;
	    }
	    
	    //console.info('deposit: ' + deposit );
	    
	    //console.info('desc3: '+desc3a+' / '+desc3m);
	    //console.warn('sub: '+desc3a+' + '+deposit);
	    totalPrepay = (desc3m * prepayMonths) + (ins * prepayMonths);
	    
	    //console.log("DESC3a: " + desc3a + "; DEPOSIT: " + deposit + "; INS: " + ins + "; TOTAL PREPAY: " + totalPrepay + "; CRRENT INS: " + currentInsurance);
	    
	    //SE QUITA EL DEPOSIT
	    //sub = desc3a + deposit + ins + totalPrepay + currentInsurance;
	    sub = desc3a + totalPrepay + currentInsurance;
	    
	    
	    if(v == 1)
	    console.info('sub ==> ' + desc3a + ' + ' + totalPrepay + ' + ' + currentInsurance);
	    
	    //get total + insurance
	    //total = sub * 1.23;
	    vatCalc = (parseFloat(globalVat)/100)+1;
	    
	    if(v == 1)
	    console.info('vatCalc ==>  (parseFloat(' + globalVat+ ' )/100) + 1 ') ;
	    
	    total = sub * vatCalc;
	    
	    if(v == 1)
	    console.info('total ==> '+ sub +' * '+ vatCalc);
	    
	    vat = total - sub;

	    nextMonthSub = desc3m + ins;
	    
	    //alert('nextMonthSub: ' + nextMonthSub);
	    //alert('vatCalc: ' + vatCalc);
	    
	    nextMonth = nextMonthSub * vatCalc;

	    //nextMonth = price+'/'+desc1m+'/'+desc2m+'/'+desc3m;

	    $('.tblCalcStorages #ppa'+v).text(totalPrepay.toFixed(2));
	    $('.tblCalcStorages #sub'+v).text(sub.toFixed(2));
	    $('.tblCalcStorages #vat'+v).text(vat.toFixed(2));
	    $('.tblCalcStorages #total'+v).text(total.toFixed(2));
	    $('.tblCalcStorages #depositMonth'+v).text(nextMonth.toFixed(2));
	    $('.tblCalcStorages #nextMonth'+v).text(nextMonth.toFixed(2));
	    //$('.tblCalcStorages #nextMonth'+v).text(nextMonth);
	    
		
	  }


	  function drawStorage(s,t,tx,ty,ts,rx,ry,rw,rh,rc,cl,id){
	    var t1 = s.text(tx, ty, t);
	    t1.attr({
	      color: '#000',
	      class: cl
	    });

	    var rect = s.rect(rx,ry,rw,rh);
	    rect.attr({
	      fill: rc,
	      stroke: '#000',
	      strokeWidth: 1,
	      id: t,
	      dir: id,
	    });

	  }

	  function drawWall(s,xStart,yStart,xEnd,yEnd,lineColor,lineWeigt){
	      var line = s.line(xStart, yStart, xEnd,yEnd);

	      line.attr({
	        stroke: '#000',
	        strokeWidth: lineWeigt
	      });
	  }

	  function colorStorageBk(v){
	    color = '';
	    switch (v) {
	      case 'rojo':
	          color = 'rgba(255, 23, 20, .5)';
	        break;
	      case 'verde':
	          color = 'rgba(0, 255, 18, .5)';
	        break;
	      case 'amarillo':
	          color = 'rgba(250, 255, 0, .5)';
	        break;
	      case 'claro':
	          color = 'rgba(255, 255, 255, .0)';
	        break;
	      default:
	          color = 'rgba(255, 255, 255, .0)';
	    }
	    return color;
	  }

	  function drawStorageMap(data,idLevel){
	    var s= Snap('#mysvg');


	    if (idLevel == 1) {
	      //acotaciones level 1
	      drawWall(s,62, 77, 419,77,'#FFF',2);
	      drawWall(s,473, 77, 504,77,'#FFF',2);
	      drawWall(s,504, 60, 504,77,'#FFF',2);
	      drawWall(s,522, 77, 595,77,'#FFF',2);
	      drawWall(s,649, 77, 1223,77,'#FFF',2);
	      drawWall(s,1222, 78, 1222, 644,'#FFF',2);
	      drawWall(s,712, 645, 1222, 643,'#FFF',2);
	      drawWall(s,649, 645, 695, 645,'#FFF',2);
	      drawWall(s,695, 644, 695, 665,'#FFF',2);
	      drawWall(s,473, 644, 595, 644,'#FFF',2);
	      drawWall(s,62, 644, 417, 644,'#FFF',2);
	      drawWall(s,104, 78, 104, 643,'#FFF',2);

	      drawStorage(s,'',0,0,'0',419,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
	      drawStorage(s,'',0,0,'0',595,78,54,5,'rgba(255, 255, 255, 0.3)','text12');
	      drawStorage(s,'',0,0,'0',595,639,54,5,'rgba(255, 255, 255, 0.3)','text12');
	      drawStorage(s,'',0,0,'0',417,639,56,5,'rgba(255, 255, 255, 0.3)','text12');
	      // dibujamos la oficina:
	      drawStorage(s,'OFFICE',235,130,'40',104,78,305,112,'rgba(111, 111, 111, 0.3)','text12');

	    }

	    if (idLevel == 2) {
	      drawWall(s,62, 77, 193,77,'#FFF',2); //INICIAL ARRIBA IZQUIERDA 225 30 px de cada puerta
	    drawWall(s,223, 77, 770,77,'#FFF',2);
	    drawWall(s,800, 60, 800, 77,'#FFF',2);
	    drawWall(s,800, 77, 1223,77,'#FFF',2);
	    drawWall(s,800, 77, 1240,77,'#FFF',2);
	    drawWall(s,1222, 78, 1222, 644,'#FFF',2);
	    drawWall(s,1010, 645, 1240, 643,'#FFF',2);
	    drawWall(s,980, 645, 62, 643,'#FFF',2);
	    drawWall(s,980, 645, 980, 660,'#FFF',2);
	    drawWall(s,104, 78, 104, 643,'#FFF',2);

	    //bodega lado derecho

	    drawWall(s,1000, 77, 1000, 187,'#FFF',2);
	    drawWall(s,999, 187, 1100, 187,'#FFF',2);
	    drawWall(s,1122, 187, 1222, 187,'#FFF',2);
	    //RECTANGULO ABAJO
	    drawWall(s,1122, 186, 1122, 211,'#FFF',2);
	    drawWall(s,1123, 202, 1098, 202,'#FFF',2);
	    drawWall(s,1122, 227, 1122, 247,'#FFF',2);
	    drawWall(s,1122, 247, 1132, 257,'#FFF',2);
	    drawWall(s,1132, 257, 1222, 257,'#FFF',2);

	    drawWall(s,1000, 87, 1077, 87,'#FFF',2);
	    drawWall(s,1147, 87, 1223, 87,'#FFF',2);
	    //RECTANGULO INTERNO
	    drawWall(s,1077, 82, 1147, 82,'#FFF',2);
	    drawWall(s,1076, 81, 1076, 162,'#FFF',2);
	    //drawWall(s,1076, 81, 1076, 162,'#FFF',2);

	    //PRIMER CELDA
	    drawWall(s,1076, 102, 1100, 102,'#FFF',2);drawWall(s,1125, 102, 1147, 102,'#FFF',2);
	    drawWall(s,1130, 82, 1130, 102,'#FFF',2);// cuadro pequeño
	    drawWall(s,1147, 81, 1147, 110,'#FFF',2);
	    //SEGUNDA CELDA
	    drawWall(s,1076, 122, 1126, 122,'#FFF',2);
	    drawWall(s,1125, 122, 1125, 142,'#FFF',2);
	    drawWall(s,1124, 142, 1140, 142,'#FFF',2);
	    //TERCERA CELDA
	    drawWall(s,1076, 142, 1100, 142,'#FFF',2);
	    drawWall(s,1099, 144, 1118, 144,'#FFF',2);
	    //CUARTA CELDA
	      drawWall(s,1076, 162, 1126, 162,'#FFF',2);
	      //EXTREMO DERECHO
	      drawWall(s,1147, 135, 1147, 163,'#FFF',2);
	      drawWall(s,1148, 135, 1170, 135,'#FFF',2);
	      drawWall(s,1147, 150, 1223, 150,'#FFF',2);

	    drawWall(s,1147, 182, 1147, 187,'#FFF',2);
	      drawWall(s,1127, 182, 1148, 182,'#FFF',2);

	    drawWall(s,1076, 182, 1076, 187,'#FFF',2);
	      drawWall(s,1075, 182, 1096, 182,'#FFF',2);

	      // dibujamos la oficina:
	      //drawStorage(s,'',130,140,'40',104,78,120,112,claro,'text12');
	    }

	    $.each(data, function(i, item) {
	      //console.info(data[i].id);
	      drawStorage(s,data[i].alias,data[i].text_posx,data[i].text_posy,'10',data[i].rect_posx,data[i].rect_posy,data[i].rect_width,data[i].rect_height,colorStorageBk(data[i].rect_background),data[i].class,data[i].id);
	      //return false;
	    });
	  }

	  $(document).on('click', '.seeOnMap', function() {
	    val = $(this).attr('dir');
	    $('rect').removeAttr('fill');
	    $('rect').attr('fill','rgba(255,255,255,0)');
	    //$('rect #'+val).removeAttr('fill');
	    $('rect[dir="'+val+'"]').removeAttr('fill');
	    //$('rect #'+val).attr('fill','rgb(120, 255, 0)');
	    $('rect[dir="'+val+'"]').attr('fill','rgba(120, 255, 0, 0.8');
	  });

	  $('.storagesMap').hide();

	  $(document).on('click', '.showMap', function() {
	    $('.storagesMap').slideDown(500);
	  });

	  $(document).on('click', '.hideMap', function() {
	    $('.storagesMap').slideUp(500);
	  });

	  $(document).on('click', '.seeOnMap', function() {
	    $('.storagesMap').slideDown(500);
	  });

	  function termNotifCheck(term,rentStart){
	    /*d = new Date();
	    month = d.getMonth()+1;
	    day = d.getDate();

	    var today = d.getFullYear() + '/' +
	      ((''+month).length<2 ? '0' : '') + month + '/' +
	      ((''+day).length<2 ? '0' : '') + day;
	    */

	    if( (new Date(term).getTime() > new Date(rentStart).getTime())){
	      return 'disabled';
	    }else{
	      return '';
	    }

	  }

	  $(document).on('click','.btnClientBoxes',function(){

	    $('.listStorages').empty();
	    $('.modalAddBoxFrm2 #idClient').val($(this).attr('lang'));
	    idClient = $(this).attr('lang');
	    getListStoragesClient(idClient);


	  });

	  

	  $(document).on('click','.btnAddClientStorage',function(){
	    showLoader();
	    $('#modalClientBoxes').modal('hide');
	    $('#modalAddBox').modal('show');
	    $('.modalAddBoxFrm1')[0].reset();
	    $('.modalAddBoxFrm2').hide();
	    hideLoader();
	  });

	  $("#searchStorageTxt").autocomplete({

	    //terms.push('#modalAddBox .cboWarehouse');

	    //source: "{{ route('findOneStorage') }}",
	    source: function(request, response) {
	    $.getJSON("{{ route('findOneStorage') }}", { building: $('#modalAddBox .cboWarehouse').val(),level: $('#modalAddBox .cboLevel').val(),term: $('#searchStorageTxt').val() },
	              response);
	  },
	    minLength: 1,
	    //data : term + '&var=hola',
	    select: function(event, ui) {
	      var idbox = ui.item.id;
	      var pricebox = ui.item.price;
	      var sqmbox = ui.item.sqm;
	      //$('.modalAddBoxFrm2').show();
	      $('.modalAddBoxFrm2').show("slide", { direction: "left" }, 1000);
	      $('.modalAddBoxFrm1').hide("slide", { direction: "left" }, 1000);
	      $('.modalAddBoxFrm2 #storageId').val(idbox);
	      $('.modalAddBoxFrm2 #price3').val(pricebox);
	      $('.modalAddBoxFrm2 #sqm3').text(sqmbox);

	      //calculatePriceOneBox(3);
	    },

	    html: true, // optional (jquery.ui.autocomplete.html.js required)

	    // optional (if other layers overlap autocomplete list)
	    open: function(event, ui) {
	      $('.ui-autocomplete-loading').css('visibility','none');
	      $(".ui-autocomplete").css("z-index", 9999999);
	      $(".ui-autocomplete").addClass('ui-front ui-menu ui-widget ui-widget-content');
	    }

	  });

	  function calculatePriceOneBox(v){
	  	console.log("FN : calculatePriceOneBox")
	    desc1 = 0;
	    desc2 = 0;
	    desc3 = 0;
	    price = parseFloat($('#price'+v).val());
	    term = parseInt($('.cboTermNotice').val());
	    prepay = parseInt($('.cboPrePay').val());
	    cc = parseInt($('.cboCc').val());
	    ins = $('.cboInsurance').val();
	    ccp = $('#ccp').val();

	    if (price <= 0 || isNaN(price)){
	      price = 1;
	    }
	    if (term <= 0 || isNaN(term)){
	      term = 1;
	    }
	    if (prepay <= 0 || isNaN(prepay)){
	      prepay = 1;
	    }
	    if (cc <= 0 || isNaN(cc)){
	      cc = 2;
	    }


		if (isNaN(ins)){
	      ins = 0;
	    }

	    ins = Number(ins);

	    (term == 2) ?  desc1 = 5 : desc1 = desc1;
	    (term == 3) ?  desc1 = 10 : desc1 = desc1;
	    (term == 4) ?  desc1 = 15 : desc1 = desc1;

	    (prepay == 2) ?  desc2 = 5 : desc2 = desc2;
	    (prepay == 3) ?  desc2 = 10 : desc2 = desc2;
	    (prepay == 4) ?  desc2 = 15 : desc2 = desc2;

	    if(cc == 1){
	      $('#ccp').attr('disabled',true);
	      ext = 0;
	    }else{
	      $('#ccp').removeAttr('disabled');
	      ext = $('#ccp').val();
	    }

	    desc1 = price - ((desc1/100) * price);

	    desc2 = desc1 - ((desc2/100) * desc1);

	    if(cc == 1){
	      desc3 = desc2 - ((desc2/100) * ext);
	    }else{
	      desc3 = desc2 * ((ext/100)+1);
	    }

	    sub = desc3 + ins;

	    total = Number(sub) * Number(vatTags);
	    vat = total - sub;

	    $('#sub'+v).text(sub.toFixed(2));
	    $('#vat'+v).text(vat.toFixed(2));
	    $('#total'+v).val(total.toFixed(2));
	  }

	  $('#modalAddBox .cboTermNotice').change(function() {
	    //calculatePriceOneBox(3);
	  });

	  $('#modalAddBox .cboPrePay').change(function() {
	    //calculatePriceOneBox(3);
	  });


	  $('#modalAddBox .cboCc').change(function() {
	    //calculatePriceOneBox(3);
	  });

	  $('#modalAddBox .cboInsurance').change(function() {
	    //calculatePriceOneBox(3);
	  });
	  
	  $('.storagesPlans .cboInsurance').change(function() {
	    	calculatePrice(1);
	        calculatePrice(2);
	  });

	  $('#modalAddBox #ccp').keyup(function() {
	    /* Act on the event */
	    //calculatePriceOneBox(3);
	  });

	  $('#modalAddBox #ccp').blur(function() {
	    /* Act on the event */
	    val = $(this).val();
	    if (val < 0 || val == '') {
	      $(this).val('20');
	    }
	  });

	  $('#modalAddBox #price3').keyup(function() {
	    /* Act on the event */
	    //calculatePriceOneBox(3);
	  });

	  $('#modalAddBox #price3').blur(function() {
	    /* Act on the event */
	    val = $(this).val();
	    if (val < 1 || val == '') {
	      $(this).val('0.00');
	    }
	  });

	  function calculatePriceOneBoxInverse(v){

		console.log("FN : calculatePriceOneBoxInverse")
	    finalprice = $('#total'+v).val();
	    desc1 = 0;
	    desc2 = 0;
	    desc3 = 0;
	    //price = parseFloat($('#price'+v).val());
	    term = parseInt($('.cboTermNotice').val());
	    prepay = parseInt($('.cboPrePay').val());
	    cc = parseInt($('.cboCc').val());
	    ins = $('.cboInsurance').val();
	    
	    ccp = $('#ccp').val();

	    if (price <= 0 || isNaN(price)){
	      price = 1;
	    }
	    if (term <= 0 || isNaN(term)){
	      term = 1;
	    }
	    if (prepay <= 0 || isNaN(prepay)){
	      prepay = 1;
	    }
	    if (cc <= 0 || isNaN(cc)){
	      cc = 2;
	    }

	    if (isNaN(ins)){
	      ins = 0;
	    }

	    ins = Number(ins);


	    vat = finalprice - (Number(finalprice)/Number(vatTags));
	    $('#vat'+v).text(vat.toFixed(2));

	    sub = finalprice - vat;
	    sub = sub - ins;
	    $('#sub'+v).text(sub.toFixed(2));

	    if(cc == 1){
	      $('#ccp').attr('disabled',true);
	      ext = 0;
	      desc3 = sub * (1 + (ext/100));
	    }else{
	      $('#ccp').removeAttr('disabled');
	      ext = $('#ccp').val();
	      desc3 = sub * (1 + (ext/100));
	    }

	    (prepay == 2) ?  desc2 = 5 : desc2 = desc2;
	    (prepay == 3) ?  desc2 = 10 : desc2 = desc2;
	    (prepay == 4) ?  desc2 = 15 : desc2 = desc2;

	    (term == 2) ?  desc1 = 5 : desc1 = desc1;
	    (term == 3) ?  desc1 = 10 : desc1 = desc1;
	    (term == 4) ?  desc1 = 15 : desc1 = desc1;

	    desc2 = desc3 / (1-(desc2/100));

	    desc1 = desc2 / (1-(desc1/100));

	    $('#price'+v).val(desc1.toFixed(2));

	  }



	  $('#modalAddBox #total3').keyup(function() {
	    /* Act on the event */
	    calculatePriceOneBoxInverse(3);
	  });

	  $('#modalAddBox #total3').blur(function() {
	    /* Act on the event */
	    val = $(this).val();
	    if (val < 0 || val == '') {
	      $(this).val('0.00');
	    }
	  });

	  $(document).on('click', '.btnAssignStorage', function() {

	    idLevel = $('.modalAddBoxFrm1 .cboLevel').val();
	    idClient = $('.modalAddBoxFrm2  #idClient').val();
	    idStorage = $('.modalAddBoxFrm2  #storageId').val();
	    price = $('.modalAddBoxFrm2  #price3').val();
	    term = $('.modalAddBoxFrm2 .cboTermNotice').val();
	    prepay = $('.modalAddBoxFrm2  .cboPrePay').val();
	    cc = $('.modalAddBoxFrm2  .cboCc').val();
	    rentDate = $('.modalAddBoxFrm2  .rentStart').val();
	    total = parseFloat($('.modalAddBoxFrm2  #total3').val());
	    insurance	= $('.modalAddBoxFrm2 .cboInsurance').val();
	    prepaid 	= $('.modalAddBoxFrm2 #prepaid').val();
	    deposit_month = $('.modalAddBoxFrm2 #deposit_month').val();

	     if (rentDate == '') {
	      toastr['error']("{{ trans('calculator.date_error') }}");
	      return false;
	    }

	    dateAr = rentDate.split('-');
	    rentDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];

	    //console.info(rentDate);

	    cad = 'idClient='+idClient+'&idLevel='+idLevel+'&idStorage='+idStorage+'&price='+price+'&total='+total+'&term='+term+'&prepay='+prepay+'&cc='+cc+'&rentDate='+rentDate+'&flag_assign=1&_token='+token+'&ins='+insurance+'&prepaid='+prepaid+'&deposit_month='+deposit_month;
		
		cad+= '&pd_box_price='+$("#pd_box_price").val()+'&pd_box_vat='+$("#pd_box_vat").val()+'&pd_box_total='+$("#pd_box_total").val();
		cad+= '&pd_insurance_price='+$("#pd_insurance_price").val()+'&pd_insurance_vat='+$("#pd_insurance_vat").val()+'&pd_insurance_total='+$("#pd_insurance_total").val();
		cad+= '&pd_subtotal='+$("#pd_subtotal").val()+'&pd_total_vat='+$("#pd_total_vat").val()+'&pd_total='+$("#pd_total").val(); 
        cad+= '&pd_total_next_month='+$("#pd_total_next_month").val();
        
        //alert(cad);
                
	    //console.info(cad);
	    $(this).attr('disabled',true);

	    $.ajax({
	      url: '{{ route('assignStorage') }}',
	      type: 'POST',
	      data: cad,
	    })
	    .done(function(data) {
	      if (data.success != true) {
	        toastr['error'](data.msj);
	        $('.btnAssignStorage').removeAttr('disabled');
	        return false;
	      }else{
	        toastr['success'](data.msj);
	        $('#modalAddBox').modal('hide');
	        getListStoragesClient(idClient);
	      }
	      $('.btnAssignStorage').removeAttr('disabled');
	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  $(document).on('click', '.btnSendTermNotif', function() {
		  $(this).prop( "disabled", true );
		  id = $(this).attr('id');
		  $('.btnAutomaticDate'+'.'+id).show();
		  $('.btnSetDate'+'.'+id).show();
	  });

	  $(document).on('click', '.btnSendTerm', function() {
		  showLoader();
		  $('#modalClientBoxes').modal('hide');

		  idSt = $(this).attr('id');
		  dateTerm = $('.selectDate'+'.'+idSt).val();
		  userId = $(this).attr('dir');
		  cad  = 'idSt='+idSt+'&dateTerm='+dateTerm+'&_token='+token;
		  console.info(cad);

		  setTimeout(function(){
			if (confirm('{{ trans('client.give_notice_confirm') }}')) {
			  $.ajax({
				url: '{{ route('wsSendTermNotif') }}',
				type: 'POST',
				data: cad,
			  })
			  .done(function(data) {
				hideLoader();
				getListStoragesClient(userId);
				console.log(data);
			  })
			  .fail(function() {
				console.log("error");
			  })
			  .always(function() {
				console.log("complete");
			  });
			}else{
			  hideLoader();
			  getListStoragesClient(userId);
			};
		  }, 500);
	  });

	  $(document).on('click', '.btnSetDate', function() {
	  		id = $(this).attr('id');
			$('#inptuAño').datepicker({
				minViewMode: 3,
				format: 'yyyy-mm-dd',
			});
			$('.btnAutomaticDate'+'.'+id).hide();
			$('.btnSetDate'+'.'+id).hide();
			$('.dateSelect'+'.'+id).show();
			$('.btnSendTerm'+'.'+id).show();
	  });

	$(document).on('click', '.btnAutomaticDate', function() {
	  showLoader();
	  $('#modalClientBoxes').modal('hide');

	  idSt = $(this).attr('id');
	  userId = $(this).attr('dir');
	  cad  = 'idSt='+idSt+'&userId='+userId+'&_token='+token;

	  setTimeout(function(){
		if (confirm('{{ trans('client.give_notice_confirm') }}')) {
		  //console.info('yes!');
		  $.ajax({
			url: '{{ route('wsSendTermNotifAuto') }}',
			type: 'POST',
			data: cad,
		  })
		  .done(function(data) {
			hideLoader();
			getListStoragesClient(userId);
			console.log(data);
		  })
		  .fail(function() {
			console.log("error");
		  })
		  .always(function() {
			console.log("complete");
		  });
		}else{
		  hideLoader();
		  getListStoragesClient(userId);
		};
	  }, 500);

	  //hideLoader();
	  /*idSt = $(this).attr('id');
	  userId = $(this).attr('dir');
	  cad  = 'idSt='+idSt+'&userId='+userId+'&_token='+token;

	  */

	});

	  $(document).on('click', '.detachStorage', function() {
	    showLoader();
	    $('#modalClientBoxes').modal('hide');

	    id = $(this).attr('id');
	    userId = $(this).attr('dir');
	    cad = '_token='+token+'&idStorage='+id;
	    console.info(cad);

	    setTimeout(function(){
	      if (confirm('{{ trans('storages.confirm_detach') }}')) {
	        $.ajax({
	          url: '{{ route('unAssignStorage') }}',
	          type: 'POST',
	          data: cad,
	        })
	        .done(function(data) {
	          hideLoader();
	          getListStoragesClient(userId);
	          console.log(data);
	        })
	        .fail(function() {
	          console.log("error");
	        })
	        .always(function() {
	          console.log("complete");
	        });
	      }else{
	        hideLoader();
	        getListStoragesClient(userId);
	      };
	    }, 500);

	    //hideLoader();
	    /*idSt = $(this).attr('id');
	    userId = $(this).attr('dir');
	    cad  = 'idSt='+idSt+'&userId='+userId+'&_token='+token;

	    */

	  });
	  
	  $(document).on('click', '.addPrepay', function() {
	    showLoader();
	    $('#modal-generico-small').modal('show');

	    id = $(this).attr('id');
	    userId = $(this).attr('dir');
	    cad = '_token='+token+'&idStorage='+id;
	    
	    var route = "{{route('storages.prepay.create',['*storage*'])}}";
	    
		route = route.replace('*storage*',id);
	    
	    $('#modal-generico-small-content').load(route);

	    hideLoader();
	    /*idSt = $(this).attr('id');
	    userId = $(this).attr('dir');
	    cad  = 'idSt='+idSt+'&userId='+userId+'&_token='+token;

	    */

	  });
	  

	  $(document).on('click', '.btnDownloadInvoice', function(event) {
	    //window.open('{{ route('wsGenInvoice') }}');
	    event.preventDefault();
	    $('#modal-generico').modal('show');

	    var storage_id = $(this).attr('id');
	    var client_id 	= $(this).attr('dir');

		var route = "{{ route('billing_index',['*storage_id*', '*client_id*']) }}";
		route = route.replace("*storage_id*", storage_id);
		route = route.replace("*client_id*", client_id);


	    $('#modal-generico-content').load(route);

	  });

	  $(document).on('click', '.viewInvoices', function() {
	    showLoader();
	    idSt = $(this).attr('id');
	    cad = 'idSt='+idSt;

	    $.ajax({
	      url: '{{ route('wsGetStorageInvoices') }}',
	      type: 'POST',
	      data: cad,
	    })
	    .done(function(data) {
	      console.log(data);
	      $('#tblStorageInvoices tbody').empty();
	      $('#storageInvoices').modal('show');
	      if (data.success) {
	        $.each(data.invoices, function(i, item) {
	          //pdf nombre
	          pdfName = item.pdf;
	          pdfName = pdfName.replace('/','');
	          //row class
	          payed = item.flag_payed;
	          if(payed){
	            rowClass = 'success';
	            btnMarkAsPayed = '';
	          }else{
	            rowClass = 'danger';
	            btnMarkAsPayed = '<a class="btn btn-xs btn-success btn_payInvoice" href="javascript:void(0);" id="'+item.id+'"><i class="fa fa-check" aria-hidden="true"></i></a>';
	          }

	          btnDownload = '<a class="btn btn-xs btn-info btn_downloadInvoice" href="javascript:void(0);" id="'+item.id+'" dir="'+item.pdf+'"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>';
	          $('#tblStorageInvoices tbody').append('<tr class="'+rowClass+'"><td>'+pdfName+'</td><td>'+btnMarkAsPayed+' '+btnDownload+'</td></tr>');
	        });
	      }else{
	        console.error('!!!!');
	      }
	      hideLoader();

	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  $(document).on('click', '.btn_downloadInvoice', function() {
	    file = $(this).attr('dir');
	    route = '{{ route('downloadInvoice',['*file*']) }}';
	    route = route.replace('*file*',file);
	    window.open(route);
	  });

	  $(document).on('click', '.btn_payInvoice', function() {
	    file = $(this).attr('id');
	    cad = 'file='+file;
	    showLoader();

	    $.ajax({
	      url: '{{ route('wsPayInvoices') }}',
	      type: 'POST',
	      data: cad,
	    })
	    .done(function(data) {
	      console.log(data);
	      if (data.success) {
	        toastr['success']('Done!');
	        $('#storageInvoices').modal('hide');
	      }else{
	        toastr['error']('Fail!');
	        console.error('!!!!');
	      }
	      hideLoader();

	    })
	    .fail(function() {
	      console.log("error");
	    })
	    .always(function() {
	      console.log("complete");
	    });

	  });

	  $(document).on("click", ".btnRegresarDeposito", function(event){
			event.preventDefault();

			if (confirm('{{ trans('client.return_deposit_confirm') }}')) {
				var route="{{route('depositos.update.regresar_deposito','*id*')}}";
				route = route.replace('*id*',$(this).data('id'));

				userId = $(this).attr('dir');

				$.get(route, function(response, status){
					getListStoragesClient(userId);
			    });

			}
		});

	  $(document).on("click", ".btnAddDeposito", function(event){
			event.preventDefault();

			var storage_id = $(this).data('id');
	    	var client_id 	= $(this).attr('dir');

			var route = "{{ route('depositos.create',['*storage_id*', '*client_id*']) }}";
			route = route.replace("*storage_id*", storage_id);
			route = route.replace("*client_id*", client_id);


			$("#modal-generico-small").modal("show");
			$("#modal-generico-small-content").load(route);

	
		});
		
		$(document).on("click", ".btnPagarDeposito", function(event){
			event.preventDefault();
			
			$("#modal-generico-small").modal("show");
			
			var route = "{{ route('depositos.pagar.create',['*deposit_id*']) }}";
			route = route.replace("*deposit_id*", $(this).data('id'));
			
			$("#modal-generico-small-content").load(route);

			
		});
		
		


	  //console.info('PW is ready!');

	});
