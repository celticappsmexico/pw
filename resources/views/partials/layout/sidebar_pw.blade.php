<!-- BEGIN MENUBAR-->
<div id="menubar" class="menubar-inverse ">
  <div class="menubar-fixed-panel">
    <div>
      <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
        <i class="fa fa-bars"></i>
      </a>
    </div>
    <div class="expanded">
      <a href="../../html/dashboards/dashboard.html">
        <span class="text-lg text-bold text-primary ">ADMIN</span>
      </a>
    </div>
  </div>
  <div class="menubar-scroll-panel">

    <!-- BEGIN MAIN MENU -->
    <ul id="main-menu" class="gui-controls">

      <!-- BEGIN DASHBOARD -->
      <li>
        <a href="{{ route('home') }}" class="active">
          <div class="gui-icon"><i class="md md-home"></i></div>
          <span class="title">{{ trans('sidebar.rdashboard_lbl') }}</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END DASHBOARD -->

      <!-- BEGIN CALCULATOR -->
      <li>
        <a href="{{ route('calculator')}}">
          <div class="gui-icon"><i class="fa fa-calculator" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.calculator_lbl') }}</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END CALCULATOR -->

      <!-- BEGIN CLIENTS -->
      <li class="gui-folder">
        <a href="#" id="btnNewClient_x" class="folder">
          <div class="gui-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.clients_lbl') }}</span>
        </a>
        <ul>
          <li class="gui-folder">
            <a href="{{route('oportunities')}}" class="folder">
              <span class="title">{{ trans('sidebar.oportunity_lbl') }}</span>
            </a>
          </li>
          <li class="gui-folder">
            <a href="{{route('users')}}" class="folder">
              <span class="title">{{ trans('sidebar.tenants') }}</span>
            </a>
          </li>
        </ul>
      </li><!--end /menu-li -->
      <!-- END CLIENTS -->

      <!-- BEGIN WAREHOUSE -->
      <li class="gui-folder">
        <a href="{{route('warehouse')}}" id="btnNewClient_x" class="folder">
          <div class="gui-icon"><i class="fa fa-building-o" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.warehouse_lbl') }}</span>
        </a>


        <ul>
        
        @foreach ($buildings as $build)

          <li class="gui-folder">
            <a href="{{route('levels',$build->id)}}" class="folder">
              <span class="title">{{ $build->name }}</span>
            </a>
            <ul>
               @foreach ($build->Levels as $level)
                  <li class="gui-folder">
                  <a href="{{route('storages',$level->id)}}" class="folder">
                    <span class="title">{{ $level->name }}</span>
                  </a>
                  <ul></ul>
                @endforeach
              </li>
            </ul>

        @endforeach
        </ul>
      </li><!--end /menu-li -->
      <!-- END WAREHOUSE -->

      <!-- BEGIN CART -->
      <li class="btnNewClient_x">
        <a href="{{route('cart',0)}}" id="">
          <div class="gui-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.cart_lbl') }}</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END CART -->


      <!-- BEGIN HISTORY -->
      <li class="btnNewClient_x">
        <a href="{{route('history')}}" id="">
          <div class="gui-icon"><i class="fa fa-history" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.history') }}</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END HISTORY -->

      <!-- BEGIN REPORTS -->
      <li class="btnNewClient_x">
        <a href="{{route('reports')}}" id="">
          <div class="gui-icon"><i class="fa fa-area-chart" aria-hidden="true"></i></div>
          <span class="title">{{ trans('sidebar.reports') }}</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END HISTORY -->

    </ul><!--end .main-menu -->
    <!-- END MAIN MENU -->

    <div class="menubar-foot-panel">
      <small class="no-linebreak hidden-folded">
        <span class="opacity-75">Copyright &copy; 2016</span> <strong>NovaCloud</strong>
      </small>
    </div>
  </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
<!-- END MENUBAR -->
