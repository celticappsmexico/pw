<!-- BEGIN HEADER-->
<header id="header" >
  <div class="headerbar">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="headerbar-left">
      <ul class="header-nav header-nav-options">
        <li class="header-nav-brand" >
          <div class="brand-holder">
            <a href="javascript:void(0);">
              <img style="heigth:30px!important;" src="{{ asset('assets/images/logo.png') }}" alt="przechowamy-wszystko" />
              @if(env('ENVIRONMENT') == "DEV")
              	&nbsp; <b>DEV</b>
              @endif
            </a>
          </div>
        </li>
        <li>
          @if (Auth::guest())

          @elseif (Auth::user()->Role_id == 1)
              <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
              </a>
          @elseif (Auth::user()->Role_id == 3)

          @endif
        </li>
        @if (Auth::guest())

        @elseif (Auth::user()->Role_id == 1)
            <!-- <li style="margin-left: 50px!important;" class="mainMenu"><a href="{{route('home')}}"> {{ trans('navbar.homeBtn') }} <span class="sr-only">(current)</span></a></li>
            <li class="mainMenu"><a href="{{route('storagesmap')}}"> {{ trans('navbar.storageBtn') }} </a></li> -->
        @elseif (Auth::user()->Role_id == 3)

        @endif
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav" style="margin-left: 50px!important;">

      </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="headerbar-right">
      <ul class="header-nav header-nav-options">
        <li>
          <!-- Search form -->
          <!--<form class="navbar-search" role="search">
            <div class="form-group">
              <input type="text" class="form-control" name="headerSearch" placeholder="{{ trans('navbar.searchPlacehlder') }}">
            </div>
            <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
          </form> -->
        </li>
        @if (Auth::guest())
        @else
          @if (Auth::user()->Role_id == 1)
	        <li class="dropdown hidden-xs">
	              <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
	                <i class="fa fa-mobile"></i><sup id="badge-admin" class="badge style-danger"></sup>
	              </a>
	              <ul class="dropdown-menu animation-expand notifDropDownAdmin">
	              </ul>
	        </li><!--end .dropdown -->
	        
            <li class="dropdown hidden-xs">
              <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                <i class="fa fa-bell"></i><sup id="badge-notif" class="badge style-danger"></sup>
              </a>
              <ul class="dropdown-menu animation-expand notifDropDown">
              </ul>
            </li><!--end .dropdown -->
          @endif
        @endif
      </ul><!--end .header-nav-options -->
      <ul class="header-nav header-nav-profile">
        @if (Auth::guest())
          <li class="mainMenu"><a href="{{route('auth/login')}}" class="headBtnGuest">{{ trans('navbar.loginBtn') }}</a></li>
          <li class="mainMenu"><a href="{{route('auth/register')}}" class="headBtnGuest">{{ trans('navbar.signupBtn') }}</a></li>
        @else
        <li class="dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle ink-reaction avatar_top" data-toggle="dropdown">
            <img src="{{ asset( Auth::user()->avatar ) }}" alt="" />
            <span class="profile-info">
                @if (Auth::user()->Role_id == 1)
                  @if (Auth::user()->name != '')
                    {{Auth::user()->name}}
                  @else
                    {{Auth::user()->email}}
                  @endif
                @elseif (Auth::user()->Role_id == 3)
                  @if (Auth::user()->name != '')
                    {{Auth::user()->name}}
                  @else
                    {{Auth::user()->email}}
                  @endif
                @endif
              <small>{{ Auth::user()->K_Role }}</small>
            </span>
          </a>
          <ul class="dropdown-menu animation-dock">
            <!--<li class="dropdown-header">Config</li>
            <li><a href="../../html/pages/profile.html">My profile</a></li>
            <li><a href="../../html/pages/blog/post.html">My blog <span class="badge style-danger pull-right">16</span></a></li>
            <li><a href="../../html/pages/calendar.html">My appointments</a></li>
            <li class="divider"></li>
            <li><a href="../../html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>-->
            <li><a href="{{route('profile')}}"><i class="fa fa-fw fa-gears text-info"></i> {{ trans('nav.profile') }}</a></li>
            @if (Auth::user()->Role_id == 1)
              <li><a href="{{route('systemSettings')}}"><i class="fa fa-wrench" aria-hidden="true"></i> {{ trans('nav.system_settings') }}</a></li>
            @endif
            <li><a href="{{route('auth/logout')}}"><i class="fa fa-fw fa-power-off text-danger"></i> {{ trans('navbar.logout') }}</a></li>
          </ul><!--end .dropdown-menu -->
        </li><!--end .dropdown -->
        @endif
      </ul><!--end .header-nav-profile -->
    </div><!--end #header-navbar-collapse -->
  </div>
</header>

<!-- END HEADER-->
