<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <style media="screen">
        	 
        	
            *{
              margin: 0;
              padding:0;
              font-size: 9.5px;
              font-family: DejaVu Sans;
            }

            .tblWrap{
              margin: 25px;
              /*width: 800px;*/
            }

            .firm{
              border-collapse: seperate;
              border-spacing: 50px!important;
            }

            .faktura-head{
              border-collapse: seperate;
              border-spacing: 3px!important;
            }
            .faktura-head td{
              text-align: center!important;
              margin: 0px;
              padding: 5px;
            }

            .dataWrap{
              border: none;
            }

            .dataWrap td{
              border: solid 1px #000;
            }

            .dataWrap td table td{
              border: none;
            }

            .dataWrap td{
              width: 50%;
            }

            .dataWrap td table{
              width: 100%;
            }

            .dataWrap td table .data{
              width: 15%;
            }


            .dataWrap td table .data2{
              font-weight: bold;
            }

            .items{
              border-collapse: collapse;
              border-spacing: 0px!important;
            }

            #row-1 {
                border: 1px solid black;
                text-align: center;
                /*padding-top: 80px;*/
                font-size: 11px;
            }
            .col-md-3 {
                margin-left: 3px;
                width: 313.3px;
            }
            #row-2 {
                border: 1px solid black;
                margin: 2px;
                width: 42.56%;
            }
            .faktura {
                border: 1px solid black;
                margin-top: 2px;
                background-color: #ccc;
                text-align: center;
            }
            .row.warszawa {
                border: 1px solid black;
                margin-top: 2px;
                text-align: center;
                height: 36px;
            }
            #span-2-warszawa {
                font-size: 10px;
            }
            .warszawa-fecha {
                margin-bottom: -5px;
            }
            .row.num-fact, .row.sprzedazy {
                border: 1px solid black;
                margin-top: 2px;
                height: 30.5px;
                text-align: center;
            }
            .kopia {
                border: 0px #FFF;
                text-align: center;
            }
            #div-row-3 {
                border: 1px solid black;
                margin-left: 17px;
                text-align: left;
                width: 69.4%;
                margin-top: 2px;
            }
            .col-md-3.forma, .col-md-3.termin {
                width: 220px;
                margin-left: -25px !important;
            }
            .col-md-3.bank {
                width: 140px;
            }
            .col-md-3.konto {
                width: 360px;
            }
            table {
              width: 100%;
                margin-top: 4px;
                margin-left: 2px;
            }
            th {
                border: 1px solid black;
                text-align: center;
                background-color: #ccc;
                padding:3px;
            }
            td {
                border: 1px solid black;
                text-align: left !important;
				padding:2px !important;
            }
        </style>
    </head>
    <body>
      <table class="tblWrap">
        <tr>
          <td style="border: none">
            <table class="faktura-head">
              <tr>
                <td rowspan="2" id="row-1">
                	<img src="http://pw.novacloud.link/assets/images/logo.png" style="width:160px"/>
                </td>
                <td rowspan="2" class="faktura">
                  <b>Faktura korygująca</b>
                </td>
                <td class="num-fact">
                  <b>19-KPW-{{$data['consecutiveCorrected']}}</b>
                </td>
              </tr>
              <tr>
                <td class="kopia">
                	<b>ORYGINAŁ</b>  
                </td>
              </tr>
              <tr>
                <td colspan="2" class="warszawa-fecha">
                	<table style="border:none !important">
						<tr style="border:none !important">
							<td colspan="5" style="border:none !important">
								19-PW-{{$data['numberInvoice']}} , 2019-09-04</span><br><span id="span-2-warszawa">numer, data dokumentu korygowanego</span>
							</td>
							<td colspan="5" style="border:none !important">
								<span id="span-1-warszawa"> Grudzień 2019</span><br>
				                  		<span id="span-2-warszawa">okres sprzedaży</span>
							</td>
						</tr>
					</table>
                  	
                </td>
                <td>
                  <span id="span-1-warszawa">2019-12-13 Piaseczno </span><br>
                  <span id="span-2-warszawa">data i miejsce wystawienia dokumentu</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="dataWrap">
              <tr>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Sprzedawca:
                      </td>
                      <td class="data2">
                        {{ $data['seller'] }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ $data['address'] }}
                      </td>
                    </tr>
                    <tr>
                    	<td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        5272703362
	                      </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table class="data1">
                    <tr>
                      <td class="data">
                        Nabywca:
                      </td>
                      <td class="data2">
                        {{ $data['clientName'] }}
                      </td>
                    </tr>
                    <tr>
                      <td class="data">
                        Adres:
                      </td>
                      <td class="data2">
                        {{ $data['clientAddress'] }}
                      </td>
                    </tr>
                    <tr>
                    	@if($data['clientType'] == 1)
                    		<td class="data">
		                        Pesel:
		                    </td>
		                    <td class="data2">
		                        {{ $data['clientPesel'] }}
		                    </td>
		                  @else
	                      <td class="data">
	                        NIP:
	                      </td>
	                      <td class="data2">
	                        {{ $data['clientNip'] }}
	                      </td>
	                      @endif
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
         <tr>
          <td style="border: none">
            <table>
              <tr>
                <td>
                  Forma płatności: : <span style="font-weight:bold"></span>
                  Termin płatności: <span style="font-weight:bold">2019-12-17</span>
                  Bank: <span style="font-weight:bold">{{ $data['bank'] }}</span>
                  Konto: <span style="font-weight:bold">{{ $data['account'] }}</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align: center !important">POZYCJE KORYGOWANE</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr >
                  <td style="text-align:center !important"> 
                      <br>
                        Data i miejsce wystawienia dokumentu: 2019-09-04 Warszawa
                      <br><br><br><br><br><br><br><br>
                    </td>
                </tr>
            </table>
          </td>
        </tr>
        
        
        <tr>
        	<td style="border: none; text-align:center !important;">POZYCJE PO KOREKCIE:</td>
        </tr>
        
        <tr>
          <td style="border: none">
            <table class="items">
                <tr>
                <td style="text-align:center !important">
                      <br>
                        Data i miejsce wystawienia dokumentu: 2019-10-08 Piaseczno
                      <br><br><br><br><br><br><br><br>
                    </td>
                    
                </tr>
             </table>
        	</td>
        </tr>
        

        <tr>
          <td style="border: none">
            <table>
              <tr>
                <td colspan="2" style="padding: 15px;">
                  Przyczyna korekty : <b>Błędna data i miejsce wystawienia faktury</b>
                </td>
              </tr>
              <tr style="border:1px solid black !important">
                <td style="padding: 15px; border:none">
                  
                </td>
                <td style="padding: 15px; border:none">
                	<br/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table class="firm">
              <tr>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do odebrania dokumentu
                </td>
                <td style="height: 120px;padding:90px 0px 15px 0px; text-align:center;font-size:12px;vertical-align:bottom;">
                  imię, nazwisko i podpis osoby upoważnionej do wystawienia dokumentu
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="border: none">
            <table  class="items" style="width:40%">
              <tr>
                <td style="border:none;text-align:right">
                 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </body>
</html>