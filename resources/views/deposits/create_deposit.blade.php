<form action="{{route('depositos.store')}}" method="post" id="frmAddDeposito">
{{ csrf_field() }}
	<input type="hidden" name="storage_id" value="{{$storage_id}}" />
	<input type="hidden" name="user_id" value="{{$client_id}}" />

	<div class="modal-header">
		<h4 class="modal-title" id="">{{ trans('storages.add_deposit') }} </h4>
	</div>
	<div class="modal-body">
	      <div class="row" >
			<div class="col-md-12">
	      		<div class="form-group">
	      			<label>{{Lang::get('storages.deposit_amount')}}</label>
	      			<input type="number" name="de_monto_deposito" id="de_monto_deposito" class="form-control" autocomplete="off" min="0" value="{{$storage->price_per_month}}" readonly />
	      		</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default closeFrmNewClient" data-dismiss="modal">{{ trans('client.close_lbl') }}</button>
		<button type="submit" class="btn btn-primary" id="btnSaveDepositos" name="button">{{ trans('client.send_lbl') }}</button>
	</div>
</form>

<script>


$("#btnSaveDepositos").click(function(e){
	$(this).hide();
});



$('#frmAddDeposito').validate({
    rules: {
        name: {
            number  : true,
            required: true
        },
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
           $.ajax({
             url: $('#frmAddDeposito').attr('action'),
             type: 'POST',
             data: $('#frmAddDeposito').serialize(),
           })
           .done(function(data) {
             //console.info(data);
             if (data.success != false) {
              toastr["success"]("{{ trans('messages.clientRegisterOk_lbl') }}");
              $('#modal-generico-small').modal('hide');
              //cargaGridUsers();
              getListStoragesClient("{{$client_id}}");

             }else{
               toastr["error"]("{{ trans('messages.clientRegisterErrorEmail_lbl') }}");
             }

           })
           .fail(function() {
             console.log("error");
             toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
           })
           .always(function() {
             console.log("complete");
           });


    return false; // required to block normal submit since you used ajax
   }
});

</script>
