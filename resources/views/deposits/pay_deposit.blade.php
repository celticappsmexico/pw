
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('cart.pay_deposit') }}</h4>
			</div>

			<div class="modal-body" style="padding:20px 20px;">
				<div id="div_depositos">
					<div class="wrapPaymentType">
						<select class="form-control selectPaymentType" name="selectPaymentType" id="selectPaymentType">
				          @foreach ($catapaymenttype as $pt)
				            <option value="{{ $pt->id }}">{{ $pt->name }}</option>
				          @endforeach
		        		</select>
					</div>
	
					<div class="wrapTransfer">
						<form class="frmPayDeposit" action="{{ route('depositos.pagar.store') }}" method="post" id="frmTransferPayDeposit">
							<input type="hidden" name="_token" id="_token">
							<input type="hidden" name="id" value="{{$id}}"/>				
							<input type="hidden" name="paymentType" id="paymentType" value="1">
							
							<div class="form-group">
								<label>{{ trans('cart.notes_deposit') }}</label>
									 {!! Form::input('text', 'de_notas', '', ['class'=> 'form-control', 'required' => 'true']) !!}
							</div>
							
							<div class="form-group" id="g_reference">
								<label>{{ trans('cart.reference') }}</label>
									 {!! Form::input('text', 'reference', '', ['class'=> 'form-control', 'id' => 'reference', 'required' => 'true']) !!}
							</div>
	
							<button type="submit" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>
						</form>
					</div>
	
	
					<div class="wrapManualPay">
						<form class="frmPayDeposit" action="{{ route('depositos.pagar.store') }}" method="post" id="frmManualPayDeposit">
							<input type="hidden" name="_token" id="_token">
							<input type="hidden" name="id" value="{{$id}}"/>
							<input type="hidden" name="paymentType" id="paymentType" value="1">
							
							<div class="form-group">
								<label>{{ trans('cart.notes_deposit') }}</label>
									 {!! Form::input('text', 'de_notas', '', ['class'=> 'form-control', 'required' => 'true']) !!}
							</div>
							
							<button type="submit" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>
						</form>
						<!-- <a href="javascript:void(0)" class="btn btn-info btnPayCash"> {{ trans('cart.pay') }}</a>-->
					</div>
	
					<div class="wrapBraintree">
						<form class="frmPayDeposit" id="frmBraintreePay" method="post" action="{{ route('depositos.pagar.store') }}">
							<input type="hidden" name="_token" id="_token">
							<input type="hidden" name="id" value="{{$id}}"/>
							<input type="hidden" name="paymentType" id="paymentType" value="2">
							<input type="hidden" name="card_user_id" id="idCardUser"/> 	
							<div class="wrapBraintree" id="braintree-cards-deposit">
							</div>
							
							<div class="form-group">
								<label>{{ trans('cart.credit_card') }}</label>
									 <input type="text" name="mask_card_user" id="mask-card-user" readonly class="form-control" />
							</div>
							
							<div class="form-group">
								<label>{{ trans('cart.notes_deposit') }}</label>
									 {!! Form::input('text', 'de_notas', '', ['class'=> 'form-control', 'required' => 'true']) !!}
							</div>
								
							
								
							<button type="submit" class="btn btn-info"><i class="fa fa-paper-plane-o"></i> {{ trans('cart.pay') }} </button>	
								
						</form>
					</div>
	      		</div>
	      </div>

			<div class="modal-footer">
					<button type="submit" class="btn btn-info" id="nextPay"></i>{{ trans('cart.next') }}</button>
			</div>

		

<script type="text/javascript">
	$(document).ready(function() {
		
		if("{{$creditCard}}" == "1"){
			$("#selectPaymentType").children().each(function() {
		        if ($(this).val() != "2") {
		            $(this).prop('disabled',true);
		        }
		    });

			$("#selectPaymentType").val("2");
		}else{
			$("#selectPaymentType").children().each(function() {
		        if ($(this).val() == "2") {
		            $(this).prop('disabled',true);
		        }
		    });

			$("#selectPaymentType").val("1");
		}
		
		$('.wrapBraintree, .wrapTransfer, .wrapManualPay').hide();

		$(document).on('click', '#nextPay', function() {
			
			type = $('.selectPaymentType').val();

			$(this).slideUp(200);
			$('.wrapPaymentType').slideUp(200);

			if (type == 1) {
				paymentType = $('#modalPayment .selectPaymentType').val();
				orderId = $('#modalPayment #orderId').val();
				$('#frmManualPay #orderId').val(orderId);
				$('.wrapManualPay #_token').val(token);
				$('.wrapManualPay').slideDown(200);
				$('.wrapBraintree , .wrapTransfer').hide();
			}

			if (type == 2) {
				$('.wrapBraintree').slideDown(200);
				$('.wrapManualPay , .wrapTransfer').hide();
				$('.wrapBraintree #_token').val(token);
				/*
				$('.wrapBraintree #paymentType').val(2);
				*/

				orderId = $('#modalPayment #orderId').val();
				
				//NUEVA FUNCIONALIDAD								

				var storage_id = '{{$storage->id}}';
				var client_id = '{{$deposito->user_id}}';
				var paymentId = '{{$storage->payment_data_id}}';

				var route = "{{ route('cards_users.credit_card_braintree.deposits',['*user*','*storage*','*paymentid*']) }}";
				route = route.replace('*user*',client_id);
				route = route.replace('*storage*',storage_id);
				route = route.replace('*paymentid*',paymentId);
				
				
			    $('#braintree-cards-deposit').load(route);

				
			}

			if (type == 3) {
				$('.wrapTransfer').slideDown(200);
				$('.wrapManualPay , .wrapBraintree').hide();
				$('.wrapTransfer #_token').val(token);
				$('.wrapTransfer #paymentType').val(3);
			}

		});

		$(document).on('click', '.btnSelectCardDeposit', function(event) {
			event.preventDefault();
			
			$(this).attr("disabled","disabled");

			$("#idCardUser").val( $(this).data('id') );
			$("#mask-card-user").val( $(this).data('mask') );
			
		});

		$('#frmManualPayDeposit').validate({
		      rules: {
		          de_notas: {
		              required: true		        
		          }
		      },
		      highlight: function(element) {
		          $(element).closest('.form-group').addClass('has-error');
		      },
		      unhighlight: function(element) {
		          $(element).closest('.form-group').removeClass('has-error');
		      },
		      errorElement: 'span',
		      errorClass: 'help-block',
		      errorPlacement: function(error, element) {
		          if(element.parent('.input-group').length) {
		              error.insertAfter(element.parent());
		          } else {
		              error.insertAfter(element);
		          }
		      },
		      submitHandler: function (form) {
		             $.ajax({
		               url: $(form).attr('action'),
		               type: 'POST',
		               data: $(form).serialize(),
		             })
		             .done(function(data) {
		               
		               if (data.returnCode == 200) {
		                toastr["success"]("{{ trans('cart.depositSavedOk_lbl') }}");

		                $("#modal-generico-small").modal('hide');

		                //var client_id = '{{$storage->user_id}}';
		                //getListStoragesClient(client_id);

		                location.reload();
		                
		                
		               }else{
		                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		               }

		             })
		             .fail(function() {
		               console.log("error");
		               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		             })
		             .always(function() {
		               
		             });


		      return false; // required to block normal submit since you used ajax
		     }
		  });

		$('#frmBraintreePay').validate({
		      rules: {
		          de_notas: {
		              required: true		        
		          },
		          mask_card_user:{
			          required : true
			      }
		      },
		      highlight: function(element) {
		          $(element).closest('.form-group').addClass('has-error');
		      },
		      unhighlight: function(element) {
		          $(element).closest('.form-group').removeClass('has-error');
		      },
		      errorElement: 'span',
		      errorClass: 'help-block',
		      errorPlacement: function(error, element) {
		          if(element.parent('.input-group').length) {
		              error.insertAfter(element.parent());
		          } else {
		              error.insertAfter(element);
		          }
		      },
		      submitHandler: function (form) {
		             $.ajax({
		               url: $(form).attr('action'),
		               type: 'POST',
		               data: $(form).serialize(),
		             })
		             .done(function(data) {
		               
		               if (data.returnCode == 200) {
		                toastr["success"]("{{ trans('cart.depositSavedOk_lbl') }}");

		                $("#modal-generico-small").modal('hide');

		                //var client_id = '{{$storage->user_id}}';
		                //getListStoragesClient(client_id);

		                location.reload();
						
		               }else{
		                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		               }

		             })
		             .fail(function() {
		               console.log("error");
		               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		             })
		             .always(function() {
		               
		             });


		      return false; // required to block normal submit since you used ajax
		     }
		  });

		$('#frmTransferPayDeposit').validate({
		      rules: {
		          de_notas: {
		              required: true		        
		          }
		      },
		      highlight: function(element) {
		          $(element).closest('.form-group').addClass('has-error');
		      },
		      unhighlight: function(element) {
		          $(element).closest('.form-group').removeClass('has-error');
		      },
		      errorElement: 'span',
		      errorClass: 'help-block',
		      errorPlacement: function(error, element) {
		          if(element.parent('.input-group').length) {
		              error.insertAfter(element.parent());
		          } else {
		              error.insertAfter(element);
		          }
		      },
		      submitHandler: function (form) {
		             $.ajax({
		               url: $(form).attr('action'),
		               type: 'POST',
		               data: $(form).serialize(),
		             })
		             .done(function(data) {
		               
		               if (data.returnCode == 200) {
		                toastr["success"]("{{ trans('cart.depositSavedOk_lbl') }}");

		                $("#modal-generico-small").modal('hide');

		                //var client_id = '{{$storage->user_id}}';
		                //getListStoragesClient(client_id);
		                
		                location.reload();
						
		               }else{
		                 toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		               }

		             })
		             .fail(function() {
		               console.log("error");
		               toastr["error"]("{{ trans('messages.somethingWasWrong_lbl') }}");
		             })
		             .always(function() {
		               
		             });


		      return false; // required to block normal submit since you used ajax
		     }
		  });
		

	});


</script>
