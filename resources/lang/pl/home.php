<?php

return [

    'welcome_lbl' => 'Witamy!',
    'msj_lbl' => 'Jesteś zalogowany.',
    'boxes_rented' => 'Puszki Wynajmowane',
    'current_boxes_rented' => 'Obecne pola są wynajmowane',
    'current_boxes' => 'Bieżące pola',
    'previous_month' => 'Poprzedni miesiac',
    'billed' => 'Rozliczono',
    'paid_invoices' => 'Płatne faktury',
    'total_billing' => 'Łączna kwota',
    'total_paid' => 'Pełna kwota',
    'sqm_rented' => 'Sqm wynajętym w tym miesiącu',
    'sqm_gained' => 'Sqm zyskał w tym miesiącu',
    'sqm_lost' => 'Sqm stracił w tym miesiącu',
    'of_space' => 'z Przestrzeni',
    'available' => 'Dostępny',
    'rented' => 'Wynajęty',
    'tenants' => 'Najemcy',
    'sqm_size' => 'Rozmiar Sqm',
    'rented' => 'Wynajęty',
    'billing' => 'Dane do faktury',
    'size' => 'Rozmiar',
    'occupacy_rate' => 'Obłożenie',
    'new_boxes' => 'Obecne nowe pola',
    'boxes' => 'Pudła',
    'billed' => 'Rozliczono',
    'payed' => 'Płatny',
    'not_paid' => 'Niezapłacone',
    'income_report' => 'Raport o dochodach',

];
