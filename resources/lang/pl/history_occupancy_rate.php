<?php

return [

    'occupancy_rate' => 'Obłożenie',
    'history_occupancy_rate' => 'Historyczna stopa wynajmu',
    'warehouse' => 'Magazyn',
    'level' => 'Poziom',
    'numberBoxes' => 'Liczba skrzynek',
    'numberBoxesRented' => 'Liczba wynajmowanych paczek',
    'numberBoxesAvailable' => 'Liczba pudełek dostępna',
    'errorSqm' => 'Proszę wybrać co najmniej jeden mkw',
    'sqm' => 'Mkw',
];
