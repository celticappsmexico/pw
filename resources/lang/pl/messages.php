<?php
  return[
    'r' => 'Polska test',
    'required'=> "To pole jest wymagane.",
    'remote'=> "Proszę poprawić to pole.",
    'email'=> "Proszę wpisać aktualny adres e-mail.",
    'url'=> "Proszę podać poprawny adres URL.",
    'date'=> "Proszę wprowadź poprawną datę.",
    'dateISO'=> "Proszę podać poprawną datę (ISO).",
    'number'=> "Proszę wprowadzić poprawny numer.",
    'digits'=> "Proszę wpisać tylko cyfry.",
    'creditcard'=> "Proszę podać poprawny numer karty kredytowej.",
    'equalTo'=> "Proszę ponownie wprowadzić tę samą wartość.",
    'accept'=> "Wpisz wartość z poprawnym rozszerzeniem.",
    'maxlength'=> "Proszę wpisać nie więcej niż {0} znaków.",
    'minlength'=> "Proszę podać co najmniej {0} znaków.",
    'rangelength'=> "Proszę wprowadzić wartość między {0} i {1} długości znaków",
    'range'=> "Proszę wprowadzić wartość między {0} a {1}.",
    'max'=> "Proszę podać wartość mniejszą lub równą {0}.",
    'min'=> "Proszę wprowadzić wartość większą lub równą {0}.",

    'clientRegisterOk_lbl' => "Rejestr Client OK",
    'somethingWasWrong_lbl' => "Coś było nie tak, spróbuj później",
    'clienDeleted_lbl' => "klient usunięte",
    'clientUpdated_lbl' => "Klient aktualizowane OK",

    'usernameCheckFail' => "Ta nazwa użytkownika jest już zajęta, proszę wybrać inną",
    'mailCheckFail' => "Ten e-mail już istnieje, proszę wybrać inną",

  ];
