<?php

return [
    'storage' => 'Przechowywanie',
    'name' => 'Nazwa',
    'company_name' => 'Nazwa firmy',
    'deposit_date' => 'Data wpłaty',
    'deposit_amount' => 'Kwota depozytu',
];
