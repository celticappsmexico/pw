<?php

return [

    'loginBtn' => 'Zaloguj Się',
    'signupBtn'   => 'Zapisz się',
    'homeBtn' => 'Dom',
    'storageBtn' => 'Mapa bagażu',
    'searchPlacehlder' => 'Wpisz słowo kluczowe',
    'logout' => 'Wyloguj',

];
