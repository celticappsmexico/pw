<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'loginHead' => 'Zaloguj Się',
    'failed'   => 'Błędny login lub hasło.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',
    'usernameTag'   => 'Nazwa Użytkownika',
    'passwordTag' => 'Hasło',
    'rememberTag' => 'Zapamiętaj mnie',
    'loginBtnTag' => 'ZALOGUJ SIĘ',
    'forgotTag' => 'Zapomniałeś hasła ?',

];
