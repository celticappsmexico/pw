<?php

return [
    'tenants' => 'Najemcy',
    'storage' => 'Przechowywanie',
    'level' => 'Poziom',
    'warehouse' => 'Magazyn',
    'name' => 'Nazwa',
    'company_name' => 'Nazwa firmy',
    'rent_start' => 'Wynajem Początek',
    'rent_end' => 'Wynajem Koniec',
];
