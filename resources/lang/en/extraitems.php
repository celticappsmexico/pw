<?php

return [

  'title' => 'Extra items',
  'item' => 'Item',
  'price' => 'Price',
  'quantity' => 'Quantity',
  'subtotal' => 'Sub-total',
  'vat' => 'vat',
  'total' => 'Total',
  'actions' => 'Actions',
  'add_item' => 'Add item',

  ];
