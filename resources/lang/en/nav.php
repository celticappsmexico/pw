<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do
    |
    */

   	'login'		=> 'Login',
   	'signup'	=> 'Sign-Up',
   	'logout'    => 'Logout',
    'notification_title' => 'Notifications:',
    'newOrderToGo' => 'New order has arrived.',
    'options' => 'Options:',
    'view_orders' => 'View all orders',
    'no_notifications' =>  'Nothing to show D:',
    'profile' => 'My profile',
    'system_settings' => 'System settings'

];
