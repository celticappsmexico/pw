<?php

return [

  'levels_title' => 'Levels',
  'new_level' => 'New Level',
  'name' => 'Name',
  'actions_lbl' => 'Actions',
  'new_levels_title' => 'New Level',
  'level_name' => 'Level name',
  'close' => 'Close',
  'send' => 'Send',
  'levelRegisterOk_lbl' => 'Level saved',
  'somethingWasWrong_lbl' => 'Something went wrong, please try again',
  'levelDeleted' => 'Level deleted',
  'deleteTitle' => 'Delete level',
  'delete_q' => 'Are you sure to want delete?',
  'delete' => 'Delete',
  'update_levels_title' => 'Update level',
  
 ];