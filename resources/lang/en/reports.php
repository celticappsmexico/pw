<?php

return [

    'occupancy_rate_by_size' 	=> 'Occupancy Rate by Size',
	'occupancy_rate_history' 	=> 'Occupancy Rate History',
    'bad_payers'				=> 'Bad Payers',
		
	'date_document'				=> 'Invoice Date',
	'payment_date'				=> 'Payment Date',
	'charge'					=> 'Amount',
	'days_late'					=> 'Days Late',
		
	'export_csv'				=> 'Export CSV',
	'export_xls'				=> 'Export XLS',
	'export_zip'				=> 'Export ZIP',
	'export_zip_copies'			=> 'Export ZIP Copies',
	'export_txt'				=> 'Export TXT',

	'paid'						=> 'Paid',
	'invoices'					=> 'Invoices',
		
	'subtotal'					=> 'Subtotal',
	'total'						=> 'Total',
	'vat'						=> 'VAT',
		
	'start_date'				=> 'Start date',
	'end_date'					=> 'End date',
		
	'number_invoice'			=> 'Number Invoice',
	'year_invoice'				=> 'Year Invoice',
		
	'number_invoices'			=> 'Number of invoices',
	'client'					=> 'Client',
	'from_invoice'				=> 'From',
	'to_invoice'				=> 'To the invoice',
     
];
