<?php

return [
    'storage' 			=> 'Storage',
    'name' 				=> 'Name',
    'company_name' 		=> 'Company Name',
    'deposit_date' 		=> 'Deposit Date',
    'deposit_amount' 	=> 'Deposit Amount',
	'notes'				=> 'Notes',
	'payment_type'		=> 'Payment Type',
	'paid'				=> 'Paid',
	'yes'				=> 'YES',
	'no'				=> 'NO',
		
];
