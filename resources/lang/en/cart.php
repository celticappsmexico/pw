<?php

return [

  'cart_title' => 'Orders list',
  'order_number' => 'Order id',
  'client' => 'Client',
  'amount' => 'Amount',
  'actions' => 'Actions',
  'order_modal_title' => 'Order detail ',
  'item' => 'Item',
  'price' => 'Price',
  'subtotal' => 'Subtotal:',
  'vat' => 'vat:',
  'total' => 'Total',
  'quantity' => 'Quantity',
  'close' => 'Close',
  'pay' => 'Pay',
  'go_to_save' => 'Invoice and Save',
  'go_to_pay' => 'Invoice and Pay',
  'pay_deposit'	=> 'Pay Deposit',
  'next' => 'Next',
  'date' => 'Date',
  // PAYMENTS
  'cash' => 'Cash',
  'credit_card' => 'Credit card',
  'check' => 'Check',
  'transfer' => 'Transfer',
  'sure_to_delete_order' => 'Are you sure to want to delete order?',
  'done' => 'Done!',
  'wrong' => 'Oops! something went wrong',
  'suscription_id' => 'Suscription Id',
  'amount' => 'Amount',
  'success' => 'Success!',
  'download_invoice' => 'Download generated invoice for this transaction',
	'print_invoice' => 'Print fiscal receipt',
	'order_unavailable' => 'Order unavailable',
  'reference' => 'Reference',
		
	'suscription_success' 			=> 'Suscription success',
	'suscription_canceled' 			=> 'Suscription canceled',
	'confirm_cancel_suscription'	=> 'Are you sure want to cancel the subscription?',

	'suscribe_braintree' 			=> 'SUSCRIBE TO BRAINTREE',
	'cancel_braintree' 				=> 'UNSUSCRIBE BRAINTREE',
	
		
		
	'card_type'		=> 	'Card Type',
	'card_number'	=> 	'Card number',
	'card_expiration'=> 'Expiration Date',
		
	'card_select'	=> 'Select Card' ,
		
	'deposit_paid'	=> 'Deposit Paid',
		
	'notes_deposit'	=> 'Notes', 
		
	'depositSavedOk_lbl'	=> 'Deposit Saved successfully'

 ];
