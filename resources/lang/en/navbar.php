<?php

return [

    'loginBtn' => 'Login',
    'signupBtn'   => 'Sign-Up',
    'homeBtn' => 'Home',
    'storageBtn' => 'Storage map',
    'searchPlacehlder' => 'Enter your keyword',
    'logout' => 'Logout',

];
