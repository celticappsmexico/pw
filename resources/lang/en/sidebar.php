<?php
  return[
    'rdashboard_lbl' => 'Dashboard',
    'clients_lbl' => 'Clients',
    'warehouse_lbl' => 'Warehouse',
    'calculator_lbl' => 'Estimator',
    'cart_lbl' => 'Order list',
    'oportunity_lbl' => 'Oportunities',
    'tenants' => 'Tenants',
    'history' => 'History',
    'reports' => 'Reports',

    ];
