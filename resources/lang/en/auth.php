<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'loginHead' => 'Login',
    'usernameTag'   => 'Username',
    'passwordTag' => 'Password',
    'rememberTag' => 'Remember me',
    'loginBtnTag' => 'LOGIN',
    'forgotTag' => 'Forgot password ?',

];
