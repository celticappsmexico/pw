<?php
  return[
    'r' => 'English test',
    'required'=> "This field is required.",
    'remote'=> "Please fix this field.",
    'email'=> "Please enter a valid email address.",
    'url'=> "Please enter a valid URL.",
    'date'=> "Please enter a valid date.",
    'dateISO'=> "Please enter a valid date (ISO).",
    'number'=> "Please enter a valid number.",
    'digits'=> "Please enter only digits.",
    'creditcard'=> "Please enter a valid credit card number.",
    'equalTo'=> "Please enter the same value again.",
    'accept'=> "Please enter a value with a valid extension.",
    'maxlength'=> "Please enter no more than {0} characters.",
    'minlength'=> "Please enter at least {0} characters.",
    'rangelength'=> "Please enter a value between {0} and {1} characters long.",
    'range'=> "Please enter a value between {0} and {1}.",
    'max'=> "Please enter a value less than or equal to {0}.",
    'min'=> "Please enter a value greater than or equal to {0}.",

    'clientRegisterOk_lbl' => "Client register OK",
  	'clientRegisterError_lbl' => "Client already exists",

  	'clientRegisterErrorEmail_lbl' => "Email already exists",
  		
    'somethingWasWrong_lbl' => "Something went wrong, please try later",
    'clienDeleted_lbl' => "Client deleted",
    'clientUpdated_lbl' => "Client updated OK",

    'usernameCheckFail' => "This username is already taken, please select another one",
    'mailCheckFail' => "This Email already exist, please select another one",
    'success_transaction' => 'Transaction was successfully',
  		
  	'pay_saved'	=> 'Pay successfully saved',
  	'prepay_saved' => 'Prepay saved',
  		
  	'complete_form'	=> 'Complete the form',
  	'card_saved'		=> 'Card saved'

  ];
