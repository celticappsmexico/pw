<?php

return [

  'storages_title' => 'Storages',
  'new_storage' => 'New storage',
  'alias' => 'Alias',
  'sqm' => 'sqm',
  'price' => 'Price',
  'rent_by' => 'Rented by',
  'rent_start' => 'Rent started',
  'rent_end' => 'Rent end on',
  'actions_lbl' => 'Actions',
  'storagesRegisterOk_lbl' => 'Storage saved',
  'somethingWasWrong_lbl' => 'Something went wrong!',
  'comments' => 'Comments',
  'close' => 'Close',
  'send' => 'Send',
  'deleteTitle' => 'Delete Storage',
  'delete_q' => 'Are you sure you want to delete?',
  'delete' => 'Delete',
  'levelDeleted' => 'Storage deleted',
  'show_storagesMap' => 'Show storages map',
  'hide_storagesMap' => 'Hide map',
  'choose_notice' => 'CHOOSE A NOTICE',
  'choose_option' => 'CHOOSE A OPTION',
  'choose_items' => 'CHOOSE ITEMS',
  'off' => 'OFF',
  'add_storage' => 'ADD STORAGE',
  'rent_storage' => 'Rent Storage',
  'incomplete_data' => 'Inconsistent data',
  'done' => 'Done.',
  'nothing_to_show' => 'Nothing to show.',
  'send_term' => 'Give notice',
  'down_invoice' => 'Invoices',
  'date_range' => 'Date range',
  'to' => 'to',
  'search_lbl' => 'search',
  'search_title' => 'Filters',
  'start' => 'start',
  'end' => 'end',
  'automatic_date' => 'Automatic Date',
  'set_date' => 'Set Date',
  'invalid_range' => 'Invalid date range',
  'detach' => 'Detach',
  'confirm_detach' => 'Are you sure want to detach this storage?',
  'box_number'		=> 'Box number',
  'recurrent_billing' => 'Recurrent Billing',
	'term_of_notice'	=> 'Term of notice',

	'return_deposit'		=> 'Return deposit',
	'return_deposit_date'	=> 'Return deposit date',

	'add_deposit'			=> 'Add Deposit',
	'deposit_amount'		=> 'Deposit amount',
	'prepaid_months'		=> 'Prepaid Months',
	'add_prepaid_months'		=> 'Add Prepaid Months',
	
	'container'				=> 'Container'





 ];
