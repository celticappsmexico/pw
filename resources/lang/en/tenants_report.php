<?php

return [
    'tenants' => 'Tenants',
    'storage' => 'Storage',
    'level' => 'Level',
    'warehouse' => 'Warehouse',
    'name' => 'Name',
    'company_name' => 'Company Name',
    'rent_start' => 'Rent Start',
    'rent_end' => 'Rent End',
];
