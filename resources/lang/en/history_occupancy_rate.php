<?php

return [

    'occupancy_rate' => 'Occupancy rate',
    'history_occupancy_rate' => 'History Occupancy Rate',
    'warehouse' => 'Warehouse',
    'level' => 'Level',
    'numberBoxes' => 'Number of Boxes',
    'numberBoxesRented' => 'Number of Boxes Rented',
    'numberBoxesAvailable' => 'Number of Boxes Available',
    'sqm' => 'Sqm',
    'errorSqm' => 'Please, choose at least one sqm',
];
