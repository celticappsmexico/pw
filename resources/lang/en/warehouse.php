<?php

return [

	'warehouse_title' => 'Warehouse',
	'new_warehouse' => 'New Building',
	'titleNew' => 'New Building',
	'titleUpdate' => 'Update Building',
	'name' => 'Name',
	'country' => 'Country',
	'full_address' => 'Full Address',
	'actions_lbl' => 'Actions',
	'close' => 'Close',
	'save' => 'save',
	'buildRegisterOk_lbl' => 'Saved correctly',
	'somethingWasWrong_lbl' => 'Something went wrong, please try again',
	'buildDeleted_lbl' => 'Building deleted correctly',
	'delete' => 'Delete building',
	'delete_q' => 'Are you sure you want to delete?',

 ];