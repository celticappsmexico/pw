<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do
    |
    */

   	'settings_title'		=> 'System settings',
    'btn_update' => 'Update',
    'system' => 'System',
    'invoice' => 'Invoice',
    'insurance' => 'Insurence',
    'vat' => 'VAT',
    'expedicion' => 'Expedition place',
    'address' => 'Seller address',
    'nip' => 'NIP',
    'payment_type' => 'Payment type',
    'update_ok' => 'Updated successfully',
    'bank' => 'Bank',
    'account' => 'Account',
    'invoice_number' => 'Invoice Number',
    'seller' => 'Seller',


];
