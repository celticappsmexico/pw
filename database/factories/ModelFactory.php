<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
        'username' => str_replace('.', '_', $faker->unique()->userName),
        'lastName' => $faker->lastName,
        'companyName' => $faker->company,
        'peselNumber' => $faker->numberBetween(00000000000,99999999999),
        'idNumber' => $faker->numberBetween(00000000000,99999999999),
        'nipNumber' => $faker->numberBetween(00000000000,99999999999),
        'regonNumber' => $faker->numberBetween(00000000000,99999999999),
        'courtNumber' => $faker->numberBetween(00000000000,99999999999),
        'courtPlace' => $faker->city,
        'krsNumber' => $faker->numberBetween(00000000000,99999999999) ,
        'Role_id' => $faker->numberBetween(1,3),
        'userType_id' => $faker->numberBetween(1,3),
        'birthday' => $faker->dateTimeBetween('-40 years', '-18 years'),
        'phone' => $faker->phoneNumber,
        'active' => 1
    ];
});
