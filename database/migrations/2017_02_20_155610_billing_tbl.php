<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillingTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing', function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('storage_id');
          $table->string('pay_month');
          $table->integer('flag_payed')->default(0);

          $table->double('abono');
          $table->double('cargo');
          $table->double('saldo');
          $table->string('pdf');
          $table->string('reference');

          $table->integer('bi_flag_last')->default(0);

          $table->integer('payment_type_id')->default(0);

          //NUEVOS CAMPOS
          $table->double('bi_subtotal');
          $table->double('bi_porcentaje_vat');
          $table->double('bi_total_vat');
          $table->double('bi_total');

          $table->enum('bi_status_impresion', array('PENDIENTE','EN PROCESO','TERMINADO','ERROR','NO APLICA','PENDIENTE PDF'))
          	->default('PENDIENTE');
          
          $table->string('bi_transaction_braintree_id');
          $table->integer('payment_data_id')->nullable();
          
          $table->integer('bi_batch')->nullable();
          
          $table->integer('bi_flag_prepay')->default(0);
          
          $table->integer('bi_paragon')->default(0);
          
          $table->string('pdf_corregido')->nullable();
          
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing');
    }
}
