<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
           'history_incomes',
           function (Blueprint $table) {
               $table->increments('id');
               $table->integer('billed');
               $table->integer('payed');
               $table->integer('not_payed');
               $table->string('month');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_incomes');
    }
}
