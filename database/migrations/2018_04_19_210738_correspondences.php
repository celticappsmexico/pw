<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Correspondences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('correspondences', function (Blueprint $table) {
          $table->increments('id');
          $table->text('users_ids');
          $table->text('asunto');
          $table->text('corresp_mensaje');
          $table->text('mail_from');
          $table->enum('co_status', ['PENDING','SENT','CANCELLED']);
          $table->timestamps();
        });

    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correspondences');
    }
}
