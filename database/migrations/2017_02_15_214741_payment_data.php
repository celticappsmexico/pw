<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_data', function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('storage_id');
          $table->integer('term');
          $table->integer('prepay');
          $table->integer('creditcard');
          $table->integer('order_id');
          $table->float('pd_vat');
          
          $table->double('pd_box_price');
          $table->double('pd_box_vat');
          $table->double('pd_box_total');
          $table->double('pd_insurance_price');
          $table->double('pd_insurance_vat');
          $table->double('pd_insurance_total');
          $table->double('pd_subtotal');
          $table->double('pd_total_vat');
          $table->double('pd_total');
          $table->double('pd_total_next_month');
          
          $table->integer('pd_suscription_canceled')->default('0');
          
          
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_data');
    }
}
