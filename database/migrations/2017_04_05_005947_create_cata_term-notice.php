<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCataTermNotice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create(
           'cata_term',
           function (Blueprint $table) {
               $table->increments('id');
               $table->integer('months');
               $table->integer('off');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cata_term');
    }
}
