<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('det_order', function(Blueprint $table){
          $table->increments('id');
          $table->integer('order_id');
          $table->string('product_type');
          $table->integer('product_id');
          $table->date('rent_starts')->default('0000-00-00');
          $table->integer('quantity');
          $table->float('price');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_order');
    }
}
