<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
           'history',
           function (Blueprint $table) {
               $table->increments('id');
               $table->integer('storage_id');
               $table->integer('user_id');
               $table->integer('payment_data_id');
               $table->string('rent_start');
               $table->string('rent_end');
               $table->string('price_per_month');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history');
    }
}
