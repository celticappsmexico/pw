<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryOccupancyRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
           'history_occupancy_rate',
           function (Blueprint $table) {
               $table->increments('id');
               $table->string('warehouse');
               $table->string('level');
               $table->float('sqm');
               $table->integer('sqm_quantity');
               $table->float('occupancy_rate');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_occupancy_rate');
    }
}
