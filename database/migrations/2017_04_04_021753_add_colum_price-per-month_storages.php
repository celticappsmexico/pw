<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumPricePerMonthStorages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('storages', function (Blueprint $table) {
          $table->float('price_per_month')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('storages', function (Blueprint $table) {
          $table->dropColumn('price_per_month');
      });
    }
}
