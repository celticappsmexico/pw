<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('depositos', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('storage_id');
    		$table->integer('user_id');
    		$table->double('de_monto_deposito');
    		$table->integer('de_regresado')->default(0);
    		$table->integer('payment_type_id')->nullable();
    		$table->string('de_notas')->nullable();
    		$table->string('de_transaction_id_braintree')->nullable();
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::dropIfExists('depositos');
    }
}
