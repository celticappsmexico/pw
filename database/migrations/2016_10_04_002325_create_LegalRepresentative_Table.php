<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalRepresentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('client_legal_representatives', function (Blueprint $table) {
          $table->increments('id');
          $table->string('user_id');
          $table->string('name');
          $table->string('lastName');
          $table->string('function');
          $table->string('phone');
          $table->string('email');
          $table->timestamps();
        }
      );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_legal_representatives');
    }
}
