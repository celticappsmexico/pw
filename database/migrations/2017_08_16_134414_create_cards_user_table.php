<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('cards_user', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('user_id');
    		$table->string('cu_token_card');
    		$table->string('cu_customer_id');
    		$table->integer('cu_eliminado');
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::dropIfExists('cards_user');
    }
}
