<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CataPaymentTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cata_payment_types', function(Blueprint $table){
          $table->increments('id');
          $table->string('name', 45);
          $table->integer('active');
          $table->string('cpt_lbl_factura');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cata_payment_types');
    }
}
