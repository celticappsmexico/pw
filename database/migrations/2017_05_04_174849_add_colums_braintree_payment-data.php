<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsBraintreePaymentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_data', function (Blueprint $table) {
        $table->string('bt_id_plan')->default('');
        $table->string('idBt')->default('');
        $table->date('start_plan')->default('0000-00-00');
        $table->integer('flag_plan_active')->default(0);
        $table->string('token_pay')->defailt('');
        $table->integer('active')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payment_data', function (Blueprint $table) {
          $table->dropColumn('bt_id_plan');
          $table->dropColumn('idBt');
          $table->dropColumn('start_plan');
          $table->dropColumn('flag_plan_active');
          $table->dropColumn('token_pay');
          $table->dropColumn('active');
      });
    }
}
