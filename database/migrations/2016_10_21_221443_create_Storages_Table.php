<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storages', function (Blueprint $table) {
          $table->increments('id');
          $table->string('alias');
          $table->integer('text_posx');
          $table->integer('text_posy');
          $table->integer('size');
          $table->integer('rect_posx');
          $table->integer('rect_posy');
          $table->integer('rect_width');
          $table->integer('rect_height');
          $table->string('rect_background')->default('claro');
          $table->string('class')->default('text10');
          $table->integer('level_id');
          $table->integer('user_id');
          $table->integer('payment_data_id')->default(0);
          $table->float('sqm');
          $table->float('price');
          $table->float('final_price');
          $table->date('rent_start');
          $table->date('rent_end')->nullable();
          $table->mediumText('comments');
          $table->integer('active');
          $table->integer('flag_rent')->default(0);
          
          $table->integer('st_meses_prepago')->default(0);
          
          $table->timestamps();
        }
      );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storages');
    }
}
