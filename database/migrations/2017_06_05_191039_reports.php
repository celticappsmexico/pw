<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
           'reports',
           function (Blueprint $table) {
               $table->increments('id');
               $table->string('month');
               $table->string('day');
               $table->string('year');
               $table->string('date');
               $table->float('month_total_amount');
               $table->float('rent_percent');
               $table->integer('storages_rent');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
