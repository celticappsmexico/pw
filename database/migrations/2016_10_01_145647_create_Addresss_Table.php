<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddresssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('client_address', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->string('street');
          $table->string('number');
          $table->string('apartmentNumber');
          $table->string('postCode');
          $table->string('city');
          $table->integer('country_id');
          $table->timestamps();
        }
      );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_address');
    }
}
