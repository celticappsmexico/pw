<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBtRecBillData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
             'bt_recurring_billing',
             function (Blueprint $table) {
                 $table->increments('id');
                 $table->string('data');
                 $table->timestamps();
             }
         );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bt_recurring_billing');
    }
}
