<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('billing_detail', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('billing_id');
    		$table->string('bd_nombre');
    		$table->string('bd_codigo');
    		$table->integer('bd_numero');
    		$table->double('bd_valor_neto');
    		$table->double('bd_porcentaje_vat');
    		$table->double('bd_total_vat');
    		$table->double('bd_total');
    		$table->enum('bd_tipo_partida', ['BOX','INSURANCE','ITEM']);
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::dropIfExists('billing_detail');
    }
}
