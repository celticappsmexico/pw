<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryBoxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
           'history_boxes',
           function (Blueprint $table) {
               $table->increments('id');
               $table->integer('boxes_rented');
               $table->integer('new_boxes');
               $table->integer('lost_boxes')->default(0);
               $table->string('month');
               $table->timestamps();
           }
       );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_boxes');
    }
}
