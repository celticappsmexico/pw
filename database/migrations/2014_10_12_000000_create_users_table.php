<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('username');
          $table->string('avatar')->default('assets/img/avatar/default.jpg');
          $table->string('email')->unique();
          $table->string('password', 60);
          $table->string('name');
          $table->string('lastName');
          $table->string('companyName');
          $table->string('peselNumber');
          $table->string('idNumber');
          $table->string('nipNumber');
          $table->string('regonNumber');
          $table->string('courtNumber');
          $table->string('courtPlace');
          $table->string('krsNumber');
          $table->integer('Role_id');
          $table->integer('userType_id');
          $table->string('phone');
          $table->date('birthday');
          $table->integer('active');
          $table->integer('validate');
          $table->integer('flag_oportunity')->default(0);
          $table->date('oportunity_reminder');
          $table->text('notes');
          $table->string('accountant_number');
          $table->integer('accountant_code');
          $table->integer('customer_id');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
