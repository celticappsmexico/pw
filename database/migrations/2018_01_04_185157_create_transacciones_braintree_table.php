<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionesBraintreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('transacciones_braintree', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('cliente_id');
    		$table->string('tb_tipo');//Registro de credit card, cargo de current month, cargo de prepay, settled
    		$table->string('tb_estatus');
    		$table->text('tb_respuesta');
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::dropIfExists('transacciones_braintree');
    }
}
