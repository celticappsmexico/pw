<?php

use Illuminate\Database\Seeder;
use App\CataPaymentTypes;

class PaymentTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      CataPaymentTypes::create(['name' => 'Cash','active' => 1]);
      CataPaymentTypes::create(['name' => 'Credit Card','active' => 1]);
      //CataPaymentTypes::create(['name' => 'Check','active' => 1]);
      CataPaymentTypes::create(['name' => 'Transfer','active' => 1]);
    }
}
