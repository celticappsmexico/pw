<?php

use Illuminate\Database\Seeder;
use App\CataSecciones;

class CataSeccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CataSecciones::create(['nombre' => 'Living room']);
        CataSecciones::create(['nombre' => 'Kitchen and dining']);
        CataSecciones::create(['nombre' => 'Bedroom']);
        CataSecciones::create(['nombre' => 'Childrens room']);
        CataSecciones::create(['nombre' => 'Home office']);
        CataSecciones::create(['nombre' => 'Laundry and cleaning']);
        CataSecciones::create(['nombre' => 'Garden']);
        CataSecciones::create(['nombre' => 'Garden shed']);
    }
}
