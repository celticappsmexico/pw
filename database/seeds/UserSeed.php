<?php

use Illuminate\Database\Seeder;
use App\User;
use App\clientAddress;
use App\LegalRepresentative;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create(['username' => 'nova.admin',
          'email' => 'noe.alonso@novacloud.mx',
          'password' => bcrypt('123456'),
          'avatar' => 'assets/img/avatar/default.jpg',
          'name' => 'Admin',
          'Role_id' => '1',
          'userType_id' => '3',
          'active' => '1',
          'validate' => '1',
          'remember_token' => str_random(10)
        ]);

      clientAddress::create([
          'user_id' => '1',
          'street' => 'Av. Plan de Ayala',
          'number' => '426',
          'apartmentNumber' => 'Piso 2',
          'postCode' => '62410',
          'city' => 'Cuernavaca, Morelos',
          'country_id' => '484'
        ]);

        LegalRepresentative::create([
          'user_id' => 1,
          'name' => 'Jorge David',
          'lastName' => 'Mendoza',
          'function' => 'Desarrollador Web Jr.',
          'phone' => '52 777 3 99 14 42',
          'email' => 'jorge.mendoza@novacloud.mx',
        ]);

        LegalRepresentative::create([
          'user_id' => 1,
          'name' => 'Hugo',
          'lastName' => 'Flores',
          'function' => 'Desarrollador Android Sr.',
          'phone' => '52 777 3 99 14 42',
          'email' => 'hugo.flores@novacloud.mx',
        ]);

        User::create(['username' => 'prueba',
            'email' => 'test@novacloud.mx',
            'password' => bcrypt('123456'),
            'avatar' => 'assets/img/avatar/default.jpg',
            'name' => 'Aldo Benites',
            'Role_id' => '3',
            'userType_id' => '1',
            'active' => '1',
            'validate' => '1',
            'remember_token' => str_random(10)
          ]);

          clientAddress::create([
              'user_id' => '2',
              'street' => 'Av. Plan de Ayala',
              'number' => '426',
              'apartmentNumber' => 'Piso 2',
              'postCode' => '62410',
              'city' => 'Cuernavaca, Morelos',
              'country_id' => '484'
            ]);
    }
}
