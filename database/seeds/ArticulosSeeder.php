<?php

use Illuminate\Database\Seeder;
use App\Articulos;

class ArticulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Articulos::create(['nombre' => '2-seater sofa', 'img_path' => 'living/two-seats-sofa.jpg', 'id_seccion' => '1', 'sqm' => '0.8', 'active' => '1']);
        Articulos::create(['nombre' => '3-seater sofa', 'img_path' => 'living/three-seats-sofa.jpg', 'id_seccion' => '1', 'sqm' => '1.0', 'active' => '1']);
        Articulos::create(['nombre' => 'Corner sofa', 'img_path' => 'living/corner-sofa.jpg', 'id_seccion' => '1', 'sqm' => '1.5', 'active' => '1']);
        Articulos::create(['nombre' => 'Chaise longue', 'img_path' => 'living/chaise-longue.jpg', 'id_seccion' => '1', 'sqm' => '0.7', 'active' => '1']);
        Articulos::create(['nombre' => 'Armchair', 'img_path' => 'living/armchair.jpg', 'id_seccion' => '1', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Footstool', 'img_path' => 'living/footstool.jpg', 'id_seccion' => '1', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Coffee table', 'img_path' => 'living/coffee-table.jpg', 'id_seccion' => '1', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Side table', 'img_path' => 'living/side-table.jpg', 'id_seccion' => '1', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Bookcase', 'img_path' => 'living/bookcase.jpg', 'id_seccion' => '1', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'TV cabinet', 'img_path' => 'living/tv-cabinet.jpg', 'id_seccion' => '1', 'sqm' => '1.1', 'active' => '1']);
        Articulos::create(['nombre' => 'TV bench', 'img_path' => 'living/tv-bench.jpg', 'id_seccion' => '1', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Television', 'img_path' => 'living/tv.jpg', 'id_seccion' => '1', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Piano', 'img_path' => 'living/piano.jpg', 'id_seccion' => '1', 'sqm' => '1.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Rug', 'img_path' => 'living/rug.jpg', 'id_seccion' => '1', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Floor lamp', 'img_path' => 'living/floor-lamp.jpg', 'id_seccion' => '1', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Picture frame', 'img_path' => 'living/photo-frame2.jpg', 'id_seccion' => '1', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Artificial plant', 'img_path' => 'living/plant.jpg', 'id_seccion' => '1', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'living/Small_box_lrg.jpg', 'id_seccion' => '1', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'living/Standard_box_lrg.jpg', 'id_seccion' => '1', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'living/Large_box_lrg.jpg', 'id_seccion' => '1', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Dining table', 'img_path' => 'kitchen/dining-table.jpg', 'id_seccion' => '2', 'sqm' => '0.5', 'active' => '1']);
        Articulos::create(['nombre' => 'Dining chair', 'img_path' => 'kitchen/dining-chair.jpg', 'id_seccion' => '2', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Stool', 'img_path' => 'kitchen/stool.jpg', 'id_seccion' => '2', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Bar stool', 'img_path' => 'kitchen/bar-stool.jpg', 'id_seccion' => '2', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Highchair', 'img_path' => 'kitchen/highchair.jpg', 'id_seccion' => '2', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Display cabinet', 'img_path' => 'kitchen/display-cabinet.jpg', 'id_seccion' => '2', 'sqm' => '0.8', 'active' => '1']);
        Articulos::create(['nombre' => 'Sideboard', 'img_path' => 'kitchen/sideboard2.jpg', 'id_seccion' => '2', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Fridge', 'img_path' => 'kitchen/fridge.jpg', 'id_seccion' => '2', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Fridge freezer', 'img_path' => 'kitchen/fridge-freezer.jpg', 'id_seccion' => '2', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'American fridge', 'img_path' => 'kitchen/american-fridge.jpg', 'id_seccion' => '2', 'sqm' => '0.7', 'active' => '1']);
        Articulos::create(['nombre' => 'Freezer', 'img_path' => 'kitchen/freezer.jpg', 'id_seccion' => '2', 'sqm' => '0.5', 'active' => '1']);
        Articulos::create(['nombre' => 'Dish washer', 'img_path' => 'kitchen/dish-washer.jpg', 'id_seccion' => '2', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Oven', 'img_path' => 'kitchen/oven.jpg', 'id_seccion' => '2', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Microwave', 'img_path' => 'kitchen/microwave-oven.jpg', 'id_seccion' => '2', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Bin', 'img_path' => 'kitchen/bin.jpg', 'id_seccion' => '2', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Picture frame', 'img_path' => 'kitchen/photo-frame2.jpg', 'id_seccion' => '2', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Artificial plant', 'img_path' => 'kitchen/plant.jpg', 'id_seccion' => '2', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'kitchen/Small_box_lrg.jpg', 'id_seccion' => '2', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'kitchen/Standard_box_lrg.jpg', 'id_seccion' => '2', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'kitchen/Large_box_lrg.jpg', 'id_seccion' => '2', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Double bed', 'img_path' => 'bedroom/double-bed.jpg', 'id_seccion' => '3', 'sqm' => '0.9', 'active' => '1']);
        Articulos::create(['nombre' => 'Single bed', 'img_path' => 'bedroom/single-bed.jpg', 'id_seccion' => '3', 'sqm' => '0.8', 'active' => '1']);
        Articulos::create(['nombre' => 'Double mattress', 'img_path' => 'bedroom/double-mattress.jpg', 'id_seccion' => '3', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Single mattress', 'img_path' => 'bedroom/single-mattress.jpg', 'id_seccion' => '3', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => '2-door wardrobe', 'img_path' => 'bedroom/wardrobe.jpg', 'id_seccion' => '3', 'sqm' => '1.2', 'active' => '1']);
        Articulos::create(['nombre' => '4-door wardrobe', 'img_path' => 'bedroom/wardrobe4.jpg', 'id_seccion' => '3', 'sqm' => '2.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Chest of drawers', 'img_path' => 'bedroom/chest-of-drawers.jpg', 'id_seccion' => '3', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Bedside table', 'img_path' => 'bedroom/bedside-table.jpg', 'id_seccion' => '3', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Dressing table', 'img_path' => 'bedroom/dressing-table.jpg', 'id_seccion' => '3', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Mirror', 'img_path' => 'bedroom/mirror.jpg', 'id_seccion' => '3', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Floor lamp', 'img_path' => 'bedroom/floor-lamp.jpg', 'id_seccion' => '3', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Picture frame', 'img_path' => 'bedroom/photo-frame2.jpg', 'id_seccion' => '3', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Artificial plant', 'img_path' => 'bedroom/plant.jpg', 'id_seccion' => '3', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Rug', 'img_path' => 'bedroom/rug.jpg', 'id_seccion' => '3', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'bedroom/Small_box_lrg.jpg', 'id_seccion' => '3', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'bedroom/Standard_box_lrg.jpg', 'id_seccion' => '3', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'bedroom/Large_box_lrg.jpg', 'id_seccion' => '3', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Changing table', 'img_path' => 'childrens/changing-table.jpg', 'id_seccion' => '4', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Cot', 'img_path' => 'childrens/cot.jpg', 'id_seccion' => '4', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Childrens bed', 'img_path' => 'childrens/children-bed.jpg', 'id_seccion' => '4', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Bunk bed', 'img_path' => 'childrens/bunk-bed.jpg', 'id_seccion' => '4', 'sqm' => '1.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Childrens wardrobe', 'img_path' => 'childrens/children-wardrobe.jpg', 'id_seccion' => '4', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Chest of drawers', 'img_path' => 'childrens/children-chest-of-drawers2.jpg', 'id_seccion' => '4', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Childrens table', 'img_path' => 'childrens/play-table.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Childrens chair', 'img_path' => 'childrens/play-chair.jpg', 'id_seccion' => '4', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Desk', 'img_path' => 'childrens/children-desk.jpg', 'id_seccion' => '4', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Chair', 'img_path' => 'childrens/children-swivel-chair.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Bookcase', 'img_path' => 'childrens/bookcase.jpg', 'id_seccion' => '4', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Baby bath', 'img_path' => 'childrens/baby-bath.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Toy box', 'img_path' => 'childrens/toy-boxes.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Rug', 'img_path' => 'childrens/children-rug.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'childrens/Small_box_lrg.jpg', 'id_seccion' => '4', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'childrens/Standard_box_lrg.jpg', 'id_seccion' => '4', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'childrens/Large_box_lrg.jpg', 'id_seccion' => '4', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Desk', 'img_path' => 'home/desk.jpg', 'id_seccion' => '5', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Office chair', 'img_path' => 'home/office-chair.jpg', 'id_seccion' => '5', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Filing cabinet', 'img_path' => 'home/file-cabinet.jpg', 'id_seccion' => '5', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Large file cabinet', 'img_path' => 'home/large-cabinet.jpg', 'id_seccion' => '5', 'sqm' => '0.6', 'active' => '1']);
        Articulos::create(['nombre' => 'Bookcase', 'img_path' => 'home/bookcase.jpg', 'id_seccion' => '5', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Shelving unit', 'img_path' => 'home/shelving-unit.jpg', 'id_seccion' => '5', 'sqm' => '0.7', 'active' => '1']);
        Articulos::create(['nombre' => 'Picture frame', 'img_path' => 'home/photo-frame2.jpg', 'id_seccion' => '5', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Artificial plant', 'img_path' => 'home/plant.jpg', 'id_seccion' => '5', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Paper bin', 'img_path' => 'home/paper-bin.jpg', 'id_seccion' => '5', 'sqm' => '0.025', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'home/Small_box_lrg.jpg', 'id_seccion' => '5', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'home/Standard_box_lrg.jpg', 'id_seccion' => '5', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'home/Large_box_lrg.jpg', 'id_seccion' => '5', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Washing machine', 'img_path' => 'laundry/washing-machine.jpg', 'id_seccion' => '6', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Tumble dryer', 'img_path' => 'laundry/dryer.jpg', 'id_seccion' => '6', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Drying rack', 'img_path' => 'laundry/drying-rack.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Ironing board', 'img_path' => 'laundry/ironing-table.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Laundry basket', 'img_path' => 'laundry/laundry-basket.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Linen rack', 'img_path' => 'laundry/linen-rack.jpg', 'id_seccion' => '6', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Vacuum cleaner', 'img_path' => 'laundry/vacuum-cleaner.jpg', 'id_seccion' => '6', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Shoe rack', 'img_path' => 'laundry/shoe-rack.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Bin', 'img_path' => 'laundry/bin.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'laundry/Small_box_lrg.jpg', 'id_seccion' => '6', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'laundry/Standard_box_lrg.jpg', 'id_seccion' => '6', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'laundry/Large_box_lrg.jpg', 'id_seccion' => '6', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Garden table', 'img_path' => 'garden/garden-table.jpg', 'id_seccion' => '7', 'sqm' => '0.8', 'active' => '1']);
        Articulos::create(['nombre' => 'Garden chair', 'img_path' => 'garden/garden-chair.jpg', 'id_seccion' => '7', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Garden bench', 'img_path' => 'garden/garden-bench.jpg', 'id_seccion' => '7', 'sqm' => '0.5', 'active' => '1']);
        Articulos::create(['nombre' => 'Parasol', 'img_path' => 'garden/parasol.jpg', 'id_seccion' => '7', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Lawn mower', 'img_path' => 'garden/lawn-mower.jpg', 'id_seccion' => '7', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Outdoor grill', 'img_path' => 'garden/outdoor-grill.jpg', 'id_seccion' => '7', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Adult´s bicycle', 'img_path' => 'garden/adult-bike.jpg', 'id_seccion' => '7', 'sqm' => '0.3', 'active' => '1']);
        Articulos::create(['nombre' => 'Kid´s bicycle', 'img_path' => 'garden/kid-bike.jpg', 'id_seccion' => '7', 'sqm' => '0.2', 'active' => '1']);
        Articulos::create(['nombre' => 'Garden tools', 'img_path' => 'garden/garden-tools.jpg', 'id_seccion' => '7', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'garden/Small_box_lrg.jpg', 'id_seccion' => '7', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'garden/Standard_box_lrg.jpg', 'id_seccion' => '7', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'garden/Large_box_lrg.jpg', 'id_seccion' => '7', 'sqm' => '0.1', 'active' => '1']);

        Articulos::create(['nombre' => 'Workbench', 'img_path' => 'garden-shed/workbench.jpg', 'id_seccion' => '8', 'sqm' => '0.4', 'active' => '1']);
        Articulos::create(['nombre' => 'Toolbox', 'img_path' => 'garden-shed/tool-box.jpg', 'id_seccion' => '8', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Ladder', 'img_path' => 'garden-shed/ladder.jpg', 'id_seccion' => '8', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Suitcase', 'img_path' => 'garden-shed/suitcase.jpg', 'id_seccion' => '8', 'sqm' => '0.1', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-small', 'img_path' => 'garden-shed/Small_box_lrg.jpg', 'id_seccion' => '8', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-medium', 'img_path' => 'garden-shed/Standard_box_lrg.jpg', 'id_seccion' => '8', 'sqm' => '0.05', 'active' => '1']);
        Articulos::create(['nombre' => 'Moving box-large', 'img_path' => 'garden-shed/Large_box_lrg.jpg', 'id_seccion' => '8', 'sqm' => '0.1', 'active' => '1']);
    }
}
