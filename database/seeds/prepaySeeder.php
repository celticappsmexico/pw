<?php

use Illuminate\Database\Seeder;
use App\CataPrepay;

class prepaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      CataPrepay::create(['months' => 0,'off' => 0]);
      CataPrepay::create(['months' => 1,'off' => 0]);
      CataPrepay::create(['months' => 3,'off' => 5]);
      CataPrepay::create(['months' => 6,'off' => 10]);
      CataPrepay::create(['months' => 12,'off' => 15]);
    }
}
