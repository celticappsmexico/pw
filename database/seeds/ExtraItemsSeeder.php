<?php

use Illuminate\Database\Seeder;
use App\ExtraItems;

class ExtraItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExtraItems::create(['nombre' => 'Scotch tape', 'img_path' => 'scotch-tape.png', 'price' => '20']);
        ExtraItems::create(['nombre' => 'Box', 'img_path' => 'box.png', 'price' => '50']);
        ExtraItems::create(['nombre' => 'Bag', 'img_path' => 'bag.png', 'price' => '30']);
    }
}
