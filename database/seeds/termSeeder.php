<?php

use Illuminate\Database\Seeder;
use App\CataTerm;

class termSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      CataTerm::create(['months' => 1,'off' => 0]);
      CataTerm::create(['months' => 3,'off' => 5]);
      CataTerm::create(['months' => 6,'off' => 10]);
      CataTerm::create(['months' => 12,'off' => 15]);
    }
}
