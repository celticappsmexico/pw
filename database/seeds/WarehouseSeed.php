<?php

use Illuminate\Database\Seeder;
use App\Warehouse;

class WarehouseSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Warehouse::create([
        	'name' => 'Red and green',
          	'country_id' => '616',
          	'address' => 'xxxxxxxxxx xxxxxxxxxxx xxxxxxx',
          	'active' => '1',
        ]);

        Warehouse::create([
        	'name' => 'Blue',
          	'country_id' => '616',
          	'address' => 'xxxxxxxxxx xxxxxxxxxxx xxxxxxx',
          	'active' => '1',
        ]);
    }
}
