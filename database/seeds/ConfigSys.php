<?php

use Illuminate\Database\Seeder;
use App\ConfigSystem;

class ConfigSys extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      ConfigSystem::create(['name' => 'insurance','value' => 16]);
      ConfigSystem::create(['name' => 'vat','value' => 23]);
      ConfigSystem::create(['name' => 'place','value' => 'Warszawa']);
      ConfigSystem::create(['name' => 'address','value' => '05-500 Piaseczno, ul. Kineskopowa 1']);
      ConfigSystem::create(['name' => 'nip','value' => '5272703362']);
      ConfigSystem::create(['name' => 'bank','value' => 'mBank Oddziaá Korporacyjny']);
      ConfigSystem::create(['name' => 'account','value' => '75 1140 1010 0000 5469 8300']);
      ConfigSystem::create(['name' => 'invoice_number','value' => '0123']);
      ConfigSystem::create(['name' => 'seller','value' => 'PRZECHOWAMY WSZYSTKO Sp. z o.o.']);
    }
}
