<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UserTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      UserType::create(['name' => 'Person']);
      UserType::create(['name' => 'One Person Company']);
      UserType::create(['name' => 'Company']);
    }
}
