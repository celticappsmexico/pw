<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\User;
use App\UserType;
use App\Warehouse;
use App\Levels;
use App\Articulos;
use App\CataSecciones;
use App\Storages;
use App\ExtraItems;
use App\CataPrepay;
use App\CataTerm;
use App\CataPaymentTypes;
use App\ConfigSystem;
use App\DetOrders;
use App\Billing;
use App\Orders;
use App\PaymentData;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->command->comment('************** Truncando tablas... **************');
        //User::truncate();
        //Warehouse::truncate();
        //Storages::truncate();
        //Levels::truncate();
        //DetOrders::truncate();
        //Billing::truncate();
        //Orders::truncate();
        //PaymentData::truncate();

        Role::truncate();
        UserType::truncate();
        Articulos::truncate();
        CataSecciones::truncate();
        ExtraItems::truncate();
        CataPrepay::truncate();
        CataTerm::truncate();
        CataPaymentTypes::truncate();
        ConfigSystem::truncate();



        $this->command->info('seeding...!');
        //$this->call('UserTableSeeder');
        //$this->call('UserSeed');
        //factory(App\User::class, 10)->create();
        //$this->call('WarehouseSeed');
        //$this->call('LevelsSeed');
        //$this->call('StoragesSeed');

        $this->call('RolesSeed');
        $this->call('UserTypeSeed');
        $this->call('CountriesSeeder');
        $this->call('ArticulosSeeder');
        $this->call('CataSeccionesSeeder');
        $this->call('ExtraItemsSeeder');
        $this->call('prepaySeeder');
        $this->call('termSeeder');
        $this->call('PaymentTypesSeed');
        $this->call('ConfigSys');
        
        $this->call('UserSeed');
        
        $this->command->info('Seeded!');
        $this->command->comment('************** FIN! **************');

        Model::reguard();
    }
}
