<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::create(['name' => 'Administrator']);
      Role::create(['name' => 'Manager']);
      Role::create(['name' => 'Client']);
    }
}
